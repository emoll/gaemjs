mkdir -p executable
sudo cat js/*.js > executable/allmine.js

#privatize it!
#sed -i '1s/^/(function(){ /' executable/allmine.js
#echo '})();' >> executable/allmine.js

#google-closure-compiler executable/allmine.js > executable/allmine.min.js
# Use the following if you don't care about keeping variable names: --compilation_level ADVANCED_OPTIMIZATIONS

# Recursively include all js files in subdirs
java -jar compiler.jar --js_output_file='executable/allmine.min.js' 'executable/allmine.js'

sudo cat lib/*.js executable/allmine.min.js > executable/all.min.js
printf 'Finished! Press [ENTER] to continue...'
read _