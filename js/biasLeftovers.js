//biasLeftovers.js

var activeSimulationMemory = {}
var actions = {}
//How do I come up with action candidates?
function IsActionViable(action) {
	return IsConditionMet(action.precondition)
}

function IsConditionMet() {
	//this is like the is() function or whatever....
}
//what described were conditions where as actions are processes. (I have no idea what I tried to say with this sentence...)

//move to theAlgorithm
var activePlayerModel = {
	//**Player model** (user)
	//	In order to present the right game to the right person the algorithm must figure out both the general human biases and the specifics of each human subject.
	//	getting a feel for the person, their playing style, the direction they tend to aproach.
	//	this is to know things like whether
	//		the player had fun
	//		the aesthetic was conveyed
	//		what emotions the player felt
	//		etc.
	//	What is the player seeking?
	//	What is the player driven by?
	//	What are the player's needs?
}

//user story climbing up a huge mountain and camera zooms out a lot to get cool perspective showing your smallness to the world.
//portable home/house, take it with you like a tent or an item in animal crossing
//I need to create underwater cavens where you have to jump of a cliff into water to get deep enough to get around 
//the corner to the entrance of the cavern.
//putting together pieces of a powerful object
//like when you rebuild this you get a powerful thing that does things.
//Tidal sea waves through simplex noise 
//	I can create tidal waves by adding a sinewave that cycle 24 or longer.
//	If I don't want the gradual change I also just keep the tops of the sinewave to create more of a single event.


//language for encouraged or intented player activity.
//	like, the sequence of action you intend they can do.
//		pick fruits from trees, put them in bag, bring them home and cook a fruit pie.
//	explore the world
//	etc

//how to make an anonymous object:
var anonymousObjects = []
anonymousObjects.push({
	is: {
		cool: 1,
		secret: 1,
		anonymous: 1,
		object: 1,
	}
})
//example: rectangleWood lol which can be used for fire experiments. Not that it is hard to label it rectngleWood, but the point is it doesn't NEED a label.
anonymousObjects.push({
	is: {
		rectangle: 1,
		wood: 1
	}
})
