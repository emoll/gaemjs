//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* Utils.js */

/* File content description:
	
	Function that are now and then and are handy to access anytime anywhere
*/

/* Todo:
	Add File content description
*/


function perf(func, ...args) { //returns duration in ms it takes to run the function once.
	//may be restricted by security reasons. bigger functions create better accuracy.
	var startTime = performance.now()
	func(...args)
	return performance.now() - startTime
}

// Console Interface
var log = (...args) => {
	console.log(...args)
}

function Run(project) {
	project()
}

function Noise(val) {
	var result = val * (Math.random() * 2 - 1)
	return result
}


function Aprox(posA, posB, percent) {
	return posA + percent * (posB - posA)
}

function Clamp(min, max) {
	return Math.min(Math.max(this, min), max)
}

//Mainly for Closure Compiler
function DefaultParameter(p, val) {
	if (typeof p !== 'undefined') {
		return p
	} else {
		//console.warn('Defualt parameter was replaced by', val)
		return val
	}
}


function IsUndefined(p) {
	return typeof p == 'undefined' ? true : false
}

function CallErrorOnUndefined(args) {
	for (var i = 0; i < args.length; i++) {
		if (IsUndefined(args[i])) throw Error("argument " + (i + 1) + " is undefined.")
	}
}

function CallWarningOnUndefined(args) {
	for (var i = 0; i < args.length; i++) {
		if (IsUndefined(args[i])) console.warn("argument " + (i + 1) + " is undefined.")
	}
}

function SetAngle(angle) { //limits a value within 2 * math.pi
	while (angle < 0) {
		angle += 2 * Math.PI
	}
	return angle % (2 * Math.PI)
}

function GetAngle(pointA, pointB) {
	return Math.atan2(pointB.y - pointA.y, pointB.x - pointA.x)
}

function GetDistance(pointA, pointB) {
	return Math.sqrt((pointA.x - pointB.x) * (pointA.x - pointB.x) + (pointA.y - pointB.y) * (pointA.y - pointB.y))
}

function GetRandomDir() {
	return Math.random() * 2 * Math.PI
}

function GetRandomPointOnCanvas() {
	var point
	point.x = GetRandomRange(0, canvas.width)
	point.y = GetRandomRange(0, canvas.height)
	return point
}

function GetRandomPointInRect(rect) {
	var point
	point.x = GetRandomRange(rect.x, rect.x + rect.width)
	point.y = GetRandomRange(rect.y, rect.y + rect.height)
	return point
}

function GetRandomRange(min, max) {
	return Math.random() * (max - min) + min
}

function GetRandomRangeInt(min, max) {
	return Math.round(GetRandomRange(min, max))
}

function GetRandomColor() {
	var letters = '0123456789ABCDEF'
	var color = '#'
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)]
	}
	return color
}

//Collision checking
function IsPosInRect(pos, rect) {
	return pos.x > rect.x && pos.x < rect.x + rect.width && pos.y < rect.y + rect.height && pos.y > rect.y
}

function Lerp(A, B, t) {
	return A * t + B * (1 - t)
}

function lerp(v0, v1, t) {
	return v0 * (1 - t) + v1 * t
}

function Vector2Lerp(A, B, t) { //Work in progress.
	var d = Math.sqrt((A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y))
	return A
}

function Point(x, y) {
	this.x = x
	this.y = y
}






function IsFunction(obj) {
	return !!(obj && obj.constructor && obj.call && obj.apply)
}

function FuncValue(obj, ...args) {
	return IsFunction(obj) ? obj(...args) : obj //Get function value else variable value
}

function InfiniteFuncValue(obj, ...args) { //TODO make a version without this keyword if shit breaks
	if (IsFunction(obj)) {
		//Using 'this' to fix this keyword context...
		this.obj9876 = obj // Using obscure variable name to not accidentally override local variable with similar naming
		return InfiniteFuncValue.call(this, this.obj9876(...args))
	} else {
		return obj //Get function value else variable value
	}
}

function SumValueOfFunctions(...functions) { // adds together the value from a set of functions
	var sum = 0
	functions.forEach(function (func) {
		sum += InfiniteFuncValue(func)
	})
	return sum
}

function AverageValueOfFunctions(...functions) {
	return SumValueOfFunctions(functions) / functions.length
}

function Clone(object) {
	return jQuery.extend(true, {}, object) //Object.assign({}, object)
}

var dirs = {
	up: -Math.PI / 2,
	left: Math.PI,
	right: 0,
	down: Math.PI / 2
}


//Colors

//rgbToHex.js
function componentToHex(c) {
	var hex = c.toString(16)
	return hex.length == 1 ? "0" + hex : hex
}

function rgbToHex(r, g, b) {
	return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b)
}

function SetPositionToRandomOnCanvas(obj) {
	obj.x = GetRandomRange(0, canvas.width)
	obj.y = GetRandomRange(0, canvas.height)
}

function WrapAround(coordinate, max) {
	coordinate %= max + 1
	return (coordinate < 0) ? coordinate + max : coordinate
}

/**
 * A linear interpolator for hexadecimal colors
 * @param {String} a
 * @param {String} b
 * @param {Number} amount
 * @example
 * // returns #7F7F7F
 * lerpColor('#000000', '#ffffff', 0.5)
 * @returns {String}
 */
function lerpColor(a, b, amount) {

	//hack
	a = InfiniteFuncValue(a)
	b = InfiniteFuncValue(b)
	amount = InfiniteFuncValue(amount)
	//end of hack

	var ah = parseInt(a.replace(/#/g, ''), 16),
		ar = ah >> 16,
		ag = ah >> 8 & 0xff,
		ab = ah & 0xff,
		bh = parseInt(b.replace(/#/g, ''), 16),
		br = bh >> 16,
		bg = bh >> 8 & 0xff,
		bb = bh & 0xff,
		rr = ar + amount * (br - ar),
		rg = ag + amount * (bg - ag),
		rb = ab + amount * (bb - ab)

	return '#' + ((1 << 24) + (rr << 16) + (rg << 8) + rb | 0).toString(16).slice(1)
}

/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSL representation
 */
function rgbToHsl(r, g, b) {
	r /= 255, g /= 255, b /= 255

	var max = Math.max(r, g, b),
		min = Math.min(r, g, b)
	var h, s, l = (max + min) / 2

	if (max == min) {
		h = s = 0 // achromatic
	} else {
		var d = max - min
		s = l > 0.5 ? d / (2 - max - min) : d / (max + min)

		switch (max) {
			case r:
				h = (g - b) / d + (g < b ? 6 : 0)
				break
			case g:
				h = (b - r) / d + 2
				break
			case b:
				h = (r - g) / d + 4
				break
		}

		h /= 6
	}

	return [h, s, l]
}

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  l       The lightness
 * @return  Array           The RGB representation
 */
function hslToRgb(h, s, l) {
	var r, g, b

	if (s == 0) {
		r = g = b = l // achromatic
	} else {
		function hue2rgb(p, q, t) {
			if (t < 0) t += 1
			if (t > 1) t -= 1
			if (t < 1 / 6) return p + (q - p) * 6 * t
			if (t < 1 / 2) return q
			if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6
			return p
		}

		var q = l < 0.5 ? l * (1 + s) : l + s - l * s
		var p = 2 * l - q

		r = hue2rgb(p, q, h + 1 / 3)
		g = hue2rgb(p, q, h)
		b = hue2rgb(p, q, h - 1 / 3)
	}

	return [r * 255, g * 255, b * 255]
}

/**
 * Converts an RGB color value to HSV. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and v in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSV representation
 */
function rgbToHsv(r, g, b) {
	r /= 255, g /= 255, b /= 255

	var max = Math.max(r, g, b),
		min = Math.min(r, g, b)
	var h, s, v = max

	var d = max - min
	s = max == 0 ? 0 : d / max

	if (max == min) {
		h = 0 // achromatic
	} else {
		switch (max) {
			case r:
				h = (g - b) / d + (g < b ? 6 : 0)
				break
			case g:
				h = (b - r) / d + 2
				break
			case b:
				h = (r - g) / d + 4
				break
		}

		h /= 6
	}

	return [h, s, v]
}

/**
 * Converts an HSV color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes h, s, and v are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  v       The value
 * @return  Array           The RGB representation
 */
function hsvToRgb(h, s, v) {
	var r, g, b

	var i = Math.floor(h * 6)
	var f = h * 6 - i
	var p = v * (1 - s)
	var q = v * (1 - f * s)
	var t = v * (1 - (1 - f) * s)

	switch (i % 6) {
		case 0:
			r = v, g = t, b = p
			break
		case 1:
			r = q, g = v, b = p
			break
		case 2:
			r = p, g = v, b = t
			break
		case 3:
			r = p, g = q, b = v
			break
		case 4:
			r = t, g = p, b = v
			break
		case 5:
			r = v, g = p, b = q
			break
	}

	return [r * 255, g * 255, b * 255]
}

function changeHue(rgb, degree) {
	var hsl = rgbToHSL(rgb);
	hsl.h += degree;
	if (hsl.h > 360) {
		hsl.h -= 360;
	} else if (hsl.h < 0) {
		hsl.h += 360;
	}
	return hslToRGB(hsl);
}

// exepcts a string and returns an object
function rgbToHSL(rgb) {
	// strip the leading # if it's there
	rgb = rgb.replace(/^\s*#|\s*$/g, '');

	// convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
	if (rgb.length == 3) {
		rgb = rgb.replace(/(.)/g, '$1$1');
	}

	var r = parseInt(rgb.substr(0, 2), 16) / 255,
		g = parseInt(rgb.substr(2, 2), 16) / 255,
		b = parseInt(rgb.substr(4, 2), 16) / 255,
		cMax = Math.max(r, g, b),
		cMin = Math.min(r, g, b),
		delta = cMax - cMin,
		l = (cMax + cMin) / 2,
		h = 0,
		s = 0;

	if (delta == 0) {
		h = 0;
	} else if (cMax == r) {
		h = 60 * (((g - b) / delta) % 6);
	} else if (cMax == g) {
		h = 60 * (((b - r) / delta) + 2);
	} else {
		h = 60 * (((r - g) / delta) + 4);
	}

	if (delta == 0) {
		s = 0;
	} else {
		s = (delta / (1 - Math.abs(2 * l - 1)))
	}

	return {
		h: h,
		s: s,
		l: l
	}
}

// expects an object and returns a string
function hslToRGB(hsl) {
	var h = hsl.h,
		s = hsl.s,
		l = hsl.l,
		c = (1 - Math.abs(2 * l - 1)) * s,
		x = c * (1 - Math.abs((h / 60) % 2 - 1)),
		m = l - c / 2,
		r, g, b;

	if (h < 60) {
		r = c;
		g = x;
		b = 0;
	} else if (h < 120) {
		r = x;
		g = c;
		b = 0;
	} else if (h < 180) {
		r = 0;
		g = c;
		b = x;
	} else if (h < 240) {
		r = 0;
		g = x;
		b = c;
	} else if (h < 300) {
		r = x;
		g = 0;
		b = c;
	} else {
		r = c;
		g = 0;
		b = x;
	}

	r = normalize_rgb_value(r, m);
	g = normalize_rgb_value(g, m);
	b = normalize_rgb_value(b, m);

	return rgbToHex(r, g, b);
}

function normalize_rgb_value(color, m) {
	color = Math.floor((color + m) * 255);
	if (color < 0) {
		color = 0;
	}
	return color;
}

function rgbToHex(r, g, b) {
	return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function GetRandomColorInGradient(colorA, colorB) {
	return lerpColor(colorA, colorB, Math.random())
}

function GetRandomEntryInArray(array) {
	return array[Math.floor(Math.random() * array.length)]
}

function GetRandomColorInPalette(palette) {
	return GetRandomEntryInArray(palette)
}


function CheckCircleCollision(circleA, circleB) {
	var objA = circleA
	var objB = circleB

	var dx = circleA.x - circleB.x
	var dy = circleA.y - circleB.y
	var distance = GetDistance(circleA, circleB)
	var totalRadius = circleA.radius + circleB.radius

	if (distance < totalRadius) { // TODO: Replace with subscription / broadcasting
		objA.behaviours.unshift(function () {
			this.OnCollide(objB)
		})
		objB.behaviours.unshift(function () {
			this.OnCollide(objA)
		})
		return true
	}
	return false
}






function Is2DContext(input) {
	if (IsUndefined(input)) return false
	if (IsUndefined(input.constructor)) return false
	return input.constructor.name == "CanvasRenderingContext2D"
}

function GetContext(layerOrContext) {
	if (Is2DContext(layerOrContext)) return layerOrContext
	if (Is2DContext(layerOrContext.context)) {
		// Uncomment if you care:
		// console.log("Warning: Should provide layer, not context.")
		return layerOrContext.context
	}
	throw Error("no context exists on input value: " + layerOrContext)
}









function GetOutlineLayer(getter, thickness) {
	// I don't know how to make the interface for this. The outline should always be drawn first.
	//set thickness thicknessFunction + thickness;
}









var simplex = new SimplexNoise(Math.random())

function simp(x, z, amount, frequencyX, frequencyZ, offset = 0) {
	// It is hard to find online but it seems like simplex noise ranges from -1 to 1.
	// I think it is more intuitive that it goes from 0 to 1, hence the 0.5* and the +1
	return amount * 0.5 * (simplex.noise2D(x * frequencyX, z * frequencyZ) + 1)
}




var Distances = {
	prettyDarnClose: 10
}




function numToDir(num) {
	if (num == 0) return "up"
	else if (num == 1) return "right"
	else if (num == 2) return "down"
	else if (num == 3) return "left"
}

function dirToNum(dir) {
	if (dir == "up") return 0
	else if (dir == "right") return 1
	else if (dir == "down") return 2
	else if (dir == "left") return 3
}

function oppositeDir(dir) {
	var num = dirToNum(dir)
	num = (num + 2) % 4
	return numToDir(num)
}

function normalize(val, min, max) { //return where on a scale from min to max val is with a value between 0 to 1
	val = (val - min) / (max - min);
	if (val < 0) val = 0
	if (val > 1) val = 1
	return val
}





function rotate2dArray(array) {
	return array[0].map((col, i) => array.map(row => row[i]));
}


function getRandomPathWithoutGoingBack(steps) {
	var path = []
	var lastDirNum = -1
	for (var i = 0; i < steps; i++) {
		var newDirNum = GetRandomRangeInt(0, 3)
		console.log(newDirNum, lastDirNum)
		while (newDirNum == (lastDirNum + 2) % 4) {
			newDirNum = GetRandomRangeInt(0, 3)
			console.log("new dir is now", newDirNum)
		}
		path.push(numToDir(newDirNum))
		lastDirNum = newDirNum
	}
	return path
}

function posMove(pos, movement) {
	if (movement == "up") pos.y++
	if (movement == "down") pos.y--
	if (movement == "right") pos.x++
	if (movement == "left") pos.x--
	return pos
} // Grid function...



//function getCircleCurveIntersection(circle, curveFunction, isUpperCircle, isRightCurve, attempts, deviation) {
//    //old broken version
//    var c = circle
//    console.log("circle", c)
//    var f = curveFunction
//    var p = isRightCurve ? -1 : 1
//    var u = -1 //isUpperCircle ? 1 : -1
//    var offset = c.r
//    var x = c.x + offset //result
//    var yf //function y
//    var y_2 // circle y
//    for (var i = 0; i < attempts; i++) {
//        console.log(i, p)
//        offset *= .5
//        x += p * offset
//        yf = f(x)
//        y_2 = c.y + u * Math.sqrt((c.r * c.r) - ((c.x - x) * (c.x - x))) // circle edge y value
//        if (Math.abs(y_2 - yf) <= deviation) break
//        if (y_2 - yf < 0) p = 1
//        else if (y_2 - yf > 0) p = -1
//    }
//    return {
//        x: x,
//        y: yf
//    }
//} //old
function getCircleCurveIntersection(circle, curveFunction, isUpperCircle, isRightCurve, attempts, deviation) {
	var c = circle
	var f = curveFunction
	var p = isRightCurve ? 1 : -1
	var u = isUpperCircle ? -1 : 1 //remember that y is downwards!!
	var y_2 // circle edge y
	var bestCandidate = c.x
	for (var i = 0; i < attempts; i++) {
		var x = c.x + p * (c.r * i) / attempts
		y_2 = c.y + u * Math.sqrt((c.r * c.r) - ((c.x - x) * (c.x - x))) // circle edge y value
		if (Math.abs(y_2 - f(x)) < Math.abs(f(bestCandidate)) && GetDistance(c, {
				x,
				y: f(x)
			}) < c.r) {
			bestCandidate = x
		}
	}
	var returnPosition = {
		x: bestCandidate,
		y: f(bestCandidate)
	}
	return returnPosition
} //new bruteforce version


function circleAABBCollisionCheck(circle, rect) {
	DeltaX = circle.x - Math.max(rect.x, Math.min(circle.x, rect.x + rect.width));
	DeltaY = circle.y - Math.max(rect.y, Math.min(circle.y, rect.y + rect.height));
	return (DeltaX * DeltaX + DeltaY * DeltaY) < (circle.radius * circle.radius) ? (circle.radius * circle.radius) - (DeltaX * DeltaX + DeltaY * DeltaY) : false
}


function getKeyByValue(object, value) {
	return Object.keys(object).find(key => object[key] === value);
}



function getFunctionCalledThroughTarget(target, fn, ...args) {
	return function (...args) {
		return fn.call(target, ...args)
	}
}

function getSwappedKeyValue(pairs) {
	var ret = {};
	for (var key in pairs) {
		ret[pairs[key]] = key;
	}
	return ret;
}
