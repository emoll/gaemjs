//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* Behaviours.js */

/* File content description:
	
	Anything an entity can use to alter its own properties.
*/
/* Todo:
	Match the guidelines
	Create framework for behaviour trees
	Clarify what a behaviour is and how they should look
		(make it more specific so it can be used without confusion)
*/
/* Guidelines:
	Function name should make it clear it's a behaviour
	Function name should make it clear behaviour is a leaf node
	function name should make it clear behaviour is assembly of other behaviours
*/

// todo separate impulse behaviours from passive/process behaviours.
// for example jump is an impulse, whereas moveTowards is intended to be iterated many times.
//btw grow should be put here




//A behavior is a computed response of a system or organism to stimuli, inputs or a CONTEXT
//What makes it different from a reaction is the computed part, where there is a system chooses response depending on context
// It an extra layer of potential memorisation and decision making capabilities.
//A bhaviour is a choosen reaction
//This document probably just lists reactions.


function AddBehaviour(target, behaviour) {
	//Todo if (!behaviourIsLegit) throw Error()

	var nonExistingBehaviourUpdater = !target.PushBehaviour

	if (nonExistingBehaviourUpdater) {
		BehaviourListUpdater.call(target)
	}
	target.PushBehaviour(behaviour)
}


function Go() { //duplicate... todo remove this function.
	this.speed = DefaultParameter(this.speed, 0)
	this.speedIcr = DefaultParameter(this.speedIcr, 0) // I think speed increase should be a separate concept from go. Go is the act of going at speed. I understand the idea of Go as an update and that in Going you update your movement logic. But acceleration is not necesarily a part of going but rather a potential side effect. Maybe it is better to have like a Accelerate behaviour and push these separately.
	this.dir = DefaultParameter(this.dir, 0)
	this.speed += this.speedIcr
	this.x += Math.cos(this.dir) * this.speed
	this.y += Math.sin(this.dir) * this.speed
}

function Turn() { //duplicate... todo remove this function.
	this.dir += this.angle //TODO: Use turningAngle istead of angle
}

var react = {
	Jump: function () {
		if (this.jumps > 0) {
			this.vy *= .1 //cancel most of existing moment
			this.vy -= this.jumpForce
			this.jumps--
		}
	},
	Die: function () {
		this.dead = true
	},
	Turn: function () {
		this.dir += this.angle //TODO: Use turningAngle istead of angle
	},
	Go: function () { //Todo: make meaning less vague
		this.speed = DefaultParameter(this.speed, 0)
		this.speedIcr = DefaultParameter(this.speedIcr, 0) // I think speed increase should be a separate concept from go. Go is the act of going at speed. I understand the idea of Go as an update and that in Going you update your movement logic. But acceleration is not necesarily a part of going but rather a potential side effect. Maybe it is better to have like a Accelerate behaviour and push these separately.
		this.dir = DefaultParameter(this.dir, 0)
		this.speed += this.speedIcr
		this.x += Math.cos(this.dir) * this.speed
		this.y += Math.sin(this.dir) * this.speed
	},
	ForceGo: function () { //Todo: make meaning less vague
		this.forceX = DefaultParameter(this.forceX, 0)
		this.forceY = DefaultParameter(this.forceY, 0)
		this.forceX += Math.cos(this.dir) * this.speed
		this.forceY += Math.sin(this.dir) * this.speed
		this.x += this.forceX
		this.y += this.forceY
	},
	SpinAroundPoint: function (me, point, angle) {
		me = DefaultParameter(me, this)
		if (isNaN(point.x)) throw new Error("target:" + point + "'s .x is NaN!")
		if (isNaN(point.y)) throw new Error("target.y is NaN!!")
		if (isNaN(me.x)) throw new Error(me + "(me).x is NaN!")
		if (isNaN(me.y)) throw new Error(me + "(me).y is NaN!!")
		var oldx = me.x
		var oldy = me.y
		me.x = Math.cos(angle) * (oldx - point.x) - Math.sin(angle) * (oldy - point.y) + point.x
		me.y = Math.sin(angle) * (oldx - point.x) + Math.cos(angle) * (oldy - point.y) + point.y
	},
	MoveTowards: function (me, target, percent, minDistance, maxDistance) {
		//TODO: clean up this function. Separate it into smaller functions
		//TODO: add dt
		me = DefaultParameter(me, this)
		percent = DefaultParameter(percent, .5)
		minDistance = DefaultParameter(minDistance, 0)
		maxDistance = DefaultParameter(maxDistance, 5)

		if (target === undefined) throw new Error(me + " does not have a target")
		if (isNaN(target.x)) throw new Error("target.x is NaN!" + target.x)
		if (isNaN(target.y)) throw new Error("target.y is NaN!!" + target.y)
		if (isNaN(me.x)) throw new Error(me + "(me).x is NaN! " + me.color)
		if (isNaN(me.y)) throw new Error(me + "(me).y is NaN!!")

		if (me.x == target.x &&
			me.y == target.y)
			return

		//Target not reached yet

		/* untested */
		var distance = GetDistance(me, target)
		var angle = Math.atan2(target.y - me.y, target.x - me.x)
		/* end of untested */

		//TODO: make jelly suspesion it's own function
		//Jelly suspension
		//me.radius = 30-(distance/4.5	)

		if (distance > minDistance) {
			me.x += Math.cos(angle) * distance * percent //me.x = Lerp(me.x, target.x, percent)
			me.y += Math.sin(angle) * distance * percent //me.y = Lerp(me.y, target.y, percent)
		}

		if (maxDistance != -1 && distance > maxDistance) {
			me.x = target.x - Math.cos(angle) * maxDistance * .3
			me.y = target.y - Math.sin(angle) * maxDistance * .3
		}
	},
	contraction: function (pointA, pointB, contractionForceOrSomething) { //TODO: Define content and test
		//Point A and B moves towards eachother based on contraction force
		//basically eliminate distance by certain factors such as tension, rigidity, etc
	},
	bop: function (target) {
		SetPositionToRandomOnCanvas(target)
		setTimeout(function () {
			bop(target)
		}, GetRandomRange(0, 1000))
	},
	MoveTowardsTarget: function () {
		if (this == null) throw Error("Can't move since I don't exist")
		this.elasticity = DefaultParameter(this.elasticity, 0.5)
		if (!this.target) throw Error("this.target is invalid: " + this.target)

		MoveTowards(this, this.target, this.elasticity, 6.5, 20)
	},
	Gravity: function (me) { //untested //TODO: use, and put somewher else
		me.y++ // add multipliers and shit also dt
		// this should be yAcc++
	},
	spinAroundRandomEntity: function (speed) { //todo separate into spinaround(randomEntity)
		var randomEntity = entities[Math.floor(GetRandomRange(0, entities.length - 1))]
		return function () {
			SpinAroundPoint(this, randomEntity, speed)
		}
	},
	followRandomEntity: function () {
		var randomEntity = entities[Math.floor(GetRandomRange(0, entities.length - 1))]
		return function () {
			MoveTowards(this, randomEntity, .02)
		}
	}
}

function BehaviourListUpdater() {
	this.behaviours = DefaultParameter(this.behaviours, [])
	this.Update = function () {
		for (var i = 0; i < this.behaviours.length; i++) {
			if (this.behaviours[i].call(this) == -1) {
				this.behaviours.splice(i, 1)
			}
		}
	}
	this.PushBehaviour = function (behaviour) {
		//Todo: Check that behaviour is function
		this.behaviours.push(behaviour)
	}
}

// BEHAVIOURS


/* failed attempt at behaviour trees
function behaviourTreeNode{
	
	return 


	function composite

		function sequence()

		function tryAllOnce

	function timer
	function steps

}
function repeater()
*/



function SnekBehaviourTree() { // To specific and it's not a behavior tree...
	this.dir = DefaultParameter(this.dir, 0)
	this.speed = DefaultParameter(this.dir, 10)
	this.turnability = DefaultParameter(this.turnability, 10)
	this.maxSpeed = DefaultParameter(this.maxSpeed, 10)
	this.minSpeed = DefaultParameter(this.minSpeed, 1)
	this.speedChangeability = DefaultParameter(this.speedChangeability, 2)

	this.speed += Math.random() * this.speedChangeability - this.speedChangeability * .5
	if (this.speed > this.maxSpeed) {
		this.speed = this.maxSpeed
	}
	if (this.speed < this.minSpeed) {
		this.speed = this.minSpeed
	}

	this.dir += Math.random() * this.turnability - .5 * this.turnability
	Go(this.speed)
}

//Behaviour prefabs // THESE ARE WEIRD TODO: look into these
/* ERROR: pointer doesn't exist
var spinAroundMouseBehaviour = function () {
	SpinAroundPoint(this, pointer, .02)
}
*/

function GetBehaviourMoveTowardsTarget(target) {
	return function () {
		MoveTowards(this, target, .9)
	}
}
