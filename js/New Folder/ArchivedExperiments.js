//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* Draw.js */

//console.log("Testing my function sum thing", FuncSum(Math.cos(1), Math.sin(1)))

var functions = []

// V Copy Paste this V

functions.push(() => {

})

// ^ Copy Paste this ^


functions.push(() => {
	//console.log(Math.random() < .5 ? .1 : -.1)
	DrawTextureOnceInCenter(
		GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, 3, '#33550011', 500,
			[
				ExtraF.AlwaysTurning,
				ExtraF.BranchInterval(
					GetCraftedLayer(
						20,
						SetupF.ConstantSpeedWithBend,
						function (i, length) {
							return (1 - (i / length)) * Math.sin((i / length * 6) * Math.PI) * 1 + 5 //Trying to make some even oscilation. Can't remember all maths dough.
						},
						'#33aa3311',
						500
					),
					function () {
						return this.dir + dirs.right + .5
					},
					5
				),
				ExtraF.BranchInterval(
					GetCraftedLayer(
						20,
						SetupF.ConstantSpeedWithBend,
						function (i, length) {
							return (1 - (i / length)) * Math.sin((i / length * 6) * Math.PI) * 1 + 5 //Trying to make some even oscilation. Can't remember all maths dough.
						},
						'#33aa3311',
						500
					),
					function () {
						return this.dir + dirs.left - .5
					},
					5
				),

			]
		)
	)
})




// "Into the archive!" - Pål

functions.push(() => {
	var plant

	// Plant node definition!

	// ----------------------------
	//Experiments with plant nodes
	//	var plant
	/*
	    var numberOfPlants = 300
	    var splits = 6
	    for(var split = 0; split < splits;split++){
	        for(var i = 0; i < numberOfPlants/splits; i++){
	            plant = new PlantNode(0, 0)
	            var val = Math.round(100 * (split/splits)+155)
	            console.log(val)
	            plant.color =  rgbToHex(val, val, val)
	//             plant.x = GetRandomRange(400,800)
	//             plant.x = GetRandomRange(200,1000)

	    //         plant.x = GetRandomRange(650,750)
	            // plant.x = 700
	    //         plant.x = GetRandomRange(0, 1920)
	//             plant.x = GetRandomRange(24,488)
	             plant.x = GetRandomRange(1280/10,1280-1280/10)
	    //        plant.x = 200
	//            plant.y = 400+50*split
	            plant.y = 150+150*split
	        }
	    }


	    plant.SplitBranchSprite(0,0,0)
	*/


	var preGrowth = 0
	for (var i = 0; i < preGrowth; i++) {
		plant.Grow(.01, 1, i / 10)
	}
	//ctx.scale(scale, scale); //how to draw with scale
	// DrawTextureOnceInCenter(plant.sprite) // Experiment to just draw plant upside down without rotations

	// StartEngine()
	//Draw()
	// ----------------------------



	function GifIt() {
		console.log("Making gif...")
		var encoder = new GIFEncoder();
		var numberOfFramesToRecord = 64
		encoder.setRepeat(numberOfFramesToRecord)
		encoder.setDelay(16) // 60 fps is 16.666666666....

		//defaultLayer = CreateNewLayer(1280,1080) //recommended resolution by twitter https://developer.twitter.com/en/docs/media/upload-media/uploading-media/media-best-practices.html
		//context = defaultLayer.context


		function GifStep() {
			var now = Date.now()
			var dt = now - lastUpdate
			lastUpdate = now
			Update(dt)
			Draw()
			encoder.addFrame(context)
		}

		encoder.start()
		for (var i = 0; i < numberOfFramesToRecord; i++) {
			GifStep()
		}

		encoder.finish();
		encoder.download("download.gif");
		console.log("gif done.")
	}


	function GreatPlanter() {
		entities.push(this)
		this.Update = function () {
			//        if(Math.random() < .2)
			for (var i = 0; i < 3; i++) {
				plant = new PlantNode(0, 0)
				plant.x = mouse.x + Noise(64)
				plant.y = mouse.y + Noise(64)
			}
			//        console.log("ug plante",mouse.x,mouse.y)
		}
		this.Draw = function () {}
	}

	//    var planter = new GreatPlanter()

	//GifIt()

	// Make a tree
	plant = new PlantNode(0, 0)
	plant.x = 250
	plant.y = 900

	//    StartEngine()

})

functions.push(() => {
	DrawTextureOnceInCenter(
		GetCraftedLayer(50, SetupF.ConstantSpeed, ThickF.quadratic2(20, 0), '#FFFFFF33', 500)
	)
})

functions.push(() => { //coroutine test

	function* testtest() {
		console.log("GO")
		yield "herrlo1"
		yield "herrlo"
		return "end"
	}
	var t = testtest()
	console.log("t is", t)
	console.log(t.next())
	console.log(t.next())
	console.log(t.next())
	console.log(t.next())
})

functions.push(() => {
	//iterator test....
	function thingy() {
		var graphics = GetCraftedLayer(50, SetupF.ConstantSpeed, ThickF.quadratic2(20, 0), '#FFFFFF33', 500, null, 0)

		this.Update = function () {
			var result = graphics.Update()
			if (result.done) {
				this.Update = function () {} // Replace local update with empty function that does nothing.
			}
		}
		this.Draw = function () {
			DrawTextureOnceInCenter(graphics)
		}
	}
	entities.push(new thingy())

	StartEngine()
})

functions.push(() => {
	//PLZ use with gradient background
	function thingy() {
		var texture
		var secondColor = GetRandomColor()
		texture = GetCraftedLayer(100, SetupF.ConstantSpeed, ThickF.quadratic2(20, 0), ColorF.GetColorGradientFunction('#FFFFFF22', "#ff000022"), 500, null, 1)

		this.Update = function () {
			var result = texture.Update()
			if (result.done) {
				this.Update = function () {} // Replace local update with empty function that does nothing.

				texture = GetGlowFilteredImage(GetStarLayer(texture, 50), 10)
				this.Draw = function () {
					DrawTextureOnceInCenter(texture) // No need to apply new filter since the texture is not changing
				}
			}
		}
		// texture = GetScaledLayer(texture, .5)
		//    texture = GetGlowFilteredImage(texture, 8)
		this.Draw = function () {
			DrawTextureOnceInCenter(GetGlowFilteredImage(GetStarLayer(texture, 50), 10))
		}
	}

	entities.push(new thingy())

	StartEngine()
})

functions.push(() => {
	//PLZ use with gradient background
	var texture
	var secondColor = GetRandomColor()
	texture = GetStarLayer(
		GetCraftedLayer(50, SetupF.ConstantSpeed, ThickF.expo(0, 20, 2), '#ffFFff', 500),
		35
	)

	// texture = GetScaledLayer(texture, .5)
	texture = GetGlowFilteredImage(texture, 8)

	DrawTextureOnceInCenter(texture)
})

functions.push(() => {
	//PLZ use with gradient background
	var texture
	var secondColor = GetRandomColor()
	texture = GetStarLayer(
		GetCraftedLayer(50, SetupF.ConstantSpeedWithBend, ThickF.expo2(0, 20, 2), '#FFFFFF', 500),
		8
	)

	// texture = GetScaledLayer(texture, .5)
	texture = GetGlowFilteredImage(texture, 8)

	DrawTextureOnceInCenter(texture)
})

functions.push(() => {
	//PLZ use with gradient background
	var texture
	var secondColor = GetRandomColor()
	texture = GetStarLayer(
		GetCraftedLayer(100, SetupF.ConstantSpeed, ThickF.quadratic2(75, 0), '#ffFFff', 500),
		60
	)

	// texture = GetScaledLayer(texture, .5)
	texture = GetGlowFilteredImage(texture, 8)

	DrawTextureOnceInCenter(texture)
})

functions.push(() => {
	//PLZ use with gradient background
	var texture
	var secondColor = GetRandomColor()

	texture1 = GetStarLayer(
		GetCraftedLayer(50, SetupF.ConstantSpeed, ThickF.Outline(ThickF.quadratic2(75, 4), 12), '#FFFFFFff', 500),
		4
	)
	texture2 = GetStarLayer(
		GetCraftedLayer(50, SetupF.ConstantSpeed, ThickF.quadratic2(75, 4), '#ffFFff', 500),
		4
	)

	texture = GetMergedImage(texture1, texture2)
	texture = GetScaledLayer(texture, .5)
	texture = GetGlowFilteredImage(texture, 8)

	DrawTextureOnceInCenter(texture)
})

functions.push(() => {
	//Restriction based Generic recursive branching on branches (With glow)
	var r = {}

	var texture
	var secondColor = GetRandomColor()

	r.steps = 110
	r.diameter = 700
	r.thickness = ThickF.linear(30, 10)
	r.color = ColorF.GetColorGradientFunction(GetRandomColor(), secondColor)
	r.setup = SetupF.ConstantSpeed
	r.extra = ExtraF.GenericBranch(.01, function () {
			return this.dir + (Math.random() < .5 ? .6 : dirs.left - .6)
		},
		function () {
			var r2 = {}
			r2.diameter = 700
			r2.setup = SetupF.ConstantSpeed
			r2.extra = r.extra
			r2.steps = this.steps - this.i
			r2.thickness = ThickF.linear(this.GetThickness(this.i), 4)
			r2.color = ColorF.GetColorGradientFunction(this.InfiniteFuncValue(this.ColorFunction, this.i, this.steps), secondColor)
			return Craft(r2)
		}
	)

	texture = Craft(r)

	texture = GetGlowFilteredImage(texture, 10)
	DrawTextureOnceInCenter(
		texture
	)
})

functions.push(() => {
	//plant with leafs and 3 branch with flowers + glow
	var leafs = []

	for (var i = 0; i < 10; i++) {
		var randomSeed = Math.random()

		var texture = new CreateNewLayer(600, 600, 300)

		SetupF.ConstantSpeedWithBend.call(texture)
		texture.pivotPoint = new Point(texture.x, texture.y) //This nedes to be automated away!@!!!
		texture = GetScaledLayer(texture, .25)

		Math.seedrandom(randomSeed)
		texture.context.drawImage(GetCraftedLayer(70, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), "#C1FF00", 600, ExtraF.AlwaysTurning), 0, 0)
		Math.seedrandom(randomSeed)
		texture.context.drawImage(GetCraftedLayer(70, SetupF.ConstantSpeedWithBend, ThickF.CatEye, "#9FC03633", 600, ExtraF.AlwaysTurning), 0, 0)

		leafs.push(texture)
	}

	var texture = GetCraftedLayer(50, SetupF.ConstantSpeedWithBend, 3, '#C1FF00', 500, [
		ExtraF.BranchInterval(function () {
			return GetRandomEntryInArray(leafs)
		}, () => {
			return Math.random() > .5 ? 1 : -1
		}, 10),
		ExtraF.TopThing(
			GetFanLayer(
				GetCraftedLayer(30, SetupF.ConstantSpeedWithBend, 3, '#C1FF00', 500, ExtraF.TopThing(
					prefabs.tinyLeafs()
				)),
				3, //wings
				.5 //angle
			)
		),
	])

	texture = GetGlowFilteredImage(texture, 20)
	DrawTextureOnceInCenter(texture)
})

functions.push(() => {
	//First split flower!
	var texture = GetCraftedLayer(50, SetupF.ConstantSpeedWithBend, 3, '#C1FF00', 500, ExtraF.TopThing(
		GetFanLayer(
			GetCraftedLayer(50, SetupF.ConstantSpeedWithBend, 3, '#C1FF00', 500, ExtraF.TopThing(
				prefabs.tinyLeafs()
			)),
			3, //wings
			.5 //angle
		)
	))
	DrawTextureOnceInCenter(texture)
})

functions.push(() => {
	//First use of top thing
	var texture = GetCraftedLayer(50, SetupF.ConstantSpeedWithBend, 3, '#C1FF00', 500, ExtraF.TopThing(prefabs.tinyLeafs()))
	DrawTextureOnceInCenter(texture)
})

functions.push(() => {
	var leafs = []

	for (var i = 0; i < 10; i++) {
		var randomSeed = Math.random()

		var texture = new CreateNewLayer(600, 600, 300)

		SetupF.ConstantSpeedWithBend.call(texture)
		texture.pivotPoint = new Point(texture.x, texture.y) //This nedes to be automated away!@!!!
		texture = GetScaledLayer(texture, .25)

		Math.seedrandom(randomSeed)
		texture.context.drawImage(GetCraftedLayer(70, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), "#C1FF00", 600, ExtraF.AlwaysTurning), 0, 0)
		Math.seedrandom(randomSeed)
		texture.context.drawImage(GetCraftedLayer(70, SetupF.ConstantSpeedWithBend, ThickF.CatEye, "#9FC03633", 600, ExtraF.AlwaysTurning), 0, 0)

		leafs.push(texture)
	}

	DrawTextureOnceInCenter(GetCraftedLayer(50, SetupF.ConstantSpeedWithBend, 3, '#C1FF00', 500, ExtraF.GenericBranch(.1, () => {
		return Math.random() > .5 ? 1 : -1
	}, function () {
		return GetRandomEntryInArray(leafs)
	})))
})


functions.push(() => {
	var secondColor = GetRandomColor()
	textures = GetCraftedLayer(110, SetupF.ConstantSpeed, ThickF.linear(30, 10), ColorF.GetColorGradientFunction(GetRandomColor(), secondColor), 700,
		ExtraF.GenericBranch(.20, function () {
				return this.dir + (Math.random() < .5 ? .6 : dirs.left - .6)
			},
			function () {
				return LayerRecipeCrafter(
					this.steps - this.i,
					SetupF.Example,
					ThickF.linear(this.GetThickness(this.i) - 4, 4),
					ColorF.GetColorGradientFunction(this.InfiniteFuncValue(this.ColorFunction, this.i, this.steps), secondColor),
					500
				)
			}
		)
	)
	textures = GetGlowFilteredImage(textures, 20)
	DrawTextureOnceInCenter(
		textures
	)
})

functions.push(() => {
	//console.log(Math.random() < .5 ? .1 : -.1)
	DrawTextureOnceInCenter(
		GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, 6, '#333333', 500,
			[
				ExtraF.AlwaysTurning,
				ExtraF.GenericBranch(
					.15,
					function () {
						return this.dir + (Math.random() < .5 ? dirs.right : dirs.left)
					},
					GetCraftedLayer(
						5 + Math.round(Math.random() * 20),
						SetupF.ConstantSpeedWithBend,
						6,
						'#333333',
						500,
						ExtraF.GenericBranch(
							.15,
							1,
							GetCraftedLayer(
								10,
								SetupF.ConstantSpeedWithBend,
								ThickF.NarrowCatEyex,
								'#ffff33',
								500
							)
						)
					)
				),
			]
		)
	)
})

functions.push(() => {
	textures = GetCraftedLayer(50, SetupF.ConstantSpeedWithBend, ThickF.linear(30, 10), ColorF.GetColorGradientFunction("#bb4400", "#ffffff"), 700,
		[
			ExtraF.AlwaysTurning,
			ExtraF.AlwaysTurning,
			ExtraF.AlwaysTurning,
			ExtraF.GenericBranch(.07, function () {
					return this.dir + (Math.random() < .5 ? .6 : dirs.left - .6)
				},
				LayerRecipeCrafter(function () {
						return 20 + Math.round(Math.random() * 30)
					}, SetupF.Example, ThickF.linear(13, 5), ColorF.GetColorGradientFunction("#bb4400", "#ffffff"), 500,
					ExtraF.GenericBranch(.07, function () {
							return this.dir + (Math.random() < .5 ? dirs.right : dirs.left)
						},
						LayerRecipeCrafter(function () {
								return 2 + Math.round(Math.random() * 10)
							}, SetupF.Example, ThickF.linear(5, 3), ColorF.GetColorGradientFunction("#bb4400", "#ffffff"), 500,
							ExtraF.GenericBranch(.15, () => {
								return Math.random() * Math.PI
							}, LayerRecipeCrafter(1, SetupF.ConstantSpeedWithBend, 10, ColorF.GetColorsFromOneRandomlySelectedPalette(), 500, ExtraF.AlwaysTurning))
						)
					),
				)
			),
		]
	)
	textures = GetGlowFilteredImage(textures, 20)
	DrawTextureOnceInCenter(
		textures
	)
})

functions.push(() => {
	textures = GetCraftedLayer(50, SetupF.ConstantSpeedWithBend, 20, "#ffff33", 700,
		[
			ExtraF.GenericBranch(.7, function () {
					return this.dir + (Math.random() < .5 ? .6 : dirs.left - .6)
				},
				LayerRecipeCrafter(function () {
					return 1 + Math.round(Math.random() * 15)
				}, SetupF.Example, 15, "#ffff33", 500)
			),
		]
	)
	textures = GetGlowFilteredImage(textures, 20)
	DrawTextureOnceInCenter(
		textures
	)
})

functions.push(() => {
	var textures = CreateNewLayer(700, 700, 350)
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	for (var i = 0; i < 5; i++) {

		textures.context.drawImage(GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, ThickF.GetOscilatingFunction(10, 30, 4.8), ColorF.GetColorGradientFunction("#000000", "#FFFF00"), 700, ExtraF.AlwaysTurning), 0, 0)
	}
	Math.seedrandom(randomSeed)
	for (var i = 0; i < 5; i++) {
		textures.context.drawImage(GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, ThickF.GetOscilatingFunction(10, 20, 4.8), ColorF.GetColorGradientFunction("#000000", "#999999") + "33", 700, ExtraF.AlwaysTurning), 0, 0)

	}

	textures = GetGlowFilteredImage(textures, 20)

	DrawTextureOnceInCenter(textures)
})

functions.push(() => {
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	textures = GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, ThickF.GetOscilatingFunction(10, 30, 4.8), ColorF.GetColorGradientFunction("#000000", "#FFFF00"), 700, ExtraF.AlwaysTurning),
		Math.seedrandom(randomSeed)
	textures.context.drawImage(GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, ThickF.GetOscilatingFunction(10, 20, 4.8), ColorF.GetColorGradientFunction("#000000", "#999999") + "33", 700, ExtraF.AlwaysTurning), 0, 0)
	// textures = GetGlowFilteredImage(textures, 20)


	DrawTextureOnceInCenter(textures)
})

functions.push(() => {
	//Cool feathers
	DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, 5, '#00000000', 500, ExtraF.FeatherBranch))
})

functions.push(() => {
	//Nice colored tall
	DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.ConstantSpeed, 5, '#00000000', 500, ExtraF.TallBranch))
})
//May 2018
functions.push(() => {
	// color glitches
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			LayerRecipeCrafter(100, SetupF.ConstantSpeedWithBend, ThickF.NarrowCatEye, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor() + "33"), 500, ExtraF.AlwaysTurning),
			10,
			0,
			2
		)
	)
})

functions.push(() => {
	var textures = GetRandomlyDistributedFanLayer(
		LayerRecipeCrafter(100, SetupF.ConstantSpeedWithBend, ThickF.GetOscilatingFunction(4, 6, 2), GetRandomColor(), 500, ExtraF.AlwaysTurning),
		10,
		0,
		2
	)

	DrawFilteredVersionOnSelf(textures, "blur", "10px")
	DrawTextureOnceInCenter(textures)
})


functions.push(() => {
	textures = GetGlowFilteredImage(textures, 20)
	var a = new thingDrawer(500, 500, textures)
	a.Update = function () {
		this.x = mouse.x
		this.y = mouse.y
		//	this.textureFunction = GetFilteredImage(this.textureFunction, "hue-rotate", "5deg")
		//this.textureFunction = GetFilteredImage(this.textureFunction, "blur", "2px")
	}
	StartEngine()
})

functions.push(() => {
	DrawTextureOnceInCenter(
		prefabs.tinyLeafs()
	)
})

functions.push(() => {
	//Favorite plant stick
	DrawTextureOnceInCenter(GetCraftedLayer(40, SetupF.ConstantSpeedWithBend, 5, '#006600', 500, ExtraF.Leafing))
})

functions.push(() => {
	//Tree splitting
	var r = { //testing out the restrictions
		steps: 100,
		color: "#FFFFFF",
		diameter: 500,
		thickness: 10,
		//alpha: .5,
	}
	r.thickness = ThickF.BySpeed
	r.color = ColorF.RandomGradient
	r.setup = SetupF.Example
	r.chanceFunction = ExtraF.branchSticks
	DrawTextureOnceInCenter(Craft(r))
})

functions.push(() => { // this makes like the dinko leaf if you do it repeatedly!
	// functions are uggly because you need to keep track of the order of parameters.
	// the conventions say you should write all parameters onto an object that defines restrictions
	var restr = {
		steps: 100,
		color: "#FFFFFF",
		diameter: 500,
		thickness: 10,
		//alpha: .5,
	}
	restr.setup = SetupF.Example
	console.log(dirs.up)
	DrawTextureOnceInCenter(Craft(restr))
})

functions.push(() => { //Magical tree or something
	DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.ConstantSpeed, 5, '#ffffff', 500, ExtraF.branchSticks))
})

functions.push(() => {
	//I'm not sure what I intended to do here. Both sprite and texture are undefined
	// DrawSprite(this.layer1, sprite, 0, 0, 0, 0, 0, 250)
	// DrawSprite(defaultLayer, texture, 0, x, y, texture.width / 2, texture.height / 2, texture.width / 2)
})

functions.push(() => {
	//grass made with transparent bits, it sometimes bends back over itself. Look like runescape?
	DrawTextureOnceInCenter(GetCraftedLayer(Math.round(GetRandomRange(30, 200)), SetupF.Example, ThickF.BySpeed, "#E5FF6011", 500))
})

functions.push(() => {
	//Super cute blob consisting of two layers, one black background, one top green with transparent, seeded randomness syncing the layers.
	Math.seedrandom("Emil")
	DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.Example, ThickF.CatEye, "#151515", 500))
	Math.seedrandom("Emil")
	DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.Example, ThickF.CatEye, "#E5FF6011", 500))
})

functions.push(() => {
	//three layers synced by a randoly selected seed, making the layers random but consistent with eachother
	// This one draws a creepy worm with two outlines (green and black)
	// I think the color scheme was inspired by the dude on twitter that drew monsters in black and green.
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(80, SetupF.ConstantSpeedWithBend, 30, "#030303", 512, ExtraF.StringyGrass))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(80, SetupF.ConstantSpeedWithBend, 25, "#C1FF00", 512, ExtraF.StringyGrass))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(80, SetupF.ConstantSpeedWithBend, 10, "#9FC036", 512, ExtraF.StringyGrass))
})


functions.push(() => {
	//pretty much the same as above but with other shape
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(90, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.Oscilating6Times, 32), "#ffFF66", 600, ExtraF.StringyGrass))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(90, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.Oscilating6Times, 30), "#C1FF00", 600, ExtraF.StringyGrass))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(90, SetupF.ConstantSpeedWithBend, ThickF.Oscilating6Times, "#9FC036", 600, ExtraF.StringyGrass))
})

functions.push(() => {
	// two layer oscilate 6 times with middle layer having transparency
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.Oscilating6Times, 8), "#C1FF00", 600, ExtraF.AlwaysTurning))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Oscilating6Times, "#9FC03633", 600, ExtraF.AlwaysTurning))
})

functions.push(() => {
	// my classical first time leaf bush or whatever, doesn't use fan layer because I wanted each leaf to be two layered and not separate the layers by each fan.
	for (var i = 0; i < 4; i++) {
		var randomSeed = Math.random()
		Math.seedrandom(randomSeed)
		DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), "#C1FF00", 600, ExtraF.AlwaysTurning))
		Math.seedrandom(randomSeed)
		DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.CatEye, "#9FC03633", 600, ExtraF.AlwaysTurning))
	}
})

functions.push(() => {
	//The coolest flower thing Ever!!!! 
	//I can use getCrafter now instead of using anonymous functions
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(
		GetStarLayer(() => {
			return GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), "#C1FF00", 600, ExtraF.AlwaysTurning)
		}, 10)
	)
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(
		GetStarLayer(() => {
			return GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.CatEye, "#9FC03633", 600, ExtraF.AlwaysTurning)
		}, 10)
	)
})

functions.push(() => {
	//Gecko lizard tail
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 16), "#9FC03633", 600, ExtraF.AlwaysTurning))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), "#C1FF00", 600, ExtraF.AlwaysTurning))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.CatEye, "#9FC03633", 600, ExtraF.AlwaysTurning))
})

functions.push(() => {
	//dark purple almost lava lamp tail
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), ColorF.Petal2, 600, ExtraF.AlwaysTurning))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), "#2E023C11", 600, ExtraF.AlwaysTurning))
})

functions.push(() => {
	//dark purple almost lava lamp tail but with increasing lightness towards top
	var randomSeed = Math.random()
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), ColorF.GetColorGradientFunction(palettes.pinkBlack[0], palettes.pinkBlack[2]), 600, ExtraF.AlwaysTurning))
	Math.seedrandom(randomSeed)
	DrawTextureOnceInCenter(GetCraftedLayer(110, SetupF.ConstantSpeedWithBend, ThickF.Outline(ThickF.CatEye, 8), "#2E023C11", 600, ExtraF.AlwaysTurning))
})

functions.push(() => {
	//thin blades with gradient, random base color towards white
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			LayerRecipeCrafter(100, SetupF.ConstantSpeedWithBend, ThickF.NarrowCatEye, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor()), 500, ExtraF.AlwaysTurning),
			10,
			0,
			2
		)
	)
})

functions.push(() => {
	//Cool combo, gradient from total white to randomly selected color. The random is for each step so the final thing has many colors.
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			LayerRecipeCrafter(100, SetupF.ConstantSpeedWithBend, ThickF.NarrowCatEye, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor), 500, ExtraF.AlwaysTurning),
			10,
			0,
			2
		)
	)
})

functions.push(() => {
	//Same as above but with thick cat eyes
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			LayerRecipeCrafter(100, SetupF.ConstantSpeedWithBend, ThickF.CatEye, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor), 500, ExtraF.AlwaysTurning),
			10,
			0,
			2
		)
	)
})


functions.push(() => {
	//Same as above but with thin constant width spagethi arms
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			LayerRecipeCrafter(100, SetupF.ConstantSpeedWithBend, 5, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor), 500, ExtraF.AlwaysTurning),
			10,
			0,
			2
		)
	)
})

functions.push(() => {
	//same as above but super thin
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			LayerRecipeCrafter(100, SetupF.ConstantSpeedWithBend, 1, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor), 500, ExtraF.AlwaysTurning),
			10,
			0,
			2
		)
	)
})

functions.push(() => {
	console.log("eysss")
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			LayerRecipeCrafter(100, SetupF.ConstantSpeedWithBend, ThickF.CatEye, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor()), 500, ExtraF.AlwaysTurning),
			40,
			0,
			2
		)
	)
})

functions.push(() => {
	DrawTextureOnceInCenter(GetRandomlyDistributedFanLayer(
		LayerRecipeCrafter(100, SetupF.ConstantSpeed, ThickF.CatEye, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor()), 500, ExtraF.AlwaysTurning),
		40,
		0,
		2
	))
})

functions.push(() => {
	// This is pretty much a palette demo in a circle
	DrawTextureOnceInCenter(GetFanLayer(
		paletteItOut(),
		20,
		.3
	))

	function paletteItOut() { // to put random palette in each layer RecipeCrafter
		return function () {
			return LayerRecipeCrafter(100, SetupF.ConstantSpeed, 20, ColorF.GetColorsFromOneRandomlySelectedPalette(), 500)
		}
	}
})

functions.push(() => {
	//make a thing that itterates through all palettes and puts them in a pattern on screen, to get a clear overview of all of them.
	console.log(palettesArray)
	Pattern(Math.floor(Math.sqrt(palettesArray.length)), 110, 250, itrAllColors, 60)

	//DrawTextureOnceInCenter(itrAllColors())
	function itrAllColors() {
		this.i = DefaultParameter(this.i, -1)
		this.i++
		let myPalette = palettes[palettesArray[this.i]]
		console.log("mi palet", this.i)
		return GetCraftedLayer(20, SetupF.ConstantSpeed, ThickF.CatEye, ColorF.GetRandomColorsFromPalette(myPalette), 500)
	}
})

functions.push(() => {
	// flower star
	for (var i = 7; i > 0; i--) {
		DrawTextureOnceInCenter(GetFanLayer(GetCraftedLayer(10 * i, SetupF.ConstantSpeed, 20, ColorF.GetColorGradientFunction("#000000", "#FFFFFF"), 500), i, .4))
	}
})

functions.push(() => {
	// flower star
	for (var i = 10; i > 0; i--) {
		DrawTextureOnceInCenter(GetFanLayer(GetCraftedLayer(10 * i, SetupF.ConstantSpeed, 20 - i, ColorF.GetRandomFromPalette(palettes.aquamarine1), 500), i, .3))
	}
})

functions.push(() => {
	// flower star
	console.log("foun you")
	for (var i = 10; i > 1; i--) {
		DrawTextureOnceInCenter(GetStarLayer(GetCraftedLayer(10 * i, SetupF.ConstantSpeed, 30 - i * 2, ColorF.GetRandomFromPalette(palettes[palettesArray[86]]) + "33", 500), i * i))
	}
})

functions.push(() => {
	// flower star
	for (var i = 10; i > 1; i--) {
		DrawTextureOnceInCenter(GetStarLayer(GetCraftedLayer(10 * i, SetupF.ConstantSpeed, 30 - i * 2, ColorF.GetRandomFromPalette(palettes[palettesArray[51]]), 500), 10, i * .1))
	}
})


functions.push(() => {
	// flower star
	console.log("tup")
	for (var i = 10; i > 1; i--) {
		DrawTextureOnceInCenter(GetStarLayer(GetCraftedLayer(10 * i, SetupF.ConstantSpeed, 30 - i, ColorF.GetRandomFromPalette(palettes[palettesArray[51]]), 500), 10, i * .1))
	}
})

functions.push(() => {
	// flower star
	console.log('rwdp')
	for (var i = 10; i > 0; i--) {
		console.log(i)
		DrawTextureOnceInCenter(
			GetStarLayer(
				GetCraftedLayer(10 * i, SetupF.ConstantSpeed, 30, ColorF.GetRandomFromPalette(palettes[palettesArray[33]]), 500),
				i * 4,
				i * dirs.up
			)
		)
	}
})

functions.push(() => {
	// flower star

	for (var i = 10; i > 0; i--) {
		DrawTextureOnceInCenter(
			GetStarLayer(
				GetCraftedLayer(i * i, SetupF.ConstantSpeed, 30, ColorF.GetRandomFromPalette(palettes[palettesArray[31]]), 500),
				i * 3,
				i * .2))
	}
})

functions.push(() => {
	// flower star

	for (var i = 10; i > 0; i--) {
		DrawTextureOnceInCenter(
			GetStarLayer(
				GetCraftedLayer(6 * i, SetupF.ConstantSpeed, ThickF.NarrowCatEye, ColorF.GetRandomFromPalette(palettes[palettesArray[30]]), 500),
				4,
				i * dirs.up * .125))
	}
})

functions.push(() => {
	// flower star

	for (var i = 10; i > 0; i--) {
		DrawTextureOnceInCenter(
			GetStarLayer(
				GetCraftedLayer(6 * i, SetupF.ConstantSpeedWithBend, ThickF.NarrowCatEye, ColorF.GetRandomFromPalette(palettes[palettesArray[73]]), 500, ExtraF.AlwaysTurning),
				4,
				i * dirs.up * .125))
	}
})

functions.push(() => {
	for (var i = 10; i > 0; i--) {
		DrawTextureOnceInCenter(
			GetStarLayer(
				GetCraftedLayer(10 * i, SetupF.ConstantSpeed, ThickF.NarrowCatEye, ColorF.GetRandomFromPalette(palettes[palettesArray[8]]), 500),
				i * 4,
				i * dirs.up * .125))
	}
})

functions.push(() => {
	for (var i = 10; i > 0; i--) {
		DrawTextureOnceInCenter(
			GetStarLayer(
				LayerRecipeCrafter(10 * i, SetupF.ConstantSpeed, ThickF.NarrowCatEye, ColorF.GetRandomFromPalette(palettes[palettesArray[9]]), 500, ExtraF.AlwaysTurning),
				i * 4,
				i * dirs.up * .125))
	}
})

functions.push(() => {
	var randomPalette = GetRandomEntryInArray(palettesArray)
	for (var i = 10; i > 0; i--) {
		DrawTextureOnceInCenter(
			GetStarLayer(
				LayerRecipeCrafter(10 * i, SetupF.ConstantSpeed, ThickF.NarrowCatEye, ColorF.GetRandomFromPalette(palettes[randomPalette]), 500, ExtraF.AlwaysTurning),
				i * 4,
				i * dirs.up * .125))
	}
})

functions.push(() => {
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			LayerRecipeCrafter(80, SetupF.ConstantSpeedWithBend, 20, ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor()), 500, ExtraF.AlwaysTurning),
			40,
			0,
			2
		)
	)
})

functions.push(() => {

	//Eraser test 1
	DrawTextureOnceInCenter(
		SubtractTexture(
			GetCraftedLayer(100, SetupF.SlowingButStraight, ThickF.Outline(ThickF.CatEye, 10), ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor()), 500),
			GetCraftedLayer(100, SetupF.SlowingButStraight, ThickF.Outline(ThickF.CatEye, 9), "#FFFFFF10", 500),
			250, 250
		)
	)
})


functions.push(() => {
	//Eraser test 2 - fans
	DrawTextureOnceInCenter(
		GetRandomlyDistributedFanLayer(
			SubtractTexture(
				GetCraftedLayer(100, SetupF.SlowingButStraight, ThickF.Outline(ThickF.CatEye, 10), ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor()), 500),
				GetCraftedLayer(100, SetupF.SlowingButStraight, ThickF.Outline(ThickF.CatEye, 9), "#FFFFFF10", 500),
				250, 250
			),
			50,
			0,
			20
		)
	)
})

functions.push(() => {
	DrawTextureOnceInCenter(
		GetFanLayer(
			SubtractTexture(
				GetCraftedLayer(100, SetupF.SlowingButStraight, ThickF.Outline(ThickF.CatEye, 10), ColorF.GetColorGradientFunction("#FFFFFF", GetRandomColor()), 500),
				GetCraftedLayer(100, SetupF.SlowingButStraight, ThickF.Outline(ThickF.CatEye, 9), "#FFFFFF10", 500),
				250, 250
			),
			50,
			.1
		)
	)
})

functions.push(() => {
	//Eraser test 3? - fans
	DrawTextureOnceInCenter(
		SubtractTexture(
			GetCraftedLayer(20, SetupF.ConstantSpeed, 40, ColorF.GetColorGradientFunction("#ffffff", "#FFFFFF"), 500),
			GetCraftedLayer(20, SetupF.ConstantSpeed, 30, "#FFFFFF", 500),
			250, 250
		)
	)
})

// April 2018

functions.push(() => {
	var fluff = new Fluff(100, "cloud")
	var SnakeSkin = new Fluff(100, "green")
	var bush = new Bush(100, 100, 1900, 800) //rename to place circles in a square
	// var w = WeirdThing()
	StartEngine()
})

functions.push(() => {
	DrawTextureOnceInCenter(GetStringLayer())
})

functions.push(() => {
	StringMatta()
})

functions.push(() => {
	StringThing()
})

functions.push(() => {
	MakeAGrassMattaEveryFrame()
})

functions.push(() => {
	DrawTextureOnceInCenter(GetAvocadoWithDoubleOutlineLayer())
})

functions.push(() => {
	DrawTextureOnceInCenter(GetPlantLayer())
})


functions.push(() => {
	Experiment()
})

functions.push(() => {})

functions.push(() => {})

functions.push(() => {})

functions.push(() => {})

functions.push(() => {})

functions.push(() => {})

/*
// Fucken old shit that doesn't work because function names don't exist any more
//Pattern(10, 300, 250, GetCircleLeafLayer)

//DrawTextureOnceInCenter(GetStarLayer(9, GetCircleLeafLayer()))
//DrawTextureOnceInCenter(GetFanLayer(3, .2, GetCircleLeafLayer()))
var b = Bush(canvas.centerPosition.x, canvas.centerPosition.y, 200, 100)
StartEngine()

//FRACTALS
DrawTextureOnceInCenter(
	GetStarLayer(4,
		GetStarLayer(4,
			GetStarLayer(5,
				GetStarLayer(5,
					GetWeirdRainBowSausageLayer()
				)
			)
		)
	)
)

DrawTextureOnceInCenter(
	GetStarLayer(4,
		GetStarLayer(3,
			GetStarLayer(2,
				GetStarLayer(1,
					GetWeirdRainBowSausageLayer()
				)
			)
		)
	)
)

DrawTextureOnceInCenter(
	GetStarLayer(3,
		GetStarLayer(3,
			GetStarLayer(2,
				GetStarLayer(1,
					GetWeirdRainBowSausageLayer()
				)
			)
		)
	)
)
DrawTextureOnceInCenter(
	GetStarLayer(5,
		GetStarLayer(8,
			GetStarLayer(5,
				GetStarLayer(12,
					GetStarLayer(7,
						GetWeirdGameBoySausageLayer()
					)
				)
			)
		)
	)
)

DrawTextureOnceInCenter(
	GetStarLayer(Math.round(GetRandomRange(2,16)),
		GetStarLayer(Math.round(GetRandomRange(2,16)),
			GetStarLayer(Math.round(GetRandomRange(2,16)),
				GetStarLayer(Math.round(GetRandomRange(2,16)),
					GetStarLayer(Math.round(GetRandomRange(2,16)),
						GetWeirdGameBoySausageLayer()
					)
				)
			)
		)
	)
)

DrawTextureOnceInCenter(
	GetStarLayer(Math.round(GetRandomRange(2, 16)),
		GetStarLayer(Math.round(GetRandomRange(2, 16)),
			GetStarLayer(Math.round(GetRandomRange(2, 16)),
				GetStarLayer(Math.round(GetRandomRange(2, 16)),
					GetStarLayer(Math.round(GetRandomRange(2, 16)),
						GetWeirdRainBowSausageLayer()
					)
				)
			)
		)
	)
)

// End of Fractals


//DrawTextureOnceInCenter(GetPlantLayer())
//DrawTextureOnceInCenter(GetCircleLeafLayer())

//DrawTextureInSquare(GetCircleLeafLayer, 100, new MyRect(100, 100, 500, 200, "#000000", 0))
Pattern(30, 250, 200, GetCircleLeafLayer())
Pattern(30, 250, 200, GetRandomLayer)
//Pattern(30, 200, 250, allStuff[8])
//DrawTextureOnceInCenter(GetPetal2Layer())
//DrawTextureOnceInCenter(allStuff[8]())

function testTest() {
	console.log("hey")
}
testTest(10) // Yes you can pass paremeters in a function that doesn't take em.


//DrawTextureOnceInCenter(GetCraftedLayer(100, BehaviourSetupFunctionExample, ThicknessBySpeed, ColorFunctionExample, 500)) //Cute little mishap
//DrawTextureOnceInCenter(GetCraftedLayer(100, BehaviourSetupFunctionExample, ThicknessOscilating6Times, ColorFunctionExample, 500)) //Cute little mishap2

//DrawTextureOnceInCenter(GetCraftedLayer(100, BehaviourSetupFunctionExample, ThicknessBySpeed, ColorFunctionExample, 500)) //Cute little mishap
//DrawTextureOnceInCenter(GetCraftedLayer(40, BehaviourSetupFunctionExample, ThicknessOscilating6Times, ColorFunctionExample, 500)) //Cute little mishap2
//^ Draw these two every frame at 9 fps for cute/weird thing



//DrawTextureOnceInCenter(googleMapPin)
//DrawTextureOnceInCenter(GetStarLayer(5, straightBalloon))
//DrawTextureOnceInCenter(GetStarLayer(5, catEye))
//DrawTextureOnceInCenter(GetFanLayer(8, .3, catEye))

// Start
DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.ConstantSpeed, Thickf.Outline(Thickf.CatEye, 25), "white", 500))
DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.ConstantSpeed, Thickf.Outline(Thickf.CatEye, 20), GetRandomColor, 500))
//DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.ConstantSpeed, Thickf.Outline(Thickf.CatEye, 20), "#00ff0011", 500))
DrawTextureOnceInCenter(GetCraftedLayer(100, SetupF.ConstantSpeed, Thickf.Outline(Thickf.CatEye, 13), "#ffffff11", 500))
// End

*/ // Mars 2018
/*
function a() {
	console.log("a", this)
}
function b() {
	this.a = a
	console.log("b", this)
	this.a()
}
new b()
*/ // Example of How "this" works depending on context. new keyword makes a local context. using this.x = func gives func context to the caller.

/* old weird and unused */
function Experiment() { //Rename into art experiment with appropriate name, put it under projects... or create a Experiments parent like Experiments.DrawingSpiral()
	FPS_LIMIT = 60

	screenClearingColor = "#00885502" //"pink"

	isScreenClearing = true
	isWraparound = true
	isUnitTest = false

	var t = new thingDrawer(400, 400, GetAvocadoStarLayer)
	AddBehaviour(t, Go)
	AddBehaviour(t, Turn)
	t.speed = 5
	t.speedIcr = .1
	t.angle = .1
	StartEngine()
} // Rename

function StringThing() {
	for (var i = 0; i < 10; i++) {
		DrawTextureOnce(canvas.centerPosition.x, canvas.centerPosition.y, GetShortStringLayer())
	}
} // Rename

function Grassmatta() { //TODO: redo into a texture or into a Draw this on texture instead generic draw on canvas. 
	for (var i = 0; i < 400; i++) {
		DrawTextureOnce(canvas.centerPosition.x + Noise(700), canvas.centerPosition.y + Noise(30), GetTheWitnessGrassLayer())
	}
} // Clarify

function StringMatta() {
	for (var i = 0; i < 1000; i++) {
		DrawTextureOnce(canvas.centerPosition.x + Noise(700), canvas.centerPosition.y + Noise(30), GetStringLayer())
	}
} // Merge

function MakeAGrassMattaEveryFrame() {
	var t = new thingDrawer(0, 0, GetAvocadoLayer)
	t.Update = function () {}
	t.Draw = Grassmatta
	StartEngine()
}

function WeirdThing() {
	entities.push(this)
	this.Update = function () {}
	this.Draw = function () {

		var layer1 = GetWeirdRainbowSausageLayer()

		for (var i = 0; i < 10; i++) {
			DrawSprite(layer1, GetRandomRange(0, 2 * Math.PI), GetRandomRange(100, 800), GetRandomRange(100, 800), 50, 50, 50)
		}
	}
	if (isUnitTest) console.log("Weird thing created!")
} // Rename

function WeirdThing2() {
	entities.push(this)
	this.Update = function () {}
	this.Draw = function () {

		var layer1 = GetWeirdSausageLayer()

		for (var i = 0; i < 10; i++) {
			DrawSprite(layer1, GetRandomRange(0, 2 * Math.PI), 100 + i * 32, 100, 50, 50, 50)
		}
	}
	if (isUnitTest) console.log("Weird thing created!")
} // Rename

function ChromaticLayer() {
	//I can do this effect better by drawing a texture three times and coloring 2 of the layers with some filter.

	var chromaticDistance = 5
	var color1 = "#FF00FF11"
	var color2 = "#00FF0011"

	//Perform magic drawing stuff two times behind the original image, with offset.

	for (var i = 0; i < 25; i++) {
		//DrawCircle(layer1, chromaticDistance + 50, chromaticDistance + 50 + i, 10 - 5 * (i / 25), color1, 1)
	}

	for (var i = 0; i < 25; i++) {
		//DrawCircle(layer1, -chromaticDistancek + 50, -chromaticDistance + 50 + i, 10 - 5 * (i / 25), color2, 1)
	}

	return layer1
} // No longer works.








/*
//project 2.75-D (Bootleg 3d positions)
//X Get a grass texture
//X make a bunch of game objects that have 3d positions
//Define dimensionalTransformation(y);
// Make game objects.draw(){sprite at x=x y= DimensionalTransformation(y)}
// make a function that increases a var z over time
// add entities to list
// start engine

var maFeathers = [];

for (var i = 0; i < 100; i++) {
	maFeathers.push(GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, ThickF.NarrowCatEye, ColorF.GetColorsFromOneRandomlySelectedPalette(), 500));
}

function Thingy(x, y, z) {
	this.x = x;
	this.y = y;
	this.z = z;

	SpriteObject.call(this, GetRandomEntryInArray(maFeathers));

	this.Update = function () {
		this.z += .01;
	}
}

function SpriteObject(texture) {
	entities.push(this);
	this.texture = texture;
	this.Draw = function () {
		if (this.z < 25) // Don't draw if it behind the hump
			DrawSprite(defaultLayer, this.texture, 0, this.x - this.texture.pivotPoint.x / this.z, TransformFunc(this.z), this.texture.pivotPoint.x / this.z, this.texture.pivotPoint.y / this.z, this.texture.height / (this.z));
	}
}

function TransformFunc(x) {
	var sum = 3 * x * x / 4 - 30 * x + 800;
	//console.log(x, sum);
	return sum;
}

for (var i = 0; i < 500; i++) {
	var t = new Thingy(GetRandomRange(400, 1000), 500, 50 - 50 * (i / 500));
}



StartEngine();

*/
