//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* Projects.js */

/* File content description:
	
	high level stuff
	Tests
	Simulations

	basically high level assembling of games/worlds
	in each project things like these are allowed to be defined
		Behaviours of the world
		Game objects
*/
/* Todo:
	Add File content description
	Move all entities into GameObjects.js
*/

var Projects = {
	Cirlcles: {
		Slinkies: {
			FirstSlinkyExperiments: function () { // TODO: Go through these
				//entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, spinAroundRandomEntity(.02)))

				//entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, spinAroundMouseBehaviour))
				//entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, spinAroundRandomEntity(.02)))
				//entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, followRandomEntity()))

				/*
				for (var i =0i < 100i++)
					entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, function(){ SpinAroundPoint(this, pointer, .02) }))
				*/
				//entities.push(new Slinky(pointer.x, pointer.y, "#00fff0", 10, 20, .7, entities[0].circles[entities[0].circles.length-1]))
				//entities.push(new Slinky(pointer.x, pointer.y, "#f00ff0", 20, 5, .9, entities[1].circles[entities[1].circles.length-1]))
				//entities.push(new Slinky(pointer.x, pointer.y, "#00fff0", 10, 20, .7, entities[2].circles[entities[2].circles.length-1]))
				/*
				for (var i=0i<3i++)
				{
					entities.push(new Slinky(pointer.x, pointer.y, GetRandomColor(), GetRandomRange(5,40), GetRandomRange(5,20), GetRandomRange(.01,.99), entities[entities.length-1].circles[entities[entities.length-1].circles.length-1]))
				}
				*/
			},

			Snek: function (target) { // TODO: Add all these sneks to GameObjects.js
				var slinkies = []
				var slink = new Slinky(100, 100, "#aa0044", 5, 15, .5)
				slink.target = mouse
				// AddBehaviour(slink, MoveTowardsTarget)
				 AddBehaviour(slink, Go)
				 AddBehaviour(slink, Turn)

				slink.speed = 5
				slink.angle = .01


				slinkies.push(slink)
				for (var i = 0; i < 16; i++) { //temporary number. 16 loops is the original
					(function () {
						var slink
						if (i % 2 == 0) {
							slink = new Slinky(0, 0, "#ffffff", 20 - i, 10, .5)
						} else {
							slink = new Slinky(0, 0, "#FF0000", 20 - i, 15, .5)
						}
						AddBehaviour(slink, MoveTowardsTarget)
						slink.target = slinkies[i].back
						//slink.elasticity = .9
						slinkies.push(slink)
					})()
					//throw Error(slinkies)
					//Snek tail
					/* temoprary commented out while testing
					slinkies.push(new Slinky(0, 0, "#FF0000", 20, 15, .5, function () {
						MoveTowards(this, slinkies[slinkies.length - 1].back, .9)
					}, "bigger"))
					*/
				}
				StartEngine()
				return slinkies


			},

			CandyBar: function (target) {
				if (!target) {
					target = new Point(0, 0)
					log("CandyBar has no defined target")
				}
				var candyParts = []
				candyParts.push(new Slinky(0, 0, GetRandomColor(), 5, 15, .5, function () {
					MoveTowards(this, target, .9)
				}))
				for (var i = 0; i < 40; i++)(function () {
					var target = candyParts[i].back
					var u = i
					if (i % 2 == 0)
						candyParts.push(new Slinky(0, 0, "#ffffff", 20, 20, .5, function () {
							MoveTowards(this, target, .7)
						}))
					else
						candyParts.push(new Slinky(0, 0, GetRandomColor(), 20, 20, .5, function () {
							MoveTowards(this, target, .7)
						}))
				})()
			},

			Shrooms: function (target) {
				var thing = []
				thing.push(new Slinky(0, 0, GetRandomColor(), 5, 3, .5, function () {
					MoveTowards(this, target, .9)
				}))
				//thing.push(new Slinky(200, 0, GetRandomColor(), 4, 60, .5, function(){MoveTowards(this, pointer, .9)}))
				for (var i = 0; i < 10; i++)
					(function () {
						var shit = i
						if (shit % 2 == 0)
							thing.push(new Slinky(0, 0, GetRandomColor(), 50, 30, .5, function () {
								MoveTowards(this, thing[shit].back, .9)
							}, "smaller"))
						else
							thing.push(new Slinky(0, 0, GetRandomColor(), 5, 20, .5, function () {
								MoveTowards(this, thing[shit].back, .9)
							}))

					})()
			},

			Wass: function (target) {
				var thing = []
				thing.push(new Slinky(0, 0, GetRandomColor(), 5, 40, .5, function () {
					MoveTowards(this, target, .9)
				}))
				for (var i = 0; i < 1; i++)
					(function () {
						var shit = i
						if (shit % 2 == 0)
							thing.push(new Slinky(0, 0, GetRandomColor(), 20, 20, .5, function () {
								MoveTowards(this, thing[shit].back, .9)
							}))
						else
							thing.push(new Slinky(0, 0, GetRandomColor(), 5, 20, .5, function () {
								MoveTowards(this, thing[shit].back, .9)
							}))
					})()
			},

			IndependentSneks: function (x) {
				for (var i = 0; i < x; i++)
					(function () {
						var target = new Point()
						bop(target)
						var snek = new Slinky(0, 0, GetRandomColor(), GetRandomRange(10, 15), GetRandomRange(40, 200), .5, function () {
							MoveTowards(this, target, .01, 0, -1)
						})
					})()
				StartEngine()
			},

			AutonomousSnakes: function () {
				for (var i = 0; i < 1; i++)(function () {
					//var target = new Point()
					//bop(target)
					//var snek = new Slinky(0, 0, GetRandomColor(), GetRandomRange(10,15), GetRandomRange(40,200), .5, function(){ MoveTowards(this, target, .01,0,-1)})
					var snek = new Slinky(300, 300, GetRandomColor(), GetRandomRange(10, 15), GetRandomRange(40, 200), .5, function () {
						SnekBehaviourTree()
					})
				})()
			}
		},

		Tree: function (target) {
			var tree = new Slinky(0, 0, GetRandomColor(), 5, 40, .5, function () {
				MoveTowards(this, target, .9)
			})
		},
	},

	Games: {
		Asteroids: function () {
			isWraparound = true
			//Create spaceship controller
			var controller = AddController()
			controller.Bind("keydown", 27, "StopEngine") //Escape clearInterval(gameLoop)
			controller.Bind("keydown", 32, "Shoot") // Space
			controller.Bind("keydown", 38, "Thrust") // Up Arrow
			controller.Bind("keydown", 37, "TurnLeft") // Left Arrow
			controller.Bind("keydown", 39, "TurnRight") // Right Arrow

			controller.Bind("keyup", 32, "unshoot")
			controller.Bind("keyup", 38, "unThrust") // player.speed = 0
			controller.Bind("keyup", 37, "unTurnLeft")
			controller.Bind("keyup", 39, "unTurnRight")

			//Spawn asteroids on random points
			for (var i = 0; i < 1; i++) {
				var point = GetRandomPointOnCanvas()
				var asteroid = new Asteroid(point.x, point.y)
			}

			//Create Spaceship
			var spaceship = new Spaceship(canvas.centerPosition.x, canvas.centerPosition.y)


			//Set player object
			player = new Player("Emil", "good", 0)

			//Set up score
			Subscribe(player.AddScore, "score")

			var score = new ScoreDisplay()
			StartEngine()

			log(localStorage.getItem("highscore"))

		},

		GridGame: function () {
            
			var grid = new Grid()
            
			var player1 = new GridEntity(5, 5, "green")
			var player2 = new GridEntity(0, 0, "red")

			var controller1 = AddController()
			controller1.Bind("keydown", 27, "StopEngine") //Escape clearInterval(gameLoop)
			controller1.Bind("keydown", 38, "move up") // Up Arrow
			controller1.Bind("keydown", 37, "move left") // Left Arrow
			controller1.Bind("keydown", 39, "move right") // Right Arrow
			controller1.Bind("keydown", 40, "move down") // Down Arrow


			controller1.Bind("keyup", 38, "unUp")
			controller1.Bind("keyup", 37, "unLeft")
			controller1.Bind("keyup", 39, "unRight")
			controller1.Bind("keyup", 40, "unDown")


			StartEngine()
		},
        GridOptimizationGame: function () {
            
			grid = new Grid(10,5)
            
			var player1 = new GridPlayer(5, 5, "green")
            var factory1 = new GridFactory(6,5,"yellow")

			var controller1 = AddController()
			controller1.Bind("keydown", 27, "StopEngine") //Escape clearInterval(gameLoop)
			controller1.Bind("keydown", 38, "move up") // Up Arrow
			controller1.Bind("keydown", 37, "move left") // Left Arrow
			controller1.Bind("keydown", 39, "move right") // Right Arrow
			controller1.Bind("keydown", 40, "move down") // Down Arrow


			controller1.Bind("keyup", 38, "unUp")
			controller1.Bind("keyup", 37, "unLeft")
			controller1.Bind("keyup", 39, "unRight")
			controller1.Bind("keyup", 40, "unDown")


			StartEngine()
		},
	},

	Music: {

		/*
	I fucking hate tone.js because of all bullshit

	Here is my todo to learn it anyways

	My goal:
	Play two independent loops simultaneously, each consisting of a sequence of 7 respectively 5 notes to create my cool song.

	In order to do that I must learn:
	1. Play a tone sequenc
	2. Put tone sequence in a loop
	3. Find the right tones
	4. Find how to make empty tones or whatever
	*/
		Piano: function () {
			var piano = new Tone.Synth({
				"oscillator": {
					"type": "triangle"
				},
				"envelope": {
					"attack": 0.01,
					"decay": 0.2,
					"sustain": 0.2,
					"release": 0.2,
				}
			}).toMaster()

			var piano2 = new Tone.Synth({
				"oscillator": {
					"type": "triangle"
				},
				"envelope": {
					"attack": 0.01,
					"decay": 0.2,
					"sustain": 0.2,
					"release": 0.2,
				}
			}).toMaster()

			var piano3 = new Tone.Synth({
				"oscillator": {
					"type": "sine"
				},
				"envelope": {
					"attack": 0.01,
					"decay": 0.2,
					"sustain": 0.2,
					"release": 0.5,
				}
			}).toMaster()


			var cChord = ["C4", "E4", "G4", "B4"]
			var dChord = ["D4", "F4", "A4", "C5"]
			var gChord = ["B3", "D4", "E4", "A4"]
			//var pianoPart = new Tone.Part(function (time, chord) {
			//piano.triggerAttackRelease(chord, "8n", time)
			//}, [["0:0:2", cChord], ["0:1", cChord], ["0:1:3", dChord], ["0:2:2", cChord], ["0:3", cChord], ["0:3:2", gChord]]).start("0")
			//pianoPart.loop = true
			//pianoPart.loopEnd = "1m"


			var pianoPart = new Tone.Sequence(function (time, note) {
				piano.triggerAttackRelease(note, "16n", time)
			}, [["F4", "0", "A4", "0", "C5", '0', '0']], '16n * 7').start(0)
			//pianoPart.humanize = true

			var bass = new Tone.MonoSynth({
				"volume": -10,
				"envelope": {
					"attack": 0.1,
					"decay": 0.3,
					"release": 2,
				},
				"filterEnvelope": {
					"attack": 0.001,
					"decay": 0.01,
					"sustain": 0.5,
					"baseFrequency": 200,
					"octaves": 2.6
				}
			}).toMaster()
			var bassPart = new Tone.Sequence(function (time, note) {
				piano2.triggerAttackRelease(note, "16n", time)
			}, [["E4", "0", "D4", "0", "0"]], "16n * 5").start(0)

			var kick = new Tone.MembraneSynth({
				"envelope": {
					"sustain": 0,
					"attack": 0.02,
					"decay": 0.8
				},
				"octaves": 10
			}).toMaster()
			/* stable on C
			var slowBassPart = new Tone.Sequence(function (time, note) {
				piano3.triggerAttackRelease(note, "16n", time)
			}, ["C2", "C3", "C2", "C3"], "16n * 4").start(0)
			*/
			var slowBassPart = new Tone.Sequence(function (time, note) {
				piano3.triggerAttackRelease(note, "16n", time)


			}, ["D3", "A3", "D3", "A3", "E3", "B3", "E3", "B3", "F3", "C4", "F3", "C4", "E3", "B3", "E3", "B3"], "16n * 4").start(0)
			//bassPart.probability = 0.9

			/*
			var kickPart = new Tone.Sequence(function (time, note) {
				kick.triggerAttackRelease(note, "8n", time)
			}, ['C2', ['C2', 'C2', 'C2', 'C2', 'C2', 'C2'], 'C2', 'C2'], "16n * 4").start(0)
			*/

			//bassPart.humanize = true
			Tone.Transport.bpm.value = 90
			Tone.Transport.start("+0.1")


		}
	},

	Tests: {
		CheckPushBehavior: function () {
			var rect = new MyRect(1, 1)
			BehaviourListUpdater.call(rect) //Comment and decoment this row.. It appears to work
			log('inherit behaviour thingy works if false:', !rect.PushBehaviour) //does 
			log('Position inheritance works if 1:', rect.x)
		},

		PrototypeChecking: function () { //Test failed. Don't know how to do this.
			var rect = new MyRect()
			log('Test 1. Should return false:', rect instanceof BehaviourListUpdater)
			BehaviourListUpdater.call(rect)
			log('Test 2. Should return true:', rect.isPrototypeOf(BehaviourListUpdater))
			log(Object.getPrototypeOf(rect))
			log(rect.entries)
		},

		Subscription: function () {
			var rect = new MyRect()
			Subscribe(rect.Signal, "mousedown")
			rect.signal = function (event) {
				if (event.type = "mousedown") {
					log("I'm ", rect, "and I can se your mouseclick!", event.value)
				}
			}
			StartEngine()
		},
	},

	GenerateNature: function () {
		log("hello nature")
		var t = new WeirdThing()
		StartEngine()
	},

	LSystem: function () {
		var axiom = "a"
		var sentence = axiom
		var rule1 = {
			a: "a",
			b: "ab"
		}

		var rule2 = {
			a: "b",
			b: "a"
		}

		function Setup() {
			log(axiom)
		}

		function Generate() {
			var nextSentence = ""
			for (var i = 0; i < sentence.length; i++) {
				var current = sentence.charAt(i)
				if (current == rule1.a) {
					nextSentence += rule1.b
				} else if (current == rule2.a) {
					nextSentence += rule2.b
				} else {
					nextSentence += current
				}
			}
			sentence = nextSentence
			log(sentence)
		}

		Setup()
		Generate()
		Generate()
		Generate()
	},

	SmileyTest: function () {
		var smileys = []

		//init smileys
		for (var i = 0; i < 300; i++) {
			smileys[i] = {}
			smileys[i].x = Math.random() * canvas.width
			smileys[i].y = Math.random() * canvas.height
		}

		var canvasTemp = document.createElement("canvas")
		var tCtx = canvasTemp.getContext("2d")

		canvasTemp.width = canvasTemp.height = 150



		DrawSmiley()

		function update() {
			context.globalAlpha = .01
			//context.clearRect(0, 0, canvas.width, canvas.height)
			context.fillRect(0, 0, canvas.width, canvas.height)
			context.globalAlpha = 1
			for (var i = 0, len = smileys.length; i < len; i++) {
				var x = smileys[i].x += Math.random() * 3 - 1.5,
					y = smileys[i].y += Math.random() * 3 - 1.5
				if (x > canvas.width) {
					smileys[i].x = -75
				}

				if (y > canvas.height) {
					smileys[i].y = -75
				}
				DrawSprite(canvasTemp, i * .01 * Math.PI, x, y, 75, 75, 75)
				//context.drawImage(tCtx.canvas, x, y)
			}
			setTimeout(update, 1)
		}

		update()
	},

	Button: function () {

		var button = new Button(new MyRect(100, 100, 300, 100))

		//button.rect =  //NOTE: Not allowed since button inherits rects. Fix a setter function for this to work

		button.Select = function () {
			this.color = "green"
		}
		button.Unselect = function () {
			button.color = "gray"
		}
		button.Press = function () {
			this.color = "yellow"
		}
		button.Unpress = function () {
			this.color = "pink"
		}


		button.color = "gray"
		button.text = "Start Game"
		button.textColor = "blue"

		button.Action = function () {
			Project.Games.Asteroids()
		}

		StartEngine()
	},
}
