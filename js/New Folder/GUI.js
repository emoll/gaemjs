//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h


/* File content description:
	
	Lorem ipsum...
*/
/* Todo:
	Add File content description
*/

"use strict";


// functionality

function AddToGUI(obj) {
	GUI.push(obj);
}

function CheckGUI() { // TODO: Clarify description
	for (var i = 0; i < GUI.length; i++) {
		GUI[i].check();
	}
}

// GUI Elements

// Should be GUI??

function Pointer(x, y, radius, color, behaviour) { // Or Cursor?
	radius = DefaultParameter(radius, 70);
	this.x = x;
	this.y = y;
	this.radius = radius;
	this.color = "#00ffff33";
	this.Update = behaviour;
	this.Draw = function () {
		DrawCircle(this.x, this.y, 10, "#FFFFFF", 1);
	}
}

function Button(sRect, text, Action) { // Note: Specifically square buttons
	var self = this;
	text = DefaultParameter(text, "");
	Action = DefaultParameter(Action, function () {});
	self.color = DefaultParameter(self.color, "orange");
	self.text = text;
	self.Action = Action;

	AddToGUI(self);

	self.state = "selected";
	Rect.call(self, sRect.x, sRect.y, sRect.width, sRect.height, sRect.color, sRect.alpha);
	self.oldDraw = self.Draw; //Jeesus self is uggly code 
	self.text = "";
	self.textColor = "green";
	self.textSize = "18";
	self.textOffsetX = 30;
	self.textOffsetY = 30;

	self.Draw = function () {
		self.oldDraw();
		DrawText(self.x + self.textOffsetX, self.y + self.textOffsetY, self.text, self.textColor, self.textSize);
	}
	self.Select;
	self.Unselect;
	self.Press;
	self.Unpress = function () {
		log("replace this function");
	};
	self.Action = Action;
	self.Check = function () {
		if (IsPosInRect(mouse, self) && mouse.left)
			self.Press();
		if (IsPosInRect(mouse, self)) self.select();

		else self.color = "gray";
	}

	self.Signal = function (event) {
		log("i get message", event.type);
		if (event.type == "mousedown") {
			if (IsPosInRect(mouse, self)) {
				self.Press();
			}
		}
		if (event.type == "mouseup") {
			self.Unpress();
			if (IsPosInRect(mouse, self)) {
				self.Action();
			}
		}
	}

	Subscribe(self.Signal, "mousedown");
	Subscribe(self.Signal, "mouseup");
}

function ScoreDisplay() { // Specifically used in Asteroids
	var self = this;
	AddToGUI(self);

	this.ChangeScore = function () {
		log("hmmm");
	};
	this.RegisterScore = function () {
		if (player.score > localStorage.getItem("highscore")) //todo: fix if null score
			localStorage.setItem("highscore", player.score);
	}
	Subscribe(this.ChangeScore, "score");
	Subscribe(this.RegisterScore, "registerScore");



	this.Draw = function () {
		DrawText(100, 100, player.score, "white", 20);
	}
}
