//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* File content description:
	
	high level stuff
	Tests
	Simulations

	basically high level assembling of games/worlds
	in each project things like these are allowed to be defined
		Behaviours of the world
		Game objects
*/
/* Todo:
	Add File content description
	Move all entities into GameObjects.js
*/

var Projects = {
	Cirlcles: {
		Slinkies: {
			FirstSlinkyExperiments: function () { // TODO: Go through these
				//entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, spinAroundRandomEntity(.02)));

				//entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, spinAroundMouseBehaviour));
				//entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, spinAroundRandomEntity(.02)));
				//entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, followRandomEntity()));

				/*
				for (var i =0;i < 100;i++)
					entities.push(new Slinky(GetRandomRange(0,canvas.width), GetRandomRange(0,canvas.height), GetRandomColor(), 10, 40, .999, function(){ SpinAroundPoint(this, pointer, .02); }));
				*/
				//entities.push(new Slinky(pointer.x, pointer.y, "#00fff0", 10, 20, .7, entities[0].circles[entities[0].circles.length-1]));
				//entities.push(new Slinky(pointer.x, pointer.y, "#f00ff0", 20, 5, .9, entities[1].circles[entities[1].circles.length-1]));
				//entities.push(new Slinky(pointer.x, pointer.y, "#00fff0", 10, 20, .7, entities[2].circles[entities[2].circles.length-1]));
				/*
				for (var i=0;i<3;i++)
				{
					entities.push(new Slinky(pointer.x, pointer.y, GetRandomColor(), GetRandomRange(5,40), GetRandomRange(5,20), GetRandomRange(.01,.99), entities[entities.length-1].circles[entities[entities.length-1].circles.length-1]));
				}
				*/
			},

			Snek: function (target) { // TODO: Add all these sneks to GameObjects.js
				var slinkies = [];
				var slink = new Slinky(100, 100, "#aa0044", 5, 15, .5);
				//slink.target = mouse;
				AddBehaviour(slink, Go);
				AddBehaviour(slink, Turn);
				slink.speed = 5;
				slink.angle = .001;


				slinkies.push(slink);
				for (var i = 0; i < 16; i++) { //temporary number. 16 loops is the original
					(function () {
						var slink;
						if (i % 2 == 0) {
							slink = new Slinky(0, 0, "#ffffff", 20 - i, 10, .5);
						} else {
							slink = new Slinky(0, 0, "#FF0000", 20 - i, 15, .5);
						}
						AddBehaviour(slink, MoveTowardsTarget);
						slink.target = slinkies[i].back;
						//slink.elasticity = .9;
						slinkies.push(slink);
					})();
					//throw Error(slinkies);
					//Snek tail
					/* temoprary commented out while testing
					slinkies.push(new Slinky(0, 0, "#FF0000", 20, 15, .5, function () {
						MoveTowards(this, slinkies[slinkies.length - 1].back, .9);
					}, "bigger"));
					*/
				}
				StartEngine();
				return slinkies;


			},

			CandyBar: function (target) {
				if (!target) {
					target = new Point(0, 0);
					log("CandyBar has no defined target");
				}
				var candyParts = [];
				candyParts.push(new Slinky(0, 0, GetRandomColor(), 5, 15, .5, function () {
					MoveTowards(this, target, .9);
				}));
				for (var i = 0; i < 40; i++)(function () {
					var target = candyParts[i].back;
					var u = i;
					if (i % 2 == 0)
						candyParts.push(new Slinky(0, 0, "#ffffff", 20, 20, .5, function () {
							MoveTowards(this, target, .7)
						}));
					else
						candyParts.push(new Slinky(0, 0, GetRandomColor(), 20, 20, .5, function () {
							MoveTowards(this, target, .7)
						}));
				})();
			},

			Shrooms: function (target) {
				var thing = [];
				thing.push(new Slinky(0, 0, GetRandomColor(), 5, 3, .5, function () {
					MoveTowards(this, target, .9)
				}));
				//thing.push(new Slinky(200, 0, GetRandomColor(), 4, 60, .5, function(){MoveTowards(this, pointer, .9)}));
				for (var i = 0; i < 10; i++)
					(function () {
						var shit = i;
						if (shit % 2 == 0)
							thing.push(new Slinky(0, 0, GetRandomColor(), 50, 30, .5, function () {
								MoveTowards(this, thing[shit].back, .9)
							}, "smaller"));
						else
							thing.push(new Slinky(0, 0, GetRandomColor(), 5, 20, .5, function () {
								MoveTowards(this, thing[shit].back, .9)
							}));

					})();
			},

			Wass: function (target) {
				var thing = [];
				thing.push(new Slinky(0, 0, GetRandomColor(), 5, 40, .5, function () {
					MoveTowards(this, target, .9)
				}));
				for (var i = 0; i < 1; i++)
					(function () {
						var shit = i;
						if (shit % 2 == 0)
							thing.push(new Slinky(0, 0, GetRandomColor(), 20, 20, .5, function () {
								MoveTowards(this, thing[shit].back, .9)
							}));
						else
							thing.push(new Slinky(0, 0, GetRandomColor(), 5, 20, .5, function () {
								MoveTowards(this, thing[shit].back, .9)
							}));
					})();
			},

			IndependentSneks: function (x) {
				for (var i = 0; i < x; i++)
					(function () {
						var target = new Point();
						bop(target);
						var snek = new Slinky(0, 0, GetRandomColor(), GetRandomRange(10, 15), GetRandomRange(40, 200), .5, function () {
							MoveTowards(this, target, .01, 0, -1);
						});
					})();
				StartEngine();
			},

			AutonomousSnakes: function () {
				for (var i = 0; i < 1; i++)(function () {
					//var target = new Point();
					//bop(target);
					//var snek = new Slinky(0, 0, GetRandomColor(), GetRandomRange(10,15), GetRandomRange(40,200), .5, function(){ MoveTowards(this, target, .01,0,-1);});
					var snek = new Slinky(300, 300, GetRandomColor(), GetRandomRange(10, 15), GetRandomRange(40, 200), .5, function () {
						SnekBehaviourTree();
					});
				})();
			}
		},

		Tree: function (target) {
			var tree = new Slinky(0, 0, GetRandomColor(), 5, 40, .5, function () {
				MoveTowards(this, target, .9)
			});
		},
	},

	Games: {
		Asteroids: function () {
			isWraparound = true;
			//Create spaceship controller
			var controller = AddController();
			controller.Bind("keydown", 27, "StopEngine"); //Escape clearInterval(gameLoop);
			controller.Bind("keydown", 32, "Shoot"); // Space
			controller.Bind("keydown", 38, "Thrust"); // Up Arrow
			controller.Bind("keydown", 37, "TurnLeft"); // Left Arrow
			controller.Bind("keydown", 39, "TurnRight"); // Right Arrow

			controller.Bind("keyup", 32, "unshoot");
			controller.Bind("keyup", 38, "unThrust"); // player.speed = 0;
			controller.Bind("keyup", 37, "unTurnLeft");
			controller.Bind("keyup", 39, "unTurnRight");

			//Spawn asteroids on random points
			for (var i = 0; i < 1; i++) {
				var point = GetRandomPointOnCanvas();
				var asteroid = new Asteroid(point.x, point.y);
			}

			//Create Spaceship
			var spaceship = new Spaceship(canvas.centerPosition.x, canvas.centerPosition.y);


			//Set player object
			player = new Player("Emil", "good", 0);

			//Set up score
			Subscribe(player.AddScore, "score");

			var score = new ScoreDisplay();
			StartEngine();

			log(localStorage.getItem("highscore"));

		},

		GridGame: function () {
			var grid = new Grid();
			var player1 = new GridEntity(5, 5, "green");
			var player2 = new GridEntity(0, 0, "red");

			var controller1 = AddController();
			controller1.Bind("keydown", 27, "StopEngine"); //Escape clearInterval(gameLoop);
			controller1.Bind("keydown", 38, "Up"); // Up Arrow
			controller1.Bind("keydown", 37, "Left"); // Left Arrow
			controller1.Bind("keydown", 39, "Right"); // Right Arrow
			controller1.Bind("keydown", 40, "Down");


			controller1.Bind("keyup", 38, "unUp");
			controller1.Bind("keyup", 37, "unLeft");
			controller1.Bind("keyup", 39, "unRight");
			controller1.Bind("keyup", 40, "unDown");


			StartEngine();
		},
	},

	Music: {

		/*
	I fucking hate tone.js because of all bullshit

	Here is my todo to learn it anyways

	My goal:
	Play two independent loops simultaneously, each consisting of a sequence of 7 respectively 5 notes to create my cool song.

	In order to do that I must learn:
	1. Play a tone sequenc
	2. Put tone sequence in a loop
	3. Find the right tones
	4. Find how to make empty tones or whatever
	*/
		Piano: function () {
			var piano = new Tone.Synth({
				"oscillator": {
					"type": "triangle"
				},
				"envelope": {
					"attack": 0.01,
					"decay": 0.2,
					"sustain": 0.2,
					"release": 0.2,
				}
			}).toMaster()

			var piano2 = new Tone.Synth({
				"oscillator": {
					"type": "triangle"
				},
				"envelope": {
					"attack": 0.01,
					"decay": 0.2,
					"sustain": 0.2,
					"release": 0.2,
				}
			}).toMaster()

			var piano3 = new Tone.Synth({
				"oscillator": {
					"type": "sine"
				},
				"envelope": {
					"attack": 0.01,
					"decay": 0.2,
					"sustain": 0.2,
					"release": 0.5,
				}
			}).toMaster()


			var cChord = ["C4", "E4", "G4", "B4"];
			var dChord = ["D4", "F4", "A4", "C5"];
			var gChord = ["B3", "D4", "E4", "A4"];
			//var pianoPart = new Tone.Part(function (time, chord) {
			//piano.triggerAttackRelease(chord, "8n", time);
			//}, [["0:0:2", cChord], ["0:1", cChord], ["0:1:3", dChord], ["0:2:2", cChord], ["0:3", cChord], ["0:3:2", gChord]]).start("0");
			//pianoPart.loop = true;
			//pianoPart.loopEnd = "1m";


			var pianoPart = new Tone.Sequence(function (time, note) {
				piano.triggerAttackRelease(note, "16n", time);
			}, [["F4", "0", "A4", "0", "C5", '0', '0']], '16n * 7').start(0);
			//pianoPart.humanize = true;

			var bass = new Tone.MonoSynth({
				"volume": -10,
				"envelope": {
					"attack": 0.1,
					"decay": 0.3,
					"release": 2,
				},
				"filterEnvelope": {
					"attack": 0.001,
					"decay": 0.01,
					"sustain": 0.5,
					"baseFrequency": 200,
					"octaves": 2.6
				}
			}).toMaster();
			var bassPart = new Tone.Sequence(function (time, note) {
				piano2.triggerAttackRelease(note, "16n", time);
			}, [["E4", "0", "D4", "0", "0"]], "16n * 5").start(0);

			var kick = new Tone.MembraneSynth({
				"envelope": {
					"sustain": 0,
					"attack": 0.02,
					"decay": 0.8
				},
				"octaves": 10
			}).toMaster();
			/* stable on C
			var slowBassPart = new Tone.Sequence(function (time, note) {
				piano3.triggerAttackRelease(note, "16n", time);
			}, ["C2", "C3", "C2", "C3"], "16n * 4").start(0);
			*/
			var slowBassPart = new Tone.Sequence(function (time, note) {
				piano3.triggerAttackRelease(note, "16n", time);


			}, ["D3", "A3", "D3", "A3", "E3", "B3", "E3", "B3", "F3", "C4", "F3", "C4", "E3", "B3", "E3", "B3"], "16n * 4").start(0);
			//bassPart.probability = 0.9;

			/*
			var kickPart = new Tone.Sequence(function (time, note) {
				kick.triggerAttackRelease(note, "8n", time);
			}, ['C2', ['C2', 'C2', 'C2', 'C2', 'C2', 'C2'], 'C2', 'C2'], "16n * 4").start(0);
			*/

			//bassPart.humanize = true;
			Tone.Transport.bpm.value = 90;
			Tone.Transport.start("+0.1");


		}
	},

	Tests: {
		CheckPushBehavior: function () {
			var rect = new MyRect(1, 1);
			BehaviourListUpdater.call(rect); //Comment and decoment this row.. It appears to work
			log('inherit behaviour thingy works if false:', !rect.PushBehaviour); //does 
			log('Position inheritance works if 1:', rect.x);
		},

		PrototypeChecking: function () { //Test failed. Don't know how to do this.
			var rect = new MyRect();
			log('Test 1. Should return false:', rect instanceof BehaviourListUpdater);
			BehaviourListUpdater.call(rect);
			log('Test 2. Should return true:', rect.isPrototypeOf(BehaviourListUpdater));
			log(Object.getPrototypeOf(rect));
			log(rect.entries);
		},

		Subscription: function () {
			var rect = new MyRect();
			Subscribe(rect.Signal, "mousedown");
			rect.signal = function (event) {
				if (event.type = "mousedown") {
					log("I'm ", rect, "and I can se your mouseclick!", event.value);
				}
			}
			StartEngine();
		},
	},

	GenerateNature: function () {
		log("hello nature");
		var t = new WeirdThing();
		StartEngine();
	},

	LSystem: function () {
		var axiom = "a"
		var sentence = axiom;
		var rule1 = {
			a: "a",
			b: "ab"
		}

		var rule2 = {
			a: "b",
			b: "a"
		}

		function Setup() {
			log(axiom);
		}

		function Generate() {
			var nextSentence = "";
			for (var i = 0; i < sentence.length; i++) {
				var current = sentence.charAt(i);
				if (current == rule1.a) {
					nextSentence += rule1.b;
				} else if (current == rule2.a) {
					nextSentence += rule2.b;
				} else {
					nextSentence += current;
				}
			}
			sentence = nextSentence;
			log(sentence);
		}

		Setup();
		Generate();
		Generate();
		Generate();
	},

	SmileyTest: function () {
		var smileys = [];

		//init smileys
		for (var i = 0; i < 300; i++) {
			smileys[i] = {};
			smileys[i].x = Math.random() * canvas.width;
			smileys[i].y = Math.random() * canvas.height;
		}

		var canvasTemp = document.createElement("canvas");
		var tCtx = canvasTemp.getContext("2d");

		canvasTemp.width = canvasTemp.height = 150;



		DrawSmiley();

		function update() {
			context.globalAlpha = .01;
			//context.clearRect(0, 0, canvas.width, canvas.height);
			context.fillRect(0, 0, canvas.width, canvas.height);
			context.globalAlpha = 1;
			for (var i = 0, len = smileys.length; i < len; i++) {
				var x = smileys[i].x += Math.random() * 3 - 1.5,
					y = smileys[i].y += Math.random() * 3 - 1.5;
				if (x > canvas.width) {
					smileys[i].x = -75;
				}

				if (y > canvas.height) {
					smileys[i].y = -75;
				}
				DrawSprite(canvasTemp, i * .01 * Math.PI, x, y, 75, 75, 75);
				//context.drawImage(tCtx.canvas, x, y);
			}
			setTimeout(update, 1);
		}

		update();
	},

	Button: function () {

		var button = new Button(new MyRect(100, 100, 300, 100));

		//button.rect = ; //NOTE: Not allowed since button inherits rects. Fix a setter function for this to work

		button.Select = function () {
			this.color = "green";
		};
		button.Unselect = function () {
			button.color = "gray";
		}
		button.Press = function () {
			this.color = "yellow";
		}
		button.Unpress = function () {
			this.color = "pink";
		}


		button.color = "gray";
		button.text = "Start Game";
		button.textColor = "blue";

		button.Action = function () {
			Project.Games.Asteroids();
		}

		StartEngine();
	},
}





//These functions draws the layers on the screen and stuff

function Pattern(texture) {
	var times = 5;
	for (var i = 0; i < times; i++) {
		for (var ii = 0; ii < times; ii++) {
			DrawSprite(defaultLayer, texture, 0, i * 300, ii * 300, 50, 50, 250);
		}
	}
}

function PatternWithGet(times, padding, radius, texture) { //TODO add radius property to all textures...
	for (var ii = 0; ii < times; ii++) {
		for (var i = 0; i < times; i++) {

			DrawSprite(defaultLayer, InfiniteFuncValue(texture), 0, i * padding + (ii % 2) * padding / 2, 0.5 * ii * padding, radius, radius, radius);
		}
	}
}

function DrawTextureOnce(x, y, texture) {
	DrawSprite(defaultLayer, texture, 0, x, y, texture.width / 2, texture.height / 2, texture.width / 2); // Fix the specific 50 dimension stuff
}

function thingDrawer(x, y, textureFunction) {
	entities.push(this);
	this.textureFunction = textureFunction;
	this.x = x;
	this.y = y;
	this.Update = function () {};
	this.Draw = function () {
		DrawSprite(defaultLayer, InfiniteFuncValue(this.textureFunction), 0, this.x, this.y, 50, 50, 50); // Fix the specific 50 dimension stuff
	}
}

function DrawTextureOnceInCenter(texture) {
	DrawTextureOnce(canvas.centerPosition.x, canvas.centerPosition.y, texture);
}


// Stuff to draw begins here

function GetWeirdRainbowSausageLayer() {
	var layer1 = CreateNewLayer(100, 100);
	for (var i = 0; i < 25; i++) {
		DrawCircle2(layer1, 50 + GetRandomRange(0, 1), 10 + i, GetRandomRange(5, 10), GetRandomColor(), 1);
	}
	return layer1;
}

function GetWeirdGameBoySausageLayer() {
	var layer1 = CreateNewLayer(100, 100);
	for (var i = 0; i < 3; i++) {
		DrawCircle2(layer1, 50, 10 + i * 10, 5, GameBoyPalette[i], 1);
	}
	return layer1;
}


//Avocado shape

function GetAvocadoLayer() {
	var layer1 = CreateNewLayer(100, 100);
	var color = "white";
	var n = 0;
	for (var i = 0; i < 25; i++) {
		DrawCircle2(layer1, Noise(n) + 50, Noise(n) + 50 + i, Noise(0.5 * n) + 10 - 5 * (i / 25), color, 1);
	}
	return layer1;
}

function GetAvocadoWithCromaticLayer() { //I can do this effect better by drawing a texture three times and coloring 2 of the layers with some filter.
	var layer1 = CreateNewLayer(100, 100);
	var color = "white";
	var cromatic1 = "#FF00FF11";
	var chromatic2 = "#00FF0011";
	var n = 0;
	var chromaticDistance = 5;

	for (var i = 0; i < 25; i++) {
		DrawCircle2(layer1, chromaticDistance + Noise(n) + 50, chromaticDistance + Noise(n) + 50 + i, Noise(0.5 * n) + 10 - 5 * (i / 25), cromatic1, 1);
	}

	for (var i = 0; i < 25; i++) {
		DrawCircle2(layer1, -chromaticDistance + Noise(n) + 50, -chromaticDistance + Noise(n) + 50 + i, Noise(0.5 * n) + 10 - 5 * (i / 25), chromatic2, 1);
	}

	for (var i = 0; i < 25; i++) {
		DrawCircle2(layer1, Noise(n) + 50, Noise(n) + 50 + i, Noise(0.5 * n) + 10 - 5 * (i / 25), color, 1);
	}



	return layer1;
}

function GetAvocadoWithDoubleOutlineLayer() {
	var layer1 = CreateNewLayer(100, 100);
	var outlineColor = "#AA8446"; //"blue";
	var outlineColor2 = "#AA8446"; //"black";

	var color = "#C69656"; //"white";
	var n = 0;
	for (var i = 0; i < 25; i++) {
		DrawCircle2(layer1, Noise(n) + 50, Noise(n) + 50 + i, Noise(0.5 * n) + 10 - 5 * (i / 25) + 6, outlineColor2, 1);
	}
	for (var i = 0; i < 25; i++) {
		DrawCircle2(layer1, Noise(n) + 50, Noise(n) + 50 + i, Noise(0.5 * n) + 10 - 5 * (i / 25) + 3, outlineColor, 1);
	}
	for (var i = 0; i < 25; i++) {
		DrawCircle2(layer1, Noise(n) + 50, Noise(n) + 50 + i, Noise(0.5 * n) + 10 - 5 * (i / 25), color, 1);
	}
	return layer1;
}

function GetAvocadoStarLayer() { //Star
	var layer1 = CreateNewLayer(100, 100);
	var color = "white";

	var avocado = GetAvocadoLayer;

	var wings = 4;

	for (var i = 0; i < wings; i++) {
		for (var i = 0; i < 25; i++) {
			DrawSprite(layer1, avocado(), (i / wings) * 2 * Math.PI, 50, 50, 50, 50, 50);
		}
	}


	return layer1;
}


//Plants and grass

function GetPlantLayer() {
	var layer1 = CreateNewLayer(500, 500);
	var color = "#AAAAAA";
	var n = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20;
	this.speed = 10;
	this.speedIcr = -.2;
	this.angle = .05;
	this.dir = -Math.PI / 2;
	for (var i = 0; i < 50; i++) {
		Go();
		Turn();
		DrawCircle2(layer1, Noise(n) + this.x, Noise(n) + this.y, Noise(n) + 10 - 5 * (i / 50), color, 1);
		if (Math.random() < 1) {
			DrawSprite(layer1, GetAvocadoLayer(), dir, this.x, this.y, 25, 25, 25);
		}
	}
	return layer1;

} //Old

function GetTheWitnessGrassLayer() {
	var layer1 = CreateNewLayer(500, 500);


	var redMin = 250;
	var redMax = 255;
	var greenMin = 240;
	var greenMax = 255;
	var blueMin = 50;
	var blueMax = 100;

	var grayMin = 180;
	var grayMax = 240;

	var gray = Math.round(GetRandomRange(grayMin, grayMax));


	var red = Math.round(GetRandomRange(redMin, redMax));
	var green = Math.round(GetRandomRange(greenMin, greenMax));
	var blue = Math.round(GetRandomRange(blueMin, blueMax));


	var color = rgbToHex(gray, gray, gray); //rgbToHex(red, green, blue) + "99"; //"#FFDD33";


	var n = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20;
	this.speed = GetRandomRange(1, 3);
	this.speedIcr = -.005;
	this.angle = 0;
	this.dir = -Math.PI / 2 + Noise(.6);

	var itterations = 100;
	for (var i = 0; i < itterations; i++) {
		Go();
		Turn();
		DrawCircle2(layer1, Noise(n) + this.x, Noise(n) + this.y, Noise(n) + 10 - 8 * (i / itterations), color, 1);
		if (Math.random() < .2) {
			this.angle -= .002;
		}
		if (Math.random() < .2) {
			this.angle += .002;
		}
	}
	return layer1;

}

function GetStringLayer() {
	var layer1 = CreateNewLayer(500, 500);


	var redMin = 250;
	var redMax = 255;
	var greenMin = 240;
	var greenMax = 255;
	var blueMin = 50;
	var blueMax = 100;

	var grayMin = 40;
	var grayMax = 240;

	var gray = Math.round(GetRandomRange(grayMin, grayMax));


	var red = Math.round(GetRandomRange(redMin, redMax));
	var green = Math.round(GetRandomRange(greenMin, greenMax));
	var blue = Math.round(GetRandomRange(blueMin, blueMax));

	//console.log(red);


	var color = rgbToHex(gray, gray, gray); //rgbToHex(red, green, blue) + "99"; //"#FFDD33";


	var n = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20;
	this.speed = 3;
	this.speedIcr = 0;
	this.angle = 0;
	this.dir = -Math.PI / 2 + Noise(.6);

	var itterations = 150;
	for (var i = 0; i < itterations; i++) {
		Go();
		Turn();
		DrawCircle2(layer1, Noise(n) + this.x, Noise(n) + this.y, Noise(n) + 2, color, 1);
		if (Math.random() < .5) {
			this.angle -= .005;
		}
		if (Math.random() < .5) {
			this.angle += .005;
		}
	}
	return layer1;

} // Rename

function GetShortStringLayer() {
	var layer1 = CreateNewLayer(500, 500);


	var redMin = 250;
	var redMax = 255;
	var greenMin = 240;
	var greenMax = 255;
	var blueMin = 50;
	var blueMax = 100;

	var grayMin = 40;
	var grayMax = 240;

	var gray = Math.round(GetRandomRange(grayMin, grayMax));


	var red = Math.round(GetRandomRange(redMin, redMax));
	var green = Math.round(GetRandomRange(greenMin, greenMax));
	var blue = Math.round(GetRandomRange(blueMin, blueMax));

	//console.log(red);


	var color = rgbToHex(gray, gray, gray); //rgbToHex(red, green, blue) + "99"; //"#FFDD33";


	var n = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20;
	this.speed = GetRandomRange(2, 3);
	this.speedIcr = 0;
	this.angle = 0;
	this.dir = -Math.PI / 2 + Noise(.6);

	var itterations = 110;
	for (var i = 0; i < itterations; i++) {
		Go();
		Turn();
		DrawCircle2(layer1, Noise(n) + this.x, Noise(n) + this.y, Noise(n) + 2, color, 1);
		if (Math.random() < .6) {
			this.angle -= .005;
		}
		if (Math.random() < .6) {
			this.angle += .005;
		}
	}
	return layer1;

} // Rename

function GetPetal1Layer() {
	/*
	variations of leaves:
		length
		coloration
		
		thicknessFunction input = i output = specificT
		BendFunction //This would be cool but because you can reverse time and shit I guess??? grow backwards. But I don't have the patience....
		
		
	*/
	var length = 100;
	var ThicknessFunction = function (i) {
		return Math.sin((i / length) * Math.PI) * 100; //Trying to make some even oscilation. Can't remember all maths dough.
	};
	var layer1 = CreateNewLayer(500, 500);
	// Make adaptible to the graphics drawn. (supposedly by itterating through all steps and calculating min/max X & Y)
	// A quick lazy way would be to just add up all speed+speed increase * steps + max thickness. (and maybe 1 pixel or other margin for blur if it's needed, don't know)
	var color1 = "#AAAAAA";
	var color2 = "#FF0033";

	var noise = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20; //replace 20 with something more substation. Maybe starting thickness?
	this.speed = 4;
	this.speedIcr = -.04;
	this.angle = 0;
	this.dir = -Math.PI / 2;


	for (var i = 0; i < length; i++) {
		let progress = i / length;
		Go();
		Turn();
		DrawCircle2(layer1, this.x, this.y, ThicknessFunction(i), lerpColor(color1, color2, progress), 1);
	}
	return layer1;
} // Rename

function GetPetal2Layer() {
	/*
	variations of leaves:
		length
		coloration
		
		thicknessFunction input = i output = specificT
		BendFunction //This would be cool but because you can reverse time and shit I guess??? grow backwards. But I don't have the patience....
		
		
	*/
	var length = 100;
	var ThicknessFunction = function (i) {
		return Math.sin((i / length) * Math.PI) * 100; //Trying to make some even oscilation. Can't remember all maths dough.
	};
	var layer1 = CreateNewLayer(500, 500);
	// Make adaptible to the graphics drawn. (supposedly by itterating through all steps and calculating min/max X & Y)
	// A quick lazy way would be to just add up all speed+speed increase * steps + max thickness. (and maybe 1 pixel or other margin for blur if it's needed, don't know)
	var color1 = GetRandomColor(); //"#FFFF00";
	var color2 = GetRandomColor(); //"#FF0000";

	var noise = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20; //replace 20 with something more substation. Maybe starting thickness?
	this.speed = 4;
	this.speedIcr = -.04;
	this.angle = Noise(0.01);
	this.dir = -Math.PI / 2; //aka up...

	this.Go = Go; //Because javascript can get confused who "this" belongs to
	this.Turn = Turn; //Because javascript can get confused who "this" belongs to

	for (var i = 0; i < length; i++) {
		let progress = i / length;
		this.Go();
		this.Turn();
		DrawCircle2(layer1, this.x, this.y, ThicknessFunction(i), lerpColor(color1, color2, progress), 1);
	}
	return layer1;
} // Rename

function GetGrasssssLayer() {
	/*
	variations of leaves:
		length
		coloration
		
		thicknessFunction input = i output = specificT
		BendFunction //This would be cool but because you can reverse time and shit I guess??? grow backwards. But I don't have the patience....
		
		
	*/
	var length = 100;
	var ThicknessFunction = function (i) {
		return Math.sin((i / length) * Math.PI) * 10; //Trying to make some even oscilation. Can't remember all maths dough.
	};
	var layer1 = CreateNewLayer(500, 500);
	// Make adaptible to the graphics drawn. (supposedly by itterating through all steps and calculating min/max X & Y)
	// A quick lazy way would be to just add up all speed+speed increase * steps + max thickness. (and maybe 1 pixel or other margin for blur if it's needed, don't know)
	var color1 = GetRandomColor(); //"#FFFF00";
	var color2 = GetRandomColor(); //"#FF0000";

	var noise = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20; //replace 20 with something more substation. Maybe starting thickness?
	this.speed = 4;
	this.speedIcr = -.04;
	this.angle = Noise(0.01);
	this.dir = -Math.PI / 2; //aka up...


	for (var i = 0; i < length; i++) {
		let progress = i / length;
		Go();
		Turn();
		DrawCircle2(layer1, this.x, this.y, ThicknessFunction(i), lerpColor(color1, color2, progress), 1);
	}
	return layer1;
} // Rename

function GetBubblePlantLayer() {
	/*
	variations of leaves:
		length
		coloration
		
		thicknessFunction input = i output = specificT
		BendFunction //This would be cool but because you can reverse time and shit I guess??? grow backwards. But I don't have the patience....
		
		
	*/

	var length = 100;
	var ThicknessFunction = function (i) {
		return Math.sin((i / length * 6) * Math.PI) * 15 + 25; //Trying to make some even oscilation. Can't remember all maths dough.
	};
	var layer1 = CreateNewLayer(500, 500);
	// Make adaptible to the graphics drawn. (supposedly by itterating through all steps and calculating min/max X & Y)
	// A quick lazy way would be to just add up all speed+speed increase * steps + max thickness. (and maybe 1 pixel or other margin for blur if it's needed, don't know)
	var color1 = GetRandomColor(); //"#FFFF00";
	var color2 = GetRandomColor(); //"#FF0000";

	var noise = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20; //replace 20 with something more substation. Maybe starting thickness?
	this.speed = 4;
	//this.speedIcr = -.04;
	this.angle = Noise(0.01);
	this.dir = -Math.PI / 2; //aka up...


	for (var i = 0; i < length; i++) {
		let progress = i / length;
		Go();
		Turn();
		DrawCircle2(layer1, this.x, this.y, ThicknessFunction(i), lerpColor(color1, color2, progress), 1);
	}
	return layer1;
} // Rename

function GetCircleGrassLayer() {
	/*
	variations of leaves:
		length
		coloration
		
		thicknessFunction input = i output = specificT
		BendFunction //This would be cool but because you can reverse time and shit I guess??? grow backwards. But I don't have the patience....
		
		
	*/

	var length = 100;
	var ThicknessFunction = function (i) {
		return Math.abs(this.speed * 5) //Trying to make some even oscilation. Can't remember all maths dough.
	};
	var layer1 = CreateNewLayer(500, 500);
	// Make adaptible to the graphics drawn. (supposedly by itterating through all steps and calculating min/max X & Y)
	// A quick lazy way would be to just add up all speed+speed increase * steps + max thickness. (and maybe 1 pixel or other margin for blur if it's needed, don't know)
	var color1 = GetRandomColor(); //"#FFFF00";
	var color2 = GetRandomColor(); //"#FF0000";

	var noise = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20; //replace 20 with something more substation. Maybe starting thickness?
	this.speed = 4;
	this.speedIcr = -.04;
	this.angle = Noise(0.01);
	this.dir = -Math.PI / 2; //aka up...


	for (var i = 0; i < length; i++) {
		let progress = i / length;
		Go();
		Turn();
		DrawCircle2(layer1, this.x, this.y, ThicknessFunction(i), lerpColor(color1, color2, progress), 1);
	}
	return layer1;
} // Rename

function GetCircleLeafLayer() {
	/*
	variations of leaves:
		length
		coloration
		
		thicknessFunction input = i output = specificT
		BendFunction //This would be cool but because you can reverse time and shit I guess??? grow backwards. But I don't have the patience....
		
		
	*/

	var length = 100;
	var ThicknessFunction = function (i) {
		return Math.abs(this.speed * 5) //Trying to make some even oscilation. Can't remember all maths dough.
	};
	var layer1 = CreateNewLayer(500, 500);
	// Make adaptible to the graphics drawn. (supposedly by itterating through all steps and calculating min/max X & Y)
	// A quick lazy way would be to just add up all speed+speed increase * steps + max thickness. (and maybe 1 pixel or other margin for blur if it's needed, don't know)
	var color1 = GetRandomColor(); //"#FFFF00";
	var color2 = GetRandomColor(); //"#FF0000";

	var noise = 0;
	this.x = layer1.width / 2;
	this.y = layer1.height - 20; //replace 20 with something more substation. Maybe starting thickness?
	this.speed = 4;
	this.speedIcr = -.04;
	this.angle = Noise(0.01);
	this.dir = -Math.PI / 2; //aka up...


	for (var i = 0; i < length; i++) {
		let progress = i / length;
		Go();
		Turn();
		DrawCircle2(layer1, this.x, this.y, ThicknessFunction(i), lerpColor(color1, color2, progress), 1);
	}
	return layer1;
} // Rename


var allStuff = [ //Without parameters
	GetWeirdRainbowSausageLayer,
	GetWeirdGameBoySausageLayer,
	GetAvocadoLayer,
	GetAvocadoWithCromaticLayer,
	GetAvocadoWithDoubleOutlineLayer,
	GetAvocadoStarLayer,
	GetPlantLayer,
	GetTheWitnessGrassLayer,
	GetStringLayer,
	GetShortStringLayer,
	GetPetal1Layer,
	GetPetal2Layer,
	GetGrasssssLayer,
	GetBubblePlantLayer,
	GetCircleGrassLayer,
	GetCircleLeafLayer
];

function GetRandomLayer() {
	return GetRandomEntryInArray(allStuff);
}

//Generic (drawing shapes out of shapes)

function GetStarLayer(texture, wings, offsetRotation) {
	var radius = texture.height;
	var layer1 = CreateNewLayer(radius * 2, radius * 2);
	for (var i = 0; i < wings; i++) {
		DrawSprite(layer1, texture, (i / wings) * 2 * Math.PI + offsetRotation, radius, radius, texture.width / 2, texture.height - 20, texture.height / 2);
	}


	return layer1;
}

function GetFanLayer(wings, angle, texture) {

	var radius = texture.height;
	var layer1 = CreateNewLayer(radius * 2, radius * 2);
	for (var i = 0; i < wings; i++) {
		//										Offset
		DrawSprite(layer1, texture, i * angle - (wings * angle) / 2, radius, radius, texture.width / 2, texture.height - 20, texture.height / 2);
	}


	return layer1;
}

//Unsorted

function Experiment() { //Rename into art experiment with appropriate name, put it under projects... or create a Experiments parent like Experiments.DrawingSpiral();
	FPS_LIMIT = 60;

	screenClearingColor = "#00885502"; //"pink";

	isScreenClearing = true;
	isWraparound = true;
	isUnitTest = false;

	var t = new thingDrawer(400, 400, GetAvocadoStarLayer);
	AddBehaviour(t, Go);
	AddBehaviour(t, Turn);
	t.speed = 5;
	t.speedIcr = .1;
	t.angle = .1;
	StartEngine();
}

function StringThing() {
	for (var i = 0; i < 10; i++) {
		DrawTextureOnce(canvas.centerPosition.x, canvas.centerPosition.y, GetShortStringLayer());
	}
}

function Grassmatta() { //TODO: redo into a texture or into a Draw this on texture instead generic draw on canvas. 
	for (var i = 0; i < 400; i++) {
		DrawTextureOnce(canvas.centerPosition.x + Noise(700), canvas.centerPosition.y + Noise(30), GetTheWitnessGrassLayer());
	}
}

function StringMatta() {
	for (var i = 0; i < 1000; i++) {
		DrawTextureOnce(canvas.centerPosition.x + Noise(700), canvas.centerPosition.y + Noise(30), GetStringLayer());
	}
}

function DrawTextureInSquare(texture, times, rect) {
	for (var i = 0; i < times; i++) {
		DrawTextureOnce(GetRandomRange(rect.x, rect.x + rect.width), GetRandomRange(rect.y, rect.y + rect.height), texture());
	}
}

function MakeAGrassMattaEveryFrame() {
	var t = new thingDrawer(0, 0, GetAvocadoLayer);
	console.log(t);
	t.Update = function () {};
	t.Draw = Grassmatta;
	StartEngine();
}

function WeirdThing() {
	entities.push(this);
	this.Update = function () {};
	this.Draw = function () {

		var layer1 = GetWeirdRainbowSausageLayer();

		for (var i = 0; i < 10; i++) {
			DrawSprite(layer1, GetRandomRange(0, 2 * Math.PI), GetRandomRange(100, 800), GetRandomRange(100, 800), 50, 50, 50);
		}
	}
	if (isUnitTest) log("Weird thing created!");
}

function WeirdThing2() {
	entities.push(this);
	this.Update = function () {};
	this.Draw = function () {

		var layer1 = GetWeirdSausageLayer();

		for (var i = 0; i < 10; i++) {
			DrawSprite(layer1, GetRandomRange(0, 2 * Math.PI), 100 + i * 32, 100, 50, 50, 50);
		}
	}
	if (isUnitTest) log("Weird thing created!");
}


// Laboration and experimentation
/*
Experiment();
//DrawTextureOnceInCenter(GetPlantLayer());
//DrawTextureOnceInCenter(GetAvocadoWithDoubleOutlineLayer());
//DrawTextureOnceInCenter(GetStringLayer());
//MakeAGrassMattaEveryFrame();
//StringMatta();
//StringThing();

//var fluff = new Fluff(100, "cloud"); // rename to SnakeSkinAndClouds
//var bush = new Bush(100, 100, 1900, 800); //rename to place circles in a square
//var w = WeirdThing();
//StartEngine();

DrawTextureOnceInCenter(GetCircleLeafLayer());
//PatternWithGet(10, 300, 250, GetCircleLeafLayer);

//DrawTextureOnceInCenter(GetStarLayer(9, GetCircleLeafLayer()));
//DrawTextureOnceInCenter(GetFanLayer(3, .2, GetCircleLeafLayer()));
var b = Bush(canvas.centerPosition.x, canvas.centerPosition.y, 200, 100);
StartEngine();

//FRACTALS
DrawTextureOnceInCenter(
	GetStarLayer(4,
		GetStarLayer(4,
			GetStarLayer(5,
				GetStarLayer(5,
					GetWeirdRainBowSausageLayer()
				)
			)
		)
	)
);

DrawTextureOnceInCenter(
	GetStarLayer(4,
		GetStarLayer(3,
			GetStarLayer(2,
				GetStarLayer(1,
					GetWeirdRainBowSausageLayer()
				)
			)
		)
	)
);

DrawTextureOnceInCenter(
	GetStarLayer(3,
		GetStarLayer(3,
			GetStarLayer(2,
				GetStarLayer(1,
					GetWeirdRainBowSausageLayer()
				)
			)
		)
	)
);
DrawTextureOnceInCenter(
	GetStarLayer(5,
		GetStarLayer(8,
			GetStarLayer(5,
				GetStarLayer(12,
					GetStarLayer(7,
						GetWeirdGameBoySausageLayer()
					)
				)
			)
		)
	)
);

DrawTextureOnceInCenter(
	GetStarLayer(Math.round(GetRandomRange(2,16)),
		GetStarLayer(Math.round(GetRandomRange(2,16)),
			GetStarLayer(Math.round(GetRandomRange(2,16)),
				GetStarLayer(Math.round(GetRandomRange(2,16)),
					GetStarLayer(Math.round(GetRandomRange(2,16)),
						GetWeirdGameBoySausageLayer()
					)
				)
			)
		)
	)
);

DrawTextureOnceInCenter(
	GetStarLayer(Math.round(GetRandomRange(2, 16)),
		GetStarLayer(Math.round(GetRandomRange(2, 16)),
			GetStarLayer(Math.round(GetRandomRange(2, 16)),
				GetStarLayer(Math.round(GetRandomRange(2, 16)),
					GetStarLayer(Math.round(GetRandomRange(2, 16)),
						GetWeirdRainBowSausageLayer()
					)
				)
			)
		)
	)
);

// End of Fractals


//DrawTextureOnceInCenter(GetPlantLayer());
//DrawTextureOnceInCenter(GetCircleLeafLayer());

//DrawTextureInSquare(GetCircleLeafLayer, 100, new MyRect(100, 100, 500, 200, "#000000", 0));
PatternWithGet(30, 250, 200, GetCircleLeafLayer());
PatternWithGet(30, 250, 200, GetRandomLayer);
*/
PatternWithGet(30, 200, 250, allStuff[11]);
//DrawTextureOnceInCenter(GetPetal2Layer());
//DrawTextureOnceInCenter(allStuff[11]());
