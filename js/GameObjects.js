//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* GameObjects.js */
/* File content description:
	Things that can exist in game world
*/ // File content description:
/* Document todo
	Add File content description
	Define the difference between GameObject and Entity
    make one set of game object per game module, just to separate each context/project and make things more overviewable and simple.
*/ // Document todo


//bias["botw"]["is"]["game"]

var basicShapes = { //Draw basic shapes as sprites - 5 min
	rect: 1,
	circle: 1,
	moon: 1,
	triangle: 1,
	etc: 1,
} //Move this to THE ALGORITHM instead, heh.


//leaf data (because it relates to mathematical concepts rather than bias??)


function mePos(x, y) {
	this.x = x
	this.y = y
}

function Entity(x = 0, y = 0, sprite, hitCircle) {
	if (!(this instanceof Entity)) return new Entity(x, y, sprite, hitCircle)
	this.x = x
	this.y = y
	this.sprite = sprite
	this.hitCircle = hitCircle
	this.Update = function () {}
	this.Draw = function (offset) {
		if (IsUndefined(this.sprite)) return
		DrawSprite(defaultLayer, this.sprite, this.dir, this.x, this.y, this.sprite.width * .5, this.sprite.height * .5, Math.max(this.sprite.width, this.sprite.height) * .5)
	}
	entities.push(this)
}
var temporarySprite = CreateNewLayer(2, 2) //Needed for plant nodes lol

var waterX = 0
var waterUpdate = new function () {
	this.Update = function (dt) {
		waterX += .002 * dt * .05
	}
	this.Draw = function (dt) {
		//water reflection stuff
		//    reflectionLayer.context.save()
		//    reflectionLayer.context.globalAlpha = 0.5
		//    reflectionLayer.context.scale(1, -1.12 - (camera.y / 1280))
		//    //            reflectionLayer.context.scale(1, -1)
		//    reflectionLayer.context.drawImage(canvas, 0, -canvas.height + camera.y * 2 - 400, canvas.width, canvas.height)
		//    reflectionPattern = context.createPattern(reflectionLayer, "no-repeat")
		//    reflectionLayer.context.restore()
	}
}
entities.push(waterUpdate)

var Curves = {
	Rock: function () {
		const groundLevel = canvas.height
		const z = 0 //hardcoded z value because we're simulating 1D space with the 2D noise because there was no 1D noise.
		return function (x) {
			return groundLevel - simp(x, z, 800, 0.0009, 1, 0) + simp(x, z + 500, 100, 0.0045, 1, 0) + simp(x, z + 4300, 30, 0.01, 1, 0)
		}
	}(),
	ShadowRock: function () {
		return function (x) {
			return Curves.Rock(x - 150) * 1.6 - 140
		}
	}(),
	Sand: function () {
		const groundLevel = canvas.height
		const z = 0 //hardcoded z value because we're simulating 1D space with the 2D noise because there was no 1D noise.
		return function (x) {
			//    x +=  waterX * 500 // move with water
			return groundLevel - simp(x, z, 400, 0.0009, 1, 0) //have multiple lines like this one to add wave function ontop of eachother
		}
	}(),
	Ground(x) { // the Y that you land on
		return Curves.Sand(x)
	},
	Dirt(x) {
		var groundLevel = 800
		var z = 0 //hardcoded z value because we're simulating 1D space with the 2D noise because there was no 1D noise.
		groundLevel -= simp(x + waterX * 500, z, 300, 0.0009, 1, 0) //have multiple lines like this one to add wave function ontop of eachother
		return groundLevel
	},
	Water(x) {
		if (!waterX) return
		var groundLevel = 800
		var z = waterX //hardcoded z value because we're simulating 1D space with the 2D noise because there was no 1D noise.
		groundLevel -= simp(x, z, 50, 0.0009, 1, 0) //have multiple lines like this one to add wave function ontop of eachother
		groundLevel -= simp(x + waterX * 500, z, 50, 0.005, 1, 0) //have multiple lines like this one to add wave function ontop of eachother
		return Math.round(groundLevel)
	},
	RockMaker: function () {
		var frequencyX = .004
		var frequencyY = .004
		return function (x, y) {
			return simp(x, y, .5, frequencyX, frequencyY, 0) + simp(x, y, .5, frequencyX * .5, frequencyY * .5, 0)
		}
	}(),
	flat: function () {
		return 1000
	}
} //These curves are essentially bias, but they are also sort of leaf functions. basically labeled leaf functions?


/* Wild ideas
   //meat body simulation
   //    this.bodyHeat
   //    this.muscleSoreness
   //    this.mood
   //    this.energy
*/


/*
//physic entity dependency
//physical forces
this.vx = 0
this.vy = 0


//Hit boxes and collisions
collisionContext = "outdoors" //use enums //TODO: rename collision context.


//rendering
this.Draw = function (o) {
    x += o.x
    y += o.y
}



// so like if player 
this.walkSpeed = (this.walkRight + this.walkLeft) * this.maxWalkSpeed
//physics update
vx = this.walkSpeed //apply input forces
vy += .5 //gravity falling
//physical forces (momentum acceleration etc)
x += vx * dt * .03
y += vy * dt * .03
//collision checks
var groundLevel
if (y > Curves.Water(x) - 40) {}
if (y >= groundLevel) { // if falling through ground, put yourself on ground and cancel any momentum
}
//collision state
if (this.collisionState === "outside") {} else if (this.collisionState === "indoors") {}
groundLevel = Curves.Ground.call(this, x) - this.legLength + 10
groundLevel = house.y
// water collision consequences for player
vy -= .5 //some sort of floating contant.
if (vy < -5) vy = -5
//        controlSet = waterControls

// collision consequences
y = groundLevel
vy = 0
this.jumps = this.maxJumps

//ground magnetism check and effect
if (Math.abs(vy) < 3 && y >= groundLevel - 20) { //when y velocity is lover than 3 and ground is 20 pixels close
    y = groundLevel
    vy = 0
}
//CAMERA FOLLOW
camera.x = x - .5 * canvas.width
camera.y = y - .75 * canvas.height
/* foot update stuff
var l = feet.length
for (var i = 0; i < l; i++) {
    feet[i].Update(dt)
}
//this only works if only two feet
var leftmostFoot = feet[0].x < feet[1].x ? feet[0] : feet[1]
this.bodyX = leftmostFoot.x + GetDistance(feet[0], feet[1]) / 2
*/

/*
// control and events
this.ControlOn = function (event) {
    if (event.repeat) return
    //event.code is generated by the browser use console.log(event) to figure out the right string
    if (event.code === "KeyZ") this.Jump()
    if (event.code === "KeyX") this.plant = true
    if (event.code === "Space") this.Jump()
    if (event.code === "ArrowRight") this.walkRight = 1
    if (event.code === "ArrowLeft") this.walkLeft = -1
    //                if (event.which == 37) this.Control("WalkLeft") // Left Arrow
    //                if (event.which == 39) this.Control("WalkRight") // Right Arrow
}
this.ControlOff = function (event) {
    if (event.code === "ArrowRight") this.walkRight = 0
    if (event.code === "ArrowLeft") this.walkLeft = 0
    if (event.code === "KeyX") this.plant = false
}
addEventListener("keydown", getFunctionCalledThroughTarget(this, this.ControlOn, Event))
addEventListener("keyup", getFunctionCalledThroughTarget(this, this.ControlOff, Event))
//actions / inputs check and consequence
if (this.plant) {
    this.Plant()
}

// properties
this.walkRight = 0
this.walkLeft = 0

this.walkSpeed = 0
this.maxWalkSpeed = 5
this.maxJumps = 2
this.jumps = 2
this.jumpForce = 10
//actions
this.Jump = Jump
//by assigning a local variable this.x you make an abstraction for x
this.Plant = function () {
    //        if (Math.random() < .2) {
    var plant = new e.botw.PlantNode(0, 0)
    plant.x = Noise(20) + this.x
    plant.y = Noise(30) + this.y + this.legLength - 10
    //        }
}
//add ground slipperyness and added slope velocity
function Player(x, y, groundF) {
    this.Update = function (dt) {}
    // if (getGroundLevel(x-5) < Curves.Ground(x)+5) //if ground position five pixels left is 5 pixels down, start accelerate (you should calculate the angle to base the amount of acceleration on)
    entities.push(this)
}
//foot
//shape
this.width = 20
this.height = 5
//    style
this.color = this.body.color
var Foot = function (x, y) {
    mePos(x, y)
    //context for movement
    this.isRightFoot = isRightFoot
    this.getCircleCurveIntersection = getCircleCurveIntersection
    this.targetPosition = {
        x: 1,
        y: 1
    }

    //functions
    this.Update = function (dt) {
        var bd = this.body
        //while standing still
        //            if (isRightFoot) {
        //                this.x = bd.x + bd.width * .5
        //            } else {
        //                this.x = bd.x - bd.width * .5
        //            }
        //            this.y = Curves.ground(this.x)

        if (bd.y + bd.legLength < this.Curves.Ground.call(this, bd.x)) { // Jumping
            this.target.x = bd.x
            this.target.y = bd.y + bd.legLength
        } else if (bd.vx != 0) { // moving in x
            var r = bd.legLength
            bd.footReachCircle.x = bd.x
            bd.footReachCircle.y = bd.y

            var bodyFeetDistance = GetDistance(bd, this)
            if (bodyFeetDistance > r) {
                //find next foot position target
                var isGoToRight = (this.x < bd.x) ? true : false
                this.target = this.getCircleCurveIntersection.call(this, bd.footReachCircle, Curves.Ground, false, isGoToRight, 100, .1)
            }
        } else { //standing still
            var p = this.isRightFoot ? 1 : -1
            this.target.x = bd.bodyX + p * bd.legLength / 10
            this.target.y = this.Curves.Ground.call(this, this.target.x)
        }
        if (this.target.x && this.target.y)
            this.MoveTowards.call(this, this, this.target, .01 * dt, 0, -1)
    }
    this.Draw = function (offset) {
        var o = offset
        var x = this.x + o.x
        var y = this.y + o.y
        var bd = this.body
        var b = {
            x: bd.bodyX + o.x, //why do I even have body.bodyX?? feels confusing.
            y: bd.y + o.y
        }
        var width = this.width
        var height = this.height
        var color = this.color
        var ctx = this.ctx
        //            DrawRect(x - width * .5, y - height * .5, width, height, color)
        DrawCircle.call(this, defaultLayer, x, y, 3, color, 1)


        //leg lines
        var p = this.isRightFoot ? 1 : -1
        ctx.beginPath()
        //            ctx.moveTo(b.x + p * bd.width / 3, b.y - 10)
        ctx.moveTo(b.x, b.y - 50)
        ctx.lineTo(x, y)
        ctx.lineWidth = 5
        ctx.strokeStyle = "#aaaaaa"
        ctx.stroke()
        //Foot target debug draw
        //            var tx = this.target.x
        //            var ty = this.target.y
        //            DrawRect(tx - 5, ty - 5, 10, 10, "#ff000033")
    }
}
*/

var WorldSets = {
	beach: [
        GetCurveLayer(Curves.Rock, "#778899", 128),
            GetCurveLayer(Curves.Sand, "#ffff66"),
            //    GetCurveLayer(Curves.Sand, "#ffff66"),
            //    GetCurveLayer(Curves.Water, "reflection"), // reflection is really broken. I don't know how to scale the reflection layer right, it is performance intensive and makes the screen blackout. also does not work with css background.
            //    GetCurveLayer(Curves.Water, "#3388FF99"),
            GetCurveLayer(Curves.Water, "#3388bb66"),
        //    GetCurveLayer(Curves.Dirt, "#447700"),
        ],
	snow: [
        GetCurveLayer(Curves.Rock, "#ddddFF", 128),
        GetCurveLayer(Curves.ShadowRock, "#99bbFF", 128),
        GetCurveLayer(Curves.flat, "#ddddff"),
        ],
}

function House() {
	//A house is a collection of connected compartments.
	basicShapes = []
	Interior
	Exterior
	randomSeed //(to save data you always generate the house sprite instead of saving the sprite to harddrive)
}

function Compartment(x, y, width, height) {
	// A compartment is an object with an exterior and an interior space
	// Currently only supporting axis aligned rectangles.

	this.sortBottom = 1 //to get Drawn behind all the stuff!

	this.interiorLayer = CreateNewLayer(width, height, .5 * width, height)
	this.exteriorLayer = CreateNewLayer(width, height, .5 * width, height)
	this.renderingState = renderingStates.INSIDE
	this.x = x
	this.y = y

	this.fadeProximity = 1

	this.generateInterior = function () {
		var el = this.interiorLayer
		DrawRect(el, 0, 0, el.width, el.height, "#111100")
	}

	this.Draw = function (offset) {
		var o = {
			x: offset.x + this.x,
			y: offset.y + this.y
		}
		/* silly optimisations
		if (this.renderingState == renderingStates.OUTSIDE) {
		    this.exteriorLayer.Draw(o)
		    //TODO: loop through shapes to blit draw from interior layers, like open windows and doors, etc.
		} else if (this.renderingState == renderingStates.INSIDE) {
		    this.interiorLayer.Draw(o)
		    //TODO: darken the world when inside. // Maybe should be done in player instead, since must know if inside anyways for collision.
		    //TODO: loop through shapes to blit draw from outside world, like open windows and doors, etc.

		} else if (this.renderingState == renderingStates.FADING) {
		    context.globalAlpha = 1
		    this.interiorLayer.Draw(o)
		    context.globalAlpha = this.fadeProximity
		    this.exteriorLayer.Draw(o)
		    context.globalAlpha = 1

		}
		*/ // silly optimisations

		//Unoptimized but easy
		//                context.globalAlpha = 1
		this.interiorLayer.Draw(o)
		var l = this.interiorPieces.length
		for (var i = 0; i < l; i++) {
			this.interiorPieces[i].Draw(o)
		}
		context.globalAlpha = this.fadeProximity
		this.exteriorLayer.Draw(o)
		l = this.exteriorPieces.length
		for (var i = 0; i < l; i++) {
			this.exteriorPieces[i].Draw(o)
		}
		context.globalAlpha = 1
	}
	this.fadeInMaxDistance = 100
	this.fadeInMinDistance = 50

	this.Update = function () {
		if (!player) return
		if (player.collisionState === "indoors") return
		//find the closest hole object
		var l = this.holePieces.length
		if (!l) return
		var indexClosest = 0
		for (var i = 0; i < l; i++) {
			if (GetDistance(player, this.holePieces[i]) < GetDistance(player, this.holePieces[indexClosest])) {
				indexClosest = i
			}
		}
		var closestHole = this.holePieces[indexClosest]

		var leftEdge = this.x + closestHole.x - .5 * closestHole.holeTexture.width
		var rightEdge = leftEdge + .5 * closestHole.holeTexture.width
		var upperEdge = this.y - closestHole.holeTexture.height
		var lowerEdge = this.y

		var playerCircle = {
			x: player.x,
			y: player.y,
			radius: this.fadeInMaxDistance
		}
		var myBoundingBox = {
			x: leftEdge,
			y: upperEdge,
			width: rightEdge - leftEdge,
			height: lowerEdge - upperEdge
		}
		var isClose = Math.round(circleAABBCollisionCheck(playerCircle, myBoundingBox))


		if (player.x > leftEdge &&
			player.x < rightEdge &&
			player.y > upperEdge &&
			player.y < lowerEdge) {
			this.renderingState = renderingStates.INSIDE
			player.collisionState = "indoors"
		} else if (isClose) {
			this.renderingState = renderingStates.FADING

			this.fadeProximity = 1 - normalize(isClose, (this.fadeInMinDistance * this.fadeInMinDistance), (this.fadeInMaxDistance * this.fadeInMaxDistance))
		} else {
			//            console.log("on the outside")

			this.renderingState = renderingStates.OUTSIDE
		}
	} // because entity.. TODO make this not an entity... , unless it would move.... but I want to separate things that on draw. Like if there's no update, then ignore trying to update.
	Textures.BrickTexture(this.exteriorLayer)
	//            Textures.VerticalPlankTexture(this.exteriorLayer)
	this.generateInterior()

	this.interiorPieces = []
	this.exteriorPieces = []
	this.holePieces = []

	this.AddWallPiece = function (wall, piece) {
		if (wall === "interior") {
			this.interiorPieces.push(piece)
		} else if (wall === "exterior") {
			this.exteriorPieces.push(piece)
		}

		if (piece.holeTexture) {
			this.holePieces.push(piece)
			if (wall === "interior") {
				this.interiorLayer = SubtractTexture(this.interiorLayer, piece.holeTexture, this.interiorLayer.pivotPoint.x + piece.x, this.interiorLayer.pivotPoint.y + piece.y)
			} else if (wall === "exterior") {
				this.exteriorLayer = SubtractTexture(this.exteriorLayer, piece.holeTexture, this.exteriorLayer.pivotPoint.x + piece.x, this.exteriorLayer.pivotPoint.y + piece.y)
			}
		}
	}
	/*silly optimisation thing to only draw one texture....
	    // Get blit of background behind door
	    var translatedDoorCoords = {
	        x: this.door.x - this.door.texture.width / 2,
	        y: this.exteriorLayer.height - this.door.texture.height
	    }
	    var behindTheDoorTexture = this.interiorLayer.context.getImageData(translatedDoorCoords.x, translatedDoorCoords.y, this.door.texture.width, this.door.texture.height) //get the blit from interior layer
	    this.exteriorLayer.context.putImageData(behindTheDoorTexture, translatedDoorCoords.x, translatedDoorCoords.y)
	*/ //silly optimisation thing to only draw one texture....

	entities.push(this)
	//TODO in future: non-neuclidian compartments (the inside and outside space are different.)
}

function Door(x, y, width, height) {
	this.x = x
	this.y = y
	this.texture = CreateNewLayer(width, height, .5 * width, height)
	DrawRect(this.texture, 0, 0, width, height, "red")
	this.Draw = function (offset) {
		offset.x += this.x
		offset.y += this.y
		this.texture.Draw(offset)
	}
	this.holeTexture = this.texture
}

function RoundWindow(x, y, r) {
	this.x = x
	this.y = y
	this.texture = CreateNewLayer(2 * r, 2 * r, r, r)
	Draw["Circle"](this.texture, r, r, r, "blue", .1)
	this.holeTexture = CreateNewLayer(2 * r, 2 * r, r, r)
	Draw["Circle"](this.holeTexture, r, r, r, "black", 1)
	this.Draw = function (offset) {
		offset.x += this.x
		offset.y += this.y
		this.texture.Draw(offset)
	}
}

//if it's thin enough it's translucent
//a tree is a plant that grows large with the biggest mass in its stem.
function PlantNode(relativeRotation, relativeDistance) {


	// Settings and performance tests
	this.spriteExperiment = 2 //Defines the current experiment. Different experiments use different approaches.
	if (this.spriteExperiment == 1) { //Fuck double sprites are even slower..
		//this.sprite2 = CreateNewLayer(2, 2)
		this.isUsingSprite1 = true
	}
	this.isRoudingPositions = false // Tried rounding the positions in an attempt to improve performance, but I think it's ugglier..
	entities.push(this)
	this.sprite = CreateNewLayer(2, 2) //For the auto-scaling
	/* Manually tested scalings: aka non dynamic scaling
	    this.sprite = CreateNewLayer(32, 32)
	    this.sprite = CreateNewLayer(64, 64)
	    this.sprite = CreateNewLayer(128, 128)
	    this.sprite = CreateNewLayer(256, 256)
	    this.sprite = CreateNewLayer(512, 512)
	    this.sprite = CreateNewLayer(1024, 1024)
	    this.sprite = CreateNewLayer(2048, 2048)

	    //funky ones
	    this.sprite = CreateNewLayer(129, 257)
	    this.sprite = CreateNewLayer(256, 512)
	*/

	//Rendering option
	this.sprite.context.globalCompositeOperation = 'destination-over' //New shapes are drawn behind the existing canvas content.

	//Plant properties
	this.x = 200
	this.y = 200 //Where plant is rendered


	this.originPosition = {
		x: .5 * this.sprite.width,
		y: 2
	} // The root and pivot point of the branch
	this.startPosition = {
		x: .5 * this.sprite.width,
		y: 2
	} // The tip and ending point of the branch. to calculate where the tip of plant is.

	this.maxGrowth = GetRandomRange(5, 40)
	//    this.maxGrowth = 400
	this.scale = 1
	this.progress = 0
	this.relativeRotation = 0

	this.color = GetRandomColorInGradient("#00ff00", "#008800") //GetRandomColor() //"#D4FF00"//GetRandomColorInGradient("#FFff00", "#DDDD00")//ColorF.Example(this.progress)//GetRandomColorInGradient("#FFff00", "#000000")//
	//

	this.nodes = [] //branches etc

	//FUNCTIONS
	this.AddBranchNode = function (node) {
		node.parent = this
		this.nodes.push(node)
	}
	this.SplitBranchSprite = function () {

		var dumpBranch = new e.botw.PlantNode(0, 0)

		//Save the current sprite and branch to separate sprite and branch
		dumpBranch.sprite = this.sprite
		dumpBranch.x = this.x
		dumpBranch.y = this.y
		dumpBranch.scale = this.scale
		dumpBranch.progress = this.progress
		dumpBranch.relativeRotation = this.relativeRotation //I don't know why this should be negative.... but hey it works.
		dumpBranch.originPosition = this.originPosition
		dumpBranch.startPosition = this.startPosition
		dumpBranch.nodes = this.nodes

		dumpBranch.Update = function () {} //castration
		dumpBranch.Draw = function () {} //castration




		//Reset current sprite and branch to a default empty sprite and branch
		this.sprite = CreateNewLayer(2, 2)
		this.relativeRotation = 0
		this.originPosition = {
			x: .5 * this.sprite.width,
			y: 2
		}
		this.startPosition = {
			x: .5 * this.sprite.width,
			y: 2
		}
		this.nodes = []
		this.AddBranchNode(dumpBranch)

	}
	this.addFollowRootBranch = function () {
		var followBranch = new PlantNode(0, 0)
		followBranch.x = this.x
		followBranch.y = this.y
		followBranch.Update = function () {
			console.log("my progress", this.progress)
			if (this.progress < this.maxGrowth)
				this.Grow(GetRandomRange(-.1, .1), 3, 10)
		}
		this.AddBranchNode(followBranch)
	}
	this.splittingAtRotation = function () { //todo put .52 and .48 so they are just .02 as parameter for this function
		//This function is supposed to be a performance solution.
		//if the bending angle becomes too large then then the sprite, since it is a square, will leave room for many redundant empty pixels.
		//My theory is that if I split the sprite in to sub sprites whenever the branch doesn't grow straight, then I'll create only straight sprites which minimizes memory usage of transparent pixels.

		//because of how logic works with < on angles below zero
		if (this.relativeRotation < .97 * 2 * Math.PI && this.relativeRotation > .03 * 2 * Math.PI) {
			this.SplitBranchSprite()
		}
	}
	this.Grow = function (addedRotation = 0, addedDistance = 0, thickness = 10) { //add a sprite parameter, if you want to draw more than circles... // add a scale variable to change scale
		this.progress++

		var newRotation = addedRotation /= 1 + (this.progress / 50) //Increases stability as progress is made
		this.relativeRotation = SetAngle(this.relativeRotation + newRotation) //add rotation and make sure it's within 2*PI becaue anle 0 == angle 2*PI

		//Branching / splitting
		this.splittingAtRotation()

		//controlled branching
		//        if (this.progress == 30) this.addFollowRootBranch()

		var targetPosition = { //New position where to put down the next circle and set the new root/origin
			x: this.originPosition.x + Math.cos(this.relativeRotation + .5 * Math.PI) * addedDistance, // + .5 * Math.PI is to compensate for the fact that angle 0 is to the right
			y: this.originPosition.y + Math.sin(this.relativeRotation + .5 * Math.PI) * addedDistance
		}

		var circleRadius = this.progress / 50 + 2 // minimum radius of 2 + grows by 1/50px per unit of progress

		// if(circleRadius > 3) circleRadius = Math.round(circleRadius)
		//           var circleColor =   this.color//"#ffffff" //this.color// "#ffff33"//GetRandomColor() //
		this.color = changeHue(this.color, 1)
		var circleColor = this.color //"#ffffff" //this.color// "#ffff33"//GetRandomColor() //
		// color changing/shading algo here
		// this.sprite = GetFilteredImage(this.sprite, "contrast",	"1000%")

		var circleAlpha = 1

		var oldSprite, newSprite
		if (this.spriteExperiment == 1) {
			if (this.isUsingSprite1) {
				oldSprite = this.sprite
				newSprite = this.sprite2
				this.isUsingSprite1 = false
			} else {
				oldSprite = this.sprite2
				newSprite = this.sprite
				this.isUsingSprite1 = true
			}
		} else if (this.spriteExperiment == 2) {
			oldSprite = this.sprite
			newSprite = temporarySprite
		} else if (this.spriteExperiment == 0)
			oldSprite = this.sprite

		//SCALING SPRITE
		//Attempt to dynamically "Scale" layers. aka create new ones all the time that match the size and copy paste old content on it.
		var safetyRadius = circleRadius //+100// +2

		var newWidth = oldSprite.width
		var newXPositionOfOldSprite = 0

		if (targetPosition.x + safetyRadius > newWidth) {
			newWidth = Math.ceil(targetPosition.x + safetyRadius) //If things are to the right, expand to the right.
		}

		if (targetPosition.x - safetyRadius < 0) { //If things are to the left
			var offset = Math.ceil(0 - (targetPosition.x - safetyRadius))
			newWidth += offset //image is scaled to the right by the defined margin, and ceiled to even out pixels.
			newXPositionOfOldSprite = offset //already drawn stuff is put to the right, by the amount it was moved.
			targetPosition.x += offset //The place to draw is also moved by the same amount
			this.startPosition.x += offset //... so is the tip or starting point of the branch
		}

		var newHeight = oldSprite.height
		var newYPositionOfOldSprite = 0
		if (targetPosition.y + safetyRadius > newHeight) {
			newHeight = Math.ceil(targetPosition.y + safetyRadius) //If things are to the right, expand to the right.
		}

		if (targetPosition.y - safetyRadius < 0) {
			var offset = Math.ceil(0 - (targetPosition.y - safetyRadius))
			newHeight += offset
			newYPositionOfOldSprite = offset
			targetPosition.y += offset
			this.startPosition.y += offset
		}

		if (this.spriteExperiment == 1 || this.spriteExperiment == 2) {
			newSprite.width = newWidth
			newSprite.height = newHeight
		} else if (this.spriteExperiment == 0)
			newSprite = CreateNewLayer(newWidth, newHeight)



		var ctx = newSprite.context
		ctx.save()
		//	ctx.translate(newXPositionOfOldSprite, newYPositionOfOldSprite) // tried this but it didn't change much....

		ctx.drawImage(oldSprite, newXPositionOfOldSprite, newYPositionOfOldSprite)
		ctx.restore()


		if (this.spriteExperiment == 0)
			this.sprite = newSprite
		else if (this.spriteExperiment == 2) {
			var temp = this.sprite
			this.sprite = newSprite
			temporarySprite = temp
		}

		if (this.isRoundingPositions) {
			targetPosition.x = Math.round(targetPosition.x)
			targetPosition.y = Math.round(targetPosition.y)
		}


		this.sprite.context.globalCompositeOperation = 'destination-over' //New shapes are drawn behind the existing canvas content. // unsure how this affects performance
		DrawCircle(this.sprite, targetPosition.x, targetPosition.y, circleRadius, circleColor, circleAlpha)









		/* mission keep track of where to draw branches

		since branches grow, the tip point must be kept track of and be accurate even when scaling the image in pixel.
		I can't just track distance, because the only way to calculate distance is to have to points to track the difference in the first place.
		Thus I must use a point and it needs to be adjusted the same way as "targetPosition"

		plan:
		1. Look into and remember how I decide the target position
		2. Visualize in your mind how this will work
		3. Figure out a solution

		*/









		//TODO:
		//put branching algo here


		this.originPosition = {
			x: targetPosition.x,
			y: targetPosition.y
		}
		this.sprite.pivotPoint = this.originPosition
		//console.log("my position is now:", this.originPosition)


	}

	this.fullBendAmount = .5 * Math.PI
	this.bending = 0

	this.Update = function () {

		//bend stuff
		var myDistance = GetDistance(this, player)
		var maxDistance = player.footReachCircle.r * 1.35
		var minDistance = player.footReachCircle.r * .75

		if (myDistance < maxDistance) {
			this.bending = this.fullBendAmount * normalize(myDistance, maxDistance, minDistance)
			//this.bending = this.fullBendAmount * normalize(myDistance, minDistance, maxDistance) //this was a pretty cool thing. the grass stand up when you go near. I wanna use for something somewhere.
			if (this.x > player.bodyX) this.bending *= -1

		}

		//growth stuff
		if (Math.random() < .8 + 2 * this.progress / 1000) return //Do nothing, at random. Makes growth feel a bit more dynamic/organic. Might help a bit with performance by reducing draw calls as well!
		if (this.progress < this.maxGrowth)
			this.Grow(GetRandomRange(-.1, .1), 3, 10)
		//        else this.Update = function () { }
	} //This update contains rendering... I'm not sure if performance improves if this is also at Draw call.



	this.DrawOld = function (originX, originY, inheritedRotation = 0, inheritedDistance = 0) {
		var targetedRotation = this.relativeRotation + inheritedRotation
		if (inheritedDistance > 0) {
			var targetPosition = {
				x: originX + Math.cos(targetedRotation + Math.PI) * inheritedDistance,
				y: originY + Math.sin(targetedRotation + Math.PI) * inheritedDistance
			}
			console.log(targetPosition)
		} else {
			var targetPosition = {
				x: this.x,
				y: this.y
			}
		}

		var texture = this.sprite
		DrawSprite(defaultLayer, texture, -targetedRotation - Math.PI / 2, Math.round(targetPosition.x), Math.round(targetPosition.y), texture.pivotPoint.x, texture.pivotPoint.y, texture.height * .5)
		//DrawTextureOnceInCenter(texture) // alternative way to draw for debug

		//Draw all child nodes!
		for (var i = 0; i < this.nodes; i++) {
			this.nodes[i].DrawOld(targetPosition.x, targetPosition.y, targetedRotation, 100)
		}
	}
	this.DrawNew = function (x = 0, y = 0, angle = 0) {
		//DrawSprite parameters
		var targetLayer = defaultLayer
		var texture = this.sprite
		var rotation = SetAngle(-(this.relativeRotation + this.bending + angle))
		var pos = {
			x: this.x + x,
			y: this.y + y
		}
		var pivotPoint = texture.pivotPoint
		//

		// Debug Draw bounding box
		//            texture.context.fillStyle = "#000000"
		//            texture.context.fillRect(0, 0, texture.width, texture.height)
		//


		DrawSprite(targetLayer, texture, rotation, pos.x, pos.y, pivotPoint.x, pivotPoint.y)

		if (this.nodes.length == 0) return
		// Debug draws
		//			DrawCircle(targetLayer, pos.x, pos.y, 2, "red")
		//			DrawCircle(targetLayer, pos.x - this.startPosition.x, pos.y - this.startPosition.y, 2, "blue")
		//			DrawCircle(targetLayer, pos.x - this.originPosition.x, pos.y - this.originPosition.y, 2, "yellow")
		//DrawCircle(targetLayer, pos.x - pivotPoint.x, pos.y - pivotPoint.y, 2, "pink")
		//


		// Figure out what the different variables represent
		//	startPosition, originPosition, pivotPoint,
		// Origin position is the pivotPoint of the sprite and also where the root of the branch is
		// StartPosition is at the top of the branch where the branch ends. It's where it all started growing from.



		// Set up variables for the next branch
		angle = SetAngle(angle + this.relativeRotation + this.bending)


		//these things are fucking hacks. I don't know what I'm doing. All I know is that I want to offset the position so that it draws where the last sprite ends.
		//            var distance = GetDistance(this.originPosition, this.startPosition)
		//            x += Math.cos(angle + .5 * Math.PI) * distance
		//            y -= Math.sin(angle + .5 * Math.PI) * distance




		//my goal is to get the accurate absolute x and y by first calculating the relative x and y, of where the tip of the current branch is
		// and then adding it to the absolute x and y, represented by the absolute root position of the parent branch


		var point = this.startPosition
		var center = this.originPosition

		// By rotating startposition around originPosition, I get rotation fixed position of the tip of the current branch.
		var rotatedX = Math.cos(-angle) * (point.x - center.x) - Math.sin(-angle) * (point.y - center.y) + center.x
		var rotatedY = Math.sin(-angle) * (point.x - center.x) + Math.cos(-angle) * (point.y - center.y) + center.y

		// I then get the relative rotation fixed position by removing the absolute position of the origin
		var newX = rotatedX - this.originPosition.x
		var newY = rotatedY - this.originPosition.y

		// the relative position of this tip is added the absolute position of the last tip
		x += newX
		y += newY

		//Draw all child nodes!
		for (var i = 0; i < this.nodes.length; i++) {
			this.nodes[i].DrawNew(x, y, angle)
			//            this.nodes[i].DrawNew(200, 0, 0, 0)
		}
	}
	this.Draw = function (offset) {
		var o = offset
		//       this.DrawOld(originX, originY, inheritedRotation = 0, inheritedDistance = 0)
		this.DrawNew(o.x, o.y, 0, 0)
	}
}



var e = {
	botw: {
		Player: Player,
		RoundWindow: RoundWindow,
		PlantNode: PlantNode,
		Door: Door,
		Compartment: Compartment,
	},
	things: {
		Slinky: function (x, y, color, thickness, length, elasticity, modifier) { //Stacks of circles
			entities.push(this)

			x = DefaultParameter(x, 0)
			y = DefaultParameter(y, 0)
			color = DefaultParameter(color, "#ffaa00")
			thickness = DefaultParameter(thickness, 10)
			length = DefaultParameter(length, 10)
			elasticity = DefaultParameter(elasticity, .7)
			modifier = DefaultParameter(modifier, "")

			this.color = color
			this.circles = []
			for (var i = 0; i < length; i++) {

				var color = this.color //rgbToHex(255,200,0)
				var alpha = 1 //.2
				//var radius = Math.pow((100-i),1)
				if (modifier == "bigger")
					var radius = i * (thickness / length)
				else if (modifier == "smaller")
					var radius = length - i * (thickness / length)
				else
					var radius = thickness

				var circle = new Circle(0, 0, radius, color, alpha)
				if (i > 0) {
					var target = this.circles[this.circles.length - 1]
					if (!target) throw new Error('This target is negative!? ' + target)
					circle.target = target
					AddBehaviour(circle, MoveTowardsTarget)
				}
				this.circles.push(circle)
			}
			Object.defineProperties(this, {
				"target": {
					"set": function (val) {
						this.circles[0].target = val
					},
					"get": function () {
						return this.circles[0].target
					}
				},
				"behaviour": {
					"set": function (val) {
						this.circles[0].behaviour = val
					},
					"get": function () {
						return this.circles[0].behaviour
					}
				},
				"x": {
					"get": function () {
						return this.circles[0].x
					},
					"set": function (val) {
						this.circles[0].x = val
					}
				},
				"y": {
					"get": function () {
						return this.circles[0].y
					},
					"set": function (val) {
						this.circles[0].y = val
					}
				},
				"back": {
					"get": function () {
						return this.circles[this.circles.length - 1]
					}
				}
			})
			this.circles[0].x = x
			this.circles[0].y = y

			this.length = length
			this.elasticity = elasticity
			AddBehaviour(this, function () {
				for (var i = 1; i < this.circles.length; i++) { //Skips first circle because it should behave uniquely
					this.circles[i].Update()
				}
				//this.circles.forEach(function(circle) {circle.Update()})
				//Set position to first circle
				this.x = this.circles[0].x
				this.y = this.circles[0].y
			})
			this.Draw = function () {
				this.circles.forEach(function (circle) {

					circle.Draw()
				})
			}
		},
		//Generated circle art
		Bush: function (x, y, width, height) {
			entities.push(this)
			this.rect = new MyRect(x, y, width, height)
			this.leaves = []
			var r = Math.round(GetRandomRange(100, 230))
			var g = Math.round(GetRandomRange(100, 255))
			var b = Math.round(GetRandomRange(50, 100))
			var colorA = "#FFBBBB"
			var colorB = "#BBBBFF" //pink blue clouds
			//var colorA = "#FF8800" var colorB = "#00FFFF" //Orange, sky blue?

			var leaves = 500 //30
			for (var i = 0; i < leaves; i++) {
				//var color = rgbToHex(r + Math.round(GetRandomRange(-10, 10)), g + Math.round(GetRandomRange(-10, 30)), b + Math.round(GetRandomRange(-30, 30)))
				var color = "#C1FF00" //GetRandomColorInGradient(colorA, colorB)
				var bushPoint = GetRandomPointInRect(this.rect)
				var leaf = new RoundLeaf(bushPoint.x, bushPoint.y, GetRandomRange(.01, .8), color, 1)
				leaf.Draw()
				this.leaves.push(leaf)
			}
			this.Update = function () {}
			this.Draw = function () {
				this.leaves.forEach(function (leaf) {
					leaf.Draw()
				})
			}
		},
		Cloud: function (x, y, width, height) {
			entities.push(this)
			this.rect = new MyRect(x, y, width, height)
			this.leaves = []
			var r = Math.round(GetRandomRange(100, 230))
			var g = Math.round(GetRandomRange(100, 255))
			var b = Math.round(GetRandomRange(50, 100))
			var colorA = "#FFBBBB"
			var colorB = "#BBBBFF" //pink blue clouds
			//var colorA = "#FF8800" var colorB = "#00FFFF" //Orange, sky blue?

			var leaves = 30
			for (var i = 0; i < leaves; i++) {
				//var color = rgbToHex(r + Math.round(GetRandomRange(-10, 10)), g + Math.round(GetRandomRange(-10, 30)), b + Math.round(GetRandomRange(-30, 30)))
				var color = GetRandomColorInGradient(colorA, colorB)
				var bushPoint = GetRandomPointInRect(this.rect)
				var leaf = new RoundLeaf(bushPoint.x, bushPoint.y, GetRandomRange(4, 14), color, 1)
				leaf.Draw()
				this.leaves.push(leaf)
			}
			this.Update = function () {}
			this.Draw = function () {
				this.leaves.forEach(function (leaf) {
					leaf.Draw()
				})
			}
		},
		Tree: function () {
			entities.push()
		},
		RoundLeaf: function (x, y, size, color, alpha) {
			this.size = size
			Circle.call(this, x, y, 5 * size, color, alpha)
		},
		Fluff: function (scale, state) { // TODO: Define what is, can't remember...
			entities.push(this)
			this.x = 100
			this.y = 800
			this.multiplier = scale
			this.circles = []
			for (var i = 0; i < 25; i++) {
				for (var j = 0; j < 25; j++) {
					var xpos = this.x + j * this.multiplier
					var ypos = this.y - i * (this.multiplier / 4)
					if (i % 2 == 0) xpos += this.multiplier / 2
					xpos += GetRandomRange(0, 5)
					ypos += GetRandomRange(0, 5)

					switch (state) {
						case "green":
							var circle = new Circle(xpos, ypos, this.multiplier / 2, rgbToHex(j * 10, i * 10, 0), 1) // green, gred
							break
						case "blue":
							var circle = new Circle(xpos, ypos, this.multiplier / 2, rgbToHex(255 - j * 10, 255 - i * 10, 255), 1) // pink scales
							break
						case "cloud":
							var circle = new Circle(xpos, ypos, GetRandomRange(this.multiplier / 2, this.multiplier), rgbToHex(100 + i * 4, 100 + i * 4, 100 + i * 4), 1) // upside down cloud
						default:
							break
					}

					circle.behaviours = [Go, Turn]

					circle.speed = i * .1
					circle.angle = i * .001
					//circle.angle = .01

					circle.speed += GetRandomRange(0, 0.5)

					this.circles.push(circle)
				}
			}
			this.Update = function () {
				for (var i = 0; i < this.circles.length; i++) {
					//this.circles[i].Update()
				}
			}
			this.Draw = function () {
				for (var i = 0; i < this.circles.length; i++) {
					this.circles[i].Draw()
				}
			}
		},
	},
	grid: { // Grid game 
		GridEntity: function (x, y, color) {
			entities.push(this)
			var me = this
			this.x = x
			this.y = y
			this.color = color
			this.moveInterval = 30 //Frame based
			//this.moveInterval = 500 //time based
			this.moveTimer = 0

			me.action = "nothing"
			this.actions = []
			this.Update = function (dt) {
				/*
		if (this.life <= 0) {
			return false //dead
		}
        */
				//this.moveTimer += dt //time based
				this.moveTimer++ //frame based
				if (this.moveTimer > this.moveInterval) {
					this.color = GetRandomColor()
					this.moveTimer -= this.moveInterval
					console.log(this.action)
					switch (this.action) {
						case "move up":
							this.y--
							break
						case "move down":
							this.y++
							break
						case "move left":
							this.x--
							break
						case "move right":
							this.x++
							break
					}
					this.action = "nothing"
				}
				return true
			}
			this.Draw = function () {
				/* FIX THIS: tilesize is undefined -> */
				DrawRect(this.x * tileSize, this.y * tileSize, tileSize, tileSize, this.color)
				//DrawRect(this.x, 0, 100, 100, this.color)

			}

			this.Control = function (event) {
				//since this function is called from outside and not called in the format "GridEntity.Control()", I have to make a new reference to the object with "me"
				me.action = event.value
				//this.actions.push(event)
			}

			Subscribe(this.Control, "controller-0")
		},
		GridEntity2: function (x, y, color) { //???? Sync error?
			this.life = 1
			this.x = x
			this.y = y
			this.color = color
			this.moveInterval = 60 //frame based
			//this.moveInterval = 1000 //time based
			this.moveTimer = 0
			this.action = "nothing"
			this.Update = function (dt) {
				//this.moveTimer += dt //time based
				this.moveTimer++ //frame based
				//Check if dead
				if (this.life <= 0) {
					return false //dead
				}
				return true
			}
			this.ShouldSynchronize = function () {
				if (this.moveTimer >= this.moveInterval)
					return true
				else
					return false
			}
			this.Synchronize = function () {
				this.moveTimer -= this.moveInterval

				if (this.action == "nothing") {
					//var behaviour = Math.round(Math.random()*5)
					var behaviour = 2
					switch (behaviour) {
						case 1:
							this.y--
							break
						case 2:
							this.y++
							break
						case 3:
							this.x--
							break
						case 4:
							this.x++
							break
					}
				} else {
					switch (this.action) {
						case "move up":
							this.y--
							break
						case "move down":
							this.y++
							break
						case "move left":
							this.x--
							break
						case "move right":
							this.x++
							break
					}
					this.action = "nothing"
				}
			}
			this.Draw = function () {
				/* FIX THIS: tilesize is undefined -> */ //DrawRect(this.x * tilesize, this.y * tilesize, tilesize, tilesize, this.color)
			}
		},
		// Optimization game
		GridPlayer: function (x, y, color) {
			entities.push(this)
			var me = this
			this.x = x
			this.y = y
			this.color = color
			this.moveInterval = 1 //Frame based
			//this.moveInterval = 500 //time based
			this.moveTimer = 0

			me.action = "nothing"
			this.actions = []
			this.Update = function (dt) {
				/*
		if (this.life <= 0) {
			return false //dead
		}
        */
				//this.moveTimer += dt //time based
				this.moveTimer++ //frame based
				if (this.moveTimer > this.moveInterval) {
					this.moveTimer -= this.moveInterval
					switch (this.action) {
						case "move up":
							this.y--
							break
						case "move down":
							this.y++
							break
						case "move left":
							this.x--
							break
						case "move right":
							this.x++
							break
					}
					this.action = "nothing"
				}
				return true
			}
			this.Draw = function () {
				/* FIX THIS: tilesize is undefined -> */
				DrawRect(this.x * tileSize, this.y * tileSize, tileSize, tileSize, this.color)
				//DrawRect(this.x, 0, 100, 100, this.color)

			}

			this.Control = function (event) {
				//since this function is called from outside and not called in the format "GridEntity.Control()", I have to make a new reference to the object with "me"
				me.action = event.value
				//this.actions.push(event)
			}

			Subscribe(this.Control, "controller-0")
		},
		GridFactory: function (x, y, color) {
			entities.push(this)
			var me = this
			this.x = x
			this.y = y
			this.color = color
			this.moveInterval = 1 //Frame based
			//this.moveInterval = 500 //time based
			this.moveTimer = 0

			//    this.shape = ["up","up","right","down"]
			//    this.shape = getRandomPathWithoutGoingBack(3)

			this.shape = rotate2dArray(GetRandomEntryInArray(randomShapes))
			console.log("shape:", this.shape)

			me.action = "nothing"
			this.actions = []
			this.Update = function (dt) {
				/*
		if (this.life <= 0) {
			return false //dead
		}
        */
				//this.moveTimer += dt //time based
				this.moveTimer++ //frame based
				if (this.moveTimer > this.moveInterval) {
					this.moveTimer -= this.moveInterval
					switch (this.action) {
						case "move up":
							this.y--
							break
						case "move down":
							this.y++
							break
						case "move left":
							this.x--
							break
						case "move right":
							this.x++
							break
					}
					this.action = "nothing"
				}
				return true
			}
			this.Draw = function () {
				/* FIX THIS: tilesize is undefined -> */

				for (var y = 0; y < this.shape.length; y++) {
					for (var x = 0; x < this.shape[y].length; x++) {
						if (this.shape[y][x] == 1)
							DrawRect((this.x + x) * tileSize, (this.y + y) * tileSize, tileSize, tileSize, 'yellow')
					}
				}
				//DrawRect(this.x, 0, 100, 100, this.color)

			}

			this.Control = function (event) {
				//since this function is called from outside and not called in the format "GridEntity.Control()", I have to make a new reference to the object with "me"
				me.action = event.value
				//this.actions.push(event)
			}

			Subscribe(this.Control, "controller-0")
		}
	}, // Grid game 

	Asteroids: {
		//Asteroids
		Spaceship: function (x, y, radius) {
			radius = DefaultParameter(radius, 30)

			entities.push(this)

			this.color = "green"
			Circle.call(this, x, y, radius, "white", 1)
			BehaviourListUpdater.call(this)

			this.behaviours.push(Turn)
			this.behaviours.push(ForceGo)

			this.speed = 0
			this.speedIcr = 0
			this.angle = 0
			this.dir = -0.5 * Math.PI
			this.sprite = new Image()
			this.sprite.src = "sprites/ship.png"
			this.Draw = function (x, y) {
				x = DefaultParameter(x, this.x)
				y = DefaultParameter(y, this.y)
				DrawSprite(defaultLayer, this.sprite, this.dir + 0.5 * Math.PI, x, y, this.radius, this.radius, this.radius)

			}

			this.timer = 80
			this.bullets = 4

			this.AddBullet = function (event) {
				this.bullets++
			}
			Subscribe(this.AddBullet, "DeadBullet")


			this.OnCollide = function (obj) {
				switch (obj.constructor.name) {
					case "Asteroid":
						this.behaviours.push(Die)
						Broadcast("registerScore", )
						break
					case "Bullet":
						break
					case "Player":
						break
				}
				return -1
			}

			this.Shoot = function () {
				this.behaviours.push(function () {
					if (this.bullets > 0) {
						new Bullet(this.x, this.y, this.dir)
						this.bullets--
					}
					return -1
				})
			}


			this.Control = function (event) {
				if (event.value === "Shoot") this.Shoot()
				if (event.value === "TurnLeft") this.angle = -.04
				if (event.value === "TurnRight") this.angle = .04
				if (event.value === "unTurnLeft") this.angle = 0
				if (event.value === "unTurnRight") this.angle = 0
				if (event.value === "Thrust") this.speed = .05
				if (event.value === "unThrust") this.speed = 0
			}
			Subscribe(this.Control, "controller-0")
		},
		Asteroid: function (x, y, radius) {
			radius = DefaultParameter(radius, 100)
			entities.push(this)
			var color = "red" //TODO: Figure out why this even here ??? If I don't have sprite???
			var alpha = 1 //TODO: Figure out why this even here ??? If I don't have sprite???
			Circle.call(this, x, y, radius, color, alpha)
			BehaviourListUpdater.call(this)
			this.behaviours.push(Go)
			//this.behaviour = Go
			//this.Update = function(){this.behaviour()}
			this.speed = 1
			this.dir = GetRandomDir()
			this.sprite = new Image()
			this.sprite.src = "sprites/asteroid.png"
			this.Draw = function (x, y) {
				x = DefaultParameter(x, this.x)
				y = DefaultParameter(y, this.y)

				DrawSprite(defaultLayer, this.sprite, this.dir + 0.5 * Math.PI, x, y, this.radius * 2, this.radius * 2, this.radius * 2)
			}
			this.OnCollide = function (obj) {
				switch (obj.constructor.name) {
					case "Asteroid":
						break
					case "Bullet":
						Broadcast("score", 100)
						this.behaviours.push(Die)
						if (this.radius > 25)
							for (var i = 0; i < 3; i++) {
								new Asteroid(this.x, this.y, this.radius / 2)
							}
						break
					case "Player":
						break
				}
				return -1
			}
		},
		Bullet: function (x, y, dir, speed, radius) { //Todo: Fix default parameters for compiler
			speed = DefaultParameter(speed, 5)
			dir = DefaultParameter(dir, 0)
			radius = DefaultParameter(radius, 5)
			entities.push(this)
			var color = "white"
			var alpha = 1
			Circle.call(this, x, y, radius, color, alpha)
			BehaviourListUpdater.call(this)
			this.behaviours.push(Go)
			this.behaviours.push(function () {
				this.timer--
				if (this.timer <= 0) {
					Broadcast("DeadBullet", true)
					this.behaviours.push(Die)
				}
			})
			this.timer = 80
			this.speed = speed
			this.angle = 1
			this.dir = dir

			this.OnCollide = function (obj) {
				switch (obj.constructor.name) {
					case "Asteroid":
						this.behaviours.push(Die)
						Broadcast("DeadBullet", true)
						break
					case "Bullet":
						break
					case "Player":
						break
				}
				return -1
			}
		}
	},
}
