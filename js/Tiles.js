//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h


/* File content description:
	
	All stuff I used for tile/grid based game experiment/project
*/
/* Todo:
	
*/

"use strict";

function Create2DArray(outer, inner) { //[x][y] 
	return Array(outer).fill(0).map(x => Array(inner).fill(0));
}

function CheckGridCollision() {
	if (entities.length > 1) {
		for (var i = 0; i < entities.length; i++) {
			for (var ii = i + 1; ii < entities.length; ii++) {
				if (entities[i].x == entities[ii].x && entities[i].y == entities[ii].y) {
					//HandleCollision(); TODO: Defininition is missing :(
				}
			}
		}
	}
}



//Visual properties // very universal global scope, because that is cooool. 
tileSize = DefaultParameter(tileSize, 32)
tileSize = 90
var spacing = 2

function Grid(widthInBlocks, heightInBlocks) { //constructor
	this.grid = Create2DArray(widthInBlocks, heightInBlocks)

	//populate with mapdata
	for (var i; i < map.groundLayerData.length; i++) {
		var x = i % map.groundLayerData.width
		var y = Math.floor(i / map.groundLayerData.width)
		this.grid[x][y] = map.groundLayerData[i]
	}
	//rendering
	this.Draw = function () {
		for (var x = 0; x < this.grid.length; x++) {
			for (var y = 0; y < this.grid[x].length; y++) {
				//                var color = 'gray'
				var color = 'gray'
				var index = y * map.width + x
				var char = map.groundLayerData.charAt(index)
				var color = groundTypes[symbolToGround[char]].color

				var style = {
					//                    color: color, //color does not work yet because I have no interpreting layer that can infer fillStyle etc.
					fillStyle: color,
					strokeStyle: "#000000",
					lineWidth: .0001 //lol
				}
				var rect = gridPosToScreenArea(x, y)
				Draw.Rectangle(defaultLayer, rect, style)
			}
		}

		/*
		for (var x = 0; x <= bw; x += 40) {
			context.moveTo(0.5 + x + p, p);
			context.lineTo(0.5 + x + p, bh + p);
		}

		for (var x = 0; x <= bh; x += 40) {
			context.moveTo(p, 0.5 + x + p);
			context.lineTo(bw + p, 0.5 + x + p);
		}

		context.strokeStyle = "black";
		context.stroke();
		*/
	}
}

//Enemy reaction time. A secondary interval in enemies where they can change their mind quicker than they can act and thus react to your action and also what action you seem to take next.
// pass through, property of movement, basically if something moves to your space and you move to that something's space then you'll both succed if one has passthrough.
