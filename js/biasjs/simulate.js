//simulate.js
/* simulation 1
my instructions/expectations
	Depict a beautiful plant or flower and present it on screen.

	break down of words:
		what is depiction
			realism vs artistic vs abstract?
			choose an artstyle
		what males something beautiful
			beautiful implies artistic.
			Decide on a composition
				composition requires elements. wait for elements of plant or flower.
				what portion on the screen
				how much
		what is a plant or flower
			what visual parts does it consist of and how are they related?
		what is done to be considered presented on screen or what state is considered presented on screen.
	Decide the context (choose from conventions, tropes, stereotypes)
		is it just a drawn plant no background
		is it like a biology poster demonstrating the leafs and stuff
		is it like a shot of the plant in nature
		is it a plucked plant in a vase?
		is it a potted plant?
		These are all common contextuals for a plant.
	...
	
	draw.
	present.


*/ //simulation 1


var minimalRectangleGame = {
	//make(game)
	//make(playerCharacter, rectangle)
	game: 1,
	contains: {
		playerCharacter: {
			is: {
				rectangle: 1
			}
		}
	}
}

var simulationMemory = {}


var a = {}
var game = "metroidvaniaHouseBuildingGame"
Extract(game, a)
console.log(game + " definition:", a)
//var b = {}
//Extract("goalStructure", b)
//console.log("the goal structure is", b)



// in a sense "var a" is the simulation.
// but there are no reactions in it that causes it to realize or manifest and no reaction to realize/mutate/tranform over time.
//the extraction must set up the system that continues it realization and tranformation.

//Realize(a) //manifest? //execute, actuate.


function manifest(key, target) {
	var obj = bias[key]
	if (!obj) return //there is nothing known to extract
	Object.keys(obj).forEach(function (relationalKey) { //assuming the structure is obj->relationKey->values that key only contains relational keys.
		if (relationalKey == "does") {
			if (!target[relationalKey]) target[relationalKey] = {} //Fill in missing relational keys aka, is, has, etc.
			Object.keys(obj[relationalKey]).forEach(function (key) {
				if (key == "manifest") {
					//DO THE FUCKING manifestation.
					//see bias.manifestation
				}
				//Extract(key, target)
				//target[relationalKey][key] = obj[relationalKey][key]
			})
		}
		if (relationalKey == "manifest") {
			if (!target[relationalKey]) target[relationalKey] = {} //Fill in missing relational keys aka, is, has, etc.
			Object.keys(obj[relationalKey]).forEach(function (key) {
				target[relationalKey][key] = {}
				Extract(key, target[relationalKey][key])
				if (Object.keys(target[relationalKey][key]).length === 0) {
					target[relationalKey][key] = obj[relationalKey][key] //default value instead of empty object, because empty object is meaningless.
				}
			})
		}
	})
}

//I know how to program a thing but how do I know how to know how to program the thing?






var metaphysicalMemory //bias //long term memory
var realWorldButHiddenMemory //alterable memory
var present //switching memory //temp holder

//make a god damn simulation that draws a rect object on screen.
//how do I want to start simulation????

//there are vertical descriptions, first x happens then y
// the smallest vertical description is, if a then b
//then there are horizontal descriptions, he was round and fat and blue:
//start simulation
//make a rectangle

//HandleMediaRequest(squarePictureRequest) // the test



//idea, have a global var set: {a: function(){}, b: ..., ...} filled with functions like for example set.angle, this way the Make() algo can look through the set index to find a related concept that will manipulate data in a correct way.


// there should be major difference between is a rectangle (manifests as a rectangle) and is like a rectangle (indirectly mnifest the properties of a rectangle)
