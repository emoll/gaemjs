let principles = { //design heuristics rather..
	//	start with the essential, build the structure around what is essential.
	//	start with the outcome, the function, and go backwards.
	//	Automatic constraints:
	//		If you have a required parameter and don't know
	//			if standard unit system (kg, meter, km/h)
	//				prototype the parameter value as 1.
	//			if scalar ((or .5 on a scale from 0 to 1)
	//				prototype the parameter value as .5 (between the theoretical minimum and maximum)
	//		Unless specified, Objects are spatially concatenated along a dimension. (Placed on a straight line.)
	//	Design freezing: (gradual)
	//		Decide the parts/aspects to not alter
	//		why:
	//			 enabling approach of a defined structure, instead of endless non-finishing opportunism.
	//			 If you change the "for what" the elements you find optimal obviously change.
	//			A sure way to never reach your goals is to move the goal post.
	//				every engineer talks about design freeze and design requirements		 
	//	Do the nobrainer check first. Can you make it work in 2 minutes? Can you fix it through habit or through precompiled methods?
	//		if you get the info or ques that it is bigger, do appropriately sized preparation
	//			project, feature, action, etc, etc.
	//	When dealing with flexible sizes, account for the biggest one first. (worst case scenario, outer maximum scope of project or aspect of project)
	//		and reduce as constraints come.
	//		for example the biggest reasonable kick drum.
	//	As you gain attention making a thing, people will come with really good suggestions.
	//	Surround yourself with people making suggestions that wants you to succeeded.
	//	learn how to filter suggestions from anyone.
	//	Always use the cheapest material possible. (derived from do the simplest required (requirement/specification) but not simpler.)
	//	Set reasonable upper and lower bounds
	//		Maybe don't consider making marble machine structure out of frigolit.
	//		Or concrete.
	//	One instance only, Once and only once, centralized working medium, optimality of less instances or less mediums:
	//		transforming all from one.
	//		Why:
	//			when implementing prototype and making a digital model
	//				the two will diverge over time unless you sync.
	//		How:
	//			The only way to only work with one instance. is to make it of elements that are abstract or temporary.
	//				as soon as you make a part that is defined you have a biased instance of your design.
	//	Use implementations and elements that have been proven to work (in other context and general contexts)
	//	rule of space efficiency
	//		when building a things you want its parts to be as concentrated as is functional.
	//	implementation follows function.
	//		function is greater than implementation/composition
}
