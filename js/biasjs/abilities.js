// abilities.js
const abilities = { // literacies:


	optionalConstraining: { // all you do in every moment is apply optional constraints that takes you more or less towards different states and outcomes.
		// understanding a range of possibilities
		// understanding goal/desirables/
		// understanding transformations
		// applying optional constraints efficiently
	},


	// ability to build the potential to perform in a domain. (preparation)
	efficiency: 1, // Ability to use potential with minimal loss

	// fundamental elements of challenge:
	//List of common challenges/skills:
	//	making out details 
	//	seeing things in complexity
	//	following multi step processes
	//	planning ahead
	// meta occupations

	// Types of challenge

	reactivity: 1,
	sensitivity: 1,
	reactionAbility: {
		//requiring low reactiontime
		// reactionTime: 1,
	},

	temporalAbilities: {
		timing: 1,
		// judging durations
	},
	rhythm: {
		// reaction based game in rhythm.
		// games where you are rewarded for reacting in rhythms.
	},
	rhythmAbility: {
		// What defines Rhythm Games:
		// There is an on-going success of rhythm games
		//
		// timing window dificulty
		// mechanical depth
		// performative play
		//
		// beat-map flow
		//
		// flow
		//
		// challenge the player's sense of rhythm
		//
		//
		// elements
		//	unfolding spatial note representation
		//	visualization of narrative and progression
		//	Music
		//	
		//	mimic (watch sequence and learn, then repeat)
		//	
		//	digital (button) coreography
		//		button
		//		morphed controller
		//			musical instrumment
		//			dance mat
		//	spatial coreography
		//		beat saber
		//		dance revolution
		//		etc
		//	Arcade/highscore elements
		//
		// avoidance:
		// just shapes and beats
		// super hexagon
		//
		// necrodancer makes the buttons be normal gameplay mechanic
		//	but bound to be used in a beat.
		//
		//
		// good rhyhtm game practices:
		//	1. the hook - "the game concept" (gimmicks)
		//	2. flow - handcrafted, feels, good charting. a map that makes you feel clever and skiller for getting through it.
		//		continous flows of notes: (natural next button mapping to make you move or dance interesting ways.)
		//			jacks
		//			streams
		//			rolls
		//	3. feedback - sound and visual cues, perfectly syncronized with the song
		//	4. make high level play make the player look impressive.
		//	
		//	
		// what makes rhythm games impressive is the player perfromance.
		// "rhyhtm games" (defined as that discussed here) is about mastering a very narrow thing
		//	time loop game.
		//
		// rhythm doctor - teaching one particular doctor
	},


	needsManagementAbility: {
		// You manage consumables like foods and tea and it is complex with many parameters
		// So like the characters have to eat the right things and then their body is like changing depending on that.
		// And then your character dies after like 7 days, but the humans health depends on your diet and stuff.
		// Either you manage your own character needs or you manage other characters needs through your character or through a omni perspective.
		// Needs management boils down to multiparametric balancing within a set of defined boundaries. 
		// Naturally depleting resources.
		// Stereotypes:
		// Survival: hunger, "energy"/tiredness, 
		// Sims: food, fun/entertainment/boredom, social, hygiene, toilet, rest/relax, sleep
		// Part of like needs management games, common in simulation games
		// For your character
	},
	humanNeedsManagementAbility: {},
	survivalAbility: {
		// Look up a wiki description of a survival game.. :)
		// Survival could be a good pacing tool in generate nature which limits exploration.
	},



	// directed focus
	nonCompulsion: 1, //delayed gratification
	goalAwareness: 1,
	agency: { //DevelopHumanAgency: // maybe this is a skill and thus should be moved to skill/challenge section (cooking etc)
		// Agency autonomy as motivational factor and learning
		//	Intrisnic motivation has to do with wisdom and the formation of vision which to base decisions.
		//		Basically the dynalist notes I have with dream to reality
		//		
		//	Increasing contextual agency - is agency a intrinsic motivation
		//		Contextual agency allows the accomplishment of intrinsic goals.
		//		But agency plays role in acquirement of extrinsic goals too.
		//		
		//		
		//	Increasing autonomy is the ability to choose the effects that happen in a context.
		//		This only comes from ability to predict and regulate the outcome of different situations through actions (activation of processes)
		//		
		//		
		//	Games develop agency without side effects in real world.
		//
		//	Games where we cannot define the singular goal will tend to be termed “play” or "toy" rather than rather than rather than “game.”

	},
	independence: 1,

	endurance: 1,
	persistence: 1,
	thoroughness: 1,
	attentionToDetail: 1,


	patternRecognition: 1,

	sequentialMemory: 1, // remember the attributes of multiple steps.
	memoryAbility: {},
	relevanceMemorization: 1, // Filter out what is relevant and remember it.

	// Orientation (finding and dealing with consistency and consistent wholes.)
	navigation: 1,
	spatialAwareness: 1,

	subtelty: 1,

	//physical
	motorControl: {
		// accurate path following
		// minute optimal changes
		// Fine motormovement
		SubtleMovement: 1,
	},
	actionAbility: {
		// An action game is a video game genre that emphasizes physical challenges, including hand–eye coordination and reaction-time
		// The action genre includes any game where the player overcomes challenges by physical means such as precise aim and quick response times

		// Benefits of action games:
		// Action games activate people. Something about these setups to act and move creates wakefulness and aliveness in people.
		//	movement games animate people...

		// Action games relies on concentration and flow. Pay attention and do the right thing, in the right amount, in the right moment.
		is: {
			videoGame: 1,
		},
		has: {
			conventionally: {
				playerCharacter: 1,
				protagonist: 1,
			},
			action: 1, // obviously lol
			violence: 1, // violence is mainly just implied by action...
			enemy: { // enemy is mainly implied by violence...
				count: {
					many: 1,
				},
				does: {},
			},
		},
	},
	evasionAbility: { //evade, sub-challenge
		// Player character gets hurt when touching "enemies" or enemy projectiles
	},

	abstractionAbility: 1,
	reasoningAbility: 1,
	scienceDeduction: { //- type of challenge
		//a game about using your senses to deduce properties of things 
		//a game about deducing the properties of artifacts by testing them.

		//And you test wether combinations have synergy and try to figure out why..


		//and there are visible indicators and invisible. like maybe they change color when close or a thing starts glowing
		//	invisible are things that like suddenly you are stronger or something.
	},

	verbalAbility: 1,
	lingualAbility: 1,


	// goal/outcome orientation
	resourceManagement: { // resource management
	},

	// organization
	shaping: 1, // Drawing
	ordering: 1, // Arrangement placement.
	itemCollection: {
		// common tropes:
		// arsenal collection (weapon collection) 
		// card collection
		// spell collection
		// ingredients
		// magical relics with passive effects
	},
	curationAbility: {
		// similar to collection but optimization includes discarding bad items.
	},
	// creative expression and compositional challenges
	decorationGames: { // expression games //artistic performance games
	},
	creativeExpression: 1,
	noveltyFinding: 1,

	combinatorialDecisionMakingAbility: { //TODO: please change name
		// Opportunity finding. min maxing?
		// Hill climbing.
		// Finding synergies and covering weaknesses.
		// ^ one of the most elemental skills/challenges
	},
	designAbility: {
		// basically what this algorithm should do lol
	},
	levelDesignAbility: {
		// You need all the different powers in order to explore all the different types of castle
	},
	baseBuildingAbility: {
		is: {
			designAbility: 1,
		}
		// is a "contextual" challenge. Involved multiple subchallenges in a "tangible" context.
		// "base building games require the player to build their bases by gathering materials or planning strategically."
		// These type of games require critical thinking and time management to ensure that your base is effective as well as economical
		//	resource management. (dealing with limited resources)

		// base builder mechanics
		// base deterioration.
		//	spontaneous.
		//	external threats.

		// invonves composition challenges, involves

		// concentrating resources, building infrastructure (structure enabling structure)
	},
	architecturalAbility: {
		is: {
			designAbility: 1,
		}
	},
	interiorDesignAbility: {
		is: {
			designAbility: 1,
		}
	},

	crafting: {
		// make crafting where the player can make a better sword by paying attention to detail
		//	but the design has like a optimal capacity which it can not get better than.
		//	So it is like most swords are only 50% but if you are skilled you can make 90% swords.
		//		and it depends on the materials, the skills and the methods,
		//			and paying attention to details
		// crafting but it is incrementally harder to improve
		//	like marginal gains 
	},
	manufacturing: { // automation // production, 

	},
	factory: {
		// What is a factory game basically??
		//	a bunch of "units" that processes some forms of resource
		//		that alters the state of the resource
		//		
		//	The goal is to generate a specific output
		//	
		//	prcessing consumes some resources
		//		time
		//		electricity
		//		code instructions
		//		money
	},


	strategyAbility: { // tacticsGame
		// https://en.wikipedia.org/wiki/Strategy_video_game

		// skillful thinking and planning to achieve victory
		// tactics games is about feeling clever (making clever non obvious decisions)

		// decision-making skills have a high significance in determining the outcome
		// Decision making challe
		//"Almost all strategy games require internal decision tree-style thinking, and typically very high situational awareness."

		// It emphasizes strategic, tactical, and sometimes logistical challenges

		// Strategy games rarely involve non-thought based challenges, like physical challenges or ones of aiming.
		//	real-time strategy game can involve reaction time and OODA loop type "think fast" gameplay.

		// Victory is achieved through superior planning, and the element of chance takes a smaller role.
		//	(strategy games are inherently non-random), if outcome is dictated by randomness, then it is not dictated by skillful thinking.

		// subgenres:
		// real-time vs turn-based
		// strategy vs tactics
		// 	tactics = the play out of combat scenario
		//	 strategy = enforcing different scenarios.

		// sub challenges




		// Tropes
		// Strategy games frequently make use of a windowed interface to manage these complex challenges.


		// Common sub-challenges:
		// Economic challenges
		// these economic challenges can include building construction, population maintenance and resource management.
		// Most strategy games allow players to accumulate resources which can be converted to units, or converted to buildings such as factories that produce more units. The quantity and types of resources vary from game to game. Some games will emphasize resource acquisition by scattering large quantities throughout the map, while other games will put more emphasis on how resources are managed and applied by balancing the availability of resources between players. To a lesser extent, some strategy games give players a fixed quantity of units at the start of the game.[2]
		//	Strategy games often allow the player to spend resources on upgrades or research. Some of these upgrades enhance the player's entire economy. Other upgrades apply to a unit or class of units, and unlock or enhance certain combat abilities.[2] Sometimes enhancements are enabled by building a structure that enables more advanced structures.[5] Games with a large number of upgrades often feature a technology tree,[2] which is a series of advancements that players can research to unlock new units, buildings, and other capabilities.[4][6] Technology trees are quite large in some games, and 4X strategy games are known for having the largest.[6][7]

		//In addition to combat, these games often challenge the player's ability to explore, or manage an economy.


		//		A build order is a linear pattern of production, research, and resource management aimed at achieving a specific and specialized goal. They are analogous to chess openings, in that a player will have a specific order of play in mind, however the amount the build order, the strategy around which the build order is built or even which build order is then used varies on the skill, ability and other factors such as how aggressive or defensive each player is.
		// exploration = dealing with the unknown and risks. Being prepared for the unknown requires forethought which is in the essence of strategic challenge.

		// Explorative challenges


		// FAIRNESS (this whole point is bullshit)
		// A fair strategy game has some form of balence in initial state between the players.
		// ^ this is really true for any competitive game lol. wtf.
		// Each side generally has access to similar resources and actions, with the strengths and weaknesses of each side being generally balanced

		// Sometimes you can pick teams or archetypes with different trade-offs but they should generally be considered balanced.

		// A strategy game calls for planning around a conflict between players
		// Part of strategy is game theory.
		// In strategy games you aim to achieve objectives.
		// strategy is about giving instructions. bossing people around.
		// logistics means to get the upperhand through resourcefulnes.

		// difference from rpgs - Strategy games tend to focus on units in larger numbers that are fairly similar rather than unique characters.

		// Techniques such as flanking, making diversions, or cutting supply lines may become integral parts of managing combat
		//		Terrain becomes an important part of strategy, since units may gain or lose advantages based on the landscape

		// some strategy games involves other forms of conflicts such as diplomacy or espionage.

		// A strategy game is typically larger in scope, and their main emphasis is on the player's ability to outthink their opponent

		// They are generally categorized into four sub-types, depending on whether the game is turn-based or real-time, and whether the game focuses on strategy or tactics.

		// a player must plan a series of actions against one or more opponents, and the reduction of enemy forces is usually a goal

		// Strategy is done from overview. bigger picture.
		//	In most strategy video games, the player is given a godlike view of the game world, and indirectly controls game units under their command.


		// most strategy games involve elements of warfare to varying degrees,[2] and feature a combination of tactical and strategic considerations
	},

	inventoryManagement: 1,
	logisticalAbility: {
		// logistics is the management (decisions about and maintenance of) of the flow (movement/transference) of things (resources) between the point of origin (sources) and the point of consumption (destination) in order to meet the requirements of customers or corporations

		// also known as supply chain management.

		// the art and science of obtaining, producing and distributing material and products (resources) in proper place and quantities.

		// the seven r's of logistics - meaning the right:
		//		product
		//		quantity
		//		condition (of product)
		//		place
		//		time
		//		customer
		//		price


		// transportation
		// warehousing - warehouse is a location with an inventory. it receives, stores and ships resources.
		// third party logistics - external managers providing logistics as service.
		// fourth party logistics - other logistic specialists, outsourcing all logistics to one organisation.

		// reverse logistics - handling returns, re use, recycling or disposal of products. That does it journey from the customer to the supplier.

		// "meet customer requirements" aka needs.

		// challenges:
		// rapid response capability to changes in the market or customer orders
		// minimize variances in logistics service
		// minimize inventory (in order to reduce cost of storage)
		// group shipments together.
		// 

		// logistical ability is all about understanding what paths exists and the ability to decide how good paths are and to choose apropriate paths.

		// Logistics is about determining optimal alternatives amongst delivery paths in order to meet needs/requirements.
		// How to best move things to achieve preferable outcomes/states.
		// Providing things as needed over space.
		// "the commercial activity of transporting goods to customers."

		// Resources are needed at different destinations.

		// The elements of logistics:
		//	resources - instances of goods that are needed at specific locations and time
		//	locations
		//	distances
		//	medium - that which exists between every location
		//		The space (distance)
		//		and it's content
		//		water, ground, air,
		//		river stream
		//		vacuum/space/gravity
		// terrain
		//		land,
		//		off-road
		//			forest
		//			snow
		//			ice
		//			sand/desert
		//			mud
		//		road
		//		track
		//		tunnel
		//		pipe
		//		air
		//		water
		//	vehicles ("transporters")- that which used in moving the resources.
		//		loading rates
		//			in
		//			out
		//		inventory
		//		fuel burn rate
		//		speeds on different terrain (emergent property)
		//		need for maintenance and repairs
		// 	modes of transportation
		//		air crafts
		//		water crafts
		//		diving crafts
		//		road crafts
		//		track crafts
		//		
		//	sources - location which holds or produces resources.
		//	destinations - locations which consumes resources
		//	paths/fields/routes of conveyance
		//	flow - the movement or transference of resoruces or vehicles.
		//	rates of
		//		production
		//		consumption
		//		transference


		// Improving the rate of transfer/conveyance.

		//	stopping time - (loading and unloading)
		//	energy consumption
		//	fuel distribution...

		//	inventory/stockpiles/storages/containers

		// what it shares with systems theory: stock and flow.

		//	transport interferences


		// logistical challenges.
		// Choosing where to build infrastructure for most profit (where it is closest for things that provide value.)
		// getting things together on time.
		// planning multistep, multivehicle routes.


		// What makes logistics a challenge and not obvious?
		//	There are many paths and it costs a lot to check all.
		//	





		elementsOfLogisticsAbilitys: {

			// Dynamics: the needs can change. sources can be depleted, 


			// Can be abstract like minimetro where it is just about eliminating resources. or can be discretely evalued by like a price.
			//		for example "company x wants to buy it for 10 kr while company y wants to buy it for 14, but they are in different locations, etc.

			// Company growths. different companies will grow differently depending on the resources they can accumulate and how good they are at doing business.

			// Problems with logistics games:
			//	it doesn't warn me enough when things are bad or the early signs of things starting to go bad. not early enough. I just find oh the last 30 minutes was spent in wain because of some dumb shit


			// Hints for logistics user interfaces:
			//	Descriptions of Logistics User Interfaces: (Mini Metro and others)
			//		Create 'conveyancePaths' between 'Places':
			//			in playing mode, as you click and hold over the area of a station/city and if you have a "line" "available":
			//				visualize a line between the station and the cursor position.
			//				in this state
			//					if the cursor is released, go out of this state.
			//					if the cursors position is over the area of another station/city,
			//						city builder:
			//							snap the end of the line to the station until the cursor exits the area of the station.
			//								in this state, if the press is released, a "real" line is built between the stations, allowing traffic to go over.
			//						Mini Metro:
			//							snap the end of the line to the station, a "real" line is built between the stations.
			//							enter the same state as the initial condition but make a line from the latest stations to the cursor instead.
			//			cool idea: Soft conveyance paths - draw soft lines for roads, you know like that soft brush in gimp
			//	 add 'transport unit' to 'conveyance path':
			//		Mini Metro aproach:
			//			from a menu of transport units, drag the transport unit that you want to have on a line, onto a line and release the button.
			//				the unit follows the cursor, and when released on the area of a line it is put on that line at the intersecting place.
			//		OpenTTD aproach:
			//			in a city or in a specific building, when you click on it you get a menu of createable transport units.
			//				click the transport unit you want, and if you have enough money the transport unit is created on the line at the city.
			//					if the city has multiple lines... then something must happen.
		},
	},

	// INTERACTIVITY AND CHALLENGE
	multitaskingAbility: {
		is: {
			ability: 1,
		},
		// A meta challenge of optimally switching between jobs/roles/sub-challenges
		// When the optimal solution involves context switching.
		// simultineity - the dificulty in multiple challenges happens in parallel at the same time.
		// maintenanceAbility
		// Figuring out when you should switch.
	},

	prioritizationAbility: {},
	// ^ related to a progression of responsibilities which naturally happens in good games
	//		leading to increasing dificulty and new content/situations to master.
	maintenanceAbility: {
		// challenge of keeping something adjusted to an optimal state.
		// baby sitting - maintaining good levels of multiple different aspects.
		// involves natural degradation or decay
	},

	transformativeResourcefulness: {
		// efficiency finding.
		// reaching required states with the least cost of in-game currency or in-game value.
		// involves considering multiple alternatives for reaching a state and determining the most cheap or generative one.
		// This is like a sub challenge of ... resource management? which is basically this + maintenance + storage/protection? +.
		// 		maximizing the accumulation of apreciating assets in order to reach goal state. and adapt to changing requirements/ goals.
	},

	bullethell: { // - The screen is covered with things that would that hurt the player
		//fundamentally based on evasion challenge.
	},

	// outside game
	socializing: {},
	// People skills:
	// 	communication
	// 	cooperation
	// 	coordination
	roleplaying: { //acting
	},




	// SIMULATED SYSTEMS/PROFESSIONS WHICH CAN HAVE INHERENT CHALLENGE IN LEARNING
	inGameOccupations: { // better to separate this from "genres" as occupations are only one aspect of games.
		walking: 1, // like in walking simulators.
		quests: 1, // errands / quests
		exploring: 1,
		research: 1, // / science / conceptual exploration

		humanNeeds: 1,

		socializing: 1,
		building: 1,
		crafting: 1,
		designing: 1, //layout
		management: 1,
		investing: 1,
		engineering: { //Engineering challenge
			// Chenzen
			// Understanding systemic inferences, in order to build machines of the right parts to serve a predictable purpose
			// Can be city building, or chemical machines or bridges. or building a transportation system.
		},
		logistics: 1,
		resourceManagement: 1,
		economy: {
			// The challenge is in obtaining and managing currencies, assets and trade rates in order to make profit in deals with other parties.
			// Economy games are intrinsically a type of optimization game
		},

		mining: 1,
		fighting: 1,
		farming: 1,
		cooking: {
			// by preparing ingredients/food for consumption.
			// The act of 
			// It is a process that require ingredients and produces a meal.
			is: {
				craft: 1, // It is a craft
				// transforming elements/ingredients to make them edible (optimize edibility)
			},
			does: {
				//optimizing state of resources for planned digestion.
				// produce meals
			},
			makes: {
				things: {
					edible: 1,
				}
			},

			//you optimize the needs of the people you serve with food
			//	doing the best of a situation with limited resources.
			//	There are ingredients at places and you must transfer them
			//	You have money which you can buy for
			//	and you can trade resources.
			//	fulfilling the needs of:
			//		nutrition
			//		non-poision
			//		satisfaction
			//the challenge of cooking is constraints.
			//	time
			//	ingredients
			//	tools
			//	uncertainty


			// edible or when edible things are brought together to cause synergistic effects when eaten.

			// Each combination of ingredients creates a unique meal
			// A meal is described by its main ingredients unless the combination has an official

			// selecting curating fresh and uncontaminated ingredients.
			// removing bad parts from the ingredients.
			// making the inedible parts edible (usually through heating or other predigestive treatment like processing from bacteria, fermentation or acid, salt )
			// combining things to create
			// good nutritional combinations
			//	things that taste good

			cookingEquipment: {
				heating: {
					stove: 1, //flat area that is heated
					oven: 1, // An air tight cage that is heated 
					microwave: 1, //air tight cage heated with microwaves
					fire: 1, //burning material that radiates/transfers heat.
				},
				surface: {
					potsAndPans: 1, // resists heat, contains liquid, isolatate from burning material

				},

				Tools: {
					knife: 1,
					spatula: 1,
					cuttingBoard: 1,
				}
			}


			//cooking skills:
			// timing
			// inspection / judgement of freshness
			// selection
			//		of tools and methods
			//		Selecting good ingredients
			// composition and artistry
			// analysing people and their individual needs.

			// The like timer based cooking is so good, you put a think in an over and wait a time.


			// culinary arts

			// the jobs of cooking:

			//		usually through substantial heating over a period of time (usually between a minute to an hour)


			// Sterelization - When heat is used in the preparation of food, it can kill or inactivate harmful organisms, such as bacteria and viruses, as well as various parasites such as tapeworms and Toxoplasma gondii. Food poisoning and other illness from uncooked or poorly prepared food may be caused by bacteria such as pathogenic strains of Escherichia coli, Salmonella typhimurium and Campylobacter, viruses such as noroviruses, and protozoa such as Entamoeba histolytica.

			//sterilization methods:
			//	heating
			//	drying
			//	biological (fresh meant and vegetables, living organisms kill intruding bacteria. Young organisms have less sicknesses.)
			//	
			// The sterilizing effect of cooking depends on temperature, cooking time, and technique used. 

			// reheating more than onec;
			//	Some food spoilage bacteria such as Clostridium botulinum or Bacillus cereus can form spores that survive boiling, which then germinate and regrow after the food has cooled. This makes it unsafe to reheat cooked food more than once


			//Cooking increases the digestibility of many foods which are inedible or poisonous when raw.
			//For example, raw cereal grains are hard to digest, while kidney beans are toxic when raw or improperly cooked due to the presence of phytohaemagglutinin, which is inactivated by cooking for at least ten minutes at 100 °C


			//Food safety depends on the safe preparation, handling, and storage of food. Food spoilage bacteria proliferate in the "Danger zone" temperature range from 40 to 140 °F (4 to 60 °C),


			//Washing of hands and surfaces, especially when handling different meats, and keeping raw food separate from cooked food to avoid cross-contamination


			//Washing and disinfecting cutting boards, especially after use with raw meat, poultry, or seafood, reduces the risk of contamination.


			// over cooking can also be bad and can harm nutritional value. 
			//	 cooking food increases the risk of some of the detrimental effects on food or health


			//cacinogens - Studies suggest that around 32% of cancer deaths may be avoidable by changes to the diet
			//cooking meat at high temperature creates heterocyclic amines (HCAs), which are thought to increase cancer risk in humans
			//human subjects who ate beef rare or medium-rare had less than one third the risk of stomach cancer than those who ate beef medium-well or well-done
			//microwaving meat before cooking may reduce HCAs by 90% by reducing the time needed for the meat to be cooked at high heat


			//Baking, grilling or broiling food, especially starchy foods, until a toasted crust is formed generates significant concentrations of acrylamide, a known carcinogen from animal studies

			//Public health authorities recommend reducing the risk by avoiding overly browning starchy foods or meats when frying, baking, toasting or roasting them
			//Cooking dairy products may reduce a protective effect against colon cancer

			//ingesting uncooked or unpasteurized dairy products (see also Raw milk) may reduce the risk of colorectal cancer

			//Heating sugars with proteins or fats can produce advanced glycation end products ("glycotoxins").[43]

			//Deep fried food in restaurants may contain high level of trans fat, which is known to increase levels of low-density lipoprotein that in turn may increase risk of heart diseases and other conditions. However, many fast food chains have now switched to trans-fat-free alternatives for deep-frying.


			//sustenance
			// human needs to eat carbohydrates, fats, proteins and vitamins in order to survive.
			//

			//Vitamins and minerals are required for normal metabolism but which the body cannot manufacture itself 
			//things can be generated / transformed in the gut through gut microbia




			// you cook things with tools that allows heating without merging with the heating agent.



			//food tends to smell nice.
			//the smell of food can depend on the smell of the environment



			// security while cooking:
			//			A lot of hazards may happen while cooking such as
			//				Unseen slippery surfaces (such as from oil stains or water droplets)
			//				Cuts (1 percent of injuries in UnIted States related to knives, ended in hospital admissions).In overall 400 000 injuries from knives are recorded in the US[19]
			//				Burns
			//Food safety



			//A cuisine is a style of cooking characterized by distinctive ingredients, techniques and dishes, and usually associated with a specific culture or geographic region. Regional food preparation traditions, customs and ingredients often combine to create dishes unique to a particular region.


			// People judge food by how it tastes. biological taste is developed to filter high quality food from low quality food (highly digestible and free from bad stuff.) 



			//good article: https://en.wikipedia.org/wiki/Taste
			//	https://en.wikipedia.org/wiki/Sense_of_smell

			//The sense of smell has many functions, including detecting hazards, and pheromones, and plays a role in taste.



			//))))))))))))))))))))))))))))))))))))))))))))))00000
			// the things about a cooking game
			//	you need a lot of diverse things
			//	cooking is more fun the more dishes you can make
			//	the more control youf have over dishes.
			//	there could be a game that simulates the underlying qualities of cooking
			//		for example which tastes goes well together etc.
			//		
			//	properties of foods and ingredients
			//		sweetness
			//		crispness
			//		fluffyness (foods with lots of air in like bread etc.
			//		softness
			//		sour
			//		spicy
			//		umami
			//		salt
			//		alcohol
			//		
			//	heat. (too hot too cold etc.)
			//	
			//	
			//	Meal size
			//	
			//	fatiness carbiness and proteinness.
			//	
			//	different people need different foods.
			//	
			//	water cooking
			//	vs pan cooking
			//	
			//	different cooking oils
			//	
			//	overcooked and undercooked.
			//	
			//	Sensitive stomach (not too spicy, not to bitter, not too cooked, not to raw)
			//	
			//	different people have different allergies and shit.
			//	
			//	quality of premade vs home made foods.
			//	deciding wether to cook a fancy thing on your own
			//	choosing teh cooking tools, some have more control but are harder to handle
			//	
			//	how easy is the food to handle (like a soup needs to be eaten with spoon.
			//	how much does the eater have to mix and match the ingredients themselves.
			//	
			//	when is it too many mixed tastes and when is it like a perfect amount of ingredietns.
			//	
			//	What is healthy food vs unhealthy.
			//	
			//	
			//	freshness of ingredients
			//	
			//	player gets to ge shopping for ingredients themselves
			//		(you go to different shop in town searching for good ingredients.)
			//	
			//	
			//	visual composition of food
			//		symmetry, etc
			//	
			//	
			//	there needs to be relatable people eating the food.
			//		so you see their expression when they eat and you're like nice ! I made them good food.
			//		
			//		
			//	the presence of salt drowns out the taste of bitterness
			//	
			//	
			//
			//A fictional cooking simulator.
			//	//why fictional: because a fictional system does not have to be as accurate simulation but rather can be whatever systems are more fun. Adding elements of fantasy, as well as skipping the more borign realistic parts.
			//
			//
			//maybe it's like a cooking show where you have to cook something with specific set of ingredients to please specific people.
		},
		food: {
			//properties of foods and ingredients
			properties: {
				material: { //material properies
					crispness: 1,
					fluffyness: 1, //(foods with lots of air in like bread etc.
					softness: 1,
				},
				chemical: { //cheemical properties
					sweetness: 1,
					sour: 1, //alkaline scale
					alcohol: 1,
					salt: 1,
				},
				temperature: {

				},
				consistingMaterial: {
					carbohydrates: 1,
					proteins: 1,
					vitamins: 1,
					minerals: 1,
					bacteria: 1,
				},
				umami: 1,
				spicy: 1,
			}
		},
		smelting: { // Ores for metals
			//
			// Abstractions:
			// 1. you put ore in smelter and you receive ingots.
			// 2.
			// 	You have "ores" in your inventory.
			// 	You put ore in smelter.
			// 	The smelter shows a loading bar.
			//	When loading bar is done you get ore.

			// 3. Heating the material to the right temperature melts the ore

			// You need a cast
			//	The metal needs to cool down...

			// 4. need to find rocks with varying levels of materials in them and you need to find ways to separate out the relevant metals.
			//		this is done by heating things up to different melting points where different types of material melts off.
			//
		},
		smithing: { //blacksmithing
			// Heating metals and banging on them
			// Or heating them until liquid and pouring them in a mold.
		},
		farming: {
			// Managing the requirements for plants to grow
			// Harvest crops(usable parts) of plants
		},
		watering: {
			// Water can be analoguely controlled by player..
			// Aka about puring or sprinkling water from one object to the soil physically.
			// I guess what I try to say is that watering in games where the player just presses a button and watch the same watering animation is very boring.
			// So watering should be analog action not digital.

			// Putting water on plant roots with the purpose of promoting plant health and growth
			// Pouring or sprinkling water lightly
			// Over plantation, to encourage plant growth.
		},
		harvesting: {
			// I like the idea of cutting grass with a scythe and then the fruits drop and you can pick em up
		},
		fishing: {
			is: {
				skill: 1, // Involving reaction time, strength, patience...
			}
			// The act of bringing fish out of water
			// Conventionally  by using a fishing pole with a floater
		},
		farm: {},
		woodcutting: { // lumberjacking???
			// Cut trees down with physics like in BOTW 
			//		when you saw wood you create saw dust
		},

		medicalTreatment: { // doctoring:????

			// Interface idea (from barrotrauma)
			//medical interface scanner view.
			//	heartbeat,
			//	infection
			//	bleeding
			//	poison 
			//	etc.
			//(biological medical simulation)
		},


		//Skills / challenges / game activities
		design: {},
		architecture: 1,
		construction: { // or building...
			// Designing, putting together resources and crafting in world structures like houses, crates, boats, machines or furniture.
			// Think about the house building Spiritfarer.
			// And designing ramps of tinted roofs etc...

		},
		buildingGame: {
			//		build buildings
			//	By drawing stuff (schematics)
			//	They are called templates
			//	And they are like whatever
			//	And they are in the dream world
			//	And you can see the dream world 
		},

		simulatedSocializing: {
			// In games there can be a simulated social life.
			// Like giving gifts to people and making people happy by choosing the right things to say in dialog.
		},
		fighting: 1,
		collecting: 1,
		curating: 1,


		buying: {
			is: {
				trading: 1,
			},
			has: {
				currency: 1,
			}
		},
		selling: {
			is: {
				trading: 1,
			},
			has: {
				currency: 1,
			}
		},
		trading: {
			// the act of two or more entities agreeing on which goods to give each other
			//
			is: {
				activity: 1,
				conventionally: {
					playerChallenge: 1
				}
			},
		},
	},

	exploration: {
		// best part of BOTW
		//	having a big giant world to explore
		//		and going, not knowing what to expect.

		//finding new configurations.
		//exploration is about revealing what has not yet been experiences or remembered.
	},


	// INBOX:
	pricing: { // How to do pricing of a product
		//	value based pricing.
		//		price based on perceived value by customer
		//	differential pricing
		//		by charging different prices to different customers you see how they react.
		//	competitive pricing
		//		based on competitors price.
		//
		//
		//
		//
		//
		//	basically the price should be enough to cover your costs, with a margin that gives you profits.
		//	You can base the price on the perceived value.
		//	with clear market comparisons you need to take into account what competitors are offering in order to have a good enough deal for any customers.
		//
		//	Also adapt the price based on who is buying because different people have different needs and different values and perception.
		//
		//
		//	the price must include all theefforts and costs the customer has to make to obtain and use a product.
		//
		//	(You buy it as long as it is cheaper than producing it yourself.)
		//
		//
		//
		//	switching to a new product is costly.(requiring time and effort)
		//	^this is the fukin moat that makes people settle with shit.
		//
		//
		//
		//	low prices lets you penetrate the market.
		//
		//	but maximizing short term profit you can skim the market by setting the highest price
		//		and attract customers that does not care so much about the price.
	},
}
