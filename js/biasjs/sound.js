var sound = {
	// audio and music
	audio: {
		//audio dynamics:
		//
		// object position affect:
		//	panning
		//	Tone
		//	Instrumment type
		//	reverb
		//	tambre?

		//waveforms:
		//	sinewave
		//	sawtooth
		//	squrewave
		//	trianglewave
		//noise?
	},
	music: {
		// Music — Music is the language of the soul. If your games are going to truly touch people, to immerse, and embrace them, they cannot do it without music.
		// the climax of the music should occur roughly where the golden ratio would be
		//	5/8ths through
		// give it crescendos and swells. dynamics.

		// 12 tone
		// music scales
		// music techniques?
		// Rhythm?
		// Riff
		// motif
	},
	soundDesign: {
		// when making audio for games it is important that both sound effects and music fit together harmoniously for the total soundscape of the game.
		//
		// sound effects must be clearly defined as separate from the playing music. so that the two are not confused for each other.
		//
		// the best is when the sound effects works as music, making you feel things in a harmonious and emotional fashion intended by the design.
		//
		// separate timbres
		// separate frequencies.
	},

	// aural illusions: Rissett rhythm, shephards tone
	musicalAssociations: {
		// associations between elements of music and emotions
		// dissonance is a tension
		// its like holding a grudge of sadness.
		//
		//
		// angry
		// loud, hard, aggressive, almost uncontrolled.
		// with some dissonance.
		//
		//
		// mysterious, suspended, almost like viewing the world from above
		// not knowing up and down
		// there is nothing to grasp, things don not make very much sense, it is very confusing.
		// a little bit bizarre
		//
		// mysterious.. only seeing it from the outside... it is concealing something.
		//
		// whole tone scale is barren of tonality as each tone is of equal distance to the other.
	},


	soundLogo: {
		// You have to make this sound logotype when starting game

		// Valve does it.
		// Polygon (fez) does it
		// Playstation
		// Windows XP

		// sound logo is presented in the intro

		// Like fukin consoles startin u kno.
		// Makes it feel real and serious.
		// You have to pretend it is running a custom video game console.
		// soundlogo for the console and for the companies.
	},

}
