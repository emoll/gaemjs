// VIDEO GAMES //GAMES:
// INTERACTIVITY AND CHALLENGE
// 31. Video game design
// 32. Video game challenges for the player
// 33. Simulated occupations (often very abstracted from reality) 
const gameAsMedium = {
	game: { // Game as thing
		// Voluntarily particible context of alternative constraints which provides a challenge of controlled action that gets aimed towards reaching a set of preferable conditions.
		// A game is a vessel for learning. The most important element of a game is the feeling knowledge or wisdom you take from it into the rest of your life.
		is: {
			medium: 1,
			playable: 1, // = can be played
			voluntary: 1, // Voluntary participation (involvement) // the player knows that they can quit and how they can quit.
		},
		has: {
			constraint: {
				atLeast: 1,
				//probably much more.
			}, // constraints implies reactivity. // Constraints (separate frame of constraints, A change in constraints, can be more or even LESS constraints than real life)// The player has (constrained) choice in the game.
			reactivity: 1, // change in state based on state, rules creating variable change, mechanics

			// Capable of providing the following to the player:
			//understandingOfConstraints: 1, //Full or partial observation of the constraints
			//understandingOfCurrentState: 1, // 1. Full or partial observation of the current constrained state.
			//conceptOfDesirableStates: { // concept of the variable desirability of conditions within the domain of constraints.

			//	 "that has declared either points or gradients (values indicative of) strived for"progression" or "success" ."

			// 	The closest is presenting what is meant to be desirable. Suggestion.
			// 	A subset of conditions within the constraints that the player desires.
			// 	Conditions of desire (the "point" (or "points") of the game)
			// 	controlled action, choice, decision. (the player is allowed and able to make decisions)
			//},


			// A game has reversibility of any physical constraints applied to the player and their possesions.

			// Differences between games and toys:
			// A game has challenge defined by desired outcomes A state or direction of desire.
			// A toy does not
			// A game is often a world, something you project into. (A game is different from a real forced situation, in that you can choose to play or not play it (to partake in the magic circle or not).)
			//A game applies constraints to the abilities/actions of the player.


			// gatekeeping failure/success.  Vessel of successful learning. Support structures. Hopefully dynamic in changing to the individual player's needs.
			// It is set up for you to get somewhere with states that let you progress or start over.


			// Three discrete levels of constraints, from objectively governed to subjectively governed.
			// Physics, defined interpretation, play 

			// the enteringness of a game has to do with participaing in a domain of alternative constraining.
			//	physical constraining (binding to physical alterations)
			//	judge constraining (abstract reading of values and applied measures) Has defined consistency.
			//	Play - imagined self constraining (acting/role-playing) (Being well-behaved with the context). constraint by fantasy/immersion/projection?

			//Examples of above: A board game has physical constraints in the rolling of dice, shuffling of cards. Sports rely on physical constraints of ball. video game relies on physical constraints of player sensors.(input channels)
			//	judge constraints are applied by noticing conditions from the ruleset and apply the right measures. In sports this is done by people that are dedicated judges. In video games this is done by the computer. In board games it can be either, and is most often judged by every player in th game together, to notice rule condition and agree upon the exact definitions of the rules if there is ambiguity.





			// Relatively mild consequences to the player, potentially grave consequences for the avatar
			//A game leaves the player unaffected in the ways agreed upon by the game.
			//	A real world scenario can harm you and continue when you don't want to.

			// A game is quittable.
			// the players can choose to enter a game.
			// The players can choose to quit at any time.
			// The player is aware of all physical changes in constraints (Anything that affects the player physically should be known by the player before partaking.)

			// there can be constraints on how a game can be entered or exited.
			// can be entered and exited through free will.
			// a game is consentual and maybe contractual.
			// it maintains/protects the player free will to enter the game or quit the game with relatively mild consequences.

			//the manner of playing in a contest. Could be the contest to a challenge.

			// Work is a game that is won for money.
			// Art is the expression/impression of sensable things. The abstract feeling you get from experiencing structures. 





			temporality: { //rules for flow of time.
				count: {
					atLeast: 1,
				}
			},

			// Vocabulary:
			// mechanics => constraints
			// inGameOccupations => challenges
			perspective: 1, // One or more
			challenge: { // One or more
			},


			// give certain impressions to the player
			// cultural or emotional associations carried by the symbol that is this game.
			gamePurposes: 1, //TODO: think if this is synonymous with gameIntention or not.

			hierarchyOfFocus: {
				// The category, archetype and communication of challenge domain.
				// 		challenge the player in specific ways

				subjectMatter: 1, // What is it about (if it is about something particular. having a Point)
				centralFocus: 1, // games, videos and other temporal media are usually character-centric.
				// in games this is done by keeping the protagonist in the center of the frame/picture
				// games can a
				// mediums with a declared protagonist are usually centrally focused on the perspective of the protagonist character.
				// ^ maybe most of these truths are about Narratives rather than games or mediums?
			}
		},
		does: {
			// A game needs to be presented through medium and needs external input (otherwise it is another medium)
			reactToInput: 1, // Listen to and react to input
			manifest: 1, // Manifest through output channels.
			//	Assume manifestation to be visually presented on a square in a screen.
		},

		can: {
			be: {
				playing: 1,
			}
		},

		// What games ought to be or should be:
		goodGame: {
			// The goodness in games is
			//	An intention that shines through the design choices of 
			//		aesthetics
			//		symbols
			//		themes
			//		mechanics 

			// A good game is one that is right for the skill level of the player
			// A good game is one that is helpful in developing skills of the player that the player desire to develop.
		},

		// play is unbounded, game is constrained

		// Is it still a game if it is not played?
		// A game means 2 things:
		//	A context of constraints (reacting agents) you can play in. "Did you play the game?"
		//	A context of constraints (reacting agents) being played in. "Did you watch the game?"
		gameDefinedAsProcess: { //Game as process
			// Voluntary participation into a context of alternative constraints which provides a challenge of controlled action that gets aimed towards reaching a set of preferable conditions.
			// A game is when you take responsibility over acting in a domain of constraints that has little to no consequences outside of it.
		},

		// It is kinda interesting what transcends the game world:
		// 	Camera often transcends the game.
		// 	The code transcends the game
		// 	GUI transcends the game, input transcends the game, sensory stimulation transcends the game.


	}, // Interestingly I could make it generate a set of files that makes a game or I could just simulate game with the current system.

	// Determine Available mediums of experience
	//	A view is a medium of experiencing a things visually.
	//	screen

	videoGame: {
		// A set of challenges and experiences that you encounter over time in various constellations
		is: {
			software: 1,
			interactive: 1,
			game: 1,
			medium: 1,
			toy: 1, // Digital toy lol
		},
		has: {
			input: {
				conventially: {
					conventionalMovementInputs: 1,
				}
			}, // Input method... controller.
			screen: {
				does: {
					present: { // todo: define?
						view: {
							of: {
								world: 1,
							}
						}, // A video game always has a choice of view at any given time. No view is also a choice in view.
					}
				}
			}
		},
		does: {
			present: {
				view: {
					typically: {
						through: {
							screen: 1,
						}
					}
				}
			}
		},
		conventionally: {
			has: {
				screen: {
					count: {
						atLeast: 1,
					}
				}
			},
			does: {
				screening: 1,
			}
		},
		// How to verify the model:
		// Player predictably classifies the medium they just experienced as a video game.


		signalling: {
			// video game signaling - the promise of a game. that which the player thinks the game will teach them about because of the signs it shows of being a particular experience
			// the signs of being a video game in the perception of human agents.
		},

		camera: 1, // Unsure if it should be here, does all games have camera?
		World: 1,
		player: 1,
		simulation: 1, // ?

		educationalAdvantage: {
			// Games teaches players to deal with costs in safe environments. (without causing harm to real systems)
			// player is always directed to what is critical for their development and what gives them flow.
			// games are relatively more engaging than other mediums
		},

		purpose: {
			// Don't waste the users time.
			// The fact that they come to play suggest they believe they have something to learn.
			// Somehow figure out their levels of development and what their goals/interests are.
			// Determine if you can provide value.

			// Priorities Highest to lowest from top to bottom
			// End goal of games is ability to maintain quality of experience without the game.
			improvePlayer: {
				//quality of user experience (is there ux in here?)
				// Get the player excited to go back into the real world with new found perspective/experience/knowledge/skill

				// The player should have experiences they can grow from.

				// Wisdom & virtue <- relevantExeperiences + good mapping/modelling + developed intelliegence <- empathy + wisdom + presentation (knowledge/ ways of thinkin beyond the boundaries of the player). 
				make: {
					player: {
						virtuous: 1,
						wise: 1,
						// vigorous: 1, //ready to live their lives to the fullest. lively? animated? vigorous, spirited, vibrant, healthy: 1,
					}
				},
				// Create meaningful relevant experiences
				//	Cherry pick only experience that are relevant 
				//		Build the player to gravitate to the meaningful elements 
				//		By choosing how the world behaves and nudging how the player behaves.


				// Teach and support players with what they need
				// Put their wisdom to the test.... (only works if they are up to a challenge (ready to act wisely), else they'll try to play a different game..)

				//		Keep player interested enough to stay (if it helps them)
				// Ability to teach
				is: {
					informative: 1, //
				},
				// Usefulness
				// Richness
				// Novelty (Not chaos)


				// resonance, art, beauty
				//		speaking to the soul
				//	Something beyond/transcendent
				//		transcendent, novel, resonating with our ancestors, forgotten essence, potential future.
				//		patterns that can not be understood easily in written definitions but when experienced it transforms you. 
				//	Something with feeling.
				//		Grounded in deep and intricate emotions.
			},
			notDegradePlayer: {
				// the player should only have experiences they can handle 

				// Avoid causing trauma (non constructive states of mind) where problems are introduced without any findable solutions, which leaves the player roaming until they stop out of exhaustion.

				// avoid confusing the player with things that are meaningless or pointless.


				// "The player is always right."
				// I want the player to be in control of their experience. 
				// Meeting player needs for control/agency:
				//		The player to be able to start and stop as they please.
				//		I want the player to stop playing when they feel they are done.
				//		"I want the player to not be or feel addicted" why? because addiction is always a negative experience? why? addiction implies compensating and not fulfillling their human needs.
			},
			Presentation: { // deliveringMeaningfulAndHelpfulUserExperiences: { //3. delivery/sharing/working (distribute)

				//		Doesn't break fantasy of the player with arbitrary limitations like video games have historically done.

				CreateWorthyPlayerExperience: { // Build a player experience. //worthy == fitting requirements / specifications aka good enough
					is: {
						playerSatisfaction: 1, // Causes player satisfaction... // meet the expectations, needs, or desires

						// I think entertainment is a need
						entertaining: 1, // Player is entertained (entertainment might be an effect of satisfaction?) //entertainment is really just the key to keeping attention...
						// (Entertainment has these connotation of just being distracting from what really is good ...)
					}
					// How? By facilitating a good play-session within which the player's attention is kept and managed in order to present/communicate meaningful patterns that the player can grokk in order to later remember in real life situations in order to be empowered when reacting and choosing in order to live a more satisfying life?
				},

				// Assumptions:
				//	1. There is a player.
				//	2. The player is meeting their own basic needs, they are here for feeling, learning or wisdom

				empathize: { // Empathy is needed to understand whether the player is learning or not.
					// You can only help and direct that which you understand.

					// Understand the player state.
					//	Model the player's behaviour.
					playerActionRecord: 1, //record player actions as a basis to infer meaning and create a model.

					// Player satisfaction can be inferred
					// You can only measure player satisfaction with an player model.
					// You model your player by recording their actions (with timestamps) and inferring what those actions means. (now a camera would be good...)
					//	Make a player knowledge memory. (assume all inputs are from player, assume it is the same player throughout the game/experience)
					// How does the algorithm know they have a good enough player model?

					// Personality modelling
					// Emotional state modelling
					// Cultural background modelling?

					// Test: "can I communicate with the player." 
					//	Something like "press A to start."
					// If they press all kinds of other buttons then that indicates explorer
					// If they spam A, then they are probably impatient.
				},

				// Become knowledgeable, wise and efficient enough to improve the player

				// Generate a desirable experience
				//	Goal maximize information and entertainment (infotainment)
				//	GeneralPrinciplesOfGoodExperience: {
				//	Do not make the player wait without good reason. (Time is valuable)
				//	Flow (do not under-stimulate, do not over-stimulate)
				//	Should not freeze the program

				// Do not break player experience for the negative unless you have a good reason (experience or teaching).
				// Old definition of goal: **generate value through symbolic representation, interaction, animation & sound**
			},
			selfImprovement: { //2. development. (transcend and include...) //self improvement, self optimization
				safetyAndTransferenceOfValuableInformation: {
					// Improvement over time
					// The algorithm should aim to preserve valuable insight in a compressed, comprehensive and accesible storage.
					// Aka the algorithm should be sure to contribute it is knowledge to a pool of knowledge in a verifiable and secure manner.
					// Realize its own incapabilities and seek support.
				},
			},
			algorithmIntegrity: { //1. (stay whole) Homeostasis: { //safety: 1, //non termination of the software before fulfillling its purpose.
				must: {
					not: {
						harm: {
							// Users/players/operators of the system.
							// The physical hardware equipment. (computer and connected equipment)
							// The operating system...
						},
						// crash the OS session (rendering the computer inoperable until rebooted)
					},
					//	Must work within the hardware limitations (Do not expect you've got more than you've got...)
				},
				// ^ Most of the above is essentially protected by operating system and hardware structures... It is made to be safe...
				//	But I guess there are still things that can be done to maximize safety.

				// Must not unintentionally interrupt (crash) the game session.

				// Must not reach beyond the limitations of the system (causing it to crash)
				//	Break the hard limits of the system
				//		Rely on network speeds faster than the connection.

				// Should maximize the utilization of given hardware. (seize the opportunity)

				// See computerConstraints

				// This is kinda handled by
				// Security and integrity of
				hardware: 1, // Hardware to run on... 
				software: { // see unacceptableComputerPerformance:
					// Garbage collection..
					//+ protected domain and process priority in the operating system
				},
				powerSupply: 1, //power supply to the hardware

				// List ways
				//	hardware can break
				//		...
				//	user realistically can be harmed
				//		...
				//	software can break/crash:
				//		Infinite loop
				//		Filling all ram
				//		Destroying internal software integrity (overwriting or deleting integral functions or variables of the local software)
				//		Corrupting the ram (overflow?)
				//		Software errors.
			},

			// runs smoothly
			// uses (computer) resources efficiently
			qualityVsPerformance: {
				// A trade off between quality and Performance
			},
			lightPerformanceGame: { //TODO: move to goals rather than game types?
				//a good game that is light on my computer. (does not make it exhaust heat and air.)
			},
		},
		//^goodVideoGame:
		videoGameAsProduct: {
			icon: {
				// a small image on transparent background with a clear, unique and recognizable icon that represents the game.
			},
			filesFolder: {
				// This is where the game files are in a classical video game.
				// All binary code files, all sounds, all images and videos and icons, etc
			},
			stages: {
				startupSequence: {
					//A logo is presented to indicate the game is starting
					//maybe a loading bar
					//this step is redundant if startup time is negligible
				},
				titleScreen: {
					//press ... to play
					//This is the first impression of the simulation inside the game.
					//usually it is enticing and speaking of the game.
					//it is like a window into the game.
					//in a theme park it is like you are just at the entrance to the theme park and you are peaking in.
				},
				beginningArea: { //TODO: move to game as this has less to do with the product side...
					//this is the first playable area of the game.	
				},
			}
		},
		videoGameTropes: {
			// First present an intro, then present a splash screen. etc.
			// Do world building in order to present a world in order to satisfy the player.
		},
	},
	software: {
		purpose: {
			//software serves humanity.
		}
	},
	gameDevelopment: {
		// You can create inspiration boards for multiple aspects
		//	moodboard for the feeling
		//	style board for the style
		//	feature board for features
		//	Animation board, etc, etc.
		//	Outline board
		//	Character board.
		//	place/location board - images of interesting places and landmarks that would fit in the game.
	},



	// Lead the player spatially with  perceived dangers and reward
	// Show environmental affordances by example of NPCs or non playable beings.
	// Monkey see monkey do, if they can then maybe i can.


	// Test what the player wants by giving them a set of options.
	// Do they seem to collect a bunch of items. maybe make the next section about item collection
	//	Consistent with desire for grinding.
	//	
	// Do they rush? then maybe help them accelerate toward their goals.
	//	Likes a challenge?
	//	Wants more actions?
	//	Impatience?
	//	Heightened adrenaline?
	// Do they stop for combat? then maybe try to make combat more intricate and interesting.

	// Conceptual separation: - add to design principles...
	// Remember you can change color scheme to match the tasks the player is doing.
	// One colorscheme for combat orientation, one for cooking, etc.


	// How do you classify games that have characters? // level of social simulation



	// Player model.
	// A player is someone consciously and willingly engaging with the rules of a game
	player: {
		// Someone (a subject) willingly participating in a game.
		// player attention/ focus
		is: {
			//entity: 1,
			subject: 1, //has subjective experiences, is impressed upon, has an isolated inner that experiences things outside of it.
			//			typically: {
			//				human: 1,
			//			}
		},
		does: {
			play: 1,
		},
	},



	// Player control.
	// _________General purpose game mechanics_______________
	cooldown: { //common game pattern trope or mechanic
		is: {
			mechanic: 1,
		},
		does: {
			//disables the ability to do something for a set limited time.
		},
	},
	//Trial? Rite of passage?
	// Minimum requirement of continuation.. aka you must build something optimally to get to the next level of the game.
	competition: {
		// against other computer or other players
	},
	timeControl: {
		// Time travel feature. go back in time to fix your mistakes.
	},


	// _____THE PARAMETERS OF GAMES:________ (Decisions, determinants (things that need to be determined))
	// some "parameters" are just relevant measurements rather than inherent properties.
	//		for example computer performance. The performance emerge


	// Genres or something:
	//		RPG
	//		Survive
	//		ResourceManagement
	//			Optimization	
	//		Simulation
	//			Farming
	//			Magic
	//			Physics
	//			Chemistry similar to:
	//				Big Pharma, SimiLand, sand simulator and breath of wild.
	//			weather

	perspective: {
		// The subject of a view comes through a perspective.
		// persipective/world representation
		//		2D
		//		Sidescroller
	},
	topDownPerspective: {
		// Seen from straight above.
		// Gives an overview.

		// Good for 2d racing games.
		// Good for 2d exploration. (because many directions compared to side scroller)


		// End goal of topdown game:
		// The satisfaction of drawing smooth roads across smooth hills and landscapes

		// Top down games:
		// Top down game about slow transportation quests.
		// Top down snake on lanskape game where snake goes down holes and stuff.
		// Top down logistics 


	},
	sidePerspective: {
		is: {
			perspective: 1,
		}
	},
	sidescroller: {
		is: {
			format: 1, // is a format. Not sure if this is relevant?
		},
		has: {
			sidePerspective: 1, // A side view/perspective - is viewed from a side-view camera angle
			// Camera movement along 1 dimension
			// 		scrolling view/camera (think a pergament scroll which is being rolled on both sides)
			// 		^scrolling implies one dimensional camera movement.
			// 		typically horizontal
		},
		// Often the camera moves so as to align the player character with the center of the view.
		// Often using Parallax layers to create depth perception
		tropes: {
			JumpingAsMeansToChoosePath: {
				// jumping is a really good way to decide you want to go up on something in the background. (up on a rock for example)
				// this usually comes with the ability to jump down of objects by pressing down and jump button.
			}
		},
	},
	interactiveHighlighting: {
		// The elements that can be interacted with, especially the ones relevant to the challenge of the game, are highlighted.
		// A clear distinction between background and foreground elements.
		// Interactive elements can be in the foreground
		// Aesthetics, settings anq cues can be in the background.

		// TODO: summarize at that design note about foreground background difference.
		// needs to have clear separation between background and foreground
		// For example: what is walkable and what is not.
	},

	systematicality: { //for games, what makes a games more systemic? What makes something a system rather than a condition?
		// Jonathan Blow:
		// There is ultimately one dictating if statement
		// But there are a lot of things that affect the condition
		// Like maybe an equation of 100 factors dictates the search results in Google
		// Which makes it more of a system than a simple if statement
	},
	interactive: { //interactivity: { //interact: interaction:
		// interaction = affecting something's state.

		// interactive = able to be affected.
		// reciprocal action or influence.
		// communication or direct involvement with someone or something.
		// allowing a two-way flow of information between entities; responding to the other's inputs.

		//(of two people or things) influencing each other.
		//"a fully interactive map of the area"

		// reacting(changing) to externals and causing reaction in externals.
		// changing from external signals and causing change through signaling out. 

		// it requires two or more isolated/bounded entities.

		// Inter active
		// systems that are active on changing each other's properties.
		//	when elements affect each other's properties

		// Activity that exists between two or more elements.
		// You act on it and it acts on you.

		has: {
			sensor: {
				atLeast: 1,
			},
			outputChannel: { //subjectively experiable output channel
				atLeast: 1,
			},
			//internals: 1, // there must be something internal that changes. In video games this often refers to aspects of the game simulation.
			//interactor: 1, // A conscious actor that interacts. is it still interactive if no one interacts with it? It is.
		},
		does: {
			// when signaled, change
			// send signals with intention of causing external change/reaction
			// change internally from input signals. (partial systemic change on input)
		},

		// interactivit: or interactiveness:
		// more interactive means:
		// 		More distinct input channels which affect distinct aspects of the state.
		//			more ways to affect unique part of a thing.
		//		responsiveness - lack of delay between input, internal change and feedback
		//		manipulability and controllability of system or subsystem 

		// the difference between reactive and interactive is memory of the history of interactions.
		//	A reactive system (like a game mechanic), responds the same to the same input
		//	whereas an interactive system changes state as to be able to respond differently to the same input dependent on state.

		// game interactivity:
		// Usually a game is called more interactive when the player is able many in-game systems at any given time.

		// video game examples:
		// A game with low interactivity = walkingSimulator


		// Purpose and what interactivity is good for:
		// 		Interactivity is sorta a need that players can seek from video games, to do action which gives feedback back to the player, making the players button presses feel meaningful.
		// 		Interactivity is the basis if practical learning in video games. it is what makes you navigate the advance organizer of information that is the game.
		// 		Interactivity can serve a fidgeting purpose, let the player just do some action to entertain their body.

		// "I would like the world to be a little more interactive and feel less static. I mean environments and buildings that can be destroyed or affected, played with in a way that creates a reaction."
	},
	perspective: {
		side: 1, //sidescroller
		top: 1, //topdown
		slinted: 1, //2.5D
		// hydronkinal, the fucking gridbased but you see it from the side. so it looks plane but top down from a corner.
	},
	multiplayerGame: {
		is: {
			game: 1,
		},
		has: {
			player: {
				count: {
					moreThan: 1,
				}
			}
		}
		// in multiplayer it is crucial that you have a shared sense of reality... (for what?)
		//	consistency of experience on multiple computers.

		// Multiplayer concerns that I can not be bothered to deal with now
		//	MMO part, like an actual MMO server with shared maps?
		//	because
		//		You want people to randomly stumble upon each other.
		//		You want a static memorable map like Maple Story world with unique landmarks
		//		You want land that no one is responsible for, like a plaza, a terminal.
		//		Easy solution is to make this world frozen in time or stuck in a time loop
		//		this way I do not have to have the organic which is heavy on a server
		//		also good reason for respawnings.
		// Multiplayer concern
		//		You can only collaborate between games that are compatible metaphysically.
	},
	PVP: {
		// playerVsPlayer
	},
	areaRestriction: { // player access?
		// Used for phasing
		// make tools to restrict the player from reaching specific future areas before they are ready.

		// Hard Gate: 
		// Halts progress until something is completed, like finding a key or pressing a switch.
		// Usually takes longer to bypass than soft gates.
		//
		// Often used when you want players to explore the area, have them enter areas they would not normally to reach their goal (like entering a mine to find dynamite to blow up rocks blocking the road), etc.

		// Soft Gate:
		// Intended to briefly slow the player down. 
		// This could be pushing down a tree, turning off a valve or needing your AI partner to help you get the door open (boost them up so they can drop down a ladder), etc.
		//
		// Handy when you briefly want to slow the pace down, have some character banter, etc.


		// Valve: //points of no return
		// Areas that close off behind the player (can later be opened or not, depending on your intent).
		// This can be having player jump down a cliff ledge, a part of the level gets destroyed behind the player, they slide down a rope, a crawlspace, etc.
		//
		// These can be effective to keep pushing players forward, allow devs to load/unload certain parts of the level, in Multiplayer (combined with blocked LoS) to protect player spawn rooms, etc.

		// Remember to restrict areas. keep them in the tutorial area, etc, make sure you can not return. make sure you can not go to the "wrong" places.
	},
	inaccessibility: { //preventing the player from the finish line
		// level gate keeping and bottle necks 
		//	you can not access the third world before you beat the second and the first.
		//	you must travel very far... and maybe through an unbearable desert
		//	You must kill this boss to access this area.
		//	you must reach level 200 to enter.
	},
	subjectOrientation: {
		world: 1,
		characters: 1,
		objects: 1,
	},
	playerRepresentation: {
		Character: 1,
		gommode: 1, //user interface
		has: {
			staticVsChanging: 1,
		}
		// character vs godmode
		//	Pros of cons with player character vs godmode/no player
		//		Player character:
		//		* Identification with body
		//		* Emotional expression
		//		* Bodily expression
		//		* Relatability
		//		* Mentralisation/phasing/limitation
		//		* Interface for reasonable multiplayer
		//		cons:
		//		* another feature
		//		godmode:
		//		* good for testing
		//		* faster interactions
		//		* less interface between player and the world
	},
	// meta game design
	reminderOfReality: {
		// to the player: "remember to take a break"
	},
	// Spatial loop, when you reach the end you are back in a familiar place.
	// game design
	diversityOfPlayableCharacters: {
		//restrictions, like, background, spawning city/town/area, gender, socio-economics, race,
	},
	// Artistical means to an end: (also domains that can be definitely SET in the engine. (color here on screen, style there, etc))
	// communicable domains....?
	// important game design decision???
	//	Which things are opt-in in the game?
	// Of interactivity: (or agency)
	// Forgiving vs unforgiving
	//	fine-tunement,
	//	paying attention to details.

	cardGame: {

	},
	rougelike: {

	},
	// Simulated occupations (often very abstracted from reality)
	// focused controlled interactions with subset of reality, following a set of heuristics in order to obtain outcomes.
	beatEmUp: {
		// confronted by hoards of enemies you are supposed to beat them to death.
	},
	fightingGame: {},


	// Platform 
	// landscape of surfaces affecting physical traversal. Generally of a character.
	// when challenge it is obstacle course.
	platformer: { // platformGame:
		is: {
			actionGame: 1,
		},
		has: {
			platformingChallenge: 1,
			world: { // TODO: make this syntax work so it merges/adds to world
				has: {
					platform: {
						count: {
							many: 1,
						}
					}
				}
			},
			moves: { //inputControls
				walking: 1,
				running: 1,
				jumping: 1,
				dashing: 1,
				//thrusters?
			}
		},

		// tropes
		// the camera focus follows the player character
	},
	platforming: {
		// I think platforming can be detached from challenge.
		// Just because a game relies on platforming doesn't mean it has much platforming challenges.
		// landscape of platforms

		// Yes platforming is a behaviour. not necesarily challenging.


		//platform literally means flat shape...
		// it is more controlled landscape traversal 

	},
	platformingChallenge: {
		// Involves the challenge of platforming
		// Platform games are action games that feature jumping, climbing, and running through many diverse levels

		// Sub challenges:
		//	Estimating and controlling trajectories
		//	Timing
		//	Jumping,

		// subject control their movement between lanscapes and platforms.

		// game as obstacle course (look up obstacle course online lol)
		// there are platform/surfaces on multiple heights.
		// The player character is intended to
		//	avoid obstacles
		//	jump between platforms
	},

	simulationGame: {

		// simulation games can be about the simulation of a specific setting.

		// Simulation games has the challenge of understanding a real or simulated system and manage it. 
		// Simulation games can be toys without challenge because there is not necesarily a challenge in something simulated.
		// sim games:
		//	menus with many icons and things.
		//	Focus on the subject that is simulated. (often objects rather than people)
		//	zoomed out or overview perspectives.
		//	Unique overlays for each concern.
	},
	//	categories of games
	//		landscapes walker
	//			has to not be boring like twitter animation
	//				interesting world
	//				interesting colors
	//				interesting camera work
	//				interesting animations (both character and world)
	//		things and objects
	//		intricate animation
	// video game challenges for the player

	//just genre combos
	dragAndDropResourceGame: {
		// The goal of the game is to create desirable things through transformations.
		// The player drags and drops things between different station
		// in the stations things transforms into new things.

		//	
		//	A drag and drop crafting system.

		// The visual theme can be
		// plants,
		// metals,
		// robots,
		// food,
		// people at hotel
		// potions

		// any sort of crafting or transformation context.
		//	multitasking cooking game kinda.
	},
	learningAndIntroductionPhaseOfGame: { //videoGameEntryPoint //Tutorial
		// when somebody starts playing a game part of what they are doing is  Figuring out what game am I playing
		// They are figuring out how to interpret the visuals.
		//
		// they are figuring out teh language, the literacy of the game.
		//
		// ^ This is why consistency is super important
		//
		//
		// How does this game actually work?
		// What am I supposed to be doing?
		// What am I supposed to be responding to?
		// What is important and what is not important?
		//
		//
		// when you decide a feature and set it in stone
		//	then you can kinda transfer the consistency of that features
		//		when you design other features of the game
		//		
		//		
		// There is some parallelism that must happen between things in games.
		//
		// Games communicate things by highlighting and that highlight must means something
		//	throughout multiple aspects of the game. stay consistent.
		//	
		//	
		// How fun or how good the game feels to play in the beginning
		//	is related to how unconfused they are
		//	
		//
		//Non realism is how you compress ideas into the brain of the player.
	},



	walkingSimulator: {
		// kinda derogatory term for a game without challenge and with low interactivity.
		// usually all you do as the player, is walk through an environment with changing perspective.
	},
	// interface 
	cardgames: {
		// using card mechanic as means of action
		// deck of card
		// shuffle deck
		// look at cards
		// draw x cards
		// pilling cards
		// flip/turn card
		// etc
	},



	//common player actions
	collect: {
		requires: {
			storage: 1,
		},
	},
	pickUp: {
		// common game action/mechanic trope.
		// to transport item from external world to an inventory.
		// often abstract in the sense that it is only proximity based and instant. (you stand nearby, press the right button and wosh it is in your inventory.)

		// fun experiment, make a game where you can bottle people.
		does: {
			collect: 1,
		}
	},
	holding: {
		// things can be held if they fit in the grabber and is solid material?
		// hand is a grabber, pincer is a grabber
	},
	attack: {
		// action with intention of causing harm
	},
	swing: {},
	slash: {},
	freeDirectionalAttackSystem: {
		// pressing attack button alone extends the weapon forward.
		// A directional input decides the direction the weapon is extended
		// releasing the attack button bring the weapon back.
	},
	jump: {
		//	jumping is an impulse force moving an object in the opposite direction of what it is standing on.
	},
	dash: {
		// quick impulse movement. often horizontal or any direction you of player choice.choose
	},
	dodge: {},
	gliding: { //sliding: {
		// surfing on wines/pipes/wires like a Tarzan!!!! in 2d aw yes
	},
	walk: {},
	run: {},
	swim: {},
	fly: {},
	levitate: {},
	dying: {
		//	dying is a system that seizes to function as a whole.
		// (stops functioning as a defined unit or concept)
	},
	climbing: {
		// traverse wall
	},
	swimming: {
		// move through liquid
	},
	flying: {
		// move through air without landing for long periods of time.
	},
	surfing: {
		// gliding along the surface of a liquid.
	},
	// poses...
	crouch: {},
	sit: {},
	layDown: {},
	//	tricks
	handstand: {},
	backflip: {},
	frontflip: {},

	rhythmBasedFightingGame: {
		//rhythm based fighting game.
	},
	murderMysteryGame: { // setting + detective work
		// challenge is critical thinking and deduction
		// logical thinking
		// I need to make a murder mystery game just for the music.
		// if not only for the ace attorney music
	},
	adventureGame: {
		//a game with an adventure
	},
	adventure: {
		// an unusual and exciting or daring experience.
		// an unusual, exciting, and possibly dangerous activity, such as a trip or experience, or the excitement produced by such an activity:
		// put in an unfamiliar place where you learn how to deal within it in order to return to home.
	},
	journey: {
		// traveling between a set of destinations.
		// an act of traveling from one place to another
	},
	videoGameAbstraction: {
		partOf: { //TODO: should be "is"??
			acceptableBreaksFromReality: 1, //make sure this refers to the trope "acceptable breaks from reality"
		}
		// A common visual/symbolic abstraction is that poison clouds are shaped like skull symbols
		// Rule of abstraction - it is OK for game to be non-realistic as long as it saves time and meaningless effort. todo add to acceptable breaks
		// The game should allow you to try put anything in machines for experimentation.
	},


	// __________ USER INTERFACES _________
	clothingGui: {
		// The drag and drop GUI from animal crossing where you can drag clothes to the icon of the character and then they apply.
		// Would be nice for applying clothes to different areas on the character
		// For example like a stick you pull it over the characters mouth and you see it stick in.
		// Because holding a stick or straw or pipe or something is kinda cool.
	},
	//^ interface for knowledge domain.
	// common video game systems (things that are commonly simulated in games)
	abstractInventory: {
		// when items enters the inventory they are removed from the game world (physical simulation) or the simulation and gets stored in like a abstract platonic idea world plane
		// this is currently the most common inventory system in video games
		// it is used because it is convenient. just suck em up with the vacuum and then they are accessed and represented in the abstract GUI plane.
		// this makes it easier to analyze, overview and organize, as it does not require PHYSICAL work.
		// usually items gets represented by simplified icons
		// counterexamples is a physical storage, like a crate or house, where objects can be put and then transfered
		partOf: { //TODO: should be "is"??
			videoGameAbstraction: 1, //part of, one of,
		}
	},
	circleInventory: { //like the game silver
		// options are presented in a circle
		// select with mouse or with controller stick
	},
	inventoryAnimations: {
		intoPocket: 1,
		intoBackpack: {
			//		throw item up in the air and make it land / vacuum into the open bag.
			//		take off backpack,
			//		unzip it and push item into it.
		},
		bubbleInventory: {
			//			the item is put shrunk into its abstract representation
			//				then trapped inside a bubble
			//				then the bubble is put in a bag of bubbles.
		},
	},

	// Specific inventory -  clear out cool ways of doing inventories
	// Grid based inventory system
	// Area based grid based inventory system (like Diablo)
	// Gridbased means, divided up into discrete units/tiles in a grid pattern.
	// Area based means the inventory is limited in area capacity and items take up area.
	dragAndDropInventoryIntoWorld: {
		// I like the idea of inventory where you god give the characters items out of your GUI.
		// Like they just plop into existence in front of the player. and they're like "hey thanks"
	},
	islandMappingAndNavigationSystem: {
		// i wanna copy the island to island navigation system used in Spiritfarer.
		// together with changing weathers etc.
		// and a timer based approach where you lerp over the ocean and stumble upon whatever is out there.
		// just the idea of going between different cities and trading goods and transporting people....
		// and maybe even multiplayer being like people own islands????
	},
	shop: {
		//a place where you trade
		is: {
			place: 1,
		},
		implies: {
			trading: 1,
			buying: 1,
			selling: 1,
		}
	},
	craftingSystem: {
		//	one example is just put the things in a box and if they match recipe thing crafted

		//Common features
		//Scrapping machine
		//	allows you to decompose things into its materials to be used in crafting.
		//		for example metal objects becoming a chunk of metal 
		//	machine because it can be inputet a bunch of elements according to a recipe that processes over time
		//Crafting machine
		//	has a bunch of recipes that requires a set of inputs and produces an output thing in some time.
		//	machine because it can automatically construct as long as it has a selected recipe and enough o produce another output.
		//	it will only produce if there is enough capacity in the output slot. the output slot can generally only hold one type of item, but can hold a stack if the game rules allow.

		has: { // Crafting Mechanic:
			Resources: 1,
			Processors: 1,
		},

		// A resource that changes a more permanent state in controlled ways.
		//	craft is something specifically made for a purpose.
		//	
		//	conscious forming of oubjects.
		//	
		// Craft - an activity involving skill in making things by hand.
		//	know how of creating objects in a predictable and controlled manner. (Knowing what object you will get and tow to make it before you have it.)
		//	make objects in a skilled way.
		//	
		// The player obtain or gain access to discretely identifiable resources with differing qualities/properties
		// The player changes resources by triggering predictable transformations/processes [if a always become b]
		//
		// Crafting gameplay:
		//	a crafting game has these phases:
		//		1 collect - take things from external world (exploration, whatnot)
		//		2 craft - Create tools with the things,
		//		3 use - then you use them on external environment.
		//	
		// The limit/constraint that makes the player having not completed the game instantly:
		//	The game is not started in the completed state.
		//	processes requires spatial configuration that do not occur on their own.
		//	processes requires time to complete
		//
		// Connection between crafting and motivation
		//	the fun in crafting is based on the love humans have for making things that are useful.
		//
		// Questions for designing crafting games:
		//	How do you collect?
		//	What do you collect?
		//	Why do you craft?
		//
		//	factorio has craft in order to craft other things.
		//	shapez.io -> unlock new goals,
		//		things in shapez doesn't give you more capacities, like energy generation or something.
		//	minefcraft -> better tools, to make better things.
		//		the thing you craft you use directly.


		// Bigger than crafting
		//	motivational failure states:
		//		boredom
		//			if only collect it is just grind. (grind is tiring and boring, makes player quit)
		//			finding things. (in world or through exploration)
		//		dificulty
		//			collecting can be laborious, 
		//				problem of rarity (finding) -> solution, things are findable, you only get new things when going to new contexts.
		//				duration of obtaining from environment (mining)
		//				transportation
		//					(carry capacity & distance)
		//
		//	intrinsic motivation is self acclaimed jobs.
		//		(the one directing the goal of the task is the one doing the task)			
		//
		//	There needs to be something that shows how far the player has come. evidence of their progression.
		//		the pride of having created something meaningful.
		//
		// Problem in Valheim is that this crafting loop is in the way of the non crafting aspects of the game.
		//
		// Problems in crafting games:
		//	resource tokenization - Can be collected and exchanged for a something = boring because it's just a grind counter.
		//
		//
		// The nature of crafting games
		//	because crafting is based on effort induced transformations, there is always a trade off (usage distance).
		//		making X from Y means you don't have Y anymore. and it is useful for [set of things]
		//		you can often transform a resource in multiple directions for divergent purposes, thus making a resource useful.
		//		
		//		some things take lesser space
		//		some things are harder to contain (maybe specific pressure, temperature, etc, etc)


		// A good way to explain a crafting system, is through a transformation tree (A node diagram with all possible transformations, highlighting transformation requirements)

		// in monsterhunter the challenges (monsters) are the containers of resources you need to build the weapons to kill them.


		// too many base level resources, so that crafting is essentially free. 
		//	The point you have to craft loads of base level resources the game becomes boring. this is where automation should be introduced.


		// There is experimentality in crafting that is a nice form of exploration.
		//	this in contrast to pre-conceived recipes on long lists of menus.

		//	There is a form of freedom, a chemistry system, that if you understand the basic rules of cooking (one piece of ingredient and one piece of y, and no pieces of z)
		//		then you know you can craft food without knowing how good food/weapon/tool/resource/etc you can make.

		//	^ with freedom you also want balance. (no real optimal strategy, but things that are differently good for different contexts)




		// The outputs of a crafting system needs to be useful.
		//	giving you more content (things or world to explore)
		//	making you more powerful. (giving you capacities or more control, less grind, etc)
		//	changing the way you interact with the world


		//	imiately impactful and relevant to the player's needs.

		// The feeling of making things that were slow and involved many steps and manual labor automated.



		// Continuous and digital properties.


		// Saker komer frÅn en sak med en intervall. och samlas


		// Basically opus magnum som man styr manuelt

		// Challenges:
		//	hålla reda på olika resurser i inventory.
		//	building intricate crafting systems. (things that easily break!)
		//	Doing intricate crafting.
		//		timing
		//		moving quickly
		//		moving finely (following lines, not colliding things)
		//	följa många steg för att komma till ett specific state. (And maybe trying to do it fast.) to reach hard to obtain state.
		//	experimentation. (figuring out unknowns)
		//	ibland måste man ta ut saker inom rätt tid.
		//	
		//	limited space.
		//	limited movement (max distances... per turn? per movement. per unit of time)

		//	give the crafting results a skill based component
		//		mashing buttons, timing 
		//		keeping something within a certain level over time.
		//		seeing minute details and inspecting things. (cleaning purifying ingredients.)


		//	dealing with some form of uncertainty, maybe unknown properties of the resources that can be found through inspection.
		//	organization/inventory management?


		// Crafting tropes	
		//	junk resource, failed craft.
		//	recycling - costly process to reconfigure things homogenously.

		// Mouse over resource = mouse is pickable
		// Press mouse on pickable makes resources picked and following mouse.
		// If released outside, the resources goes back  (polish: animated)  to it's original placement space.
		// When released click button in compatible placement space, it is put in that space.

		// A machines reacts when reachign a processing condition
		//	examples of processing conditions
		//		having object in input spot

		// Dropping creates an input

		// There are "delays"
		// There are meters, visual representations of fluid things. for example delays.

		// Resources are identifiable by 
		//	color
		//	shape
		//	icon?

		// Possibly you can hover over resource to see a list of it's properties.

		// Empty processors can be picked up as resources.
		// A processor resource that is dropped in space unpacks into a processor.


		// The diference between crafting and building systems is that systems have pipes or otherwise defined connectors rather than manual drag and drop.


		// Ways to make it more strategic (which s not what I want in this particular game)
		//	With system you can add the cost of moving/connecting/disconnecting parts.
		//	you could add a cost of turns. 

		//	^It becomes strategic when there is irreversibility, because then you have to think before you act to not do mistakes.
		//		strategy is based on irreversibility.
	},
	//add genres:
	technicalSimulationGame: {
		is: {
			simulationGame: 1
		}
	},
	socialSimulationGame: {
		is: {
			simulationGame: 1
		}
	},




	conventionalKeyboardControls: { //moveset//controlscheme
		keyboardAndMouse: { //wasd:{
			moveLeft: "a",
			moveRight: "d",
			moveUp: "w",
			moveDown: "s",
			navigateLeft: "a",
			navigateRight: "d",
			navigateUp: "w",
			navigateDown: "s",
			jump: "spacebar",
			use: "e",

			attack: "mouseLeft",

			inventoryQuickSlots: "1234567890"
		},
		keyboardOnly: { //arrows:{
			// Usually used with
			moveLeft: "leftArrow",
			moveRight: "rightArrow",
			moveUp: "upArrow",
			moveDown: "downArrow",
			jump: [
				"z",
				"spacebar"
			],
			attack: "x",
			use: "x",

			inventoryQuickSlots: "1234567890",

			// UI menu action
			navigateLeft: "leftArrow",
			navigateRight: "rightArrow",
			navigateUp: "upArrow",
			navigateDown: "downArrow",
		},
	}









	upgradeGameConvention: {
		//		sometimes there is a double currency
		//			one currency for building (usually called, gold, money)
		//			and one currency for upgrade, (usually called research points, upgrade points)
		//		sometimes you are only allowed to upgrade in the breaks between waves.
		//			or in a "lobby" or otherwise safe area.
	},
}
