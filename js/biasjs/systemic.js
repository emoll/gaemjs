//systemic.js
systemic = { // systemic and relational properties that are universal/abstract/generic
	// ENGINEERING CONSTRAINTS
	// Systems, engineering, decision making... - design cost value assessment //optimization /decision-making
	cost: {
		// cost is measured in:
		//		increased irreversibility
		//		energy loss
		//		time loss (delay)

		// cost is the manifestation of undesired behavior.
		// cost arises as you value certin behaviors over others.
	},

	behaviour: {
		//behaviour is the way in which things change.
	},


	// Nature of sustained change of same type over time:
	FeedbackLoop: { //about systems?????
		// A cycle in which output feeds back into a system as input, changing subsequent output. 
		// • A feedback loop is a cause-and-effect chain that forms a loop. There are two types of feedback loops: positive and negative. 
		// • Positive feedback loops amplify output, resulting in accelerated growth or decline. Therefore, positive feedback can be useful for creating rapid change, but perilous because it is difficult to arrest. 
		// • Negative feedback loops dampen output, resulting in equilibrium around a point. Therefore, negative feedback can be useful for stabilization, but perilous because it can be difficult to change. 
		// • Use positive feedback to create change, but include negative feedback to prevent runaway effects. Use negative feedback to resist change, but note that too much negative feedback leads to stagnation. 
		// See Also Iteration • Root Cause • Shaping • Social Trap 

		// Bridges resist dynamic loads using structures and materials that create negative feedback. But the negative feedback built into the 1940 Tacoma Narrows Bridge was no match for the positive feedback between the bridge’s deflection and the wind. The bridge collapsed five months after it opened. 
	},

	//statistical tendencies
	normalDistribution: {
		// A data set in which many independently measured values of a variable are plotted. 
		// • A normal distribution is a data set in which most of the data are close to the average, and the rest are equally distributed on either side of the average. The result is a symmetrical bell-shaped curve. 
		// • Normal distributions are found everywhere—annual temperature averages, stock market fluctuations, student test scores—and are thus commonly used to determine the parameters of a design. 
		// • Do not design for the average person. For example, a shoe designed for the average foot size would fit only about 68 percent of the population. 
		// • Aspire to create designs that accommodate 98 percent of a population. While it is sometimes possible to design for everyone, this generally results in diminishing returns. 
		// See Also Convergence • MAFA Effect • MAYA • Selection Bias 

		// The measures of men and women are normally distributed. The wide range of measures across the distribution illustrates the problem of simply designing for an average person. 
	},
	paretoPrinciple: { // 80-20 principle
		// A high percentage of effects in any large system is caused by a low percentage of variables.
		// • Proposed by the economist Vilfredo Pareto, who observed that 20 percent of the Italian people possessed 80 percent of the wealth.
		// • Applying the 80/20 rule means identifying and focusing resources on the critical 20 percent. Focusing on aspects of a design beyond the critical 20 percent yields diminishing returns.
		// • For example, 80 percent of a product’s usage involves 20 percent of its features; 80 percent of a product’s bugs are caused by 20 percent of its components.
		// • Use the 80/20 rule to assess the relative value of elements, target areas of redesign and optimization, and focus resources in an efficient manner. Note that 80/20 is just an estimate. The actual percentages can be more or less (e.g., 70/30, 90/10).
		// See Also Cost-Benefit • Feature Creep • Hanlon’s Razor Normal Distribution • Ockham’s Razor

		//pareto principle kinda implies that: significant improvements are often measured in order of magnitude.
		//you find something critical when you're improving an aspect ten-fold or more.
		//
		// you use 1 out of 5 tools 4 out of 5 times.
		//
		// 20% of tasks will fix 80% of problems.
		//.2*.2 = 4%
		//.8*.8 = 64%
		//
		//.04*.04 = 0.16%
		//.64*.64 = 41%
		//
		// 1/1000 tasks will solve 41% of the problems
		//
		// this is the true benefit of repeating the patterns that work.
		//
		// aka in complex system there is always a gem.
		//
		//
		// pareto is a statement about property independence.
	},
	affordance: { //The physical characteristics of a thing influence its function and use.
		// The lack of inertia of an object for achieving a defined state. 
		// The cost or loss/investment of energy required to affect an object in a defined way.
		// TODO merge with my own notes about affordance and simplify.
		// • The form of a thing makes it better suited for some functions than others. For example, wheels afford rolling, and negatively afford being stationary.
		// • The form of a thing makes it better suited for some interactions than others. For example, buttons afford pushing, and negatively afford pulling.
		// • When affordances are good, things perform efficiently and are intuitive to use. When affordances are bad, things perform poorly and are hard to use.
		// • Design things to afford proper use, and to negatively afford improper use. When affordances are correctly applied, it will seem inconceivable that a thing can function or be used otherwise.		
	},
	emergence: {

	},

	//of behaviour...
	rootCause: {
		// The key initiating cause in a sequence of events that leads to an event of interest. 
		// • Most problems, especially difficult problems, have multiple causes, and their proximal causes are rarely their root causes. The root cause is the key event in a cause-event sequence that leads to a problem. 
		// • Asking “why?” an event occurred five times (plus or minus) is an effective way to identify root causes. 
		// • For example: Why did the welder burn herself? She wasn’t wearing protective clothing. Why wasn’t she wearing protective clothing? It was hot in the room. Why was it so hot? The air conditioner was broken. The root cause of the accident was a broken AC. 
		// • Focus on root causes when troubleshooting problems. Use the five whys technique to identify root causes and other elements in the causal chain. Since asking why can lead to infinite regress, focus on actionable causes that create the majority of the effects. 
		// See Also 80/20 Rule • Confirmation Bias • Errors • Visibility 

		// A partialblame￼ cause map exploring root causes for the sinking of the RMS Titanic. It is oversimple to the iceberg. 
	},

	//behavioural tendencies
	garbageInGarbageOut: {
		// The quality of system output is largely dependent on the quality of system input. 
		// • The “garbage in” metaphor refers to two categories of input problems: type and quality. 
		// • Problems of type occur when the wrong type of input is provided; for example, entering a phone number into a credit card number field. 
		// • Problems of quality occur when the correct type of input is provided, but with defects; for example, entering a phone number into a phone number field, but entering it incorrectly. 
		// • Avoid garbage-out by preventing garbage-in. Use affordances and constraints to minimize problems of type. Use previews and confirmations to minimize problems of quality. 
		// See Also Affordance • Confirmation • Constraint • Errors Feedback Loop • Signal-to-Noise Ratio 

		// The Mars Climate Orbiter disintegrated in the Martian atmosphere in 1999. The cause? Garbage in-garbage out. Trajectory corrections were entered in English units versus the required metric units, dooming the craft. 
	},



	SaintVenantsPrinciple: { // Local effects of loads on structures have negligible global effects. (the localization of effects) //effects tends to be bound by spatial and temporal proximity.
		//  • Proposed by French mathematician and engineer Adhémar Barré de Saint-Venant. 
		//  • Load effects at one point of concentration on a structure become negligible a short distance away from that point. For example, overtightening a bolt deforms just the region that is near the hole. 
		//  • Locate loads closer than 3–5 characteristic lengths to merge load effects. For example, spacing bolts less than 3–5 bolt diameters apart creates a weld-like join. Separate loads more than 3–5 characteristic lengths to keep load effects isolated. 
		//  • Secure structural and mechanical systems by controlling 3–5 characteristic aspects of a system. For example, the anchored section of a cantilevered arm should be 3–5 times the length of the cantilever. 
		// See Also Factor of Safety • Redundancy • Satisficing 

		// stress is more uniformly distributed the further from the stress point.

		// Bolts spaced within 3-5 bolt diameters apart (top) have overlapping stress cones, forming a weld like connection. Bolts spaced farther apart (bottom) have negligible impact on one another. Both effects are due to Saint-Venant’s principle. 
	},



	abbe: {
		// the cost of errors compound. no matter how small the errors are.
		// it is good to fine tune very accurately early.
		// It is good to retune and re-evaluate as you progress.
		// especially test right before presentation or execution. to be sure it will hold up to action.
	},


	singlePointOfFailure: {
		// weakspot
		// is any non-redundant part of a system that, if dysfunctional, would cause the entire system to fail.
		// Opposite of rudancy...
	},
	weakestLink: { // 80 20 there is a small part of the structure that is essential to its function. protect it.
		// An element designed to fail in order to protect more important elements from harm. 
		// • Weakest links work in two ways: halting systems when they fail (e.g., electrical fuse), or activating mitigation systems when they fail (e.g., fire suppression system). 
		// • Properly designed, the weakest link in a chain is the most important link. 
		// • Weakest links predictably and reliably fail first. 
		// • Weakest links are applicable to systems with cascading fault conditions—i.e., when there is a chain of events that can be interrupted. 
		// • Consider adding weakest links to systems where failures occur as cascading events. Test and verify that weakest links only fail under the appropriate, predefined failure conditions. 
		// See Also Errors • Factor of Safety • Feedback Loop Modularity • Redundancy 

		// Crumple zones are one of the most significant automobile safety innovations of the twentieth century. The front and rear sections of vehicles are designed to crumple in a collision, reducing the energy transferred to the passenger shell. 
	},

	// abundance, error proofing
	factorOfSafety: {
		// The design of a system beyond expected loads to offset the effects of unknown variables and prevent
		// failure. 
		// • Increasing the factor of safety of a design is a reliable method of preventing catastrophic failure. 
		// • The size of the factor of safety should correspond to the level of uncertainty in the design parameters.
		// High uncertainty requires a high factor of safety. 
		// • New designs use high factors of safety because uncertainty is high. The focus is on survivability. 
		// • Tried-and-true designs use low factors of safety because more is known. The focus shifts to cost. 
		// • Use factors of safety in proportion to your knowledge of the design parameters and the severity of the consequences of failure. 
		// See Also Design by Committee • Errors • Modularity Structural Forms • Weakest Link 

		// The O-rings of Challenger’s solid rocket boosters had a factor of safety of three—inadequate for low temperature conditions. Catastrophic failure occurred shortly after launch in 1986.
	},
	Redundancy: {
		// Using back-up or fail-safe elements to maintain system performance in the event of failure. 
		// • Redundancy is the most reliable method of preventing catastrophic failure. 
		// • When the causes of failure cannot be anticipated, use different kinds of redundancy; for example, having both a hydraulic and a mechanical brake. 
		// • When the causes of failure can be anticipated, use more of the same kinds of redundancy; for example, using independent strands of fiber to weave a rope. 
		// • When performance interruptions are not tolerable, make redundant elements active at all times; for example, using additional columns to support a roof. 
		// • When performance interruptions are tolerable, make redundant elements passive but available; for example, having a spare tire in the event of a flat tire. 
		// See Also Crowd Intelligence • Factor of Safety • Modularity Saint-Venant’s Principle • Weakest Link

		// Ample￼
		// redundancy ensured that Super Cow did not get blown off of his thirty-story perch during season. 
	},






	// add to design method
	// Research



	// Measurement...??? This is really about inability to measure certain things without infering with the things.
	uncertaintyPrinciple: {
		// Measuring things can change them, often making the results and subsequent measurements invalid. 
		// • Measuring sensitive variables in a system can alter them, undermining the validity of the results and the instrument of measure. 
		// • For example, event logging in computers increases the visibility of how the computer is performing, but it also consumes computing resources, which interferes with the performance being measured. 
		// • The uncertainty introduced by a measure is a function of the sensitivity of variables in a system and the invasiveness of the measure. Beware using invasive measures, for they can permanently alter system behaviors and lead to unintended consequences. 
		// • Use minimally invasive measures of performance whenever possible. Remember the maxim: Not everything that can be counted counts, and not everything that counts can be counted. 
		// See Also Abbe • Feedback Loop • Garbage In-Garbage out 

		// Many weight-loss programs promote keeping a food journal to lose weight—i.e., writing down everything you eat. Why does this work? Tracking what you eat changes how you eat. 
	},

	SignalToNoiseRatio: {
		// The ratio of relevant to irrelevant information. Good designs have high signal-to-noise ratios. 
		// • In communication, the form of the information—the signal —is sometimes degraded, and extraneous information—the noise —is added. 
		// • Signal degradation occurs when information is presented inefficiently: unclear writing, inappropriate graphs, unnecessary elements, or ambiguous icons. 
		// • Maximize signal-to-noise ratio in design. Increase signal by keeping designs simple. Consider enhancing key aspects of information through techniques like redundant coding and highlighting. Minimize noise by removing unnecessary elements, and minimizing the expression of elements. 
		// • Signal strength occurs when information is presented simply and concisely, using redundant coding, and highlighting of important elements. 
		// See Also KISS • Ockham’s Razor • Performance Load

		// Reduce noise in graphs by removing unnecessary elements and quieting the expression of elements.
	},





	detail: {

		// A detail is a part of something that has at least one unique aspects from what it is a part of.

		// an individual fact or item.
		// details means the parts have individually unique aspects. (a part with individually unique aspects)
		// it is a detail that one grass of strand is unique and different from another grass of strand.
		// attention to detail is the ability to notice details.
		// details are appreciated when they are meaningful.
		// too much detail is redundant and unnecessary
		// humans usually want important details to be consistent over time.
		// what is important is slightly subjective.
		// details can be generated procedurally as long as you remember the seed from which is was generated.
	},


	// Moderation and simplicity - Control, curation, less is more
	constraint: { // restriction:?
		// Definite modification or applied rule or enforcement that changes the flow/development of action/change

		// Constraints create consistency (consistencies).

		// constraints are just rules that are enfoced to some degree. A rule is just the idea of what should happen for a given condition
		// maybe we can give up on mechanics once and for all and call it constraints?


		// Limiting the actions that can be performed to simplify use and prevent error. 

		// Limiting the actions that can be performed to simplify use and prevent error. 
		// • There are two types of constraints: physical and psychological. 
		// • Physical constraints limit the range of possible actions by redirecting physical motion. The three kinds of physical constraints are paths, axes, and barriers. 
		// • Psychological constraints limit the range of possible actions by leveraging the way people perceive and think. The three kinds of psychological constraints are symbols, conventions, and mappings. 
		// • Use constraints to minimize errors. Use physical constraints to reduce unintentional inputs and prevent dangerous actions. Use psychological constraints to improve the clarity and intuitiveness of a design. 
		// See Also Affordance • Confirmation • Control • Errors Forgiveness • Mapping • Nudge 

		// Common examples of physical and psychological constraints.


		// constraints alters affordances.
	},




	// Systems design
	modularity: {
		// Managing system complexity by dividing large systems into smaller, self-contained systems. 
		// • Modularity involves identifying groups of functions in systems, and then transforming those groups into self-contained units or modules. 
		// • Modular designs are easier to repair, scale, and upgrade, but are significantly more complex to design than nonmodular systems. 
		// • Most systems do not begin as modular systems. They are incrementally transformed to be modular as function sets mature. 
		// • Modular designs encourage innovation. They create opportunities for third parties to participate in structured design and development. 
		// • Consider modularity when designing complex systems. Design modules that conceal their complexity and communicate with other modules through simple, standard interfaces. 
		// See Also Cost-Benefit • Flexibility Trade-Offs • Self-Similarity

		//	 Google’sopen￼ - Project Ara is reinventing the cell phone using modularity. This should reduce costs and innovation. 
	},






	// 7. Simulation & metaphysical
	persistence: { //Memory, SAVING and heuristics
		// The conditions of which something is loaded in and out of memory and remanifested in the product/game.
		// When is it required to keep existing in memory and remanifest?

		// presented as stable over time. especially after exiting the current context and then coming back the same in a later context.

		// persistence of activity means that interactions are simulated of screen.
		// For example if there is a war in a village and the player goes away from the village. When the player returns the village should have suffered the consequences of its condition be in an "aftermath" stage.
		// the plot should go on in the background...


		// What are the dynamics of persistence?

		// "The frog's persistence is that whenever a fly is on the screen the frog will manifest within 40 seconds"
		// ^the above rule is one of contextual consistency. in this context x always happens.
		// consistency has partially therefore to do with normalcy/precedence


		// Relevant questions to determine what to apply persistence for:
		// Which things are worth remembering? //(This is partially related to plot (storytelling). Where things introduced or set-up early should be meaningful later.)
		// 	To what accuracy/detail is it worth remembering?
		// Which things are worth simulating in the background?
		// 	To what extent are they worth simulating int he background?


		// What is the persistence of smoke?
		//	As long as smoke has relatively little effect on game-play or experience it is not worth simulating.


		// Conventions:
		// As long as it has relatively little effect on game-play or experience it is not worth simulating.
		// It is all about priorities.... relatively unimportant things are not worth caring about.


		// If it only serves for aesthetics, then it is not worth simulating in the background. //if the point is the experience rather than re-experience.
		// But if is serves a "mechanical purpose" in the game, then it is probably worth simulating......


		// Style is often persistent throughout the product.
		// The plot set-up is often persistent throughout the product.
		// Core mechanics

		// to some extent, what the player thinks is important is what should be persistent.
		// What is persistent dictates what the game is oriented around.



		// There is a pretty general rule of persistence that things that are in the screen should be seen.


		// Magic is the illusion of persistence.


		// What makes persistence preferable in certain situations?
		//	when you want the world to feel real. and consistent.
		//	it creates traces of the player's actions which is interesting. to see the memory of your actions... (self-determination theory?)
		//	lets you continue in the state/context where you left of. (Lets you experience continuation.)
		//	lets you go away from something that takes time and then you come back to it. (For example things that are not worth observing or paying attention to)


		// Is this also persistence?
		//        Persistent - hard to budge (or alter) over time.
		//            examples
		//                persistence: 50%
		//                persistence: 100%


		//    things only need to recurrence with accuracy if it is relevant for the player experience.


		// Saving is to create an event which you can go back to. 

		//    Rank everything that can be saved with these factors and restrictions then act upon what is computationally reasonable:
		//    I must define importance of preservation, certain values must be more resistant to change, aka the program must be biased to change specific things less than others.
		//    SAVING - there should be different levels of persistence of artifacts dependent on
		//        Saving
		//        Reentering world
		//        This should mostly depend on
		//            memorability of artifact
		//                contrast
		//                uniqueness
		//                usefulness
		//                How much it has been used or adjusted manually
		//            Recency - How long since it was last experienced
		//            Memory size or computational restrictions.
		//            Saving time???
		//            (if online saving, then network throughput is relevant)
		//    ^ the saving point is really a point about axioms, You should simplify that which is least important but most costly to perform


		// Persistence deepens experience
		//	As it stays over time the experience gets stronger in sense
		//	Persistence is there to remind the player of what is important over time.
		//	To create the aesthetic of familiarity.
		//	Spaced repetition of what to take with them from the experience.
		//	Persistence is there to show a thing in different lights over time. Developing that which is familiar.
		//	You remember things so that you can use then wisely later when the opportunities rise.
		//
		//	You forget things because ...
		//	They are not contextually relevant.
		//	They disturb the clarity of beauty or the aesthetic.
	},
	consistency: { // magic circle, Magic Model (model of consistency) AKA coherence: {//??},
		// Has to do with stability of state/condition

		// Relationships between nodes are maintained and unchanging.
		// Persistence = transformations are consistent in presentation.

		// Google definitions:
		//	Acting or done in the same way over time, especially so as to be fair or accurate.
		//	Without logical contradiction (paradoxes) //(of an argument or set of ideas) not containing any logical contradictions.

		// Consistent:
		// adjective
		// 1. acting or done in the same way over time, especially so as to be fair or accurate.
		//		"the parents are being consistent and firm in their reactions"
		// 2. (of an argument or set of ideas) not containing any logical contradictions. //or: not containing any paradoxes
		//		"a consistent explanation"

		// ... so predictable behaviour.
		// persistent over time...

		// consist:
		// verb
		// 1. Be composed or made up of.
		// 2. Be consistent with.


		// Consistency seems to be what is static/unchanging in the relationship between one or more subjects/objects/identities.


		// Consistency is of style? similarity in pattern over space and time.
		// ^same meaning.

		// Consistency, what it consists of...


		// The consistency between two elements is what they have in common. (or what is static in their relationship as archetypes to each other over time)
		// Similarity on the other hand has to do with the amount of consistencies between two elements
		// Consistency is thus more about the similarity of the elements or aspects or parts of something, rather thn similarity of a whole.


		//	Consistency is important to understand the meaning of the symbols and words we are served
		//		if we say, no a hand is just a spider web in this universe, then the word loses all symbolic meaning
		//			existing in the audiences mind.
		//	Consistency is important in order to establish and work with the mental models of the subjects mind.


		//	When it is established its helpful that it stays the same. (is persistent or logically consistent)
		//	It can seem like the author forgot and therefore the story lost an element of goodness
		//		broke expectation negatively...

		// "One thing I love about the world is that it is ever moving
		// Even if we do not see stuff happen
		// Everything just moves and happens naturally "off screen""

		purpose: {
			// Increases fairness
			// Increases accuracy
		},

		opposite: {
			// unpredictable
			// unfair.
			// unreliable
			// untrustworthy.
		},


		// Conservation of state/data/information:
		// What can (and can’t) you change in runtime without the player calling out your bullshit.
		// How can you manage the hardware limitations through managing the attention and action and experience of the player?
		//
		// A model of memorability too…
		//
		// General rules:
		// Don’t change that which currently is perceived. Don’t alter graphics on screen, don’t change sounds that are playing. Don’t change the consistency of what the objects are in the narrative.
		//
		// Don’t go over the edge of the built/presented consistency. (Don’t break immersion)
		//
		// Do all these magic tricks to hide paradoxes, bugs and errors from the player.
		//
		//
		// (Beauty only matters if it can be perceived and is perceived (consciously or subconsciously))
		//
		//
		// Hide things
		//	Outside screen,
		//	example: Trap player in area to enforce not looking outside screen (to not see externally)
		// Obstruct it (visually) //occlude
		//	Underground
		//	Behind objects
		//	Behind mist
		//	Behind reflection
		// Cammoflague it
		//	"It was there all along, it was just hard to see"
		//	basically create noise where it could be, but it could just be something else that looks like it...
		//		(a red herring)

		// ^ why? In order to (a method for):
		//	Having “multiple outs”
		//	Save performance
		//	Slightly alter how it looks???
		//	Load things
		//	Ditch things
		//
		// Create distraction
		//	Glimmer
		//	Glare
		//	A mission/quest
		//	Danger
		// In order to:
		//	Nudge player, buy time
		// generate things when needed
		//	aka pretend they where hidden there all along
		//	you can find a lot of random things in a rubble
		//		anything that can contain or obclude things has potential to contain literally anything.
		//		That is why I choose to live three dimensionally, as all directions are alway obstructed by objects
		//		hiding the infinite potentials behind it.
		//
		// Simulation:
		//	Use fake props 
		//	Reuse the same prop
		//	Use mirrors (blit the same data)
		//	Upscaling
		//	Downscaling
		// In order to:
		//	Quick placeholder reality
		//	Save work/data/computation/resources
		//	Avoid hyber intricate situations/systems
		//	Be more critical about where to put performance.

		// Ways for things to disintegrate or disappear
		//	Cover up
		//		Smoke
		//		Light
		//		Object
		//		Edge of screen
		//	Transformation
		//		Pop
		//			Like bubble
		//			Like star
		//		Become
		//			Sand
		//			Smoke
		//			Water

		// Palm - hold in something that looks empty
		// Ditch - to secretly dispose of dispose of an unneeded object
		// Steal - to secretly obtain a needed object
		// Load - to secretly transform the object to what and where it needs to be.
		// Simulation - make the impression that something that has not happened has
		// Misdirection - to lead attention away from secret movements.
		// Switch - to secretly exchange an object for another. (re use the same object, or use a placeholder...)

		typesOfConsistency: {
			// really just 2 dimension... functionVsAesthetic and internalVsExternal

			aesthetic: { //aesthetic consistency
				// consistency of style and appearance
				// (font, color, graphics, logo, symbols,)
				// enhances recognition, communicates membership, sets emotional expectations.
			},
			thematic: {
				// kinda the same as aesthetic but more about consistency of selection of things... 
				// like the object you use at Halloween that are sorta independent of any style.
			},
			genre: {
				// genre consistency
				//	following trends of other fictional works and genres
				//	familiarity of neighboring space, making it easy to get into and understand the art.
				//	use tropes, especially the ones in the most related genres.
				//	borrow properties from the greatest's art.
				//	A "dragon" when stated should be the following the general conventions of what a dragon is.
				//	aka words have contextual meaning and please use the language I understand.
			},
			functional: {
				//consistency of contextual reaction over time.
				//when I put in A I always get B.

				// consistency of meaning(intention???) and action
				// of sequence and expected results (cause and effect)
				// lets people use apply previously known patterns to operate the system.
			},

			internal: {
				//with other elements within the system

				//internal consistency
				//	what you break when things does not stay the same in-universe
				//	people get especially frustrated with internal consistency
				//		when it violates the stated premise or goal of the game/product/series
				//		
			},
			external: {
				//with elements in the environment of the system
				//example: your game looking and feeling like other's games.
				//your car looking and feeling like the general conception of a car

				//external consistency (according to TV Tropes)
				//	consistency to the real (external) world
				//	"it should be like reality unless noted" -said every baby ever.
			},
		},

		consistency: { //From the book Universal Principles ofDesign... Describes the value of consistency. these concept can probably merge.
			// Usability and learnability (knowledge transfer) improve when similar things have similar meanings and functions. 
			//	consistency is when similar things have similar meaning and function. (shared interface? common user interface. things are used in the same way.)
			//	usability and lernability improves with consistency.

			// consistency has to do with similarity and chunking. 
			// consistency has to do with non changing.

			// • Consistency enables people to efficiently transfer knowledge to new contexts, learn new things quickly, and focus attention on the relevant aspects of a task. 
			// • Make things aesthetically consistent to enhance recognition and communicate membership. 
			// • Make things functionally consistent to leverage existing knowledge about functionality and use. 
			// • Make things internally consistent to improve usability, and to signal that a thing has been well designed and not cobbled together. 
			// • Make things externally consistent to extend the benefits of internal consistency to other things. 
			// • Use consistency in all aspects of design. When design standards exist, generally follow them—but remember: “A foolish consistency is the hobgoblin of little minds.” 
			// See Also Back-of-the-Dresser • Feature Creep • Mimicry Recognition Over Recall • Similarity 

			// Consistency enables international travelers to understand traffic signs even when they don’t speak the local language. 
		},




		// time consistency... players remember previous consistency. it is therefore good to stick with previously chosen values. like for example if orange meant strong, keep that convention going throughout the game.
	},
	appearance: { // TODO: rename, this has to do with appearance through time. I think the word appearance on its own inaccurately represents the concept.
		// RELATED TO CONSTANCY
		continuous: 1, // Can't instantly change form, but needs to tween between them
		inconsistent: 1 // Not continuous, appears differently in each context, does not strictly follow the same rules every time.
	},

	// About pattern
	pattern: {
		// regularity, harmony,
		// organized in a predictable manner.

		// exactly the same but in different contexts.

		// A pattern is stronger the more commonly it appears. (the more occurences it has)

		// feels predictable.
		// humans seek patterns and we like to complete incomplete patterns. (partly because completion is satisfactory)
		// patterns are less noisy, which is easier for the brain, makes it easier to process.

		// repetition of data
		// repetition legitimizes.
		// repetition legitimizes.
		// repetition legitimizes.
		// There is something about repetition that gets the human eye interested.
		// There is something about repetition that gets the human eye interested.
		// There is something about repetition that gets the human eye interested.

		// repetition is the validation of a pattern

		// art has patterns. a style is a set of patterns.

		// Adherence to patterns creates predictability.
	},
	similarity: { //TODO: merge with distanceVsProximity
		// having things in common
		// sharing properties
		// 100% means identical while 0% means practically nothing in common? (I am not sure if anything can be 0%)

		// TODO:, make sense of qualitative similarity vs quantitative similarity
		// qualitative is non-numerical, deals with essences, commonality in labels?
		// qualitative, matching classes, finding relevant labels... so how much they fit under similar labels.
		// quantitative is measurable on same scale. How something compares in a chosen essence.

		// Things that are similar are perceived to be more related than things that are dissimilar. 
		// • One of the Gestalt principles of perception. 
		// • Similarity indicates and reinforces the relatedness of elements. Lack of similarity indicates and reinforces differences among elements. 
		// • Color similarity is an effective grouping strategy when there are no more than 3-4 colors, and the design is still usable by the color blind. Size and shape similarity are effective grouping strategies when elements are clearly distinGUIshable from one another. 
		// • Design elements so that similarity corresponds to relatedness. Design unrelated- or ambiguously-related items using different colors, sizes, and shapes. 
		// • Use the fewest colors and simplest shapes possible for the strongest grouping effects, ensuring that elements are sufficiently distinct to be detectable. 
		// See Also Chunking • Consistency • Mimicry • Self-Similarity 

		// The Tivo remote control uses buttons of different colors, sizes, and shapes to reduce complexity and improve usability. 
	},
	distinct: { //aka archetypal... I might just say shape:{archetypes:{...}}
		shape: {
			square: 1,
			circle: 1,
			triangle: 1,
			rectangle: 1,
			ellipse: 1,
			octagon: 1,
			hexagon: 1,
			halfMoonShape: 1,
		},
		color: {
			//distinction is like defined by hue distance so this could be done with a function
		},
		// moon
		//	can be seen something with the sun
		//	can be clearly seen during night.
		//	is mostly bluish white
		//	glows in the dark sky
		//	generally monochromous.
		//	occasionally different color/hue (red, yellow, orange, green, purple, whatever)

	},
	unique: {
		is: {
			count: 1,
			distinct: 1,
		}
	},
	comparison: {
		// (optimizing for human comparison) /comparisons is what all art and experiences is composed of...
		// A story or narrative is a linear sequence of comparisons over time.
		// highlighting difference.
		// A method of highlighting relationships by depicting information in controlled ways. 
		// • One of the most compelling ways to present evidence is to compare things apples-to-apples, within one eyespan, and against familiar benchmarks. 
		// • Comparing things apples-to-apples means comparing the same variables in the same way. 
		// • Comparing things within one eyespan means presenting data side by side so that differences are easily detectable. 
		// • Comparing things against benchmarks means providing context so that the significance of differences can be understood. 
		// • Use comparisons to highlight relationships and present evidence. Ensure that the variables compared are apples-to-apples, presented within one eyespan, and contextualized with appropriate benchmarks. 
		// See Also Framing • Garbage In-Garbage Out • Layering Signal-to-Noise Ratio • Visibility 

		// Florence Nightingale’s Coxcomb graph used comparison to make the case that the predominant threat to British troops was not the Russians, but cholera, dysentery, and typhus. 
	},

	diversity: {
		// Number of different or unique things.
		// Quantity of different things.
		// ... quantity of things that are uniquely different?
		// 4 unique things can be more diverse than 50 different things...

		// conceptual width? conceptual dimensionality.
		// Diversity goals for interesting art/compositions (I genuinely do not remember what I meant by this)
		// Quantifiable diversity level:
		//	distinct types of plants
		//		1 - 1
		//		2 - 1.5
		//		3 - 3
		//		4 - 10
		//		5 - 17
		//		6 - 40
		//		Consistent compositions
		//		Diverse compositions
		//		Consistent format
	},

	phenomena: {
		// Probably merge with some else like occurrence or something
		// Pust like phenomena as word
	},


	//engineering the study and management of consistent behaviour

}
