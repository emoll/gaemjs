var worldy = {

	// the narratives and worldly and spatial consistency
	World: { // world should the describe what a generic world is, not the current world
		has: {
			spatialDimension: {
				count: 2
			},
			landscape: 1,
		},
		contains: {
			physical: 1 // All I am trying to say here is that the world contains physical objects... maybe exclusively physical things?
		},
		etc: 1,
	},
	landscape: {
		is: {
			composition: 1, // A landscape is a composition of elements that make out at location in a world.
		},
		has: {
			places: 1,
		},


		//	Non repeating and unique
		//	Often existed to be memorable in video games
		//	Natural continuation of area
		//	nature
		//		an area dense of nature's elements
		//		area exclusively containing nature's elements...



		//		Put in a way that is interesting, memorable and practical.
		//		Landscape is architecture
		//		It creates feelings
		//		Big and small
		//		Cramped and open
		//		A landscape tells a story
		//		A landscape had been naturally formed over millions of years.

		// Make things stick out of the land
		// I mean like sticks, and rocks and stuff should be just straight up stuck down into the sand and dirt 
		// Or a sword struck in a tree or something you know.
		// Or old houses or ruins or statues buried in sand the sand, speaking of the history of the landscape.

		//how would locals build on the existing natural landscape?
	},
	landmarks: {
		//sticks out of the surroundings and is locally very unique for the surrounding environment, making it easy to orient against (know where you are)
		lighthouse: 1,
		statue: 1,
		bigTree: 1,
		ruin: 1,
		mountain: 1,

		// rules of contrast
		// weirdly shaped or deformed
		// has a crack in it
		// ruin
		// rusty
		// abandoned.
		// asymmetrical (symmetry contrast. aka different in terms of symmetry to its surroundings)
	},
	microLandmarks: {
		// landmarks that helps you navigate when very zoomed in.
		// a landmark is a unique looking recognizable presentation, that creates a unique identification for where you are in the world.
		// micro landmarks in time perception makes time seem more detailed. (more time between time.)

		// trees
		// bushes
		// some rocks
		// monolith
		// weapon stuck in something (axe in stump or a sword in the ground etc)
		// doors
		// paths
		// stumps
		// bulletin board...
		// sign.
		// fireplace.

	},


	places: {
		// every place has a theme. and thus even its elements become part of it as a landmark.
		// everything on the beach is beach like
		// this also creates internal consistency.

		// it is also good to spatially map the difference in rules and flavors of your game.
		// this makes your game  become more of a theme park where the player can choose where to go to satisfy their needs.
		// which generates player autonomy and freedom

		// elevations: flatland, mountain, whatever
		// layers
		// rockground: rock, mountains, caves
		// sticky material (water, mud, soil/dirt),
		// dropped material and grown (rocks, houses, plants and trees),
		// windy/moving/dynamic materials (sand, dust, soot, rain)


		// every place has its own aesthetic.
		// places are the units of landscape.
		// it is a good chunk (see chunking) of landscape.
		// when generating/composing places think about:
		//	number of poles (how many centers are there in the city where the biggest stuff is)
		//	asymmetry(is the pole in the center or slightly tilted towards one end)
		//	self similarity
		//	
		//	intent and history (why are things built here, what functions do they serve.)
		//	
		//	
		//	
		//	^these are all about natural variability. (variability seen in nature.)

		// when designing a landscape and world, you can start with a set of places in relationships that would be interesting.
		// a city and a volcano.
		// examples:
		// human/artificial structures
		city: 1,
		port: 1, //like sea port or air port
		abandonedCity: 1,
		camp: 1,
		tunnel: 1,
		bridge: 1, //almost too small.. maybe more of a pure landmark
		castle: 1,
		farm: 1,
		bigBoat: 1,
		largeBuilding: 1,
		importantHouse: 1, //maybe it is a mansion, or a tower or just where you live. this area is defined by this house. Often characterized as the housing of a important character.
		temple: 1,
		church: 1,
		pyramid: 1,

		//natures formations
		volcano: 1,
		mountain: {
			//a rock that is a landscape.
			//landscape made out of rock that protrudes from the ground
		},
		cave: 1,
		dungeon: 1,
		island: 1,
		desert: 1, //this is almost too big, it is like a biome. but even if desert is big it is often so barren that it feels like the same amount of content as a place.
		savanha: 1,
		forest: 1,
		abyss: 1,
		beach: 1,

		//		ice (blocks of ice)
		rainland: 1, //(a place where it always rain. think Britain, or that biome in BOTW.)
		rainforest: 1,
		space: 1,
		underwater: 1,
		mudfield: 1,
		garden: 1,
		labyrinth: 1,
		underground: 1,
		//		around a black hole
		//other worlds...
		dream: 1, //		in a dream world
		//		between world
		//		another world or dimension
		//		lava lake (tectonic plates
		//		waterfall?
		//		mushroom
		//		a huge skeleton...
		//		Or a graveyard
		//		a place that is just a pile of things.
		//		pile of bones
		//		junkyard pile of trash
		//		GIANT things:
		//		A tree that is a whole continent
		//		a world that is a ring (halo)
		//		a giant tree
		//		a giant fruit
		//		a giant animal
		//		in a mountain
		//		[biome] + [place]
		//		forest + ruin = overgrown forest ruins.
		//		house + desert = sanded house.. lol
	},
	clicheTropeEnvironments: { // Top 10 cliche environments in video games: ("themes")
		outerSpace: 1,
		lavaLevel: 1, // Volcano or hell
		snowlandLevel: 1,
		jungle: 1,
		forest: 1,
		swamp: 1,
		savannah: 1, // Kinda underused...
		desert: 1,
		beach: 1,
		underwater: 1,
		boat: 1,
		underground: 1, // Dungeon / cavern / tomb
		laboratory: 1,
		factory: 1,
		scrapyard: 1,
		hauntedHouse: 1, // Mansion/castle
		graveyard: 1,
		urban: 1, // Gritty urban, (streets punks, mutants, )
		medievalSettlement: 1, // Medieval city
		town: 1,
		ruins: 1,
		mushroom: 1,
		pirate: 1, //!(ship / town / island)
		sewers: 1,
		farm: 1, // Countryside
		angelic: 1, // Cloud city
	},

	portal: {
		// Singular point/shape/area connecting two otherwise isolated  interior/worlds/places
		// Allowing people/objects/entities to pass freely between the two.


		// Different from magical portal which is a specific aesthetic for a portal


		// Use howl's door as a way to navigate and make 2d exploration nice.
		// This is just like fez teleport which allows you to travel between distinct worlds or areas.
	},




	dream: {
		// Figment
		// dream worlds ungraspable where you might never return 
		// but only have a vague memory of
	},





	dungeon: {
		// a cave with beginning and end, filled with enemies???
		// is this a real definition?
		// the main thing with a video game dungeon, is that it is indoors, kinda like a labyrinth, filled with challenge
		// often related to specific missions.
		// each dungeon often has a back-story of what it is for
	},
	// Under water caves and caverns
	//		Bounciness property for physics things woooo


	artificialLandscapeBump: {
		// landscape wave
		// make a bumb that moves through the landscape.
		// this super interesting way to make landscapes that changes over time
		// like sand dunes. but all grounds. heightening and sinking random landscapes.
	},

	slope: {
		//			smooth landscape that goes slightly or gravely downward
		// perfect to surf or glide on, if the situation afford it!
		//glide gliding
	},

	hole: {
		// Emptied area of a solid
		// Can be made by digging
		// Digging can be made with limbs or with tools.
		// Things that are in a hole that is filled are buried.
		// Buried seeds grow
		// Buried roots grow
		// Organic material buried in soil rots and is filled with worms.
		// Worms can be dug up.
		// When digging you sometimes find buried secrets
		// Usually it is just dirt and rocks
	},
	cover: {
		// stand in front or atop of to obstruct, acting as a physical barrier.
	},
	buried: {
		// You can find things that are buried by digging them up
		// Covered by porous(?) material....
		// Covered by small grained material like sand, gravel, rocks, ash, soil, dust, etc.
	},


	island: {
		// dry land separated/disconnected from other dry land by another substance that is not land
		// most commonly water.
		// land is ground that is not submerged in liquid.
	},


	peak: {

	},
	hill: {

	},
	valley: {
		//a low area of land between hills or mountains, typically with a river or stream flowing through it.
	}






	// instances
	shadowWorld: {
		//i should search this in TV Troples lol
	},
	underworld: {
		// uhh.. can be metaphorical or like hell or like a dungeon.
	},
	spiritRealm: {
		//like heaven or hell?? i should search this in TV Troples lol
	},
	mirrorWorld: {
		// also called inverted world
		// upside down castle.
		// free reuse of content.....
	},



	mountain: {
		//huge tall rock material formations that come out of the ground.
	},

	ruin: {
		// broken/partial building or piece architecture, often ancient. 
		// in the state of disintegrating.
		// "the broken parts that are left of a building or town or construction or architecture that has been destroyed"
		// which surpassed the test of time.

		// implies previous settlement and discontinued active culture

		// which material is left is based on 
		// hardness of material - rocks last longer than wood....
		// environmental accessibility - in the desert it is more common with sand stone....
		// accessible technology - even if stainless steel might be harder than rock, if this tech did not exist, it would not be left.
		// size of the original structure - really small things have easier to deteriorate completely. while a huge build there will most likely be something left.
		// for most games I think stonebricks....
	},
}
