//Style and artistic technique
const styleAndArtisticTechnique = {
	art: {
		// Non-motivated functions
		//			Basic human instinct for harmony, balance, rhythm. (beauty)
		//			Experience of the mysterious 
		//			Expression of the imagination - Unlike words, which come in sequences and each of which have a definite meaning, art provides a range of forms, symbols and ideas with meanings that are malleable.
		//				aka it is informal nature allows an endless freedom of expression. Creating new contexts that might be more desirable than what we are currently bound by.
		//			Ritualistic and symbolic functions. - participate in art in order to feel.
		// Motivated functions
		//		Communication.
		//		Entertainment - Art may seek to bring about a particular emotion or mood, for the purpose of relaxing or entertaining the viewer. 
		//		The avant-garde
		//		Art as "free zone" - A place where you can do as you want without being judged. erase the differences between people
		// 		Art for social inquiry, subversion and anarchy ()
		//		Art as therapy - psychological and healing purposes, (art therapy)
		//		Art as influence/manipulation - propaganda, commercialism. Change moot or feeling. framing.
		//		As framing artist as impressive

		// idealism - conveying something greater beyond.
		// realism - ability to accurately reproduce features of nature. (whether sounds, visuals, behaviours, etc)
		// art's role as the communication by artifice of an essential truth that could only be found in nature
	},

	aesthetic: {
		//aka collection of adjective scalars (TODO: find and label every adjective as "is adjective"...)

		//an aesthetic is a certain taste. a certain feel. what characterises how something (object of consistency) is sensed or felt, in contrast to other things
		//it's sensual archetype.
		has: {
			unique: { //differentiating / characteristic: {
				sensation: 1,
				emotion: 1,
			},
		},
		//anything that can be felt in an emotionally unqiue and identifiable way, is an aesthetic.

		//The aesthetic always trigger the same underlying basic sensations for every perciver
		//but one aesthetic can induce different feelings for different subjects

		//examples and trivia:
		//math isn't an aesthetic. mathematical is. or math themed could be.

		//	sometimes underpinning an aesthetic is a philosophy:
		//		consumerism
		//			aka appearance specifically due to coming from a specific way of thinking and thus a specific way of acting.


		// aesthetics are communicated willingly or unwillingly.

		//questions to figure out aesthetic: how was it like?


	},
	// does [action] by/through [method] when [condition] // this is like a reverse of if statements., and it makes more sense, when happy do poop.

	style: {
		// A style is a preference in behaviour and expression
		// A style is the consistency between expressions. (expressed behaviours)

		// A style has a bias in what things it is concerned about that is not strictly functional.
		has: {
			//philosophy: 1, //???
			//personality: 1, // ???
			//values: 1,
			//themes: 1,
			//topics: 1,
		},
		is: {
			//perceived
			//expressed
		},

		// what affects aesthetics/style? Everything that is presented.
		// The audience's perception of the product and its nature and behaviour over time. (and how this is consistent in internal relationality)
		//	based on what is memorable and emotional

		// the strength/intensity of a style.
		//	how consistent is it: how strict is it adhering to patterns and rules. how much is it changing over time or from situation to situation.
		// orderlyVsChaotic  //	rulebasedVsRandom

		// everything that is drawn that is not strictly geometry has a style.
		// What are the dimension of style?


		// stylistic critical decision
		//	anime style thing
		//	constant line thickness
		//	//in anime they have a constant line thickness, this is why characters get so silly when they are far away.
		//	It is a nice stylistic decision.


		// Style - Dictates what elements meet:
		// actions, symbols, 
		// shapes, curves, thickness, contrasts,  colours, emotions, 
		// patterns
		// relationships,

		// behaviour and mannerisms. the variation in methods to achieve the outcomes/functions.

		// A style applies to a whole. It applies to generalities. Like a whole game or a whole level or a whole faction, etc.

		// Style is like the great filter for noise.


		// There is something about style that is more about feeling than function.
		// Or maybe experience... style is the type of experience. (only indirectly function, where as something functional has directed systemic effect)
		// It is like on top of function. additive. particular adaptation for something archetypal?
		// It is style when it does not affect function. It is design when balances function and style.

		// Style: how you present things and everything that affects a presentation.

		// Style
		//	Personality, identity
		//	Quirks. habits. biases. demeanour.
		//	
		//	
		//	A style is how a personality want to do things and be expressed.
		//	
		//	
		//	Way of doing things
		//	
		//	Based on philosophy and values. Valuing specific needs over others.
		//
		//	Which creates a consistent pattern.
		//	
		//	Coming as reaction to a set of styles and incorporating specific styles
		//	Evolved out of previous styles, where you keep the best that fits the current zeitgeist.
		//	
		//	The do's and do not to reach the destination
		//	Method. way of doing things.
		//	
		//	
		//	What subjects you prefer to depict (symbols of the style)
		//	The ways and tools you depict with
		//		color limitations
		//		gradience
		//		brushes
		//		outlines and thickness
		//		
		//		realism vs symbolical. 
		//		
		//		level of detail
		//		level of abstraction
		//		
		//		
		//	Imitation of nature (baroque decoration)
		//	Imitation of math (modern decoration)
		//	Imitation of psychedelic experience
		//	Imitation of dreams.
		//		
		//	HOW YOU DECORATE THE GOALS YOU PURSUE AND THE FUNCTIONS YOU FULFIL
		//	
		//	
		//		a style can have a theme. (recurrence
		//		
		//	Theme can be developed during the play of a game... getting more and more defined.
		//	
		//	Common themes: loss,
		//	Beauty of simplicity
		//	Chaos and order
		//	Circle of life
		//	Coming of age
		//	Facing darkness
		//	Facing reality
		//	Fading beauty
		//	Faith versus doubt
		//	Good versus bad
		//	Immortality
		//	Loss of innocence
		//	Power of silence
		//	Rebirth
		//	Role of men
		//	Role of women
		//	Self – inner and outer
		//	Life
		//	Nature
		//	Oneness
		//	
		//	A theme tells how it is like (for people)
		//	A theme is often a focus on a type of experience. What is it like to go through a type of experience as a person.
		//	
		//	A theme is what the author believes about a topic... and topics are like, "love, loss, anger, ..."
		//	A theme is like. "the author believes that valuing security too high can lead to missing out on important life experiences."
		//	A theme is the central idea of a piece of literature.
		//		a message or meaning, a life lesson.
		//		something that applies to all of us, that is universal.
		//	A theme is an an opinion about a subject/topic/thing
		//		Love is not a theme, love sucks is a theme.
		//			a subject/topic together with an experience of it.
		//		themes have to be a little bit interesting and non obvious.
		//	
		//	Style, how you use things, how you wear clothes and put them together.
		//		and use things as a form of self expression. (of themes and philosophies and personalities.)
	},


	// Composition and natural world building
	composition: {
		// Composition is beautiful artistic a play of meaningful distances between things.

		// The artist determines what the center of interest (focus in photography) of the art work will be, and composes the elements accordingly.
		// The gaze of the viewer will then tend to linger over these points of interest, elements are arranged with consideration of several factors (known variously as the principles of organization, principles of art, or principles of design) into a harmonious whole which works together to produce the desired statement – a phenomenon commonly referred to as unity. 

		// Organizing the elements of art  according to the principles of art
		// Some principles of organization affecting the composition of a picture are:
		//	Shape and proportion
		//	Positioning/orientation/balance/harmony among the elements
		//	The area within the field of view used for the picture ("cropping")
		//	The path or direction followed by the viewer's eye when they observe the image.
		//	Negative space
		//	Color
		//	Contrast: the value, or degree of lightness and darkness, used within the picture.
		//	Arrangement: for example, use of the golden mean or the rule of thirds
		//	Lines
		//	Rhythm
		//	Illumination or lighting
		//	Repetition (sometimes building into pattern; rhythm also comes into play, as does geometry)
		//	Perspective
		//	Breaking the rules can create tension or unease, yet it can add interest to the picture if used carefully

		// I need a method to figuring out the most compelling compositions I can make with my engine.
		//    I know factors like:
		//        colorful
		//        with a quality color palette
		//        consistent patterns
		//        Clear lines
		//        Familiar
		//        Emotional
		//        Human
		//        face
		//        hair and clothes
		//    Things a painter can not normally do.
		//        infinite patterns
		//        flawless or perfect shaping
		//    Things that are costly for a painter or takes the most time:
		//        precision, patterns of many elements
		// Create a theory of placeability (restrictions in placement to create good compositions)
		//    Placable area/point definitions (where can the algo put things? where is it likely to put things? ) #Could #Code 
		//        Examples:
		//            Grows on:
		//                Rocks
		//                Sticks
		//                Branches
		//                Leaves
		//                etc
		//            Only grows in:
		//                Heat
		//                Moisture
		//                Sunshine
		//    Placeable area/point definitions (where can the algo put things? where is it likely to put things? )
		//        Examples:
		//            Grows on:
		//                Rocks
		//                Sticks
		//                Branches
		//                Leaves
		//                etc
		//            Only grows in:
		//                Heat
		//                Moisture
		//                Sunshine


		// visually chunking related elements that are presented.
	},
	placement: { // placeability: // how to place objects and characters in a world
		// bad example:
		// Put player character in an idle standing position on a room's floor.
		// playerCharacter:{
		//	is:{
		//		location:1,
		//	}
	},
	environmentalConstraints: { // obviously a bad name but I do not know what right now
		// basically what defines the place?
		//		indoors vs outdoors
		// open space vs cramped space

		// setting: nature vs city

		// earthlike vs alienlike

		// concepts for accessibility and non accessibility
	},
	techniquesForGoodComposition: {
		// composition techniques
		alignment: { // Positioned (with edge) to a line. Part of composition. TODO add to composition...
			// The placement of elements such that edges line up along common rows or columns, or their bodies along a common center.		
			// Alignment of elements creates a sense of unity and cohesion
			areaAlignment: { //positioning center of gravity/mass along a line.
			},
		},
		figureGround: {
			// Elements are perceived as either figures (objects of focus) or ground (the rest of the perceptual field). 
			// • One of the Gestalt principles of perception. 
			// • The human perceptual system separates stimuli into figure elements or ground elements. Figure elements receive more attention and are better remembered than ground elements. 
			// • Elements below a horizon line or in the lower regions of a design are more likely to be perceived as figures. 
			// • Elements above a horizon line or in the upper region are more likely to be perceived as ground. 
			// • Increase the recall of key elements by making them figures in a composition. Clearly differentiate between figure and ground elements to focus attention and minimize perceptual confusion. 
			// See Also 3D Projection • Closure • Signal-to-Noise Ratio
			// 		
			// Placing elements below a horizon line or at the bottom of a page makes them figures, which them more memorable. 
		},
		highlighting: { // Emphasis

			// A way to highlight is to make everything else less prominent/noticeable/noteworthy. which hightens the contrast...

			// A technique for focusing attention on an area of text or image. 
			// • Highlighting focuses attention by varying elements (e.g., capitalization) or adding elements (e.g., color).• Highlight no more than 10 percent of a visible display. When everything is highlighted, nothing is highlighted. 
			// • Use bold to highlight titles, labels, and short word sequences. Use italics and uppercase text for more subtle highlighting. Use underlining sparingly, if at all. 
			// • Use color to highlight long word sequences. Ensure high contrast when using color to highlight. 
			// • Animation such as blinking is a powerful means of attracting attention. Accordingly, it should only be used for alert-type information that requires an immediate response. It is important to be able to turn off the animation once it is acknowledged, as it distracts from other tasks. 
			// See Also Interference Effects • Layering • von Restorff Effect 

			//￼ An example of BOLD  UPPERCASE AND ITALICS and COLORed text used differently for the text:
			// Chapter 8
			// The Queen turned crimson with fury, and, after glaring at her for a moment like a wild beast,
			// screamed “Off with her head! Off —”
		},
		chunking: {
			// Grouping units of information to make them easier to process and remember. 
			// • The term chunk refers to a unit of information in short-term memory—a word, or a series of numbers.
			// Information that is chunked is easier to remember. 
			// • For example, most people cannot remember a ten-digit number for more than 30 seconds. However, by breaking the number into three chunks (i.e., such as a phone number) recall performance is equivalent to recalling one 5-digit number. 
			// • The original estimate of the maximum number of chunks that can be efficiently processed and recalled was 7±2. The modern estimate is 4±1 chunks. 
			// • Chunk information when people are required to recall and retain information. In high-stress environments, chunk critical controls and information in anticipation of diminished cognitive performance. 
			// See Also Mnemonic Device • Performance Load Recognition Over Recall • Signal-to-Noise Ratio 

			// This book uses multiple levels of chunking to make design principles easier to read, understand, and remember.
		},
		closure: { //human pattern recognition, shape language, visual communication
			// A tendency to complete incomplete or interrupted forms and patterns. 
			// • One of the Gestalt principles of perception. 
			// • Patterns that require closure for completion grab attention and are considered more interesting. 
			// • Closure likely evolved as a form of threat detection, enabling human ancestors to reflexively detect partially obscured threats, like snakes in the grass. 
			// • Closure is strongest when patterns are simple and elements are located near one another. 
			// • Use closure to reduce complexity and increase the interestingness of designs. When designs involve simple and recognizable patterns, consider removing areas to trigger closure in viewers. 
			// See Also Flow • Threat Detection • Zeigarnik Effect 
		},
		negativeSpace: {
			//	Leave enough negative space for you to be able to make sense of contours.
			//	The empty room around your subjects that is "silent" and contrasting.
			//	Use patterns, gradient or randomness (clouds, fog, texture, whatever) to fill these spaces.

		},
		layering: {
			//????
		},
		framing: { //(matching geometry to the bounds of the screen according to geometric rules)
			//	put things around the edges
			//	align the golden ration spiral towards center.
			//	divide into a 3 by 3 grid.
		},
		pointOfFocus: { //compositional point of focus.
			//
		},
		secretDetails: { //beautiful secret hidden details. tiny happy occurances/coinsidences.
			//
		},
		grouping: {
			//grouping ???
		},
		//
		hiearchyOfIsolation: { //hierarchy of isolation
			//	isolate the core subject(s)
		},
		//
		contrast: { //composing for great contrasts
		},
		beautifulAssociations: { //compose for beautiful associations.
		},

		//http://neiloseman.com/5-principles-of-cinematography-we-can-learn-from-turner/
		ruleOfThirds: { //the rule of thirds. divide camera view into a 3 by 3 grid.
			//	place things along the lines and along the corners where the lines intersect.
		},
		leadingLines: { //leading lines (our eyes are drawn along lines.)

		},
		frames: { //create frames within the composition which divides sets of elements.
			//	"Your frame can be man-made (bridges, arches and fences), natural (tree branches, tree trunks) or even human (arms clasped around a face)."
		},
		blur: { // blurring things that are too close or too far (sorta focusing, highlighting)
			//
		},
		asymmetryVsSymmetry: 1,
		senseOfScale: { // you create depth by showing the perspective. put big thing up close in the foreground to give a sense of scale inside the picture.
			// Use objects that are easy to make sense of how big they are, banana for scale or something familiar like a penny or a man made object, or a tree or whatever.

			// Play with scale
			// By making the player small, a plant that fills the screen feels huge.
			// With a small scales character you can give the impression of
			// Massive buildings, empty landscapes.
		},
		focalPoint: { //sun, or a focal point which shots out straight lines in all directions and aligning things to the lines that goes to this point creates interesting composition.
			//	like the lines when you draw in 3d perspective.
		},
		repetition: { //repeating patterns and textures are pleasing to the eye.
			//	I guess it is a bit like a repeating melody in a music song.
			//	repetition legitimizes.
			//	patterns lets you visit the same thing in slightly different contexts (lighting, dirt, color, etc) creates familiarity.
		},
		ruleOfOdds: { //rule of odds: it is more visually appealing with odd number of subjects. (and objects of a kind?)
			//	"An odd number of elements is seen as more natural and easier on the eye."
			//	it is easier to choose what to focus on.
			//	I think this is kinda a point about asymmetry.

			// something something too much conformity to patterns is boring and stale. it becomes to formal, too confusian, too Aspberger
			// "the antidote to chaos is the balance between chaos and order. and that manifests itself as the instinct for meaning."
			// ordered enough to be secure but exposure to enough chaos that you are learning and being renewed. //TODO add to flow
			// chaos degenerates into nihilism and hopelessness.
			// order degenerates into tyranny
			// natural environments has balance between order and chaos

			// if you are not allowed to think badly then you are not allowed to explore thought at all.
			// we need free speech because everyone starts out foolish and ignorant.
			// only through acceptable foolish interaction with feedback can we grow as people
		},
		simplification: {
			// Images with clutter can distract from the main elements within the picture and make it difficult to identify the subject. By decreasing the extraneous content, the viewer is more likely to focus on the primary objects. Clutter can also be reduced through the use of lighting, as the brighter areas of the image tend to draw the eye, as do lines, squares and colour. In painting, the artist may use less detailed and defined brushwork towards the edges of the picture. Removing the elements to the focus of the object, taking only the needed components.

			// can be done through curation

			//foreground background
			//highlight
		},
		//https://petapixel.com/2016/09/14/20-composition-techniques-will-improve-photos/
		opposingForces: { //opposing forces
			//	elemental contrasts are interesting, dark light, good evil, warm cold, etc.
		},
		rhymes: { //subjects or elements that kind of rhyme.
			//	grass is sorta like a bush and a bush is sorta like a tree and by being together in their similarity they are interesting.
			//
			//	this is sort what makes collections interesting. bunch of elements that share certain unspoken properties enough to look interesting together.
			//	like a collection of weapons. or a collection of round things. or whatever.
			//
			//	this might seem to break the rule of odds (odd numbers) but it works even in pairs when the repeating element is evidently not exactly the same.
		},
		goldenTriangles: { //golden triangles
			//	similar to 3 x 3 grid
			//	but it is a straight line from left bottom corner to top right corner and then lines sticking out it. (look it up I am to lazy to explain properly)
			//	align lines with the lines, align objects into the created section by these lines. it is a good ways to divide things interestingly.
		},
		FibonacciSequence: {
			// A sequence of numbers that form patterns commonly found in nature. 
			// • A Fibonacci sequence is a sequence of numbers in which each number is the sum of the two preceding numbers (e.g., 1, 1, 2, 3, 5, 8, 13...). 
			// • Patterns exhibiting the sequence are commonly found in nature, such as flower petals, spirals of galaxies, and bones in the human hand, and as such, are conjectured to represent proportions with universal appeal. 
			// • The division of any two adjacent numbers in a Fibonacci sequence yields an approximation of the golden ratio. Approximations are rough for early numbers in the sequence but increasingly accurate as the sequence progresses. 
			// • Consider Fibonacci sequences when developing compositions, geometric patterns, and organic motifs, especially when they involve rhythms and harmonies among multiple elements. 
			// See Also Golden Ratio • Self-Similarity • Wabi-Sabi 

			// Le Corbusier derived two Fibonacci sequences based on key features of the human form to create
			// the Modulor, a model used to optimize spaces for humans.
		},
		goldenRatio: { //beauty and related to fibbonacci
			// A ratio within the elements of a form, such as height to width, approximating 0.618. 
			// • The golden ratio is commonly believed to be an aesthetically pleasing proportion, primarily due to its unique mathematical properties, prevalence in nature, and use in great artistic and architectural works. 
			// • The prevalence of golden proportions over other proportions may, however, be illusory—a result of cherry-picked evidence and confirmation bias. 
			// • Psychological evidence supporting a preference for golden proportions over other proportions is weak.
			// If a preference exists, it is small. 
			// • Explore golden ratio proportions in your designs, but not at the expense of other design objectives. 
			// See Also Confirmation Bias • Fibonacci Sequence Rule of Thirds • Selection Bias • Waist-to-Hip Ratio 
			// ￼
			// In each of these classic forms, the ratio between the blue and red segments approximates the Golden ratio.

			//	fill the boxes of the golden ratio with interesting things.	or align things with the spiral. (see alignment)
		},
		goodContinuation: {
			// The tendency to perceive lines as continuing their established directions. 
			// • One of the Gestalt principles of perception. TODO: read up on the gestalt principles of perception.....
			// • Things arranged in a straight line or a smooth curve are perceived as a unit, making them easier to visually process and remember. The line is perceived to continue in its established direction. 
			// • When sections of a line or shape are hidden from view, good continuation leads the eye to continue along the visible segments. If extensions of these segments intersect with minimal disruption, the elements along the line are perceived to be related. As the angle of disruption becomes more acute, the elements are perceived to be less related. 
			// • Use good continuation to indicate relatedness between elements in a design. Arrange elements in graphs and displays such that end points of elements form continuous, rather than abrupt lines. 
			// See Also Alignment • Area Alignment • Chunking • Closure Similarity • Uniform Connectedness 
			// ￼
			// The first graph is easier to read than the second because the end points of its bars form a line that is more continuous.
		},
		selfSimilarity: {
			// A property in which a thing is composed of similar patterns at multiple levels of scale. 
			// • Natural forms exhibit similarity at multiple scales. Accordingly, self-similarity is often considered a universal aesthetic. 
			// • People find self-similar forms beautiful, especially when the mathematical density of the pattern resembles savanna-like environments and trees. 
			// • Self-similar modularity is an effective means of scaling systems and managing complexity. 
			// • Explore self-similarity in all aspects of design: storytelling, music composition, visual displays, and engineering. Use it to enhance aesthetics, manage complexity, and scale systems. 
			// See Also Archetypes • Hierarchy • Modularity Savanna Preference • Similarity • Symmetry 
			// ￼
			// The Mona Lisa photomosaic, the African acacia tree, fractals, and Roman aqueducts all exhibit self-similarity. 

			// fractals and self similarity is beautiful.
			// self similarity, big thing, smaller things near it, even smaller things around the smaller things, etc.
			// is a form of pattern
		},
		ruleOfThirds: {
			// A technique of composition in which a medium is divided into thirds. 
			// • The rule of thirds is derived from the use of early grid systems in composition. 
			// • Application of the rule of thirds generally results in aesthetically pleasing compositions. 
			// • Divide a medium into thirds, both vertically and horizontally, creating an invisible grid of nine rectangles and four intersections. 
			// • Position key elements at the points of intersection on the grid, vertical elements to vertical lines, and horizontal elements to horizontal lines. 
			// • Do not use the rule of thirds when a composition is symmetrical and contains one dominant element. Centering in this case will increase dramatic effect. 
			// See Also Alignment • Area Alignment • Golden Ratio Symmetry • Wabi-Sabi 
			// ￼
			// This photo from the “Thrilla in Manila” makes excellent use of the rule of thirds, placing the heads of both fighters at opposing intersections on the grid. 
		},
	},
	TechniquesForCameraControl: { //this is partially related to timing and time related things. //cameraWork:
		zoom: {
			//how to camera zooming
			//	Kan du hjälpa mig med ett problem? Jag vill kunna zooma in och ut från en punkt i ett kordinat system på så vis att kameran fokuserar på samma kordinat i mitten av skärmen men all geometri ska förstoras/förminskas på rätt sätt och hamna på rätt ställen på skärmen.
			//	Hur ska jag räkna ut skalan på geomtrin och punkterna de ska ritas ut på givet jag vet kamerans koordinat Kx, Ky och ett värde för skala S, där 2 är dubbelt så stort och .5 är hälften etc.
		},
		//	(look into cinematography)
		//	get good shots
		//		get stuck with the frame on interesting world compositions
		//			conveniently make the world cool.
		//
		//	Generally any thing that obstructs the player should fade out so as to reveal the player. (I mean the character and its gear, do not make them naked.)
		//
		//
		//	zoom out enough to give context.
		//	zoom in enough to create focus.
		//
		//
		//	try to not overwhelmed (like that doom map or whatever where they talk about multiplayer map and their complexity)
		//
		//
		//	camera movement
		//
		//	point of focus
		//
		//
		//	when to cut between scenes.
		//
		//	camera shake
		//
		//
		//	know when to rotate or swing the camera 
		//
		//
		//	aligning rotation to the rotation of an object.
		//
		//	the rule of thirds. divide camera view into a 3 by 3 grid.
		//		place things along the lines and along the corners where the lines intersect.
		//
		//
		//
		//	think about camera angle, are you
		//
		//	looking from below, creates awe, like it is important and big, grand.
		//	looking from above makes it small, makes you feel on top, like you are distanced.
		//
		//	looking from the side makes you feel levels with the subject. 
		//
		//	etc.
		//	https://petapixel.com/2016/09/14/20-composition-techniques-will-improve-photos/
		//
		//
		//	Rule of Space
		//		leave space in the direction the focal movement are going towards.
		//		it is more interesting to look ahead where things are going.
		//		the space implies where there is room for things to move to.
		//
		//
		//	left to right rule (bias)
		//		we prefer left to right movement because we are used to it.
		//		we read images like we do text.
		//what types of camera movements do I want?
		//	stand still
		//	zoom in
		//		infinite zoom!
		//	zoom out
		//	smoothed movement
		//	camera rotation (upside down flip, etc)
		//	linear movement in one direction
		//		loop around
		//
		//	Any camera movement that isn’t smooth is essentially called a cut.
		//	Cuts are usually related with instant jumps in space and time.

		//Using vertical space to create effect, like falling through a hole destroying everything on the way down and being trapped at the bottom of a well.
	},

	asymmetryVsSymmetry: {
		// A property of visual equivalence among elements. 
		// • Symmetry is the most basic and enduring aspect of beauty. It is ubiquitous in nature. 
		// • There are three basic types of symmetry: reflection, rotation, and translation. 
		// • Reflection symmetry refers to mirroring elements around a central axis or mirror line. 
		// • Rotation symmetry refers to rotating elements around a common center. 
		// • Translation symmetry refers to locating equivalent elements in different areas of space. 
		// • Use symmetry to convey balance, harmony, and stability. Use simple symmetries when recognition and
		// Recall are important. Use combinations of symmetries when aesthetics and interestingness are important. 
		// See Also Area Alignment • MAFA • Self-Similarity • Wabi-Sabi 
		// ￼
		// The Notre Dame Cathedral uses multiple, complex symmetries to create a structure that is as
		// Beautiful as it is memorable. 

		// Asymmetry speaks to incompleteness
		// Symmetry speaks to perfection and completeness.

		// Asymmetry is beautiful when it is almost symmetrical. you see the rules of symmetry but they are slightly crooked which gives it personality.
		// Symmetry creates balance.

		// Symmetry - it is a costly signaling that it can generate patterns and reproduce order and therefore high quality. //same with pattern, reproducibility.
		// Symmetrical face is is more likely to belong to a healthy and fertile partner.
		// Symmetry signals it is as it should be.
		//	And repetition legitimizes. repetition creates a pattern. and a pattern can be incorporated and understood.
		//		which gives us usable insights.
		// Asymmetry - to balance the symmetry. it signals differentiation. a collection of different things.
		// Asymmetry creates a power dynamic. powerdynamics are inherently interesting.


		// Aproximate symmetry is when symmetry is achieved by putting element on each side that form almost the same shape but are composed of different things.

	},
	imbalanceVsBalance: {
		// Art definition:
		//		Balance is used to illustrate the visual weight of an image.
		//		A carefully balanced image lends a sense of stability to a photograph. An unbalanced image creates disunity or unrest.

		// We either like
		// A good contrast (see contrast)
		// Or a good mirroring
		// If there is absence of both, it feels unbalanced.

		// We want things of similar weight on both ends.

		// If an artwork consists of one pole to one of the side and nothing on the other, it feels unbalanced. (because it is framed as polar.)

		// Imbalance makes it more interesting and up to interpretation and mysterious and unsettling.
	},
	contrast: {
		// Contrast is when two or more opposing elements are present
		// Light vs dark
		// warm vs cold
		// Sharp vs soft
		// Big vs small
		// Something vs nothing

		// Present the contrast which dynamics are important to learn about in the moment.

		// Everything gets defined why what is contrasts from.
	},

	cameraStyle1: {
		//framed shots that you go through Gorgoa style.
		//no camera work only static scenes..
	},






	colorLanguage: {
		// color language:
		// certain colors evoke moods.
		// certain groups of colors evokes certain moods.
		//
		// yellow - joy and happiness, sick, intense
		// green - safety
		// red - dangerous and sexy
	},
	shapeLanguage: {
		// Square is boring - stability, trust, stubbornness,
		// Circle is fun - friendly bouncy soft, warm, welcoming, happy
		// Triangle is repulsive - edginess, intensity, danger, intensity, speed.
		shapeMotif: 1, //shape motif is to only use one of the above for defining the silhouette of a character
		// It is good to add unique shape to head, makes the character easily identifiable and clearly communicates the direction they are heading
		//
		// Pokemon is based on all their monsters being identifiable from just the silhouette.

	},









	// __COLOR__


	//color, light and tones
	colorRelatedHumanBiases: {
		is: {
			bias: 1
		}
		//human color biases: A simple style always will appear to people like it has less colors,
		//a complex style always will appear to people like it has more colors.
		//Monochrome always less than full chroma.

		//heuristic for deciding number of colors??????
		//
		//https: //twitter.com/cyangmou/status/1346343631972667392
	},

	// TODO COLORS
	// Beautiful color palettes and combinations.  #Must #Research
	// Color/hue control based on palettes. #Must #Research 
	// Option to draw everything restricted to color scheme. #Code #Must 
	// Beautiful interpolations with more than 2 colors. #Design #Code #Should 
	// A process to generate more color schemes from images. #Design 
	colors: {
		// all "value" (color intensity) should be different.
		// basically if you make the character grayscale they should still be dynamic in value.
		// An object needs to fit its background
		// try your character/object on multiple backgrounds.
		// make a generic colorscheme that will work with all the backgrounds that it will encounter.
		// or change the colors of th character/object dynamically to fit the environment.
	},
	colorSpace: {
		//	define color names and defines areas of color of where it corresponds.
		//	define how general a color is.
		//
		//	Red is very genera
		//	blue is very general
		//	green is very general
		//
		//	define how to think about RGB and HSV etc
		//
		//	How to use the color wheel well, how to divide into opposites and tri-tones and quart whatevers
		//
		//	How to find complimentary colors
		//
		//	color contextuality, how a set of colors serve different roles depending on
		//		what other colors they are presented together with.
	},
	colorScheme: {
		//color restriction
		//color guideline
	},
	colorPalette: {},
	ColorsRelate: {
		// A set of colors has a unique meaning
		// When presented together they exemplify a very defined context of reality.
		// You can guess what a palette of colors will be used to depict.
	},
	colorTheory: { // compose according to color theory etc.
		// TODO: make sure this is actually what color theory is
		green: { //add to color theory...
			// Cognitive and behavioral effects triggered by exposure to the color green. 
			// • Green is universally associated with safety and security, perhaps a vestige of our arboreal ancestry, and the reason that green traffic lights around the globe mean “go”. Additionally, green is associated with nature and sustainability. 
			// • Green environments reduce stress and mental fatigue, and promote problem solving and creativity. 
			// • Green apparel tends to negatively impact physical attractiveness. For example, a person in green is rated less attractive than all other colors except yellow. 
			// • Use green to promote associations with nature and sustainability. Use green when creating signage and controls to indicate safe passage. Consider green interiors to reduce stress and promote creativity. 
			// See Also Biophilia Effect • Gloss Bias • Prospect-Refuge Savanna Preference • Yellow Effects 

			// A hospital hall redesigned with ample greenery to comfort patients as they move from the lobby to their destination. 
		},
		White: { //color theory
			// Cognitive and behavioral effects triggered by exposure to the color white. 
			// • White is universally associated with goodness and security, likely due to an evolved association with daytime and a reduced vulnerability to predators. 
			// • White signals passivity, submission, and cleanliness. 
			// • Sports teams wearing predominately white uniforms are perceived to be less aggressive and less likely to cheat than those wearing dark colors. Accordingly, they incur fewer penalties. 
			// • White products are generally perceived to be classy, high value, and timeless. 
			// • Use white to increase perceived value in products, and perceived authority in people. Consider white to create a sense of peacefulness and submission. 
			// See Also Archetypes • Black Effects • Red Effects Supernormal Stimulus • Threat Detection
			//
			// Archetypal heroes wear mostly white to signal their goodness, but with a dash of black to make them tough and intimidating. 
		},
		yellow: {
			// Cognitive and behavioral effects triggered by exposure to the color yellow. 
			// • Yellow is the most visible color to the eye, likely the result of an evolved sensitivity for detecting ripe fruit. 
			// • When people eat a diet rich in fruits and vegetables, carotenoids give their skin a subtle, but detectable, yellow glow. This glow increases attractiveness. 
			// • Yellow pills and chemicals are perceived to be stimulative and energetic. 
			// • Yellow legal pads and yellow stickies may foster concentration and problem solving. 
			// • Yellow clothes decrease attractiveness in both males and females more than any other color. 
			// • Use yellow to grab attention, signal energy and potency, and promote problem solving. When attractiveness is key, generally avoid yellow apparel. 
			// See Also Green Effects • Supernormal Stimulus • White Effects 

			//Painting fire trucks yellow versus red reduces the risk of visibility-related accidents by three times.
		},
		Red: { // TODO: add to color theory
			// Cognitive and behavioral effects triggered by exposure to the color red. 
			// • Red makes women appear more sexual. 
			// • Red makes men appear more dominant. 
			// • Wearing red apparel confers a small competitive advantage in sports contests. 
			// • Red increases performance on simple physical tasks, but impairs problem solving and creativity. 
			// • Red promotes competitive behaviors, but undermines cooperative behaviors. 
			// • Use red to increase general attractiveness, gain an edge in competitive contexts, and increase competitive behaviors. Avoid red in environments requiring learning, testing, and cooperation. 
			// See Also Attractiveness Bias • Blue Effects • Priming

			// A lady in red exaggerates her fertility, making her more sexually attractive to males.
		},
		black: { //color theory???
			// Cognitive and behavioral effects triggered by exposure to the color black. 
			// • Black is universally associated with evil and foreboding, likely due to an evolved association with darkness and vulnerability to nocturnal predators. 
			// • Black signals aggression, dominance, and dirtiness. 
			// • Sports teams wearing predominately black uniforms are perceived to be more aggressive and more likely to cheat than those wearing light colors. Accordingly, they incur more penalties. 
			// • Black products are generally perceived to be classy, high value, and timeless. 
			// • Use black to increase perceived value in products and perceived authority in people. Consider using
			// black to signal aggression and intimidate adversaries. 
			// black￼ effects make black dogs less adoptable than lighter-colored dogs. Animal shelters call this dog syndrome”. 
		},
		blue: { //color theory
			// Cognitive and behavioral effects triggered by exposure to the color blue. 
			// • Blue is the world's most popular color, and is commonly associated with water and purity—except in food contexts, where it is associated with spoilage. 
			// • Blue signals friendliness and peacefulness (e.g., color of the United Nations). 
			// • Blue fosters openness and creativity, and promotes aspirational thinking. 
			// • Blue promotes alertness and well-being during the day, but can disrupt sleep at night. 
			// • Use blue when you need a generally popular color, though avoid blue in food-related contexts. Consider blue to promote friendliness, open-mindedness, and aspirational thinking. Use blue lighting to increase alertness, but avoid it in sleep environments. 
			// Blue works best for aspirational messages (e.g., white teeth). Red works best for risk-avoidance
			// (e.g., preventing cavities).
		},

		// Figure out whether or not I need to implement restrictions from color theory in code or what the cost of winging it without that would be.
		//	Consider options, how to restrict by color theory
		//	Color/hue control based on palettes
	},
	symbolismOfColor: {
		blackVsWhite: {
			//polarOpposites.
		}
	},

	colorstrength: {
		// strong color (high contrast, means intensity of substance.
		// often a warning sign in nature. "I am poisonous" or "I want to be eaten"
		// strong colors grab attention. especially when sticking out from surroundings.
		// strong color is like hyper stimuli.
	},
	// Look at the contrasts between black and white and nuances. Super good and meaningful communication.

	colorfulness: {
		// ... containing many colors?
		// diversity of colors?
		// many strong hues.
	},
	hue: {

	},
	saturation: {

	},
	value: {

	},

	transparency: { // opaqueness, alpha, translucence
		// of a material, how much light shines through.
		// how much you can see through an object.

		// Things that makes the desired sense of translucence:
		//	reflection
		//	"deep" shadows
		//	fog
		//	3d geocetry containing things, like plants etc.
		//	Outlines/ patterns on outside of geometry

		// translucent, translucence
		// TODO: Try making something that looks translucent? like MapleStory slimes.

	},




	// colorschemes...
	bluepink: {
		// the major colors are blue and pink and gradients between the two
	},
	greenpink: {
		// the major colors are green and pink and gradients between the two
	},
	ghiblitime: {
		is: {
			colorscheme: 1,
		}
		// colors that reflect colorschemes reminiscent to what is seen in studio ghibli movies as well as Adventure time. and maybe Zelda BOTW
		// generally bright.
		// contains these colors: green (both warm and cold), earthly colors (brown, sand, wood, beige, skin) and sky/water blue(warm=more green, cold more white or).
		// often aligns with the piss filter coloring trope.
		// it highlights aspects of the natural landscape in a often hightlighting fashion of the beauty in nature.
		// it can either be silvery/glossy or dull. often silvery makes it more cold colors. and dull makes it warmer.
	},
	tumblrEdge: {
		// stuff people would share on tumblr.
		// often with a filter. - making it feel matte, rosy or off color.
		// often about anime and manga.
		// a restricted color palette
		// often with white taking big parts of the composition
		// often kinda cold colors. blue green pink and white
	},

	tropesInColorSchemesAndLighting: {
		monochromousLighting: {
			// similar effect to grayscale
			// usually used for dramatic effect, indication a restriction, unnatural impbalance
			// can be perceived as natural or unnatural depending on color and context.
			// green forest makes sense
			// blue oceanscape makes sense
			// a gray cityscape makes sense
			// but colors like red, yellow and purple often feel unnatural, or supernatural.
			// this works very well in dark environments and foggy landscapes.
			// red dark monochromous creates an "evil" or sinister aura. almost supernatural.

			// it is like one signal, purity, focused on one element, contextually focused,

			// even if it is monochromous, when combined with gray and white, it feels less pure.
			// because something purely gray or white starts to contain all other colors too.. (rgb)
		},
		//			Analogous color scheme
		//			Complementary color scheme
		//Split-Complementary color scheme
		//			triadic color scheme
		//Tetradic color scheme
		//		tritone: 1, //??
		bright: 1,
		pastel: 1,

	},

	lighting: {

	},
	lumination: {
		// amount of light
		// darknessLightnesRatio brightness
		// 0 means pitch black
		// 1 means blinding white light

		// everything is dark by default
		// light sources makes things bright

		// darkness implies low visibility:1
		//	to make things high visibility with low lumination, make them glow!!!
		//	glowing in darkness has some associations.. psychedelic, disco, neon, whatevers.
	},
	shadows: {
		// shadows in compositions:
		// What you basically want to do is reduce the amount of complex shadows in an already complex environment and try to blend shapes together, so they are perceived as one whole shape.
	},
	godray: {

	},
	paintedStyles: {
		waterColorGraphicalStyle: 1,
		crayon: 1,
		sketch: 1,
	},
	glow: {
		// emits light (without reflection)
		// emits light in a way that is noticable in the dark.
		// there is a glow effect, which is like a blurred outline around an object
		// there is also glow, as in, something that is way brighter than its surroundings.
	},



	//visual qualities
	// of texture and form
	slick: {
		// what is slickness?
		// smooth surface
		// blobs
		// reflection consisting of smooth mono colored stuff
		// round shapes
		// like waterdroplet, always
		// as if smooth shaped like droplets.
		// blobby like liquid but not necesarily flowing like liquid
		// common examples: smooth rocks, plastic, glossy things, thickly outlined characters, 2d simplex noise, sand dunes,
		// thick outlines sort of prevent sharpness...
		// tubular or cylindrical shapes (with reflection)
		// things without hard edges.
		// softness and gradience in constrast with sharpness and texture
		// often used in discrete colors
	},
	crisp: {
		// Good aliasing...
		// strong textures.
		// clear and intricate silhouettes.
	},

	neon: {
		// neon lightbulb has to do with the shape of the lampe being one curved smooth lines
		// of emitting light in a way, that is soft, has one specific color per lamp and fades out in a way where it is most intense the closer to the lamp.

		//generally things that glow in dark (aka in a way that looks flouresent) with soft light that have strongly defined bright color (aka now white. but yellow, orange, green, red, purple, blue, etc)
		// is like a form of glow
		// also specifically a type of lamp that emits soft light from glass tubes
		// also a kind of aesthetic on its own. related to 80s aesthetic, retro aesthetic and 
		// ultraviolent?
		// can be a bit psychedelic when used in dark environments.

		// Is an aesthetic and a lighting
		// the aesthetic is to rely heavily on the lighting
		// the lighting style is dim glowy

		// how neon can feel: exciting, otherwordly, futuristic, technological

		// associated with night life, gaming, streetlife

		// often combined with glossy surfaces
	},

	Glitter: {
		//		How to implement glitter
		//		glitter is literally just a particle which brightness is dependent on a 2d x,y noise function

		// many very small reflective surfaces. can be white, can be tinted a specific color can be random color.
		// small particles that reflect bright light depending on angle.

		// glimmering like sand/snow

		// what is glitter for?
		// highlight something as extraordinary or special
		// show that something is pretty
	},
	glimmer: {},
	sparkle: {},

	lightReflection: {
		// the shine and texture on animal crossing objects since new leaf

		// Water reflection todo:
		// Copy texture
		// Flip it upside down
		// Make it fade out downwards
		// Draw circular ripples
	},
	reflection: { //gloss:
		//	silver glare and gradient, shimmer as it moves
		//		level of reflection
		//			Reflective flat surface
		//			morphed reflection based on object shape
		//		Reflection image target (what is reflected?)
		//			random sky box glares or something
		//			same as in front but morphed with shape
		//			full simulated back view (how it looks in the direction of the camera)
	},




	//Visual Effects VFX
	//steal all the canvas functions... or whatever you find in other engine of use.
	blur: {
		//nongranular texture
		//smoothed out x and y wise
		//a specific drawing method/filter you can apply with varying intensity.
	},
	bloom: {
		//Gloom? (just blur on a separate layer?) #code #should

	},
	noise: { //grain:
		//random pixels
	},
	masking: {
		//Masking effects/texture to shape
	},

	filter: {
		// please define. and make it different from overlay texture.
		// for example colorfilter/Color overlay
		// Make a thing that can define how much hue, saturation, etc, etc is affected by a colorfilter.
		// Make something that changes color based on screen position! #Could #Code

	},
	overlayTexture: {
		// take some black and white thing to bend the straight color with
		// like the picture of an empty canvas
		// or crumbly paper
		// or just noise
		// this creates like a cozy feeling, like handcraftedness.
	},
	texture: { //very unclear concept still
		has: {
			dimensions: {
				x: 1,
				y: 1,
			},
		}
	},

	// textures
	waterColor: {
		// Water colors rules! Use them for fog and clouds.
		// having noise texture makes it feel painted, it is really cool and adds to the experience making it feel less static.

	},
	canvas: {
		// a canvas or paper filter makes the game feel like it is a live painting....
		// having noise texture makes it feel painted, it is really cool and adds to the experience making it feel less static.
	},



	// rendering techniques:
	stamping: {
		// drawing a shape or texture repeatedly onto another texture.
	},
	stamp: {
		has: {
			texture: 1,
		}
		//when a radius is too small (when to not bother drawing/stamping.)
	},
	stampLine: { //is a form/type of brushStroke 
		//	Consecutive stamps  from a drawing utensil plotted on a line
		//	With a spacing that is not more than 1/10 of the brush surface width
		//	var spacing = Math.Max(.5, textureWidth * 0.1) //no less than half a pixel. no more than 1/10 of diameter. according to: https://losingfight.com/blog/2007/08/18/how-to-implement-a-basic-bitmap-brush/


		has: {
			length: 1,
			curvature: 1, //curving... can be defined through curve function...!
		}

		// what is the magic that makes lines smooth even when stamp size gradually increase or decrease?

		// Optimizations, when distance is not fare enough to bother drawing (less than .5 a pixel)
	},
	drawingUtensil: {

	},
	brush: {

	},
	pencil: {

	},
	draw: {

	},
	curveAngleFunction: {
		//for any provided point/step/number, returns the angle to be used in a line.
		is: {
			function: 1,
		},
		has: {
			parameters: 1,
		},
		does: {
			returns: {
				angle: 1,
			}
		}
	},
	curveThicknessFunction: {
		//for any provided point/step/number, returns the radius? to be used for a stamp in a line.
		is: {
			function: 1,
		},
		has: {
			parameters: 1,
		},
		does: {
			returns: {
				radius: 1,
			}
		}
	},
	curveColorFunction: {
		//for any provided point/step/number, returns the color to be used for a stamp in a line.
		is: {
			function: 1,
		},
		has: {
			parameters: 1,
		},
		does: {
			returns: {
				color: 1,
			}
		}
	},

	luxurious: {
		//big
		//expensive. highPrice: 1,
		//exclusive
		//intangible: 1,
		//relative pleasantness
		//comfortable: 1,
	},
	sophistication: { //aesthetic...
		highQuality: 1,
		transcendent: 1,
		curated: 1,
		integral: 1, // integrated, all scenarios are thought of
		QualityBeyondTheProduct: 1, // it makes you better outside the area it is intended for.
		representational: 1, // it represents what you want.
		// makes you blend in but look extraordinary
		godly: 1,

		// it is crucial to justify the price point
		//
		// what is high price to some is a rounding error to the super wealthy
		//
		// they are tech savvy and can look up information
		//
		//
		// beware! The value of a luxury product is not in its materials or the time it took to create it.
		// The extreme value of a luxury product is driven by an intangible, yet enormous value component:
		//	 the ALV (Added Luxury Value)
		//
		// an elusive concept that few brands manage well.
		//
		//
		// the product is not a significant driver of alv
		//
		// high quality is important for emotional desirability
		//	but frankly, quality and craftsmanship is expected.
		//		(so... is artistic vision and curation inside of this or outside?)


		// the product makes it seem like you are smart, intelligent, or whatever quality you seek.


		// a brand never compromises. Do not buckle under the pressure.
		//	You do not lower the price because lack of demand.
		//		you say "this will only go up in price."
		//			"here is why"			

		// can you create enough value to justify the price tag?

		// (it is the best experience you can put your attention on)
		//	it is installing a mindset


		// funny material that is hard to get
		// well thought out shapes

		// and it sorta lets you blend in but look extraordinary

		// a product giving the impression of being magical. defying current reality.
	},

}
