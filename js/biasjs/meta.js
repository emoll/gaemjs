const meta = {

	// INTERNAL LANGUAGE AND COMPREHENSION

	// Requirements and presupositions for the algorithm:
	//	There is one reference list of classifications.

	classification: {
		// classifications can have inherited values from other classifications (is).
		// classifications can have defined constraints (has)
		// classifications must have either more than 0 count has values OR 2 or more inheritances.
		// Classifications with identical composition of properties are aliases of the same and do not need separate memory.
		//	aliases should generally be further defined or removed as multiple titles for one thing is redundant.
		//	(different titles of the same thing)
	},
	instantiation: { // (what defines something as instantiated vs not?)
		// at some point there is something that must operate on a definition to realize/instantiate it. A function call or execution of a scrip.
		// Some concepts/definitions are only instantiated through implementation/execution.
		// Some concepts/definitions are only instantiated in continous implementation/execution.
	},
	execution: {
		//	(how to determine what causes execution? How to measure execution?) 
		//	execution means computational operations, under conditions which change raw data of primitive types in memory.
		//	execution can be measured in change of memory/composition. unfamiliarity?
	},


	iss: { // Inverted relationship. eg body is physical. this is just a quick way to list relations.
		// I probably need a way to define ^ order in code. like if a syntax that decides the order.
		// Add iss: in objects to indicate that those other things is this. confusing but maybe time saving?
		// Is refers to type or fulfilling of certain criteria
		physical: {
			body: 1,
			limb: 1
		},
		Drawn: { // TODO: Draw everything that "is drawn" (maybe I should use does draw instead, but that implies it is autonomous like the entity does it)
			body: 1,
			limb: 1,
		},
		limb: {
			head: 1,
			leg: 1,
			arm: 1,
		},
	},

	// META keywords - relational, etc
	is: { //being:

		// is = belongs to category of. Categorizable as.

		// being = behaving as. (reasoning by analogy) I am a rock, I am like a rock. I am human.
		//	identity, about unchangingness of properties
		// subject = that which is aware of object. (awares objects)
		// manifesting, doing.
		// stuff you can't take of. because it is essential. core part.

		//is seems like a shorthand for has. Is blue means has the color blue... or maybe manifests the color blue.

		// heuristic, is implies direction of fundamentality: a is b, b is c, c is most fundamental.

		//first-principles implies "is" and "has" defintions, and lack of "like" or "similar" definitions



		//// is means fulfillls the model of:, 

		//	"is" is short hand for acts x-like?
		//		is happy = acts in a happy manner
		//		is cat = manifests in a catlike manner/style
	},
	has: {
		//Basically means contains instance of .
		//having = accessing as subject
		//	separation from things.

		//has, is something you access and control but is objectified, seen in isolation.
		//object = things you can see and know about.

		//has is testable, 
	},
	like: {}, // (Similar to)
	does: {},
	with: {}, //  I like "with" more than "has" because I don't believe in ownership
	means: {
		is: "is of type"
	},
	defaultValues: { //different default values
		//	unknown maximum
		//	unknown minimum
		//
		//	default to average
		//	default to median
		//	default to maximum
		//	default to minimum
		//
		//	default to 1
		//	default to 0
	},
	etc: { //- "And related things."
		//I like the idea of writing:
		//Contains
		//related meaning contextually relative
	},

	and: {
		//all the things within the tag
	},
	or: {
		// ethier of the things within the tag
	},


	count: { //restriction/constraint in number value
		//Quantity
		//	count. //Example qty 2. this means playerCharacter is 2 humanoids
		//	words
		//		some
		//		few
		//		dousin

		// quantity: Quantity or amount is a property that can exist as a multitude or magnitude, which illustrate discontinuity and continuity. 
		// amount of continuities between discontinuities.
		// Usually refering to physical instances rather than temporal instances. (physical instances persists over time between occurences)
	},
	ofValue: {
		high: 1, //a high value is towards the upper end. more than expected of the average
		low: 1, //a high value is towards the lower end. less than contextually expected of the average
		much: 1,
		little: 1,
	},
	positiveNumber: { //rename lol
		moreThan: 0, // rename lol
	},
	//todo: consider whether i need a "more" keyword?
	leafConditions: { //numeral boundaries/restrictions/constraints
		moreThan: 1, //more than x
		lessThan: 1, //less than x

		atLeast: 1, //atLeast x or more //min x ???
		min: 1, //atLeast x or more //min x ???

		atMost: 1, //atMost x or less // max x ???
		max: 1, //atMost x or less // max x ???

		between: 1, //range. value between x and y	
	},
	precision: { //estimation, proximity
		around: 1, //around x //close to example: around average speed. // maybe with defined deviance...
		lagom: 1,
	},
	oneInSet: 1, // One in the set, chooses one out of a defined set of conceptions rather than a math value range.
	statisticalMeasures: { //uh why this not just Math.x?
		average: 1,
		median: 1,
	},
	//negations!
	not: {
		//(can mean, not identical and could also mean not similar "a tree is not a car", you gotta think about this, but then again then it has to do with similarity)
	},
	notNecesarily: {
		//example: not necessarily humanoid, removes the humanoid property from the element.
	},
	oppositeOf: { //opposite: //opposition: anti:
		// used to check if something is or make something the opposite of something else.
		// I think this should be a function..... 
		// this feels like a leaf function. 
		// It's kinda easy to find the opposite in linear scales. 
		// like numbers: 1 => -1, 5 => -5
		// 20% hungry => 80% hungry ??? or 20% full....??
		// the opposite of a color... is that the inverted color?  just flip the values around 255
		// is the opposite of a complex concept the negation of all it's properties?
		// the opposite of a shape is an infinite fill with a hole shaped like the original shape.
		// I guess I bound which use cases of opposite are allowed or something...
	},

	otherwise: {
		// If convention is not followed, then this (the alternative convention?)
	},

	good: {
		// something that fulfillls human needs.
	},
	bad: {
		// something that worsen human needs.
	},

	needsFulfilmentLanguage: { //stopping (fixing the stopping problem)
		satisfying: 1, //satisfy his need for novelty //hit a good threshold //about knowing
		enough: 1, //do it enough times
		// much - not yet too much but more than expected or needed.
		// too much
		//
	},

	normalcy: { //precedence. rarity //commonality/ commoness commonliness //recurrency? recurrence frequency? //normality 

		// Normal = that which is most persistent and most ubiquitous or widely distributed?

		//    Distinct - noticeable from surroundings
		//    Unique - one of a kind
		//    Extreme/non-ordinary/non-normal/uncommon

		//    Conventional -  sharing the most commonly seen characteristics.
		//        conventional means that is stereotypical appearance of that object but might have some variance depending on context.
		//    ^normal
		//    Unconventional
		//         Mostly within the concept  but with rare or uncommon properties. Unconventional also implies will of breaking convention. To go against what most people do or usually decide. Usually a niche utility.
		//        for example an unconventional house  might only be made out of circular shapes that has a lot of tiny compartments.
		// Frequency/probability language and words (for probability and causation)

		// it can be normal that something in particular is unpredictable/random. (normal that some things are not normal)


		//    simulations
		//        probability: 50%
		//            This means it will be humanoid in 50% of cases.
		//        probability: range: .5 to .7
		//            this means that the probability is determined as being somewhere between 50% and 70%
		// Exoticism as a concept
		// Example: exotic plants

		// Extra words:
		// Most - "most cars are blue." aka the biggest portion, of all existing portions.

		// Extra words: probably

		// Scale / ranges, //example 50% to 70% humanoid. This means the 50% needs to be context of some other races //How of something else something is.

		// Edge case - normal case - archetypal case
		//    archetypal - normal variety - uncommon/unusual - fringe examples
		//        archetypal - the point of the bell curve, often over exemplified in a biased world view.
		//            the good thing about archetypes that it is a good target for caching. you can save performance and memory by 
		//                assuming things..
		//        normal variety - general common occurrence
		//        uncommon - not towards the center of the bell curve
		//        fringe - fringe on the bell curve.
		//    there is like different levels of archetypal and normal variety.
		//        You can have the archetypal one case, or you can have a few cases.
		//        You can have normal variety meaning things near the top of the bell curve
		//        or you can have normal meaning, get any point on the mass of the bell curve
		//            (where you are more likely to get a normal result)
		//    Archetypes - used to denote destination in human perception?
		//        color archetypes are - red blue green yellow
		//        creature archetypes are - human, cat, dog, fish, bird, lizard
		//        shape archetypes are - circle, moon, star, square, hexagon, ...
	},
	levelOfRecurence: { // words for different levels of recurence
		Always: 1, //100% of the time
		AlmostAlways: .99, //??
		conventionally: .9, //???? a standard. seldom broken?
		Generally: .8, // (?) (more than 50% of the time?)
		probably: .6, //(more than 50% )
		Often: 1, //
		regularly: 1,
		Sometimes: .35, // (figure out how often is sometimes... lol)
		Seldom: .2,
		uncommon: .2, //???
		Rarely: .1,
		almostNever: .01, //consider "almost" its own keyword.
		Never: 0, //0% of the time
	},
	frequency: { // maybe move to systemic?

	},
	likelihood: {

	},
	probability: {
		// "sannolikehet" truth-likeness.
		// The ratio at which a defined outcome ocurs in a defined context.

		// For algorithm about chance:
		//	outcomes are usually not deterministic, they are a range of possibilities.
		//		so you want to figure out what those probabilities are.
		//		It is fine to gamble as long as you are the house. (because the probabilities are in your favor.
	}
}
