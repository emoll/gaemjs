// The Core/Meta functions
function is(a, b, inTermsOfC) {} //Is A, B in terms of C? // I might need to specify even further the type of isness: similarity, contains

function is(context, keyword) {
	// what should is return in this case? how do I check if it's kinda red, or red with a certain probability.
	//what is kinda red is defined by bias
	//is() collapses uncertain probabilities.

	//Use case 0: context is invalid, return
	if (IsUndefined(context)) throw Error("invalid context")

	//Use case 1: The thing already is determined to be or not to be
	if (context[keyword] && !IsUndefined(context[keyword].is)) return context[keyword].is

	//todo: code
	//this could actually just return false? because the statement is is false.



	//Use case 2: the thing has been marked with a probability for some reason
	if (context[keyword] && !IsUndefined(context[keyword].p)) {
		if (math.random() < context[keyword].p) {
			context[keyword].is = true
		} else {
			context[keyword].is = false
		}
		return context[keyword].is
	}

	// Use case 3: the thing has neighter been marked with "is" nor "p" and it is up to a higher power to determine what answer is useful to give.
	//this is where bias with more complex judgementcomes in...
	//aka make the decision, is it better to say yes or no in this situation based on the bias?
	//Or I could just pick one at random??? but then how does the context come in. I can't just rely on everything having an assigned "is" or "p" value?

	//At this point it needs to do some trial and error, to simulate the future and see if it's a good one.
	//So either a good simulation system that can do trial and error
	//Or an trial and error algorithm that generate bias over time, similar to neural learning algorithms.
}

function make(context, targetProperty, goal) {
	//make I need another structure,
	//basically make(contextStructure, goalStructure)
	//contextStructure context[property][property]...
	//goalStructure change x, add y, remove z???

	//I'm thinking of general rules to add to this function...
	//if no target property, assume targetProperty = "is"
	//if targetProperty is "is" then generally goal should be pushed????
	//you need to see if there is a paradox
	//and you need a solution depending on the paradox
	//I believe the easiest paradox to solve is the one of opposites
	//if you set something to it's opposite, it's current value needs to perish.


	//case 0: target property does not yet exist.
	//todo: I suppose I add the property here then???

	//case 1: you can't make what already is reality.
	if (is(targetProperty, goal)) return
	//This format might be better:    if (is(context, targetProperty, goal)) return

	//Do I need to determine a reason or is it just magic?

	//what context / situation: world
	//when: now
	//where: here
	//how: (the reason, is it just magic?)
	//what method / way:
	//    for how long:
	//    for what desired result:
	console.log("setting", context, targetProperty, goal)
	console.log(bias[context][targetProperty])
	//    bias[context][targetProperty] = goal //the most basic way to mke, just override the property yo.
	bias[context][targetProperty][goal] = 1 //the most basic way to mke, just override the property yo.
	console.log(bias[context][targetProperty])
	console.log(bias[context])
}

function What() {
	//	the what function gets/specifies the most IS'y variable in a context.
	//the what function observes the isness of a range and returns the most issy.
	//	Maybe what is more important than “is()”
	//“What on screen is blue?”
	//“What on screen is the most blue?”
	//“What is real?”
	//“What is drawn?”
	//On one hand you want to get these lists.
	//On the second hand these list could be generated in the Make() process.
	//I’m unsure of this concept, even thought I really like it.
}

function whatis() {
	//	Another interpretation is whatIs()
	//I want to player in terms of temperature??
	//What is player in terms of temperature?
	//In terms of??? <- is this a useful concept in programming?
	//It's a request to specify in uncertainty.
	//You're asking the game master.
	//WhatIs can do some collapsing too.
}

function Reaons() {
	//The software reason to prioritize a result. that collapses the wave function. (this might not need to be a function.)

}

function commonalities() { //or Common() ?
	//Do I need Functionality to get what a set of things most have in common?
	//common(things)
	//Can be useful for finding relevance?
}

function identify(target, context) {

} //what is target in terms of [context] for example what is {x,y} in terms of geometry => point/position

/* todo
	1. conceptualize a rectangle and draw it!
		I'm unsure of what conceptualize mean
			but I know what should happen: (backwards order)
				the specifics needed to draw a rectangle is handed to the draw function
					the dimensions of a defined rectangle are set (within reasonable parametric range).
						the reasonable parametric range is set through best matching heuristics.
							
			the most obvious heuristics is 
				between a and b
				range
				scalar
				^ these are all clues that 
						
					
	2. populate the bias with keywords from google keep and from dynalist
	3. make rules for composition
		symmetry, assymetry, the square lines, the golden ratio, sacred geometry lol.
		hubs/centers, random distributions, noise as guide.
*/ //todo

function means() {
	//	Means() is it needed?
	//X means y in context z
	//It creates the ability for one term to mean different things in different contexts.
	//Basically contextual namespace as replacement for private namespace in conventional programming.
	//means(term, inContext, returnValue)
	//means(full, context(hunger), between(0, .1) )
	//This basically generates heuristics!

}

//Core leaf node
//var HandleMediaRequest = {
//
//}

function HandleMediaRequest(request) { //outdated function
	//check for media types in the request.
	//I suppose I have to loop through properties in the request
	Object.keys(mediaTypes).forEach(function (mediaType) { //something something there are two arrays, you should do the one check that requires the least checks
		console.log(mediaType, request[mediaType])
		if (request[mediaType]) { // is media mediaType in the request?
			console.log("there was a media type like this!")
			mediaTypes[mediaType].handle(request)
		} else {
			console.log("media type:", mediaType, "does not exist in the request")
		}
	})
}


function Realize(thing) { //I think of it as "realizing" the game, actuate, make it manifest.
	//step 1 is I kinda want to compile the object.
	var workingMemory = {}
	Extract(thing, workingMemory)
	//step 2? you need to be pulled towards the goal state! values needs to be realized.
	//I'm a bit unsure what the goal state is though.... It is the wish/request realized through the bias
	Extract(KPI, goalStructure)

	//step 3
	//it needs to be clear what is unrealized, and what needs to be realized.
	//just follow the manifest calls... if it contains element "manifest" then manifest the thing~!
	manifest(thing)
	//manifestation needs to realize the chain reaction of time. (this is part of manifestation)
	//time is just continual unrealized/unresolved states. It's like, when everything is realized, you realize that there is more to realize.
	//continually solved in to the unsolved. 
	//I need to be able to mark things as unresolved, and not only that but unresolved until condition.
	//I feel like I'm tying myself up in a knot, because I'v said that conditions are biases, to be conditioned.
	//I'm unsure if I need to keep the original request
	//I'm unsure if I need to keep the bias intact in runtime
	//I'm unsure if working memory should be in the "extract"
	//I'm unsure were working memory should be and how isolated it should be from other data like bias etc.
}

function continualRealization() {
	realize(unrealized)
	window.setTimeout(continualRealization, 100) //100 ms should be 10 times per second!
}

//at first there was nothing
//var reality = realize(0)
//reality = realize(reality)


function Extract(key, target) {

	//TODO:
	// Merging rules for seelding algo:
	//
	// you want a rule overriding system so that you know what is from where:
	//
	// if I have a some extra stuff I want to add to MY player character
	//	like a specific position.
	//	
	//	you could just keep track of the origin
	//	
	// and also have some default behaviour like:
	//	always override the root concept value.
	//	add all fields that does not exist
	//	replace the value of fields with new values
	//	if the value is a {} (table) then add subfields in stead of override.

	var sourceObj = bias[key] // The object/dictionary to extract from
	if (!sourceObj) {
		if ('object' === typeof key)
			return false
		else console.log("No defined entry for key: " + key)
		return false
	}
	Object.keys(sourceObj).forEach(function (relationalKey) { // Assuming the structure is sourceObj->relationKey->values that key only contains relational keys.
		if ("is" == relationalKey) {
			if (!target[relationalKey]) target[relationalKey] = {} // Fill in missing relational keys aka, is, has, etc.
			Object.keys(sourceObj[relationalKey]).forEach(function (relationEntityKey) {
				Extract(relationEntityKey, target)
				target[relationalKey][relationEntityKey] = sourceObj[relationalKey][relationEntityKey]
			})
		} else if ("has" == relationalKey) {
			if (!target[relationalKey]) target[relationalKey] = {} // Fill in missing relational keys aka, is, has, etc.
			Object.keys(sourceObj[relationalKey]).forEach(function (relationEntityKey) {
				target[relationalKey][relationEntityKey] = {} // populate target with empty object to be filled by recursive call
				Extract(relationEntityKey, target[relationalKey][relationEntityKey])
				if (Object === sourceObj[relationalKey][relationEntityKey].constructor) { // if the instance has body
					var instance = {}
					Extract(sourceObj[relationalKey][relationEntityKey], instance) // extract recursively
					Object.assign(target[relationalKey][relationEntityKey], instance); // append properties. TODO: customize how merge is done... this current solution might just override all existing values.
				}

				// if value is 1, look up definition.
				// if value is object, create instance and look up definition. if it exists add.

				if (Object.keys(target[relationalKey][relationEntityKey]).length === 0) {
					target[relationalKey][relationEntityKey] = sourceObj[relationalKey][relationEntityKey] // default value instead of empty object, because empty object is meaningless.
				}
			})
		}
	})
	return true
}


//Find:
//find all matching condition x
//find all "is: blue"
//Inventory: //aka create an indexed list you can use.
//index all with condition x in array y
//"what applies to everything that is x?"




/* //this is essentially how you test path finding in each step. the shortest conceptual path is the one already established or cached.
	if (request["motif"]) {
	Object.keys(request["motif"]).forEach(function (motif) {
	if (bias[motif]) {
	if (Draw[motif]) {
	Draw[motif](defaultLayer, realizedMotif) //example: draw["square"]()
*/
//You need to compile a list of uncertainties that needs to be collpased:
// What is the style, what is size etc? 
// how to practicly function through the objects 






//mission path find with what I have to a point where I have a desired picture?
//I start with both where you are now and where you want to be defining the end point or the goal..

/* The Triad?
	the request is the impulse. like the seed. the wish. the dream. the specific we want to go to.
	if the wish contains none of the fundamental requirements, then the wish has no input in the creation process.
	this basically means everything is improvised by the engine (the already existing culture). For example, selecting an archetypal shape and making it's own canvas, etc.
	requirementstructure is the end. the goal. is it the very fundamental of the request/wish that is specific to a picture request?
	bias is the tool box? the lexicon, the look up in uncertainty. It is the place you go and ask for help when you don't know. the wisdom?
	I'm adding a fourth piece to the triad which is working memory.... and also simulation memory lol.
*/ // The Triad?

//thought simulation
//let's say the request contains a canvas, then it just goes to the next
// is there a depiction? no
// then, is there a shape or a sprite?
//there is a shape
//then is there a draw call? //now the hard part is telling the algorithm that it needs to employ the leaf node Draw
//I'll employ the keyword "call" to signify it is a requirement to call the functions it contains.
//there is a draw function
//draw()
//but draw never gets


//how do you say "I need a realized rectangle rather than a conceptual?
//aka how do you state this requirements or wish?
//can the concept be realized/collapsed
//do I know what it will look like when realized
//You can not draw what you have no conceptualization of. Step 1 is to conceptualize the motif.

//A realized rectangle shape requires a width and a height to have defined values.

//I need instantiation as well, I want to make a rectangle INSTANCE and then manipulate that

//I don't want to manipulate the bias with this sort of request.
//						var realizedMotif = realize(bias[motif])
// realization process


//in this instance I don't need to remember any instances beyond the point it has been drawn
//and we do not really need instantiation or realization then I suppose.
//... I men the values must be realized.

//	var realizedMotif = { //this is experiment, this is essentially what I want to end up with
//		is: {
//			rectangle: 1,
//		},
//		x: 100,
//		y: 100,
//		width: 200,
//		height: 100
//	}
// so when should these be specified?
