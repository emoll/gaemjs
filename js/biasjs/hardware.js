// REAL WORLD SYSTEMS AND HARDWARE MODELS
// 1. Computer limitations and constraints
// 2. Input devices and methods
// 3. Various forms of media

// Hardware constraints
// Computer limitations and constraints
// Player platform device type: PC, tablet, PHone, Console, Other, etc
// You need to test the capacity of the device....
const hardware = {
	reality: {
		// the domain of real things
		// For example hardware and people.
	},
	hardware: {
		// is in reality
	},

	computerConstraints: { // What are the hard limits of the computer system?
		CPU: {},
		ram: {
			//		on chrome:
			//			performance.memory.jsHeapSizeLimit; // will give you the JS heap size
			//			performance.memory.usedJSHeapSize; // how much you are currently using
		},
		hardDisk: 1,
		heating: 1, //applies overall to the device but to individual sections or heat sensors...
		//	battery drainage
		//	network connect
		//	speed
		//	ping
		//	reliability
		//	power consumption
		//	type of device
		// number of screens
		//	size of screen
		// positioning of screens.

		portability: 1,
		//	handheld?
		//	laptop vs stationary
		//	phone, tablet, 
		//	touchscreen?
	},
	NetworkConnection: {},
	handController: { //gamePad
		// Checking Controller support
		// Hand controls should be ergonomic when playing the game.
		//	(Do not force the player to hurt their hands!)
		// Give the player ergonomic instructions of stretches in-between intense moments.
		// Think about the mapping on the controller mapping to physical space.
		// A top button could be used for high attacks.
		// Think about the mapping of the controller and the button that are used the most for the console.
		// Emulate the standard control schemes of games in the same genre.
	},
	computationalCosts: { //TODO measure these...
		Electricity: 1,
		OccupiedTime: 1, //you can measure how fast the computer is doing a certain amount of work. and then you can you timestamps to get an indication of how much compputation has been done.
		RAM_Occupation: 1,
		CPU_occupation: 1,
		GPU_occupation: 1,
		Networking: 1,
		//		Disk reading/writing
		Heat: 1, //can you measure this in a browser?
		batteryLevel: 1, //this can be especially important to adapt to. Basically save performance or ask the player if they want battery save mode.
	},
	unacceptableComputerPerformance: {
		nonCrash: { //the minimum to even be software...
			// Do not make the CPU endlessly loop (infinite for loop for example)
			// Do not fill the ram beyond capacity//above 95%
			// Do not fill the disk beyond capacity.
			// Do not make the PC burn or break from heating.
		},
		basicMinimalExpectations: { //very troublesome but basically functional.. for a while.
			// CPU - Do not freeze the program (unless loading) for more than 5 seconds
			// Ram - Do not fill most of the ram (more than 80%)
			// Fan/heating -
			// Do not make the computer smell burned plastic.
			// Do not make the cooling fans insanely loud. // maybe move to next one..
			// HardDisk - Do not fill more and more disk space in the gigabytes. //the average session should not increase file storage with a GB
			// Battery - Do not drain 5% per minute
			//		network connect
			//			speed
			//			ping
			//			reliability
			//		power consumption
		},
		poorButFunctionalProduct: {
			//		CPU - lag spikes, low fps
			//		Ram - 60% ram or something...
			//		HardDisk - saving 200 new mb per session
			//		Heating? 
			//		Battery drainage
			//		network connect
			//			speed
			//			ping
			//			reliability
			//		power consumption
		},
		mediocreProduct: {
			//		CPU
			//		gpu?
			//		ram
			//		hardDisk
			//		heating?
			//		battery drainage
			//		network connect
			//			speed
			//			ping
			//			reliability
			//		power consumption

		},
		decentProduct: {
			//		CPU
			//		ram
			//		hardDisk
			//		heating?
			//		battery drainage
			//		network connect
			//			speed
			//			ping
			//			reliability
			//		power consumption

		},
		goodPrudct: {
			//		CPU
			//		ram
			//		hardDisk
			//		heating?
			//		battery drainage
			//		network connect
			//			speed
			//			ping
			//			reliability
			//		power consumption

		},
		greatProduct: {
			//		CPU
			//		ram
			//		hardDisk
			//		heating?
			//		battery drainage
			//		network connect
			//			speed
			//			ping
			//			reliability
			//		power consumption

		},
		beyondExpectations: {
			//		CPU
			//		ram
			//		hardDisk
			//		heating?
			//		battery drainage
			//		network connect
			//			speed
			//			ping
			//			reliability
			//		power consumption
		},
		optimalGame: {
			//		CPU - do not use
			//		ram - do not use
			//		hardDisk - do not use
			//		heating? - do not use
			//		battery drainage - do not use
			//		network connect - do not use
			//			speed - do not use
			//			ping - do not use
			//			reliability - do not use
			//		power consumption - do not use
		}
	},

	// input devices and methods inputChannels
	sensor: {
		// A device which detects or measures a physical property and records, indicates, or otherwise responds to it.
		// Any sensor can act as an input method....
	},
	input: {
		// feedback from the player.
		// key presses, mouse moves, controller, gyroscope, whatevers.
		// might as well listen for all user inputs
		//	keyboard
		//	midi
		//	mouse
		//	touch
		//TODO: search input method and incorporate.
		does: function () { //I will need to run functions at some point.... I have not decided how.
			document.addEventListener("click", function () {
				console.log('click')
				/*stuff*/
			})

			document.addEventListener("mousedown", function () {
				console.log('mousedown')

				/*stuff*/
			})
			document.addEventListener("mouseup", function () {
				console.log('mouseup')

				/*stuff*/
			})
			document.addEventListener("mousemove", function () {
				console.log('mousemove')
				/*stuff*/
			})

			document.addEventListener("keydown", function () {
				console.log('keydown')
				/*stuff*/
			})
			document.addEventListener("keyup", function () {
				console.log('keyup')
				/*stuff*/
			})
			//todo: and handcontroller and touch
		}
	},
	conventionalMovementInputs: { //For Video Games in general
		//	move / walk
		//	jump
		//	interact
		//	swing / attack		
	},
	videoGameControlTropes: {
		// keyboard: move with arrow keys
		// move with WASD
		// controller
		// move with +
		// move with left control stick
		// "free arm swing mode":
		//	the arm moves in the direction the (right) stick is pointed. (like in frog climbers!)
		//	The arm moves do that the hand is extended in the direction of the pointer
		//	Either with a controller
		//	Or with mouse pointer
		//	Or with touch pointer
	},



	preferedUserInputDevices: {
		// An input device prefered by the player to be used as main means of interaction.
		is: {
			prefered: 1, //prefered by user
			inputDevice: 1,

		}
	},


	// something something player character movement control...... walk run, jump, etc.

	ADSR: { // attack decay sustain release
		// Primarily used to define sounds in music. 
		// But can be also used to define effect of input
		// And can also possibly describe any event or window of time.
		Attack: {
			//what happens when you press the button
		},
		Decay: {
			//whatever when it goes to sustain
		},
		Sustain: {
			//you hold the button this happens
		},
		Release: {
			//you release the button this happens.
		},
	},

	// Uh filtering input signals... modulating the input signal.
	// Making the analogous input signal digital.
	// Making the mouse pointer flip around the screen edge.




	// "output" outputchannels. sensory stimulation devices
	outputChannel: { //subjectively experiable output channel
		// can create sensation for subjects.
	},
	OutputDevice: {
		//An output device is any piece of computer hardware equipment which converts information into a human-readable form. It can be text, graphics, tactile, audio, and video. 
		// Examples include monitors, printers, speakers, headphones, projectors, GPS devices, sound cards, video cards, optical mark readers, and braille readers.
		is: {
			hardware: 1,
			outputChannel: 1,
		}
	},


	displayDevice: {
		is: {
			hardware: 1,
		}
	},
	screen: {
		is: {
			displayDevice: 1,
		},
		has: {
			rectangularSurface: {
				is: {
					rectangle: 1,
				},
				has: {
					width: 1920, //hard coded assumption
					height: 1080 //hard coded assumption
				}
			}
		}
	},
	Vibration: {
		//    Only supported in some devices
		//        So if you are multiplatform do not make this the essence for experiencing the game
		//    Use for
		//        Emphasis
		//        Impactfulness
		//        Intensity
		//    Tacit-ness or impacts
		//        Earthquake rumble
		//        Explosion
		//        Rock impacts, weapon impacts
		//        
		//    Simulate feelings
		//        Pain (muscle spasm, impact)
		//        Weakness (muscle spasm)
		//        Frightfulness (goose bumb, cat back)
		//        Freezing (brrr...)
		//        
		//    Buzzing or vibration or noise
		//        Bees
		//        Waterfall
		//        Rocket
		//        Fire? (Very light vibrations in that case)
		//        Bass (enhance low end sounds)
		//    Look into vibration web API 
	},

	pointinDevice: {

	},
	mouse: {

	},
	click: {
		// A type of sound...
		// A physical interaction with the button on a mouse.
		// The thing you do with a mouse
		// Or a specific sound
	},
}
