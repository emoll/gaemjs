const theHuman = {
	// You can only create meaningful and guiding experiences for something you know.

	comments: {
		// the human missions:
		// 	compress knowledge to be used by other people.
		// 	Be healthy and live long

		// Humans seeks improvements in their physical condition through their emotional condition.
		// Overcoming limitations of inate biases.

		//	people comprehend experiences through stories
		// stories is what we remember of the dynamics of experience over time
		// Stories allows for reflection.
	},

	human: {
		is: {
			biological: 1,
		},
		has: {
			biologicalCondition: 1, //physicalCondition: 1, // well-being?
			humanNeeds: 1,
			memory: 1,
			emotionalCondition: 1,
			humanBiases: 1,
			personality: 1,
			humanPreferences: 1, //individual preferences
		}
	},

	// Shared amongst all humans
	humanNeeds: {
		// human needs serves underlying evolutio-biological needs.


		maslowsHierarchyOfNeeds: {
			Physiological: {
				Homeostasis: 1,
				Health: 1,
				sustenence: 1, //Food and water
				Sleep: 1,
				Clothes: 1,
				Shelter: 1,
				sex: 1, //?
			},
			Safety: {
				//security
				Personal: 1, // physical security of human?
				Emotional: 1, // nervousness // feeling vulnerable or inferior or unstable
				Financial: 1, // having resources to support a standard of living. now and in the foreseeable future.
				Health: 1, // physical vitality
				wellBeing: 1, // physical vitality, mental alacrity,
				risks: 1, // Against accidents/illness

				// safeness.
				// TODO: search safe in this document and ensure it is linked with or referring to safety.
			},
			SocialBelonging: {
				Friendships: 1, // relationship and mutual affection
				Intimacy: 1, // that involves physical or emotional intimacy
				Family: 1, // relationship with people that have similar genes.
			},
			SelfEsteem: 1, // ego needs...// validation //aka accepting yourself
			SelfActualization: 1, // pursuit... working on what feels important. be caught in the flow of doing what you are supposed to.
			Transcendence: 1, // go beyond self, transcend your limitations. Identify as the world. //existing after you are dead.
		},


		CONNECTION: { //
			acceptance: 1, //
			affection: 1, //
			appreciation: 1, //
			belonging: 1, //
			cooperation: 1, //
			communication: 1, //
			closeness: 1, //
			community: 1, //
			companionship: 1, //
			compassion: 1, //
			consideration: 1, //
			consistency: 1, //
			empathy: 1, //
			inclusion: 1, //
			intimacy: 1, //
			love: 1, //
			mutuality: 1, //
			nurturing: 1, //
			respect: 1, //self-respect:1, //
			//CONNECTION continued
			safety: 1, //
			security: 1, //
			stability: 1, //
			support: 1, //
			known: 1, //to know and be known:1, //
			seen: 1, //to see and be seen:1, //
			understanding: 1, //to understand and:1, //
			understooding: 1, //be understood:1, //
			trust: 1, //
			warmth: 1, //
		},
		physicalWellbeing: { //PHYSICAL WELL-BEING
			air: 1, //
			food: 1, //
			movement: 1, //movement/exercise:1, //
			rest: 1, //
			sleep: 1, //
			sexualExpression: 1, //
			safety: 1, //
			shelter: 1, //
			touch: 1, //
			water: 1, //
		},

		HONESTY: 1, //
		authenticity: 1, //
		integrity: 1, //
		presence: 1, //

		PLAY: 1, //
		joy: 1, //
		humor: 1, //
		//
		PEACE: 1, //
		beauty: 1, //
		communion: 1, //
		ease: 1, //
		equality: 1, //
		harmony: 1, //
		inspiration: 1, //
		order: 1, //
		//
		AUTONOMY: 1, //
		choice: 1, //
		freedom: 1, //
		independence: 1, //
		space: 1, //
		spontaneity: 1, //
		//
		MEANING: 1, //
		awareness: 1, //
		celebrationOfLife: 1, //
		challenge: 1, //
		clarity: 1, //
		competence: 1, //
		consciousness: 1, //
		contribution: 1, //
		creativity: 1, //
		discovery: 1, //
		efficacy: 1, //
		effectiveness: 1, //
		growth: 1, //
		hope: 1, //
		learning: 1, //
		mourning: 1, //
		participation: 1, //
		purpose: 1, //
		selfExpression: 1, //
		stimulation: 1, //
		mattering: 1, //to matter:1, //
		understanding: 1, //
	},
	// Needs test

	// ^ human needs are required maintenance to stay human.

	// human information processing - learning and
	mentalModel: { //(What the players experiences are stored as in their memory)   human psychology and the origin of symbols and shit 
		// A mental simulation of how things work. 
		// • People understand and interact with things by comparing their mental models with the real world. People make errors when their mental models disagree with how things really work. 
		// • Mental models about how things work are called system models. Mental models about how people interact with things are called interaction models. 
		// • Engineers necessarily have strong system models, but typically have weak interaction models. Users develop strong interaction models, but typically have weak system models. Designers must develop strong system and interaction models. 
		// • Leverage existing mental models in design, referencing established conventions and behaviors. Develop strong interaction models by personally using products—i.e., “eating your own dog food”—and observing people using the product. 
		// See Also Affordance • Expectation Effects • Visibility

		// When a interaction model contradicts convention—as with anti-lock brakes—potential benefits be slow to materialize.
	},

	humanEmotionalState: {
		emotionalSystems: {
			sixBasicEmotions: {
				anger: 1,
				disgust: 1,
				fear: 1,
				happiness: 1,
				sadness: 1,
				surprise: 1,
			},
		},

		//TODO: put somewhere:
		confusion: 1, // is confusion an emotion? perplexed.
	},


	//agents and agency
	voluntary: {
		// subject can freely decide to be involved or not without serious consequences or effects.
		// done, given, or acting of one's own free will.

		// The subject has stated an agreement to being exposed to actions within specified cosntraints.
	},



	// Biases and behavioural quirks.
	// biases are tendencies generalizable over a population
	// human biases:
	// 	human tendencies in behaviour generalizable over all people.  

	// biases: heuristics of human tendencies and preferences.
	// Tendencies shared amongst all humans

	indexOfKnownHumanBiases: {
		// BIASES IN APPRECIATION
		// The following are beautiful because they remind us of fulfilling human needs.
		// Beauty
		// Why attractiveness matters
		attractivenessBias: {
			is: {
				bias: 1, // humanBias?
			},
			// tendency to attribute positive attributes to attractive people.
			// they are often seen as intelligent, competent, moral and sociable, just by being pretty.
			humanAttractiveness: {
				symmetrical: 1, // especially facially
				clearSkin: 1, //
				// ideal waist-hip-ratio
				// indications of status
				// signs of health and fertility
				women: {
					// exaggerating fertility
				},
				men: {
					// exaggerating status
				},
			},
		},

		// Attractiveness of bodies and characters	
		faceBodyRatio: { // "Face-ism Ratio" 
			// The ratio of face to body in an image that influences how the person is perceived. 
			// • The face-ism ratio is calculated by dividing the head height by the total visible body height. 
			// • Images depicting a person with a high face-ism ratio—i.e., the face takes up most of the image—focus attention on the person’s intellect and personality. 
			// • Images depicting a person with a low face-ism ratio—i.e., the body takes up most of the image—focus attention on the person’s body and sexuality. 
			// • When a design requires a thoughtful response, use images of people with high face-ism ratios. When a design requires an emotional response, use images with low-face-ism ratios. 
			// See Also Attractiveness Bias • Baby-Face Bias Classical Conditioning • Law of Prägnanz 	

			// The high face-ism ratio image emphasizes attributes like intelligence and ambition. The lower
			// face-ism ratio images emphasize attributes like sexuality and physical attractiveness. 
		},
		WaistToHipRatio: {
			is: {
				bias: 1, //humanBias?
			},
			// A preference for a particular ratio of waist size to hip size in men and women. 
			// • The waist-to-hip ratio (WHR) is a significant factor in determining the attractiveness of people and anthropomorphic objects. 
			// • WHR is primarily a function of testosterone and estrogen levels, and therefore serves as a biological signal of reproductive potential. 
			// • WHR is calculated by dividing the circumference of the waist by the circumference of the hips. 
			// • Men prefer women with a WHR between 0.67–0.80. Women prefer men with a WHR between 0.85–0.95. 
			// • Consider WHR when depicting attractive people and anthropomorphic objects. Feminize objects by making their WHRs approximate 0.7 and masculinize objects by making their WHRs approximate 0.9. 
			// See Also Anthropomorphism • Attractiveness Bias Baby-Face Bias • Supernormal Stimulus 

			// When asked to pick the most attractive bodies, people favored female A and male C,
			// corresponding to WHRs of 0.70 and .90. 

			// Mannequins have changed with the times, but their WHRs have been 0.70 for women and 0.90
			// for men for over five decades. 
		},
		MafaEffect: { // most average face aaaaaaaaaaaaaaaaaaaaa
			// The average face of a local population is generally more attractive than any individual face. 
			// • People find the most average facial appearance (MAFA) of a population more attractive than faces that deviate from the average. Population refers to the group in which a person lives or was raised. 
			// • The effect may be due to the fact that evolution tends to select out extremes. Therefore, a preference for averageness, which results in increased symmetry, may have evolved as an indicator of fitness. 
			// • Use composite images of faces from target populations to represent local perceptions of beauty. 
			// • Consider digital compositing and morphing software to create attractive faces from common faces for advertising and marketing campaigns, especially when real models are unavailable or budgets are limited. 
			// See Also Attractiveness Bias • Baby-Face Bias • Symmetry 

			// Two generations of composites of four males were sufficient to average out idiosyncratic features and increase symmetry
		}, // The most average face of a population is the most attractive.

		// Beauty
		anthropomorphism: { // part of beauty
			is: {
				bias: 1, //humanBias?
			},
			// we like human characteristics in things.
			// We prefer things with human characteristics. over those without.
			// (todo define human characteristics....)

			// think of anthropomorphic forms.
			// antropomorphic forms are evaluated more emotionally

			// use the shapes of women, shapes of men and shapes of children to elicit those associations.

			// these shapes generate attention and emotional connections.

			// • Use contoured or hourglass proportions to elicit feminine and sexual associations. Use round forms to elicit baby-like associations. Use angular forms to elicit masculine and aggressive associations. 
			// these are the same as circle is baby, triangle is evil and square is a dude
			// hourglass shape is a woman. water droplet shape is woman. (small top big bottom)
		},
		babyFaceBias: {
			// things that look like human baby faces are adorable.
			// A tendency to see things with baby-faced features as having the personality characteristics of babies. 
			// • People and things with round features, large eyes, small noses, high foreheads, short chins, and light hair and skin are perceived to be more naive, helpless, and honest than those with mature features. 
			// • Large round heads and eyes appear to be the strongest facial cues contributing to the bias. 
			// • The bias applies to all anthropomorphic things, including people, animals, and cartoon characters, and products such as bottles, appliances, and vehicles. 
			// • Consider the baby-face bias in the design of characters or products when face-like features are prominent. In advertising, use mature-faced people when conveying expertise and authority; use baby faced people when conveying innocence and honesty. 
			// Round products tend to elicit baby-face associations. Angular products tend to elicit mature and masculine associations. 
		},
		contourBias: { // beauty and shapes
			is: {
				bias: 1, //humanBias?
			},
			// A tendency to favor objects with contours over objects with sharp angles or points. 
			// • Things that possess sharp angles or pointed features activate a region of the brain associated with fear. 
			// • The bias likely evolved as a form of threat detection, enabling human ancestors to reflexively detect dangerous plants, animals, and objects. 
			// • In neutral contexts, people prefer round, curvy objects to sharp, angular objects, but the latter is more effective at getting and holding attention. 
			// • The degree of fear activation in the brain is proportionate to the sharpness of objects, and inversely related to object preference. 
			// • Use angular and pointy features to attract and hold attention. Use contoured features to make a positive first impression. In emotionally neutral contexts, favor round, curvy forms over sharp, angular forms. 
			// See Also Affordance • Archetypes • Baby-Face Bias Freeze-Flight-Fight-Forfeit • Threat Detection

			// The angular kettles are more effective at grabbing attention. The contoured kettles are more emotionally appealing. 
		},

		glossBias: { //beauty
			is: {
				bias: 1, //humanBias?
			},
			condition: {
				// perceiving dullness/Glossiness of objects
			},
			effect: {
				// Preferring glossy objects
				// Paying more attention to glossy objects
			}
			// TODO read up more on this...
			// A human preference for glossy versus dull objects. 
			// gloss works as a way to highlight important items
			// • People find glossy objects more interesting and appealing than dull objects. 
			// • For example, people generally prefer glossy lipsticks, jewelry, paper, and paints to their matte counterparts. Young children presented with glossy objects lick them significantly more than dull objects. 
			// • The preference is likely an evolutionary artifact. The ability to find water sources provided early human ancestors with an adaptive advantage. Glossy surfaces suggested nearby water sources. 
			// • Consider the gloss bias when selecting finishes. The bias applies to all objects and images of objects. The bias is stronger in general audiences, and weaker in audiences with experience with different finishes. 
			// See Also Archetypes • Biophilia Effect • Savanna Preference Scarcity • Supernormal Stimulus 

			// Humans have evolved to find glossy things appealing. You might even say we thirst for them.
		},

		// Environmental design for human preferences
		savannaPreference: {
			is: {
				bias: 1, //humanBias?
			},
			condition: {
				//player knows about multiple environments
			},
			effect: {
				//Player tend to prefer the more savanna-like environments
			}
			// A preference for savanna-like environments over other types of environments. 
			// • Humans prefer parklike landscapes that feature openness, uniform grass, scattered trees, visible water, and signs of wildlife. 
			// • The savanna preference likely provided adaptive benefits to human ancestors—i.e., those who lived in savannas enjoyed a survival advantage over those who lived in harsher environments. 
			// • The savanna preference is found across all age ranges and cultures, but it is strongest in children. 
			// • Consider the savanna preference when featuring landscapes in art, advertising, and other contexts involving depictions of natural environments—especially contexts involving young children, such as stories, toys, and play environments. 
			// See Also Archetypes • Biophilia Effect • Defensible Space Prospect-Refuge • Self-Similarity 

			// The Teletubbies mesmerized children in more than sixty countries and thirty-five languages.
			// Simple stories, baby-faced creatures, and a savanna landscape equal excellent design for young children. 
		},
		biophiliaEffect: {
			is: {
				bias: 1, //humanBias?
			},
			condition: {
				// player sees a view of nature or is in a nature environment.
				// This applies to representations of nature as well like imagery, painted pictures or photos.
			},
			effect: {
				// reduced stress
				// improved concentration
				// restorative cognition
				// physical benefits
			}
			// nature views and natural environments replete with lush green flora and visible water causes a state of reduced stress and improved concentration with restorative cognitive and physical benefits.
			// this effect is likely a vestigial response to ancestral habitats, related to the savana preference.
			// both imagery (painted or photo), and real plants can trigger this effect.
			// consider the effect for the design of all environments. 
			// Pay espececial attention and weight to this effect when designing spaces for learning, healing, and concentration.
		},
		cathedralEffect: { // environmental composition and architecture
			is: {
				bias: 1, //humanBias?
				symbolism: 1, //?
			},
			// High ceilings promote abstract thinking. Low ceilings promote detail-oriented thinking. 
			// • Conspicuous ceiling height—that is, noticeably low or noticeably high ceilings—promotes different types of cognition, with high ceilings promoting abstract thinking and creativity, and low ceilings promoting concrete and detail-oriented thinking. No effect is observed if the ceiling height goes unnoticed. 
			// • The effect may be due to priming, or a vestigial preference for high tree canopies and open skies, as would have been common on the African savannas. 
			// • Favor large rooms with high ceilings for tasks requiring creativity and out-of-the-box thinking. Favor small rooms with low ceilings for tasks requiring detail-oriented work (e.g., surgical operating room).
			// Favor high ceilings to extend the time in which visitors remain on site (e.g., casino) and low ceilings to minimize loitering (e.g., fast-food restaurant). 
			// See Also Priming • Prospect-Refuge • Savanna Preference 
			// High ceilings promote abstract and creative thinking. Low ceilings promote concrete and detail oriented thinking. 
		},
		prospectRefuge: {
			is: {
				bias: 1, //humanBias?
			},
			condition: {
				//knowledge about multiple environments
			},
			effect: {
				//preferring ones with unobstructed views
			}
			// A preference for environments with unobstructed views, areas of concealment, and paths of retreat.
			// • The design goal of prospect-refuge is to create spaces where people can see without being seen.
			// • People prefer the edges versus middles of spaces, ceilings or covers overhead, spaces with few access points, and spaces that provide unobstructed views from multiple vantage points.
			// • Consider prospect-refuge in the creation of landscapes, residences, offices, and communities.
			// • Create multiple vantage points within a space, so that the internal and external areas can be easily surveyed.
			// • Make large, open areas more appealing by using screening elements to create partial refuges.
			// See Also Biophilia Effect • Cathedral Effect Defensible Space • Savanna Preference

			// The design of a cafe from the perspective of prospect-refuge.
		},





		// BIASES IN EXPLORATION


		// The familiar
		confirmationBias: { //human bias huamn behaviour psychology
			is: {
				bias: 1, //humanBias?
			},
			// It is easier to create exceptions for a framework than throw it over and re-learn a deeper truth from scratch.

			// A tendency to favor information that confirms pre-existing views. 
			// • The confirmation bias is a tendency to focus on information that supports pre-existing views, and to ignore information that contradicts pre-existing views. 
			// • Effects of the bias include overconfidence, selective memory, poor decision making, and resistance to change in light of contrary evidence. 
			// • For example, a person who believes the moon landing was faked seeks out information that confirms this belief, and ignores evidence to the contrary. 
			// • The bias is strongest for beliefs in which there has been significant emotional investment. 
			// • Consider the confirmation bias when conducting research and development. Recognition of the bias is the best antidote. Expose yourself to contrary opinions and independent critiques. Beware exploitation of the confirmation bias by those with political agendas. 
			// See Also Cognitive Dissonance • Selection Bias

			// The long-term trend clearly indicates rising global temperatures, which is why denialists tend to on the short-term trend. 
		}, // Humans tend to prefer confirming pre-existing views and perspectives.
		mereExposureEffect: {
			is: {
				bias: 1, //humanBias?
			},
			// The more people are exposed to a stimulus, the more they like it. 
			// • Repeated exposure to a neutral or positive stimulus makes people like it more. Repeated exposure to a negative stimulus makes people like it less. 
			// • The mere-exposure effect is used in music, slogans, images, advertisements, and political campaigns. 
			// • The mere-exposure effect is strongest during the first 10–20 exposures. Brief exposures are more effective than long exposures. 
			// • Consider the mere-exposure effect to strengthen advertising and marketing campaigns, enhance the credibility and aesthetic of designs, and improve the way people think and feel about a message or product. Keep the exposures brief, and separate them with periods of delay. 
			// See Also Classical Conditioning • Cognitive Dissonance Framing • Priming • Stickiness 

			// The mere-exposure effect is commonly used to increase the likability and support of political leaders. Similar techniques are used in marketing, advertising, and electoral campaigns. 
		}, // We like things more based on how often we experience it.



		// Error in compression
		expectationEffects: {
			// We tend to have preconceived notions of things based on previous experiences. (If it seems similar to what we've experienced, we override our curiosity and perception with previous judgements.)

			// Changes in perception or behavior resulting from personal expectations or expectations of others. 
			// • When people have expectations about something, it influences how they perceive and react to that thing. 
			// • For example, students perform better or worse based on the expressed expectations of their teachers. And patients experience treatment effects based on their belief that a treatment will or will not work. 
			// • These effects create challenges for direct measurement techniques, such as focus groups, interviews, and surveys. 
			// • Consider expectation effects when conducting research, interviews, and testing. Favor blinded experiments and noninvasive research such as field observation over invasive methods. In persuasion contexts, set expectations in a credible fashion versus letting people form their own unbiased conclusions. 
			// See Also Affordance • Confirmation Bias • Framing Mere-Exposure Effect • Uncertainty Principle

			// A taste test between two wines: an inexpensive wine in cheap packaging and an expensive wine packaging. People rate the expensive wine as tasting better—even when the wines are same. 
		}, // We think of things, as the closest thing we already know. We rely on memory rather than curiosity based on how similar the first impression of a thing is. (especially in non-curios and non-open mindsets)
		powerOfDefaults: {
			// you use conventions and defaults because everyone knows them so then you have an interface through which you can communicate with your audience.
			// Why you should rely on tropes and culturally established things...
			// related to the thing about
		},


		// Errors in reasoning
		selectionBias: {
			// A bias in the way evidence is collected that distorts analysis and conclusions. 
			// • Selection bias results from the non-random sampling of evidence. Accordingly, it over-represents certain aspects of the evidence and under-represents others, distorting analysis and conclusions. 
			// • For example, if subscribers of a science magazine are surveyed and their responses generalized to the overall population, science-minded viewpoints would be over-represented in the analysis and results. 
			// • Avoid selection bias. Collect data from entire populations when they are small. Randomly sample from populations when they are large. 
			// • Scrutinize the selected population and sampling methods when evaluating conclusions based on statistical analysis. 
			// See Also Confirmation Bias • Garbage In-Garbage Out Normal Distribution • Uncertainty Principle 

			// The red dots indicate areas of combat damage received by surviving WWII bombers. Where would you add armor to increase survivability? The statistician Abraham Wald recommended reinforcing the areas without damage. Since these data came from surviving aircraft only, bombers hit in undotted areas were the ones that did not make it back. 
		}, // Sampling error. We tend to base our judgment on a limited sample rather than general random population. We mistake our chosen local to represent the global.
		dunningKrugerEffect: { //Human bias
			// Systems tend to be naturally incapable of judging their own competence and perfomance.
			// They need to seek external validation or learn how to detect compentency and performance.
			// Feedback and critique promotes the development of self-assessment skills.

			// Humans tend to over estimate their competence and performance.
			// You mainly gain the ability to judge competence, in the process of becoming competent.
			// (Incompetent people lack the information needed to judge how competence)
			// This is why people on average like simple, easy to grasp things.
			is: {
				bias: 1, //humanBias?
			},
			// A tendency for unskilled people to overestimate their competence and performance. 
			// • Incompetent people lack the knowledge and experience to recognize their own incompetence, as well as the competence of others. 
			// • This creates a vicious cycle: An incompetent person can’t perceive their own incompetence because they are incompetent; and overcoming incompetence requires the ability to distinGUIsh skill levels, which is an ability they lack. 
			// • Conversely, highly competent people tend to underestimate their abilities and performance, and overestimate the skills of others. 
			// • Combat the Dunning-Kruger effect by teaching the inexperienced how to discern competence from incompetence. Provide regular feedback and critiques to promote the development of self-assessment skills. 
			// See Also Crowd Intelligence • Design by Committee Hanlon’s Razor • Not Invented Here 

			// The least competent tend to be the most confident, and then the roller coaster ride of reality begins. 
		}, // A tendency for unskilled people to overestimate their competence and performance. 





		// Human needs and reactions - Evolutionary/biological response
		FreezeFlightFightForfeit: { //human psychology behaviour tendency biad needs stuff
			// The ordered, instinctive response to acute stress. 
			// • When people are exposed to stressful or threatening situations, their instinctive responses are to freeze, flee, fight, and forfeit, in that order. 
			// • When a threat is suspected, the instinctive response is to freeze—to stop, look, and listen for threats. 
			// • When a threat is detected, the instinctive response is to flee—to escape from the threat. 
			// • When unable to escape from a threat, the instinctive response is to fight—to neutralize the threat. 
			// • When unable to neutralize the threat, the instinctive response is to forfeit—to surrender to the threat. 
			// • Consider freeze-flight-fight-forfeit in the design of systems that involve performance under extreme stress. It is critical to design systems and training to address each stage of the stress response differently versus a one-strategy-fits-all approach. 
			// See Also Classical Conditioning • Threat Detection

			// People respond to extreme stress in four ways. It is important to design systems and training to each of them. 
		},
		tendAndBefriend: {},

		// human bias and needs and preferences
		hunterVsNurturerBias: {
			is: {
				bias: 1, //humanBias?
			},
			// A tendency for male children to be interested in hunting-related things and female children to be interested in nurturing-related things. 
			// • Male children tend to engage in play that emulates hunting behaviors. Female children tend to engage in play that emulates nurturing behaviors. 
			// • Hunter bias is characterized by activities involving object movement and location, weapons and tools, hunting and fighting, predators, and physical play. 
			// • Nurturer bias is characterized by activities involving form and colors, facial expressions and interpersonal skills, caretaking, babies, and verbal play. 
			// • Consider hunter-nurturer bias when designing for children. Consider hunting-related items and activities for male children, and nurturer-related items and activities for female children. 
			// See Also Archetypes • Baby-Face Bias • Contour Bias Supernormal Stimulus • Threat Detection 

			// Like their human cousins, female vervets prefer stereotypically female toys and male vervets prefer stereotypically male toys. 
		}, // Gender based bias in interests.

		ikeaEffect: { //IKEA
			// We value things higher that we have put effort into making. (irregardless if the thing we made is worse than something someone else made.)
			// customization, alternative choices and required effort increases the perceived value.
			// make them think they are part of creating the experience..

			// The act of creating a thing increases the perceived value of that thing to the creator. //(you see the effort and thought required. Dunning cruger.)
			// • Creating or partially creating a thing (e.g., assembling furniture) makes it more valuable to the creator. 
			// • People are willing to pay more for products they create than equivalent preassembled products. 
			// • People value things they personally create as much as if it had been created by an expert. 
			// • The level of effort invested in creation corresponds to its level of valuation: high effort translates into high valuation, and low effort translates into low valuation. 
			// • The IKEA effect only holds when tasks are completed. 
			// • Consider the IKEA effect in product strategy and user-experience design. Engage users in the creation of products to increase their value perception. 
			// See Also Closure • Cognitive Dissonance • Not Invented Here Sunk Cost Effect • Zeigarnik Effect

			//	The effort people expend when assembling IKEA furniture actually makes them value the furniture
		}, // self determination theory...

		scarcity: {
			// Things become more desirable when they are in short supply or occur infrequently. 
			// • Scarce items are more valued than plentiful items. 
			// • Scarcity motivates people to act. 
			// • Apply the principle through: exclusivity, limited access, limited time, limited number, and surprise. 
			// • The principle holds across the spectrum of human behavior, from mate attraction to marketing to tactics of negotiation. 
			// • The effect is strongest when the desired object or opportunity is highly unique, and not easily obtained or approximated by other means. 
			// • Consider scarcity when designing advertising and promotional initiatives, especially when the objective is to move people to action. 
			// See Also Expectation Effect • Framing • Veblen Effect 

			// The Running of the Brides event at Filene’s Basement offers bargain-basement prices on bridal gowns once a year, one day only. The competitive shopping chaos that ensues is a case study in the power of scarcity.
		}, // The less of them can be gotten, the more desirable it is.


		// SOCIAL BIASES
		Reciprocity: {
			// The tendency for people to give back to those who have given to them. 
			// • Reciprocity is a tendency to respond to kindness with kindness. Anything that is perceived as a gift or concession creates the effect. 
			// • The acts that create reciprocity most effectively are meaningful, personalized, and unexpected. Reciprocity only works if the initiating act is perceived as sincere. 
			// • For example, a nonprofit organization sending mail-outs that contain useful, personalized gifts—such as mailing labels—will receive significantly more donations in greater amounts than sending just a solicitation letter. 
			// • Use reciprocity to promote goodwill, garner attention and consideration, and move people to action.
			// Apply the principle sincerely and sparingly, else it will be perceived as manipulative. 
			// See Also Framing • IKEA Effect • Nudge • Scarcity • Shaping

			// After a decade of mailing out free-trial CDs, AOL increased its subscribership 12,000 percent. The campaign used reciprocity effectively in the early stages, but they didn’t know when to stop. As people began receiving multiple CDs, the once novel gesture looked increasingly like a wasteful manipulation. 
		},
		socialTrap: {
			// A tendency to pursue short-term gains that create long-term losses for the greater group. 
			// • A situation in which people act to obtain short-term gains, and in so doing create losses for everyone in their group, including themselves. 
			// • For example, ranchers overgraze cattle on public land. This depletes the land of grasses faster than the land can replenish. This then starves all of the ranchers’ cattle, including the original overgrazers. 
			// • Social traps are most problematic when a resource is readily available and highly desirable, when people compete to access and use that resource, and when the long-term costs are not visible or easily monitored. 
			// • Mitigate the effects of social traps by enforcing sustainable limits on resource use (e.g., fishing limits), rewarding cooperation and punishing freeloading, and increasing the visibility of long-term costs. 
			// See Also Confirmation Bias • Gamification • Nudge Sunk Cost Effect • Uncertainty Principle • Visibility

			// Despitetollways.￼
			// everyone wanting to get home as fast as possible, traffic jams occur—but rarely on
			// Having to pay tolls to drive on the roads moderates use, mitigating the social trap. 
		}, // We tend to overdo opportunities that benefits us or our local group to the detriment of everyone.


		ThreatDetection: {
			// Threatening things are detected more efficiently than nonthreatening things. 
			//￼ 
			// • People reflexively detect and pay attention to certain threatening stimuli, such as spiders, snakes, predators, and angry human faces. 
			// • This threat-detection ability provided early human ancestors with an adaptive advantage. Modern humans have inherited this ability. 
			// • Things possessing key features of threatening stimuli can also trigger threat detection; for example, a wavy line that looks snakelike. 
			// • Consider using key features of threatening stimuli in your designs to capture and hold attention. 
			// See Also Archetypes • Black Effects • Contour Bias Freeze-Flight-Fight-Forfeit • Savanna Preference

			// In visually complex environments, threatening stimuli are detected more quickly than stimuli. 
		}, // Threats steals our attention easier than non threats




		TopDownLightingBias: {
			// A tendency to interpret objects as being lit from a single light source from above. 
			// • People interpret dark areas of objects as shadows resulting from a light source above the object. 
			// • Things that are lit from the top tend to look natural; whereas things that are lit from the bottom tend to look unnatural. 
			// • Objects that are lighter at the top and darker at the bottom are interpreted to be convex, and objects that are darker at the top and lighter at the bottom are interpreted to be concave. 
			// • Use a single top-left light source to depict natural-looking things. Explore bottom-up lighting when depicting unnatural-looking or foreboding things. Adjust the difference between light and dark areas to vary the perception of depth. 
			// See Also 3D Projection • Figure-Ground Relationship Threat Detection • Uncanny Valley 

			// The Lincoln Memorial is usually lit top-down (left), but it has on occasion been lit bottom-up
			// (right), revealing zombie Lincoln. 
		}, // pattern recognition of lighting


		// Attention biases
		inattentionalBlindness: {
			// A failure to perceive an unexpected stimulus presented in clear view. 
			// • When concentrating on tasks, people are blind to unrelated stimuli up to 50 percent of the time. 
			// • For example, a shopper seeking a certain brand of soda will likely notice other soda bottles, but not dishwasher soap bottles. 
			// • Inattentional blindness is behind many of the tricks and misdirections employed by magicians. 
			// • Consider inattentional blindness in contexts where attention is key, including security, safety, and advertising. Given the robustness of the effect, the best strategy is to create or alter tasks to focus attention on desired stimuli (e.g., receiving a coupon book prior to visiting a store can predefine the shopping targets ahead of time). 
			// See Also Flow • Interference Effects • Signal-to-Noise Ratio 

			// When people were asked to count the number of times the team in the white shirts passed a basketball, half failed to see the person in a gorilla costume casually stroll across the screen.
		}, // Humans get blind to their surroundings when focusing on a task.
		Visibility: {
			// what we perceive we are biased to use. (we are biased to use things presented to us rather than things we can find or generate)
			// Things in clear view are more likely to be used than things not in clear view. 
			// • Visible controls and information act as cues for what is and is not possible. Things that are not seen are less likely to be considered, and this is especially true when people are under stress. 
			// • Accordingly, systems should clearly indicate their status, the key actions that can be performed, and the consequences of those actions once performed. 
			// • Visibility increases probability of use when the number of options is small, but can overwhelm when the options are numerous. Beware kitchen-sink visibility. 
			// • Design systems that clearly indicate their status, possible actions, and consequences of those actions.
			// Balance visibility and complexity by selectively revealing and concealing controls and information based on context, relevance, and frequency of use. 
			// See Also Affordance • Control • Performance Load Progressive Disclosure • Recognition Over Recall 

			// Analysis of the Three Mile Island accident in 1979 revealed blind spots that made solving the
			// problems almost impossible. In addition, alarms were blaring, lights were flashing, and critical
			// system feedback was routed to a printer that could only print fifteen lines per minute—system
			// status information was more than an hour behind for much of the crisis. 

			// Out of sight, out of mind. 
			// Short term memory.
		}, // We tend to think about and interact more with the things we are currently directly sensing and seeing.
		interferenceEffects: {
			// Things that trigger conflicting thought processes reduce thinking efficiency. 
			// • Interference effects occur when nonessential mental processes interfere with essential mental processes, increasing errors and slowing task performance. 
			// • Nonessential mental processes can be triggered by conflicting meanings, distractions in the environment, and memories that are irrelevant to the task at hand. 
			// • For example, a green “stop” button triggers a mental process for “go” because of the color green, and a mental process for “stop” because of the label “stop”. The two mental processes interfere with one another. 
			// • Minimize interference by eliminating elements that distract. Keep designs simple. Abide by strong color and symbol conventions when they exist (e.g., red means stop, green means go). 
			// See Also Errors • Inattentional Blindness • Performance Load 

			// Arrows mean “go”, but red arrows mean “stop”. When traffic signs and signals create interference, accidents increase. 
		}, // Non-essential stimuli (noise) interferes with the thinking efficiency/performance.



		// Human interaction - accessibility and human performnce.
		fittsLaw: {
			// The time required to touch a target is a function of the target size and the distance to the target. 
			// • Proposed by American psychologist Paul Fitts. 
			// • Used to model pointing to an object or computer screen using your finger or pointing device. 
			// • The law is predictive over a wide variety of conditions, devices, and people. 
			// • The primary implication of Fitts’ law is that close, large targets can be accessed more quickly and with fewer errors than distant, small targets. 
			// • Constraints can effectively increase target size. For example, a dropdown menu located at the top of a computer display effectively has infinite height because the screen edge stops the cursor. 
			// • Consider Fitts’ Law when designing controls and control layouts. Keep controls close and large when speed or accuracy is important. 
			// See Also Constraints • Errors • Hick’s Law • Performance Load

			// The time and error rate involved in whacking a mole is a function of the distance between the and the mole. 
		},

		// errors bias....
		SunkCostEffect: {
			// The tendency to continue investing in an endeavor because of past investments in that endeavor. 
			// • People frequently let past investments influence future investments. For example, you buy a nonrefundable ticket to a show, decide you no longer want to go to the show, but you go anyway because you paid for it. 
			// • Rationally speaking, past investments, or sunk costs, should not influence decision making. Only the cost-benefits of current options should influence decisions. 
			// • People are susceptible to the effect because they fear losses more than they desire gains, and because they do not want to feel or appear wasteful. The effect leads people and organizations to make bad decisions, throwing good money after bad. 
			// • Recognizing the sunk cost effect is the first step to recovery. Focus on current cost-benefits only. Beware the words, “We have too much invested to quit.” 
			// See Also Cognitive Dissonance • Cost-Benefit • Framing 

			// The British and French governments funded development of the Concorde SST long after they knew it would be an economic failure. Why? They had invested too much to quit.
		}, // When we've payed to pursue something we tend to continue the pursuit even when we are no longer believe in the outcome.



		veblenEffect: {
			is: {
				bias: 1, //humanBias?
			},
			// A tendency to find a product more desirable because it has a high price. 
			// • Proposed by the economist Thorstein Veblen. 
			// • In certain cases, higher prices increase demand, and lower prices decrease demand. 
			// • For example, the effect is most pronounced for items and services that signal status, such as art, jewelry, clothes, cars, fine wines, hotels, and luxury cruises. 
			// • High prices increase perceived quality, and low prices decrease perceived quality. 
			// • Consider the Veblen effect in marketing and pricing. Promote associations with high status people (e.g., celebrities). Employ strategies to discourage knockoffs, including legal protection, watermarking, and aggressive counter-advertising. Set prices high based on the intangible aspects of the offering. 
			// See Also Classical Conditioning • Cognitive Dissonance IKEA Effect • Left-Digit Effect • Scarcity 

			// Electric cars are historically slow, ugly, and uncool. How to change this perception? Introduce a sexy electric car in limited numbers, associate it with people of status, and charge a premium.
			// Once product perception makes the transformation from white elephant to white tiger, introduce lower-priced models. The Tesla Roadster: Veblen good. 
		}, // High price bias. (thinking things with high price are more rare and desirable)

		LeftDigitEffect: { //pricing
			// People overweigh the left-most digits of prices in buying decisions. 
			// • When viewing a price, people have an emotional reaction to the first digit in the sequence and overweigh its value. 
			// • In cultures that read from left to right, this reaction is based on the left-most digit in prices. 
			// • Given two prices, $29.99 and $30.00, people react as if the difference is not one cent, but one dollar. This reaction can increase sales up to 15 percent. 
			// • Prices ending in 99s indicate low prices, but also lower quality. Prices ending in round numbers indicate high prices, but also higher quality. 
			// • Use the left-digit effect when price is more important than quality in buying decisions. 
			// See Also Framing • Priming • Serial Position Effects 

			// Ending prices with nines is a good strategy when price drives buying behaviors, as with gasoline. 
		},






		// human memory biases
		memorability: { // and forgetfulness... which things stick to memory and which does not.
			repetition: 1, // repetition legitimizes
			timing: {
				spacedRepetition: 1, // look into the optimal timing spacing.
			},
			uniqueness: 1,
			contrast: 1, // sticking out...

			exhaggeration: 1,
			unprecedented: 1,
			vividness: 1, // ?
			chock: 1, //?
			brightness: 1, //?
			stimulation: 1, //high stimulation
			animation: 1, //movement triggers some more brain area...
			colorfulness: 1,
			clarity: 1, //not muddled with a lot of noise.
			consistency: 1, //you can not remember a pattern you can not follow or understand
			detail: 1, //something detailed and crisp is more memorable than something blurry and unclear.
			contextualSensemaking: 1, //if it makes sense, then we learn it.

			positive: 1, //we remember good times and good things. especially when we are in good mood.
			emotional: 1, //strong emotions and emotional moments
			//needs based
			sexual: 1, //because maslows.
			danger: 1, //see maslow
			drama: 1,
			meaningful: 1,
			//https://docs.google.com/document/d/1t3nhrehzloA2kZwKOOUvFbTsPSIS0ScAjU--nmnmy8g/edit#heading=h.fngeqp4rk7f7
			ZeigarnikEffect: {
				// A tendency to experience intrusive thoughts about a task that was interrupted or left incomplete. 
				// • Proposed by Soviet psychiatrist and psychologist Bluma Zeigarnik. 
				// • The unconscious mind seeks closure and completion, and preoccupies the conscious mind until it gets it.
				// Accordingly, interrupted or incomplete tasks are better remembered than completed tasks. 
				// • Once an interrupted task is completed, the recall benefits are lost. For example, waiters have better recall for in-process orders than served orders. 
				// • The effect is strongest when people are highly motivated to complete a task. 
				// • Apply the Zeigarnik effect to engage and maintain attention. For example, the “To be continued...” device is used in cliffhangers to keep people interested. Most importantly, never use the Zeigarnik effect
				// to... 
				// See Also Closure • Flow • Gamification • IKEA Effect

				// A never-ending series of puzzles leaves Tetris players shifting blocks in their dreams, a condition dubbed the Tetris Effect.
			},
			vonRestorffEffect: { //add to memory
				// Uncommon things are easier to recall than common things. 
				// • Proposed by German psychiatrist Hedwig von Restorff. 
				// • The von Restorff effect results from the increased attention given to novel things relative to other things. 
				// • For example, in the set of characters EZQL4PMBI, people will have heightened recall for the 4 because it is the only number in the sequence. 
				// • The strength of the effect is a function of the novelty of the thing to be recalled. 
				// • Apply the von Restorff effect to attract attention and increase memorability. Since recall for the middle items in a sequence is weaker than items at the beginning or end, use the von Restorff effect to boost recall for middle items. Consider unusual words, sentence constructions, and images to improve interestingness and recall. 
				// See Also Highlighting • Layering • Mnemonic Device Serial Position Effects • Stickiness • Threat
				// Detection

				// The highly distinctive form of the Wienermobile makes it—and the Oscar Mayer brand completely unforgettable. 
			},
		}, // What humans tend to remember
		// memory - the start, peak and end of an event are more memorable. //figure out how to define and overlap events.
		serialPositionEffects: {
			// Things presented at the beginning and end of a sequence are more memorable than things presented in
			// the middle. 
			// • Things presented first generally have the greatest influence; they are not only better recalled, but influence the interpretation of later items. For example, words presented early in sentences have more impact than words presented later. 
			// • Things presented last are more memorable when the presentations are separated in time, and a person is recalling information or making a selection soon after the last presentation. 
			// • Use serial position effects to increase recall or influence the selection of specific items. Present important items at the beginning or end of a sequence in order to maximize recall. 
			// See Also Chunking • Interference Effects • Left-Digit Effect 

			// Items at the beginning and end of lists are easier to remember than items in the middle. If recall is attempted 10 seconds after the list is presented (RED), recall is about the same. If recall is attempted more than 30 seconds after the list is presented (BLUE), the items at the beginning are still easily recalled, whereas the items at the end are becoming less memorable. 
		}, // nature of human memory over time





		// Uncanney valley seems like the least important bias, just sayin.
		uncannyValley: {
			// Both abstract and realistic depictions of human faces are appealing, but faces in between are not. 
			// • When a face is very close but not identical to a healthy human—as with mannequins or computer generated renderings of people—it is considered repulsive. This is the uncanny valley. 
			// • The uncanny valley refers to human features generally, but uncanny faces are the key source of the revulsion. 
			// • The response is likely due to evolved mechanisms for detecting and avoiding people who are sick or dead. 
			// • Until robots, computer renderings, and mannequins are indistinGUIshable from humans, favor more abstract versus realistic depictions of faces. Negative reaction is more sensitive to motion than appearance, so be particularly cognizant of jerky or unnatural facial expressions and eye movements. 
			// See Also Anthropomorphism • Attractiveness Bias Threat Detection • Top-Down Lighting Bias

			// Mannequins that are realistic—but not perfectly realistic—are considered repulsive, dwelling in uncanny valley. 
		}, // abstractions are beatiful, reality is beautiful. human faces that are inbetween abstract and realistic are horrendous. Uncanney representations looks like sick or damaged versions of the real thing (people).

	},

	humanImpressions: { // impressions from experiences:
		tiring: {
			does: {
				increase: {
					tiredness: 1,
				}
			}
			// wearing,

		},

		lighthearted: {
			// cheerful
			// carefree
			// not too serious.
			// (not making the heart go intensely)
			// like soft and nice or something dunno.
		},
		relaxing: {
			//reducing tension or anxiety

			//restful

			//often using safety and meeting needs.

			// non challenging and without antagony.
		},

		refreshing: {
			// new and pleasant?
			// stimulating
			// making young/fresh again

			// opposite of rut.
		},
		funny: {
			// making you laugh
			// causing laughter or amusement
			// comedy??? comedic????????	
		},
		silly: {
			// silly is like surprising/unexpected in a harmless way
			// in a silly game animals wears clothes
			// not serious
			// not dramatic
			// can be adorable
		},

		thrilling: 1,
		exciting: 1,
	},



	preferenceTerms: { //bias words/language
		likes: 1,
		dislikes: 1,
		hates: 1,
	},



	emotionality: {
		// body language
		// facial expression
		// postures
		// sensations (bodily)
		// emotional music
		// emotional associations

		// creating rapport between characters
		// usually involves intimate socialization
		// you care about the emotions of people that you relate to.

		// closely related to the fulfilllment of needs. like I learned from NVC

		//https://docs.google.com/document/d/12wrmo3uULuFqWLL5jVi9QVx8Tczf_AX3PhG62OGMyTU/edit#heading=h.6d1o58ar4h78
	},
	emotional: {
		//high emotionality
	},
	coreEmotions: { //based on inside-out lol. look at that emotion diagram too...
		disgust: 1,
		fear: 1,
		anger: 1,
		joy: 1,
		sadness: 1,
	},
	processOfAbstraction: { //https://movies.stackexchange.com/questions/42134/inside-out-what-is-non-objective-fragmentation
		// abstract thought is the shortcut from long term memory to the train of thought and to imagination.
		// stages: of abstracting something
		reality: 1,
		nonObjectiveFragmentation: 1, // non objective fragmentation. no longer objective. becoming the elements.
		deconstructing: 1, // seeing the elements as separate. //non-interacting pieces.
		twoDimensional: 1, // flat plan or schematic that connect only the labels of the elements.
		nonFigurative: 1, // non-manifested wish. Only have an inkling of a general direction.

	},
	//	affection:1,
	expressiveness: { //what makes something an expressive video game?
		//is something expressive inherently highly stimulant?
	},




	// human action
	play: {
		// Engage in activity for enjoyment and recreation rather than a serious or practical purpose.
		and: {
			interaction: 1, //Engage in activity
			// for enjoyment and recreation
			motivation: {
				is: {
					aMoreThanB: {
						or: {
							enjoyment: 1,
							recreation: 1
						},
						or: {
							serious: 1,
							practical: 1,
						}
					}
				}
			}

			//			play does not have immediate life consequences or outcomes but is rather an enactment or simulation for training
			// purpose can be acting with resting of consequences.
			//			playing is similar to enacting a scenario, as if different rules applied.
			// games can be divided into soft games with fuzzy boundaries and hard games with rules. 
		}

		//measured inputs and considerations.

		// engage in a limited context
		// Without serious consequences
		// not serious

		// A player is someone consciously and willingly engaging with the rules of a game
		// A player is most likely a human, but could also be an animal or AI
		// human player
		// what is a player.

		// Projecting themselves into a domain or world of constraints that is non real. // That has not the consistency of the real world.


		// Play makes people smart:
		// play matures the neocortex
		// social play (with people) leads automatically to social integration
		// You are only playful if all your other needs are met.

	},
	playable: { //can be played
		can: {
			play: 1,
		}
	},
	learning: {

	},
	understanding: {

	},
	grokking: {
		// Built a sufficiently accurate model as to be capable of consistently producing sufficient or perfect results
		// Understand something intuitively or by empathy... (statistics? lol)
	},


	alienation: {
		// alienation is a negative feeling.
		// it is when you do not understand or can not relate emotionality.
		// too much novelty leads to alienation
	},

	engagement: {
		// todo define engagement...
		// player interactions (button presses)
		// uninterupted playing.. (if the player takes a lot of breaks then they might not be that engaged?)
	},
	experience: { // player experience... impressionable stimulation
		// encounter or undergo event or occurrence which leaves an impression (memory) on someone, momentarily or long lasting.
		// either changes the player or leaves memories.
		// the sum total of stimulation received to the player from the outside in terms of sensual stimulation, and inside in terms of memories, feelings and reactions.

		//Belongs and can be given to something with a memory.

		//requires attention.
		is: {
			remembered: 1, // must be are have been remembered.
			memorable: 1, //enough to leave a momentarily or long lasting impression.
		},
		has: {
			valence: {
				// is it positive or negative, or neutral or mixed 
				// words: good, great, bad, horrible/terrible
				// scale from -1 to 1
				// negative number means negative valence.
			},
			arrousal: { // TODO: Google and see if diofferent from stimulation

			},
		}
	},
	stimulation: { //sensoryStimulation:, sensation:, //stimulating. arousal. It can be stimulating without necessarily being positive or negative.
		//encouragement of something to make it develop or become more active.
		// arousing
		//raising of levels of physiological or nervous activity in the body or any biological system.
		//arousing interest, enthusiasm, or excitement.
		has: {
			intensity: 1,
		}

		//types:
		//	vision
		//	sound
		//	touch


		// visually arousing stimuli:
		//	High contrast
		//	intense color

		//stimulating patterns:
		//	higher patternness.
		// 	intricacy and details



		//	changes
		//		highly constrasting changes.
		//		fast movement
		//		switching perspectives
		//		switching stuff
		// diversity in color, value, saturation, etc.
		// diversity in shape language.
		// bright, then dark, then bright
		// too much stimulation is dangerous and can give seizures.
		// be careful to find a balance in stimulation.
		// big changes in the world. things that appears and changes the landscape. (interesting for game-play)
		// people enjoy a world that have big changes. Like oh here comes a big entity or oh here is the world turns upside down

		//narrative is like the stimulation of interesting meeting of concepts. story
		//experience is like the stimulation of meeting sensory instances. stimulation
	},

	attention: {

	},

	stimuli: { //stimulus:
		//unit of stimulation
	},
	supernormalStimulus: {
		// highlight are often supernormalstimuli

		// An exaggerated imitation that elicits a stronger response than the real thing. 
		// • A supernormal stimulus is a variation of a familiar stimulus that elicits a response stronger than the stimulus for which it evolved. 
		// • For example, female cuckoos sneak into the nests of other birds to lay their eggs. Because the cuckoo egg is typically larger and brighter than other eggs, the nest’s owner gives it preferential attention. The size and brightness of the egg are supernormal stimuli to the unwitting adoptive mother. 
		// • Supernormal stimuli dramatically influence the way people respond to brands, products, and services. 
		// • Consider supernormal stimuli to increase attention and interest in logos, brands, products, and advertising. Explore stimuli involved in well-established biases and preferences for greatest effect. 
		// See Also Baby-Face Bias • Gloss Bias • Waist-to-Hip Ratio

		// Exaggerationsfaces—grab￼
		// of things we have evolved to like—e.g., attractive features, fat and sugar, baby
		// our attention. 
	},
	// valence arousal diagram... 

	brainChemicals: {
		// TODO: fill in (find documents where I describe these...)
		oxytocin: 1,
		dopamine: 1,
		nerophrenine: 1,
		adrenaline: 1,
		noradrenaline: 1,
		dmt: 1, //lol
		whatever: 1,
	},


	flow: {
		//	A state of immersion so intense that awareness of the real world is lost.•When people are not challenged,
		//	they become bored.When they are challenged too much,
		//	they become
		//	frustrated.Flow occurs when people are challenged at or near their maximum skill level.•People in a state of flow lose track of time and experience feelings of joy and satisfaction.•Tasks that create flow experiences have achievable goals,
		//	require continuous engagement,
		//	provide clear
		//	and immediate feedback,
		//	and are able to maintain a balance between difficulty and skill level.•Incorporate elements of flow in activities that seek to engage the attention of people over time— e.g.,
		//	instruction,
		//	games,
		//	and music.Designing tasks to achieve flow is more art than science;therefore,
		//	leave
		//	ample time
		//	for experimentation and tuning.
		//	See Also Control• Gamification• Performance Load Progressive Disclosure• Zeigarnik Effect

		//Flow is attained when high difficulty matches high skill level.
	},
	immersion: {},

	indexOfEmotionalStates: {
		// Emotional state
		// Human emotions
		anger: { // angry:
			is: {
				emotion: 1,
				// negative valence
				// high arousal
			},
		},
		fear: { // scared:
			is: {
				emotion: 1,
				// negative valence
				// high arousal
			},
		},
		joy: { // happy: happiness:
			// happniness?
			is: {
				emotion: 1,
				// exhilerating
				// positive valence
				// high arousal
			},
		},
		sorrow: { // sad: //sadness
			is: {
				emotion: 1,
				// negative valence
				// low arousal
			},
			associations: {
				// vacuum, hollow space
				// hold back
				// loss
				// broken pieces
				// irreversible, not restorable
				// uncontrollable, vibrating
				// heavy
				// downward motion
				// wallowing
				// emptying you emotionally
				// crouched hands over face
				//
				// unfulfillled potential
				//	a future/reality that never went to exist
			}
		},


		awe: {
			// disruption of fundamental axioms is the mother of both awe and terror
		},
		terror: {
			// disruption of fundamental axioms is the mother of both awe and terror	
		},

	},
	tenderness: {
		is: {
			emotion: 1,
			// positive valence
			// low arousal
		},
	},


	// emotional state
	astonishment: { //
		// extremely surprising or impressive.  (memorable)
		// highly arousing 

		// stunning, you become like a stone.
		// you no longer act, it is more than what you can deal with or make use of.
		// awesome
		// astonishment and awe seem pretty similar

		// it is different from simple arousal or stimulation in that it unexpected.
		// whereas something can continually be highly stimulating. (like a cacophony)
		//	which if you stay becomes less arousing and less surprising over time
		//	or if it is too much too handle in a negative way, it does not stun you but rather triggers fear or disgust, making you fight or flee.
	},
	frustration: {
		// close to anger
		// when things are not going as expected.
		// when your mythology does not work anymore.
	},

	// physical state
	tiredness: {

	},



	//human differences
	//(personality) (unique life experiences/background)


	//preferences:



	// ART BEAUTY AND CATERING THE HUMAN CONDITION

	// 6. Player and audience modeling and human nature

	// Needs drives and satisfaction
	humanSatisfaction: {
		// Player and human experience
		//    around the game
		//        is the game interesting to watch?
		//        is the game easy to backseat game?
		//        Does the game sound in ways that sparks people's interests?
		//        Easy the game easy to join in on? (locally)
		//        Does the game make you proud of playing it?
		//        negative
		//            does the game make you hide it from others?
		//            or makes you feel ashamed of playing it?
		//    How to make good games that does not frustrate people
		//        the most frustrating thing with games for a non gamer (and me) is 
		//            when you have an idea
		//                but it does not work simply because no developer made sure that would have an effect.
		//            also dying and dying again and again is frustrating because it is like the developer tells you 
		//                what you are supposed to achieve but only allowing a specific path without giving instruction
		//                    on how to do it right.
		//        DnD has a fix to this quality by the mechanics always being a response by another human creativity.
		//        in games things are most often directly scripted, there is no emergence in this sense and no
		//            contextual decision-making.
	},
	playerExperience: {

	},

	playerSatisfaction: { // AKA human satisfaction + player expectations.
		// How do I know or guess how satisfied the player is?
		MeausableIndicators: {
			playtime: 1, //TODO: make a getPlayTime function
			engagement: 1,
			DepthOfExploration: 1, //how much of the game has been experienced.
			// player needs are identified and met.
			computerPerformance: 1,
		},
		does: {
			meetingPositivePlayerExpectations: 1,
			causingFlow: 1,
		},
		is: {
			wellPaced: 1,
			fair: 1, // is fair or has fairness? "is fair" implies it does not contain unfairness?
			// beautiful: 1, //is beautiful vs has beauty. what is the difference? 
		},
		has: {
			beauty: 1,
		},
		// how do you fulfilll human need by using art(beauty) and video games?  
		avoid: {
			// Bad game //add the negatives of these as the definition for a good game instead?
			//	Bad pacing Bad timing
			//	Unfair
			//	ugly/repulsive
			//	(whether flow or not)
			//		Overwhelming?
			//		Underwhelming?
			//player feeling frustrated. (unless it is part of the game experience aesthetic or part of a learning experience.)
		},
	},
	playerExpectation: {
		// Human expectations and conventions model:
		//		"if a exists then I assume b should exists because I am a gamer."
		// Divide into classes... like fundamental expectations, which when broken makes the player quit. 

		// subverting expectations positively creates positive experiences. living up to anticipation does too.
		// subverting expectations positively creates positive experiences
		// or living up to anticipations.
	},
	meaningfulPlayerExperience: { //meaningful and relevant experiences to the player:
		// Knowing and choosing what is worth presenting to the player right now.
		// Look at 
		// You only need to present what they have not already grokked.
		// But also a library of contexts to learn in and a library of predefined teachings.
		condition: {
			// The player carries the experience in memory over time
			// The player reflects on the experience. (especially abstracting it and turning it into wisdom, intuition and virtue)
			// The player grokks the experience that they can take with them. Have a useful mental model or simulacrum.
			// The player finds it useful and applicable in real life situations.
			// Player states it was impacted by the experience.
		},
		comesFrom: {
			empathy: 1, // understanding the player's condition
			adaptation: 1, //adaptation to the player's situation, development and needs....
			storytelling: 1,
			beauty: 1,
			art: 1,
		}
	},
	positivePlayerExperiences: {
		// "I want the player to be impressed and amazed" why? because we get impressed with either hypernormal stimuli or things that are profound/meaningful.
		//	That which is profound and meaningful is what the player should familiarize themselves with in order to grow.
		// "I want the player to feel revitalized"
		// "I want the player to feel transformed"
	},

	preferedInputMethod: { //add to player model.
		// if a hand controller is plugged in then handcontroller should work.
	},

	addToIndividualPlayerNeeds: {
		Consistency: 1,
		playfulness: 1,
	},
	// Related to the human condition




	// Developmental/linear state

	maturation: { //attempt to make a scale of development?
	},
	maturity: {},

	// human development with age look into
	// developmentalPsychology



	// look for extraordinary traits, higher or lower than usual. find the player's strengths and weaknesses.






	playerAgency: { //add to player needs
		// The player should be in control over their situation
		// They should know the difference between the realm of the uncontrollable and the controllable
		// Otherwise the game feels unfair, too unpredictable,
		// The player must be informed of information critical to progress
		// The player input must be deterministic and understandable by the player.
		// If controls change temporarily, this must be visibly presented to the player, or tutorialized with the predictable context where it applies
		// Unforgiveness and inability to reverse tiny errors
	},

	// human specifics and capacities
	reactionTime: { //responsetime
		//measurable delay between stimulus and action
	},
	intelligence: {
		// inter-legere = the ability or action of reading between concepts/simulacra
		// the process of contextualizing, relating and comparing concepts

		// see commonalities and differences according to some contextual relevancy.

		// breaking, grouping, analyzing, etc

		// intelligence abstracts.
		//		it draws partials, properties, details out of wholes. deconstruct or focus on parts.
		//			in order to compare
	},
	intuition: {
		//knowing without reasoning.
		//	pre-compiled knowledge?
		// understanding without deconstruction.
		//	effectiveness through use of hunches/heuristics/feelings
		//lateral thinking?	

	},


	personality: { // The patterns in which an agent acts. The style and methods of being and doing that are not dependent on external physical properties.

		personalityTestModelDataStuff: { // personality and player type models.
			// personalitytypes personality dimensions
			theBigFive: {
				opennessToExperience: { // novelty
					traditionalismVsLiberalism: 1,
					peopleAndThingsVsIntellect: 1,
					factOrientationVsImagination: 1, // kinda correlation with preferring realism vs fantasy
					practicalInterestVsArtisticInterest: 1, // productivity, "what is its function?" vs "
					emotionality: 1, // unemotionalityVsEmotionality: 1, //preference or dispreference for emotional content
					desireForRoutineVsAdventurousness: 1,
				},
				Conscientiousness: { //chllenge
					unSelfEfficacyVsSelfEfficacy: 1,
					disorganizationVsOrderliness: 1,
					resistanceVsDutifulness: 1,
					contentmentVsAchievementStriving: 1,
					procrastinationVsSelfDiscipline: 1,
					impulsivenessVsCautiousness: 1,
				},
				Extraversion: { //stimulation
					reservednessVsFriendliness: 1,
					nonGregariousnessVsGregariousness: 1,
					receptivenessVsAssertiveness: 1,
					//submissive traits - (add seedling) vs assertive?
					//	tell me what to do
					//	tell me who is most in control. who is authority?
					activityLevel: 1,
					excitementAversionVsExcitementSeeking: 1,
					inexpressivenessVsCheerfulness: 1,
				},
				Agreeableness: { //Harmony
					skepticismVsTrust: 1,
					guardednessVsStraightforwardness: 1,
					altruism: 1,
					competitionVsAccommodation: 1,
					immodestyVsModesty: 1,
					indifferenceVsSympathy: 1,
				},
				Neuroticism: {
					fearlessnessVsAnxiety: 1,
					calmVsAngryHostility: 1,
					resilienceVsDepression: 1,
					selfConsciousness: 1,
					temperatenessVsImmoderation: 1,
					poiseVsVulnerability: 1,
				}
			},
			divided: { //people are normally distributed in their personal interest. The edges are more niche (which means they care a lot) 
				// .0 to .16 is bout 5%, very invested/caring
				// .16 to .32 is bout 15%, invested/caring
				// .32 to .64 is bout 60%, meh
				// .64 to 84 is bout 60%, invested/caring
				// .84 to 100 is bout 5%, very invested/caring
			},
			middleOut: {
				center: "does it have this feature in one way or another",
				between: "is this feature good/decent?",
				edge: "is this feature/facet among the best in the world?"
				// further out is generally harder and more costly to achieve.
				// and creating the multiplicity (filling area) is probably also harder as you must weight mechanics against each other to work together.
			},
			// 24 facets is apparently way too much.
			// they instead  broke it into 2 for each meaningful category
			TheMeaningfulFacets: { //big five based on some talk on player motivation
				// what people look for when buying games
				// but people's motivation changes over time
				// This preference map predicts player's taste.
				// taste means less over time
				//
				"fantasy-realism": 1,
				"build-explore": 1, // creation(creativity) //exploration
				// space
				"skill-accessibility": 1, //challenge //accessibility and UX
				"work-effortlessness": 1, //
				// space
				"coop-competition": 1,
				"mechanics-context": 1, //(context, the characters, the story) //system vs narrative
				// does the mechanics generate stories?
				// space
				"multiplayer-solo": 1, //multiplayer capacities // single player capacities
				"calm-thrill": 1, //calming //thrilling (excitement)
			},
			playersJourney: {
				// the further down the players journey, the less taste matters.
				discovery: "I've heard of it.",
				evaluation: "I've tried",
				use: "I've played it",
				affinity: " I am an x player (identity)",
			},
			//SDT
			selfDeterminationTheory: {
				// there are 3 satisfactions that are universal to human beings.
				// pens PENS, player experience of need satisfaction
				competence: {
					// control outcomes and experience mastery 
					// "easy to learn hard to master"
					// after playing I still know the skills.
					// example survival, crafting
				},
				Autonomy: {
					// confirming your existence, the trails you do as an independent system.
					// "choice, customization & agency"
					// after playing the world is changed the same way I remember
					// I remember or access what I created (artifacts, art, etc)
					// when asked "is the time of your life well spent?
					// people that answer yes tend to have high autonomy
				},
				relatedness: {
					// Purpose and place in life. knowing where you fit in rank. who you are related to the world.
					// "social grouping, status feedback systems"
					// shared experience, public canvases.
				},
				intrinsicVsExtrinsic: {}
			},
			extrinsicMotivation: {
				// extrinsic - choice as a means to an end
			},
			intrinsicMotivation: {
				// intrinsic - choice for the sake of choice (sake of multitude)
			},

			spiralDynamaicsdevelopmentalstages: {
				//can you make code/program that goes through all stages of development?
			},
			//psychological developmental stages? 
			stagesOfPsychosocialDevelopment: {
				// Infancy - Trust vs Mistrust
				// Early childhood - Autonomy vs Shame/doubt
				// Preschool - initiative vs GUIlt
				// school age - industry vs inferiority
				// adolescence - identity vs role confusion
				// youngAdulthood - intimacy vs isolation
				// middleAdultHood - generativity vs stagnation
				// maturity - ego integrity vs despair
				// https://en.wikipedia.org/wiki/Erikson%27s_stages_of_psychosocial_development
			},
			// Drives
			// certain drives apply to certain people.
			// it is subjective how intensely each drive is.

			// drive and bias are pretty much the same....

			// A bias is maybe a bit more context specific.
			// A drive feels more essential and underlying. More strong.
			// Whereas a bias is just kinda quirk or unconscious tendency.				

			// Drives are used to cross initial intrigue/beginning of the game
			// to the point where they have good memories and a story to tell about the game experience. (which determines if you will buy the next game or trust the brand and what not.)
			// if the core mechanic creates drive then it is a good mechanic.
			// drives kill boredom
			// when we are not bored we are in flow
			// addiction, sex, drugs, bloodlust, 
			// Freud's id
			drives: { //these are garbage.... lol
				curiosity: 1, //"curiosity is about the unseen"//potential opportunity or danger.// the player generates hypotheses of what can fill the unfinished pattern.//what is behind the wall? What is gonna happen next?
				bloodlust: 1,
				hunger: 1,
				sex: 1,
				addiction: 1,
				learning: 1,
				socialContact: 1,
				vengeance: 1,
				saving: 1,
				OthersDrives: {
					awe: 1,
					prediction: 1, //filling in patterns trying to understand a system. skinner box randomness.
				},
				octalysis: {
					MeaningAndCalling: 1, //beyond the game? (to change the world) In the narrative?
					// legends are good callings... triforce, lugia, 
					DevelopmentAndAccomplishment: 1, //proof of skill level, indicators of progress.
					EmpoweredCreativityAndFeedback: 1, //see the result/expression of their unique and creative input
					OwnershipAndPossesion: 1, //Governance, care taking, Controlling it, objects that people identify themselves through. "these things belong to me" (material attachment)
					SocialInfluenceAndRelatedness: 1, //relationship roles, mentorship, companion cooperation, competition and grouping (grupptillhörighet.) 
					ScarcityAndImpatience: 1, //Gaining exclusive access to uniques/rares or high values. "There's specific cool things but you do not have them now."
					UnpredictabilityAndCuriosity: 1, //Dealing with unknown and unpredictable situations.
					LossAndAvoidance: 1, //Be freakin careful. pay attention. stakes are high. 
					leftBrain: { //logic, analytical thought, and ownership. Motivated by extrinsic sources (receiving from the world). After obtaining/mastering interest is lost.
						// extrinsic motivations impairs intrinsic motivation.
						accomplishment: 1,
						possesion: 1,
						ScarcityAndImpatience: 1,
					},
					rightBrain: { //Intrinsic motivation. For the experience. Being. Playing. Trying out new things. Experiment. Novelty, Diversity. Do not need a goal or reward. what you continually do naturally.
						// finding inner truth. Oneness. setting your own goals.
						// The two necessary elements for intrinsic motivation are self-determination and an increase in perceived competence.
						// What you do dictated from within. Flow and genius.
						// resistance to reaction to external circumstances.
						EmpoweredCreativityAndFeedback: 1, //expressing what is within
						SocialInfluenceAndRelatedness: 1, //ways of being with others
						UnpredictabilityAndCuriosity: 1, //novelty
					},
					whiteHatDrives: { //empowering
						MeaningAndCalling: 1,
						DevelopmentAndAccomplishment: 1,
						EmpoweredCreativityAndFeedback: 1,
					},
					blackHatDrives: { //depriving
						ScarcityAndImpatience: 1,
						UnpredictabilityAndCuriosity: 1,
						LossAndAvoidance: 1,
					}
				},
			},
			SixteenBasicDesiresTheory: {
				acceptance: 1,
				curiosity: 1,
				eating: 1,
				family: 1,
				honor: 1,
				idealism: 1,
				independence: 1,
				order: 1,
				physicalActivity: 1,
				power: 1,
				romance: 1,
				saving: 1,
				socialContact: 1,
				SocialStatus: 1,
				Tranquility: 1,
				Vengeance: 1,
			},
			//mysteries?
			// the need for novelty. change. intellectual stimulation. Something important/interesting to deal with.
			// TODO:, list biases, fill in brain chemicals
			// tend and befriend

			// tend and befriend
			// women care for other life so they are more cautious and care more for surrounding yourself with people that protect you
			// but also men because it is more optimal strategy.
			// therefore it is more optimal to take care of others and befriend allies in order to hold life together, rather than go out and kill the tiger.
			// women has more oxytocin understress rather than adrenaline.

			// some people want game as an distraction
			//	for those people, features that waste time are great.

			// disc explanation? agreeableness
		},
		introversion: {
			// small gestures, hiding self
		},
		extroversion: {
			// big gestures, exposing visibly
		},

		ambiguityVsClarityPreference: { //ambivalence. related to symbolism, to trait openness.
			// some people like ambiguity, some people like clarity of boundaries.
			// some people like shapes that looks like many different things and some like things to be clearly only one thing.

			// need for openness ambivalence, room for interpretation.
			// need for clarity, certainty, definitiveness.
		}, //TODO: add to personality

		friendliness: { //friendly
			// kind and pleasant
			// adapted for and not harmful
			// collaborative and anti-competitive.
			// opposite of antagony/antagonistic/antagonism
		},
	},

	// adulthood:1,









	// Roles and action 

	femininity: {
		// scale from 0 to 1
		// also a set of characteristics both physical and behavioural that are associated with the feminine
		// what is a feminine body shape?
		//	feminine has often more body fat, thicker tights, water drop shape, narrower shoulders, wider hips.
		//		less sharp angles, more round shapes. generally more slick shapes.
	},
	// things to add to the algorithm
	// girliness
	// femininity
	// womanhood
	// the relationship between gender and body shapes?
	// factors: //?
	//	muscularity
	//	fatness
	//	big bones
	//	thick skinned
	masculinity: {
		// scale from 0 to 1
		// also a set of characteristics both physical and behavioural that are associated with the masculine
		// what is a masculine body shape?
		//	More muscular, more square, more muscular shapes
		//	bigger upper body.
		//	squary eye sockets and jaw
	},
	feminine: {
		// soft round, emotional, blush, sad
	},
	masculine: {
		// blocky, muscular, robust, non-emotional, dramatic,
	},





	// There are social/behavioural aspects of masculinity and femininity
	//	 there are a biological.


	// Taste/identity/belonging
	senseOfFashion: {
		// what is the character's sense of fashion?
		// what do they value? Aesthetic wise and function wise.
	},
	culturalColorPreference: {
		// Person/character color preference by origin:
		// People close to equator likes strong intense contrasting colors/pigments
		// Countries closer to the poles like more muted colors, grayscales and black and white.
		// Color can signify the environment the character comes from.
		// Color tells a story.
	},



	// Cultural thing add to style or something
	fashionability: { //what makes something fashion or fahsionable?
	},



	experiencePleasantness: { //maybe has better name?
		// human limitation and exhaustion.
		// doing things in excess tires people out...
		// not needlessly exhausting
		// not needlessly understimulating.
		// not needlessly unforgiving. .. 
	},
	// Limited human visual attention. Overwhelming visuals. How to highlight.






	cooperation: { //teamwork multiplayer
		// Scarcity and challenge drives cooperation.
		// Minecraft som är krångligt

		// Springa 6 kilometer. tog timmar irl
		// Ha tur vart man hamnar när det är natt.
		// Jobbigt att få resurser.

		// Vanliga minecraft är mer slot machine grinding.
		//	Everything is instantly accessible for the individual
		//	Which makes it you might as well progress on your own.

		// Dificulty gör human interaction and cooperation more motivated.

		// Scarcity

		// It is a choice of what you get good at. which leads to specialization.
		//	And you need specialized tools for each thing.

		// Because cooperation is needed to make progression less boring.
	},



	maleAndFemaleVideoGamePreferences: {
		// Differences in male and female preferences in games according to the book of lenses

		// 5 Experiences attractive for males (Like to See in Games)

		// 1. Mastery for mastery's sake. Males enjoy mastering things. It doesn’t have to be something important or useful — it only has to be challenging. Females tend to be more interested in mastery when it has a meaningful purpose.
		// 2. Competition. Males really enjoy competing against others to prove that they are the best. For females, the bad feelings that can come from losing the game (or causing another player to lose) often outweigh the positive feelings that come from winning.
		// 3. Destruction. Males like destroying things. A lot. Often, when young boys play with blocks, the most exciting part for them is not the building, but knocking down the tower once it is built. Videogames are a natural fit for this kind of gameplay, allowing for virtual destruction of a magnitude far greater than would be possible in the real world.
		// 4. Spatial Puzzles. Studies have shown that males generally have stronger skills of spatial reasoning than females, and most people would agree that this matches anecdotal evidence. Accordingly, puzzles that involve navigating 3D spaces are often quite intriguing to males, while they can sometimes prove frustrating for females.
		// 5. Trial and Error. Women often joke that men hate reading directions, and there is some truth to that. Males tend to have a preference for learning things through trial and error. In a sense, this makes it easy to design interfaces for them, since they actually sometimes prefer an interface that requires some experimentation to understand, which ties into the pleasure of mastery.

		// Five Things Females Like to See in Games

		// Females want experiences where they can make emotional and social discoveries that they can apply to their own lives. – Heidi Dangelmeier

		// 1. Emotion. Females like experiences that explore the richness of human emotion. For males, emotion is an interesting component of an experience, but seldom an end in itself. A somewhat crass but telling example of this contrast can be found at the ends of the “romantic relationship media ” spectrum. At one end are romance novels (one-third of all fiction books sold are romance novels), which focus primarily on the emotional aspects of romantic relationships, and are purchased almost exclusively by women. At the other end of the spectrum is pornography, which focuses primarily on the physical aspects of romantic relationships, and is purchased almost exclusively by men.
		// 2. Real World. Females tend to prefer entertainment that connects meaningfully to the real world. If you watch young girls and young boys play, girls will more frequently play games that are strongly connected to the real world (playing “house, ” pretending to be a veterinarian, playing dress up, etc.), whereas boys will more frequently take on the role of fantasy characters. One of the all-time best-selling computer game titles for girls was Barbie Fashion Designer, which lets girls design, print, and sew custom clothes for their real-world Barbie dolls. Compare this to Barbie as Rapunzel, an adventure game in a fantasy setting. Although it featured the same character (Barbie), it did not have a real-world component, and was not nearly as popular. This trend continues through adulthood — when things are connected to the real world in a meaningful way, women become more interested. Sometimes this is through the content (the Sims games, for example, have more female players than male, and their content is a simulation of the day-to-day life of ordinary people), and sometimes it is through the social aspects of the games. Playing with virtual players is “just pretend, ” but playing with real players can build real relationships.
		// 3. Nurturing. Females enjoy nurturing. Girls enjoy taking care of baby dolls, toy pets, and children younger than themselves. It is not uncommon to see girls sacrifice a winning position in a competitive game to help a weaker player, partly because the relationships and feelings of the players are more important than the game, but partly out of the joy of nurturing. In the development of Toontown Online, a “healing” game mechanic was required for the combat system. We observed that healing other players was very appealing to girls and women we discussed the game with, and it was important to us that this game work equally well for males and females, so we made a bold decision. In most role-playing games, players mostly heal themselves, but have the option of healing others. In Toontown, you cannot heal yourself — only others. This increases the value of a player with healing skill and encourages nurturing play. A player who wants to can make healing their primary activity in Toontown.
		// 4. Dialog and Verbal Puzzles. It is often said that what females lack in spatial skills they make up for in increased verbal skills. Women purchase many more books than men do, and the audience for crossword puzzles is mostly female. Very few modern videogames do much very interesting or meaningful with dialog or verbal puzzles at this point in time, and this may be an untapped opportunity.
		// 5. Learning by Example. Just as males tend to eschew instructions, favoring a trial-and-error approach, females tend to prefer learning by example. They have a strong appreciation for clear tutorials that lead you carefully, step-by-step, so that when it is time to attempt a task, the player knows what she is supposed to do.

		// There are many other differences, of course. For example, males tend to be very
		// focused on one task at a time, whereas females can more easily work on many
		// parallel tasks, and not forget about any of them. Games that make use of this multi-
		// tasking skill (the Sims, for example) can sometimes have a stronger female appeal.

		// why video games appeal to men:
		// The introduction of affordable computers gave us a type of game that:
		// ● Had all social aspects removed
		// ● Had most verbal and emotional aspects removed
		// ● Was largely divorced from the real world
		// ● Was generally hard to learn
		// ● And offered the possibility for unlimited virtual destruction

		// boys mainly like skillfully destroying enemies.
		// girls prefer defending rather than destroying and prefer signaling and communicating in the ship.
		// mothers tend to care more about the well-faring and experience of the family rather than their own experience.
		//	doing the most boring thing that helps everyone have fun and having an overview of everyone having a good experience.
	},









	//Social behaviour

	socialStrategies: {
		// Benefiting from the sharing and receiving of information rather than physical for.
		// The agents are rearranging their own models rather than exerting forces to modify their physical environment.
		// Choosing what information to share and what information to hide and in what way to convey the information for the subjects.
		// Ways agents align goals and create organizations
		// Often related to sharing information, and interacting through information rather than physical contact
		// Conversation -  A back and forth information sharing guided by the participating agents and guiding the their attention and response in order to find essential information and experiences.
		// Cooperation.
		// Cross support.
		// Common enemies.
		// In an area of information coming back and forth between agents 
	},


}
