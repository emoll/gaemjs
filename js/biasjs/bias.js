//bias.js

// 1. Load beliefs into memory.

// (beliefs include: model of environment (computer/input devices), desires/goals, known conceptual relationships, uncertainties, methods of clarification/identification, model of simulation, model of player state )
const bias = {
	// With assumptions you can judge and verify
	//	Biases are labelled bundles of conditions.
	//	"according to my model" = According to the model's set conditions.

	// I suppose bias should be considered a model similar to the active player model and the runtime model. etc.


	// TODO: A Model for the algorithm of what it does not know that it needs to know in order to reach its goal... (best guess Where to research)

	// Chapter index:
	// 1  GOAL ACHIEVMENT
	// 3  INTERNAL LANGUAGE AND COMPREHENSION //bad category... lol
	// 4  ART BEAUTY AND CATERING THE HUMAN CONDITION
	// 5  INTERACTIVITY AND CHALLENGE
	// 6  ON SCREEN //bad category... lol
	// 7  TROPES AND CONVENTIONS //bad category... lol
	// 8  METAPHYSICS TO PHYSICS
	// 10 PHYSICAL OBJECTS AND MAN MADE THINGS
	// 11 UNORGANIZED AND LEFTOVERS

	// Sub-chapter index:
	// GOAL ACHIEVMENT
	// 1. Goals and KPIs
	// 2. Decision making paradigms
	// 3. Business design and development language
	// 4. Engineering and design methods...
	// 5. Constraints enforcement



	// INTERNAL LANGUAGE AND COMPREHENSION
	// 9. META keywords - relational, etc
	// 10. Simulation & metaphysical
	// 11. Processes, formulas and reactions
	// 12. abstractionszzz
	// 13. Scalar opposites
	// 14. Unknown, ambiguity, uncertainty

	// ART BEAUTY AND CATERING THE HUMAN CONDITION



	// 15. Player and audience modelling and human nature


	// 16. Psychology, learning and human player guiding
	// 17. Biases: heuristics of human tendencies and preferences.
	// 18. Human information processing - learning and
	// 19. Perception and discernment?
	// 20. Human emotions
	// 21. Player model.
	// 22. Human developmental psychology and teaching


	// 23. Storytelling and memory sequencing lol
	// 24. Storytelling and perception?


	// 25. Beauty art and virtue
	// 26. Architecture world building and design
	// 27. Composition and natural world building
	// 28. World building and world tropes?
	// 29. World building - common consistencies of planetary worlds
	// 30. Character design and human being traits 



	// ON SCREEN (and in ear)
	// 34. Spatial dimensions, Spatiality and measurement
	// 35. Geometry, shapes and figures


	// 36. Color, light and tones
	// 37. Styles and aesthetics
	// 38. Visual Effects VFX
	// 39. Audio and music
	// 40. Camera and views 

	// TROPES AND CONVENTIONS
	// 41. Specific characters
	// 42. Video game tropes and genres
	// 43. Common player actions
	// 44. Common video game systems
	// 45. video game genres...

	// METAPHYSICS TO PHYSICS
	// 46. Abstractions of room and empty space.
	// 47. Time related concepts
	// 48. Movement and interaction of physical forces
	// 49. Physics and forces
	// 50. Processes... transformations...
	// physical processes or actions
	// 51. Physical processes like drying and soaking
	// 52. Physical (material) properties
	// 53. Substance/material
	// 54. Power, light and Thermodynamics 



	// PHYSICAL OBJECTS AND MAN MADE THINGS
	// 59. physical properties of being and creatures
	// 60. clothing and fashion!
	// 61. Thing, Item, tools.. very object
	// 62. architecture and types of buildings 

	// UNORGANIZED AND LEFTOVERS









	// GOAL ACHIEVMENT

	// Goals and KPIs

	anotherGoalStructure: { //requirements and presuppositions for the algorithm.


		//Attempt refining the top goals/requirements: 
		//Refining the top goal:
		//	exclusively engage with
		//		audience
		//			that is 
		//				appears
		//					[to participate willfully]
		//					
		//				the audience are informed of anything they want about what the software does and what it tries to achieve.
		//				The audience is in control of their participation.
		//				has consented to the constraints and bounds of the code.
		//	Virtuously guide people to virtue




		//What is usually the goal
		//	to instantiate the running video game software
		//
		//drives:
		//	cautiousness:{
		//		// Only do things when there is high confidence.
		//		// Only get confidence with excruciating proof.
		//	},
		//	
		//	if the player seems wildly unpredictable or selfharming, quit.
		//		aka they must show willfullness to be presented with the thing,
		//			and to interact with the product.
		//	noWaste:{ // Cause no waste:
		//		Do no harm to people or equipment.
		//		cause the best possible runtime efficiency...
		//	safely and openly bring people closer to virtue. (quantify harm, improvement and delay)
		//		Try defined methods first in the cause of finding the best methods.
		//		Openly communicate what it's gonna try and why, and verify any changes to what it believes are improvements.
		//		improve safety and efficiency of methods //confidence
		//			improve modeling
		//	safely interact with human
		//	cause no harm and revert harm if possible.
	},

	seedlingAgentDesireModel: { // aka goalStructure: desire: goal: algorithmHierarchyOfNeeds: KPI: KPIS: DesireStructure:

		longtermAndAproximations: {
			// That which comes from ultimate virtue ... (Whatever is beyond virtue)

			// World virtue (virtuous behaviour.) (measure most behavior and judge if virtuous.)
			//	Requires to know what is virtuous. Which can only be done through inclusive dialogue.
			// 		Exposing people to all forms of virtue and let them make up their own mind.
			// 		congregate all perspectives of what is virtuous.

			// Aprox:
			// 	Apply the known aproximations of virtue. and listen for feedback of what details of it was good/bad.
			// 	We have definedVirtues which are pointers to what has been agreed to be virtuous.

			// Aprox:
			// 	Increase individuals virtue.

			// Aprox:
			// 	Attempt to increase players virtue aspects through their willful interaction with a customized video game.

			// Aprox:
			// 	Represent a consistent style through visual means.
		},

		// act virtuously
		// and in a virtuous manner train the user to become virtuous.
		is: {
			virtuous: 1,
		},
		does: {
			teaching: {
				subject: {
					audience: 1,
				},
				matter: {

				}

			}
		},



		// What the algo should do: (my goal is to make people virtous, what makes people virtuous? [search entries for causing virtue...] oh it is teaching according to this defintion.







		// making someone virtuous is better than providing tools of virtue

		// Increase human individual virtue



		// aid virtue or train virtue

		//there are two purposes:
		//	Tool/aid, or teacher/learning.
		//	A tool enables a subject to reach states they would be unable to without it.
		//	A teacher/trainer enables a capacity in the subject by efficiently changes the inherent structure of the subject.

		// ^ aid virtue or create virtue.

		//	A trainer does internal transformation, a tool is an external aid.


		//	improve playerstate towards virtuosity(part of which is autonomous studies)
		//		by deconstructing current state and building it into something more useful.
		//		through carefully constructed sequences of events (narratives) and meaningful player interactions

		// most details are irrelevant
		// filter the abstraction of your creation to what is contextually relevant (relevant for what the player can learn from or be impressioned in a positive manner)


		// games allows you to explore scenarios in a bounded context of rules.
		//	more accurately, consistenly and faster than you you can typically imagine alone in yoru head.
		//	
		//you should give people agency especially over ways to meet their own needs.
		//
		//make people independent.
		//
		//
		//
		//easiest morality is bounded goodness
		//	aka goodness in oslated context.






		// PRIORITY / HIERARCHY / LEVELS OF NEEDS / REQUIREMENTS / DESIRE STRUCTURE / SYSTEM REQUIREMENTS / prerequisites
		// Priority is all about identifying criticality (optimal investment/investing)
		// Optimally Invest potential energy/resources to increase human virtue

		// Like Maslow's hierarchy; Like the human body, the software ought to feel what is most important to put cognitive focus and resources on in body. (while the body is also made out of autonomous cells and structures that maintain themselves independently)

		// Figure/find/determine/feel what is worth spending resources on



		// Strive to achieve the higher (in achieved value) goals
		// Higher goals depend on satisfying underlying goals... (Maintaining the fundamental needs in the pyramid.)

		// Strive to quickly fulfilll higher goals to eliminate unnecessarily maintaining the lower needs (second order goals).
		// The goal is to effectfully and quickly reach higher goals.





		//	meeting the player at their "level" and their 


		// Respect the player's time.
		// the player should be encouraged to quit when it has nothing more to learn/benefit from.
		// "I want the player to stop Playing when the cost of teaching the anything valuable is too high."
		// "I want the player to be/feel done with the game"
		// "I want the player to take breaks when appropriate so that they take care of their human needs."









		CostsToAvoid: { // define these like the other concepts and sorta add them to the KPI or whatever
			// TODO define how to measure the costs and weight them against each other.

			// Action cost
			// unknowns.
			unpredictability: 1, // (uncertainty) of affected parameters
			uncertaintyOfMagnitude: 1, // Uncertainty of effect’s magnitude
			irreversibility: {
				// Irreversibility (lack of options/multiple outs. Reversibility is essential to remove cost if there is any risk of error involved)
				// irreversibility exists because of consistency...
			},
			interruption: {
				//Interruption of experience (partly what model of consistency deals with)
				//TODO define how interruptibility of actions are measured.
			},
			// Noticability of computational bias or bad data manipulation

			// Heuristic
			actionCount: 1, //Number of required action (cognitive load of the pathfinding or problem solving algorithm)
			//	(this can be learned by encoding expectation value based on the circumstances)

			//Non-alignment with other KPI’s for the performance of player experience.
		},


		// How the algorithm ought to behave: (?)

		// Algorithm should optimize for:
		Flexibility: {
			//- ability to change things on the fly without many negative consequences. it is about getting into variety of states without breaking.
		},
		Adaptation: {
			//. the capacity to change state in response to changes in its environment. Reading environmental states and react uniquely. "To become better suited for its environment."
		},
		Homeostasis: {
			//- Ability to maintaining integrity. Preserving goals and capacity to act.
		},



		// Simplicity with the right abstractions wins: (Aka a good heuristic)
		// As always you want the most orthogonal actuators. The fewest set of uniquely roled actuators that together can be combined to create the broadest rage of desired effects.
		// It is like a swiss army knife. you want a good selection that serves a function in many situations.

		// The algorithm should consider:
		// What do I know? (everything in beliefs) What should I know? (best ways to reach your goals)
		//	Your memories of what you know are only considered in process of reflection.
		//	You only need to reflect about what is relevant.
		// What needs to be done? What should I do now? 
		// (some sort of OODA looping or something)



		// (prioritization is not important. it is just maximize value.) //and you do that by knowing limits of what you can do.
		// Prioritize, initially measure how much do I need to spend on which things in order to get which results?
		// When game is started:
		//	How smooth does the game run? (is smoothness important for this particular game/experience?)
		//	How satisfied is the player?
		//	Check events
		// As an algorithm:
		//	What am I striving towards? (goals/kpis)
		//	How do I fulfilll my strive?
		//	How do I stay consistent? (How do I continue the user experience consistently?)
		//		What have I presented?
		//		Historical memory, in order to create consistency/presence of things.
		//		What does the player remember?
		//	How satisfied is the player?
		//	What indicates how satisfied the player is?
		//	In what ways are these not manifested yet?
		//		Aesthetics - How do you enforce particular aesthetics?
		//		Beauty - How do you enforce principles of beauty?
		//		Mechanics

		// Generate certainty:
		//	The code should test the platform it is on, (this is part of figuring out algorithmic desire.)
		//	to test the capacities of the hardware it is running on over time.
		//	in order to generate good enough heuristics in methods (specific ways) of achieving the desired goals.
		//	
		//	if you know how much computing power you have in between each frame
		//		and you have made some tests before hand on how much performance each tasks takes
		//			then you can choose the appropriate heuristics
		//				The ones that cause most overall benefit 
		//					when all concerns are taken into account. 
		//	good tests:
		//		rendering unique sprites (how many unique sprites can you render at 60 fps)
		//		how big unique sprites can you render at 60 fps

		// You only affect what is related and you only check on what is affected.

		// (design principles) quality/detail based on interest/focus (some sort of 80/20)
		//    playing the game should be a walk from abstract images to more detailed as you get closer
		//        Progressive disclosure of details in environment and user guided increase in level of detail and focus for the algorithm.
		//            Measure player engagement and what they engage with and how it arouses them or changes their behaviour.
		//            ^the image I get in my head is a sword stuck into the ground, but it looks just kinda plain
		//                kinda somewhere in-between a symbolic representation of a generic sword and a fully detailed
		//                    hd rendered sword with a detailed back-story.
		//        progressive disclosure also has this property of being able to generate challenges adaptively to the player's skill level.
		//            if the player rushes through content, give them the possibility to
		//                go to a harder area of the same challenge
		//                or shift focused challenge slightly
		//                or add a "parallel" challenge manage/juggle

		// I want the algorithm to create a data node network in memory like "A:{is: {B} }" makes like a database entry a|is|b
		//	so as to allow to search for every a and every b and figure out their relation.
	},
	attention: {

		// The whole purpose of attention is efficiency in meeting the needs of underlying drives.

		// Attention is a computation that selects computations to do what best satisfy the current needs presented by ... analyzing? the underlying drives.
		// (use knowledge to cause action that met "needs" of underlying drives)

		// Attention = attending-ness

		does: {
			// 1. Determine what action best satisfies needs going forward.
			//		needs list of possible action associated with truthful data about how well it can satisfy needs, indirectly and directly.
			//			When sophisticated enough it develops the (heuristical) ability to get/consider only relevant options for a task context. 
			// 2. do it. actuate it.			
		},


		// Background:
		// in humans that is adhering to the underlying laws of physics in a manner that sustains the structure of the being/organisation.
		// Computers have limited temporal computational capacity (computation per time unit)
		// forces selection of computation that sustaions the being.
		// This is what forces it to develop smart tricks and optimizations, through caching simulacra.
		// ranking(priori), caching and compression.
		// It optimizes sufficient structure and maintenance of parameters to sustain itself over time.

		is: {
			// Selective allocation of computational resources (for various cognitive processes ("trains of thought"))
			// Active filtering of access to information over time.
			// Which things are allowed to be thought on together to create new insight.
		},

		// To pay attention is to be homing in a space of differences
		//	you are tracking a particular. Locking in on it. Holding/maintaining the structure of a percept.
		//
		//and this tracking is the same as teh concept of ab-straction, abstrahere
		//	drawing out, honing in, on a detail from a bigger something.

		// Optimizes value capture. (which can also be in the form of risk avoidance)

		has: { //memory
			selection: 1, //considers a (limited or not) set of resources and ranks (roughly or not) their contextual value. 
			allocation: 1,
			//requires the autobiographical tracking of context.
		},
		does: {
			selection: 1,
			allocation: 1,
		},
		// it allows to consider many aspects with high potential of a limited set of resources.

		// To figure the best way to "capture" value. 
		// (basically you are constantly in the position of being able to optimize any aspect of anything, but attention makes you do what you need, optimized.)
		// And save it for future reference in proportion to how valuable the action was and how likely it is to be encountered again.


		// underlying needs:
		//	security - least sloppy
		//	efficiency - most efficient
		//		avoid hyperfocusing on neglible optimizations. (optimization for optimizations sake...)

		// Generally how it manifests:
		// The goal: to reach the goal more efficiently and less sloppy next time.
		//	through the memorisation of implementing last time's experience.
		//	and through simulating practice in our minds. partial practice.
		//
		// pay attention = look out for what is worth considering in order to cause this exact effect but better (more effect or more efficient).
		//		choosing a limited set of parameters with 80 20 importance of effect and adjust them carefully.


		// reason for efficiency
		//	you want to increase the speed of implementation so that you can be one step ahead as soon as possible.
		//		and have time to do other more profound things.
		//	
		//one of the best things you can do for learning is take more time doing what you are doing
		//	so that you reflect deeply of every next step and can hone in it perfectly.

		// a side effect of attention is inattention blindness: (as you focus on limited aspects you down regulate ability to notice peculiarities in the field of perception.)
		//	these stimuli are filtered away due to deep allocation of focus.
		//	resulting in failing to see unexpected but salient objects or stimuli


		//_________________
		// Background: (pre requisites of attention model)
		// There are underlying needs. statistical flukes from environment that gives the system longevity.
		// The drives are adaptations that tilts the gradients based on these underlying needs.
		// This is macro prioritization of an organism.

		// We pay attention to concerns that are relevant for rebalancing, maintaining balance, or avoiding potential debalancing of our needs.
		// We especially pay attention to the most urgent*important of these.



		// Emergent properties of attention:
		//	"is it really as bad as the simulacrum tells me? truthfulness algorithm. unpack situation. What does the gradient landscape look like if I look for key factors."
		// 
		// Dynamic: The more unknown the situation the more reliance on pure perception (presence, awareness) rather than attention.
		// Perception is ultimately how we find any tilts in the gradient landscape.
		// The more known, the more confidence, the more habitually it is dealt with, the less attention neeeds to be paid in general. The more the attention can be paid or devoted to something new.
		// In between is the tuning process which requires attention.

		// practiced/learned things comes with higher confidence and require less attention to be done. (It can be done mainly with simulacrum)
		// habitual is the parts of execution that are isolated from attention. the ones that are not 80 20 important for improving the overall execution to met current needs.

		// boredom comes from the opportunism in the state of having excessively met needs. You've sufficiently met needs and gained resources in every way you know, so the best you can do now is non-directed or non-instrumental play. Learn something new, search less important aspects of memory or surroundings for something interesting model better.
		// boredom is the drive to find something to pay attention to, possibly to maintain the fitness of the attention system.
		//		as you get bored things remind you of long thoughts and projects which you can pickup and maybe progress on.


		// Attention is directed by underlying drives, to invest the attention in learning how to adapt in the context to match the needs and desires that comes as drives (urges?)
		//	by paying attention to contextually relevant ques.





		//attention looks for novel, unknow or disruptive factors.






		// attention is the filter of contextual relevance
		// what is contextuall relevant are the steep gradients associated with the context. The strong affordances related to the relevancy of the filter.

		// You pay attention to learn.
		does: {
			// learn
			//	improves models and methods for the future.
			//	improve ability to improve models and methods for tho future.
			//It learns:
			//	what is truthful and accurate
			//	what is efficient and useful (optimization and casue-effect)
			//determine what to compare, what sensors to read?
			//"conducts" transformation of state of what is observed through the models.
			//	in order to match the needs and desires that comes from drives.

			// Match the needs and desires that comes as drives (urges?)
			//	by conducting transformation of state

			tracks: { // actively listens to signal of updates models of
				physicalSpace: 1,
				socialSituation: 1,
				ComputerState: 1,
			},
			// refers to and builds upon the model of all known things (the knowledge)
			// filters inconsistencies, harmonizes perception and action.
			// enables the results only possible from being concerned with one thing at a time.
			//	through efficiency, through, conceptual isolation (abstraction)
			// thus chooses what to procrastinate on in order to best travel towards goal.
			// stops the perception of irrelevant percepts/stimuli
		},
		has: {
			//action historical memory, memory of what it has done.
			// aka can: traverse historical events and find them through contextual proximity
		}
		//you need a model of truth and accuracy to be able to stay true and accurate.

		//agents need to achieve sufficient wisdom to judge themselves whether the simulacra or known recipes are sufficient for the desire, (and wether the desire is sufficient lol)
	},


	control: {
		// Any control system requires truthfulness for what it regulates
		// truthful schemas (or rather the truth) is needed to control a system
		//control require truthful schemas
		//
		//A truthful schema of the physical is necessary for tracking and controlling the body.
		//	it also lets you model and predict others bodies
		//The same is true with attention, in order to track and control it and understand others attention.
		//
		//
		//The brain strategically and selectively controls attention, moving between points in a high dimensional parameter space.
		//
		//selective attentions dips us on specific experiences. (states) among uncountable parameters.
		//
		//the cerebral cortex enhances one set of signals and suppresses competing signals.
		//	aka engages in abstraction.
		//	
		//
		//
		//The selective attention schema enables effective control of attention
		//	consists of information that
		//		describes attention
		//		represents the (complex) state the person is in
		//		predicts the effects attention might have on their thoughts and behaviors.
		//		
		//		
		//the model/schema/(conceptual simulation) is necessary for physical(non-virtual) control that is good.
		//(this is a general principle of control engineering (look up))
	},

	drives: {
		// underlying drives:
		//	agency (ability to follow internal drives)
		//		safety - avoiding damage and changes
		//		effectiveness - causing desired effects/changes
		//			efficiency
	},
	//presumptions and assumptions of running software
	confidence: { //The algorithm needs a confidence algorithm to know what is right.
		// confidence has to do with validation (truthfulness truth)

		//confidence algorithm:
		//	// what does the algorithm do with 100% confidence?
		//	// How does the algorithm build confidence?
		//	cautious testing
		//	the definitions of what to execute is made by some user and may not be completely accurate.
		//	and thus should do everything with caution and verify it before scaling up.
		//	
		//	//verify through multiple means. critical thinking...
		//	
		//	At any time what it think it does may not be what it actually does. (self reflection?)
		//	user feedback is more valid than internal models.
		//	But certain things are more valid than user feedback...
		//		The safety of the user.
		//
		//Manually hard code methods to verify things outside the simulation.
		//	What indicates the state of parts of the real world and by how much?
		//
		//Examples of items confidence algorithm should go through at startup
		//	presumptions of a running software:
		//		This code is running on a computer: 100%
		//		
		//	assumptions of a running software: (to be tested)
		//		There is a user/player that decided to initiate this code: 95%
		//		There is a user that can and will give input to this computer: 92%
		//		This computer can communicate and present ideas through function calls to output channels.
		//		doing specific output activities affects the user in the defines sensory ways. (easily testable with a responsive user)
	},

	learning: {
		// The adaptive updating of models to a selected recognizable context, in order to cause contextual prediction and control (actable options with, to some degree, known outcomes) 
	},

	// drives tells us what is contextually relevant.
	reasoning: {
		// a kind of learning
		// does not need to wait for (physical) environment to supply results for the changes you are making.
		// playing out previously unseen changes of models based on the symmetries in the knowledge
		// somehow needed to infer non-obvious things...?

		//reasoning is the ability to compare and combine simulacra
		//	(so, it's what intelligence does? inter-legere, read between parts)
	},
	simulacrum: {
		// simulacrum is mapped data. meta-data. cached data. compiled and optimized code.
		//		you know instantly, the meaning of something in a context
		//			because you've done the math. You've done the transformations and saved the answer.
		//	examples: using a lookup table instead of doing the math.
		//	a simulacrum represents something more complex than it contains.
		//		basically a symbol, to have bigger meaning in an interpretation.

		// a simulacrum is only accurate if the underlying models are still accurate.
	},

	goal: { // a constraint to be fulfillled/realized over time
		has: {
			desireState: 1,
			priorities: 1, //or hierarchy...
		}
	},


	// Goals and ideals
	// product ideals
	accessibility: { // principles of accessibility for the player? //NOT TO BE CONFUSED WITH ACCESS. like  having access to this and that thing or place
		// tutorials, good understanding, pedagogy
		//	look into 125 design principles and the like organization of information and good learning and patterns and stuff.
		// "it should be easy for the player to place furniture on the inside of their house."
		// fine controls
		// contrast

		accessibility: { // the principle of benefit by generalizing and universalizing the utility of tools/designs.
			// Accessibility is a major goal of design
			// Things should be designed to be usable, without modification, by as many people as possible.
			// Modern practice accepts that most accommodations can be designed to benefit everyone.
			// Make things perceptible, usable, understandable, safe, distributed,  etc for EVERYONE.
		},

		//usability:?
		// The mom test:
		// 	"if my mom can play the gamme, anyone can."
		// 	can somebody play this without getting a manual or get any instructions?
		// 	distil the essence of what you try to do and make the ui enhance that.
	},

	// Human ideals
	wisdom: {
		// Makes you understand every context how to navigate it, what is meaningful and accurately know where every path leads before walking it
		// What is real, of substance and meaningful is what makes you virtuous and wise.
		// Wise: = high/more wisdom
		// Ability to construct, concise, predictive, useful heuristics to map and wayfind the nature o transformations and fuulfilment of needs.
		source: { //comesFrom:
			relevantExperiences: 1,
			wisdomSkills: 1, // good skills ((wayfinding), intelliengece, prediction and memory of experiences).
		},
	},
	virtue: {
		// virtues are characteristics deeply believed to promote collective and individual greatness
		// Qualities that unlock the potential of greatness in human.

		// Prudence - The mother of all virtues
		// At the right times, about the right things, towards the right people, for the right end, and in the right way, is the intermediate and best condition, and this is proper to virtue
		//perfect prudence is indistinguishable from perfect virtue
		//In this way, wisdom is the central part of virtue

		//prudence is like long term resourcefulness r something.

		//knowledge is merely correct belief that has been thought through and "tethered".
		//Mere true belief is more likely to be lost, which makes it less valuable than knowledge.

		// by thethered I believe consistently manifestable as needed. 

		//environmental virtue is the virtue achieved simply through environmental or physical condition.
		//	A man whose needs are met is simply more virtuous.

		// Virtues are balancing guiding principles of fulfillling human needs.
		// Virtues are the aesthetics and characteristics of goodness. (properly functioning people)
		// Having virtue just means doing the right thing, at the right time, in the right way, in the right amount, towards the right people.
		// For the virtuous there is no need to be specific, because if you are virtuous, you know what to do. All the time. You know how to handle yoursel f and how to get along with others. You have good judgment, you can read a room, and you know what is right and when.
		// Virtuous: = high/more virtue
		// Beauty is a virtue in itself.... Is it not?
		// A virtuous character is developed through habituation - if you do a virtuous thing over and over again, eventually it will become part of your character.
		// The way you know what to do in the first place is by finding someone who already knows and emulating them. (copy by example  )
		// And then you trial and error and then you learn.

		// Humans are inherently motivated to be virtuous.

		// You motivate people by promising eudaimonia. being the best person you can be.




		// Virtues:
		//		flourish with yourself and with others.
		//		realizing you are everyone and then care for yourself.
		//		all inclusive experience of life is both independent and one at every level.
	},
	virtuous: {
		//manifests/implements/instantiates virtue.
	},


	// Decision maker algorithm:
	//	Look at all possible actions.
	//
	// The "bias" describes conditions
	// Actions are different from bias by structurally holding preconditions and post conditions.
	//
	// Action is a non-reactive, manually engaged process
	// An action can have ranges of preconditions and ranges of post conditions
	// Some actions have dynamic input-output ratio
	//
	// Actions only exists by contract. it is actions that are bound by laws and restrictions.






	linearityOfEmergences: {
		// A root cause which all the other things emerge from.
		//	Underlying phenomena.
		//	this way it can all "make sense".
		//	Example:
		//		Lets say a root is position and that determines heat and moisture, 
		//		and heat and moisture and position determine x, y, z
		//		you get more and more things to work with but you have like napoleon structure.
		//		You have one cause that generates all parameters of the game.
		//	The same limitation applies to Biome concept which makes it unnecessary,
		//		it is just something that emerged out of weather and geographical position.
		//		^The features is what defines the biome and the features depend on the underlying properties.
	},


	// you can either support people, or build the support inside their brains through teaching/training.





	// Concepts of effectiveness/efficiency
	Satisficing: {
		// Aim to make things that are good enough (meeting constraints) rather than perfect
		// It's rewarding to surpass standards but it is more important to  meet them.

		// A problem solving strategy that seeks a satisfactory versus optimal solution. 
		// • In certain circumstances, seeking to roughly satisfy—i.e., satisfice—design requirements is more productive than seeking to optimally satisfy design requirements. 
		// • When problems are complex or time-constrained, satisficing is more productive than optimizing. 
		// • Satisficing is the basis for iterative prototyping, design thinking, and most real-world problem solving. It epitomizes the maxim: “Don’t let perfect be the enemy of good.” 
		// • Use satisficing versus optimizing when problems are complex or time-limited—in other words, for most problems most of the time. Once a problem is solved, the solution can be optimized as time permits. 
		// See Also 80/20 Rule • Cost-Benefit • Iteration • KISS Prototyping • Root Cause 

		// The legendary makeshift adapter that fit a square CO2 filter to a round receptacle on Apollo 13. NASA satisficing perfection.
	},
	practical: {
		// 	Action that is Sufficient for reaching a goal or an approximation.
		//		sufficient. (satisfactory) satisficing. (good enough)
	},
	essential: { //TODO: add to a section about requirements 
		// a scale how important or essential something is to something...

		// something that iss essential is something that gives the support that lets something be.
		// requirement for essence.
		// prerequizite.
	},
	benefit: {
		// benefit is measured in:
		//	increased reversibility
	},
	costBenefitComparison: {
		// Every decision creates a set of costs and benefits.

		// the goal is to increase the rate of increase in value.
		// benefits - costs

		// I guess this (costBenefitComparison:) is really about invenstment..

		// nature of investment:
		// tiny optimizations gets you over boiling points 
		// sometimes big investments gets you over boiling points...


		//________________________________--

		//investigating value / cost in seedling engine:
		//	error avoidance is avoidance of irreversibility.
		//	unknown costs are the  most dangerous.
		//		you must be careful and perform tests
		//		to offer small parts before trying to offer big parts.
		//		if a small trial creates costs 
		//		
		//		
		//		Trying different scales:
		//			does the super small experiment generate any cost value?
		//			
		//		
		//		does the cost/value seem to change predictably with as you change level of investment?
		//		and the costs seem to
		//	
		//	
		//	
		//
		//min maxing value maximization
		//
		//Job:
		//	determining what is uncertain. determining which experiments to apply. (scientific min maxing lol)
		//____________________________________






		// The value of a thing is a function of its costs of acquisition and use versus the benefits it provides. 
		// • Cost-benefit does not just refer to finances. It refers to all costs and benefits associated with a design, including physical costs (e.g., effort), emotional costs (e.g., frustration), and social costs (e.g., status). 
		// • If the costs associated with a design outweigh the benefits, the design is poor. If the benefits outweigh the costs, the design is good. 
		// • A common misconception is that adding features increases product value. This is only correct when the benefits provided by new features outweigh the costs of increased complexity. If they do not, adding features decreases product value. 
		// • Consider the cost-benefit principle in all aspects of design. Do not make design decisions based on costs or benefits alone. 
		// See Also 80/20 Rule • Feature Creep • IKEA Effect Performance Load • Veblen Effect 

		// The hassles (i.e., cost) of transporting a kayak are high, which reduces the popularity of kayaking. Making the kayak more portable reduces this cost, making kayaking more appealing. 
	},
	priority: { // priorities and importance // kinda "softness/hardness" or severity of constraints

		// Priority is all about criticality
		// Critical =
		//		Something that needs specific difficult treatment to produce high value
		// 		Only producing value when under specific constraints.
		// 		That which most greatly produce value 
		// 		Something like strictness required to produce high value.
		// 		That which is worth investing most (or sufficient) resources into.
		// 		(Usually you need just enough to create a tipping point or boiling point sort of phenomena.)
		// 		(Breaking inertia or stopping a run-away exponential.)

		// The formula: condition, priority, frequency, process, context
		// Examples of the formula: apples must always fall from trees.

		// Priorities are based on requirements, needs and desires.

		levels: {
			must: 1, // enforce or die
			should: 1, // enforce but do not die for it
			could: 1, // seemingly allowed possibility

			// the following can just be must! + not + X. should+not+x could+not+x... could kinda implies it can go either way (true or false).
			// wont: 1, //must not?? // but it is more like, do not spend the effort here
			// shouldnot:1,
			// couldnot1, seemingly disallowed possibility
		},
		haveTo: 1, // must... do or perish
		needTo: 1, // It (probably) improves KPIS
		wantTo: 1, // attractive to human but not


		// urgency (just time constraints... can be part of goal structure. but priority(value) together with urgency(time constraints) makes cost structure... )
		// time model - to prioritize based on urgency.
		// I need something that just stacks things to do in a todo lists and orders it by priorities and executes.
		// I need some cool examples that I can play with to build this sort of system.
		// Some experiments is... A box that falls from above the screen to below the screen.
		// Duration is very important. Like for pacing, you want things to be intense for a while.
		// You want things to be calm for a while. you want t
	},









	//customer service:ability.
	//	"target audience" is just scope limitation by customer characteristics.


	// Business design and development language
	targetAudience: { //consumer scope
		// what is in common among all people that I am able to serve the best? (compared to all the people I am less able to serve)
		// What kinds of people is this game for
		// What previous experience should they have?
		// Talk in moscows. (must have, should have, etc)
	},
	// methodology // design/development techniques
	productLifeCycle: {
		// The common stages of life for products. 
		// • There are four stages of life for products: introduction, growth, maturity, and decline. 
		// • Introduction: The official birth of a product. 
		// • Growth: A stage of rapid expansion. The focus is to scale supply and performance of the product to meet the growing demand, and provide the level of support necessary to maintain customer satisfaction. This is where most products fail. 
		// • Maturity: The peak of the product life cycle. Product sales have begun to diminish, and competition from competitors is strong. The focus is product refinement to maximize customer retention. 
		// • Decline: The end of the life cycle. Product sales continue to decline and core market share is at risk. The focus is to minimize costs and develop transition strategies to migrate customers to new products. 
		// See Also Development Cycle • Hierarchy of Needs • Iteration 

		// A product needs to evolve over the course of its life to maximize success. Failure to make the right changes at the right time can shorten its life cycle.
	},
	developmentCycle: {
		//The stages of product creation: 
		stages: {
			analysis: 1,
			design: 1,
			development: 1,
			testing: 1,
		},
		analysis: {
			// Analysis identifies and documents needs, resulting in a requirements document. Needs are discovered
			// through research, customer feedback, focus groups, and direct experience with the problem. 
		},
		design: {
			// • Design transforms the requirements into visual form, which becomes the specification. Design involves user research, ideation, prototyping, and testing. 
		},
		development: {
			// • Development is where the design specification is transformed into an actual product. 
		},
		testing: {
			// • Testing ensures that a product meets both the requirements and design specification. 
		}
		// • Establish iterative development cycles, meaning that there is back and forth between groups at each stage. Work together to modify requirements and specifications as needed. 
		// See Also Design by Committee • Hierarchy of Needs • Iteration KISS • Life Cycle • Prototyping
		//		
		// The ideal development cycle fosters iteration between adjacent stages in the cycle—and, as nonadjacent stages. 
	},
	// aka continual improvement:
	iteration: {
		// Repeating a set of design and development operations, with each repetition building on the last, until the desired result is achieved. 
		// • Complexity does not emerge without iteration. In nature, iteration allows complex structures to form by progressively building on simpler structures. 
		// • Design iteration refers to repeating the basic steps of analysis, prototyping, and testing until a desired result is achieved. 
		// • With iterative design, there is no failure—just iterations with lessons learned to apply to the next iteration. Accordingly, failure is not to be feared but embraced. 
		// • Iteration is the backbone of design and design thinking. Great design does not happen without it. 
		// • Embrace iteration in all aspects of design and development. Seek ways to accelerate iteration to speed learning and progress. Fail fast. Fail early. 
		// See Also Development Cycle • Feedback Loop • KISS Prototyping • Self-Similarity 
		//￼
		// By focusing on designing a plane that could be rebuilt in hours versus months, engineer Paul MacCready enabled his team to dramatically speed up iteration. Six months later, the Gossamer Condor flew over 2,000 meters, winning the first Kremer Prize. 
	},
	// aka test before you apply 
	Prototyping: {
		// Rapidly building low-fidelity models to explore ideas and deeply understand problems. 
		// • Prototyping is research, not development. The goal of prototyping is understanding. 
		// • Iterative prototyping—i.e., rapidly building a series of prototypes to find solutions to problems—is the basis of “design thinking”. 
		// • Prototypes should use simple software tools or readily available materials and makeshift fabrication to accelerate construction. The rate of prototyping is effectively the rate of learning. 
		// • Discard prototypes once they serve their purpose. Prototypes should generally not become products. 
		// • Use prototyping to understand problems deeply and explore solutions quickly. Prototype to the level of understanding: build basic prototypes when understanding is basic, and build increasingly advanced prototypes as understanding develops. 
		// See Also Iteration • KISS • Satisficing • Scaling Fallacy

		// Problem-solving through￼
		// challenges involving many unknowns and time constraints are best solved rapid prototyping. 
	},









	// Imitation
	modelling: {
		// modelling is the process of creating a model which is intended to be used
	},
	model: { // Synonyms: heuristic, belief, assumptions, bias, abstraction, simplification, mock-up, prototype, archetype, sample, emulation, simulation

		is: {
			// Simple representation of something that helps us understand how something works or will work.
			abstract: 1, // A model is always an abstraction. a simplification. Something that is true for certain aspects in certain contexts.
			partially: {
				incorrect: 1, // "All models are incorrect, but some are useful"
			}
		},
		has: {
			accuracy: 1,
		},
		purpose: { // what a good model does:
			// model tells you where to look for behaviour to happen (what conditions are likely to cause it)
			// model tells you what to try to reach desired behaviour.
			// MODELLING IS ONE OF THE MOST ESSENTIAL SKILLS OF THIS SOFTWARE. look at modelling: 
		},

		can: {
			// Models are dangerous if they are misunderstood or misused.
			// Like an abstract reference to something without being it...
		},



		// Statistical models.
		// Scientific model.
		// Black box thinking = model thinking





		// In condition x it will be like y


		// Highly performing but only contextually relevant.


		// Models has to be tested
		// "An unvalidated model is an invalid model"
		// Aka good model is tested whether it works or not.


		// Models can be valid or invalid
		// Can you use it to predict?
		// A model is only as good as the test showing how well it works or not.
		// Aka a model needs to be based on perceived.
		// Models are based on validation. (recording contextual re-occurrence. pattern.)
		// You can not say a model is true from one occurrence. (you can validate that it CAN happen, but not how frequent...)
		// But you can predict that it is uncommon enough for you to not have experienced it any other times in your life.

		// best idea of how something works
		// good enough idea of how something works


		// A model is used to:
		// Gain insight/intuition. (it describes something without being it)
		// Predict. (how something behaves under condition)

		// Model is when you put in a starting condition and then you see where it leads.


		// Models are means to an end. they are tools.
		// A model is never reality. 

		// A useful model:
		//	1. Has a clearly defined purpose.
		//	2. Is one accompanied with knowledge of the limitations of the model.
		//	3. Conveys/represents the relevant aspects of reality.








		// science, statistics, modelling:
		// The idea is that I can create models through statistics to "predict" what is appropriate for a situation.
		// For example by generating a player model I start understanding what the individual player needs.

		// The process of figuring out relevant aspects of something

		// (predicting behaviour in certain circumstances)

		// All of science is based on statistical modelling. you take a context and you measure what happens in that context and then you have a statistical model.
		// It is like taking a very thin but highly measurable slice of the world and then try to infer what that means.

		// Every belief is a model. the player has their own models of the world, that is called mental model

		// I am essentially going to confirm models using other models... which are essentially based on this bias or belief structure.
		// https://www.youtube.com/watch?v=uWuNfhDvZz8
		// https://www.youtube.com/watch?v=yur7JLDD6pU

		// How to verify models:
		// Player predictably classifies the medium they just experienced as a video game.
	},
	heuristic: { //simulacrum //compilation //recipe //nodes //objects //models //approximation // intuition? // concept // hunch
		// A practical method that is not guaranteed to be optimal, perfect, or rational, but is nevertheless sufficient for reaching a goal or an approximation.

		// good side effects:
		// 		Relatively good utility for its level of simplicity.
		//		Relative simplicity to other methods and to general theories.
		// 		optimality/fitness of results.

		//		use risky things with caution.
		//			identify what is risky.
		//			Only heuristics are risky.

		// Where finding an optimal solution is impossible or impractical, heuristic methods can be used to speed up the process of finding a satisfactory solution.
		// Heuristics can be mental shortcuts that ease the cognitive load of making a decision.

		// When in doubt, try heuristics first.
		is: {
			practical: 1,
		},
		has: { // A heurstic has:
			applicableDomain: 1, // 1. A stated situational use case. "while these conditions"
			methodology: 1, // 2. Methodology (list of constraints of action).
			predictableResult: 1, // 3. Stated effects when applied in such situation (at least aproximate effects)
			tradeOff: { //is not guaranteed to be optimal, perfect, or rational
				//	heuristic implies there is a trade - off.
				//		Examples:
				//			reduced optimality.
				//			reduced generality.
				//			reduced explainability (works without knowing why)
				//			increased dependence on more specific situation.
			},
		},

		// Domain simplification:
		//	A simpler solution than the general that applies in a limited conditional situation.
		//		You don't need to take in factors that are irrelevant for "equation" of the situation.

		// "it applies here", do x,

		// The things that need heuristics the most are the things that take most time
		//		things that are most complex and most frequently used.

		// this is analogous the the principles of magic, where the constraints on perspective gives the illusory appearance of a real situation.
		//	magic is tricking the heuristical system of the watcher.



		// estimate functionality.

		// synonyms:
		//		rule of thumb
		//		educated guess (guesstimation). 
		//		lossy compression.
		//		applied 80 20 tradeoff


		// Part of heuristics is it's lack of need for explanation or justification. Follow these steps to get result x. You don't need to know why it works.
		// "if it works it works"
		// ^ the danger is unknown side effects.

		//Abstract example:
		// Situation A, Apply method x, Situation or relative transformation B
		// situation on linear scalar A, Apply method x, linear range of outcome B. (skipping the actual in-between steps)

		// the heuristical map between situation and outcome is cheaper than a full calculation.

		// Examples:
		// Trial and error
		// A recipe rather than the general theory of cooking.
		//		A sufficient instruction for particular result. (Cooking with specific resources and equipment and a bit of pre-cooking knowledge)
		//		An insuficient instruction for general results. (cooking with non-specific resources and equipment)

		// A good heuristic is to balance assumption and questioning.
		//	assume more on what is rigorously tested but question it once in a while.




		// The opposite: If it is not heuristical it is:
		// explanation, justification
		//	rigorous
		//	factual
		//	Communicable
		//	internally valid.
		//	unquestionable.
		//	certain.
		// The absolute opposite of heuristical thinking is first-principles thinking. Relying on the most rigorously tested.


		heuristicality: { // meta info about what deegree it is heuristical
			// Goodness of heuristic:
			// 1. easily identifiable usable situation.
			// 2. Followable/applicable/executable method.
			//	how much simpler is the heuristic than a non-heuristic?	
			//	Will it be practical enough to be worth remembering?
			//		cheapness of requirements
			//			breadth of applicability
			//			fewness of requirements
			// 3. easy pre-comprehension of effects and side-effects.
			//	How accurate is the heuristic across different situational contexts? (heuristic accuracy mapping over multidimensional field)
			//	 	What is it an heuristic for?
			//			What does it assume or simplify?
			//			What does it replace or act as alternative for?
		},

		// Practicality
		// Guaranteedness






		// The nature of heuristics:
		// 
		// heuristics relies on partial truths or magical thinking.
		//
		// Therefore heuristics creates errors.
		//
		// Heuristics can be better because they can 
		//	Sometimes get the full solution in less time time.
		//	Sometimes get the essentials in less time.
		//	
		//	
		// A heuristic is good if it on average saves more time than a "full solution"
		//	
		// If the errors of a heuristic is non-grave, to the point where fixing the error costs less than the time saved using the heuristic with success.

		//What attention asks itself:
		//given the landscape of possible effects from a heuristic
		//	does it save time?

		//^measuring or estimating the safety of the possible errors of heuristics.

		// how elemental is the heuristic (how small is it)? How often can you test it?


		// relying on heuristics is very similar to the skills humans develop through games/training/play. aka intuition
	},


	bias: {
		// A bias is a preference in behaviour.... (That is at least the original idea.)
		is: {
			model: 1, //??????
		},
		// Predictive model of a tendency in humans.
		// Either an effect that applies under certain conditions
		// Or a correlation that something scale with something else (in certain conditions)
		// Subject tend to x in situation y
		subject: {
			//human
		},
		condition: {

		},
		tendencyOfOutcome: { //effect

		},
		correlation: {

		},
		//biases are models that are scientifically testable.
	},









	// Design inconsistencies
	backOfTheDresser: { //this principle has the worst name.
		is: {
			heuristic: {
				//applicableDomain: "design", //Application boundary.
				// whenever standards are relevant
				// about consistency of standards
				// in order to change outcome: proof of holistic thinking which creates real and perceived reliability.
				//		communicates care: "These elements reflect the passion and care (or lack thereof) of the creators."
				//		as any part is exposed it should be evident it was intentional and thought through.
			},
		},
		// Consistency of standards. The standards shining through all aspects.
		// All parts of a design should be held to the same standard of quality. (How does this account for normal distributions and pareto princpiple???)
		// aka Design quality inconsistencies creates suspicion and lack of trust.
		// (What you make should be kept to the standards of products in the same category...)
		// And it is more like, Things that are more thorough tend to be better because they account for more edge cases.
		// • Proposed informally by Steve Jobs, American entrepreneur and personal computer pioneer. 
		// • The principle asserts that the craftsmanship applied by designers and developers to areas not ordinarily visible to customers is a good indicator of product quality—it is proof whether they applied consistent craftsmanship to all aspects of the product. 
		// • Indications of craftsmanship include quality materials, precision fit, uniformity of finish, internal consistency, and maker marks or signatures. These elements reflect the passion and care (or lack thereof) of the creators. 
		// • Apply excellent design and development to all aspects of products. Treat invisible areas as if they were visible, and include them in testing and evaluation. Light is the best disinfectant for shoddy craftsmanship. 
	},

	// Design heuristics
	formFollowsFunction: {
		is: {
			heuristic: {
				applicableDomain: 1, //Application boundary.
			},
		},
		// form follows function, constraints and limitations
		// utformning från funktion.

		// let hardware constraints/limitation guide the design of the game.
		//	do not make a game that wont work on the platform.
		//	affordances

		// Aesthetic considerations should be secondary to functional considerations. 
		// • A maxim derived from the architect Louis Sullivan, who wrote that, in nature, “form ever follows function”. 
		// • The motto became a guiding principle for modernists who emphasized function over ornamentation. 
		// • The prescriptive interpretation means that designers should focus on function first, form second. The descriptive interpretation means that beauty arises from purity of function. 
		// • Though the maxim is often considered oversimple and cliché, most great designers abide by the principle. 
		// • Consider form follows function a strong guideline in design. While it does not apply to all cases all of the time, it generally leads to superior design. 
		// See Also 80/20 Rule • KISS • MAYA • Ockham’s Razor 

		// The Jeep is the embodiment of form follows function. General Eisenhower identified it as one of three tools that won WWII. 
	},
	hierarchyofDesignNeeds: {
		// The user-centered goals that a design must satisfy in order to achieve optimal success.
		// • The hierarchy of needs consists of five levels that are typically satisfied in sequence:
		levels: {
			functionality: 1, //1. Functionality: fosters satisfaction by meeting basic functional needs.
			reliability: 1, //2. Reliability: fosters trust through consistent and reliable performance over time.
			usability: 1, //3. Usability: fosters fondness through ease of use.
			proficiency: 1, //4. Proficiency: fosters pride and status through increased productivity and empowerment.
			creativity: 1, //5. Creativity: fosters cult-like loyalty through innovation and personal enrichment.
		}
		// • Consider the hierarchy of needs in design strategy. Markets with products low on the hierarchy are ripe
		// For disruption; whereas markets with products high on the hierarchy will be the most competitive. 
		// See Also Aesthetic-Usability Effect • Form Follows Function

		// GoPro cameras were designed to be small and rugged, but who could have anticipated the ??? they would unleash? 
	},









	// Clean up
	curation: {

		// Selection of worthy examples.

		// Selecting, organizing and maintaining items for a collection or exhibition.
		// Curation is the process of scanning many alternatives and selecting a set that together is more optimal or focused?
		// The algorithm - be prepared for all actions. curating function
		// One idea of the curator process for the engine
		// Scan all the possible paths initially.
		//		build the game around the best combinations
		//			and this lets you be prepared

		// Curation is mainly about getting what you need without inertia, complexity and negative side effects
		// Curation mainly mean to get clean info you do not normally get.
		// Picking out the novel bits
		// Criticality. No bumps, no noise.
		// Straight to the point.
	},


	// efficiency heuristics
	KISS: { // Keep It Simple Stupid
		// Simple designs work better and are more reliable. 
		// • The acronym KISS—“keep it simple stupid”—was proposed by Kelly Johnson, lead engineer at the Lockheed Skunk Works. 
		// • KISS asserts that simple systems typically work better than complex systems. Accordingly, simplicity should be a key goal in design. 
		// • Simplicity means minimal elements with the minimal interactions between those elements. 
		// • Simple designs are typically faster and cheaper to build, perform more reliably, and are easier to troubleshoot and maintain. 
		// • KISS is commonly employed in engineering and software development, but is applicable to any field that makes things, from entrepreneurship to writing. 
		// • Keep it simple stupid. 
		// See Also 80/20 Rule • Feature Creep • Form Follows Function Modularity • Ockham’s Razor 

		// A testament to Mikhail Kalashnikov’s dedication to KISS, the AK-47 is perhaps the least expensive and most reliable assault rifle ever produced. It has only eight moving parts
	},
	OckhamsRazor: {
		// Simple designs should be preferred. 
		// Prefer what is simple over what is complex
		// If otherwise equal prefer low complexity alternative
		// • Named after William of Ockham, a fourteenth-century Franciscan friar and logician who reputedly made abundant use of the principle. 
		// • Given two functionally equivalent designs, the simpler design should be preferred. 
		// • Applying Ockham’s razor means cutting unnecessary elements from a design. 
		// • Proficient designers favor simplicity over ornamentation, supporting use of Ockham’s razor. 
		// • Apply Ockham’s razor to elements in your design. If the design is no worse for their loss, apply it again. 
		// See Also Form Follows Function • Horror Vacui • KISS Mapping • Signal-to-Noise Ratio 

		// The evolution of the iMac proves that no company wields Ockham’s razor with the skill and aggression of Apple.
	},
	featureCreep: { //complexity comes from scale. complexity is harder to manage.
		// A continuous expansion or addition of new product features. 
		// • Feature creep is one of the most common causes of cost and schedule overruns. This occurs because features are easy or convenient to add, accumulate over multiple generations of a product, or are added to appease internal project stakeholders. 
		// • The key driver is the perception that more is better, and therefore features are continuously added and rarely taken away. But adding features adds complexity, and complexity is expensive. 
		// • Beware feature creep in design and development. Ensure features are linked to customer needs, and are not added out of convenience or appeasement. Create a milestone to formally freeze the product specification, and shout “feature creeper” at any person attempting to add features beyond that point. 
		// See Also 80/20 Rule • Design by Committee • KISS 

		// On its maiden voyage in 1628, the Swedish warship Vasa sank after going less than one mile. The
		// cause? Extra guns, decks, and carvings added during construction compromised its stability.
		// Feature creep literally sank the ship. 
	},

	// Design trade-off heuristics
	flexibilityTradeOffs: {
		// As the flexibility of a design increases, the usability and performance of the design decreases. 
		// • Accommodating flexibility entails satisfying a larger set of design requirements, which invariably means more compromises and complexity in design. 
		// • Increasing complexity generally decreases performance and usability. 
		// • For example, the Swiss Army knife has many tools that increase its flexibility, but the tools are less efficient and less usable than their standalone equivalents. Performance and usability are traded for flexibility. 
		// • Specialized things outperform flexible things when their requirements are stable. Flexible things outperform specialized things when their requirements are volatile. 
		// • When flexibility is key, prepare to compromise performance and usability. When performance or usability is key, prepare to compromise flexibility. 
		// See Also Cost-Benefit • Feature Creep • Hick’s Law

		//The Swiss Army knife is a prime example of flexibility trade-offs. It is the proverbial “Jack of all master of none”. 
	},
	scalingFallacy: {
		// Designs that work at one scale often do not work at smaller or larger scales. 
		//  • Designs often perform differently at different scales because the forces involved scale in different ways.
		// Two types of scaling assumptions result in scaling errors: load and interaction. 
		//  • Load assumptions are assumptions that working stresses will be the same when a design changes scale. 
		//  • Interaction assumptions are assumptions that the way people and other systems interact with a design will be the same when a design changes scale. 
		//  • Minimize scaling assumptions in design by raising awareness, testing assumptions, and researching analogous designs.
		// When you are unable to verify scaling effects, build in factors of safety. 
		// See Also Factor of Safety • Feedback Loop • Modularity Prototyping • Redundancy • Satisficing

		// Flapping to fly works at mid-range scales, but it does not work at very small scales where wings can’t displace air molecules, or at large scales where the effects of gravity are too great.
	},








	//safety heuristics
	// Seurity and Error proofiong (engineering) //error handling engineering design 
	confirmation: { //security, error prevention
		// A technique for preventing errors by requiring verification before actions are performed. 
		// • A technique used to verify critical actions, inputs, or commands. Confirmations provide a means for verifying that actions and inputs are intentional and correct before they are executed. 
		// • There are two basic confirmation types: dialogs and two-step operations. 
		// • Dialogs ask if an action was intended, indicate the consequences of the action, and then provide a means to either verify or cancel the action. Two-step operations involve a preliminary step that must occur prior to the actual action or input. 
		// • Use confirmations to minimize errors. Avoid using too many confirmations, else they will be ignored.
		// Allow noncritical confirmations to be disabled. 
		// See Also Affordance • Constraint • Errors • Forgiveness Garbage In-Garbage Out • Redundancy

		// This industrial paper-cutting machine requires a two-step confirmation: both hands must depress safety releases (ensuring that they are not in harm’s way) before the foot press is unlocked to cut the paper.
	},
	control: { //Give apropriate level control to the level of skill/ability of the user.
		// When making a mechanic it should be highly abstracted and simplified for players with low skill level in that area.
		// The level of user control should be related to the proficiency and experience of the user. 
		// • Novices perform best with less control, while experts perform best with more control. Accordingly, a well-designed system should offer varying levels of user control according to level of expertise. 
		// • Novices benefit from structured interactions, minimal choices, more prompts, constraints, and access to help. 
		// • Experts benefit from less structure, direct access to functions, and minimal use of constraints and prompts. 
		// • Since accommodating multiple methods increases complexity, the number of methods should be limited to two: one for beginners and one for experts. 
		// • Provide methods optimized for beginners and experts for frequent operations. When systems are complex and frequently used, consider designs that can be customized to user preference and levels of expertise. 
		// See Also Constraint • Flexibility Trade-Offs • Visibility

		// Thebicycle￼ level of rider support decreases as expertise develops: from tricycle to training wheels to to Cirque du Soleil. 
	},
	errors: {
		// An action or omission of action yielding an unintended result. 
		// • There are three categories of error: slips, mistakes, and lapses. 
		// • Slips occur when an action is not what was intended; for example, when a person intends to press one button but accidentally presses another. 
		// • Mistakes occur when an intention is incorrect; for example, when a nurse interprets an alarm incorrectly and then administers the wrong medicine. 
		// • Lapses occur when an action is forgotten for example, when a pilot forgets to lower landing gear for landing. 
		// • Minimize slips using affordances, confirmations, and constraints. Minimize mistakes using clear feedback, mappings, and training. Minimize lapses using alarms, checklists, and visibility. 
		// See Also Affordance • Confirmation • Constraint • Visibility
	},
	forgiveness: {
		// Designs should help people avoid errors, and protect them from harm when they do occur. 
		// • Forgiving design prevents errors through constraints and good affordances. 
		// • Forgiving design warns of potential dangers, and asks for confirmation when choices may cause harm. 
		// • Forgiving design allows actions to be reversible when errors do occur (e.g., undo function). 
		// • Forgiving design provides safety nets to prevent harm resulting from errors or catastrophic failures. 
		// • Make all designs forgiving. Best to prevent errors, then to warn of potential errors, then to reverse errors when they occur, and then when all else fails to have safety nets in place. To err is human, to forgive, design. 
		// See Also Affordance • Confirmation • Constraint • Errors Factor of Safety • Weakest Link

		//When all else fails, what pilot and crew wouldn’t want this ballistic recovery system as a safety net of last resort?
	},



	Mimicry: {
		// things with similar structure tends to have similar functionality or behaviour.
		// similar structures tends to have similar behaviour?

		// Copying properties from familiar things in order to realize benefits of those properties. 
		// • In design, mimicry refers to copying pre-existing solutions to problems. There are three kinds of mimicry: surface, behavioral, and functional. 
		// • Surface Mimicry: copying the way things look (e.g., knock-offs of expensive brands or products). 
		// • Behavioral Mimicry: copying the way things act (e.g., robots simulating human facial expressions). 
		// • Functional Mimicry: copying the way things work (e.g., mimicking the keypad of an adding machine in the design of a touch-tone telephone). 
		// • Mimicry is perhaps the oldest and most efficient method for achieving advances in design. Consider mimicry as one approach in solving problems, considering the surface, behavioral, and functional properties of both natural and man-made things. 
		// See Also Anthropomorphism • Baby-Face Bias Savanna Preference • Supernormal Stimulus

		// risk in mimicry is cargo cultism (mimicking arbritrary or otherwise non critical aspects.)

		// Scuba suits that mimic the black and white banding of the Indo-Pacific sea snake show promise as deterrent. 
	},








	// Constraints enforcement

	// Possibilities
	// Restrictions/constraints /limitations/ enforcement
	// Constraints limitations
	// Words: can, cant ( example: cant cause fire) //future: will, wont... you should probably turn "will" into can/must in future or in x time 
	// Use together with normalcy words: always, never, sometimes
	// examples "









	// THE VALUE OF USER AND HUMAN FEEDBACK

	// Design should be dictated by users and people. Both listening to their words and actions. the average of people has wisdom.
	crowdIntelligence: {
		// An emergent intelligence arising from the unwitting collaboration of many people. 
		// • Crowd intelligence means that the average responses of a group to a certain class of problems are better than any individual response from that group. A crowd is any group of people whose members contribute independently to solving a problem. 
		// • Crowd intelligence works best on problems that have definitive answers. For example, crowd intelligence is effective at solving math problems, but less effective at solving design problems. 
		// • The highest crowd intelligence emerges from groups made up of diverse opinions and beliefs. Strong authoritative or social influences within a group undermines its crowd intelligence. 
		// • Consider crowd intelligence as an alternative means of solving problems. Limit its use to problems that can be simply expressed and with definitive answers. 
		// See Also 80/20 Rule • Normal Distribution • Selection Bias 

		// Almost 800 people guessed the weight of an ox butchered and dressed. Statistician Francis Galton found that the average of these guesses missed the weight by only one pound, which was closer than any individual guess. 
	},
	designByCommittee: {
		// A design process based on consensus building, group decision making, and extensive iteration. 
		// • Design by committee is preferred when requirements are complex, consequences of error are serious, and stakeholder buy-in is important. 
		// • Successful design committees are small and diverse, and equipped with the method to facilitate decision making and prevent deadlocks. 
		// • Use design by committee when error mitigation and stakeholder acceptance are primary factors, and there is time for iteration. Use design by dictator when an aggressive time line is the primary concern. 
		// • Autocracy is fast, but risky. Democracy is slow, but careful. Both models have their place depending on the circumstances. 
		// See Also Dunning-Kruger Effect • Feature Creep • Iteration Not Invented Here • Prototyping 

		// Design by committee averages out idiosyncrasies, as seen in the evolution (left to right) of One World Trade Center. The final design is less visually interesting, but overall it is superior because itbetter addresses all stakeholder requirements. 
	},
	desireLine: { //the places things are worn indicate inforcement/improvement is needed.
		// Traces of use or wear that indicate preferred methods of interaction. 
		// • Desire lines generally refer to worn paths where people walk, but can be applied to any signs or traces left by user activity. 
		// • Desire lines represent a noninvasive and unbiased measure of how people use things. For example, wear on computer keys indicates their frequency of use. 
		// • Desire lines can be artificially created and studied using technologies such as video cameras, GPS, and website heat maps. 
		// • Pay attention to desire lines. They indicate strong and unbiased user preferences, and represent opportunities for improvement. 
		// See Also Affordance • Cost-Benefit • Gamification Performance Load • Root Cause 

		// Desire lines are often seen branching off of designated paths, indicating a strong pedestrian preference for alternate routes. 
	},
	convergence: {
		// A tendency for similar characteristics to evolve independently in similar environments. 
		// • Environments influence which adaptive strategies are successful. Analogous environments result in analogous solutions to problems. 
		// • It is common for discoveries and inventions to be made independently and simultaneously by multiple independent inventors and scientists. 
		// • Stable environments promote convergence, favoring refinements to existing solutions. 
		// • Volatile environments obstruct convergence, favoring innovation and experimentation. 
		// • Consider convergence in innovation strategy, and as a reminder that others are likely working on ideas similar to yours right now. 
		// See Also Iteration • Mimicry • Most Advanced Yet Acceptable 

		// Strategies for movement through a fluid have converged over millions of years. This suggests that new systems will likely need to use one of these strategies to be competitive. 
	},



	// make things that are both functional and preferable to humans.
	performanceVsPreference: {
		// Humans prefer things based on many factors independent of performance. (based on their bias)
		// Preferences may be based on innate tendencies, cultural biases, aesthetic or emotional considerations, or legacy practices and conventions. 

		// Increasing performance does not necessarily increase desirability. 
		// • People often believe that a functionally superior design—the proverbial “better mousetrap”—is good design. This is not necessarily correct. 
		// • The reasons people favor one design over another is a combination of many factors, and may have nothing to do with performance, but with preference. 
		// • Preferences may be based on innate tendencies, cultural biases, aesthetic or emotional considerations, or legacy practices and conventions. 
		// • Success in design is multivariate. Consider both performance and preference factors in design to maximize the probability of success. Beware the trap of creating a superior product in one dimension, but having it fail due to neglect of other dimensions. 
		// See Also Aesthetic-Usability Effect • Control • Desire Lines Flexibility Trade-Offs • Hierarchy of Needs 

		// The performance advantages of the Dvorak keyboard are no match for the generations of users trained on QWERTY. 
	},



	optimizations: { // Code optimization or otherwise waste reduction heuristics
		instanceOnUnique: {
			// When multiple things are literally the same data structure, then you can point all of them to the same data structure.
			// You only need to create new instances of the data structure as soon as their properties become unique.
			// Then you can the new instance only contain that which differs.
			// Like we have 100 humans.
			// But only one is standing 10 meters to the right.
			// Then
			// There can be 5000 things that only need one instance, wh
			// You only need to instance	
		},
		ManyPointsWithRelativePositions: {
			//		turn them into like int8 and store pixel distance
			//			divide int by 10 to get float values lol.
			//
			//		or uint8 and store rotation and distance,
			//			constraints it to 256 degrees instead of 360
			//			and maybe 256 px distances
		},
		compressedRam: {
			// Compressing data in RAM for more space in ram rather than writing to disk.
		},
		monoArray: {
			// How make the computer work efficiently - everything is one array of raw data, loop it with for loops
			// Arrays are easy for the CPU to predict and cache. like the next 
			// For loops.
			// CPU notices linear pattern of iteration in memory.
			// You essentially want to flatten your whole software to one array that is itterted on in linear fashions.
			// ^ Maybe even make multiple arays tht contain the same data (not pointers) for this purpose...
			// 95% of lines of code should not exist.
		},
		randomProbabilityOverMemory: {
			// Principle replace memory of duration with probability (i guess like Minecraft make plant grow at random rather than timer)
			//	Processes > memory
		},
		loadingRequirementsPrediction: {
			// Based on the density of items in a chunk (and likelihood chunk will be seen)
			// Make sure to load in time.
			// Like if a 10000 items area is around the corner, then most of that should have been loaded already to be ready.
		},
		justInTimeProcessing: { // Just in time determination (just in time delivery)
			// States and properties in games only have to change when it matters....
			// Literally no property has to be changed until the moment of presentation.
			// But sometimes values must be remembered at the time of presentation.
			//
			// It only has to be remembered as long as the player relies on the external(simulated world) to remember for them.
		},
		fastAndSlowComputerOperations: {
			// Notoriously slow operations:
			//	divided by
			//		(35 to 88 clock cycles)
			//	square root
			//	x87 floating point unit instructions
			//		very very slow
			//	any instruction that references ram or memory.
			//	instructions with a lock prefix

			// branchless programming:
			// keep if statements (branching) out of your for loops
			//	(unless the the compiler know what you are trying to do, you can gain like 3.3x speed.
			//	You can get it like 6 or 7 times faster!!

			// cool optimization techniques:
			//	Single instruction multiple data
			//	Branchless programming

			// simd -> 40x speed...

			// Read -> modify -> write
			// Usually it is written to a memory cache before being written to ram.
		},
		renderingOptimizations: { // Rerendering optimization:
			// Do not change graphics.
			// Too much animation can cause overstimulation anyway

			// Interior does only need to render when they are seen.

			// Delayed presentation is fine... Good distraction is overwhelming light or overwhelming darkness
			// Let's say the player opens a door. It is ok if the interior of the house that should be drawn behind the hole of the doorway
			// Is delayed, and instead it is fully black/white for a couple of frames and, when ready, fades in the actual graphics.

			// Optimization: You can keep details low in aproximate colorspace.... If something is really bright or really dark, then you do not need as many unique colors.
			//	It is harder to make out details anyway, so it is ok to have both low res and low color count for as long as the space is dark.
			// The same is true with low contrast or filters/ half translucent mediums (for eample fog). In these cases it is not important to preserve interior details.
			// with back lit fog you only see countours.

			//	Re-render as little as possible (as few draw calls, as few details as possible.)
			//	Show the layers behind when there is transparency
			//	Solution
			//		I have to keep track of the rectangles over which there is transparency in the top layer
			//		Rendering this top layer consists
			//			Render the blit of underlying layers over the rectangles of transparency
			//			Render the top layer.
			//		To save performance, the blits of underlying layers can be saved into the actual top layer.
			//			This works as long as the underlying layers are not changing. 	
		},

		// About textures: opaque performs better than transparent.

		// OPTIMIZATIONS: There are multiple ways to increase collision performance.
		// One is quad tree. that is to keep track of what is in a square. so as to not do checks on things that have 0% chance of colliding
		// Another is collision layers, where you specify what combinations can collide in the first place.
		//	For example we do not care about sun colliding with ground or whatever.
		// Mmy flip on collision layers is to track emergability. that is, in the given context does there exist a possibility of emergence of that type.

		// You eliminate the checks that needs to be done in smart ways (by trying to do a tree elimination, eliminating 50% of possibilities every step)
		// You do the possible condition checks and put them in an update function
		// Then you update them all at once in a time point, an event.

		//OPtimization: figure of if there is ever an exception (if alternate conditions can ever be triggered in the context). if not, give the conditional change in context a trigger to re-check excpeptions and from now on skip all steps between initial action to final event/result. (aka use cached results.)
	},









	// Various forms of media
	mediumTropes: {
		comicBook: {
			//panels
			//	are images
			//	presented left to right
			//	
			//spell out sound effects in bubbles
			//Sound effects are spelled out in screen in comic book style
		},
		music: {
			songs: {
				//Every song in a music album is greatly supported by being accompanied by a unique cover art or graphic or music video.
			},
		},
	},
	medium: {
		// Vessel/channel of communication
		// A structure of format of communication

		//intentions: 1, // A media product is created with intentions.

		//mediums of feedback
		//	imagery
		//	audio
		//	vibration
		has: {
			//everything mediated has an aesthetic and a style though....
			aesthetic: 1,
			style: 1,
		}
	},

	// ( Kinda meta )

	picture: {
		is: {
			// A plane of pixels
			texture: 1, //then I do not need to specify box or dimensions or whatever.
			medium: 1
		},
		requirementStructure: { //of a picture // because there must be a Collapsation Needs Statement //goalstructure
			// Previous idea
			// is there motif?
			// is there a definition for the motif
			// is there a draw function for the motif

			// What should happen is a search for a a canvas, a search for a "depiction", which contains shape or sprite and a draw call
			// It is a shopping list, or maybe a todo list? 
			// Requirements
			canvas: 1, //drawing requires: a plane to draw on
			depiction: { //a picture must depict a subject matter
				// it is only depiction if what is depicted is fit in within the canvas.
				//	The shape must have its horizontal lengths values within screen width values
				//	The shape must have its verticals within screen height
				//	The shape must have its length less than the closest screen edge of its position
				// a shape or sprite to draw
				oneOf: { // basically an OR function, these root or ternary keywords should be checked first when the algorithm does not know what todo.
					figure: {
						atLeast: 1,
					}, //drawing a shape requires a style
					sprite: {
						atLeast: 1,
					}
				},
				call: { // basically requires you to call the functions in here
					draw: {
						atLeast: 1, //The process of depiction is to drawon surface
					}
					// draw calls always have the restrictions of: surface, shape, style
					// Everything that is drawn has a position (where it is drawn)
					// in draw calls I do not really need to separate the arguments.
					//	because why do I assume they are different things?
				}
			}
		},
		has: {
			//surface: defaultLayer,
		},
	},
	gif: { // wait a minute... this is essentially a classical object because it stores both functions and data. did I fail to overcome object orientation?
		// I do not think I failed because this can only be interpreted as an object, the whole software is not object oriented. There is no internal
		is: {
			medium: 1
		},
		types: {
			smoothLooping: 1, // is it made look endless
		},
		features: {
			dimensions: {
				x: 1,
				y: 1,
			},
			frames: {},
			fps: {},
			duration: { //this is indirect. You can manipulate frames and fps indirectly with the duration concept, but the duration does not need to be saved. 
			}
		},
		handle: function () {},
	},
	sound: {
		is: {
			medium: 1
		}
	},
	video: {
		is: {
			medium: 1
		}
	},

	// add to mediums...
	interactiveMusicAlbum: { //interactive music album
		// You play a game
		// For as long as a song goes.
	},






	manifestation: { // (controlled aka predictable actuation, what happens from agents in control)
		// Presentation. (things with agency still act even when out of sight) persistence of function
		// 1. clear or obvious to the eye or mind.
		// 2. Show by one's act of appearance.

		// "Communicate qualities or feelings with clarity and intention."
		// Something that manifest is felt/measurable presence (by the player)

		// The conditions of which something is presented or simulated
		// + the meaning, what it means for it to manifest....

		// Consider this:
		//	what lets something manifest? What stops things from manifesting?
		//	load from abstract into practical implementation. (from ideas/philosophies and styles into concrete things)
		//	fill in the necessary uncertainties...

		// needs to manifest through a medium.
		// memory, process, image, audio, vibration.

		// **existence/realness**}, realized in reality, real:
		// (at this point I was still thinking that the algorithm defines concepts of the game world that are abstract and then fills in details if the details are needed)
		// the main point is that established things needs to be remembered by the algorithm to stay persistent.
		// and maybe that abstract concepts can be unpacked. decompressed. You can take it for surface value or you can create a super accurate model in memory.

		// Levels of realization:
		//	Is rectangle (the concept of rectangle)
		//	Is defined rectangle (properties set in stone)
		//	Is realized (it has appeared in the simulation)
		//	Though: are concepts holy and can not be drawn?
		//	Is asserted? has been presented in one form or another to the player.
		//	Is perceived as rectangle (properties set in stone and it is rendered and interpreted as rectangle by player)
		//	Sometimes things are not what they look like.
		//	Shaped like a rectangle basically means fills most of a rectangular area. the more it fills a rectangular area without filling outside it, the more it is rectangular in shape.

		statesOfManifestation: { //Describing simulation "levels": 
			// I think essentially the metaphysics should be static and the existence should be generated perception of the changes in reality.
			// Inconsistent - paradoxical and non persistent. (is inconsistency truly the opposite of persistence?)
			// Seemingly - Showing the symptoms of. This means it can turn out not to be the case at a later point. Like suggestively. A red herring,
		},

		simulation: { // Simulation is functional persistence.
			memory: 1, //?????
			react: 1, //(memory)//???????

			// experiment section
			memory: 1, // activeSimulationMemory,
			has: { // maybe the has keyword can be implicit? like unless it is a keyword like "is" it just means it has that property. but maybe "has" indicates it is not realized...
				memory: 1, // activeSimulationMemory, //where the state is kept and the initial state is put.
				rules: {},
				reaction: { // Uninitiated reaction is what makes something able to simulate
					// This is a tension between current state and end state
					// Directed by the concerns of a goal structure
					// Example:
					// 		put a rock in a mid air state so that when simulation starts it falls down and crashes into the ground by the set up rules and forces of the simulation.
				}
			}
		},
	},
	reproduction: { // reproducing phenomena
		// Generate the same content between saves and between computers, with seeded random
		// (basically compression/decompression.)

		// you have one algorithm that generates random seeds
		// these seeds are put as the seed of each unique domain
		//	one for every isolated system.
		//		one for cooking
		//		one for growing vegetation
		//		one for weather
		//		One for every room
		//		One for every place.
		//		etc
		// this way multiple instances can generate the same result in the isolated instances
		//
		// basically "I need the root seed for places so I can get the root seed for area 5"
		//	1,2,3,4,5 now you have root seed for places
		//		1,2,3,4,5 now you have root seed for area 5
		//		
		//		
		// predictable sequencing needs to be considered in this way of generating.
		//	I need to sequence the items in each subdomain predictably.
		//		"places" has to always be root seed number 5
		//			"area 5" always has to be sub seed number 5, etc.
		//		so the algorithms must come to the same conclusions of what seed an area has...
		//		or maybe every place has the same seed... It is just everything else that is different.
		//			It is the initial conditions that are different. not the seeds.
		//			that way you do not have to make a predictable list of every area
		//				but just get the generic area seed....
	},









	metaphysics: { // 1 Metaphysics are the underlying rules of the world. The philosophy the world is based on. (like art style or aesthetic or code of conduct of the world.)
		// underlying non touchable aspects of the universe and the nature of the world.
		// generating rules, can be conceptual or non-conceptual.
		// conceptual - having defined archetypal boundaries.
		// (Non-conceptuated phenomena can be randomly generated)
		//	non conceptual phenomena is characterised by either solely on label which points to some repeatable randomness.
	},
	world: { // 2 World or universe
		// bounded space or realm, of consistency, countaining objects. // a universe is a world you can't escape.

		// a world is like a realm of realms.
		// a realm is like a world as an element of a world.

		has: {
			realm: { //defined area or volume of space, distances, and composition of places.
				has: {
					bounds: 1 //isolated, requiring effort to escape or enter. or edges which transitions into other realms
				}
			}
			// consistency: aesthetic, style, relative quantities of things, 
		},

		// world is defined as that which contains objects. that which all objects recide within?
		//world:
		//	has:
		//		objects

		// is the model of the world is supposed to be like. This is defined in the process of worldBuilding. //what stays consistent between present moments.
		// worldbuilding = deciding which substances exists in a world and their temporal or historical (the metaphysical rules that exist how the world developed with them) development in composition and form.

		//		characterarte, geografi, plantor, metaphysic (magic? etc), how is it like to live in? what happen during the days and nights, what is the biomes and weather ranges, 
		// does it have realistic consistencies (planets, moons) is it completely fantastical or abstract?
		// what are it's natural resources? which are scarce and which are every where?
		// what does actors in the world desire, or seek, etc.

		// history, how long has it existed like this? how did it develop to this? has there been any major events? disasters? Is there a mythology or lore to this world.
		//	what of the worlds history is hidden to the present inhabitants?

		// what sort of life inhabits the world? biology? monsters? robots? ghosts/spirits? etc.
		// are there hybrids? (mermaid, fishmen,)

		//what are peoples belief systems or religions? are there gods?
		// are there different cultures.

		// what langauges do beings speak? how do they communicate with eachother?

		// are there being from another world (or other novetly/mismatch of historical properties)

		// is there a political structure? Who has power? who is in control? Who is supressed. who are outsiders. what are their agendas/intentoion?

		// how is power measured in this world/society?

		// social classes?

		// high abstraction requires low world building. or rather few dimensional world building.

		// abstract, realistic, fantastical, dreamlike
		// a complex abstract game becomes fantastical. unless the metaphysics are specifically congruent with our reality in which it becomes realistic.

		//realism is the congruence with the realistic world aesthetic. it has physics as we know it, our planets, our history, etc.
		// fantastical just means incongruent or misaligned with realism.
		// dreamy means less consistent. whereas a fantstical world may still be consistent.
		// abstract means less complex more streamlined world. (often very consistent.)

		// how are peoples daily life and routines. what do they do to meet their various needs with their environment.


		// This is where the memory of persistence resides. (according to metaphysics and philosophy of things persistence lol.)

		// What is the purpose of the game world mechanically
		//	to walk through
		//	to be a playground
		//	to hide information

		// Everything that occurs in the world. Does only have to be noticed when it affects what is real.
		// Things are constantly loaded in and out of reality from the world.
		// basically the concept of the world
		// and also anything of the simulation that is not directly present
		// like the ram memory of entities and stuff.
		// And any non sensory simulations that happens and need to be accounted for to create accurate presentation.
	},
	thePresent: { //presentation: { // 3 presentation (present moment experience) // presented...  presentation: {
		// Is always in the present moment.
		// Comes (Communicated) to the player through the mediums (as visuals on screen, as audio in speakers, as vibrations in controllers, etc.) into the player's senses.
		// Does: appear
		// Creates: impression/experience
		// The content of the presentation is Everything that is in the world and is now observed by the player's field of view or frame of reference.
		// What the player is with. What the player has when being.
		// presented things are especially persistent. Both being consistent on screen and being remembered accurately to be useful/relevant in the plot or game/mechanics.
		// Communication of the world, style and philosophy all has to come as changes through thePresent moment by moment over time.
		// That which has been presented is the only thing confirmed as real to the player.
		//	And the player might need patterns to repeat multiple times in order to really get it.

		//when truths has been presented to the player those truths has to stay consistent over time to be meaningful.

		//That which has been presented is the only thing has been set in stone.
		//Everything that has not yet been presented directly or indirectly (indirectly is for example: someone talking about it, or it being something you can infer) else could change based on what the game master wants.
	},









	sounding: { //I need a sounding condition
		is: {
			in: {
				world: 1
			}
		}
		//something something distance and volume is more than 0
	},
	screening: {
		// Output graphics through a graphics screen.
		//requires:
		//	screen
		//		is
		//			on
		is: {
			// is presented on screen.
			// you could say that all real things react with their position being on the screen and this reaction is the rendering 
			// is in world.
			// is intersecting the screen rect.
			// is not fully obscured.
		}
	},

	unrealized: { // non-realized, unreal:
	},


	// unknown, ambiguity, uncertainty
	predictability: {
		// known contextual probabilities.
	},
	unpredictability: { //novelty:?
		// It is not known until it is played out.
		// Hidden information. //the stuff you can toy with behind the scenes.
		// Speculations and analysis are not rewarded by the unpredictable.

		// Creates intrigue and speculation
		// Presenting something that has many question marks on how it relates to other things.
		// Discovery is the pursuit of identifying something unpredictable, by finding patterns how it relates to things.

		// unpredictability can give the effect of: comedy, urgency/stress, frustration, relief, 
	},
	accuracy: { //add to optimization
		// The further away something is the less accurate it has to be
		// The smaller it is the less accurate it has to be (no need to draw or calculate sub-pixels)
		// The more chaotic the less accurate it has to be. (do not realistically create the sound of each flapping leaf of a tree, just create noise lol)
	},

	// "Skills:"!
	uncertaintyElimination: { // for productivity of the system...
		// uncertainty ambiguity (unpredictability)
		heuristics: {
			// only eliminate uncertainty out of necessity
			internalSystemCheckUp: { // when uncertain context it will look for resolver code (heuristic?) which can create order
				// check if there are more definitions in the system... this sorta happen automatically I presume.
			},
			chooseTheMostSpecified: { // Choosing the most specified	
				// If there are multiple choices, choose the one that is most specific/related to the context.
				//	For example should "playerCharacter humanoid head" is more specific than "head", if there is a draw function for that specificity intersection use it. (aka if there is a rule when more things are together in a context, prefer the one that usus more variables???) //This has to be defined as a deep principle in the bias.
				//	// This can be seen as a core heuristic
				//	// the law of specificity
			},
			askingExpertSystem: {
				// There must be a protocol for when to ask and whom to ask about what.
				// Certain things can be asked to and decided by the game testers.
				// Certain things can be do polls
			},
			trialAndError: { //figure out through trial and error and memorisation of results.
				//The trial and error approach is used most successfully with simple problems and in games, and it is often the last resort when no apparent rule applies
				//. Given a search problem with a hidden input, we are asked to find a valid solution, to find which we can propose candidate solutions (trials), and use observed violations (errors), to prepare future proposals
				//TODO define the cost of trial and error
				// aka do a study yourself....
				// It is costly to do experiments and it is hard. so this should be a last step I guess?
				// There needs to be a way to know what experiment to do
				// There must be a way to save the result of any experiment performed. to never have to do the same research again..
				// Certain contexts are best for internal testing... like a lot of performance and player satisfaction things.
				inSimulatedEnvironment: 1, //implying the research does not require player interaction. so uncertainty can be eliminated by attempting to generate results and then double check them (I do not really know why I would need to double check anything if I the restrictions are defined in the generating algorithm... it is like "make only these constraints!" "did you make it by only these constraints?")
				// Part of trial and error can basically be to develop neural net with intuitions for how to make games....
				// And in that case you need a more hard system to check against whether things created good results..

				// With systems that can learn through trial and error you have separate:
				//	Intention(goal)
				//	action(method)
				//	effect(result)
				//	comparison/learning between intention and effect. (reflection)

			},
			randomization: { // When it is too costly it is less costly to settle with a coin flip.
				// TODO define the cost of randomized trials
				// Just assume the first best thing.
				// Randomization is what it means to do "trial" in trial and error....
				// So I guess randomization is really a sub part of trial and error
				// If nothing else works. and everything else is too costly, just put in some random number. Worst case your game goes from temporarily halting to stopping. best case, the game just moves on.
				// This is probably the best when deciding in impossible scenarios, like when or if paradoxes arises.
			}
		},
	},
	howToPrioritizeUncertainty: {
		// How to prioritize it
		//    Description of collapsing uncertainty
		//        Noise and normalized values creates a standardized interface between diverse functions (just like ai)
		//        |
		//        V
		//        Dependency of what you need to know/define in order to create the desired thing.
		//        |
		//        V
		//        Start with the most defined or most certain and generate outward.
		//        This is how to rank/prioritize.
		//        Like plockepin. (You take the ones that are not touching the others first, then the ones on top that are easiest to move)
	},
	howToMeasureUncertainty: {
		// You can create ambiguity in abstraction: how many known phenomena are represented by this abstract structure of a set of characteristics?
		// unit/scale/level of ambiguity
		//    10 - vagueness/openness - tags, words
		//    ...
		//    5 - selection, reduction, elimination, decision - judger, bias, ranker, logic, "game master"
		//    ...
		//    1 - reality, fact, action, implemented.
		//    example:
		//        10 - cool
		//        ...
		//        5 - icy
		//        ...
		//        3 - cold, hard, translucent, blue or white, crunchy,
		//        2 - a melting piece of ice that is drawn center on screen
		//        1 - pixels are drawn according to a defined shape, and plays a defined sound.
	},

	Manipulability: {
		// TODO: check if synonymous with reversibility

		// keep track of the manipulability/changeability of parameter
		//	What actions of the ones I have can affect the parameter?
		//	which ones of the actions can best reach where I want and at what cost?
		//	How finely can I tune the parameter with the available.
		//	How well can I tune this parameter without changing other parameters?
		//	Can I tune this back and forth indefinitely or is it irreversible?
		//
		//^ basically dynamically build a model of how well you can play plock-i-pin
		//	and keep track of how critical the side effect changing parameters are
		//		how tight does the side-affected parameter's values need to be fixed?
		//			how much does their value depend on a lot of other values?
		//
		// whether or not you can manipulate a parameter
	},




	domain: {
		// Domain meaning: an area of territory owned or controlled by a particular ruler or government.
		// Domain is determined by relationality of an object.

		// Things align with conditions over time. and when aligned conditions have predictable/identifiable ranges of effects/transformations.
		// in domains specific rulers/rules govern the consistency and transformation things.
	},



	IsPossible: 1, // Whether something is allowed to be done without contradiction.



	condition: { //criteria: { //state: {
		// Anything that is defined, is defined by its condition.
		// The condition is What it holds, manifests or relates to.
		// being(is),having(has), or relating to.

		// Everything is in a condition. every thing has a condition.
		// every condition is a relation

		// is blue means relates blue to eyes. 
		// has means something like A is in a "has"ing relationship to B.
		// aka all conditions are relations...

		// a relation is how it associates.

		// What state something is in.
		// How something is in comparison to similar things.
		// its nature

		// A condition is an area in a multi-parametric space represented as a recognizable concept. "Oh that is a fish bowl"
		// A condition is a parametrically defined boundary

		// You usually "check" a conditions with if statements containing logical comparative expressions.
		// You figure out its nature by checking parameters. "is it round?" "is it cool" "is it baroque?" etc

		// You ever only need to check if something get into a condition by coincidence. because the architect of the object already knows its nature. (because it defined its parameters.)
	},
	criteria: { // requirement in condition
		is: {
			requiring: { //require = depends on
				condition: 1,
			}
		}
	},








	// Logic
	inference: {
		// Under what conditions can what assumptions be made?
		// I do not know how to create this syntxt
		//    has: ["limb", "body"] = { 
		//        assume: {
		//            can: {
		//                move: 1
		//            }
		//        }
		//    }
	},









	// Processes, formulas and reactions
	// Process and change theory
	actionExample: {
		precondition: {
			// Example: is human, has hp more than 100, is level between 5 and 7, 
			// ^ this is a bit misleading... What is an actual purpose of an action?
			// It is to nudge the algorithm in an allowed fashion towards a stated goal.
			// So I guess a basic contract is based on exclusively indirectly affectable variables.
			// So how do I set up these values, like for example: computer performance, or player satisfaction?
			// Basically define a way to check the KPI without any way of directly manipulating the KPIs.
		},
		postcondition: {
			// Can be defined in new state ("du e mammae") or in changes/affection ("you lost 20 health")
			// This can contain relative or absolute values. For example:
			// Player health is 1. player arms are 20% bigger. Height increase by 5 cm (probably pixels though..)
			// Maybe even ranges... like player health is anywhere/somewhere between 20 and 40.
		},
	},

	reaction: {
		// Happening/changing/manifesting out of condition.
		has: {
			change: 1, //types of change: absolute: "du e mammae" relative: "you lost 20 health)
			condition: 1
		}
		// Something that changes (in a constrained sort of way) when something else changes...
		// aka condition : effect
	},
	parallelReaction: { // basically chemistry, use for inputs
		// when a set of criteria is fulfillled, generate reactiton.
		// A is true and B is true and C is true ....
		//	then execute this process/code.
		// multiple criteria aligned to cause one (or more) output.
		// (outputs can also be parallel or sequential...)
		// and it is important to differentiate between simulated parallelism and real code parallelism...
		// aka alchemy lol
	},
	sequentialReaction: {
		// First A then B, then C, Then, E -> causes specific reaction/output
		// use this for inputs like Like cheat codes in games
		//	or like combos in fighting games.
	},


	// About processes and function/purpose
	// Fundamental dimension of... events? //themes?
	constructiveVsDestructive: {
		// Something constructed is built togther as a whole.
		// Something destructed is taken apart from a whole.
		// It is a measure of change in structuredness. Organization?
	},

	// keywords?:
	// The game loop, chemistry, processes, cause and effect, simulation, the performer, core decision, transformations, gradual transformations, emergence.
	// SOLUTION: effect, emergence, performance, decision, transformation, reaction

	//	TODO: bring together process with sequence and time
	process: { //procedure:
		// gradual change as reaction to internal (itself) or external condition, according to rules or formula.
		// conversion rate.

		// A process causes alteration.

		// processes that are unsolved, continue over time (through multiple update calls)
		// whereas processes which conditions are no longer met are solved and thus stopped.

		// process kinda implies graduality (opposite of discreteness) in change. steps, gradients.

		is: {
			// process is the change of something over time
			process: 1
		},
		has: {
			rate: 1, //rate of change...
		},
		can: {
			// Processes can continue
			// Processes can stop.
			// processes can change over time.
		},

		// final implementation, something like:
		// function process(dt){ //value transformation
		//	effect += effectRate * dt
		// }
	},
	alteration: {
		// A General principle of alteration, don't change what is working. Change as little as possible. (the player can only comprehend certain amount/rate of change.)
	},
	causeAndEffect: {
		// Conditions/criteria => constraints/restrictions
		// Cause and effect
		//    causes
		//    causedBy
		//    direction
		//        to - toward
		//        from
		//    causes (insert reaction type)
		//        this can be pure reaction or even behaviours if that makes more sense.
		//        hmm yeah maybe I should have a bias towards the easiest? Like Ockhams Razor. Or maybe that will just cause the most straightforward boring things to happen?
		//     Never seen / occurring together with
		//    with
		//    without
		// TODO: add to enough to satisficing...
		//    enough - gränsland/condition suggesting emergence. cause and effect
		//        Enough suggests a satisfying bottom level.
		//        "when hot enough it burns."
		//        "When temperature is cold enough everything in existence freezes"
		//        "When a thing is cold enough it freeze
		//    too - more than should
		//        "When it is too hot, it dies."
		//    Too is a good word. When too...
		//    dependence and correlations, attractions, incompatibilities, paradoxes.
	},
	reaction: {
		// a process of change over time
	},

	rate: {
		// a measure, quantity, or frequency, typically one measured against another quantity or measure.
		// 5 myror, 4 elefanter
	},

	formula: {
		// a function which gives you a result from a bunch of input parameters
		// a mathematical relationship or rule expressed in symbols.
		// e = mc^2
		// often used when calculating physical phenomena and time etc.
	},
	leftoverChemistryNotes: {
		// something needs to tell the direction of the desires and goals
		// Problems:
		//	something needs to measure where and when a reaction starts in the runtime //aka when a defining condition is met
		//	Something needs to continue all active reactions
		//	Something needs to measure where and when a reaction ends

		// One way to make reactions is to check like chains of effects. x changed, area depends on where x is, area related to collision. collision causes.......


		// approximation functions. convenient heuristics. used to get result by being good enough for the situation.
		// in the end this whole software is just a good heuristic for making games.
		//___________________________________________________

		// implementation concerns:
		// cool ideas:
		// Emergence dictionary
		// can you encode a transformation or a function that encapsulates multiple correlated transformations?
		// like for example both gravity and collision checking?
		// Like compiling it to one thing, one check. one pattern transformation?
		// make it into one frame in Conway's Game of Life.
		// make it one iteration of l-system.
		// one pattern text matching and replacing operation. 

		// problems with emergence:
		// How do I write up emergences on a subscription?? Entity component system something something.


		// when a process stops causing a reaction. Then you have usually entered a state (something in stasis).
		// when a process stops reacting it has reached a stable
		// one time occurrences can be done by transforming the object out of the condition. example: burnable -> burnt.


		// For sake of consistency I could make everything a reaction phenomena.
		// Player Character is a condition that appears during bla bla, its effect is how it manifests etc.
		// A rock is a condition th.... but I also want specific instances of things though, then I can not just make rock a condition?

		// possible collisions and proximity effects. an array which is continually updated that checks for what to check
		// ^ I really do not know what I was talking about here.


		// SOLUTION: criteria, condition, pattern, trigger, cause
		// Something needs to state what conditions creates a reaction
		// how can you define conditions ?

		// Check every object/condition for whether or not it contains any one of all the existing conditions.
		// OPTIMIZATION: ^ Except I suppose the ones that already has checked the boxes of being on fire or whatever I am looking for.
		// this seems like an extremely costly process... But maybe I should leave attempts of optimization for later lol.
	},

	dynamic: {},






	// abstractionszzz
	abstraction: {

		//iconicRepresentation, communicating the idea with a composition of simple elements. A symbol, a sigil, representant of a type in place of any instance. 

		// The following is reasoning by analogy. What it is like, not what defines it: or it is rather extrapolations from abstraction.

		//numbers is the abstract representation of quantity, of repetitions.

		// Often measured in terms of number of instances

		// abstraction is when we skip the details and focus on the most prominent similarities in aspects between objects.

		// When it becomes abstract it is often a selection between extremes
		// With abstraction usually continuous things are turned into discrete archetypal states/extremes in abstraction
		// You generally want to abstract away what is uninteresting and boring to the player.
		// Abstract the most boring uninteresting and wasteful things out of the game. to save performance for the game and time for the player

		// Discretion is a form of abstraction

		// Video game trope
		//	Action in abstract (abstract action)
		//		Play an animation, do a loading bar. Instead of stimulating a real physical interaction.
		//		This is convenient and saves programming time
		//		In reality we do not need to think about the actions we do as habits. This is essentially what this trope stimulates
		//		If the action is not memorable you might as well use this trope to save performance
		//	Abstract Time


		//the cost of replicating reality conceptually is higher than replicating 
		//	abstract concepts like boxes, red and triangle

	},
	functionalistAbstraction: { //functionalist abstraction
		// a type of discrete abstraction

		// concepts as defined purely by archetypal function.

		//	aka a saw is for making wood
		//		so you don't have to worry about secondary effects like real like
		//		saw can only interact with wood. and not cut rocks or animals or anythign
	},
	abstractVsWorldy: {
		// A scale from abstract concepts like math and basic shapes
		// To wordly stuff like nature and natural worldly phenomena.
		// I suppose symbolism is somewhere in the middle... bridging the gap sorta.

		//the cost of replicating reality conceptually is higher than replicating 
		//	abstract concepts like boxes, red and triangle
	},

	continuousVsDiscrete: { //continuous vs incremental //analog vs digital discretion:
		// Example: is the weather continuous or discrete?
		// Will it fade between weathers? will it go up and down in smooth curves.
		// Or will it just decide. "Now it is rain weather.

		// Discretion is a form of abstraction putting things into even units of distance/difference.
	},



	simplicityComplexityScale: { //simplicityVsComplexity: 1, //minimalism vs CHAOS?
		// Simplicity is the opposite of complexity
		// Complexity:
		elementCount: 1, //	number of elements
		elementTypeCount: 1, //	Types of elements
		elementConnectionCount: 1 //	number of connections
		//	non-linearity of connections
		//	autonomy of elements.

		//		simplicity:{
		//			count: "low",
		//			//like minimalism?
		//		},


		// a complex visual has:
		//	many in each category
		//	many categories:
		//		shapes
		//		textures
		//		colors
		//		styles
		// diversity: //scale of difference in quality. spanning many categories.m
	},





	// dimesions...
	// disperse vs merge
	fairness: { //vs unfairness
		// when things are unfair for the player it is often experienced as frustrating.
	},




	// social
	agreementVsConflict: {
		// unifying will vs differentiate will	
	},
	nurtureVsBattle: {
		// Cooperate, work as whole.
		// incompatible - so different in nature as to be incapable of coexisting.
		// separation?
		// contend, compete, conflict, fight for domination/direction
	},


	// human reaction
	acceptVsResist: {

	},

	healthyVsWicked: {
		// natural
		// corrupted
	},

	tastyVsDisgusting: { // stimulating disgust, vs stimulating opposite of disgust. //TODO add one of these for every... emotioN? need?
		// Delicious, tasty, tasteful
	},









	subvertingExpectaions: {
		// Makes you learn
		// Positively - feels good
		// Negatively - feels bad
	},




	// The crafting of human experiences:
	// Psychology, learning and human player guiding
	classicalConditioning: {
		// A method of influencing how people react to a particular thing by repeatedly presenting it with other things they either like or dislike. 
		// • The influence occurs at an unconscious level, and results in a physical or emotional reaction. 
		// • Classical conditioning is commonly employed in animal training, behavior modification, and advertising. 
		// • For example, presenting images of attractive people with a product creates positive feelings for that product. Conversely, presenting disturbing images of extreme violence or injury with a product creates negative feelings for that product. 
		// • The stronger the reaction to a thing, the easier it will be to generalize that reaction to other things. 
		// • Use classical conditioning to influence emotional appeal. Pair designs with appropriate stimuli to promote positive or negative associations. 
		// See Also Aesthetic-Usability Effect • Mere-Exposure Effect Operant Conditioning • Shaping

		// This poster uses before and after images of a drunk driving victim to condition negative feelings about drunk driving.
	},


	// biases?
	cognitiveDissonance: { //psychology and human bias
		// We default to either or thinking based on our own mentally constructed archetypes and models and heuristics.
		// A state of mental discomfort due to incompatible attitudes, thoughts, and beliefs. 
		// • Cognitive dissonance is a state of mental stress due to conflicting thoughts or values. It also occurs when new information conflicts with existing thoughts or values. 
		// • When a person is in a state of cognitive dissonance, they seek to make their incompatible thoughts or values compatible with one another. 
		// • For example, diamond sellers urge consumers to demonstrate their love by buying diamonds, creating cognitive dissonance in consumers—i.e., dissonance between the love that they have for another, and the pressure to prove that love by buying diamonds. 
		// • Consider cognitive dissonance in persuasion contexts. Alleviate cognitive dissonance by reducing the importance of conflicting thoughts, adding new confirmatory thoughts, or changing the conflicting thoughts to appear compatible. 
		// See Also Expectation Effects • Framing • Sunk Cost Effect

		// Benjamin Franklin was a formidable social engineer. He once asked a rival legislator to lend him a rare book, which he did. The rival greatly disliked Franklin, but had done him this favor. How to alleviate the conflict? The two became lifelong friends. 
	},

	framing: { //psychology and experience. storytelling??
		// A method of presenting choices in specific ways to influence decision making and judgment. 
		// • Framing is the structuring of words and images to influence how people think and feel about something, typically to influence a decision or judgment. 
		// • For example, a frozen yogurt that is advertised to be “95% fat-free” elicits a positive emotional response, whereas a frozen yogurt advertised to be “5% fat” elicits a negative emotional response, even though the two statements are logically equivalent. 
		// • Frames that emphasize benefits are most effective for audiences focused on aspiration and pleasure seeking. Frames that emphasize losses are most effective for audiences focused on security and pain avoidance. 
		// • Consider framing to influence decision making and judgment. Use the appropriate type of framing for an audience, ensuring that frames do not conflict. 
		// See Also Expectation Effects • Mere-Exposure Effect

		// The Ohio Dry Campaign of 1918 is a case study in framing: Are you for the defenders of freedom, you for the booze? 
	},
	nudge: {
		// A method of modifying behavior without restricting options or changing incentives. 
		// • Nudges use the following methods to modify behavior: smart defaults, clear feedback, aligned incentives, structured choices, and visible goals. 
		// • Smart Defaults: Select defaults that do the least harm and most good versus the most conservative defaults. 
		// • Clear Feedback: Provide clear, visible, and immediate feedback for actions. 
		// • Aligned Incentives: Align incentives to preferred behaviors, avoiding incentive conflicts. 
		// • Structured Choices: Provide the means to simplify and filter complexity to facilitate decision making. 
		// • Visible Goals: Make simple performance measures clearly visible so that people can immediately assess their performance against goals. 
		// See Also Constraint • Framing • Gamification • Hick’s Law

		// Why￼ does etching an image of a fly into urinals reduce spillage by 80 percent? When people see a they try to hit it. 
	},
	operantConditioning: {
		// Using rewards and punishments to modify behavior. 
		// • A technique used to increase behavior by following it with rewards, or decrease behavior by following it with punishment. Rewards and punishments are most effective when administered right after a behavior. 
		// • Commonly used in animal training, behavior modification, and incentive systems. 
		// • Over-rewarding can undermine intrinsic motivation. For example, lavishly rewarding people to perform tasks they enjoy will diminish their enjoyment of those tasks. 
		// • Predictable reward schedules result in low performance that extinGUIshes quickly if the rewards stop.
		// Less predictable reward schedules result in high performance that is resistant to extinction if rewards stop. 
		// • Consider operant conditioning to change behaviors. Avoid over-rewarding behaviors when intrinsic motivation exists. Favor unpredictable over predictable reward schedules for peak performance. 
		// See Also Classical Conditioning • Gamification • Shaping

		// Whether a lever-pressing rat or a slot-playing human, rewards delivered right after the behavior
		// it addictive by design. 
	},
	Priming: {
		// Activating specific concepts in memory to influence subsequent thoughts and behaviors. 
		// • Priming is the intentional activation of specific memories in an audience for the purposes of influence. 
		// • When we perceive the outside world, associated memories are automatically activated. Once activated, these memories can influence subsequent thoughts, emotions, and behaviors. 
		// • Priming will not induce people to act against their values, but it can increase the probability of people of engaging in behaviors consistent with their values. 
		// • Consider priming in all aspects of design. First impressions and antecedent events are opportunities to influence subsequent reactions and behaviors. 
		// See Also Expectation Effects • Framing • Nudge Red Effects • Serial-Position Effects 

		// A poster that primes being watched versus a generic poster can significantly reduce malfeasant behavior. 
	},
	// Psychology
	Shaping: {
		// Training a target behavior by reinforcing successive approximations of that behavior. 
		// • First studied by the psychologist B.F. Skinner, a pioneer in behavior modification. 
		// • Shaping involves breaking down a complex behavior into a chain of simple behaviors, which are trained one by one until the complex behavior is achieved. 
		// • Positive reinforcement is provided as an observed behavior increasingly approximates a target behavior. 
		// • Shaping results in the development of superstitious behaviors when irrelevant behaviors are accidentally reinforced during training. 
		// • Use shaping to train complex behaviors in games and learning environments, teach rote procedures, and refine complex motor tasks. 
		// See Also Classical Conditioning • Operant Conditioning

		// Duringpigeon-guided￼
		// WWII, B.F. Skinner used shaping to train pigeons to peck at aerial photographs, creating
		// bombs. 
	},


	// driving stimuli
	interactibilitySignals: {
		//hand and fingers.
		//"press [button]"
		// treats. avoidance desire. Only works if you already believe interactivity is an option.
		// Doors, doorknobs.
	},
	attractingSignals: {
		//brightness and highlights
		// glimmer and shine(goldness...)
		// openings and ability toe go beyond obstructions of vision..
	},






	// Perception and discernment?
	gutenbergDiagram: {
		// A diagram that describes the pattern followed by the eyes when looking at a page of information. 
		// • Western cultures read displays from top-left to bottom-right, sweeping from left to right as they progress. The tendency to follow this path is described as “reading gravity”. 
		// • Layouts that follow the diagram and work in harmony with reading gravity purportedly improve reading rhythm and comprehension, though there is little empirical evidence to support this. 
		// • The Gutenberg diagram is only predictive of eye movement for homogeneous displays of information. 
		// • Consider the Gutenberg diagram in layout and composition when elements are evenly distributed. Otherwise, use the weight and composition of elements to lead the eye. 
		// See Also Alignment • Entry Point • Progressive Disclosure Readability • Serial Position Effects

		// Compositionsbottom-right.￼
		// that follow the Gutenberg diagram position key elements diagonally from top-left to
	},



	Mapping: { //user interface design
		// A correspondence in layout and movement between controls and the things they control. 
		// • Good mapping occurs when there is a strong correspondence in layout and movement between controls and the things they control. Good mapping makes things intuitive to use. 
		// • Bad mapping occurs when there is a weak correspondence in layout and movement between controls and the things they control. Bad mapping makes things counterintuitive to use. 
		// • For example, pushing a switch up to move a car window up is good mapping. Pushing a switch up to move a car window down is bad mapping. 
		// • Ensure good mapping in your designs to minimize errors and make things easy to use. 
		// See Also Affordance • Constraint • Nudge • Proximity Similarity • Visibility

		// Which stove control turns on which burner? Bad mapping (top) makes it unclear. Good mapping (bottom) makes it intuitive.
	},
	// Human shape pattern recognition
	orientationSensitivity: {
		// Certain line orientations are more quickly and easily processed and discriminated than others. 
		// • People can accurately judge vertical and horizontal lines, but have problems judging diagonal lines. 
		// • Lines differing in orientation by more than 30 degrees from a background of lines are easy to detect, and differing by less than 30 degrees difficult to detect. 
		// • Compositions where the primary elements have vertical or horizontal orientations are considered more aesthetic than diagonal orientations. 
		// • Consider orientation sensitivity in compositions requiring discrimination between lines. Facilitate discrimination between lines by making their orientation differ by more than 30 degrees. Use horizontal and vertical lines as visual anchors to enhance aesthetics and maximize discrimination.  See Also Alignment • Figure-Ground • Good Continuation Highlighting • Signal-to-Noise Ratio 

		// Harry Beck’s classic London Tube map is both aesthetically pleasing and easy to read because railway lines are only represented in vertical, horizontal, and 45-degree orientations. 
	},

	// Human bias and Beauty
	LawOfPragnanz: { //LawOfPrägnanz
		// We tend to see things as their generalization. We see people instead of body parts. etc.
		// We see things for the chunks they resemble
		// we see isolated elements as part of a composition.
		is: {
			bias: 1, //humanBias?
		},
		// A tendency to interpret images as simple and complete versus complex and incomplete. 
		// • One of the Gestalt principles of perception. 
		// • When people are presented with a set of elements that can be interpreted in different ways, they tend to assume the simplest interpretation. 
		// • For example, the characters :-) are interpreted as a smiling face versus three separate characters. 
		// • The law applies similarly to the way in which images are recalled from memory. For example, people recall the positions of countries on maps as more aligned and symmetrical than they are in reality. 
		// • Generally, people are better able to interpret and remember simple figures than complex figures. 
		// • Consider the Law of Prägnanz when depicting and interpreting images. Favor simplicity and symmetry to facilitate visual processing, and favor complexity and asymmetry to impede visual processing. 
		// See Also Closure • Depth of Processing • Good Continuation

		// Dazzle camouflage prevents simple interpretations of boat type and orientation, making it a
		// difficult target for submarines. 
	},

	uniformConnectedness: { //mostly relevant for abstract symbol drawing spaces and user interfaces
		// Things connected by lines or boxes are perceived to be more related than things not connected. 
		// • One of the Gestalt principles of perception. 
		// • There are two strategies for applying uniform connectedness: connecting lines and common regions. 
		// • Connecting lines touch two or more elements. 
		// • Common regions enclose two or more elements with boxes or shaded areas. 
		// • Uniform connectedness is the strongest grouping principle, and will overpower other grouping effects. 
		// • Consider this principle to correct poorly grouped control and display configurations. Use common regions to group text and clusters of control elements. Use connecting lines to group individual elements and imply sequence. 
		// See Also Closure • Common Fate • Figure-Ground Good Continuation • Proximity • Similarity 

		// Uniform connectedness is a means of grouping elements and overriding competing cues like proximity and similarity. 
	},


	visuospatialResonance: {
		// A phenomenon in which different images are visible at different distances. 
		// • Images rendered at a high spatial frequency appear as sharp outlines with little between-edge detail.
		// High-spatial-frequency images are easily interpreted up close, but are not visible from a distance. 
		// • Images rendered at a low spatial frequency appear as blurry images with little edge detail. Low-spatial frequency images are not visible up close, but are easily interpreted from a distance. 
		// • When images rendered at different spatial frequencies are combined, the result is visuospatial resonance. The effect can be stunning. 
		// • Consider visuospatial resonance as a means of increasing the interestingness of posters and billboards, and masking sensitive information. 
		// See Also Figure-Ground Relationship • Law of Prägnanz

		// A hybrid image of two people at different spatial frequencies. Up close: Albert Einstein. Far away:	Marilyn Monroe.
	},






	familiarityVsNovelty: { //unfamiliarity vs familiarity,  
		// emergence:

		// emergence can manifest from Unaligned cyclical phenomena //unaligned rhythms. 
		// each element/parameter shifting at its own pace, so that each moment is a unique configuration.
		//
		// emergence comes from unalignment of signals
		// interference.

		// does extreme polyrhytms aproximate chaos?
	}, //of patterns...









	levelOfDetail: {
		// a measure of how much detail something has
		// small details/patterns can not be seen from far away
	},









	// Meta modern narrative
	// I very much like the idea of explaining why the player character is not acting according to the history of that character narratively.
	// excuses:
	//	The character is just born into a new world. An open slate and therefore has no history and is guided by impulses (from the player)
	//		this can be a robot or organic being.
	//	the character influenced by an external and hidden force ( implying that the player is an external entity that is controlling.)
	//		The player is really a demon, ghost, hacker, dimensional being or otherwise hidden character (hidden history)
	//			you control/influence the "player character" through magic, supernatural forces, hacking, or just by simply telling them what to do through text or voice.









	virtues: {
		//or representation of the virtuous.
		//friendship, kindness, honesty, laughter, generosity, loyalty, magic (and all the other ponies )trust?
	},

	love: {
		// love, that which creates order from nothing

	},
	// human pattern/repetition preference
	beauty: {
		// beauty is the general human bias of what is considered attractive and valuable
		is: {
			// pleasing the senses or mind aesthetically. especially because of very high standard.
			// that which represents virtue. The presentation of virtue?
			// What is truly beautiful is what is ultimately and universally desirable.


			// is pleasing to senses and attracts you towards virtue or the understanding of it's aspects.

		},

		secondaryImplications: { // Just too much rambling notes that basically say the same thing with different words
			// 	A combination of properties/qualities, such as shape, colour, or form, that "pleases"(pleasure) the "aesthetic senses" of humans, especially the sight.
			// 	Biases in human perception that generally fulfilll human needs

			// beauty is that which is alluring, attractive, a bit mysterious and shares properties with pleasurable things. It is patterns which our brains can understand. and some extraordinary sophistication. It is whole or incomplete in a self-aware way.

			// That which appeals to one's sense of taste.

			// It has objective aspects, ones that are shared with all people. 	// certain things are beautiful to everyone. (there are things all humans inherently find beautiful.... because of our shared composition.)
			// It has subjective aspects, pleasures that are instantiated diferently in different subjects.

			// beauty is the speaking of a subjective liking as if it was universal.
			// Everyone has his own sense of taste. The case of "beauty" is different from mere "agreeableness" because, "If he proclaims something to be beautiful, then he requires the same liking from others; he then judges not just for himself but for everyone, and speaks of beauty as if it were a property of things."




			// that which gives pleasure to the senses or pleasurably exalts the mind or spirit 
			// the quality of being pleasing, especially to look at, or someone or something that gives great pleasure, especially when you look at it:
			// a quality that makes something especially good or attractive:
			// an attractive quality that gives pleasure to those who experience it or think about it, or a person who has this attractive quality:


			// a particularly graceful, ornamental, excellence, quality


			// some wiki person's perspective about beauty:
			// snippet from wiki about aesthetics:
			// enjoyment" is the result when pleasure arises from sensation, but judging something to be "beautiful" has a third requirement:
			//		sensation must give rise to pleasure by engaging reflective contemplation. Judgments of beauty are sensory, emotional and intellectual all at once


			// beauty:
			//	costly signalling -there is beauty in making visual a heavy investment into something.
			//		the most costly thing is to produce order from chaos.
			//	impossible structures (illusions and things giving impression of something that can't happen in our world)
			//	Seemingly controlled or done with intention. - magic, control over reality. mastering the elements. It is a big dream of human kind.



			// "death is the mother of beauty"

			// The fact that things end (change) is what gives them value. they are delicate. beauty is fleeting.
			// The mind is delicate and is easily broken if handled without care.
			// The mind can break when seeing god the same way eyes can break when staring at the sun.
			// (so god is just drugs and too much drugs break your brain.)

			// good summary: values, goals, aesthetics, spirituality, virtue, love, care, compassion

			// other idea: true beauty is a minimal modification to nature to achieve your goals
			//	true beauty is just to use minimal modification of nature to achieve your goals.
			//		(true functionalism and resourcefulness)
			//
			//
			//	extreme geometry (reflecting
			//
			//		maybe it is just a solar sail that has been gaining speed for a very long time.
			//			because there is no resistance in space.
			//				it is all about bouncing light efficiently
			//
			//				you could even have like a thin material that bounces light in particular ways
			//					and shots of the light in particular directions.

			// beautiful:

			// beauty has two properties/value: aesthetics and taste
			// Aesthetics is the philosophical notion of beauty.
			// Taste is a result of an education process and awareness of elite cultural values learned through exposure to mass culture (pop culture).



			// we feel through expressions of human faces and body language...

			// Aesthetic judgments may be linked to emotions or, like emotions, partially embodied in physical reactions.
			// For example, the awe inspired by a sublime landscape might physically manifest with an increased heart-rate or pupil dilation; physiological reaction may express or even cause the initial awe.

			// emotions are conformed to 'cultural' reactions, therefore aesthetics is always characterized by 'regional responses', 

			// Evaluations of beauty may well be linked to desirability, perhaps even to sexual desirability. Thus, judgments of aesthetic value can become linked to judgments of economic, political, or moral value.
			// In a current context, a Lamborghini might be judged to be beautiful partly because it is desirable as a status symbol, or it may be judged to be repulsive partly because it signifies over-consumption and offends political or moral values.

			// The context of its presentation also affects the perception of artwork; artworks presented in a classical museum context are liked more and rated more interesting than when presented in a sterile laboratory context.

			// context/story is more important than whether or not an art piece is by the original artist or a fake one.

			// suggestions of beauty:
			// "this is deep and symbolical" "Everything that happens has a purpose and is intentional"


			// 1. sophistication: Technical skills. showing impressive techniques.
			// 2. Non-utilitarian pleasure. It is enjoyed for its own sake and not to fulfilll a basic need.
			// 3. Style. Artistic objects and performances satisfy rules of composition that place them in a recognizable style.
			// 4. Criticism. People make a point of judging, appreciating, and interpreting works of art.


			// aesthetics is the properties which reminds us of useful or practical things but are extrapolated away from its original context.

			// What a thing means or symbolize is often what of it is being judged.

			// aesthetic judgments might be seen to be based on the senses, emotions, intellectual opinions, will, desires, culture, preferences, values, subconscious behaviour, conscious decision, training, instinct, sociological institutions, or some complex combination of these
			// beauty is in the eye of the beholder. It is subjective. It is what you find virtuous and what fulfilll your needs.

			// beauty is local. focused on an artistic idea and artistic expression. 

			top10beautifulthings: {
				women: 1,
				landscapes: 1,
				//healthiness. of life and ecosystem.
			},

			// some quote: "Simply expressed, this means that before beauty can appear in it any work of art, whether it be a picture, a chair, or a furnished room, must consist of many parts; which parts, however numerous or diverse, must be so combined that they appear to concur in forming one whole. That is, they must present themselves to the mind as a unit, with a single aim, design and purpose. No bare room, no room which lacks a diversity of lines, shapes, colors and textures, of lights and shadows, of plain and ornamented surfaces, can be beautiful. Nor can any room be beautiful which, possessing this diversity, fails to fuse it into an essential unity. Conversely, no room so decorated that it reveals a stimulating degree of diversity, while at the same time its unity is perceptible instantly and without effort, can be wholly lacking in beauty."


			// The world is imperfect, and that is the beauty of asymmetry.

			// Incompleteness is beautiful. We like to fill in incomplete patterns, it lets human imagination free. it creates mystery. drives curiosity.

			//beauty is supposedly personality independent and is thus one of the core major goals to achieve (perceived beauty)
			is: {
				//perceived as beautiful be people.
				sensation: 1,
				patternsInherentInNature: { //beauty is patterns inherent in nature
					//		because human evolution makes us like natural shapes which we are optimized to interact with.
					//		essentially symbols of food, weather, water, danger, hurt, sex, survival
					//			explosion, glow, obstruction, 
					//		beauty comes from pattern recognition.
					//		human shapes, natural shapes of nature and environments.
				},

				maslowsHierarchyOfNeeds: 1, //beauty is related to Maslow's Hierarchy. things that fulfilll/pleases needs are beautiful. 
				evolutionaryTriggers: {
					cleanliness: 1, //hygiene in maslow
					violence: {
						destruction: 1,
						chaos: 1,
					},
					sex: 1,
					emotion: 1,
				},

				//It is beautiful when you can see something without getting triggered by it. To see it from a new perspective, a new light.


				carefulPlanning: { //or considerations.... study show legit artworks has a more carefully planned pattern/symmetry 
					//		which is not seen in paintings by children or monkeys 
					//		and people can recognize this difference.
					//thoughtfulness... consideration.

				},
				symbolical: 1, //filled with meaningful symbols used in the right context.


				virtues: 1, //how we ought to be... on top of the Maslows?

				meaning: { //purpose?
					//people keep focusing on details or ornaments of architecture (humans do not like monotony!!! )
					//	and brushing quickly over blank walls.
					//		and blank walls has proven to make us miserable.
					//		vast dull facades makes us feel bored and uncomfortable
					//			this increases heart rate and stress levels.
					//		the opposite is true for aesthetically pleasing things
					//			improves human wellbeing, behaviour, cognitive function and mood.
					//	newly renovated vard required less pain medication and patients recovered 2 days earlier on average. 
					//individual happiness is affected by how beautiful you find the city/environment you live in. (even more important that cleanliness or safety)	

					// meaning of meaning
					// substance - predictable kind/form of matter. real, tangible.
					// intend - strive a course of actions. plan for purpose.
					// mean - direct towards or move to a substance
				},


				//in terms of reaction
				memorable: 1, //impressive//beauty is also about what is most impressive to us. //humans are biased to remember what is important to fulfilll needs. aka what is beautiful.
				stunning: 1, //The player willingly stops to watch

				//	how much of an improvement it is.

				unpresedented: 1, //	things are stunning if they are unprecedented.
				//uncommon things for the real world. Like Seeing something incredible being destroyed. or something very broken being put back to pieces.
				novelty: 1,
				extraordinarity: 1,

				natural: 1, //beauty also lies in the recognitions of its incorporated patterns. it just makes sense and aligns with the tools your brain has to analyze it with.
				//we like nature because it is our natural environment
				pedagogical: 1,
				goodness: 1, //	association with good things //	dissociation with bad things

				//	https://www.youtube.com/watch?v=-O5kNPlUV7w	
				archetypal: {
					//it speaks through associations between archetypes to generate its own archetype.
					//frequency or dominance of archetypes 
				},
				coherence: 1, //zeitgeist: 1, //tidsanda (so... contextual coherence?)
				Sensual: 1, //through our senses //real beauty involves many senses. what engulfs many senses is real.
				asymmetryVsSymmetry: 1,

				simplicity: 1,


				balance: 1,
				harmony: 1,
				clarity: 1, //pure and understandable
				secrets: 1,
				hiddenOrder: 1, //hidden patterns, non obvious coherence, seeing meaningful patterns in noise makes us feel smart and in control. Speaks of a great and perceptive mind. 
				revalation: 1,
				// rule based coherence
				// self coherence: I mean like fractals, it contains itself on all levels
				// like nature

				controlledChaos: 1, //power. To create desired effects with specified form of destruction...

				//silence, room, waiting, emptiness, ma

				geometryBased: 1, //(platonic solids)

				petical: 1, //poetry, poetic
			},
		},

		// J B Peterson on beauty:
		//	beauty is the call that urge us to imitate the best of nature
		//	beauty is more serious than anything, other than perhaps truth.
		//
		//
		//	modern people don't have imagination and commitment to beauty.
		//	beauty = There is something beyond you. it invites you to come along.
		//		beyond the grasp and understanding of the subject.
		//	it is something like the most beautiful woman in the world on the dance floor inviting you
		//
		//	She rejects you because you are not all that you could be.
		//
		//	beauty is invitation when you are ready.


		//Things to teach from beauty:
		//	teach player that that beauty can come from simple rules: Noita, sand sand sand, BOTW chemistry, game of life, 
	},
	pleasantRepetition: { //TODO: rename to something comprehensible... //The repeating for pedagogical/teaching purposes.
		// you need to repeat a pattern enough to get people on the wagon
		// but not too much as to spank a dead horse
	},
	horrorVacui: {
		// A tendency to fill blank spaces with things rather than leaving spaces empty. 
		// • A Latin expression meaning “fear of emptiness”. 
		// • Though the term has varied meanings dating back to Aristotle, today it is principally used to describe a style of art and design that leaves no empty space. 
		// • The style is commonly employed in commercial media such as newspapers, comic books, and websites. 
		// • As horror vacui increases, perceived value decreases. 
		// • For example, the number of products in retail windows tends to be inversely related to the average price of products and store brand prestige. 
		// • Consider horror vacui in the design of commercial displays and advertising. Favor minimalism to signal high-priced products. Favor horror vacui to signal low-priced products. 
		// See Also Inattentional Blindness • Left-Digit Effect Ockham’s Razor • Signal-to-Noise Ratio 

		// The perceived value of a store’s merchandise in a storefront window is inversely related to its horror vacui. 
	}, // perceived value (importance, significance) is correlated with the amount of empty space around it.




	// PATTERN
	// beauty
	rhythm: {
		// Repeating temporal pattern
		// Rhythm dictates the recurring or organized/disorganized distribution of visual elements throughout an image.
		// Like: ... ... ... or: .. . .. . .. .
		// Rhythm is a property of any pattern (I think)
		// It is a sort of pacing that repeats. not small enough to be tamber/texture. but big enough to repeat recognizably.

		// What can be thought to the player:
		//	The beauty of rhythmic intricacy
	},

	emphasis: { //aka highlighting (actually TODO: merge highlighting into emphasis)
		// Emphasis is a combination of contrast and imbalance. what is highlithed is the small thing (figure) sticking out from the big (background)

		// Hierarchy/Dominance/Emphasis
		// A good design contains elements that lead the reader through each element in order of its significance. The type and images should be expressed starting from most important to the least important. Dominance is created by contrasting size, positioning, color, style, or shape.The focal point should dominate the design with scale and contrast without sacrificing the unity of the whole
	},
	// Humans can filter out what they think is noise from perception, as long as they can discern the signal
	// The human internal noise filter used to understand things follows the gestalt rules



	movement: {
		// May refer to 3 things...
		// 1
		// The effect movement has on camera shutters. like motion blur, elongation, bending, etc.
		// It is about the movement of each frame or presented image.

		// 2
		// The patterns in which the eye's focus of the perceiver/player naturally moves across an image
		// Emphasis and highlight creates destinations
		// Lines creates visual highways.
		// Jagged lines create excitement, shifting the viewer’s gaze from one point to the next. Curved lines are more subtle. These reduce the speed at which a photograph is viewed.
		// ^TODO move to like "line language" or something
		// TODO: add to color: the human eye is more sensitive to certain colors over others.

		// 3
		// animation. picture frames of shapes that through the gestalt rules are seen coherent objects across individual frames.
	},







	// add to aesthetic or feeling
	secret: {
		//unpredictable
		//previously unknown
	},
	hidden: { //hide: hiding:	

	},
	revealing: {
		//uncover, find, see, sense, experience	
	},

	representation: {
		// Present with associations in a limited context 
		// Is the associations an audience find 
		// Is the associations the audience build in their head after consuming 
		// Viktigt För att lära ut saker som är 
	},









	soil: { //dirt: earth:
		is: {
			// main nutritional source of most plants
			// very decomposed organic matter
			//	black, gray, brown
			// can soak/hold water
			// is visibly darker when wet/moist
		}
	},



	movingLandscapes: {
		// maybe the elevation of the landscape changes on its own over time
		// or maybe a big chunk of it arises from the rest of the landscape and flies away or walks away.
	},
	parallaxPerfectAlignment: {
		// when the camera is just in the right spot the foreground and backgrounds align perfectly overlapping to create the impression and optical illusion of an whole object.
	},
	MicroWorld: {
		is: {
			// landscape trope
		},
		// (everything is zoomed in tiny like a Pikmin)
		// conventionally small things are big in comparison to the character.
		// (good reason might be performance)
	},
	field: { //like a field of grass
	},



	tropeRoomTypes: {
		diningHall: 1,
		kitchen: 1,
		bedroom: 1,
		office: 1,
		garage: 1,
		bathroom: 1,
		swimmingHall: 1,
		machineRoom: 1,
		basement: 1,
		storage: 1,
		attic: 1,
		library: 1,
		trophyRoom: 1,

	},
	room: {
		// An interior space that is protected from exterior space by a boundary.
		// Interior of a building???
		has: {
			interior: 1,
			individuality: 1, //uniqueness
			conventionally: {
				theme: 1,
				function: 1, // or purpose
			},
		},
		is: {
			place: 1,
			conventionally: {
				furnished: 1,
				oneOf: {
					tropeRoomTypes: 1,
				},
			},
		},
	},
	home: {
		// A living place that helps you sustain your needs.
		// your domain
		// the place you live.
	},
	furnished: {
		has: {
			furniture: {
				count: "1 or more", //TODO: decide this.. because I forgot if i use 1 or more or atLeast: 1, keyword....
				is: {
					//is placed in aesthetically pleasant and functional ways.
				}
			}
		}
	},









	character: {
		// A memorable being or agent that acts soveringly and autonomously with their own style and philosophy.
		is: { // what is a character
			// A memorable being. you look at it, you know it is a being and you go "ah that is mickey mouse" or "ah I have seen this guy before"
			being: 1, //pretty sure this is the case 99.9 times
			memorable: 1, // a good character is identifiable. (aka they stick out so much to their surroundings that you remember them.)
			conventionally: {
				humanoid: 1,
			},
			unique: 1,
		},

		// we like relatable characters

		// Different visual components coming together to create meaning
		// in a seemingly organized fashion around an autonomous entity who is free to act upon the world.

		// a character is a controlled conversion of resources to optimize an locally defined ideal.

		// you generally want components that overlap in symbolic meaning which enhances the point they are trying to make.
		// the nature of the symbolic meaning is further expressed an clarified by the behaviours of the character.

		// Characters are mainly inspiration material for how you can act and be, creating experiences and feelings for people.


		// Some players want deep characters with rich story. like Advance Wars, Majora's Mask
		//	Conceptual compression and representation.
		//  Characters that act autonomously.
		//	Main characters should have character growth.
		//	Side characters can become main characters...

		//		attention to detail of mannerisms makes characters feel real and alive

		has: {
			history: 1, // what is their history that formed how they are today.
			culture: 1,
			autonomy: 1, // "free will" - freedom to act upon the world. // often autonomy from other objects and beings in a world.

			personality: 1,
			//			richness (money),
			//			social status,//?
			skill: 1, //any character has a set of skills and can learn a set of skills
		},

		// Definition of deep character:
		// (A shallow character is one you learn everything there is to know about very quickly.)
		// (The deeper the character the more time is required to get to know them, and typically keeps surprising you as you get to know them.)
		// ^ Has to do with exploration of character.
		//

		// Deepness has to do with face value. What you see is what you get. Whereas deep things unravel.
		// Games is art that literally unravels. And not only that it unravels reactionary and systematically.
		//
		// Complex characters, on the other hand, have a tangled knot of motivations, and everything from how they present themselves to how they act and react in the course of the story can change, depending on which bit of knot things are bouncing off at any given moment. They can look like a mess, but under stress reveal a core of steel, or appear in control of themselves only to fall apart when things get tough. This makes complex characters much more useful to writers, in most cases, because they are more flexible – the writer can get many different reactions from one character by adjusting the circumstances.

		// A Complex character, also known as a Dynamic character or a Round character displays the following characteristics:
		// 1. He or she undergoes an important change as the plot unfolds.
		// 2. The changes he or she experiences occur because of his or her actions or experiences in the story.
		// 3. Changes in the character may be good or bad.
		// 4. The character is highly developed and complex, meaning they have a variety of traits and different sides to their personality.
		// 5. Some of their character traits may create conflict in the character.
		// 6. He or she displays strengths, weaknesses, and a full range of emotions.
		// 7. He or she has significant interactions with other characters.
		// 8. He or she advances the plot or develops a major theme in the text.
	},
	playerCharacter: {
		// A player character  

		// Avatar?

		// should:
		//	Be interesting to play.
		//	Grow as the game and narrative Progresses

		// A playable character is not necessarily a player character. Characters normally serve narrative purpose, being memorable, defined, with growth.
		// 	There are games where you can play characters where the characters has personality independent of the player's actions.
		// aka player controlled character...
		is: {
			character: 1,
			conventionally: {
				humanoid: 1,
			},
			otherwise: {},
			controlled: { //is a vessel for the player
				through: {
					interface: {
						by: {
							player: 1
						},
					},
				},
			},
		},
		does: {
			communicate: { //explaining the condition of the player interfacing with the game world
				condition: {
					of: {
						player: 1,
					}
				}
			}
		}
	},


	// Character design and human being traits

	characterDesign: {
		// add the character design stuff.... aka rewatch that video because you fucked up

		// Good silhouette + good palette = extremely recognizable character.
		// Recognizable character = clear silhouette, good color palette, Clear exaggeration (of personality)
		// A good color palette make a character recognizable from a rectangular swatches alone.

		// The masking effect. (by Scott Mcloud)
		// Characters with few features are easier to project ourselves onto.
		corePersonalCharacteristics: {
			// MasculinityVsFemininity
			// Pose
			// Posture
			// introversionVsExtraversion
		}
	},









	entryPoint: {},


	foregroundBackgroundSeparation: {
		// make foreground and background visually distinctive
		// Clear separation of foreground background
		// They play with foreground and background very right. It is super clear when you are in the foreground and jumping is a really good way to decide you want to go up on something in the background. (up on a rock for example)
		//
		// A thought to add to this is to have the possibility to use the space within structures. For example with a rock: you can run on top of rock, you can run beside rock and you can also have tunnels or empty space within rock. (think a bit like houses in MapleStory)

		// TODO: there should be a universal principle of design about this...

	},

	// Is the game about symbols or reality?
	// Is the reality about objects or beings?


	// intellectual need. need for novelty.

	playgroundofToysGame: {
		// Games as a world with unlimited toys.
		// It is an infinite playground where you get to choose toys play with relationality.
	},






	floaty: {
		// of movement, like floating on liquid
		// up and down back and forth
		// slow to accelerate and decelerate.

		// like waves and water.
		// like bubbles rising underwater
		// squiggly?

		// floaty controls means that input do not have immediate effect but a delayed one.
		//	for example accelerating, or just delayed because of a startup animation/
		// to avoid floaty movement the characters acceleration to the maximum is often done quickly and the same with deceleration after button has been released.
		// also when changing direction to opposite the effect should be immediate with no acceleration/deceleration in order to not be floaty.

		// The longer the delay between changing states the more floaty things are.
		// its floaty when what changes on screen is not as immediate as the changes in player input.
	},

	// dreamy underwater experience: water stuck in mid air that you can swim through. super cool.
	// when in water you magically transform into a swimming creature. YES this feels freaking amazing.









	// Optional simplified UIs that are optimized for navigating rather than styles/aesthetics









	// ON SCREEN (and in ear)








	//spatial and geometrical concepts 
	// Measurement of space, Spatial dimensions, Spatiality

	dimensionalCoordinate: {
		is: {
			scalar: 1 //scalar value... 
		},
	},
	x: { // I assume the keyword x will only be used for coordinate.... I am a bit scared about occupying this label/namespace
		is: {
			unique: 1, //only pointer to specific dimensional coordinate
			dimensionalCoordinate: 1,
			horizontal: 1 //locally, not globaly only inside the plane 
			// something something camera or viewport represents x as being low value is left, high value is right
		}
	},
	y: { // I assume the keyword x will only be used for coordinate.... I am a bit scared about occupying this label/namespace
		is: {
			unique: 1, // only pointer to specific dimensional coordinate
			dimensionalCoordinate: 1,
			horizontal: 1 // locally, not globaly only inside the plane 
			// something something camera or viewport represents x as being low value is left, high value is right
		}
	},

	position: {
		has: {
			dimensionalCoordinate: {
				count: {
					//equal to the number of dimensions in the space it is "plotted" on.
				}
			}
		},
		default: {
			is: screen.center,
		}

		//	definingPositions:{ //how to define positions? something like this?
		//		is: {
		//			in: {
		//				building: 1,
		//			}
		//		},
		//		has: {
		//			position: {
		//				building: 1,
		//			},
		//
		//		}
		//	},
	},
	positionalRelationships: { // positional/location relationships
		// used in composition!
		Outside: 1,
		Contains: 1, // encapsulates / contains
		covers: 1,
		On: 1,
		In: 1,
		around: 1,
		Surrounds: 1,
		above: 1,
		StuckTo: 1,
		under: 1, // beneath:1,
		behind: 1,
		inFrontOf: 1,
		beyond: 1,
		along: 1,
		throghout: 1,
		surrounded: 1,
	},

	direction: {}, //spatial direction
	// direction up


	vertical: {
		// verticality can symbolize transcendence. reaching newer/higher levels, reach the sky, going towards the stars.
	},
	horizontal: {},

	north: 1,
	south: 1,
	west: 1,
	east: 1,

	up: {
		is: {
			direction: 1,
			// smaller y's
		}
	},
	left: {}, // smaller x's
	down: {}, // bigger y's
	right: {}, // bigger x's

	top: {},
	bottom: {},

	size: { // or scale
		// A property which describe the relative area oj object
		// If a thing has double area, it has double the size or scale.
		// So size should be determinable by a function
		// howManySizeAinB(a,b)
		// Play with scale
		// Size is important in art to create feeling
		// usually big things are seen as more powerful
		//	more clumsy
		//	more heavy
		// Etc
		// Big areas are worth exploring
		// By making the player small, a plant that fills the screen feels huge.
		// With a small scales character you can give the impression of
		// Massive buildings, empty landscapes.

		// Size/scale language conventions
		//
		// Big should refer to the usual size of the thing or similar things.
		//
		//
		// A big eye can refer to breaking the conventional size of eye
		//
		// A huge eye would be more like, it is bigger than what the eye sits on.
		//	huge is like more relative to its closest contextual surrounding objects.
	},
	//hugeness / smallness

	length: {
		scalar: 1,
		should: "positiveNumber", //should be a positive number, can be measured in pixels or relative length (Example: 1.5 the length of a spider)
		//measure of distance
	}, //1D
	area: { //space
		// Area grows when dimension increases
		// Area diminish when dimension diminishes
		// ^this gives the algorithm an intuition on how to affect area
	}, //2 D
	volume: {}, //3D

	// directional lengths
	width: {
		is: {
			length: 1,
			horizontal: 1,
			value: {
				scalar: 1,
				unrealized: 1, //maybe realized:0???
			}
		}
	},
	height: {
		is: {
			length: 1,
			vertical: 1,
			value: {
				scalar: 1,
				unrealized: 1,
			}
		}
	},
	depth: {},

	proximityVsDistance: { //scale
		// A calculation of two scalars. abs(a - b)
		// Describing two elements commonality in how much they compare to each other.
		// Aka describing 2 elements relationship.
		// The unit used and how to make sense of it contextually depends on context.
		otherWords: {
			// Distance vs proximity
			// Difference vs similarity (like-ness)
			// Closeness vs farness
		},

		// The meaning of proximity/distance
		Proximity: {
			// Things that are close together are perceived to be more related than things that are farther apart. 
			// • One of the Gestalt principles of perception. 
			// • Proximity is one of the most powerful means of indicating relatedness in design, and will generally overwhelm competing visual cues. 
			// • Degrees of proximity imply degrees of relatedness. 
			// • Certain proximal layouts imply specific kinds of relationships. For example, touching or overlapping elements are interpreted as sharing one or more attributes; whereas proximal but nontouching elements are interpreted as related but independent. 
			// • Arrange elements so that proximity corresponds to relatedness. Ensure that labels and supporting information are near the elements they describe. Favor direct labeling over legends or keys.  See Also Common Fate • Figure-Ground • Good Continuation Law of Prägnanz • Uniform Connectedness

			// This sign at Big Bend (top) misleads hikers by using proximity incorrectly. The redesign (bottom)
			// the problem. 
		},
	},

	proportion: {
		//the relation between somethings spatial dimensions.
		// description of what forms it takes.
		// like wide, high, tall, vertical, horizontal,
		// or based on similarity, like a spiral, like a bird.
		// proportional - "corresponding in size or amount to something else."
	},
	ProportionsLanguage: { //of shapes (part of shape) proportions (part of shape)
		long: 1,
		short: 1,
		tall: 1,
		broad: 1, //, wide
		big: 1, // size/scale
		giant: 1, // size/scale
		small: 1, // size/scale
		tiny: 1, // size/scale
	},


	abstractSpace: { // quantitySpace
		// "this room can hold exactly 5 items." Does not matter what size they are or what properties they have, as long as they  classify as items.
	},








	// geometry, shapes and figures
	// geometry...
	figure: {
		// not sure if I should use is or has...
		has: {
			shape: 1, // I am a bit conflicted because in a sense a sprite is a figure. but fuk it, I guess it is either a figure or a sprite, when drawing.
			position: 1,
		}
	},
	shape: {
		is: {
			drawable: 1, // The thing is, it is only drawable WHEN it is defined as a shape. it is only so when it has a shape template (like rectangle) and fulfillled all restriction of that template. in this case width and height, but then draw does also require shape to have position.
			// What i need is info about who needs values to be defined. If I know that it is draw that needs x, y, width, height to be defined, then it is easy.
			// If I know it is simulation that needs to know if an event has happened.
			// Aka archetypal... I might just say shape:{archetypes:{...}}
		},
		archetypes: {
			square: 1,
			circle: 1,
			triangle: 1,
			rectangle: 1,
			ellipse: 1,
			octagon: 1,
			hexagon: 1,
			halfMoonShape: 1,
		},
	},
	// basic shapes
	Circle: {
		is: {
			shape: 1,
		},
		has: {
			radius: "scalar", // if radius is 0 then it is a point
		},
	},
	rectangle: {
		is: {
			shape: 1,
		},
		has: {
			width: 1, // should be a positive number, can be measured in pixels or relative length (Example: 1.5 the length of a spider)
			height: 1,
		},
		// if width === height, it is a square and you can replace width and height with side. 
		// A box represents/symbolizes containment and so does a square or rectangle.
	},
	axisAlignedBox: { //rectangle with rotation 0 or no rotation
		synonym: {
			aab: 1,
		},
		is: {
			rectangle: 1,
		},
		has: {
			or: {
				no: {
					rotation: 1,
				},
				rotation: 0,
			}
		}
	},
	triangle: {
		is: {
			shape: 1,
		},
		has: {
			point: {
				quantity: 3
			}
		}
	},
	square: { // I probably do not need a definition for a square lol.
		is: {
			shape: 1,
			//			geometry: 1, //basic geometrical shape/concept
		},
		has: {
			side: "scalar",
		},
		make: function (x, y, side) {
			//square is not yet tested
			return new Geometry.rectangle(x, y, side, side)
		}
		// a box represents/symbolises containment and so does a square or rectangle.
	}, // I probably do not need a definition for a square lol.

	roundness: {
		is: {
			scalar: 1,
			adjective: 1,
		}
		//	roundness is the biggest circle around all points and its distance
		//	
		// make a circle that touches all points outside the shape
		// make a circle that touches all shapes outside the shape.
		//
		// basically go through every point on the edge of the shape and compare its distance a defined center of the shape.
		//	so you find the biggest distance and the smallest distance. 
	},

	simplexNoise: {
		// the Benefits of simplex noise and when to use it.
		// smooth randomization
		// multidimensional continuity of pseudo random / unpredictable.
		// get the in-between values.
	},


	overlap: {
		// A relation that shapes can have to eachother
		is: {
			relation: 1,
			boolean: 1, //either true or false
		}
	},
	//shape overlap checking  "collision" checking
	//simple collision or geometric overlap checking - add seedling
	aabToAabOverlapCondition: { //(axis Aligned Box)
		// definitions:
		//	a1, a2 = axis Aligned Box
		//	w = width
		//	h = height
		// condition:
		//	a1.x < a2.x + a2.w && a1.x + a1.w > a2.x && a1.y < a2.y + a2.h && a1.y + a1.h > a2.y
	},
	circleToCircleOverlapCondition: {
		// sqrD = square distance between origin position of the two circles
		// sqrD = (x1-x2) * (x1-x2) - (y1-y2) * (y1-y2) > radius1 * radius1 + radius2 * radius2
		// r1 = radius of circle 1
		// r2 = radius of circle 2
		// sqrt(sqrD) < sqrt(r1 * r1 + r2 * r2)
		// simplified version that works in most cases?: sqrD < r1 * r1 + r2 * r2
	},


	thick: {

	},
	thin: {
		// Something that is thin, like a leaf or grass, is so thin that even though it is mostly opaque, light still shines through it from the other side.
		// (backlit things look like they are glowing a bit and you see shadows through it)
		// (like you see the blood vessels of the leaf but you can slightly see through the leaf skin)
	},



	// Objects are made out of sufficiently convincing contrasts.


	// styles and aesthetics

	// part of style:
	// 	how detailed/noisy it is.
	// a large number of objects to take in, a large number of saturated colors, a large number of possible points you can be attacked from, etc.
	//	The more visually complex something is, the more extreme(highlighted) something has to be for us to notice it (like in the jungle image above with the guiding elements turned on).









	// add to level design, landscape. etc.
	// design the landscape for the player's abilities.
	// exaggerate points of inaccessibility. (Do not make the player think they can figure out how to get past something. It should be clear that something is current impassable)

	//balance colors to mid-tone gray. Its most universal background..	
	//when drawing use a mid-tone (gray) background to make the light and dark tones stand off visually. (good for color balancing)




	coloringLandscapes: {
		// Atmospheric perspective:
		// Make background far distance lighter color(brighter, more white)
		// Making the foreground read clearer in contrast. 
	},


	// artistic composition ideas (from GRIS)
	// Artistic composition of environments
	//
	// FUCKING environmental compositions man. Like, building that when you stand on top on it the sun is perfectly aligned in the background to create like a fucking halo and then the sun just becomes the gris logo.
	// foreground and background parallaxes that align when you are in the perfect spot.
	// play with sand ground and objects sticking out of sand.
	// running in the sky, super effectful. they run on the lines between stars. fucking amazing.
	// Mountain parallaxes joy division style. Super pretty and effectful.
	//
	// use sculptures with symbolic meaning. aka a hand sticking out of the sand that is all broken and cracked.
	//
	// use fog in the foreground as a color filter for things behind it.


	droplet: {
		//	rain and water droplets
		//		translucence (alpha)
		//		reflectiveness/glare
		//		water jiggle forms. (how do you make water uniformly round when still and jiggly when moved. increase simplex jigglyness with acceleration or something...)
		//		water form merge. (how does the underlying particles work to make a droplet become part of the ocean (or any other body of water))
	},




	shaders: {
		// the Benefits of shaders (and post processing? / filters) and when to use it.
	},

	// crazy inventor logic. He made himself sweat a lot, and he sweats a substance that attracts a lot of bees. so now he is constantly surrounded by bees.



	//something for guidelines or mechain

	//feelings

	// What is the process of deciding a player situation. scenario. location/position ?
	// What clues is there for the player to indicate where they are in space, time and universe?
	// What do you want to teach the player/audience.
	// About life forms, objects, abstractions?
	// Character oriented?
	// Human oriented?
	// Technology level?









	// TYPE OF CHANGE

	damage: { //hurt:
		// irreversibility to a part of something. part made increased irreversible.
		// violence that impairs the value, usefulness or normal function of something.
		// damage can slow down a function or make it less efficient
		// while destruction completely stops something from manifesting or being possible.

		// see also: impact
	},
	destruction: { // destroy
		// making a conept irreversible (increasing the cost of bringing it back)
		// something that is very destroyed can not be put back together without very high cost/effort.

		// Good definition: causing enough damage for something to something that it no longer exists/is or can not be repaired/reversed.

		// the stopping of a consistency
		// broken identity or constancy
		// 	that has some irreversibility.
		// a system or a whole that changed in a way
		// discontinuation
		// disruption
		// un-earthing (earth element)
		// breaking

		// destructive environments? destructive buildings?
	},






	//silhouette and composition is more important than details. because details are harder to see.


	practicality: { //practical
	},



	symbolicMeaning: 1,




	// find a way to reverse engineer extraordinariness, basically when generating, say how unlikely the result is.

	// camera and views
	cameraControl: { // viewControl - the movement of the view and changes in perspective.

		//movement behaviour
		//

		// based on these ways to do cameras in other 2d games: https://www.youtube.com/watch?v=l9G6MNhfV7M

		// Object tracking camera styles:
		//		position locking camera. = the center of the view is aligned with the object
		//			vertical = happens along x-axis.
		//			horizontal = happens along y-axis

		//^ only x tracking is common in 2d platformers/sidescrollers. This lets you be zoomed while tracking the world, and not creating motion sickness. + this perspective lets you see thee floor. or the bottom ground at all times.
		//^ x & y tracking is common in
		//	top down games.
		//	or sidescrollers that are very zoomed out.


		//minimum vertical movement
		//	lock y axis when below threshold.
		//	only change y as player lands on a higher platform.


		// fixed grid movement, or "room" movement" - the camera shifts to the next room as the player goes through the threshold of the screen.

		// room cut. - when the player goes out of a room the camera cuts to the next room (this is common in castlevania)


		//camera trap - the view is only pushed minimally to aligned the tracked object with "camera trap" bounding box.
		//^ the benefit of this is that the character can do things on screen without constantly moving the camera.
		//		which reduces motion sickness.

		//camera rubber band - smooth movement.

		// autoscrolling camera - the camera moves on it's own and becomes a challenge where you have to keep the character in the camera.
		//^ useful for time trial jumping challenges.

		// A stopped camera indicates player characters inability to move beyond the boundary. aka a floor or a wall.


		//look ahead, the camera is a bit in front of the player in the direction they are moving.




		has: {
			position: 1,

			// of screen?
			width: 1,
			height: 1,
			zoomLevel: 1, //(image scale ? )
			// movement behaviours. (defined in TechniquesForCameraControl) 
		},
		//		camera focuses on what is relevant and interesting
		// This is generally the playercharacter, sometimes beings or extraordinary items nearby
		do: {
			generally: {
				follow: {
					playerCharacter: 1,
				}
			}
		}
		// Zoom out lot to get cool perspectives. bigger picture.

		// camera control tropes:
		//
		// follow player/subject.
		//
		// dolly zoom - you move camera forward as you zoom out, so that an object in the foreground stays while the background is distorted/zoomed.
		//
		// look ahead - move camera to smoothly point to a distant target
		//	Often used to look towards important elements of the game like objectives or dangers. 
		//
		// pedestral - camera is zoomed in on character feet and goes up to their head.
		//
		// camera related:
		// lense flare
		// chromatic aberration
		// shutter - (you see the blur pattern in light of the sh
		// photo exposure -
		//
		// eye shoot - intense, fierceness of character.
		//
		// cutting. when do you cut to another shot? When do you skip through time?
		// when can you give the illusion of conclusion, where the audience convinced of what happened without you showing it.
		// (Like showing right before the impact of a bomb and cutting to a funeral.)
	},
	view: {
		// is like a camera. but camera is more about movement or something
		has: {
			archetypeOfPerspective: 1, // A view has one archetype of perspective at any given time. To have no perspective is also a choice of perspective.
		},
		does: {
			// creates visual experiences for viewers.
		}
	},




	// TROPES AND CONVENTIONS
	// specific characters
	characterThatHasTheWindWithThem: { //categorize this better
		// a character that has the wind about them.
		// leafs are stuck in an air current around the player
		// all these object sail around em
	},
	specificDude: { //just some experiment thing?
		is: {
			character: 1,
			humanoid: 1,
		},
		wearing: {
			scarf: 1,
			witchHat: 1,
			robe: 1,
		}
	},

	wearing: { //wears: {},
		// held on the outer layer of body
		has: {
			x: {
				is: {
					on: {
						body: 1,
					}
				}
			}
		}
	},
	holds: {
		has: {
			in: {
				hand: {
					x: 1,
				}
			}
		}
	},

	giant: {
		of: { // this keyword is great, lets you have different definitions
			creature: {
				// giants are cool... like giant people and giant animals.
				// giants are slower.
				// generally bigger version of things move slower.
			},
		}
	},





	// Video game tropes and genres

	// ____Tropes_____
	tropes: { //conventions: convention:
		tropesAreTools: { //tropes are tool
			//	used to control audience expectations (and aesthetic emotion-experience?)
			//	tropes can be overused (over consistency. beating a dead horse)
		},
		discoveryVsMastery: { //a basic video game orientation
			// two fundamental video game approaches
			//	revealed enemies, - games where the player get to know every fact about the enemy.
			//		the game tries to be open and revealing about its system
			//		It is about making the right decisions in complex environments.
			//			to harness the available data and have a successful mental model how to deal with situation.
			//			optimization problem, to make the most efficient or economical choice ina complex environment.
			//		
			//	secret weakness - games where the players are supposed to explore and find the properties 
			//		of the enemy through interaction.
			//		this is a form of exploration
			//		It is about the art of science and finding criticality, finding the weak spots.
			//			being a good scientist, a good detective. and good at making safe experiments.
		},
		backtracking: {
			//	Conceptual revisiting
			//	Can give a sense of mastery
			//		You go through the same but now you are stronger
			//	It gives you perspective.	
			//
			//	Can make you notice things you did not see before (heavily used in brainvania games)
			//
			//	There is a unique form of back tracking where the place returned to is destroyed or altered
			//		This can either give a sense that time is moving on and we can not visit the past.
			//		Or if altered, it becomes a remix, which is a play on familiarity and novelty.
			//			Remixing allows the curation of things and reframing of other things.
			//				That which is consistent over instances.
			//		Remixes gives opportunity to provide new challenges
			//			Diable makes remixed enemies that are harder.
			//			Remixed areas allows you to use the same assets with new challenges.
			//	
			//	It is good design to reuse the concept or spatial configuration for multiple purposes.
			//		standardized interface.
			//		good for the platonistic mind
			//		good for the perfectionists who wants curation around some single stability or nodality.
			//	backtracking is annoying when it is both forced upon the player and seen as redundant.
			//		when it gives no flow it is "filler" content
			//		
			//	uses
			//		balancing familiarity vs novelty
			//	backtracking can be avoided by
			//		linear progression (making backtracking impossible)
			//		making levels a spatial loop.
			//			aka a temporary linear progression that brings you back to where you were
			//			example: dungeon in Skyrim, trial in BOTW, usually seen in mission and side quests.
			//			this goes well with heroes journey.
			//			this can either be done by
			//				physically pathing a loop
			//				or making a portal at the end taking you back to the beginning.
			//				
			//		introducing speed travel or spatial shortcuts.
			//			castlevania examples: space portal, elevator, super jump, movement speed enhancement.
			//
			//	related trope is Level in reverse
			//		wher you visit a mirrored world or go through the steps backwards
			//		upside down Dracula's castle
			//		Mario Kart mirror levels
		},
		filler: {
			//	opposite of exciting
			//	used to sustain feelings over time.
			//	delay rewards
			//	or to give the player a pause or a break from excitement or intensity. (important! especially for introverts.)
			//
			//	anything that does not cather to the player's needs while occupying challenge/attention/time
			//		will be seen as unneeded fillers by the player.
			//		this is good opportunity to provide optional skipping (skip cutscene, dialog, speed up time, etc)
			//	
			//	filler is usually defined by how unrelated it is to the main content or excitement in the game/product.
			//
			//	serve only to take up space
			//	padding...
			//
			//	filler usually stems from a need to make things seem bigger than they are.
			//		by putting in content that is cheap to produce.
			//	think hot dogs of food or like adding water to a product to make it weigh more.
			//	to overcome a threshold of a criteria of judgment like:
			//		it is heavy in weight, it costs much, it has many levels.
			//		long playtime, 
			//	it is commonly an overcompensation for inability.
			//		anime doing filler because of extreme deadlines
			//
			//	filler require less coordination to create as it is less connected to the value of a product.
			//	
			//	in a sense the opposite of curation. but it still tries to avoid introducing any measurable pain
			//		which low curation still might overlook.
			//
			//	over-excitement could be seen as a form of pain or damage to the player.
			//
			//	in a sense it is abusive or exploitative to rely on filler,
			//		occupying subjects with meaningless content without nutrition.
			//
			//	good Filler can usually be safely ignored without any loss of important information.
			//
			//	some say filler has to do with the authenticity and intention of the 
			//		it is filler if its compromise of the source's vision.
			//	
			//	a filler can be a call to an adventure that leaves you in the end in almost fully the same state
			//		as once you started.
			//
			//	Basically either a forced interruption
			//	or a voluntary interruption.
			//
			//	in anime there is a concept of a recap episode.
			//		these introduce literally no new information.
			//		the only value they provide is:
			//			a break
			//			Curation of memories (directing the focus going forward)
			//			cause the player/viewer to reflect and organize their thoughts.
			//	
			//	examples of concept used as filler
			//		backtracking
			//		dialog
			//		loading times
			//		any waiting
			//		grinding
			//		mini-games, side quests
			//		"occupying busy work"
			//		regeneration of obstacles/enemies
		},
		padding: {
			//	unnecessary filler
			//
			//	usually meaninglessly dragging things out or meaninglessly revisiting things.
			//	causes too much boredom
			//
			//	happens by poor/meaningless design or rushed work.
			//
			//	in books this is like long meandering text.
			//
			//	the easiest way to pad is to increase numbers. 100 meters instead of 10. (or slow down speed.)
			//		and duplication (copy-paste padding). 5 baby snakes, 10 baby snakes. 100 baby snakes.
			//			reusing the same assets.
			//	
			//	in video games it usually manifests as
			//		long fetch quests
			//		high grinding requirements
			//		low spawn rate (you can only progress 2% every 5 blood moons)
			//		or straight up bad communication, leading the player to 
			//			aimlessly wander until randomly stumbling upon meaningful progression.
			//		or insufficient carrying capacity, 
			//			requiring you to load objects back and forth over long distances...
			//		resetting fail state that is usually dependent on luck or otherwise hard to prepare for.
			//			perma death
			//			going back to 0
			//			Majora's mask time reset
			//		load/wait time
			//		things that break
			//			either by chance
			//			or systematically over time
			//		insufficient reward
			//			you only get 1/100th of what you need to progress
			//		resource sink/drain
			//			example: daily maintenance require the same resource as progress..
			//				constantly filling and unfilling the progress meter
			//					while costs usually increase exponentially. (leveling etc)
			//		regeneration of obstacles/enemies
			//	
			//	in comic its blank pages or information page or just fan-art or drawings pages.
			//
			//	a good counter in games is to signal to the player 
			//		to the aspects and extent a concept or area has been explored.
			//		Mark level as finished, say "you found all blurbs", you got perfect score.
			//			or ask "do you really want to replay this?" (which is a bit... confrontational lol.)
			//			or straight out say "maybe you wanna go x to progress the story"
			//				basically good communication of where to go to fulfill your player needs/drives.
			// padding is different from "waiting for input"
			// waiting for input is a temporality model and trope where progression is completely controlled by player input
			// and its different from
		},
		remixedLevel: { //remixed level
			//	a thing that has been experienced is re-experienced but different.
			//		maybe time passed on or you went back in time. or it mutated or was transformed in some other way.
			//		maybe chaos, space is warped.
			//		Maybe the environment was destroyed.
			//		maybe it was taken over or rebuilt.
			//		maybe natural season shift or a shift in temperature
			//		or now the level is filled with lava or water or other substance
			//		now it is 0 gravity in space without oxygen.
			//		or you are in an alternate dimension, like the shadow world or "the other side" or just some other parallel world.
			//		or mirror world..
		},
		recurringRiff: { //recurring riff
			//	leit motif
			//	a jingle or pattern that reoccurs 
			//	like the jazz riff.
			//	is like a meme but people do not meme about it.
		},
		nostalgiaLevel: { //nostalgia level
			//	a level meant to invoke memories. usually a remixed level of an important place the player experienced in the past.
			//	music is often important in these circumstances. a theme you have not heard since that time.
			//	this can follow the trope of going back to "where it all began"
		},
		retrauxFlashback: { //Retraux Flashback
			//	when a series does an art shift to an older presentation style for flashback effect.
		},
		megamix: {
			//	cherry picking the best content from a product or series of product into a curated super content.
		},
		// All the Worlds Are a Stage
		//	This is when levels come Back for the Finale
		//	The final level is made of parts from and/or in the theme of most to all past levels, or at least it has a section like that.
		//	 It is a good way to extend the final level and make it feel more epic
		//
		backForTheFinale: { //back for the finale
			//	when things from every chapter or part of a story reunion in the end, to create an impactful impression.
		},
		recurringLocation: { //reoccurring location
			//	a place which is not the main hub or boss area, but a minor place which appears over and over again.
		},
		hubWorld: {},
		archetyping: {
			//	Catering to the popular most grokkable most memorable most familiar tropes and symbols.
		},
		// Inaction sequence or scenery shots
		//	A break in video where nothing happens except the background noise
		//	Lighting, cicadas, raindroplets, clouds go  by, reflection of water
		//	The wind goes through the grass.
		//	Cars or trains go by.
		//	Put you back in reality and make you reflect
		//
		chaosArchitecture: { //chaos architecture
			//	A designed world that is inconsistent amongst instances or over time
			//	It has an identifying structure that makes it the same
			//		but it is details are different every time.
			//	Dracula's castle is different every game
			//	The game where things switch out as soon as you look away from them.
			//
			//	Switching from game to game, from level to level, from instance to instance
			//		or for every encounter
			//
			//	It has similarities to nature which changes with seasons because plants and animals so dynamic.
			//		or cities changing over time.
			//		the only difference from reality is the illogical non-realistic inconsistency 
			//			which is not explained.
			//	It is also different from simple redecoration. it is beyond movement of objects
			//
			//	Deeply connected with magic, alien tech or the supernatural.
			//
			//	Bee and PuppyCat does this
			//	
			//	Things are different when you go there.
			//		different rooms
			//		different colors
			//		different objects
			//		different connection between rooms
			//	
			//	Chaos architecture might actually be the foundation of the algorithm....
			//		as it generates results based on current criteria, where it might be hard to pin down consistency.	
			//	Chaos architecture is usually conveniently redesigned for the occasion or context
			//		it can therefore be categorized with soft magic systems.
			//		(like JoJo)
			//		just like a story or character can manifest differently depending on how it is told.
			//
			//	It is a counter action against limiting consistency.
			//		a freedom realistic consistencies are unimportant.
			//
			//	Not to be confused with non-euclidean architecture which are still consistent but non-euclidean...
			//		does not confirm to the normal physical laws of space, scale and distance, etc.
			//		often plays with optical illusions
		},
		willingSuspensionOfDisbelief: { //Willing Suspension of Disbelief
			//	the acceptance of non-realism (Play a game, not as a means to act in real life, but to engage abstraction or fantasy)
			//	stepping inside the "magic" circle.
			// acceptable breaks from reality (of realism)
			//	"It is possible for these to become unacceptable, when the abstraction gets in the way of enjoying the work. 
			//	On the flip side, it is possible to get so accustomed to a particular break from reality that people stop realizing its unrealistic.
			//	different people have different tolerances for the balance between "abstraction" and "simulation,"
			//
			//	some times its not worth spending time and resources justifying.
			//		(only Aspbergers and boomers get hung up on shit like that dude)
		},
		antiFrustrationFeature: { //anti-frustration features //(game design)
			//	are conventient changes or suspension of the rules temporarily for an edge-case or specific section
			//		where complying to the rules would be too tedious or complex.
			//	"quality of life"
			//	
			//	examples
			//		the game lets you skip
			//		the game boost your powers temporarily
			//		skipping dialog.
			//		in the Zelda Majora's remake you could watch hints in-game what to do next.
		},
		ruleOfIndex: { //rule of index (and artistic license)
			//	Creators are allowed to be inaccurate if the inaccuracy serves the story better than accuracy would.
			//		"However, this is a double-edged sword. For the license to work, the story has to be good."
			//		The license also does not allow every kind of inaccuracy. People still expect characters to be consistent. This cannot be used to excuse Character Derailment or Contrived Stupidity Tropes.
			//	plot holes and inconsistency is acceptable by the audience as long as it leads the audience to the desired experience and feelings

			// People accept and can enjoy things that does not make sense if it is enough of the following: (the rule of x in TV Tropes)
			//	cool
			//	creepy
			//	scary
			//	cute/adorable
			//	funny
			//	pretty/Glamorous
			//	romantic
			//	sexy
			//	symbolic (Symbolism)


			//^ all of these are things that makes people react on a deep level. They surve evolutionary purposes.

			//	Rule of Drama: If the potential for conflict is visible, then it will never be passed over.
			//	Rule of Empathy: A character's likelihood to survive is directly proportional to how much the audience can empathize with them.
			//	Rule of Fun: If a game is fun enough, all else can be excused.
		},

		conservationOfDetail: { //the law of conservation of detail
			// every represented information is important?
			//	every presented piece of information is important. (should be)
			//	so curate because your audience has limited time and attention.
			//	aka it is good to pack everything with meaning because we have limited time and the audience want meaning.
			//
			//	opposite of narrative filigree
			//
			//	the audience expects what we signify to be of importance.
			//	and the outs are rules of index
			//		and possible some rules about subverting expectations like red herrings and comedy.
			//	the importance of the law of conservation of details
			//		depends on the time the audience has.
			//	with little time you have to pack all symbols tightly and maybe use exaggerating
			//	with lots of time you have the ability use filler to balance pacing
			//	
			//	basically it comes down to nitpick the symbols you have because the audience attention is finite.
			//		focus to create the archetypes that are essential
			//		bind the context down.
			//		choose the realm of communication.
			//	communicate without overwhelm.
			//	
			//	in video games this is often presented with focused core mechanics 
			//		and certain systems that are more programmed, animated, sounding, than others.
			//		important buildings and scenery. important enemies like bosses etc.
			//		it helps the player know what aspects are core to the game.
			//		usually, however, this is the reoccurring frustration I have with games
			//		because it makes it feel like it has cardboard walls around their little meaningless core plot.
			//		like a game about gardening but 90% plants is grass and it serves no functional purpose in game.
			//		so fuck that shit
			//		because its what makes everything unleveld and hyperfocused
			//			like oh guns and barrels is everything that is important
			//			so lets make the interactable people useless blobs
			//		feels unbalanced. its what makes every gamer a fucking dickhead.
			//		its the localization of importance, like oh in this context the stats of sword is the only important.
			//		(and like reuse of assets)
			//
			//	when the law of conservation fails, by oversimplifying or compressing things unwell
			//		it leaves a good opportunity for deconstructing it.
			//			aka another piece of art that analyze what its made out of and comment on it
			//				and maybe rebuilds it into something more beautiful and accurate and helpful.
			//		make sure to deconstruct (take apart as it was asemmbled)
			//			 rather than destruct it (break it apart recklessly)
			//
			//	one viewer's "boring five-minute long tracking shot of a beautiful mountain range" is another viewer's "this is not a movie, it is art."
		},
		chechovsBoomerang: { // Chechov's boomerang //storytelling
			//	An important item that was used for a meaningful moment.
			//	And then unexpectedly it comes back and is re-used.
			//	In games it is important that the item is not forgotten, so that when it is required you stumble upon it again.
		},
		obviousRulePatch: { //obvious rule patch //(game design, balancing?)
			//	prop the loophole so it can not be abused in the game.
			//	stopping the player from playing in unintended ways.
			//	especially exponential feedback loops and going beyond intended actions.
			//
			//	you can not put inventories in your inventory.
			//	you can not place a house in a house.
			//
			//	there is a max level. a max limit.
		},
		geniusLoci: { //genius loci
			//	a location that has a mind. 
			//		A world or a place.
			//example: Dracula's castle. or earth being supercomputer in hitchhikers guide.
		},
		// Scale and size does not have to be consistent if it is justified...
		//
		// Having multiple outs:
		// 	by unclearly defining properties the system has artistic freedom and can even change things as they are convenient as long as it is either unnoticed or justified by the rule of index.
		//
		// True art
		//	The story puts forth the proposition that "True Art" must of necessity be incomprehensible, or at best, only comprehensible by the "right people."

		villain: {
			//central source of evil
		},
		// joker immunity
		//	aka villains can not die because we need them to be reused.
		//
		PointOfNoReturn: { //The Point of No Return is a place in the story of a video game where it permanently becomes impossible to revisit earlier areas.
		},
		linearityOfGames: {
			// one way
			// no options
			// same ending or low number of endings
		},

		evenDeeperTrope: {
			// Going beyond what is possible. putting the sound system up to 11.
			// The next level - hitting the edge, and going even further. Beyond it.
			// Like you do something dramatic once to get to something amazing unexpected, but then you do it again to get even further.
			//	you break the ground to get to the cellar level. then you break the cellar floor to go down to the underworld of the underworld
			//
			// ground is the shadow or reflection upside down but inverted colors. like water reflection or the world of opposites.
			//	In the shadow world, if there is an object in the real world there is none in the shadow world.
		},

		// toon physics
		//
		// animation can be expensive, try to compensate as much as possible withou loss of value.
		// static backgrounds are cheap.
		//
		//
		// what happened to the mouse? //add to resourecfulness...
		//	aka drop small character storylines that ain't important because is too much to track
		//
		//
		// Epileptic Trees
		//	A term for wild, off-the-wall theories.
		//	secret cow-level
	},
	RNGTropes: { // artifical/narrative luck
		//			"fortune at the beginning": the dies are rolled before actions are made. allowing the player to adapt to the new circumstance.
		//			"fortune in the middle": ??? the player decides on some things then randomness strucks and then the player gets to adapt some. ??????
		//			"fortune at the end": //setting up your situation for the outcome to be randomly determined. You set up for good probabilities.
	},


	// METAPHYSICS TO PHYSICS
	// Abstractions of room and empty space.
	storage: {
		// isolated/protected space to put content in.
		does: { // this is its function, does not mean there are items in it.... think about the language
			holds: { // or stores?
				items: 1
			},
			stores: {
				items: 1
			},
			hides: {
				items: 1
			},
		}
	},
	inventory: {
		is: {
			storage: 1, //???
		},
		does: {
			lists: {
				items: 1
			}
		},
		conventially: {}
		// each inventory item has unique index
	},





	animation: {
		// visual sequence of events/happenings.
		// sequence of images depicting sequential moments in time.
		// increases visual stimulation
		// is essential in any (visually represented) physical simulation

		// TODO: look up the "principles" of animation or whatever it is called.
	},
	// Visual Representation of movement
	//	procedural swoosh animations for things that move quickly
	//		You can increase drag lines behind objects that move quickly.
	//		Just assign a swoosh style etc. 
	//		Example like the player holds a sword and if you move your sword with the cursor quickly the swooshes gets more prominent!!!

	//asynchronous updating....
	subFrameAnimation: {
		// subframe animation (of movement)
		//
		// have certain things animate at a higher frameRate, without clearing the screen.
		// this will create the blurry effect seen in animation + save rendering performance.
	},
	// TODO: more technical things like frame might be closer to game:




	//chemistry
	alchemy: {
		//		If I take a snowflake and zoom in the context becomes more of crystal world.
		//		If I combine snow with water it becomes ice or rain.
		//		stuff like that.	
		// doodle god logic
		// which might need to be separate from alchemy...

	},
	chemistry: {
		// predictable reactions of transformation in substance
		// The nature of how matter changes in substance.
		// I want chemistry. I want there to be consistency in combination of elements.

		// of preconditions and post conditions
		// you could make a reaction that is, move y down and trigger move y down next frame.
		// maybe there is a solution in entity component systems??? It feels like they deal with things.


		// If I would describe a chemical event I would use
		//	contact area (overlap)
		//	Then I would like the reaction to happen functionally from the 2 inputs
		//	there is a ratio of consumption and a ratio of output that are independent.
		//	let's say it is 1:1 ratio of consumption in reaction between r and g that produces 1 b
		//	then I can think starting condition r:255, g: 100, b: 0
		//	if there is no limitation of rate then the result would instantly be r:155 g:0, b:100
		//	I am not sure if rate should be an emergent property or if it should be defined with each reaction.
		//	I can think of scenarios where in 2d collision it happens over time because r and g only touches 
		//			and then builds a layer B layer, which moves out of the way and then touches.
		//	I can also think of a solution of water where the r and g molecules only hit each other ever so often.
		//	For that reason I think it is ok to use a model of conversion rate. but I am not certain.
	},
	metal: {
		// is a material, defined by its chemical purity (it is made of the same stuff all over)
		// metal vibrates when hit or impacted
		// the vibration creates a tone dependent on the size, dimensions and shape of the piece of metal.
		//	metal can serve musical function by vibration make sound so like rain can come down and play music like a xylophone
	},

	silver: {
		is: {
			metal: 1,
			gray: 1,
			reflective: 1,
		}
	},


	lightning: {
		// spawn in thunder weather 
		// very loud sound impact rumbling sound
		// heard from far away
		// in night, lightning lights up the whole sky
		// lightning can be seen from far away
		// lightning are generally shaped like roots.
		// they generally com from storm clouds  (????)
		//	or... specific clouds atleast... tall thick clouds lol. thunder clouds.
	},


	// folklore & fantasy artifacts, places and creatures
	monster: {
		has: {
			abnormalProperties: 1 // for example many limbs or missing limbs, or wrongly proportioned limbs.
		}
	},
	vampire: {
		can: {
			// can suck blood out of creatures through their fangs.
			not: {
				// cross water
				// stay in the sun
			}
		},
		has: {
			not: {
				//reflection in mirrors or floors.
				//shadow?
			}
		},
		does: {
			recover: {
				body: 1,
				power: 1,
				from: {
					//drinking blood
				}
			}
			// prefers fresh blood
		}
		// hyper confident vampires smug. they have lived for ever and have so much experience.
		//	I like that the vampire has this really old-timey vibe. mannerism and speech.
		// vampire is hiding in chest brought by carriage during daytime.
		// vampires amass power and they get people and monsters to do their bidding.
	},



	object: { //I call this object by lack of a better label
		// https://en.wikipedia.org/wiki/Physical_object

		// identifiable units in space.
		// A visually identifiable whole that tends to stay together over time.
		// An object is an identifiable collection of matter, which may be constrained by an identifiable boundary, and may move as a unit by translation or rotation in space
		// Each object has a unique identity, independent of any other properties. Two objects may be identical, in all properties except position, but still remain distinguishable. In most cases the boundaries of two objects may not overlap at any point in time. The property of identity allows objects to be counted.

		// A physical object or physical body (or simply an object or body) is a collection of matter within a defined contiguous boundary (in space)
		// The boundary may change over time. The boundary is usually the visible or tangible surface of the object.
		// An object's boundary may also deform and change over time in other ways.
		// An object is not constrained to consist of the same collection of matter.
		// An object is defined by the simplest representation of the boundary consistent with the observations.


		// An object is something consistent that is discernable from a background.
		//	something smaller that is different from something bigger
		//	generally of something isolated spatially
		//object:
		//	sticks out from background
		//	stays consistently basic properties (position, shape. behaviour)


		is: {
			instance: 1, //an instance
		},
		can: {
			exist: "worldy", //worldy meaning systematically affecting other things in the world domain 
			exist: "on screen",
		}
	},
	artifact: {
		is: {
			physical: 1,
			alive: 0
		}
	},
	being: {
		has: {
			agency: 1,
			autonomy: 1,
			aliveness: 1, // I am unsure whether I should use object or array. with array I can keep only labels and do not have to provide key value pairs. I might want to be more specific though..
			health: 1,
		}
	},
	agent: { //https://en.wikipedia.org/wiki/Intelligent_agent
		is: {
			entity: 1,
		},
		// Agents have perception. can sense or measure their environment
		// Agents have ability to change/affect its environment. (and possibly its own structure)

		// Agents works towards an end. (trying to reach or optimize a known condition)
		// Agent has choices. (Agents have a decision-making mechanism enabling them to have different responses to different situations)

		// Intelligent agents may also learn or use knowledge to achieve their goals.

		// Agents can only adapt if they can observe the effects they have on the environment. (analyze their own behaviour, error and successes)


		// Levels
		// Reaction - input -> output based on lookup, direct mapping.
		// Logic - deduction is applied to the input generating that which decides the output.
		// Belief-desire-intention - Updating models of what is true, gathering beliefs from experience.

		// Belief:
		// Desire: goals. object of perfection. what is worth striving for. what the agent wants to bring about.
		// Intention: desires to which the agent has to some extent committed. (plans in action) plans are sequences of actions. which can be composed of subactions.
		// Intention is the active attempts to progress towards the desire.
		// Plans are initially only partially conceived, with details being filled in as they progress.
		// Event: significant occurrences worth remembering. that perceived in the environment that is noteworthy. has big enough implications to change behaviours/plans.

		// Layered architecture - problem is analyzed through multiple paradigms, working on different levels of abstraction and the results of these are taken into account when making the decision

		// Multiperspectile validation


		// Evaluate how satisfied the agent will be with the given decision based on the believed range outcomes and risks/rewards.
		// Choose the one maximizes satisfaction.
		// Believed average outcome of decision... a number, positive if good and negative if bad that corresponds to average satisfaction.

		// The cumulative reward can be computed as a sum of the individual reward and the reward inherited from parents.
		// This is equal to a loss function (costs) and reward function (gains in value)



		// Keeping track on actively executed plans (applied action over time)
		// Balance between planning and execution
		// Balance between exploration (of uncharted territory) and exploitation (of current knowledge)


		// Learning agents can act in unknown environments to create their own models of how to satisfy their needs.
		// (Just needs safe enough learning environments... currently ai needs like a gazillion examples to learn any generalized lesson (or at least that is my impression))


		// Prediction, Approximating/modeling how the world will be soon and in the immediate future. to optimize for timing of action.
		// Where are things going if I do not intervene? Where are things going if I intervene?
		// What do I believe the consequences of my action will be? How certain am I? What do I believe the the side effects could be?
		// Is it more desirable? then act.

		// How well is the algorithm perceiving its environment? Does it see all parts? 

		// "Search and planning are the subfields of artificial intelligence devoted to finding action sequences that achieve the agent's goals."


		// Agents essentially maps a perceived now/current situation to a defined actuation. that is the entire function of an agent.
		//	To use a measured circumstance and in response generate a reaction that fulfills the goal.
		// The actuation can be found in an index or generated for the circumstance based on some methodology.


		//		agents need to achieve sufficient wisdom to judge themselves whether the simulacra/known recipes are sufficient for the desire, (and wether the desire is sufficient lol)
	},
	organization: {
		// Group of agents collaboratively working towards a common goal
	},
	// ????? like object???





	// MISC

	RewardFunction: { //For AI Artificial Intelligence or other agents?
		//shaping function:
		//	"being close to your goals is better than being far away from them."
		//	proximity to reaching goal -> higher reward. 
		//		(this just feels like it misses a point...)
		//		Being close to your goals means nothing if you still can't achieve the goals.
		//		So proximation to goals should only be motivating if the agent believes proximation will lead to desire.
		//		
		//		
		//		
		//terminal conditions: when to reset to previous predictable state.
		//	when to give up. When is the system exhausted enough to give up in their pursuit.
		//	cut their losses.
		//	
		//	basically find the eliminating factors early to avoid doing work that is unecesary.
		//	
		//	
		//	
		//Reward: desire to get higher value reward. 
		//	the more rewarded the more driven for higher reward.
		//	
		//	
		//	
		//	reward is essentialy relative height.
		//		you should always try to go higher than you are.
		//		in a ways that has low risks of falling very low.
		//		
		//	reward here is used as an indicator of close-by value.
		//		the gut feeling of what to do next.
		//		
		//		
		//create ways for your ai to safely explore terminal conditions, in order to know the dangers to avoid.
		//
		//
		//
		//logarithmic reward curves makes the agent want to do more effort the better it gets.
		//	basically to pay more attention to detail as it reaches mastery.
		//	
		//	
		//	
		//
		//
		//if there is no way to tell if the agents gets closer to the goal or in any sense of far away it is from the goal.
		//then it gets really hard for the agent to learn.
		//
		//you must include the information necesary for the agent to solve the problem
		//
		//as long the agent can infer the relevant properties from a model, then it can still solve the problem.
		//	(infering how close it is to reaching its goals. infering how good it's behaviours were.)
		//	
		//	
		//	
		//the rewards is for guiding the direction of it's testing (in multiparametric space) in aproaches toward the desired outcome.
		//
		//
		//sequentiential, you create reward systems for each relevant task in the moment.
		//	The relevant task gets to get to the initial condition of the next relevant task
		//	
		//	
		//you switch around the reward by conditions, aka by defining the equation that decides the reward, and make that equation context dependent.
		//	if in state a use reward equation x,
		//	if in state b use reward equation y,
		//	etc.
		//	
		//	
		//	
		//why I should only pay attention to that which is most relevant, not get lost in local optimizations.
		//	but optimize locally in ways that is optimal for the whole.
		//	
		//	
		//https://www.youtube.com/watch?v=0R3PnJEisqk
	},

	lookAt: {
		//an eye sees what it looks at.
		// characters can look at things.
		//	When looking at things their eyes are directed towards it and their head is turned towards it.
		// direct eye
	},


	// UNORGANIZED AND LEFTOVERS


	// Composition for desired effect
	defensibleSpace: {
		// An environment designed to deter crime through markers, surveillance, and signs of ownership. 
		// • There are three key features of defensible spaces: territoriality, surveillance, and symbolic barriers. 
		// • Territoriality refers to clearly defined spaces of ownership, like community markers, gates, walls, hedges, and fences. 
		// • Surveillance refers to features that promote being seen, like external lighting, mailboxes located in well trafficked areas, and well-maintained courtyards. 
		// • Symbolic barriers refer to things that indicate ownership, like swings, flowers, and lawn furniture. 
		// • Deter crime by marking territories to indicate ownership, increasing opportunities for surveillance,
		// reducing features that allow concealment, and using symbolic barriers to indicate activity and use. 
		// See Also Affordance • Archetypes • Prospect-Refuge

		// Deterbarriers.￼
		// neighborhood crime by adding territorial markers, surveillance opportunities, and symbolic
	},
	// Composition.... put together with progressive disclosure and advance organizer. (if advance organizer exist..........)

	// organization and design techniques
	fiveHatRacks: {
		// A metaphor representing the five ways information can be organized. 
		// • Information can be organized by category, time, location, alphabet, and continuum. 
		// • Category refers to organization by similarity or relatedness; for example, a store organizing products by aisle or a library organizing books by subject. 
		// • Time refers to organization by chronological order; for example, time-lines and TV guide schedules. 
		// • Location refers to organization by geographical or spatial reference; for example, emergency exit maps and travel guides. 
		// • Alphabet refers to organization by alphabetical sequence; for example, dictionaries and this book. 
		// • Continuum refers to organization by magnitude; for example, baseball batting averages and search results. 
		// See Also Framing • Hierarchy • Layering • Similarity

		// i guess this has mostly to do with generating overviewing index of things. how to store in a database or something....

		// Six breeds of dog organized using the five hat racks. Note how the method of organization the story the data tell. 

		// BUT THEY FORGOT THE MOST IMPORTANT WAY TO ORGANIZE INFORMATION: STORY, a fuking story is the best way to organize information because it transcends the information with more insight.
		// instruction is a story or narrative that takes what essential for the moment and presents it in order to generate a specific effect. a recipe.
		// usually instructions are flavoured with elements of drama and interest in order to make it more memorable and meaningful and also give context.
		// BUT THEN AGAIN story forgets organizing information in experience like VIDEO GAME because that transcends storytelling through interactivity and advance organizing. (see advance organizer).
	},



	// Interaction design:
	// Pulling a fruit from a tree should be like pulling a rubber band. The tree should bend until it snaps


	// Temporary transformation. transform into a sheep or skeleton.

	collectable: { // or pickable lol. //condition
		// Remember that you only need to look whether something is collectable if a collection action is imminent. Most contexts it is completely irrelevant whether or not something is collectable.
		is: {
			small: 1,
			loose: 1,
		},
		can: {
			beCollected: 1
		}
	},

	hanlonsRazor: {
		// Never attribute to malice what can be adequately explained by incompetence. 
		// • Proposed both by Robert Hanlon and science fiction author Robert Heinlein. 
		// • A variant of Ockham’s razor, Hanlon’s razor asserts that when bad things happen that are human-caused, it is far more likely to be the result of ignorance or bureaucracy than conspiracy or malice. 
		// • For example, when Apple’s Siri search was unable to find abortion clinics, many claimed Apple purposefully excluded them from the search results. The more likely explanation is that Siri was incomplete or buggy. 
		// • Keep Hanlon’s razor in mind when bad things happen. The principle does not exclude the possibility of malice—sometimes bad things are, in fact, caused by bad people—but malice is generally less probable. 
		// See Also Affordance • Black Effects • Contour Bias Mimicry • Supernormal Stimulus • Threat Detection 

		// What caused the post-Katrina New Orleans levees to fail? Inadequate engineering is more likely than government conspiracy—though, the government did blow up the levees in 1927 under similar circumstances. 
	},


	acceptableBreak: 1, // You are allowed to break conventions/consistencies if you have good reason.

	//TODO: put these in context...
	thorough: {
		// completing, perfecting, thinking about every detail
		// processed
		// personality trait??
		// off process when leading nothing to chance and taking in many if not all considerations before deciding.
	},



	pointToward: { //point toward - add seedling engine //TODO add to like, actions chapter in bias.
		// The edge is turned to point in the direction of.
	},

	animatedCoolRing: {
		// you can make an animated cool ring by doing the noise algorithm I use to make pebbles but with both upper and lower threshold
		// just shift the noise slightly but keep everything the else the same so you get the same artifact.
	},


	// Patterns in data creates intuitions or biased emergence
	//	Thing has 1 leg = can jump
	//	Thing has 2 + legs = can walk
	//	temp > 100 and temp < 200 = hot

	// I feel like I could define these concepts by their results instead and then infere cause and effect. For example throw:{ //limb that is holding something releases it in a way that it flinged through a substance}
	inferencenses: [ //emergentcapacities
		{
			condition: {
				is: {
					limb: 1,
				},
				does: {
					hold: 1,
				}
			},
			result: {
				can: {
					throw: 1,
				}
			}
		},
		{
			condition: {
				has: {
					leg: {
						count: {
							atLeast: 1,
						},
					},
				},
			},
			result: {
				can: {
					jump: 1,
				}
			}
		},
		{
			condition: {
				has: {
					leg: {
						count: {
							atLeast: 2,
						}
					},
				},
			},
			result: {
				can: {
					walk: 1,
				}
			}
		},
		{
			condition: {
				has: {
					temperature: {
						value: {
							atLeast: 100,
							atMost: 200,
						}
					}
				}
			},
			result: {
				//considered "hot" by subjects...
			}
		}
	]
}
// 2. Clarify/decompress desire/goal structure.

// 3. Generate action tree plan based on prioritisation according to beliefs/assumptions. (What needs to happen in what condition/event?)

// 4. Clarify the intended plan (Current optimal path. the finally defined sequence of actions. clearly defined in immediate future. vaguely defined in far away future and low probability/risk circumstances)
// (Step 4 should essentially be compiled code)
// a redundant alternatives plan. Execute A if risk x manifests then do y. If encountering unknown or improbable thing, break for analysis/re-evaluation.

// 5. Execute the intended plan/code until check up or break condition occurs. (test results, animation frame (the fps time limit..), player inputs, game conditions, paradox, error, )

// 6. analyze the information of the check up condition.

// 7. Update beliefs//assumption according to data

// 8. Rebuild the affected parts of the action tree plan. (you know where to look because you have flags, you've put up the meta data) (the beliefs that no longer hold up to reality)

// 9. Goto step 4.

var uhuh = {
	// realism, intricate,
	//		Art
	//			The beauty of reality
	//			The beauty of movement that makes this world alive
	//			The beauty of the darkest fantasies
	//		Poetry
	//			the beauty of perspectives, of paradox, of beauty itself.

	//	Desires:
	//		Storytelling
	//			I want the [] of the mysterious and fantasy.

	// meaning?
	//		Art beauty and aesthetic
	//			I want the awe of a natural world. like the witness, journey, flow, Abzu, animal crossing and breath of the wild.
	//			The beauty in style of minimalism just like journey, the witness, advance wars and any other truly refined game.

	// meeting social needs:
	//		The connection and intimacy you get when visiting your friends town in animal crossing.
	//		And I do not just want to channel the soul of games, I want to channel the souls of art in all of its forms.



	// show/teach the player:
	//		The ingenuity of using your mind to design fantastic solutions and provide help with your unique ability.
	//			Like in World of Warcraft, like in Runescape and social games like Habbo.
	//		To show that personal and character development is not a single trail just like in Runescape. Become the expert of whatever you want.
	//		To show that everything makes sense and is connected like in the witness
	//		To comment on myself through the work and be a metamodern art piece just like
	//			Mountains, Everything, the Stanley parable and the beginner's guide.
	//		To show that games can be impressive even on the browser just like all the fantastic flash games I grew up with.
	//		And that learning is a blissful experience just like told in doodle god, rhythm games, the witness, advance wars,
	//			Puzzle games, magnum opus, optimization games, RTS games,
	//		That the player has just as much creative force and influence as the developers
	//			like in Mario Paint, Game Maker, Animal Crossing, The Sims, 



	//	The empowering feeling of various forms of affects


	// Domains/things to get right and nail in seedling:
	//	Gloss and reflections
	//	Liquid and droplets
	//	Underwater movement of creatures
	//	Smoke and clouds
	//	Plants
	//	Trees and bushes
	//	Natural landscapes
	//	Swords and weapons and tools
	//	Animals beings and humans
	//	Cloth and fashion
	// Think about how these synergize and which are essential to create the good experience
	//
	// Essential
	//	Landscapes are 100% essential
	//		without landscapes freedom in movement.
	//	Rain and performance (much to render, needs creative cheating)
	//	Rain droplets and slickness, synergistic with types of smoke...
	//		and maybe plants!
	//	Trees are essential to show the complex extent of plant algorithm...
	//		to make individual trees comprising of many segent
	//			and recreate them.
	//	Animals
	//	Humanoid
	myValues: { //?
		purity: 1,
		innocence: 1,
		simplicity: 1,
		freedom: 1,
		comfort: 1,
		// ability to relax and be fully 100% without any worry
		safety: 1,
	},
	// Flow
	// Breaking expectations positively (better than expected)
	// Not breaking expectations negatively (worse than expected)
	// Expectations should be broken negatively if it is fair. aka not randomly. (player's judgment or action has to be wrong)



	toy: {
		is: {

			physical: 1,
			// 	A toy does only have constraints that are physical.
			// 	A toy is an object or a thing, or a set of things,

			usedInPlay: 1, //todo: rename lol
			//	Any thing that is used in play can be considered a toy.
			//	something you use in play.

		},

		//^ play-ground, context to play within. Something that is understood through play.

		// What toys should be:
		// Toys are made to be played with. (Their purpose of existance is to be interacted with for entertainment and learning)
		//		Good toys behave in a predictable but peculiar way. often demonstrating an interesting property of reality.
		// A toy should be safe to use without harm or physically irreversible consequences.
		// 	Many physical toys contains elements that can be either dangerous or sensitive but in those cases encapsulates and isolates these aspects from the player.
		//		Examples: batteries are on the inside and contained with screws that require specific tools to undo. 

		// virtual representations of weapons and tools in games are toys to the player, because they do not affect the player physically. 

	},
	startingCondition: {
		startingLocation: {
			// Should communicate the world
			// Conventionally starting locations are always the same in games 
		},
	},
	mediaProductPurpose: { //purpose of media products
		// 	[inspiration, synthesis of information]
		// 	poetic games (expression) 
		//		I think I like poetic games. interpretive experiences. non-verbal. Symbolic. 
		// 	Philosophical
		// 	[novelty]
		// 	Ambiguous.
		// 	expressive. 
		// 	[emotional awakening, maturing, connecting with emotions]
		//		I just want these things to happen to you though as a player.
		// 	All the things that happens in inspirational images on keep
		//	 I want it to just be as emotional as these characters.
		//	 Visceral, stimulating
		//
		// 	[Expression]
		// 	It is a game about expression - creative games
		// 	 [problem solving or training with system simulation]
		// 	Optimization games (refinement)
		// 	Socialize
		//		Stumbling upon others
		//		Hanging out and playing with friends
		//		Building relationships
	},




}
