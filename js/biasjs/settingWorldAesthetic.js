// Setting and theme: //and worldbuilding...
const settingWorldAesthetic = {


	stylized: {
		//artified... lol
	},
	realism: {
		// non-realistic vs realistic
		// accurate or true to life.
		// "down to earth" practical.
		// like the real or natural world. Strictly Physical.

		// replication of reality.

		// opposites:
		// unreal
		// fictional (based in mind and patterns. imagined)
		// 	made up. not functional in reality.
		// abstract
		// simplified. (not matching simplicity of reality by either making it too complex or too simple)
	},
	minimalistic: { //minimalism:
		// as simple as possible.
		// as little work as possible to construct.
		// reduced to the simplest shapes and forms
		// only dealing with basic forms and geometry (circle, rectangle, triangle, etc)
	},

	cool: {
		// fairly low temperature

		// calm movement or no movement
		// ability to stay calm under pressure.
		// not easily triggered or aggravated
		// being looked up upon.

		// being yourself and not caring how other people view what they say or do.

		// relaxing


	},

	dreamLogic: {
		//as opposed to reality's wakeful coherence
		is: {
			symbolical: 1,
			emotional: 1,
			not: {
				coherent: 1, //incoherent
				realistic: 1, //unrealistic
				bounded: 1, //unbounded
			}
		}
	},

	cartoony: {
		// using tropes seen in cartoons
		// applying a cartoon style and cartoon physics
	},
	cartoonStyle: {
		// of rendering and animation
		// rendered in 2d, often very stylized and abstracted and simplified.
		// characters are archetypal and made easy to understand.
		// like comic book but animated
	},
	cartoonPhysics: 1,


	edgyDeepProfound: { //I am 14 and this is deep or profound
		quotes: {
			//  "death hold no meaning in this world"
			//even if you kill someone you can't stop their existence
			//death doesnt exist here 
			//Death holds no meaning in this world
			//
			//you've experienced yourself that death is meaningless

			//I am stuck in this world. forever bounded.
			//Never to see the original again.
		},
	},
	becomingAMonsterNarrative: {
		//Maybe it should be about Becoming vampire
		//and learning how to be a successful vampire/Monster Lord
		//Or going in and out of this world as an outsider.

		//darker impulses taking over, hard to control self
		//split identity

		//dulling senses and emotions
		//becoming some gangster type.
	},

	// Themes... or like physical subject lol oriented around a single quality....
	miniatureWorld: {
		//The world is big relative to a human perspective.
	},
	insectPerspectiveGame: { // insect scale
		// You are small to the scale of an insect
	},
	beetleGame: {
		// you are a beetle in a small world. and you walk around hide under leafs when it rains and stuff. meet snails or something.
	},

	coronaGame: { //the experience of corona, for the corona video game mission by Gotland county
		// how is its experiences
		//	crazy news
		//	unclarity of what is going on
		//	people with different information and different strategies?
		//	stay away from work.    
		//	work from home.
		//	people look weird at you for having a mask
		//	people look weird at you for not having a mask.
		//	Do not travel
		//	Do not meet friends and family
		//	exponential numbers.
	},

	topdownAbstractStyle: { // topdown abstract style:
		//	Discrete colors
		//	Super rounded shapelanguage
		//		no sharp contrasts
		//	Buildings are one color each.
		//	Common map layout
		//		islands
		//		land
		//		rivers
		//		oceans
		//	Simplicity of layers... 
		//		water
		//		ground
		//		high ground
		//		beach
		//	It's pretty good if it is slick
		//	
		//	almost all are 2.5d in order to show houses from the side
		//		aka generally 100% topdown can look boring

		// the style is bright and pleasant to the eye with nice shapes and patterns.
		//		super slick, like nice popping animation, and it has this sorta "well-designed" style, like every professional designer has.

	},

	MonsterHunterMiceVsCatGame: {
		// Genre: Action Adventure Role Playing Game
		// Setting: MiniatureWorld, crude technology, antropomorphised animals.
		// Gameplay: Monster Hunter

		// Original pitch: miniature monster hunter
		//	A monster hunter game but you are small critters against normal pets.
		//	The player has to stop the house cat or something lol.
	},

	castlevaniaAesthetic: { // castlevania aesthetic???

		is: {
			aesthetic: 1,
		},

		// Genre: Action adventure

		// Setting: historic fantasy, mythological, "religious", dark (occultism?), super-natural
		// Themes: life and death, heroism

		// Aesthetic: historical, not silly, (not frightening, not disgusting)
		//		older: heroic/he-man, muscular brute, macho masculine energy.
		//		sotn & aos: bishonen, anime

		// Tropes and world: Dracula, Dracula's labyrinth castle, Dracula's tower, henchemen, vampire hunter family, exorcism, relics and magical items
		//		beating up bad guys.



		// Mythical does not really have to be mythical
		//	It can be just cool looking monsters or anomalous animals.
		//	
		// Fashion with weird mashups like streetwear + baroque wear
		//	Loads of straps and pockets (edge-wear)
		//		persona gear
		//		kingdom hearts

		has: {
			style: {
				is: {
					historical: 1,
					baroque: 1,
					medieval: 1,
					gothic: 1,
					museum: 1,
				}
			},
		},


		//	Castlevania Castle + castlevania style player character with all cool weapons and spells
		//	Something like the Aria of Sorrow just a character and bunch of weapon and spells to switch between.

		// Feminine/androgynous looking characters. (especially male main characters)

		// Diverse set of weapons, 

		// More eerie, maybe creepy, than scary or frightening.


		// Mythological beings.
		// SupernaturalBeings
		// Demon's
		// Monsters
		// Vampirism
		// Undead
		// Possessed objects

		// Blood
		// Creepy but not scary

		// Church designs

		// Magic
		// Souls and curses
		// Amulets and enchanted items

		// Symbolical stuff

		// The antagonist is Dracula, Dracula's castle, death, the force of evil and chaos.

		// Theme: contrast, opposites
		// 	It is about contrast between the divine and the cursed, the pure and corrupted. 


		// Setting: mostly indoors in castles
		// Usually one big main building
		// Divided into many different unique looking areas with their own aesthetic niche.

		// World/layout
		// Memorable discernable areas.
		// Rooms branch into other rooms

		// Mechanically uniqueness:
		// You can only save the game on certain safe rooms.
		// Camera always stay within the bounds of the room. (Clamps when close to walls) //true for many games. find a bigger context for this constraint.

		// As you progress enemies get stronger and harder to beat and more varied and the player finds more and more items and gear that are helpful for defeating new enemies.
		// As to create a good difficulty curve
		//
		// Some rooms are dedicated to boss enemies.
		// When entering a boss room you can not exit it until you have beat the boss (or lost to the boss and restarted at the save location)
		//
		// Specifically for my game:
		// 20xx fashion
		// Flashy cloth animations



		// Castlevania defined by dimensions
		definedByDimensions: {
			safety: "low",
			nurturing: "low",
			disgusting: "low", // high at times but generally not.
			Mythology: "high", //High mythology/story (vs practical/technical/realistic/logical)
		},
		// Castle Vania is not like harvest Moon because
		//	Nurture vs battle
		//	building vs destructing
		//	Tasty vs disgusting/gritty
		//	Demons do not need food
		//	castlevania is about old stories that linger (mythology)



		// Enemy ideas
		//	giant snake
		//
		//	modifiers:
		//		giant
		//		undead
		//		bloody
		//		cursed
		//		big
		//		powered
		//		burning
		//		ethereal
	},
	metroidvaniaGangsterAesthetic: {

	},
	vaniaGameAestheticsAndThemes: {
		// void
		// neon
		// blue glow
		// neonshine
		//
		// monochromous
		//
		//
		//
		// tech
		//
		// retro tech
		//
		//
		//
		// betong architecture
		//
		//
		//
		// grainy matte images
		//
		//
		//
		// baroque
		//
		//
		// flower patterns
		//
		// butterfly moth patterns
		// insect patterns
		// nature patterns in general
		//
		//
		// demons
		//
		// ghosts
		//
		//
		//
		//
		//
		// crying women
		// gold
		// blood
		//
		//
		// freeze framed intense/brutal moments (frozen time)
		//
		// Seeing all times at once. time exposure?
		// or just side by side 
		//
		// swords
		// big sword
		//
		//
		// impaling
		//
		//
		// forbinned things
		// pains pleasures
		//
		// naked bodies 
		// snakes
		// intense eyes and hands
		// sensing, touching feeling
		//
		//
		// intense sense of calmness
		// late nights, sunsets
		// slow moving clouds, constant moon
		// non movement.
		//
		//
		//
		// angels
		//
		// the sacred
		// the ... forgetten... what is called, the punished? the banished? forsaken.
		//
		//
		//
		// slowly falling, dying giving up. Accepting death
		// A sense of continual and slow descent. endless traversal.
		//
		//
		// desolate spaces.
		//	like a humming background white noise or brown noise or whatever.
		//	
		//	
		//
		// computers looking like plant life
		//
		//
		//
		// like isolated. seeing explosions and fire from afar. not feeling the impact just observing like a ghost.
		// detachment?
		//
		// A lightness of the body and clothes almost like ghost or like underwater
		//	light like feathers. almost not real.
		//	
		//
		// ruins, remnants of beatiful things and patterns now beig forgotten.
		//	slowly erroding over time.
		// the beauty of abandonment. of a hous being drowned in sand
		// or a castle fallen apart and overgrown
		//
		//
		//
		// something about statues and pillars that have this feeling of stuckness.
		// I know pillars symbolize stability but it also just feel like stuckness and inability to react in any way.
		//
		//
		//
		// feminine looking men and women
		//	they look like beauty ideal of body types
		//	
		//	
		// being that remain even after being scorn.
		//	like dying happens to them but it doesn't mean anything to them.
		//	because they are beyond it.
		//	not necesarily in this undead clingy sense
		//	but in a meditative determination sense.
		//	or a bringer of ruthless truth sense.
		//	
		//	
		//	
		//	
		//	
		//	
		//	
		//	
		// maybe it is not even like a game world
		//	but more like an abandoned world that you are free to roam
		//	a bit like the witness with less of the puzzling part and more of the observing part.
		//	
		// All that is left to do in this world is to take upon yourself challenges if you wish to
		//	build a sand castle
		//	Start a fight club. some sort of competition out of boredom.
		//	
		//	
		// the realization that everything in this moment is futile and will not last.
		// and that no matter what you do things will change.
		// and that it is like this moment is frozen in time in presence of the knowledge that everything is going to change.
		//
		// Like the present moment is just you revisiting the past, or rather points in space time.
		//
		// In a world where it ultimately doesn't matter where you are, who you are or how you drift through it.
		//
		// But still this, this moment is evidently here.
		//
		// and the realization and recognition that there is no difference in essence between other people and you
		//	you are them and they are you, just choosen to express the same in different ways.
		//	
		// This expressed mutual underlying understanding that this is the case.
		//
		// and the agreed upon evident lie when we act as if this rule is broken.
		//	drama and acting.
		//	
		// This game is a long and thoughtful shower
		//
		// A slowly enjoyed cup of coffee.
		//
		// "It is weird. You feel so at peace when you're old" howl's moving castle.
		//
		//
		//
		// ^ the intense feeling of youth is almost an anthitesis to this
		//	
		// a spreading like sunrays and patterns from a single converging point
		//
		// whispful patterns like smoke or veins or tree stems
		//
		//
		//
		// Maybe in this game you die over and over and then you are reborn in different worlds
		//	in different contexts.
		//	
		//	You go through many miseries. and you observe them taking place slowly.
		//	
		// you the player is the one being reincarnated in different contexts.
		//
		// some timkes waking up, in a hospital bed, or driften up on the beach.
		//	laying in nature.
		//	misty forest bed.
		//	as a ghost rising.
		//	a homeless person on the street.
		//	
		//	you find yourself walking out of a fog in a desert.
		//	
		//	then you're a wolf
		//	A bird
		//	A snail?
		// ^ interesting fading in and out of subjective experience.
		//
		//
		// what I don't like with this is the sense of given up - ness
		//	which might make people not even try.
		//	
		// What I want it to be is maybe more of a break, that will let you get back to doing meaningful things in the world.
	},
	vaniaRelatedConcepts: { //TODO probably merge with above "vaniaGameAestheticsAndThemes"
		//being cursed, being forsaken, being turned into a monster, 
		//
		//losing control, becoming dangerous, powerful, 
		//
		//
		//living armor/object
		//
		//monster plants.
		//
		//
		//demons, imps, 
		//
		//shadows
		//
		//
		//ghosts and spirits.
		//
		//
		//animated skeleton
		//	undead, it's already dead yet it is animated.
		//	maybe the soul has long gone.
		//	All that is left is the structure.
		//	A ragdoll.
		//	Doesn't need muscles when it can move through supernatural means.
		//	ghost force. it is what allows them to move.
		//		imbued with otherwordly spirit
		//	
		//	
		//	demons can only live in their own natural environment. Only indirectly touching reality.
		//	they need protection of spells to enter this world.
		//	
		//	
		//	
		//the confident edgy cool man.
		//	in this world you only fight with other cool people.
		//	we don't have time to fight with the lame.
		//	everyone has this aura of confidence.
		//	standing up among all disorder
		//	the eye of the storm
		//	organizing principle of their environment.
		//	
		//	
		//	
		//For every world that is thriving there is a world that is void and emptied of all life force.
		//
		//
		//
		//
		//upheld by magic, upheld by fire, upheld by sacrifice,
		//	it's only temporary
		//	everything eventually runs out of luck
		//	
		//
		//
		//
		//
		//
		//good luck charm
		//
		//the buildings that we live and work in, the things we buy at second hand or thrift store.
		//
		//the left over bits of architecture and structure, that suggest it's previous use, has a kind of hidden or occult quality to them.
		//
		//(things that are obscure or incomprehensible seem to suggest something whole that is beyond what you find everyday. 
		//
		//
		//building structures that separates us out or protect us from nature or the natural order
	},
	metroidvaniaStyleSplitting: {
		// splitting up metroidvania:
		//
		// dark themes in general.
		// dark narratives.
		//
		//
		//
		// this romantic idea of slow and swift movement.
		//	matrix movement.
		//	flowing hair
		//	slowmotion
		//	gentle
		//	
		//	beautiful battle.	
		//	
		// slow and detailed collapse.
		//
		//
		// abandonement -= the gods that left us long ago.
		//
		//
		// living harsh conditions, non-abundance.
		//
		//
		// everything that you don't want to let go of.
	},
	mellowVania: {
		// Mood
		// Kinda the vampire spirit.

		// Has to be mellow
		// No sudden stimulating action
		// No big explosions
		// No instant sudden sequence of events
	},

	CatlevaniaStyleDevelopmentTakeAways: {
		// Aura of seriousness.
		// Unique Settings cool: future.
		// refreshingly minimal color palete (pastel and brighter...)
		// brightness creates this futuristic minimalism
		//
		// USE RAINBOW COLORS and cycling through rainbow colors for vfx.
		//
		//
		// soft youthful and efeminate quality
		//
		// swift main character animation
		//		swift weightless and graceful
		//
		// Clear and readle graphics. cleanness.
		// gradient.
		//
		// ORNAMENTAL
		// delicate beauty and artfulness.
		//
		// beautiful marble tile-work, grand columns, rich drapery, priceless works of art.
		// a world of commanding gothic architecture.
		//
		// hollywood quality 90s cinema feeling.
		//
		// Every composition, every place, has a clear curated colorscheme for the scene.
		//
		//
		// brilliantly fluid enemies. (and animations in general)
		// smart reacting enemies. They idle and then they notice you.
		//
		// slower gameplay - (more strategic)
		//	logner and more detailed animations
		//	^(they realized the importance of flowy animations.
		//
		//
		// Fuck scoobydoo and shonen.
		// fuck grittiness and disgusting gore.	
	},

	castlevaniaFightClubGame: {
		// A metroidvania, but it is like a 

		// Demon fight club
		// All demons are against each other in a chaos architecture realm.

		// turf wars.
		//
		// You visit different demon's living rooms and fight them in duels.
		//
		// You claim your own space in the castle (the realm of ideas?) (like a room in all of the chaos architecture)
		//
		//
		// and this might work well with multiplayer! which would be really cool.
		//	because it is like all about magic and stuff and you can "merge realities"
		//	with the people you want to play with.
		//
		// It is sorta like Dracula's castle
		//
		// You look for powerful artifacts and to capture or tame powerful spirits.
		//
		// You walk between worlds.
		//
		// and the levels are procedurally generated, but still seeded
		//	so you can always find what your way back to the same areas.
		//
		// this game needs to feel genuine thought, like symbolical
		//	and like it was designed well, all the creatures etc.
	},

	demonMagicSpiritGame: {
		//	It has like a slickness and gradience to it
		//		luxury,
		//		fashion
		//		stylish
		//		expressive
		//		CLEAN
		//		fresh (like fresh air)
		//		still human
		//		subtle
		//	a lot about what it is not
		//		not too shallow
		//		not too girly
		//		not too messy
		//		not too emotional

		//	video game
		//	what is important in this game?
		//		that you can create your own expressive character
		//		that everything is slick like described above

		style: { //(as observed as the core of the examples from Google keep as of 2021 February)
			//	is matte
			//	bleached out
			//	colorgradients of cmyk color
			//	Clean (opposite of gritty)
			//	flow - unhindered movement, ease
			//	lightness, light bodies, light movement 
			//	neonshine
			//	beige, peachplum
			//	slick and sleek
			//	supernatural / holy
			//	creamy soft
			//	enhanced natural landscapes. - (landscapes that reflect most of the other properties.
			//		natural beauty
			//	nature that is framed to look like a artistically made composition.
		}
	},

	untitledGameAboutDying: { // experience / feeling
		//	A game where death is so commonplace that no one in the game even reacts to it
		//	but it is like so "holy fuck" for normal people that it is completely mesmerizing.
	},

	someMythologicalGame: {
		// The mythology of Howl’s moving castle and Castlevania (Aria of Sorrow)
		// Ghibli movie feeling in terms of symbolism theme and animation
		//
		// Symbolic focal points:
		//	Dreams
		//	Sky
		//	Water
		//	Space
		//	Nature
		//		Wind
		//		Growth
		//		Rain
		//	Desert
		//		Endlessness
		//	Death and graveyards
		//	Magic
		//		Supernatural
		//		Electricity, frost, fire, wind
		//		Gravity, movement and space bending
		//	Creature and being
		//		Layered
		//			Skeleton
		//			Fluids (bloods )
		//			Meat
		//			Skin
		//	Hair and cloth (for wind)
		//	Dream context
		//		Relentless shifting and transition of theme, often in unnoticeable fashion
		//		Rewatch inception
	},

	bonesGame: {
		// Everything is bones and skeletons
		// It is a kinda silly style, like a cartoon drawn bones etc.
		// The whole game is gray scale. all bones are white and most background is black
		// Game-play is a bit like Mario paint, but I guess it is more about composition of bones or something
		// Something like skeleton jokes
		// Something something playful poetry about death.
	},

	darkForest: { // for dark games or castlevania-esque
		//		forest of darkness. swamp, dangerous nature.
		//			beast land.
		//			dangerous nature.
		is: {
			place: 1,
			area: 1,
			biome: 1,
			//don't know which of the above is really true
		},
	},


	occult: {
		// Dark and hidden knowledge/magic
		is: {
			culture: 1,
			aesthetic: 1,
		},
		// the occult stuff feels exciting and unnerving because of it's seeming autonomy.
		// Done too it's own motives, in a way that feels foreign or incomprehensible.
		//	
		// if a game would be about this feeling, is it possible that the player is the generator of such things?
		//	is it possible to have a system? Where the player does rituals systematically?
		//	and will that be counterproductive to the feeling of the game?
		//	
		// You have to always kinda not know what is going on.
	},
	mystery: { // add or connect with mystery.
		// Inaccessible content:
		// Things that exists that you can't reach
		// Locking objects up in boxes(chests), mazes and stuff to make them inaccessible.
		// There can be rooms in your castle that are inaccessible.
		//		containing secrets you are not poweful enough to behold.
	},

	randomIdeasforMetroidvaniaGame: { //TODO: put these somewhere else!
		//	narrative or trope ideas for metroidvania-esque, dark or creepy games:
		//
		//a room full of people standing one by one, with serious face.
		//	talking to them all you see is "..."
		//
		//
		//maybe the castle is its own entity and you only serve it or have a relation with it rather than being fully in control.
		//	a contract, an agreement
		//	
		//	
		//
		//
		//
		//
		//Beasts - faction of fictional beasts, that are not necessarily grim.
		//	unicorns
		//	griffin
		//	lion with wings
		//	
		//	
		//guarded and protected. constantly surrounded by servants.
		//
		//
		//
		//
		//I love ancient robots, always add ancient robot technology.
	},


	losingControl: {
		// I guess this is an experience or aesthetic or feeling???
		//can be seen as a dark thing, especially if something other seems to control you. then it has this sinister hypnosis or manipulation vibe.
		//idea for castle games - as your castle grows you lose control over it. It becomes more and more chaotic.
	},

	castlevaniaCastleStyle: { //The style of castles for castlevania esque games
		//		what is given in the castle style:
		//			dark
		//		parameters of castle style:
		//			alien/zerg-ness
		//			sci-fi-ness
		//			makeshift/scrappy vs perfect
		//			plain vs decorated/intricate
		//	
		//	surrealness
	},
	ripperGame: { //a game where you are the ripper (with cool personality)
		// A game where the player character is the ripper who has to meet characters and bring them to the other side.
		// This ripper has cool personality
	},
	ghostGame: {
		// Make a game where the player Character is a ghost, walk through walls and watch people in houses as they do their daily life
		// But the ghost can not interact with the people. so it is like a walking game about just observing people.
	},

	ThemeOfHoldingOnTooMuch: {
		// the game could be about the trauma that makes you hold on to your dearest possesions too much, and then the release of letting go of everything
		// a trauma that makes you hold on to your dearest possesions
		// and that is the root of the problem
		//
		// and then the release is letting the castle get obliterated into nothing.
		//
		// the mini list
		//	player
		//		move jump attack
		//	enemy
		//		walk left right
		//	you can punch enemy
		//	you are in a room
		//	enemy spawns
		//	you get stunned when hit
		//	enemy dies when their health is too low.
	},


	incomprehensibleAesthetic: {
		//the unknown and unexplainable.
		//	incomprehensible.
	},
	symbolsOfEvil: { //symbols of evil
		//	goats
		//	snakes
		//	poision
		//	blood
		//	horns
		//	skulls skeletons
	},


	liminalSpace: { //the aesthetic of a place presented outside its intended or normal context

	},

	acwwHomelyCozyFeeling: {
		//homely cozy feeling of decorating your home in acww but 2d sidescroller ofc - add seed
		//I want to make the homely cozy feeling of decorating your home in ACWW
		//	but 2d sidescroller ofc
	},


	windEnvironment: { //windWorld, or wind... area
		// wind having a creative and destructive role in the world.
		//
		// wind funnels, predictable wind streams. habits of the wind.
		//
		// in a world that is often windy, it must be nice contrast to get into a house
		//	where things stand still.
		//	indoors house
		//	wind shelter
		//	cave
		//	underwater being unaffected by wind!
		//
		// wind based life forms
		//	birds
		//	flying squirrels
		//	flying snakes???
		//	more flying animals lol
		//	flying fish?
	},
	windCulture: {
		// a culture adapted around the wind of the environment.
		//  society is adapted around it.
		//wind that goes through buildings and man-made structures.
		//wind technology
		//	using it when needed, like sails tame the wind.
		//	sails
		//	paraglider, aeroplanes/gliders
		//	windmill/spinners
		//	controlled wind flows
		//	controlled wind funnels
		//		wind funnels in the air, which makes air-glider good (paraglider)
		//	airpressure? (balloons, blowing bubbles.
		//	nets and air fishing.
		//		catching things in net that fly towards you at random because of the wind.
	},
	beautyOfTheWind: { //things that are/look cool in the wind. (Things that are animated by the wind in interesting ways.)
		// lines and ropes
		// ropeladders
		// airbaloons? zepelines?
		// things hanging on lines.
		// wind spinners/windmills, wind turbine
		// witches on flying brooms... look very good in wind.
		// dripping watter and water flows.
		// vindspel - wind chimes
		// wind flute
		// sails
		// fluffy animals
		//	sheep.
	},
	// I do not want wind to be a hindrance or hazard to the player or experience
	campfireGame: {
		// campfire 
		// you hear the wind blow through the grass and trees. the fire beside you is crackling and glowing dimly you see the smoke rise and disappear\ in the darkness
		// the light going in all directions and casting strange shadows
		// it fades to black as you drift away into sleep
		//
		// you are slower if you are cold and tired and do not eat. or if sick. these dynamics makes sense in a game bout wandering and surviving in the wilderness.	
	},
	medievalNordicGhiblitimeAesthetic: {
		// A medieval Nordic ghiblitime aesthetic with tundra and grasses that blow in the wind.
		//	medieval - undisturbed nature, old fashion, no technology,
		//	nordic + medieval = possibly runic scriptures in stone. a
	},
	forestGameThemes: { //unique and separable forest game themes...
		// Forest man game
		//
		// Robinhood, BOTW, king Arthur, Ghibli? adventure time.
		// Kidlike playful adventure? or young adolecense 14 - 17? or like 20?
		// Big forests. fantasy
		//
		// Building tree houses, building swings, 
		// Building cottages
		//
		// Swedish (Nordic) forest
		// Wildman, heavy packing,
		// About walking far
		//
		// Modern era forest cottage/settlement. ("I bought a house/plot in the forest" the game)

		// Scandinavian Forest walking simulator
		//	Not to furry. Think at most like Mumin
		//	It is bounded to Scandinavia landscapers forests and sound
		//	Ronja Rövardotter
		//	Ghibli time
		//	Skogstroll?
		//	The activity in the game is mostly to walk though the forest. Maybe take breaks and start a campfire.
		//	Fridfullt
		//	Bird song
		//	A bit like that bird hike game I guess
	},
	plantSpiritsGame: {
		// Plant spirits game.
		// Snofi basically...
		// Hattifnattar, soots in ghibli..., mushishi
		// Petals flying in the wind 
		// Full of life and spirit
		// Mushrooms, and eyes hiding behind the leafs.
		// Deep in the forest filled with tiny mice and  
		// Shadows of fish in the rivers and ponds,
		// The game play would just be walking through this world and interacting?
		// 	like in Proteus.
		// Maybe some inspiration from Zelda with the fairies and fox spirits or whatever.
		//	Kokoriki forest...
	},
	nurtureTheNatureGame: {
		// you take care of nature, plant seeds and see nature grow back from desolation.
		// as it grows it becomes more colorful and interactive. and nature starts liking you and talking with you.

		// caretaking caregiving
		// needs management
	},


	medieval: { // epoch based aesthetics. (technology based mainly.)
		// people lives in either wood or stone buildings.
		// stone buildings are upper class.
		// castles are the most upper class.

		// churches are important.

		// people fight with bows, spears, axes, swords, shields, metal armor.
	},



	hotel: {
		// A place where you pay to borrow a clean room to sleep in. 
	},


	tamagochiButPlantGame: {
		// A tamagotchi but it is a plant.
	},

	pottedPlantSimGame: {
		// Things that exists:
		//	Window (a view mode to view a room to see potted plants)
		//	Window browser (A way to find public plants)
		//	Rooms (place where the player can have a set of potted plants)
		//	Shop
		//		where the player can buy supplies
		// Plant truths:
		//	It grows differently whether or not you have it in the sun
		//	how much you water it, where the player water it, what type of plant it is, if you cut it, the quality of the water.
		//	if the player breathes nearby it. lol.
		//	The genetic (random) code that makes it unique. If you use a growing stick, what pot you use.
		//	how the water drains in the bottom, what soil you use.
		//	You can like get seeds and trade them and stuff. gotta catch them all, and they mutate and stuff.
	},

	hideAwayGame: {
		//		A game that is a secret backyard or house where the player can go to when you feel that you need space and need to be left alone.
		// like Brie's thingy, but maybe like your own world
		// a bit like animal crossing.
		// you go back from the real world to rest and renew yourself.
		// to wash bad experiences of and bathe in a beautiful landscape.

		// shelter, safe haven,
	},

	// theme/premise
	SideCharacterGame: {
		// This is about the the dimension of centrality of character.
		// Is your character central to the story?
		// The player character the major influencer in the game?
		// Is the game focused, intentionally on the player character, or something else?

		// A game where the player plays as the side character
		// You only play a supportive role to the protagonist.
		// You live as a helper of some sort to the main cast of the adventure/story
		// Experience:
		// the player feels as if they are not the most important person.
		// the player feels as if they are not the most skilled or best character.
		// That would be interesting, I think.
	},

	nostalgiaGame: {
		// A time travel game about nostalgia and inability to go back where you have forever changed the world
		// and where the world is forever different from the first time.
	},

	underwaterGame: {
		// underwater world game
		//	colorscheme bluepink
		//	corals, girls, hair, swimming.
		//	floating movements
	},
	underwaterConventions: {
		//		distorted lighting
		// low pass audio
		//	Being underwater makes everything muffled and silences the noises of rain and wind.
		// bubbles
	},

	magic: { //causing physical or metaphysical change. (in like an unconventional shortcut kinda way)
		// unexplainable powers, usually about controlling different elements
		// domains
		//	elemental
		//		fire
		//			heating on touch
		//			drying things out
		//			sticky fire
		//			fire ball (throwing fire)
		//			fire spray
		//			non-stopping fire
		//			circle of fire
		//			fire trace where you walk
		//			
		//		electric
		//			bolt
		//			lightning
		//			zap on impact
		//			charge item
		//			
		//		water
		//			water or liquid bending
		//			controlling dryness/wetness??
		//			producing water
		//			breathing underwater???
		//			
		//		ice
		//			freezing
		//				on touch
		//				cloud
		//			producing icicles or snow balls
		//			creating a blizzard
		//		wind
		//		earth
		//	"dark"
		//		invoking spirits or gods
		//		curses
		//		soul magic //trapping spirits and stuff
		//		blood magic
		//			animal and human sacrifice
		//			the power comes from spilling blood
		//			sacrificial
		//		necromancy
		//		imbuing power
		//	healing and restoration
		//	time
		//	spatial
		//		// teleport
		//		// portal
		//		// television
		//		// telekenisis?
		//		// levitation
		//	gravity
		//	psychic //mental
		//		causing sensations
		//		reading mind
		//		mind control
		//		supernatural
		//		//nose bleed
		//	luck
		//		luck can often be emulated by skilled gamblers and foolers.
		//		//more favourable game-play randomization
		//		//encountering more favourable circumstances
		//		//luck is in the world though... like leperchauns, rainbows
		//		//it is in material, 
		//		//four leaf clovers are found by the lucky and maintains luck wherever it goes.
		//		luck might reduce unpredictability, but not necessarily.
		//			I think you can still be lucky but in unpredictable ways.
		//
		//		//luck especially controls
		//		//drop rates
		//		//critical hits
		//		//positive chain reactions
		//		//avoidance of critical failures, and bad chin reaction
		//
		//		// A common trope is that luck in controlled by the gods of faith.
		//		// (see Random Number God or RNGesus, random number generator (rng))

		//		// what happens to you is based on gods judgment or on karma.
		//	music
		//		listening gives you effects.
		//	alchemy
		//		transmutation of elements...
		//	relic magic
		//		knowing how to use magical mechanical artifacts long forgotten by the modern people
		//	demon
		//		summoning spirits from other realms.
		//	white magic:
		//		holy magic
		//		light and blessing, purification, anti-dark, purging of evil spirits
		//		giving pain to those who deserve (evil)
		//	illusion
		//
		//		creating and modifying sensation
		//	witchcraft?
		//		moon cycle and timing magic
		//		brewing and magic potions
		//	witchdoctor?
		//		voodoo
		//		plagues
		//	nature (druid)
		//		plants
		//			talking with plants
		//			growing plants
		//		Animals
		//			morphing into animals,
		//			talking with animals
		//			taming the wild
		//	realm
		//		//connecting with other sides and other worlds
		//	property
		//		//(turning people into rubber)

		//A magic system (from another earlier note....)
		//    Elemental
		//        fire burning
		//        ice freezing
		//        electricity energy
		//        Earth, sand, mud, rock
		//        water and liquid
		//        wind and smoke
		//    life magic 
		//        Animal
		//            charming/taming 
		//            becoming (werewolf etc)
		//        plant magic
		//            growth
		//            nurture
		//    unholy?
		//        shadow
		//        curses
		//        undead
		//        blood
		//    Chaos magic
		//        randomness
		//    spirit magic
		//        ghosts
		//        spiritual protection
		//        synsk
		//        possession?
		//    Psychic
		//        read minds
		//        supernatural phenomena
		//        aliens?
		//    energy/enchant magic
		//        crystals
		//        enchantments
		//        creating potions and objects
		//    object?
		//        light
		//        telekinesis
		//        moving objects
		//        teleportation

		// weather magic, you can see the weather forecast (because it is simplex based)
		// magic to see into the future (the simplex generation)
		// magic to watch places remotely
		// magic to cast your spirit to a location and to simple ghostly interactions
	},


	// Fashion
	streetWear: {
		// A style of fashion
		// Streetwear - what you see on the streets yo
		// Sharp edges
		// Usually a bit big
		// Hoodie almost obligatory
		// Hoodie is usually very stable
		// And they have particular shoes
		// Usually clothes have discrete colors
		// Usually black, white, red, dark blue. or other dark colors.
		// Can also be intense red..
		// Similar styles:
		// 	Snowboard
		// Possible accessories
		// 	Cap
		//	Mössa
	},


	casinoAesthetic: {
		// abundance
		// high contrast
		// service
		// guarded
		// leisure
		// relaxation
		// high profile people
		// concentration of wealth
		// scams. trickery
		// covering up underworld
		// surface,
	},
	hotelAesthetic: {
		// hotel lobby 40s casino aesthetic
	},


	artisticEpochs: { //partly zeitgeists!
		//classical antiquity
		//	pillars in Rome.
		//	they found geometry and math..
		//	obsessed with building circles, triangles and rectangles.
		//	good sculptures
		//	everything is made out of rocks
		//	staircases everywhere.
		//	arches
		//	the wheel
		//gothic art
		//	medieval
		//	gargoil:y
		//	colored class panes
		//	tall pointy towers.
		//	the tall echo:y church
		//	meshed glass panes.
		//	mythological
		//	pointy and bone like
		//	intrinsic goofiness
		//	kinda 2d, feels very flat
		//	uses few colors
		//	no sense of perspective (things far away are smaller and lines go to horizon)
		//	simplistic in a kind of nice sense. Does not feel like they try to overcompensate for something.
		//	kinda good enough..
		//	"just let someone do it."
		//	kinda feels like the French froginess comes from here.
		//	feels kind ineffective, like "oh we build armour in metal and shit."
		//		like the stuff that kinda work as a hack
		//			but it is like easy to win over with some realism and critical thinking.
		//renaissance
		//	renaissance means rebirth, and they tried to cut with the old ways.
		//	take the art of classical antiquity but northern Europe methods and with science!
		//	the development of new techniques and new artistic sensibilities
		//	increased awareness of nature, a revival of classical learning, and a more individualistic view of man
		//high renaissance
		//	"exceptional artistic production"
		//	adding realism to your art.
		//	finding tools and methods and theory to depicting reality in accuracy.
		//	proportions, balance, and ideal beauty
		//mannerism
		//	quirky realism
		//	they were so bored of realism that they criticized it
		//	by exaggerating proportions, balance, and ideal beauty
		//	asymmetrical and unnaturally elegant.
		//	"intellectual sophistication"
		//	"I have such control I can make art out of realism."
		//	hyper-realism (clear exaggerating)
		//	quirky clothes and human proportions.
		//	no real focus on gloss or drama.
		//	(the gloss only manifest as depiction of real things, golden amulets, shield, vase)
		//	it is like they started adding quirks to realism??
		//	somehow this feels like meta-modernism....
		//baroque
		//	create dramatic art that shows how rich and powerful the church is
		//	in order to get people back from losing faith.
		//	dramatization of bible stuff.
		//	added realism
		//	columns in buildings that make shadows and are dramatic and symbol of stability.
		//	A lot of repeating patterns like domes and towers and symmetry. circles
		//	rows and rows of pillars, geometrically precise windows and stuff.
		//	spirals and scroll shapes like "mastery of nature shapes"
		//	dramatic statues.
		//	Frames that are as important as the window/painting
		//	gold pearl marmor
		//	clams, leafs, cloth, scrolls, banners, golden flowers and plants.
		//	almost naked people. soft skinned
		//	huge "gardens" geometric grass fields with paths...
		//	ornaments.
		//rococo is like what it means to be rich
		//	we are happy wamen in heaven.
		//non-drama, pleasure
		//	depicting lazy meaningless people doing nothing of importance.
		//	glossy and flowery for sense pleasure only
		//neoclassical
		//	clear drawings
		//	realism
	},

	pixelArt: {

	},
	anime: {
		// a rendering style with its own patterns, tropes and abstractions.
		// Techniques for animation developed in japan
		is: {
			style: 1,
			technique: 1,
		}
	},


	// styles / aesthetics?
	jomon: {
		// ancient
		// Clay 3d objects, gray mono colored.
		// Mostly clay figures or clay vases..
		// Useful objects but with parallel grooves making a sort of psychedelic look.
		// Parallel lines bulging out of the material and going straight, in waves or spirals.
		// Very adorned, kinda intricate. but not super perfect proportions.
		// Kinda looks natural, imitating root like patterns

	},
	// Gecko lizards are cool. Especially with cool patterns in them. Sorta like Australian mythology stuff and cool spiral patterns etc.
	//	Like Mayan. The ones Zelda uses a lot

	WabiSabi: {
		// A form of beauty
		// An aesthetic style that embodies naturalness, simplicity, and subtle imperfection. 
		// • In sixteenth-century Japan, a student was tasked to tend the garden. He cleared the garden of debris and raked the grounds. Once the garden was perfectly groomed, he proceeded to shake a cherry tree, causing a few flowers and leaves to fall randomly to the ground. This is wabi-sabi. 
		// • Wabi refers to beauty achieved through subtle imperfection. Sabi refers to beauty that comes with the passage of time. 
		// • Wabi-sabi runs contrary to many innate biases and aesthetic conventions (e.g., preference for symmetry).
		// • Consider wabi-sabi when designing for audiences with sophisticated design sensibilities. Use elements that embody impermanence, imperfection, and incompleteness. Favor colors drawn from nature, natural materials and finishes, and organic forms. 
		// See Also Desire Line • Ockham’s Razor • Zeigarnik Effect

		// Deborah Butterfield uses found pieces of metal and wood in her horse sculptures. Equine wabi



		//	wabi-sabi
		//	Do the things that high life has perfected but through simple and natural means.
		//	
		//	You can still work with simplicity, impermanence and imperfection.
		//	
		//	things that find a way unevely, like nature.
		//		an evolved just good enough solution.
		//	things that show touch of time. signs of decay, of wear and tear or damage.
		//		but that is still functional and alive. 
		//		
		//		
		//		
		//	the more we try to perfect something, the more rigid or fragile or simplistic it becomes.
		//		the sharper the knife, the easier it is to dull.
		//		it is easier to carry an empty cup.
		//		The more wealth you have the harder it is to protec
		//		
		//		
		//	You can't prevent nature from doing it's thing.
		//	
		//	
		//	
		//	we human are never perfectly still. imperfection is a natural state.
		//	
		//	it is the beauty of imperfect perfection.
		//	
		//	self improvement is ok as long as it is within reason and without a goal of perfection (reaching statically defined state)
		//	
		//	
		//	disatisfaction is when we refuse to come to terms with reality.
		//	so come to terms with EXACTLY THIS.
		//	Don't come to terms.
		//	
		//	
		//	everything is in an imperfect transient condition.
		//	
		//	
		//	is there anything more perfect than a consistently imperfect unfolding of nature.
		//	
		//	impermanence, suffering and emptiness	
	},

	baroqueInteriorDesign: {
		// Lots of statues
		// danceHall
		// livingRoom
		// diningHall
		// plantHouse (indoors but glass)
		// backyard / garden
		// outdoor areas:
		//	fountain.
		//	castle entrance.
	},


	beeAndPuppyCatAesthetic: {
		// Bee and PuppyCat die or fall apart like a emotionless robot
		// Game where the player character dies but they are a robot like Bee in Bee and PuppyCat
		//	And it is visually stunning as parts break apart.
		//
		// You wake up in a bed, you turn on light and you are in a house.
		//	You go out of house and you are on an island near Iceland.
		//		It is only you, a field of grass and some sheep.
	},
	vaporwaveAesthetic: { // vaporwave A E S T H E T I C
		is: {
			aesthetic: 1,
			style: 1,
		},
		//		cynical
		//		eerie
		//		Japanese high life
		//		luxury as an empty promise, surface level
		//		post-modern / meta-modern.
		//		Nostalgia
		//		Utopia
		//		mocking perfection.
		//		Even when ticking all boxes its not utopia.

		//		shopping mall
		//		Miami
		//		90s computers, technology and consumerism
		//		80s 90s computer desktop
		//		sculptures of philosophers.
		// palmTrees
		// dolphins
		// pretty plants, like baroque
		// pools
		// japanese text
		// stylized english text with big letters separated by spaces A E S T H E T I C
		// abandoned malls and escalators.
		// ms paint
		// a lot of japanese like cityscape underground druglord something....
		// california?
		// miami
		// beaches
		// greek statues
		// sunsets
		// cloudy skies

		// low poly 3d graphics
		// retro

		// often silvery gradient like colors
		// bluepink
		// green against pink
		// maybe even green blue and pink... silver gold
		// pastel

		// Consumerist culture
		// things that are shiny but contains nothing but emptiness, empty calories.
		// the eerie promise of a future we did not have
		// due to late stage capitalism??? 

		// holographic
		// slick

		// bad camera footage with noise and rgb

		// glitchy softwares and old windows logos and sounds.

		// Japanese culture
		// Japanese waves
		// Tron
		// neon everything
		// pixelart??
		// glitter
		// glossy reflective surfaces
		// clean smooth architecture.
		//	like either squares or circles/slick
	},

	nonAbstractNatureGame: {
		// See principleOfNonAbstraction
		//		a non-game like simulation of pure nature.
	},
	principleOfNonAbstraction: { // principle of non-abstraction
		// TODO add/link to game design constraints and aesthetics and needs.

		// A game as a counter reaction to the trend of relying on abstractions in video game
		// Which makes video game worlds feel mechanically constrained and dull.

		// In abstract games there is no fluidity.
		// It is just abstract and biased, like, "choose one of these 5 cards" or "select one of these dialog options.

		// Goal: Create a game, world and simulation that feels “real” or “organic” compared to today’s standards.
		// Aesthetic goals: feels "real" or "organic"
		//	and not "just like a game"
		// Show that the digital can be organic
		// (can manifest water element..)

		// This is a game of non-pattern
		// It is raw, as in unprocessed.
		// It is analog
		// It is fluid
		// It is ruthless
		// It continues to mutate without you

		// Non abstractable:
		// And there are no things of description
		// All describing is player interpretation of what really is. extrapolation. 
	},


	psychedelic: { //style? state of mind? theme?
		// Mimicing the effects of taking drugs like LSD or other hallucinogens
		// relating to or denoting drugs (especially LSD) that produce hallucinations and apparent expansion of consciousness.
		// a "trip" is know as a temporary psychedelic experience of altered state of consciousness
	},



	beach: { //???
		//Beach
	},




	aesthetic: {
		//What is the process of deciding the appropriate aesthetics? (or lock parts of its qualities.)
		has: {
			beauty: 1,
			//sensePleasure: 1,
			consistency: 1, //internalConsistency: 1, //or just consistency?
		},
	},

	pixelArt: {
		// Low resolution axis aligned graphics.
		// Art that looks good on low resolution screens.

		//commonly accompanied by limitations in colorscheme as to emulate older times and hardware limitations.
	},

	realism: {
		// realism often comes with detail, and detail (that follows rules of beauty) makes things look stunning.
		// todo merge with the other realism or whatever I called it
	},
	fantasy: {
		// "subgenres"
		// dark fantasy
		// high fantasy
		// cartoonish fantasy
	},
	highFantasy: {
		// It does not take place in "our" universe. aka you wont find boston in it.
		// It is not imagined to be this earth culture. does not pull on any of our historical events.
	},

	lowfantasy: {
		// it is definitely here and now in this world but no one knows about it.
		// harry potter, and secret.
	},

	// urban fantasy, taking place in the city. dresden files
	// vampire fantasy.

	dreamy: {
		// dreamlike
	},

	cuteness: {
		// has to do with affection, adoration and love.
		// oxytocin...
		// cuteness is a positive trait.
		// kinda the opposite of disgusting....
	},


	// Qualities of experience?
	cute: {

	}, // see cuteness
	serious: {
		// demanding
		// challenging
		// real
		// practical
		// non-joking
	},
	serene: {},
	strange: {},
	mysterious: { //mystery:
	},
	weird: {
		// unfamiliar
		// unpredictable
		// high novelty
	},

	creepy: { //creepyAesthetic - types of creepy
		// types of creepy:
		// eerie - like castlevania?
		// cold - it feels like, exposed, cold, abandoned, hopeless??
		// Evil - like just butcher, Satan death cults, real death, often implied rather than explicit. things you do not want to know about...
		// sinister
		// deranged - reality bent out of shape...
		// mental derangement - the mind going insane. losing control. seeing things that are not there.
		// pain - things that symbolizes pain. sharp shapes, intense red colors, sad and suffering beings. certain Facial expressions
		// ghostly - foggy, kind supernatural you can not make out the details.
		// intense - pure darkness, eyes staring at you
		// brutal - death metal, sharp cool monsters, power mighty, strong, magical?
		// crawling/disgusting?? - insects and things you do not want around you.
		// gore
		// bloody, fleshy,
		// unexplainable - seems like dark tools or something with unknown intended use.
		// cryptic
		// otherworldly - 
		// 	What is that? either lurking in darkness or straight up incomprehensible.
	},



	// always night time (or twilight?)
	// symbols
	// cross
	// moon, full moon, eclipse
	// skull
	// candle

	horror: {},
	horrorBeings: { //stereotypical... like horror THEMED beings...
		skeleton: 1,
		zombie: 1,
		mummy: 1,
		vampire: 1,
		ghost: 1,
		demon: 1,
		devil: 1,

		beast: 1,
		bat: 1,
		raven: 1,
		wolf: 1,
		warewolf: 1,
		snake: 1,
		scorpion: 1,
		giantInsect: 1,

		dragon: 1,
		manticore: 1,
		merman: 1,
		mermaid: 1, //?
		goblin: 1,
		gnome: 1,

		possesedObject: 1,
		witch: 1,
		wizard: 1,
	},
	mythological: { // by this I mean like one of a kind sort of monster
		Dracula: 1,
		frankenstein: 1,
		Death: 1,
		merlin: 1, //?
	},

	AestheticsOfElthammarsWorldView: { // so I kinda want to define my aesthetics into the software like what
		//		the "aesthetic" aesthetic is ...
		//		what the "creepy" aesthetic ...
		//		"creepyaesthetic" ...
		// "sofisticated" "ominous" "cold" "non-caring" "dark"
		//		what makes something ominous or creepy ...
	},

	ominous: {
		// "giving the worrying impression that something bad is going to happen; threateningly inauspicious."
		// unexplainable
		// seemingly dangerous
		// looming
		// seemingly sinister or beyond comprehension.
		// something that stares you down.

		// a black hole is ominous
		// A shadowy figure is ominous
		// something unknown staring you down is ominous.
	},

	EmergentAesthetic: {
		// methods to define and create proper new aesthetics
	},

	chaos: {},

	violence: {
		// processes or behaviour containing physical force
		// that intends or results in destruction or death.
	},
	gore: {
		// violence onto life, resulting in hurt/damaged beings
		// (usually revealing blood, flesh, bones and organs)
		// (which in turn usually results in death over time)
	},


	gritty: { //my own weird definition
		//  TODO: define this concept so that I can do the exact opposite of it.
		// being forced to suspend your disgust sensitivity to pursue or move forward
		// unsmooth
		// complicated
		// diesel, rust, dirty, sweaty, dirty clothes, wrinkly old men, broken things, metal and machinery, objects
		// usually no colors
		// environment is either cityscape, ruin, dead landscape or dessert
		// It is an easy way to make games that seems like it has content because it looks "real" and "serious"
		// it is for people that has some weird sort of complex, like too much adrenaline or something.
		// it is the game industry's way to compensate for their immaturity. (if it is dark then it serious)
		// "teenagers wants things that seems adult" as if grittiness was adult...
	},



	//zeitgeists?
	//1900s
	the60s: { //mandatory 'the' because you can't start with a fukin number
		//if 60s was super mellow and genuine
	},
	the70s: {
		//70s feels so goofy, like we don't know what we are doing, boopin these machines.
		// quirky, funk it up. start of disco?
	},
	the80s: {
		// big boom of plastics
		// super-stimuli
		// he man
		// frizzy hair
		// everyone is a grunge surfer
		// Synths are hip
	},
	the90s: {
		// cynical.
		// doom aesthetic. (nuclear waste)
		// 
	},







	// Qualities of things (aesthetics???)
	exquisite: {
		//extremely beautiful and delicate.
	},

	delicate: {
		//very fine in texture
		//intricate worksmanship and quality

		//fragile - easily broken or damaged.
	},
	elegant: {
		//TODO
	},
	graceful: {
		//TODO
	},
	flamboyant: {
		//of character that attracts attention through their energy, confidence or style.
	},
	// elegance
	// sweetness
	// softness
	// freshness?
	// bitterness
	// tenderness


	// of experience: (aesthetic?)
	// Bizarre vs mundane
	// realism vs fiction
	// shallow vs deep wisdom
	// obvious vs indirect. (I am thinking about communication of the game, is what you see symbolic of something deeper or is it very down to earth.)


}
