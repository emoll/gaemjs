//implements.js

// inanimate things that are made out of materials

const implements = {

	// Human and cultural artifacts

	// Tools and inventions
	tool: { // A tool is an object used to extend the ability of an individual to modify features of the surrounding environment.
		//		Every tool made is there to manipulate the state of things in the world.
		byPurpose: {
			cutting: 1, // cutting or edge tools. wedge-shaped, for producing shearing force along a narrow surface.
			fastening: 1,
			moving: 1, // used to concentrate force of movement. wheel, hammer,
			shaping: 1, // forming. such as molds, jigs, trowels.
			chemicalReaction: 1, // like lighter...
			measurement: 1, // Guiding, measuring and perception tools include the ruler, glasses, set square, sensors,
		},
		toolsAreWornByUsage: {
			// then they need restoration or reparation
		}
	},

	// physical functionality
	// functions
	fastening: {
		// get to items together, hold their relative position or orientation/rotation.
		typesOfFastening: {
			clenching: 1,
			stiches: 1,
			glue: 1,
			welding: 1,
			wrapping: 1, //bandages, tejp etc
			binding: 1, //ropes?
			nailing: 1, //nails, pins,
			bolting: 1, //?
		}
	},
	cutting: { // shearing
		// Ideally, the edge of the tool needs to be harder than the material being cut or else the blade will become dulled with repeated use
		// But even resilient tools will require periodic sharpening, which is the process of removing deformation wear from the edge.
		// sharpening is a form of repair
	},
	storing: { // merge with inventory notes....
	},
	restoration: {
		// bringing something back to a state more aligned with its original form or function.
		// make something more whole (again)
	},
	repair: {
		// mechanical restoration?
		// repair can also include refinements or modifications to the original tool.
	},
	weapon: {
		is: {
			tool: 1,
			//used to hurt or damage
		}
	},

	// essential parts of any tool:
	handle: {},
	blade: {
		is: {
			hard: 1, //enough hard to to be sharp lol
			sharp: 1, // it is not a blade unless it is relatively sharp
		},
		has: {
			bluntnessVsSharpness: 1,

		}
	},


	projectile: {
		// something flying/airborne with velocity through air
		// projectile weapon
		// sends projectiles in desired direction.
	},
	projectileWeapon: {
		// gun is more powerful than bow
		// bow is more powerful than sling
		// sling is more powerful than throw
		can: {
			shoot: 1,
		},
		// held in a limb that can be rotated around axis. = aiming
		// aiming is the act of controlling the direction of the projectile with intention.
	},

	gun: {
		is: {
			projectileWeapon: 1,
		},
		has: {
			trigger: 1,
		},
		can: {
			shoot: {
				condition: {
					//pulled trigger
				}
			}
		}
	},
	bag: {
		//		contains items
		// animal crossing portable bag idea
		// the portable bag in animal crossing new leaf lets you create a set of things to bring at once.
		// You can have bags designated to their function. You can have a digging bag or a fishing bag, etc.
		// This makes it easy to bring and switch between prepackaged sets of things 
	},

	liquidContainer: { // a liquid storing container
		// an object that holds liquid
		// often by surrounding the liquid by a non-leaky material.
	},

	entryway: { //egress - //thing that takes you to new previously inaccessible places
		archetypal: {
			//		door/portal
			//		balloon
			//		airoplane
			//		tunnel
			//		spaceship 
			//		boat
			//		death
			//		sleep (dream)
			//		drug
			//		rabbit hole
			//		wardrobe
			//		mirror
			//		water reflection
			//		painting
			//		television/book/device sucks you in
		}
	},



	Town: {
		//	a place you can visit
		//	which is like far away from where you live
		//	but there you can buy all kinds of comfortable things
		//	for money
		//	
		//	it is a bunch of shops in close proximity
		//		where each shop specialized in what type of items they're selling.
	},
	store: {
		//	a location meant for selling products
		// example: a seed store, that sells various plant seeds, both rare and common.
	},


	robot: {
		// a mechanical and non-biological being
		is: {
			machine: 1,
		}
	},
	machine: {
		is: {
			mechanical: 1, //as opposed to organic?
		}
		//machines can be disassembled into components
	},

	// Mechanical machine parts
	propeller: {
		// in water, causes motion
		// in air, requires much force, with big propeller
		// causes wind or drafts...
		// propelling means spinning fast.
		// actually waving like any object creates slight wind... I need to think about this physically.
	},


	food: {
		//things that are edible are considered food
	},
	sushi: {},
	Tea: {
		// usually served in a cup
		// hot water
		// +
		// plant leafs (usually dried)
		// .
		// real tea is with tea leaves.
		// .
		// if you put foods in, it becomes soup instead
		// hot water with tea leaves
	},




	// architecture and types of buildings
	building: {
		has: {
			room: {
				count: "1 or more",
			}
		}
	},
	house: {
		is: {
			building: 1,
		}

		//house building
		//	states:
		//		non existing
		//		plan/drawing/simulation on paper
		//		plan/drawing/simulation as projected in world space
		//		In progress (partially finished blocks)
		//		Complete (Finished block)

	},
	tower: {
		is: {
			building: 1,
		}
		//- a slim and tall building
	},
	castle: {
		is: {
			building: 1,
		},

		// A castle serves as a fortress, protecting the inside from the outside

		// ... building consisting of many interconnected houses?
		// looks in particular way.
		// often built out of rock materials
		// medieval associations.


		//symbolism of castle:
		// The reason you build castles is because the outside world is too crazy.
		// But castles concentrates valuables which makes raiding valuable

		is: {
			building: 1,
		}
	},

	sign: {

	},
	monolith: {
		// A rock or hard stone, stuck in the ground with inscriptions
		// ancient monolith rocks with ancient symbols

		// stones with ancient symbols scribbled.
		// from times so distant they're inaccessible.
		// only ruins remain

		// Associations:
		//	pre-historic
		//	viking
		//	aliens
		//	ancient message or important message. "set in stone" text or truth holding the test of time.
		//	mayan calendar
	},

	//structure elements
	door: {
		// implies house. isolated interior.
		// opens or closes a portal.
		// Locked
		// Stuck
		// Open/closed
	},
	gate: { //gate - implies fence, protection, guarding, 
		//opens or closes a portal.
	},


	appearingPortal: { //entrance trope
		// A house,
		// building or entrance emerging out of the sand,
		// mud,
		// lake,
		// ocean,
		// island rising out of the water.
		// a portal opening up on a rock wall
	},

	// house parts and building material
	window: {
		// A hole in a wall to let in light to the interior of a building
		has: {
			conventionally: {
				glassPane: 1,
			}
		}
	},
	glassPane: {
		is: {
			rectangle: 1,
			glass: 1,
		}
	},
	brick: {},


	vendingMachine: {
		is: {
			unbreakable: 1, //and somehow you can't just steal items from it. Except sometimes it can drop an item when physically impacted or shaken. but only once after each purchase.
		},
		does: {
			display: {
				inventory: 1,
			},
			//	dispense inventory item for money.
		},
		has: {
			inventory: 1,
		},
	},
	boat: {
		//floats on water
		//efficiently transports people and resources over water.

		//"house in water

		//boat should sink if to much weight in it. or if holes in it....

		//idea
		//Manually row the boat... by spinning control sticks.

	},
	train: {
		// runs on rails
		// stereotypically powered by electricity or coal
	},
	car: {},
	bike: {},

	ladder: {
		// A long slim wooden structure allowing humanoids to climb from one end to the other.
	},
	zipline: {
		// A rope used to glide across...
		// Which you hang on and go from one place to the other.
		// And also like a rope you grab and as you do it pulls you up. ??????
	},
	ropeBridge: {
		// You can build a rope bridge between two mountains.
	},
	bridge: {},

	hookshot: {
		is: {
			tool: 1,
		},
		does: {
			//1. shoots out a rope that attaches to impactable surfaces
			//2. retracts rope so that the holder of the tool is pulled towards the impacted surface.
		}
	},

	sprinklerSystem: {
		//throws water in the air for watering plants
	},
	geolyzer: { //TODO: break apart and add to like curiosity and science based video game ideas.
		//	geolyzer lets you see the hardness of block
		//	aka like a hardness radar.
		//	the further away the more noise you get though.
		//	so figure out what material it is based on hardness.
		//	doing scan costs a lot of energy so you do not wanna do it a lot
	},


	knife: {
		is: {
			weapon: 1,
		},
		has: {
			blade: 1,
			handle: 1,
		},
		does: {
			slicing: 1, //lol
			cutting: 1, //lol
			carving: 1, //lol
		}
		//can be thrown
	},
	axe: {
		is: {
			tool: 1,
			//can be weapon
		}
	},
	shovel: {},
	saw: {
		//manually sawing trees down is nice.. like in spiritfarer. you go back and forth motion with stick
	},



	bow: {},
	arrow: {
		// long narrow projectile with a sharp point on one end and feathers on the other.
	},
	exlodingArrow: { //exploding arrow or bomb arrow
		// Arrow with attached explosive material
		// used with the intention of exploding enemies
		// either by time mechanism, by physical contact/force, or through remotely controlled mechanism

		// transforming snakes into wood material.
		// turning people into stone. (statues)gno
	},
	sword: {
		is: {
			weapon: 1,
		},
		has: {
			blade: 1,
			handle: 1,
		},
		does: {
			slicing: 1, //lol
		}
	},





	//head wears
	Mask: {
		// Worn on face
		// purpose is often to conceal identity or to gain mysterious powers.
		// TODO: read up on mask in tv tropes lol.
		does: {
			cover: {
				face: 1, //of the wearer.....
			}
		},
		typically: {
			is: {
				accessory: 1, //todo define accessory?
			}
		}
	},
	eyewear: {
		//- worn on face covers eyes
		purposes: {
			//either to modify vision
			//or to protect eyes.
		}
	},
	hat: {
		//- worn on head covers top
	},
	helmet: {
		//headwear that surrounds the skull in a protecting fashion...
	},
	WitchHat: {
		draw: function () {
			//        var hatWingRadius = 50
			//        ctx.beginPath()
			//        ctx.lineTo(x - hatWingRadius, headY - .5 * headR + 20)
			//        ctx.lineTo(x + hatWingRadius, headY - .5 * headR - 20)
			//        ctx.lineTo(x, headY - .5 * headR - hatWingRadius)
			//        ctx.lineTo(x - hatWingRadius, headY - .5 * headR + 20)
			//        ctx.fillStyle = "black"
			//
			//        ctx.fill()
		}
	},

	cape: {
		// Cape makes you look cool
	},

	scarf: {
		// Scarf makes you look cool
		has: {
			material: {
				cloth: 1
			}
		},
		draw: function () {
			//    DrawRect(defaultLayer, x - .5 * 13, headY + .5 * headR, 13, headR, "#FF6699")
			//     this.color = "#99aaff" //"pink"
		}
	},
	//torso wear
	shirt: {},
	robe: {},

	pants: {},






	fern: {
		is: {
			plant: 1,
			philisinophyta: 1,
		}
		// I want to draw ferns that are photogenic and fully grown for the frames james doin for spelkollektiv
		// I'm thinkin like a boquette, aka they all kinda come from one point and go up and spread out a bit.
		// Would be super nice with rocks and moss growing as well...
		// it has a stalk that is like rolling out into a bow/curve.
		// leafs shoots are frequent, and come out the same on both right and left side of the stalk
		//	in some plants the shoots alternate left right rather than come out parallel.
		// When I look closer alternation seems to be more common than parrallel shoots.
		// Some ferns shoot a layer of stems which then grow leafs.
		// Whenever a fern stem curves or has a curve to it, it looks kinda cool. especially spiraling patterns.
		// The leafs are thick close to the stalk and linearly gets thinner towards the ends.
		// The leafs are kinda woobly... or they are kinda like long leafs Ive done before actually!
		// Some fern leaves have kind of leaf out shoots on the leafs similar to the shoots coming out of the.

	},
	cactus: {},
	bamboo: {},
	ginko: {},
	pineTree: {},
	tall: { //pine
	},
	gran: { //pine
	},
	birch: {},
	oak: {},
	palm: {
		//		grows in sand
		//		warmer climates
		//		one big stem with big leaves spreading from the top
		// often seen on beach
		// or sandy deserted islands.
		// carribean...
		// miami
		// can give a plastic vaporwave A E S T H E T I C
	},

	melon: {},
	blueberry: {},
	banana: {},

	//flowers:
	rosemary: {},
	rose: {},
	lavender: {},
	sunflower: {},







	//animal archetypes
	dog: {
		//I guess dogs are kinda defined by their behaviour
		//long nose
		is: {
			animal: 1,
			fluffy: 1,
		},
	},
	cat: {
		is: {
			animal: 1,
			fluffy: 1,
		},
		// catlike - an aesthetic/style with the behaviour and properties of a cat.
		//	fluffy soft non agressive, cool and lazy relaxing, self-sufficient/autonomous
		// themes like, fluffy, soft, fish, tuna, mouse, 
		// curious, completely uninterested, 
		// movement kinda swift and smooth and instantly reactive to movements in surroundings.
	},
	fox: {
		is: {
			animal: 1
		},
	},
	crab: {
		//user story: 
		//	catch a bunch of crabs
		//	give them candles
		//	let them wander around the church graveyard like spooky ghosts

		is: {
			animal: 1
		},
		can: {
			//crabs can be caught
		},
		does: {
			// crabs grab onto items that re moving close to them.
			// crabs hold onto items in certain circumstances ... ??
		}
	},
	reptile: {
		has: {
			// pupils that are oval

		},
		does: {
			// blink less often with eyes
		}
	},
	lizard: {
		is: {
			animal: 1
		},
	},
	//the ones I thought about from keep:
	bird: {
		is: {
			animal: 1
		},
		// likes to sit on tree branches
		// symbolizes the transcendant and of freedom (as they go and fly where they want)
		// birds generally fly away if you get to close.
		//	boids show that simple rules can create complex behaviour
		//	so does game of life, etc

		// Bird like animals that can sing however they want (voice synthesis or generated sounds!)
	},
	fish: {
		is: {
			animal: 1
		},
		does: {
			swim: 1,
			not: {
				blink: 1
			}
		}
		// lives under water
		// floats
		// can jump out of water
	},
	snake: {

		is: {
			animal: 1
		}
	},
	// idea Snek but dragon or larvae or worm or jellyfish
	worm: {
		is: {
			animal: 1
		}

	},
	jellyFish: {
		is: {
			animal: 1,
		},
	},

	seagull: {
		is: {
			bird: 1,
			white: 1,
		},
		eats: {
			fish: 1,
		},
	},








	bottle: {
		is: {
			liquidContainer: 1
		}
	},
	flasks: {
		is: {
			liquidContainer: 1
		}
	},
	fishbowls: {
		is: {
			liquidContainer: 1
		}
	},
	aquarium: {
		is: {
			liquidContainer: 1
		}
	},
	teapots: {
		is: {
			liquidContainer: 1
		}
	},









	// clothing and fashion!
	// humanoid characters typically wear an outfit of clothes. Exception to this rule are for beast
	// an outfit is a full set of clothes and usually involved pants, shirt, jacket, accessories.

	// add to somewhere. hyperstrength is a reflection of POWER and power is cool, thus an exception to the rule of cool.
	// power of character. The power of character reflects their agency in and mastery(control, manipulability) of their surrounding environment.


	// many layers of cloth
	// Multiple cloth layers
	// Play around with multiple layers of cloth, gris's skirt is the most beautiful thing ever.
	// feels soft, looks like flowers, impressive in the wind.
	// associations: layered, hidden, endless?


	samiPatterns: {
		//	sami patterns:
		//	xxxxxxxxxxxxx
		//	XXXXXXXXXXXXX
		//	vvvvvvvvvvvvv
		//	VVVVVVVVVVVVV
		//	-------------
		//	-_-_-_-_-_-_-
		//	+ + + + + + +
		//	+|+|+|+|+|+|+
		//	=-=-=-=-=-=-=-
		//	.-.-.-.-.-.-.-.
		//	||||||||||||||
		//	\/\/\/\/\/\/\/
		//	\\\\\\\\\\\\\
		//	/////////////
		//	-._.-._.-._.-.
		//
		//	oOoOoOoOoOoOoO
		//	:::::::::::::
		//	:.:.:.:.:.:.:
		//	<<<<<<<<<<<<<
		//	><><><><><><><
		//	^^^^^^^^^^^^^
		//	)( )( )( )( )( )( 
		//	()()()()()()()()(
	},

	bulbyClothes: {
		// bulby clothes can be defined around a ground shape as soft brush around lines.
		//	this brush represents like the range of the clothes
		//
		// then some sort of mechanism choosing the wind direction  affects the gradient from the center of the circle
		// to get strength toward the direction of the wind.
		//
		// maybe this is literally another soft brush with offset from the line in the direction of the wind.
	},
	clothes: { //defining clothes
		// % of arm length (from shoulder and out towards hand)
		// % of torso length (from neck down)
		//
		// neck hole. (hole size, v-neck.)
		//
		// just shoulder straps (no neck hole)
		//
		// looseness, does it sit tight on body or not
		// thickness of cloth (is it very tiny and flimsy or thick and sturdy
		//
		//
		// skirt from mid down, no legs
		// pants
		//	shorts
		//	normal (from mid, down to ankles)
		//	
		//	
		//	
		// https://sewguide.com/types-of-clothes/
	},
	cloth: {
		// soft thin and flexible material 
		// Multiple cloth layers
		// Play around with multiple layers of cloth, gris's skirt is the most beautiful thing ever.

		//	cloth feel not as essential but when its done it feels like a huge value add.
		//		kinda requires humans
	},









	// TODO and define
	// grass types
	// grass composition
	//
	// tall grass
	// straw
	//
	// cereal plants

	mushroom: {
		has: {
			stem: 1,
			hat: 1,
		}
	},
}
