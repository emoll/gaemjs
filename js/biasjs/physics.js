// physics and forces
// physics:

const physics = {
	space: {
		//interconnected scalars of spatial dimensions
	},
	entropy: {

	},
	naturalDecay: {
		// Natural decay as a way to save memory and processing
		//	If places you do not visit deteriorate, that means they can go back to be generated by algorithm.
		//		Thus you can remove anything artificial made by the player
	},

	physical: { // think physical body. or physical entity I guess.
		has: {
			shape: 1,
			mass: 1,
			material: 1,
			persistence: 1,
			position: 1,
		},
		is: {
			object: 1,
			breakable: 1,
		},
		can: {
			be: {
				buried: 1
			}
		}
	},
	weight: { // An inherent property of any physical object
		// weight can be determined by density and area
		// all objects that are "nailed" attached to each other can be counted as the sum weight of all the elements, when calculating gravity etc.
	},

	density: {
		// the density of compounds depends on atmospheric pressure etc.
	},

	forceField: { //TODO: move to... forces and gravity etc.
		// the less the distance to the field, the stronger repulsive force it applies
		// remake all those cool force field things I made in Unity where effects are stronger the closer they are.
		// especially like pushing aside grass and stuff
	},

	bearing: { // or like how much something... a material can handle before it breaks. crumbles, splinters. cuts open. etc.
	},
	springy: { //bendy
		// add to physics bendy bendiness and springy springiness

	},
	stuckness: {
		//	stuck to means, relative position is constant, moment to moment it appears on the same relative position.
		// i guess this concept can apply not only to physical relative position, but also to variables with inertia. or should I then have an inertia concept?
		// stuckness should then be a "is a form of inertia"
		//
		//
		//it is inertia in relative position.
		//
		//this relation can be described with
		//	object A, local coordinate on object A,
		//	object B, local coordinate on object B,
		//	distance vector from point A to B
		//	
		//
		//	stuck generally also means locked in rotation.
		//
		// usually you want things to be stuck to physical things.
		//	You do not want something to float with the position of the player
		//		you want the arm to be stuck to the torso, you want the hat to be stuck on the head.
	},
	breakable: {
		has: {
			durability: "scalar", //between 0 and 1
			brokennesswholeness: 1
		}
	},
	brokenness: {
		stages: {
			pristine: 1,
			beyondRepair: 1,
			unusable: 1,
			beyondRepair: 1,
		}
	},
	broken: {
		cant: { // broken things can not be worn.
			worn: 1,
			do: 1
		}
	},

	friction: {
		//	when impacted hard, the power is transfered, making you you glide on the floor even if it is rough/high friction.
		// aka with strong enough force you glide back  even when friction is high.

		//	friction is important for gliding
	},
	stickiness: { // stickiness property lol
		//    calculating stickiness for composite objects
		//        stickiness variable. the threshold needed in acceleration in order to decouple from what a thing is stuck on.
		//        if I for example have a bunch of leaves on a tree and the stickiness of each leaves is in a range of values
		//        Then a applied force to the tree, either a wind or a hit, will decouple the set of leaves and other elements
		//        which threshold has been met. Resulting in them falling off the tree.
		//        a stem of a plant can grow from the middle as long as the middle has the same thickness as the parts it is connecting.
		//            this creates an elongating effect		
	},
	gliding: {
		//when an object does not stop on surface due to low friction.
	},


	buoyancy: {}, // bouancy
	displacement: {
		// a liquid matter always occupies the same amount of area.
		// if an object enters a body of water, the water has to rise.
	}, // liquid displacement
	floating: { //float: {//move near flying
		// Make boat/object angle the angle of the wave area it is resting on.
	},



	// :processes... transformations...
	Transformations: {
		Bending: {},
		Scaling: {},
		scewing: {},
		// TODO: add all those other transformations. ??
	},

	transformation: {
		// changing in nature or condition.

		// Transformation
		// there is tranformation in value/quantity, then there is transformation in nature/components.
		// there are relative transformations, +5, and absolute, value is 105.

		// something needs to determine what effects/reactions/transformations manifest.
		// A manifesto?

		// think of this like l-systems. certain combos develop into new structures according to rules.
		// this is emergence, this is chemistry. But is this input output???? Is this transformation?
		// This is more about defining emergent properties that arise in combination of other properties.


		// Conversion/transformation
		// input    restriction rate, translation of value    output
	},

	// Space and area related change 
	grow: { //expand
		//over time size/area is more
	},
	shrink: {
		//over time size/area is less
	},

	explode: {
		// shatter into pieces with a force from a center
		// impulse,
		// expression of power.
		// forcefully expand (in a way that often shatters the container of the expanding materia/pressure)
	},
	implode: {
		// cram into center with an omni-directional force towards a center
	},



	// movement and interaction of physical forces
	// move to physics
	// collision and touching
	impact: {
		// 2 surfaces go from non touching to touching over time.
		// impact has surface area, force vector (direction and intensity)
		// often with strong/high force.
	},
	fragility: {
		// all things have some level of fragility
		// fragility is a measure of how much damage something takes from the intensity of an impact
	},
	knockback: {
		// When strong force you get an impulse of force that pushes the impacted object away
		// I think knockback should generally be relative to how much damage you take compared to how much life you have.
		// When you get hurt you bounce back //this is like true for many ... sidescrollers? platformers?? (TODO: find the most common denominator so that I can use it broadly automatically where it is appropriate)
		// Abstract knockback knocks a body equal amount any time it is hurt rather than by pure physical (impact) force...
	},

	movement: { // what currently does/manifests through time.
		// relative movement / animation
		from: 1,
		toward: 1,
		into: 1,
		around: 1,
		spinning: 1,
		rotating: 1,
	},
	symbolismOfMovement: {
		StraightUpward: {
			//				ascending
			//				creates a feeling of freedom and power, lightness, 
			//				doing things right
			//				Being in control
		},
		upwardButCameraZoomsOut: {
			// smallness
			// danger of falling down
			// excitement
			// freedom
		},
		straightDownward: {
			//				falling
			//				Descending
		}
	},

	// Movement & physicality - inspirations from GRIS
	// Gliding/surfing on slopes!!!!
	//
	// Moving environments. You are inside a building that has legs and moves around in the environment. like a freaking howl's moving castle.. super cool. There are entrances and exits.
	//
	// I like the heavy stomp fast fall
	//
	// I also like walking around like a freaking square tank. Like put on a really silly big thing and walk around to not get affected by wind. Genius.



	// Physical (material) properties
	material: {
		has: {
			density: 1,
			opaqueVsTranslucent: 1,
			//surface
			texture: 1,
			roughVsSmooth: 1,
		}
		// can be composite/mixture materials???
	},
	opaqueVsTranslucent: {
		// opaque alpha 0
		// translucent alpha 1
	},
	// liquidyVsSolidity - water, clay, slime, rock
	softVsHard: { // soft: hard:
		// softness vs hardness
		// there is a soft that is like "spongy" or maybe squiggly. when you press it it compresses.
		// , the opposite is hard	

		// soft definition: easy to mould, cut, compress, or fold; not hard or firm to the touch.
	},
	roughVsSmooth: {
		// there is a soft that is like smooth, the opposite is rough	
	},
	// there is a soft that is like fluffy...

	statesOfMatter: {
		solid: 1, //earth
		liquid: 1, //water
		gas: 1, //wind
		plasma: 1, //fire
	},
	solid: {}, // earth
	liquid: {
		// secondary meaning of the word consistency. Consistency of liquid: the way in which a substance holds together; thickness or viscosity.
		// boils at boiling temperature
		// boiling temperature is generally close to 100 degrees Celsius
		// liquid is slippery
		// liquid drips through any hole (so does sand.... add same whatever to sand)
		// can be frozen into solid
		// can evaporate into gas
		// multiple liquids either blend together on contact. or refuses to blend (like oil and water )
	}, // water
	gas: {
		// https://www.thoughtco.com/gases-study-guide-607536#:~:text=A%20gas%20is%20a%20state,act%20in%20a%20similar%20matter.
		// Gases assume the shape and volume of their container.
		// Gases have lower densities than their solid or liquid phases.
		// Gases are more easily compressed than their solid or liquid phases.
		// Gases will mix completely and evenly when confined to the same volume.
		// All elements in Group VIII are gases. These gases are known as the noble gases.
		// Elements that are gases at room temperature and normal pressure are all nonmetals.

	}, // wind
	oxygen: {},
	helium: {
		//lighter than oxygen. and thus has lifting force
	},
	plasma: {}, //fire

	elements: {
		fire: 1,
		wind: 1,
		water: 1,
		earth: 1,
		aether: 1,
		//		space:1,
		//		vacuum:1,
		//fundamental elements 
		//	solid, liquid, gas, plasma
		//	space, gravity
	},
	materials: { //phases //this probably has a more specific term. like, elements?
	},

	colliding: { //collision
		// from not overlapping to overlapping, causing an impact or physical impulse
	},
	// something something define the laws of physics, so that you can create gungbräda, levers, water pressure, steam engines, uh whatevers lol

	melt: { //melting?
		// turn from solid into liquid form
		// often due to heating a material over the melting point....
	},
	solidify: { // freeze: in layman's terms
		// from liquid to solid
		// certain compounds solidifies as specific temperatures in specific pressures
	},
	evaporate: {
		// liquid to gas
	},
	condensate: {
		// gas to liquid
	},




	rust: {
		// brown, ginger red, orange
		// powdery
	},


	//physical processes or actions
	penetration: { //or impale
		//		hard things can be used with force to penetrate softer things
		//penetration leaves a hole in the softer thing.
	},
	outgrowth: {
		//		penetration from inside out.
		//a buried things that penetrates out
	},

	crumble: {
		//		driedUp things that are "destroyed", crumble
	},
	cut: {
		// an opening like a line
		// a stroke or blow given by a sharp-edged implement
		// a line which severs
		// a fast cut that 
		// the action inter
	},
	sever: {
		// divide into two parts by a line of a cut
	},
	impactToMaterialEffect: {
		//		leaf pile -> Leafs, maybe dust?
		// Sand -> dust smoke
		// Dry clay -> dust smoke
		// Coal remains from fire -> dust smoke
		// wood material burns when hot
	},


	//physical processes like drying and soaking
	soak: {
		is: {
			collect: 1
		}
		// material goes from: "material + gas/vacuum" to: "material + liquid

		// soaking: {
		//	condition: {
		//		soakable touches soaker
		//	},
		// }

		// function that describes the soaking behaviour at any given hydrationLevel
		// This can be described with a curve that ranges from 0 to 1
	},
	dry: {},
	burn: {},

	soaking: {
		effect: {
			// make(soakable, "less", volume) //make soakable less in terms of volume
			// make(soaker, "more", soaked ? ? ? )
			// soaking rate can be determined by how soaked something is with a
			// function !!!
		}
	},
	soakable: {},
	// Things that are wet/soaked are generally shown as darker. more soak is more dark
	capacity: { //maximum and minimum capacities
		has: {
			value: 1,
		}
	},
	soakCapacity: {
		is: {
			capacity: 1
		},
		has: {
			value: 1,
			measure: "liter"
		}
	},
	soaker: {
		has: {
			soakCapacity: 1
		}
	},
	painting: { // the concept of dipping an object in a liquid and dragging the object along a surface to make the liquid go off
		//	painting surfaces by:
		//		dipping surface in liquid
		//		using a brushlike object
	},
	// heatingReaction
	// melts, evaporates
	dryable: {
		can: {
			dry: 1
		}
	},



	porous: {
		// having minute interstices through which liquid or air may pass.
		// a solid material which gas and liquids can pass through
	},
	corrotion: {
		// rusting
		// how metals chemically deteriorate
		// often when left alone in nature in harsh environments.
		// iron naturally rusts from water.
		// salt water from the ocean is extra corrosive, i think to aluminum???

		// Corrosion is a natural process that converts a refined metal into a more chemically stable form such as oxide, hydroxide, or sulfide. It is the gradual destruction of materials (usually a metal) by chemical and/or electrochemical reaction with their environment.
	},









	// Power, light and Thermodynamics
	heating: {
		is: {
			process: 1
		}
	},
	heatDisipation: {
		// temperature evens out into the environment.
		// heat radiates through space.
		// heat is stuck in objects temporarily, and radiates into the environment based on how well the material 
		//	transfers heat..
	},
	cooling: {
		is: {
			process: 1
		}
	},

	refraction: {
		// water
		// humid air?
		// hot air?
		// things that get hot get refraction filter
		// heat "refracts" light the same way as underwater in anime
		// heat wave and underwater wave is basically the same. shader
	},

	burning: {
		is: {
			process: 1
		},
		has: {
			burningRate: 1
		},
		consumes: { // removes, reduces
			fuel: 1, // or whatever general burnable material is called
			// burnable.parent.mass--
		},
		creates: { //generates, produces
			heat: 1, //heat is increased at the point of fire relative to to burning rate
			flame: 1,
			smoke: 1 //smoke is created which rate and nature is based on the chemical compounds being burned, the burning rate. 
		}
		// reaction: { //now summarized in consumes and creates
		//	make(x, combust)
		//}
	},
	flame: {
		is: {
			//burning: 1,
			plasma: 1
		}
	},
	fire: {
		//manifests in the process of burning
		is: {
			process: 1,
			physical: 1,
		},
		has: {
			flame: 1, //has enough flames lol.
		}
	},
	ember: {
		//things that glow out of hotness.?
	},
	burningRate: {
		// Does burnable material go away slowly or does it instantly disappear and turn to heat ?
		//	You might want things to have like a processing resistance that stops things from exploding.
		is: {
			rate: 1
		},
		does: {
			multiply: { //I do not know what syntax to use to fuking write math as data structure.
				burnability: 1,
				heat: 1,
			}
		}
	},
	combustion: {
		condition: {
			//temperature is combustionTemperature //at which material burns
		},
	},
	// burning rate depends on heat and burnability of the substance.
	// generated End result:
	burningRateFunction: function (burnability, heat) {
		return burnability * heat
	},
	flammable: { //burnable:
		can: {
			burn: 1,
		}
	},

	// Example fire
	// vague signs like "flammable"
	// or "good temperature"
	// or "temperature range(-1, 23)"
	// high temperature touches burnable
	// temperature reaches burning point (ignition point)

	electricity: {
		// goes through the most conductive path.
		// water has high conductance
		// metals have high conductance
	},
	spark: {},

	electricalDischarge: { //transfer process
		//emitting light, heating whatever it passes through.
	},
}
