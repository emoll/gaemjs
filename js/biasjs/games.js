const games = {
	// Index:
	// Turn based strategy games
	// Side scrollers
	//	movement/fighting
	//	exploration/open world
	// Top down
	//		management games
	//			inventory
	//			logistics
	//		exploration open world

	descriptionsOfWhatIWantAGameToBe: { // and appealing aesthetics...
		// **AESTHETIC VALUE RANKS** (ranked by resonance with my heart/soul)
		//10
		// A mind opener
		// You visit each others worlds 
		// Nature
		// Dreams
		// Softness
		// It feels alive and filled with spirits.
		// It is a room for your soul. All inspirational artifacts that makes you who you are.
		//	Like a doll/toy house but it is a real and alive world that is a visualization soothing your soul
		//		where
		//			You can have characters live
		//			You can create your own items
		//			You can copy the items to and from other players.
		// I want the game world to be an artifact,
		//	Just like showing of your collection or a museum
		//	Or a well taken care of house.
		//	The world is literally an interactive developing art piece you are making.
		//	It needs to be 
		//		persistent
		//		resistant to change
		//		sendable over network
		// Being in connection and dialog with nature
		// A get away
		//	from daily life
		//	from people
		//	from routines
		//	Environmentally extraordinary??
		//		like a desert, beach, volcano, ice mountain peek, underwater, jungle, etc
		// Expression of emotions
		// Subtleness
		// Human interplay (connection)
		// Thoughtful
		// You meet and then you exchange
		// Harmonize/synchronize people
		// Bridging the inside world with external
		// Living other's life
		// Game about dying
		//	Dealing with death
		//	Understanding death
		// 9
		// A display (of art or collection)
		//	Collections of unique
		//		items
		//		plants
		//		elements
		//		spells
		//		liquids
		//		foods
		// A garden
		//		structure of organized plants
		// A meditation space
		//	calm
		//	persistent
		//	flowing (sounds, wind, water, etc)
		// A private space
		// A secret hideout
		// Natural witchcraft magic game with spirits and stuff, inspired by howl and castlevania MapleStory, Nox
		// "not a game, but a window to another world that seems real in itself"
		// A game that reflects and expresses all my thoughts and feelings. (the poetry I like)
		// Empowerment of women and femininity
		//	Stronger mythology.
		//		About
		//			femininity
		//			the female body
		// Actualization/realization of feminine fantasies
		// toy scape
		//	I also like the idea of making a game that is a toy-shop
		//		where the player makes their own toys that are one of a kind and that you can share.
		//		Everyone has their own workshop with their own crafted tools.
		//	Essentially I just want to make a toy-scape.
		//		interactive interesting self contained systems without purpose
		// think Tim's toys
		// Think flume desert car https://www.youtube.com/watch?v=7TML_MTQdg4
		// Think Burningman
		// freedom
		// the ocean
		// Increase love, understanding and acceptance
		//	"Exploring my femininity."
		//	"Representing underrepresented things through games in a way that makes those things more attractive in order to increase people's love and understanding in the world."
		// Deep rooted
		// 8
		// cool things and items
		// Snoofi
		// architecture
		// 7
		// my game is like Pinterest,
		// A big point is that it should seem like a game that is almost impossible that it exists.
		//	Awe.
		//	There is something about art or architecture that is abandoned or destroyed.
		//	An unfinished work is more beautiful because of its imperfections.
		//	Likewise a ruin is like a mental puzzle of what once were.
		//	And each artifact speaks about the past and the minds that designed it.
		//	Everything is finite
		//	Everything is eventually meaningless in the face of an unforgiving environment.
		// I like the idea of permanently existing places. Like this idea that you travel between permanently existing, but physically disconnected places.
		//	A remote tropical island. The top of a snowy mountain. A forest cottage. A "demon" or dark world made out of skulls and weapons and magic.
		//	And maybe your character changes into an appropriate form when you go from one world to another.
		//	So you become like a collector of worlds.
		// Organic and alive digital media
		//	"A game that feels truly organic and alive."
		// A game without any externally imposed goals
		// "Creating something autonomous that will surprise me and continually break expectations positively in order to satisfy my need for positive novelty and to make myself redundant by making the things I value live on without me and to blow me and other people's minds. In order to fulfill my instinct (I can really explain why I want this.)"
		// Animal crossing with MapleStory like expression and the emotional expression buttons in animal crossing/MapleStory
		//	with a highly customizable expressive character that creates the new and unique aesthetic
		//		horns, sparkles, bubble gum, eye lashes, cigarette,
		// A tool for the soul.
		//	A guided path, a ritual to take someone through the teachings I've learned towards enlightenment.
		//	A game that reflects my inner light that I've received, and help that light shine through
		// That is why I love consuming others art, because it is like I live vicariously through their creative search.
		//		And I can the the light that resonate with me, concentrate it and pass it on.
		// Beautiful plants and shapes in an interactive world that seems deep, emotional and meaningful to people.
		// It is  role playing game that ensures you have a unique identity to stand out among your friends..
		// Seedling should basically be Noita but persistent
		// Good cause and representation things.

		// It is pretty clear that I want to make a game about
		// 	aesthetics
		//	animation
		//	crystals
		//	liquids
		//	sand
		//	plants
		//	autumn
		//	girls
		//	tasty food
		//	smoke
		//	blood
		//	demons
		//	cuteness
		//	skin
		//	beautiful bodies
		//	textures
		//	death
		//	magic systems
		//	Ecosystems
		//	Curses
		//	exploration of worlds?
		//	Dream explorer
		//	feminine softness
	},


	// Real games and their conventions
	pinBallGame: {},
	oxygeNotIncludedGame: {
		// make a game like oxygen not included.
		//	charming tiny people
		//	organizing a colony and directing the behaviours of a colony.
		//	designing architecture in an interesting space.
		//	survival and exploration inside a god damn comet (interesting challenge.
		//	keeping track of details of resources and 
		//		how you can transform materials into new materials with novel processing and ingredients.
	},
	miniMetroGame: {},
	unrailedGame: {
		//the train runs on tracks.
		gridworld: 1, //specifically gridbased placing of objects and resources. // one space holds one item.
		topdown: 1,
		impendingDoom: 1,
		rougelike: 1,
		resourceGathering: 1,
		needsManagement: 1,
		localCoop: 1,

		craft: {
			rails: 1,
		}
	},





	harvestMoonGame: {
		activities: {
			farming: 1,
			planting: 1,
			harvesting: 1,
		}
		// Every day consists of 24 hours and every hour lasts 30 seconds.
		// You have to go to bed at night
		// Ever day has a weather at random. the weather is discrete and archetypal.
		// Usually top down
		// About farming, maintenance and economy
	},

	smashBrosMeleeGame: {
		//fine control, fine physics
	},




	snakeDrifter: { // 1h gamejam game
		//What would it take to make this game/code seedling compatible?
		// What is it that is incompatible with my current aproach and by gaemsjs framework?
		//
		//	I have more constrained constraints (specific values) rather than abstract values.
		//	I have no enforcement of type constraints (I never make sure the functions that change colors can't change the values of the color to be non-color.
		//	
		//	
		//	values are only relevant to the functions that will process them.
	},

	// ___Side scrollers_____
	BOTWGame: {
		is: {
			game: 1
		},
		has: { //contains: {
			//persistent: { // this feels like it can be interpreted wrong...
			being: {
				count: {
					atLeast: 1
				},
			},
			artifact: {
				count: {
					atLeast: 1
				},
			}
		},
		// perspective: sidescroller
		// Setting: open landscapes fields of grass, forests, bow and arrow? grass, hills, overgrown ruins, sword. ropes
		// Areas:
		// 	mountains, big tree forest, fields, round hills,  cliff, ocean.
		// Halfway up the mountain is birch forest.
		// Potential places:
		//	waterfalls, city, lighthouse, castle..., shed/cottage, desert, lake, 
		// MVP order: square area of grass, flat field, round hill field, forest, forest to field, etc.
		// Aesthetic:
		// 		movement: The important part is that the wind blows through the grass and trees and it looks cool.
		// 		colorscheme: is like green, blue, white, brown and gray (and for some details maybe red, pink, purple, orange)




		// What are the overarching goals of BOTW game?
		// What are the core consistencies?
		// The goal of the style of BOTW
		//	Make wind look impressive
		//	Be beautiful
		//	Not feel too complex
		//
		//	Hard shadows.


		// Seagulls plz.

		// Trying to define BOTWGame styles through other styles:
		//	sami
		//	windwaker
		//	howl's
		//	moomin
		//	adventure time?

		// painted wood objects!!!
		//	like tiny patterns or images of animals painted onto tools and walls etc.
		//	(like in a paint on wood kind of feeling... wind waker intro. but sami textures??)


		// BOTWGame Style:
		// windy, fresh, present, aware, important, duty, home-sewn, human connection, flow, animated, moving, freedom, fashionable, expressive, coming out, release, fantastical, serious,
		// been stuck inside for too long, been weighted down.
		// maybe thick fog, has been locked inside. outside is dangerous or unpleasant.
		// but now i get to go outside and live. see the sun the cloud the grass.
		// spiritual - ghost fly away in the wind.
		// .
		// Sami
		// close relations
		// family
		// ancestral connection.
	},
	BOTWGameDevelopmentPriorities: {
		//Erik wise words:
		//	breath of the wild prototype and then you know............
		//	calculate vart du är och vart du vill, gå ett steg i den riktningen repeat.
		//
		//prioritized list of things needed:
		//	landscapes
		//	sky
		//	grasses
		//	character
		//	trees
		//	mountains
		//	cloud
		//	bushes
		//	Rocks
		//	mosses (and other things that grows on other things.)
		//	flowers and unique plants (ferns etc)
		//
		//world areas:
		//	mountains
		//	nordic pine forest
		//	steppe (and tundra)
		//	deep groove forest
		//	the horse fields
		//		infinite grass fields
		//		big trees inbetween
		//		soft grassy hills
		//		some areas have sparse rocks sticking out of ground.
		//	river city
		//	grassy beach banks 
		//	desserts
		//
		//ruins
		//hidden treasures
		//camps:
		//	old temporary abandoned camps
		//	active capms
		//	smaller and bigger camps
		//
		//bone remains
		//
		//
		//Towers
		//
		//activities:
		//	wandering
		//	foraging
		//		roots
		//		mushrooms
		//		herbs/leafs
		//		
		//nymph, small people a city of small friendly creatures. lol. miniature city
		//
		//critters
		//	birds
		//	lizards, snakes
		//	deer
		//	cicadas?
	},
	// Just good combos...
	mapleStoryMovementWithSpiritfarerIslandsGame: {
		// MapleStory movement like a network graph of worlds 
		//	so that you can go like non-linearly on each island
		//		each 2d plane landscape is a route between two points.
		//	
		// have a moving castle like in howl's moving castle.  it is like the boat in Spiritfarer but on land.
		//	and also the teleport door! so you can like move your castle and also escape somewhere.	
	},

	breakDownOfBOTW: { // What the zelda botw actually is
		// Breath of The Wild is:
		//	action-adventure
		//	open world
		//	nonlinear gameplay
		//	using
		//		physics engine
		//		chemistry engine - that defines the physical properties of most objects and governs how they interact with the player and one another
		//			
		// players can perform actions such as running, climbing, swimming, and gliding with a paraglider
		// 
		// limited by his stamina
		// 
		// gather/scavenge things from nature or the world
		//
		//	the mechanics involved is, aiming at or being close to something distinctly pickupable
		//		seeing the highlight that indicates the thing is marked to be picked up
		//		and pressing the button the player identifies as the pickup button.
		//
		// things/resources:
		//	gear, ingredients,
		//	
		//	weapons, food, 
		//	
		// melee weapons and shields degrade with usage based on durability and the hardness and type of impact the weapon endures.
		//
		// chemistry:
		//	hard impact get stuck in wood
		//	fire makes wood burn
		//	shield is surfingboard
		//	
		// hunting - natural life that mostly hides, you can hunt and eat
		//
		// riding - horses and animal
		//
		//
		// reward trophy items from killing enemies
		//	
		//	
		// cooking system (ingredients combination system / crafting system)
		//
		//
		// create meals and elixirs that can replenish Link's health and stamina, or provide temporary status bonuses such as increased strength or resistance to heat or cold
		//
		// map and navigation system. (for exploration)
		//
		// proximity sensor (to object)
		// temperature sensor
		//
		// camera
		//
		// combat moves:
		//	target lock - makes link face and coordinate around the selected or closest target
		//		all attack swings are direct towards the target
		//			and all movement is with link facing the target
		//	attack - (different swing and duration based on weapon)
		//	attack dynamics
		//		simple attack combo (what happens when you press attack repeatedly)
		//		hold attack (when holding link does some form of charge up attack
		//		throw weapon (throws weapon to give damage at distance. usually breaks weapons.)
		//		certain button combinations allow for advanced offensive and defensive moves
		//	defend (hold up shield)
		//	bow
		//		aim
		//		pluck string
		//		
		// indirect combat (physical and chemical damage)
		//	fire is sticky, fire damage over time
		//	wind can put out fire or strengthen big fire
		//		strong wind can make enemy fall over.
		//	water puts out fire
		//	big object with momentum - impact/crushing damage/falling
		//	cutting
		//	impaling
		//	explosive damage. (highly accelerated impact)
		//	explosive force.
		//	make enemy fall into
		//		pit
		//		fire
		//		water
		//		etc
		//
		// combat intelligence
		//	a system that adapts move set based on the the characters proportions, experience
		//		and type of equipment.
		//	 
		// weapons have
		//	special physical properties
		//		materials
		//		dimensions
		//	swinging/move duration/speed
		//	special attack bonuses
		//	special moves
		//	special combos
		//	
		// certain weapons have certain unique properties and inherent abilities
		//	spears are more throwable
		//	boomerang weapons
		//	korok leaf (wind weapons)
		//	wind cleaver
		//		sends slashes through air in front of the slack area
		//		when thrown floats like a frisbee
		//	hammers - higher chrono impact, crushes mineral
		//		and drill shaft (spear) (mining type weapons)
		//		very strong against rock enemies.
		//		
		//	enemy arms
		//		liz arms (boomeragn
		//		
		//	shika lazer weapons
		//		guardians die from guardian wepaons lol
		//	wands!
		//		korok leaf..
		//		(electricity ball or fire ball or freeze gust)
		//		tripple want (3 projectile version of normal wand)
		//		
		//	springloaded hammer (big big knockback)
		//		
		//		
		//	throwing distance depending on weight
		//	throwing spear goes way further
		//	
		//	master sword
		//		beam ability
		//		
		//	long blade wepaons
		//		powerful knockback!
		//	
		//	
		//	silly wepaons (household items)
		//		lids and slev and mop and kvast
		//		branch
		//
		// wood chopability
		//	wood cutter 2 blows
		//	double axe insta
		//	sword alright
		//	40 damage sword, insta?
		// crate breakability
		//	2 handed weapons is insta
		//	Drill shaft
		//	bombs?
		// stone mineability
		//	hammers and drill shaft
		//	bombs..
		//	
		// intentional ragdolling 
		//	
		// extra set bonuses (random found modifiers on weapons)
		//	throw distance
		//	durability
		//	
		// ^ This is a tool selection puzzle lol. what to use for the situation
		//	to modify the situation in an interesting way given the setup.
		//	
		// bows
		//	draw speed
		//	multiple arrows at once (either split area, scattered or multiple right after eachother)
		//	range/straightness (lack of gravity drop off)
		//	arrow air speed?
		//	
		// light arrow bows???
		//
		//
		// bombs impulses everything in a radius, giving it impact damage (vibrational damage)
		//	and accelerating the objects in the out of the center of the bomb.
		//	
		//	use shield to block the impact, while jumping to get the momentum from the bomb.
		//	
		// Arrow types...
		//	
		// Sneaking/hidden enemy attention system. (sound system and vision system)
		//	possibly smell system.
		//	
		// Powers and special abilities
		//	ever present (only limited by cooldown)
		//		remote bombs
		//		magnetism
		//		chronofreeze or whatever its called
		//		form ice blocks on water
		//		
		//	from bosses (limited use)
		//
		// Upgradeable abilities
		//	upgradable health and stamina (from clearing puzzles)
		//	upgradable shika powers (remote bomb et al.) (from gathering material)
		//	Buyable equipment (from gaining money)
		//	upgraded equipment (from rare resources)
		//	increased inventory space for weapons shields and bows (from finding koroks)
		//	
		// Shops and trading system
		//
		// Quest system
		//	main questline
		//	side quests
		//
		// Sleeping/resting (serves no real purpose in the game, except maybe time skip.)
		//
		// Resting (timeskip)
		//
		// Lore from interaction with hyrulers
		//
		// Puzzle shrines
		//
		//
		// You know what you want? the fuking fire emblem chained weapons you can pull back.
		//	fuck yeah.
		//	
		// Interesting world mechanics
		//
		//	floating balloons attachable to objects
		//	objects float or sink
		//	
		//	cutting grass
		//	
		//	tree -> wood -> fire
		//
		// CLIMBING
		//
		// freezing things in mid air and walking on top of them as temporary platform.



		//What imitators of botw fail at:
		//	ocean horn
		//		impressive feature set
		//		good graphics
		//		.
		//		bad attention to detail and integration
		//		non curated.
		//		lack of focus? (diversity but not consistency.)
		//		.
		//		haphazardly put together.
		//		.
		//		pretty but shallow (when you go closer you notice it is made out of paper.)
	},

	parkour2dCityscapeGame: { //basically: slick/tight movement/transportation platformer....
		// like Spiritfarer but all about that movement. maybe even skate boards and shit
		// other comparative games with slick movement... fancy pants? Spiritfarer, BOTW, that 3d parkour game? spiderman games?
		// skaing gates... altos adventure! Abzu, Journey
		// supersmashbros melee has fuking tight movement.
		keyActivities: {
			gliding: 1,
			jumping: 1,
			accelerate: 1,
			decelerate: 1, //maybe
			//choosing where to go... reaching goals.
			timeTrials: 1,
			racing: 1,
			exploration: 1,
			fineControl: 1, //like smash... analog controls.
			//reaching hard to reach areas
		}
	},
	rollerSkaterCityScapeSkatingGame: {
		// making them a roller skater city scape skating Game
		// fashionably skate through a city with cool slopes and aqueducts etc.
	},

	boatHotelGame: {

		// it is about transporting  passengers to their destinations while also satisfying their needs on the way, serving food, giving them space to sleep or socialize.
		// It is like the ultimate multitasking game.

		// but it is more like a hotel where passengers buy their own food
		// and you have workers on your boat like a crew of people and shit.
		// or like going to islands, picking up people... sending some people to go on missions on islands and then pick them up later.mcam
		inspiration: {
			// Spiritfarer
		},
		keyLocations: { // key locations denotes the environments in which the game mostly takes place... can be hardly constrained or kinda open.
			boat: 1,
			islands: 1,
		},
		keyActivities: {
			navigation: 1,
			timingActivities: 1,
			economy: 1, //?
		},
		has: {
			hotel: {
				on: {
					boat: 1,
				},
			},
		},
	},


	// ___ DARK ___
	metroidvaniaGame: { //...
		is: {
			videoGame: 1,
			conventionally: {
				sidescroller: 1,
				platformer: 1,
				actionGame: 1,
			}
		},
		has: {
			conventionally: {
				playerCharacter: {
					// the player starts weak and gets more powerful as they progress.
				},
				world: {
					// consisting of many rooms connected on a map forming labyrinth like structure.
					// the world is an otherworldy realm with scarce familiarity/ordinarility.
					has: {
						rooms: {
							someOfWhich: {
								// boss rooms
							}
						}
					}
				}
				// world has items that can be obtained to empower the player in various ways.
			}
		}
	},
	castlevaniaGame: {
		is: {
			metroidvaniaGame: 1,
		},
		has: {
			castlevaniaAesthetic: 1,
			world: {
				has: {
					castle: 1,
					playerCharacter: {
						is: {
							protagonist: 1,
							// castlevania conventionally has two types of protagonists:
							// 1. Vampire hunters there to stop Dracula or evil forces.
							// 2. Vampire born or vampire spirits that deal with their past.
						}
					}
				}
			}
		}
	},



	worldbuilding: {
		// Difficulty and challenge appeals to the elitist high-life feeling. Only for those who have what it takes. Exclusivity. Very powerful wow.
		// The onlything meaningful that is left is drama, beauty, extraordinarity

		// It is a commentary of high culture and an analogy for a kinda dystopian existance.
	},
	gameFeel: {
		// Avoid lame attacks.
		// If someone is slashed with a sword I want it to feel like they are freakin slashed with a sword. Not just a bonk on the health bar.
		// I suppose stamina gets more important. Like your character can do good shit as long as they have enough stamina like auto dodge?
	},

	// One idea is to make the game super difficult. Kinda dark souls.

	// Develop a feeling of excellence in the player.
	// Simulate an eternity of training
	// "Super human" skills Provided by the actual player… (like super dodging skills etc, just from training the player) 
	// ^ Appeals to the mastery of action games.


	// Time as excuse for excellence:
	// Time Loops

	// Slowmo
	// Epic slow motion battles
	// Freeze time, get time to think
	// Good tool is like a time crystal that gives an excuse for the player to master a situation, (kinda super hot style..) but predictability is also boring though so maybe not.

	antithesisOfDarkGangsterGameAesthetic: { //	Anti-thesis of gangster game. Aka What to Not use
		// General:
		// Big action and explosions
		// Instant sudden Sequence of events
		//
		// Ridiculous
		// Ridiculousness
		// Stereotypes & Exaggerations
		// Silly
		// Warm (ok in very small amounts, like dying lights and small hopes.)
		// Hearty
		// Warm colors
		// Strong saturation
		// Rainbow "colorful"
		// oversimplified
		//
		// Clothes/ Fashions
		// "Mismatching" clothes
		// Glitter
		// "Normal" clothes
		// Forest gear
		// 70s Hippie 80s shoulder muffs
		// Monocle guy.
		// ^Do not use existing oversimplified icons.
		// Beach
		// Safari
		// Fluffy Clothes
		//
		// Weapons
		// Silly
		// unbalanced
		// Things not exclusively designed to be weapons
		// Primitive tech (Rocks & twigs)
		// Improvisatory weapons
		//
		// Styles
		// Silly Adorable cute Girly
		// "Summer"
		// Hearty
		// Dentist/Hospital/Clinical
		// Thriving life & forests
		// Dirty, Filthy, Gritty
		// Vibrant
		// Vibrant colors
		// "Natural lighting"
	},

	excellentControls: {
		// Excellent controls:
		// The player should be able to make their character Quickly switch between their weapons & spells. (in a game majorly about using different weapons and spells )
		// ^ in order to:
		// 	Give the player many options and dynamics what to do.
		// 	Make the player feel in control
		//	 Not interrupt the flow by forcing some cumbersome menu...
	},


	modernVampireGangsterAesthetic: {
		// Street metroidvania
		// Highlife
		// Hires gangster

		// clean apparel

		// Cool serious

		// Real world with vampires
		aesthetic: { // and feeling
			// Feeling of the game: (throughout the game)
			//	Eerie
			//	Serious
			//	Cold

			//	Aesthetics:
			//		Ultimate horror and beauty.

			//occult
			// Mysterious world, incomprehensible and magical.
			// a lot of symbols / symbolism of death and dying.

			// Subtle rather than pronounced.
			// Piece of art of beautiful broken unholy (unfulfillled) things

			// Beauty of the forbidden.
		},
	},
	metroidvaniaFightingPlot: {
		// Immortal demons so bored they are Challenging each other for fun. (Demons have nothing better to do)
	},


	darkAestheticRepresentation: {
		Emotional: {
			// Emotional Range:
			//	Neutral
			//	Pleasure
			//	Pain/Despair
			//
			// Generally subtle emotions
			// Drained Exhausted
			// Mellow
		},
		mannerism: {
			// Serious Disciplined Sharp
			// Exclusive
			// Of course everyone that is cool is smoking as long as it looks cool.
		},
	},
	// chaos architecture world

	modernGansterCulture: {
		//Related:
		//criminal organization
		// yakuza
		//urbanCulture
		streetCulture: { //for lack of better words
			//smoke
		}

	},

	// Dark + sophistication + gangster

	metroidvaniaGangsterGame: { // Street metroidvania game
		index: {
			// INDEX:
			// 1 narrative / setting
			// 2 aesthetic
			// 3 representation
			// 4 manner
			// 5 soundscape
			// 6 style (colors should be part of style)
			// 7 style things (effects etc)
			// 8 COLORS
			// 9 Drawing constraints
			// 10 playable character classes
			// 11 is
			// 12 has
			// 13 INFLUENCES AND INSPIRATIONS
			// 14 Setting
			// 15 Place
			// 16 People and factions
			// 17 Cool scenarios
			// 18 NARRATIVE:
			// 20 features
			// 22 what the game is about	
		},

		theme: {
			// Every story about forsaken people or vampires and wizards.
			// it is always about filling the empty void in your heart.
			//	just the feeling that something is missing, that it incomplete, unresolved.
			// something holds the space open you need to put something in there.


			// Colonialism... gathering/plundering all the exclusive resources from all over the world.
			// Thriving built on dysfunction


			// The idea of finite resources.

			// Curation
			// A beautiful selection of features isolated from nature.
			// A safe haven curation of kuriosa interesting items

			// Kuriosa, märkvärdiga föremål.
			// omsorg, överprotection.

			// You only feel the need to protect and keep secrets in a world that is hostile or dangerous.

			// Stealing what does not belong to you
			// Relying on unsustainable contracts...

			// Imbalance.

			// “complex”
		},


		darkThemesSymbolism: {
			// 	Windy Rain
			// 	Clock ticking
		},

		soundscape: {
			// 	Windy Rain
			// 	Clock ticking
			//	dripping
			//	howling wind
			//	thunder
		},



		Style: {
			//    Not gory nor disgusting
			//        NOT
			//            gory
			//            disgusting
			//            Does not involve healthy normal people. 
			//        It has to be like dark people in a dark universe that is completely isolated from anything wholesome.
			//        Generally want clean kills than gruesome stuff.
			//        I like the idea that there is time travel and that everything is like already done or unchangeable... "Nothing can done, really changes."
			//            You travel through the memories of past and future.
			//                and that everything resets over time.
			//                but you still have to struggle as a player to go places??

			// Characters
			//    beautiful young, slim and fit people.
			//    Tall women?


			// add to general character design (narrative with characters)
			//    gimmicky characters.
			//        enough to be memorable.
			//        often they have a associated quality
			// __________________________________________________________

			//
			//            vampires, angels, demons, 
			//            kings

			// Metroidvania prologue
			// "The gods live on beyond this world."
			// **(panning footage of a statue in the real world of powerful deities.)**


			// Honestly the game could start with a visiting religious places in the real world casually.
			// and then you are taken into a journey
			// honestly trapped in the spirit world is also a good plot

			// style things
			//		VHS filter
			//		Chromatic Aberration
			//		Neon stuff.
			//		 Clean and slick
			//		 Shiny, glossy.

			//	Personality:
			//		Edgy
			//		Serious
			//		Elitist?
		},

		fashion: {
			// functional
			// Desert, tech, tactical
			// Strict, functional, Decorated, embellished
			// Stylized
			// custom made, personalized.
			// Embellished, ancient artifacts.
			// Historical
		},
		sophisticationAesthetics: {
			coldChillyColorFilter: {
				// not warm
				// Colors: exclusively framed as cold
			},
			colors: {
				// subtle
				// muted
				// white
				// clean
				// gradient
			},
			lavishColors: {
				// strong reds gold purple, blue green,
				// lavish fabrics and silvery
				// high contrast,
				// blackest black
			}
		},

		colors: { // COLORS
			// colorscheme rules:
			// base background tone is always a kinda colorless dark pigment:
			//		usually red,brown,green,teal,bluish, purple, with a bright foreground usually white bright gray/beige and then there is usually like 1 highlight color of intensity, pink, orange, red blue, purple,
			//
			// if there are more color than the highlight, those colors are also toned down to not compete.
			// _____________________________
			// metroidvania colorscheme:
			//	mostly dark, not too dark to see things, moments of brightness
			//	
			//	Cold
			//	
			//	generally tumblredge lol
			//	
			//	gold silver
			//	unnatural (surreal) lightings
			//		monochromous
			//	
			//	grayscale (black, gray white)
			//		might have sparks of intense colors (red blood, orange lava, etc)
			//		
			//	bleached out, drained of life force


			// Subtle (to context)
			//	dark blue, dark green, 

			//	color theme approximation
			//		autumn
			//		matte
			//		coldghiblitime

			// grayscales
			// _____________________________
			//	colors get more intense the more painful
			//		blood, fire, dangerous things.
		},

		drawingConstraints: { //Drawing constraints
			// hard-edge (clearly define, no sketch, binary black or white discrete)
			// smooth lines (not rough or noisy.)
			// Graphical: Repeating patterns of symbols

		},
		characterClasses: {
			// Normal human - Normal person dragged into the vampire world

			// Part time monster:
			//	Either cursed/spelled or bitten by a monster. 
			//	They are only monster part-time.

			// Beastborn - furries. humanoid animals or demons.
			// Vampire that does not try to "save the world.
			// Damned - or otherwise transformed humans like werewolf or vampire

			// Playable character classes
			//	Tactical demon slayer (Lots of cool weapons, good at dodging.)
			//	Huge armor strong person (conan/he-man)
			//	Slonk demon guy/girl (half demon)
			//	Clever/experienced witch/wizard
		},
		is: {
			MetroidvaniaGame: 1,
		},
		has: {
			playerCharacter: {
				is: {
					gangster: 1,
				},
				has: { //owns
					weapon: {
						//many different weapons. uh?
					},
					gun: 1, //multiple guns...
				}
			},
		},
		influences: { // INFLUENCES AND INSPIRATIONS
			// initial idea:
			//		 Bang bang, smoke, streetwear, techaesthetic.
			//		 It is an urban vampire baroque
			//		 (more like Digimon than Pokemon lol)
			//		 Futuristic or 2020 modern.
			//		 Zeitgeists
			//		    the rawness/bluntness of the 80s with the slickness of 2011 and maybe the coolness of the late 90s, 1999
			//		        the calmness of the 60s, and the saturation/intenseness of 70s
			//		        the relaxed and taking self serious of 50s, taking place. confidence.
			//		        40s strict clean serious wardrobe. functional, durable, dawn of exclusively modern clothing?
			//		        (feels less classical)
			//		            train everyone to do practical/strict work.     
			//		            like wtf the 60s had this progressive music like lets take intricate music things serious.
			//		 
			//		 Influences and inspirations
			//			Devil May Cry???
			//			Vampire hunter blood lust D
			//			Howl's moving castle...
			//			Gorillaz in a bag 2d videos
			// Inspirations
			//    Existing styles similar or related to metroidvania
			//    Game-play similar to metroidvania
			//		GAMES
			//			CastleVania: Aria of Sorrow
			//			Hollow Knight
			//			graveyard keeper
			//			Nox
			//			Monster Hunter
			//			Botw
			//			League of Legends (dope characters and lore?)
			//			StarCraft
			//			Fantasy universes:
			//				Lord of the Rings
			//				Heroes Series
			//			 Game-play / Mechanics
			//					MapleStory
			//					MegaMan 0
			//					3d fighting games like monsterHunter and darkSouls
			//					Madness combat gangsta wars...
			//					You know the "cool" weapons and the Noir with blood style.
			//					Fighting games to some extent
			//			tekken
			//		SERIES
			//			Mr Robot
			//			Buffy the Vampire
			//			Blade
			//		ANIME
			//			Vampire Hunter D
			//			Naruto
			//			Mushishi
			//			Van helsing
			//			Monster
			//			Madoka magika
			//			One piece (bosses evil disconnected from humanity), Zoro and Jinbe and greatest swordman, and the zen samurai dude
			//			samurai shamplo
			//			Cowboy bebop
			//			Steins gate????
			//			samurai shamplo
			// 			Death Note
			//			Digimon?
			//			Wolf kids
			//		MOVIES
			//			Alien
			//			The Matrix
			//			Spiderverse
			//			Kill Bill
			//			Taran och den magiska kiteln
			//		Culture
			//			k pop / korean rap
			//			hip hop trap gangster stuff
			//			Jazzy
			//			Highlife
			//			Cultures around people:
			//					Billie Eillish	
			//			Russian culture????
			//			fantasy, vampire, demon, occult?
			//			futuristic tech / sci-fi
			//			Gothic goth culture
			//			Cyberpunk
			//			Western movies
			//			Underground cultures (both historical and current)
			//			Chinese underground culture
			//			Citylife
			//		Marceline the vampire queen (+ those themed episodes about vampires)
			//		Grimes (videos)
		},
		setting: {
			//		 Technology could mix in, in high tech forms.
			//		 Decorated artifacts.
			// Ruined and deserted worlds and cities. ominously empty. all life force has been sucked out dry.
		},
		place: { // Place
			// World:
			//	Dying, overgrown, isolated/frozen/protected/sterile, dead/deserted, gathering enmassing (piling/concentrating resources)

			// Snowy cityscape
			// Rainy cityscape
			// Foggy cityscapes
			// Abandoned streets? (I can not imagine a lot of wandering regular people lol)
			// Running in empty landscapes by the edge of dawn. where you are still protected from direct sunlight.
			// Nightlife
			//
			// Strong shining moon.
			// Ruins
			//
			// Chaos architecture interiors of buildings that go from gradually cityscape interiors to baroque.
			//
			// Gangsta city (city life and high life)
			// Moscow snowy streets
			// Foggy dead forest (fog branches shadows)
			// Botanical garden (indoors.	
			// _____________________________
		},
		peopleAndFactions: { // People and Factions
			// Young Adults to Old but youthful looking older people
			// Maybe old looking witches/wizards and younger cherubs or something...
			// Rich people with actual good taste and access to immense resources.

			//Those who are Thriving in dysfunction
			//Exponential systems built on destruction.

			//Groups/Labels
			// Mainly existing disconnected from world
			//	Deities
			//	Demons (manifestations of pain)
			//		Demons
			//		vampires/werewolves/mermen
			// Mythological beings
			// Shadows (manifestations of fear)
			//
			// Artifacts (objects powered from people's beliefs)
			// ^ manifests when people believe
			//
			// the rot - Invisible, bad luck, cursed, misfortune, life crumbles and weakens around it. objects spontaneously break or combust.
			//
			// Humans that made deals with devils, demons, angels and deities.
			//	warlocks, vampires, liches, 
			//
			//Masters of their beliefs and observers of things essence:
			//	Witches/wizards
			//
			//
			//	Shadows
			//
			//	Gangsters
			//	Military?
			//	AI/robots?
			//	Normies
			//
			//
			// Gangsters working for demons?
			// Gangsters with supernatural powers.
			// Levitating gun fights with swords.
			//
			// Walking through chaos architecture.
			//
			//
			// Zombies,
			// Bats, giant spiders?
			//
			// People turning into demons/monsters/werewolves/mermen?
			// Witches
			//
			// Undeads.
			// Ethereal/ghosts
			//
			// Just an empty robe...
			// of course death in a hoodie.
			// _____________________________
			//	Beings and "factions":
			//		Shadow beasts... shadow fiends (faunwood style beasts and shadows merged.)
			//		
			//		The great nothing... (void)
			//		The god of chaos.
			//		
			//		The great beyond
			//		
			//		The threshold... the boundary... the in-between, the (like a line connecting both sides)
			//			intermediary, intermission, medium, mediation, medial, middle.
			//			
			//		
			//		Then there's all these lesser gods and demons that are like comprehensible characters.
			//		
			//		VERY iconic and memorable characters. (and monsters/demons, etc)
			//		
			//		Cosmetic excuse. You can use magic to hide your real armor under the cosmetic clothes off your choice. You could even use magic to get the protection of a big Armor while being lightweight through magic
			//		
			// This world is hidden in plain sight. Hidden with spells and illusion	
			//		
			// there are characters that does the fourth wall breaking stare.
			//		
			// some epic dialogs. (I do not know how to write good dialog, fuk.)
			//			It is like philosophical in nature.
		},
		scenarios: { // Cool scenarios
			// And a lot of people spontaneously combusting because fire looks cool.
			// Demons that are just like a lot of arms coming out of a seemingly normal person.
			// People that turn out to be harpies or otherwise have wings.
			//		
			// Medusa hair characters. and animal tamers like snakes, bats, spiders, scorpions
			//		
			// and a bunch of shadows beings and maybe smoke being.
			//		
			// Haunted people with levitating objects around them.
			//		
			// Floating patterns of weapons, like sets of swords/knifes
			//		
			// Or a lot of guns.
			//		
			//		
			// arms coming out of the ground.
			// tentacles coming out of the ground
			//		
			// illusions and transformations. (rooms held up by spells or magic)
			// _____________________________
		},
		narrative: { // NARRATIVE:


			// Maybe it could even be about the super wealthy killing the rich.
			//
			// The game start with you attending some really bad taste rich people place.
			//
			// But that is interrupted by some uber wealthy demons that destroys them.
			//
			// and it is this like bigger fish eating bigger fish feeling like oh shit there are bigger fish.


			//_________________________________________________________
			//actually would be cool if there was like Soma the drink of gods
			//	and something like separating body and mind
			//	separating from teh physical realm
			//_____________________________________________
			// Call to adventure?
			// Confrontations (The demons have what you need)
			//______________________________________________________
			//		 In between a set of disconnected places. connected only by symbolism and dreams.
			//		 Non time specific... You go into historical times and places and take.
			//		 For us time no longer makes sense. We access all times and worlds to draw (copy) their essence.
			//

			//
			// "I kill your time - every moment of your life"
			//
			// "Oh so You are the new one, We finally met"
			// "You are the one turning all of the underworld over."
			// ____________________________________________
			//		 It is like the secret life of the city that you do not know is going on. underground tunnels and shit.
			//
			//		 lore/history:
			//		 	very ancient and religious families connected to the gods.
			//		 	People that have made deals with the gods.
			//		 	cursed people and artifacts.
			// ____________________________________________
			//		a time traveling society of rulers ruling over all time.
			//		
			//		what changes in this society, is internal dominance. Who governs what domain.
			//		
			//		Who is banished.
			// ____________________________________________
			//		What is the player/character that gets invited into such a world?
			//
			//		Dracula's realm leaked into the ordinary world
			//		church isolating these worlds?
			//		
			//		it manifested when people lost faith and craved chaos and destruction	
			// ____________________________________________
			// We live in the future where we deserted all these deities of the past.
			//	only few artifacts remain
			//	and some of these artifacts spirits can get you back in time???
			// ____________________________________________
			//		 Break from mundane life...
			//		 "I wish there was something more than what I have. I know there is something beyond this, I can feel it."
			//		[something significant must happen to break from the mundane]
			//		 "Demon: I saw your soul, You entertain me so I chose to approach you."
			//		 "What do you say, should we take him with us?"
			//		 "I am warning you, they are watching you Neo" "I know what you've been looking for, why you've hardly sleep, why you day after day ..."
			//		" you are looking for him, i know because I once did the same thing, and when he found me, he told me i wasn't really looking for him, I was looking for an answer."
			//		 "The answer will find you, if you want it to."
			//		 blurred lines between waking and sleeping. Suddenly wakes up as if it was a dream but then turns out to have been real.
			//		 You have a problem with authority, you believe that you are special, that somehow the rules does not apply to you, obviously you are mistaken.
			//		 this business work because every employee understands they are part of a whole
			//		 (Maybe something similar to the matrix where it begins with things being slightly off u know.)
			//		 What is the forbidden fruit that makes them approach you
			//
			//		Choosing to take a risk to go through a barrier
			// ___________________________________________
			//	Narrative world building
			//		 Castle is a seal of protection to protect/sustain/suspend artifacts
			//		 Fengshui mind palace, memory castle. the inside of a powerful character is their castle.
			//		 You literally fill your own castle/palace with powerful artifacts at powerful position, suspended for optimal flow of energy empowering your character.
			//		 As your power gains your inner castle gets stronger with more and more areas and mythological beings start inhabiting your castle. You can teleport to any point of your castle.
			//		 But there are also unconscious parts of your own castle and there are always places where your castle leaks into the real world.
			//		
			//		 Every person, every artifact has its own inner room of memories. the room is a measure of how much soul the object has.
			//		 Every artifact both binds you and frees you.
			//__________________________________________________________________
		},

		features: {
			// Different ways to enhance and imbue the weapons and relics you find
			//     Maybe with like a chance of success or failure.
			//     To transform the resources you do not care about into a chance of improving or getting resources you do care about...
			//	character Posing menu for taking nice screen shots and for communicating with online friend	
		},

		whatTheGameIsAbout: { // What the game is about:
			// Doing what is forbidden.
			// Reaching forbidden areas.
			// Locked doors and mysterious impassable barriers.
			// Interesting with rituals the player need to get to certain places.
			// Interesting with specific keys the player has to buy at stores.
			// Secret areas which entrance is just very well hidden.
			// House purpose - isolate interior environment from exterior.
			//     creating an isolated space/environment
			// Just not showing the stats of items... you should just feel it in game lol.
			//     Focus on experience rather than min maxing???
			//     idfk
			//     would I play castlevania if I had no idea which weapon was good?
			//     Can I indicate effectiveness of weapons in other ways than damage number display?
			//         some linear impact effects.
			//     Implement: the longer you use a tool the better your character becomes at using it and very similar tools.
			// Lockig unimportant areas behind?
			//     or making them accessible easily for fun...
			//     or remix them as harder levels?
			// Scenarios
			//    You get walk on water ability so you can walk over a lake.
			// Evil laboratory ai tech could work with aesthetic - evil ai, technology, clones in vats and shit could work with the aesthetic


			// References when designing 2d metroidvania games
			//    Ori and the will of the wisp (designing desert level: https://www.youtube.com/watch?v=gIdHTL18kTU)
			//    Those new 2D Rayman games?
			//    Cool things for metroidvanias
			//        burrowing as traversal method in a metroidvania.
			//        generally transform into monsters and animals. for different movement.
		},

		// demon court, the court of the underworld. everyone's suited up.

		//	Knifes, and guns,
		//	Blood...

		//	Portals to "Dracula's" castle 
		//	Banishing each other from their territorial realms.
	},

	// The idea of excessive magic use and preparations for extreme buffs:
	// for metrodvania game:
	//	you have to put on a shit ton of buffs and shit so that you can go 5 minutes 
	//		to another planet just to mine some rare ore and then return.

	metroidvaniaHouseBuildingGame: { // castleDesignerVaniaMMO:
		// It is a base building and action game where the layout, architecture of your base determines your characters power levels and capacities.
		// Dungeon designer MMO
		is: {

		},
		has: {
			challenge: {
				has: {
					baseBuildingAbility: 1, //basebuildingChallenge: 1,
					architecturalAbility: 1, //architectureChallenge: 1,
				}
			},

		},


		// Metroidvania style
		//		level traversal.
		//		enemies and fights.



		// Why multiplayer?
		//	majorly enticing to particular people
		//		
		//	Without it there is little point in
		//		level design if no one plays your level.
		//			why level design challenge?
		//		interior design, since interior design is partly made for people to see.
		// Invite people to your castle. in order to
		//		hangout
		//		socializing space.


		//	Secret powerup rooms. you need to build a path of rooms to those rooms to access them.
		//	so you build like a bridge to existing gridspots on your map.

		// Maybe Cool ideas:
		//	The smaller your castle is the harder it is to spontaneously find?
		//____

		// maybe there is a chaos realm where you randomly meet other players
		// and you get to fight through random rooms.
		// and maybe the chaos realm restructures itself every now and then.
		//	either a bit all the time or once ever 24 hours.
		//
		// ^problem with this idea is 100% random dificulty...
		//	will you find super duper boss of doom or like the easiest piss enemy?
		//
		// ___
		//
		//Questions:
		// Is the game about architecture or about fighting through castles?
		// How do you make things Metroidvania when people gain all powers outside the castle they are in?
		//	Do you steal other people's artifacts?
		//	Are there npc castles?
		//	Are there duel pvp?
		//	How can the player decorate nicely?
		//	What limits the player from making broken troll content castles?
		//	Do you get rewards from people dying in your castle?
		//	Can you trade with other players?
		//	Can you have areas where you can just hang out?
		//	What stops people from making castles that are super easy?
		//	What stops people from giving away powers for free?
		//	What age group, what amount of blood? what amount of gore?
		//	How much narrative consistency?
		//	How will your castle look like? will the exterior be procedurally generated based on your room compositions?
		//	Multiplayer?
		//		It does not necesarily have to be multiplayer to have these mechanics of characters represented as buildings...
		//	Explore pre-existing labyrithm castle vs gradually grow your castle?


		// When you "die" in someones castle you are thrown out of their realm and you are limited to enter soon again.
		//	you need some spell in order to enter a castle and this spell is broken if you are
		//__________________________________________________________________

		// People are castles
		//	A castle is the inside imaginary world of a character
		//	A castle is like the imaginary world of someone else and all the relics are there to empower their characters.
		//	So castles are like the interiors of bosses and people. and so you also design your own castle and interior of things to gain great powers. or something .... cool idea


		// Instead of health bar you could have objects or items
		//	Something like voodoo dolls as extra lives, it destroys the item instead of yourself
		// Maybe Something like this but it affects the rooms in your castle.
		// But if I wanna avoid destroying finely crafted user content then it probably just locks up the rooms. 
		//
		// But I like the idea of magical barriers, force fields and offering of valuable possessions instead of a classical health bar system.


		is: {
			castlevaniaGame: 1,
		},



		// related biases: metroidvania, castlevania, fightclub, gangster,

		// Castle in a world/realm upheld by magic
		//	invite new sections into your castle
		//		The player has to master them before they have control over them


		// function exclusive rooms (the function can not be in any other type of room that has not the same exclusively dedicated function for it)

		// game-play
		// I guess it is 50 50 
		//	build structures that empowers your character
		//	Getting good at the fighting aspects.
		//	Designing interesting environments 
		//		(Metroidvania Level design)
		//		nice compositions and architecture and art

		// in the start of the game:
		//	you conquer your first home castle.
		// Maybe you just start with a regular house... and then it grows into a castle as you modify it with magic

		// Keys from others castles that allows you to visit new places in your own castle.
		// There are parts of your castle that are locked away that you do not know  what is inside
		// Unlock secrets within.

		// Castle Mechanics:
		// Every castle has a heart in the center.
		// The heart room is like a control room from which everything can be accessed.
		// Heart room is like heart in howls moving castle, the heart is thumping. dok dok, dok dok.
		// Maybe the heart room is like you can poke every room, that it works like a hub world of all the things you need, access to store, teleport to enemy castles, etc, etc

		// Room mechanics:
		// You have to master a room before you gain control over it.
		// By clearing a room you gain control of the things in the room
		// Rooms that are under control can be customly furnished by its owner with the furniture the owner has.
		// Furnishing
		//	Turn money into candles that light up your castle...
		//	You have a room with the real furniture which you use magic to duplicate across the castle.
		// Specific constellations of rooms, furnishings, and room themes gives your player character specific abilities and stuff.

		// Defined rooms:
		//	game functions
		//		weapon room
		//		inventory - the inventory is a room in your castle.
		//		healing/recovery room - maybe you have a limited amount of lives/ healing recoveries.
		//	Defense functions:
		//		boss room
		//		enemy room (i think all the normal rooms are kinda like these)
		//	in-between areas... room connectors.
		//		corridors,
		//	convenient functions
		//		shop room
		//		portal room
		//		cafeteria room
		//	bank/chest room - chest room that is filled with chests for long term storage. to protect and lock things up
		//	Engine room
		//	Smeltery/forge
		//	mine room - mining caverns.
		//	door room
		// outdoor areas
		//	garden
		//	forest
		// biomes:
		//	underground
		//	sky
		//	forest
		//	lava lake
		// dusty attic
		// overgrown place..


		// Interconnectedness:
		// Every thing is all of these: a weapon, a furniture, a character/monster/bosses, a room
		// The small model of the castle is in the heart of the castle (like a dockhus)
		//	Artifacts have personality? (things are people)
		// Artifacts and clothes and weapons and furniture are all rooms. you can enter all of them if you have magic strong enough.
		// The more cursed/enchanted an item is the more funky stuff it has in its room.
		// Aesthetic/narrative speculations
		//	Ship in a bottle
		//	Maybe the castle is in a bottle which you have in your pocket.
		// Maybe castle weakens as player character weakens
		//		The castle gets smaller?
		//		or
		//		Things get destroyed?


		// Artifacts - magically imbued objects
		// You go through dungeons in order to obtain artifacts
		// Artifacts are objects which transforms to monsters or powers inside your castle.


		// Managing rooms:
		// The control room which you can later re-arrange rooms from.
		// Adding rooms to your castle:
		// New rooms are artifacts which the player has to spawn in existing rooms
		// Creating barriers between rooms
		// Leaving a door open etc.
		// What parts in your own castle do you have control over???


		// Castle Design
		// The powers you have lets you design for that power:
		// Having double jumps power lets you design double jump castles.
		// Fengshui
		//	Arbitrary rules of how to place rooms and things in rooms
		//		to force good design.
		//		because you can not just place trials of power rooms after each other.
		//			(like double jump, underwater, shadow
		// Maybe you just get powerups by following conventions of a good castle design.
		//	it is like in the fengshui system.
		// Maybe your castle has to be built to be accessible by the spirits of maintenance (NPCs) in your castle...
		// Principle: if your castle requires a power to reach an area then that power has to be made accessible to obtain in the castle accessible before that filter.

		// The player obtains furnitures throughout the game


		// Questionable MMO/Mobile game "social network game" aspects:
		// You can make your castle appear in others worlds which gives you passive income but also open up for others to raid your castle
		// If your castle is raided it is banished from that realm for a moment...

		// you can have NPC monsters that do jobs in your castle, maintenance, cleaning and cooking and brewing potions, crafting, smelting.
		// There could be small quests you send NPCs on missions.
		// There could be cooking/farming and other micromanagement.
		// The player has to protect their staff
		// I imagine maintenance can feel a bit like graveyard keeper
		//	You kinda just clean up a church and you unlock new parts
		//	You remove the dust and get better things u know.


		// monsters/NPC's
		// They guard rooms by attacking any intruders
		// There are tiers of monsters:
		//		regular
		//		powerful
		//		bosses
		//	The bosses might not even be friends with the owner of the castle.
		//	But they are in the way for any intruders.


		// Some artifacts become bossses and those you use to protect valuable possessions.
		// Enemies have spawn rate instead of respawning every entering of the room
		// So it is like a Minecraft spawner. puts things in an area based on some rule
		//	Stops spawning when a maximum amount is reached.


		// Optional principles of the game:
		// Turf wars
		//	Maybe the game could be like it is Dracula's castle but you take it over room by room.
		//	Isolated instances of the chaos realm.
		//		You cut a piece out of the chaos realm and isolate it, there you have your castle.
		//
		// Limited number of resources in the world
		//	You can have a metroidvania that works with few power ups shared across multiple castles 
		//	You have to plan heists to steal the limited resources.
		//
		//
		// An eye for an eye
		//	Every benefit has an equal and opposite downside.
		//	If you gain something you lose something
		//		Maybe you get more cursed, etc.
		//		More attack but weaker castle.
		//		Etc etc.

		// Crazy ideas and extensions
		// open world?

		// Player Character:
		// the owner of the castle can teleport freely through their castle.
		// The player is as powerful as their castle
		// You have to level up a room (through feng shui) in order to make it generate powers for you
		// If you lose a power, places in your own castle becomes inaccessible.

		// map as obtainable item
		// bringing glowing things as a player to see in dark
		// lighting like candles and stuff.

		// problem with design:
		//	There is a joy of discovery that stops when you have seen the powerup
		//		when you have the underwater power it is maybe not that interesting to see
		//			an entrance leading down water.
		//	It is also like if I already have the way to go through spikes or the things that lets me breathe underwater. then those barriers are useless.
		//	"yeye another spike world another double jump, whatever"
	},




	// curse objects
	//	You can curse objects which makes those harder to deal with by the ones that steal them.





	// The player can swiftly change which rooms connects to which.


	// "MECHANICS"
	predefinedSlots: {
		// A predefined x spots which you can fill with x
	},
	lateGameUseForEarlyGameResources: {
		// Recycle or modify low quality rooms that you dont want anymore	
	},
	blackArtifactMarket: {
		// A market place for various hidden gems and treasures of the world.
	},




	// random resources -> improvisation, riskmanagement,




	// Defence game. build up

	// The goal is to defend a player's territories or possessions by obstructing the enemy attackers or by stopping enemies from reaching the exits, usually achieved by placing defensive structures on or along their path of attack.







	// Possibilities, what can happen with a body

	//Basic design
	//	An enemy body that touches the moving blade takes damage 
	//		this damage based on the force multiplied by the sharpness in the moving blade
	//	There is a room
	//	Enemies exists in rooms
	//	When player's body touches an enemy the player take damage based on the impact.
	//	Enemies are humanoid that walks towards the player or walks in a predictable pattern.
	//
	//Cool details:
	//	cut of limbs
	//	todo define in terms of % importance of different types of fun.
	//		also octalysis

	//physics
	bluntForce: 1, //relies on mass, has crushing effect
	sharpForce: 1, // limited area but higher concentrated force. dissects.


	combatChallenge: { // battleChallenge: fightChallenge:
		// violent confrontation to achieve an end. overcome, eliminate, or prevent
		// Any game where the method to accomplishment is through the challenge of incapacitation or destruction of other agents.

		tropes: {
			abstractVideoGameViolence: {
				attackAndHealthPoints: {
					// attackPoints / attackPower
					// HealthPoints

				},

				geometryOverlapViolence: {
					// Agents has geometry that gets hurt when overlapping with damaging geometry,
					// hurt geometry
					// damage geometry
					//^ (ViolentForceGeometry and violenceSubjectGeometry)					
				},

				//binaryCapacity: often games have two discrete levels of agency/power/capacity. On or Off. Where capacity is turned off the moment healthPoints reaches 0.
			},
			selectiveViolence: {
				OnlyDamageEnemiesAndDestructibles: {
					//attacks can only be recived by enemies and destructibles
				},

				explosiveDamageException: {
					//You can get hurt by explosions you cause.
				},

				environmentalObjectException: {
					//as the hurt geometry implement is not wielded by the player, they can be hurt by its effects.
					// examples traps, liquids, fire,
				},

				temporalViolenceGeometry: { //temporally activated violence geometry
					//violence geometry collisions are only checked in active state.
					// activation only occurs on specific time windows of specific attack animations.
				},
				FriendlyUnitsCantDamageEachother: { //Friendly-fire off
					//no self damage
					//no in faction damage
					//no unit can cause damage to any unit in a belonging faction.
				},

			},
		},
	},

	principlesOfGameDesign: {
		//^ forgiving game desing

		// make designs forgiving when the main point to be taught is something else.
		// if the main point is to see the architecture, leave out enemies and obstacles for that part.
	},

	metroidvaniaIntro: { // Some idea of metroidvania intro:
		//	I want a menu that feels like a solid
		//		either just pure black, or monocolour gray.
		//
		//	You woke up in this desolate void world
		//
		//	There is a ground
		//
		//	There is wind
		//
		//	And a smoky mist. all around.
		//
		//	And there is the strong shine of the fullmoon.
		//
		//	The only way you can fill this place is by blundering other worlds.
		//
		//	"I don't remember anything"
		//	_
		//
		//	Somehow you get a portal between worlds.
	},


	metroidvaniaCompatibleMechanics: {
		autoBattle: 1,
		coop: 1,

	},

	autoBatleMetroidvania: { // Inspired by SNKRX
		// (aka metroidvania with passive attacks only)
		// auto battle metroidvania
		// like snkrx but it is a metroidvania style game.
		// touhou dodging game (aka bullethell.)
	},

	// Common video game trope/abstraction:
	//	There is an infinite stock of common goods.
	//	buying spawns item.

	// god hand placement: Drag from inventory and place it into the world.

	//openTTD but you can't just "buy" things out of thin air. you need to buy it on the actual map from some factory or shop that has produced it.

	topDownMinimalisticSimGame: {
		// inspiration:
		//	Mini Motorways
		//	Mini Metro
		//	Anno (wii version)
		//	OpenTTD
		//	Good Goods
		// I like limited responsibility. Aka just being the logistics guy. or just being the road build guy.
		//	It often feels a bit too much when you make the roads and build the cities and you 
		//		keep factories alive. 
		// city builder logistics game factory
		// buildings take spaec like that house building game... wher you build as many houses you can on an island
		// I want logistics to be needed in order to build... aka you can't just plop things you have to deliver them there!.
		// mark priority for next building/destruction like in oxygen not included	
	},

	// ___Logistics and infrastructure____
	realisticMiniMetroGame: {
		// Mini Metro

		//	Realistic landscape elevation
		//		change in elevation increases transport time and cost.
		//		The price of building tracks and trains is based on the terraion of the landscape. (how hard it is to transport resources there and how hard it is to build there)

		// buying permits to be allowed to building roads


		// You can build railroads straight or curved.
		// You take loans and earn money from delivering.
		// It is not "u lose" like normal Mini Metro?
	},

	topDownLogisticsGame: {
		has: {
			topDownPerspective: 1, // Topdown view
		},


		// Style:
		//	You see landscapes, something between a landscape and a abstract map view.
		//	Colorful bright
		//	Landscapes

		// There are towns/stations
		// challenge: Logistics
		// interface: The player draws lines.
	},
	topdownMinimalistCityBuilder: {
		// think like square blocks of areas and buildings connecting them.

		// an urban top down city building simulator that is as clean as Mini Metro
		//	You need to make sure everything flows efficiently
		//	The point is to build an eco friendly low crime
		//	High diversity
		//	Easy travel place
		//	With high technology
		//	And good farming etc.
		// And you begin from like eco village.

		// This picture inspired this: https://twitter.com/sewnbycollette/status/1273367215300100097/photo/1
		// (if this picture does not exist any more, search in Google keep for top down city or something)

		//		game-play is like on the side you have buildings to drag and drop onto the World
		//		.the world is zooming out like in Mini Metro
		//		everything is sorta grid based so you can only make straight roads.
		//		your goal is to create points to go to and lines between them.
	},

	logisticsInsideCityGame: {
		// Logistics game 
		// Top down
		// You're delivering packages between points in the city.
		// You have to take into account city trafic and demands and shit.
		// And it's about having the best vehicles and following laws and stuff.

		// Buying properties and storage space in a city. Selling office space and storage space.
	},
	cityBuilderTowerDefenceGame: {
		//city builder + tower defense.
		//you have to provide resources for your people that then defend
	},

	//___exploration___
	topdownSharkInWaterGame: { //top down shark in water game
		has: {
			topDownPerspective: 1,
		},
		// Smooth swim animation
		// You see waves above you.
		// You swim under boats and stuff.
		// Simplex noise defines islands you can be on.
	},
	topdownCarAndBoatGame: {
		has: {
			topDownPerspective: 1,
		},
		// top down really zoomed out car and boat game
		//	where your care is just like a rectangle
		//	and your boat is just a photogenic shape with a shadow in the water
		//	and it is like super calm, you drive on roads
		//	and take your boat through interesting waterways with landscapes around.
		//	
		//	
		//the point of the game is that it is really calm and relaxing and flowing smoothly
		//	with maybe natural ambiance. you see birds fly by.
		//		you see the wind through flower and grass fields.
		//	
		//	And it is just a good time moving around u know.
	},
	// ^ mulle meck bygger bil/båt

	// ____grid based turn based strategy____
	advanceWars: { // advanceWars warGroove clone
		//The rules of advance wars
		//    Every match has a number of players
		//    Each player take a turn
		//    At the start of every turn
		//        All your units become unmoved
		//        All buildings produce funds
		//        All factories become able to create
		//        
		//    Unmoved units can be moved
		//    A move can result in
		//        Ability
		//            Attack
		//            Capture
		//            Load
		//            Heal
		//            Supply
		//			  Wait
		//    Every unit has
		//		  Actuator
		//			ability type
		//			ability properties
		//        Attack
		//            Attack has an impact type
		//                Explosive
		//                Etc
		//        Defence types
		//        Fuel
		//        Movement cost
		//        Health
		//        Ammo
		//        
		//    The units
		//        Infantry
		//        Mech
		//        Scott
		//        Tank
		//        MD tank
		//        Neo tank
		//        Anti-air
		//        Anti-air missile
		//        
		//        ABC
		//    ^just White these down from the internet
		//1	Ground
		//1.1	Infantry
		//1.2	Mech
		//1.3	Recon
		//1.4	APC
		//1.5	Tank
		//1.6	Medium tank
		//1.7	Neo Tank
		//1.8	Anti Air
		//1.9	Artillery
		//1.10	Rockets
		//1.11	Missiles
		//2	Sea
		//2.1	Battleship
		//2.2	Sub
		//2.3	Cruiser
		//2.4	Lander
		//3	Air
		//3.1	Fighter
		//3.2	Bomber
		//3.3	Battle Copter
		//3.4	Transport Copter
		//4	Charts
		//4.1	Unit Chart
		//4.2	Terrain Movement Chart


		//General purpose Advance Wars-like games framework.
		//    Pitches:
		//        is a new video game with focus on the multiplayer aspect of Advance Wars and war groove.
		//        It supports mobile, pc, mac, windows, linux and any other platform with a browser.
		//        It is made to be universally accessible, more intuitive and easier to get into.
		//    Problems with the advance wars games
		//        It doesn't label what everything is and how things are related for the new players the UI is not intuitive enough
		//        The win and lose conditions are ambiguous as duck
		//        The color of buildings must be clear and stand out in all environments
		//        Colorblind proofness
		//    Customizability
		//        Remixing pre-existing levels
		//        Creating your own rule set
		//            Like the "advance wars rules"
		//            Etc
		//    From playing war groove
		//        "I wish there was more useless stats"
		//            Like how long the the game took
		//            How much each of us earned
		//            Like how we spent our resources.
		//            How long our characters survived
		//            How many units were built.
		//        It's usually goes into endless stalemate.
		//        "I wish you could go back when you miss click"
		//    Maps
		//        Share your maps
		//        Better filters when searching for shared maps.
		//    Convenience
		//        A mode where you can reverse and remake your last turn. But only 3 times per match. (To fix dumb mistakes for noobs)
		//    Maps with their own natural climate
		//        You can see how likely weather's are on the map. A snow lands might be snowy most of the time, but sometimes not.
		//    innovation
		//        Upgrades and research
		//            Like StarCraft
		//    technical
		//        Time line system (backtrack through all events)
		//        "Path finding" all possible places you can go with a dude
		//        making a format to save each move and the move order.
		//            rewatch turn feature. and then a step back in time to see each movement.
		//        go to next unit
		//            cancel middle of movement when going to next 
		//    Hosting ...
		//        Reliable
		//            able to handle
		//                the traffic
		//                the data storage
		//            Secure
		//        cheap
		//    pathfinding
		//    Rendering walkable ground
		//    Fog of war
		//    Replay (playback what happened)
		//        history playback...
		//        For memory.
		//        For sharing experience.
		//            Gifs
		//    Take a local game online...
		//    Your friends who doesn't own the game can still get the game and play it with you, but can't start their own games or create their own maps.
		//    There seems to be a bunch mechanical clones.
		//        Most of them have bad user interfaces.
		//        Only the best stand out

		// suggested features to add:
		// feature for advancewars = agree to go back in time (undo mistakes)
		//	put the match back to an agreed upon state in the past
	},
	cooperativeAdvanceWarsGame: {
		// advance wars but about co-operation in regenerating forests
		//	You are competing in re-generating the forest and supporting each other.
	},
	minimals: {
		// Check the discord... and a few notes on dynalist
	},
	bugWars: {
		//	check prototype discord and dyna
	},

	pointAndClickNatureGame: {
		// Like a 2d side scroller godsim
		// Pet plants by dragging cursor over them.
		// Create smoke from clicking on sand dunes
		// Create fire by repeatedly clicking on wood
		// Planting by click
	},

	kimsSpaceGame: {
		// setting: space ships in space

		// "themes" ?
		// mission adventure
		// space exploration

		exploration: 1,
		cooperation: 1, // in shared spaces. collective care taking
		needsManagement: 1, //& maintenance...
		riskTaking: 1, //risk... management??? I do not know what to call it. high risk. or risk based... considering dangerous alternatives.
		prepping: 1, //Take time and effort to be prepared and organized enough to met chaos.

		// game type
		onlineMultiplayer: 1,
		characterOriented: 1,
		topdown: 1, //or isometric?
		gridbased: 1, //about units of measurement and precision/discreteness of placement

		// each player has their own dedicated space on the ship where they are allowed to put their things.
	},
	eriksGame: {
		// text and curses
		// forbidden words
		// not doing the wrong things
		// trying to make people say and not say certain things
		// _
		// elemental magic, each aspect is an axis and it is about finding the sweet spots of emergence.
		// (there might even be a dimension that always creates a type of emergence out of what you have.)
		// it is a discovery
		// a mad laboratory
	},
	anotherErikGame: {
		//erik vision game
		//	multiplayer
		//		terraria style - do whatever you want but don't expect it to not break if you stress the system.
		//		player hosted. and you can make dedicated servers. (whatever is easiest to implement.
		//	rougelike
		//	survival
		//	big
		//	survival
		//	cool
		//	emergent
		//	(crafting systems
		//catalysm
		//	
		//maybe:
		//	gridbased
		//	also playeable in single player 
	},

	stardewDesertedIslandGame: {
		// But it is in the deserted island.
		// You get washed ashore, and you get to start surviving.
		// You build up a town from nothing.
	},
	HarvestMoonOnIslandsGame: {
		// you have farmlands on islands, you go between the islands
		// Islands with slightly different biomes and natural affordances
		// You can put like animals on islands because they can not like go away from there.
		// You have to make sure to pack transport all the crops to the right areas
		// You have to look at the weather and make sure all plants are fine etc.
		// You need to gather specific things for specific shipments
		// And it is like bigger ships that go every week or so...
		// Maybe you can get like air drop packages of things you order on online or via phone.
	},

	GardenOfExperienceGame: { //or experience as landscape. // it’s like a trope I just invented
		// An open world with various unique areas of unique experience.
		// The player has total freedom to go and focus on the area of experience that they need the most.
		// you know generally what experience you want and you go there, but there is still room for positive surprise and novelty
		//		A game that gives you variance. That not only gives you one aesthetic but many that you can go between when you need it. A non linear game. Not only slash enemies. Not only take care of pets. Not only exploring. Not only skilled based. Not all the time.
	},

	MinecraftCraftingGame: {
		//This shares a lot with abstract crafting and GUI common in strategy games (StarCraft, Heroes, etc)
		dragAndDrop: 1, //and you drag things around.
		//2d: 1,//TODO rename, because you can not start keyword with number...
		GUIBased: 1, //No 3d world, only GUI.
		abstract: 1,
		//	and you see the furnace interface for every furnace you own.
		timerBased: 1,
		timingBased: 1,

		// make a game that is literally the Minecraft crafting system.
		// and maybe you drag like tiny people around. like put the dude in the black smithery....
		// woa did could even be a versus game lol.

		// so you see like boxes of open inventories


		// and like the crafting table
		// etc.
		// and maybe have like hoppers to. 
		//	but then maybe it is like a music dao, like you drag wires between components.
		// this idea works super well with me request ubiquitous game
	},
	myCraftingGame: {
		// influences:
		//	cookie clicker
		// drag-n-drop.
		//	känns enkelt och intuitivt att plonka runt saker
		// this might even be like an idle mobile game.
		//
		//
		// could be incremental game.
		//
		//
		// random ideas:
		//	timer based resource drops of random content (Mulle meck style)
		//	
		//	
		//	
		// ha en massa processererare.
		// man får nya processerare,
		// skaffa bas resurser,
		//
		//
		// it\s a toy, we want to test the mechanics of moving around things.
		// sense pleasure non-challenge grej. a crafting toy.
		//
		//
		//
		// management spel?
		//	managing which resources to use where.
		//	how much and where to put them.
		//	
		//	
		// skorttsamling, svårta att ha utrymme för allt skrot, man måste slänga saker man minst behöver.
		//
		// pipes! - automate transfers with pipes between inventories and processors.
		//
		// Buffers are inventories that stacks incoming resources.



		//Review of potion craft:
		//	I like the simple graphics which immediately tells a story.
		//	^Good minimalism, and the music adds to the setting
		//	good minimal story telling.
		//	the sense pleasure of the crafting interactions!!!
		//		sweeping the soup back and forth
		//		blowing the air and seeing the coal get hotter!!
		//			this is fucking satisfying.
		//		THIS however is done so frequently in the game that you get bored of it in 5 minutes. (just a hassle gimmick immediary step.)
		//		
		//	make a demo that is less than the stuff gamer streamer gets.
		//
		//		the demo is just for the tactiles and to be a hook.
		//
		//	teaching of the game
		//		the efficiency of ingredients, and that mortar creates more concentrated potions.
		//		how perception of your store changes over time and how reputations works.
		//
		//	potion craft quickly gets boring.
		//		feels like 99% busy work. you just make a bunch of the same health potions.
		//			and then based on RNG you get ingrendient you need.
	},

	fastPacedHealthManagementGame: {
		//	adventure game
		//	life-simulation
		//	Basically like 2d side scroller the urbz.
		//	maybe like oxygen not included but you play as one of the dudes.
		//	kinda like harvest moon, but a whole day cycle?
		//		sleeping have more benefits when done in the dark.
		//		and when adequately physically exhausted.
		//	you know a game that models realistic good habits and behaviours.
		//		but with a time limit that makes you iterate quickly
		//		and value time.
	},
	fishFeedingGame: {
		// aquarium
	},
	factoryTowerDefenceGame: {
		// Mindustry but you create factories of robots to send to the other person
		// Like factory tower defences
	},
	combineRulesOnlineMultiplayerGame: { //combine your own rules online multiplayer game
		// A gam like shoot shoot mega pack
		// where the players create the game by combining sets of rules
	},



	towerDefenceGame: { //TODO: look up genre definition of tower defence on wiki, TV Tropes, etc.
		gameGenres: {
			RTS: 1,
			simulation: 1, //sorta city builder....
			resourceManagement: 1,
		},
		usedGameConcepts: {
			treadmill: 1, // (it gets harder but you gets upgrades)
			calmMoment: 1, // moments of calmness.... uh breaks, break from intensity.
		},
		buildings: {
			has: {
				cost: 1, // (in currency/resources)
				buildingTime: 1,
			},
			towers: {
				conventionallyHas: { // TODO: separate into whatever conventionally keywords will be final
					range: 1,
					gun: { // (basically an abstract gun...)
						firingRate: 1, // generic treatment by anything in engine that is a rate? should I use some engine language here?
						ReloadTime: 1, // optional //TODO: use whatever format I use for time
						bulletsAsResource: 1, // optional
					},
					bulletRate: 1,
					bulletDamage: 1,
				},
				conventionallyCan: { // TODO: separate into whatever conventionally keywords will be final
					beSelected: 1,
					beSold: 1, // selling implies recovery of resources
					orBeDeleted: 1, // deleted implies no recovery of resources
				},
				// Towers are unique types with unique strengths
				//			canHave:{ //this is not conventional, this should be like a customizable option for some spice.
				//				health:1,	
				//			}
			},
		},
		GUI: {
			buildSelection: 1, // common in strategy games, very common in RTS
			customCurson: 1, // polish thing, often appreciated unless slow/laggy
			newBuildingPurchaseMenu: { // TODO: separate this into its components... is menu, purpose: purchase new buildings to be placed in the world
				// A GUI shortcut makes it visible (menu button or keyboard shortcut etc)
				// displays each available building 
				//	together with the relevant properties of the corresponding building.
			},
			timeController: { // (play, pause, faster, x times faster)
				//				usually media player symbol conventions.
				//				(common amongst.. simulation games? city builders?)
				//				its purpose is time for the player to be strategic
				//					hard thinking, non urgent decision making.
				//					building phase and simulation phase.
				//					building a thing and then testing it in the real environment.
			},
			currencyDisplay: { //TODO: define GUI display for the engine or whatever
				//				often on the edge of the screen, not covering the area of the map.
				//				if it covers the area of the map, then it switches position when
				//					the covered area is engaged with.
			},
			healthBars: {
				//				a loading bar displaying health/totalHealth
			},
			buildingTimeIndicator: {
				//				a loading GUI showing progress builtTime/totalBuildingTime
				//				usually on top of or beside the corresponding building.
				//				otherwise it is in a list of identifiable buildings
				//					when hovering over the item in the list the corresponding building
				//						on the map lights up. (pc cursor/mouse conventions)
				//				or both
			},
			currentWaveDisplay: 1, //TODO: define GUI display for the engine or whatever
			futureWavesDisplay: 1, // (or other wave information) //TODO: define GUI display for the engine or whatever
			enemyInformation: 1,

			//generic video game optional things
			pauseMenu: 1,
			titleScreen: 1, //?
		},
		currencyOrResources: 1, //TODO: name better... //what I want is the concept of resources as part of resource management games. things in quantities that can be transformed, (according to conventions) into other resources.
		map: {
			enemyPath: 1, //just me getting an insight: enemy when references inside tower defence is implied exclusive behaviour to enemy in the context of towerDefence.
			buildableAreas: 1,
			builtBuildings: 1,
			obstacles: 1, //unbuildable areas
		},
		levelSelection: 1,
		upgrades: {
			upgradeCost: 1, // TODO: use the language of upgrade games and a generic definition of costs used in video games (or I guess any transactional system outside of video games too).
			upgradeTime: 1, // TODO: 
			//towers has their own upgrade trees
		},
		waves: {
			//			when all enemies in a wave is dead, the wave is completed.
			//			sometimes the player has to wait for the wave timer to finish
			//			sometimes the player can skip to the next wave when there is no
		},
		enemies: { // TODO: plural of enemy, this needs to be implied to the engine....
			// different types with different properties to counter
			has: {
				speed: {
					//movementSpeed
				},
				health: 1,
				generalDefence: 1,
			},
			AlternativeConventions: {
				//enemy either dies in contact with core building(s) or stops and shoots. 
			},
		},
		coreBuilding: {
			//			a central(of high importance...) object which your goal is to defend from enemies.
			//			often it has lives or health or both.
		},
		alternativeConventions: {
			//			gridbased vs freeform //TODO: decide on engine format for this
		},
	},



	tropicalSurvivalGame: {
		has: {
			beach: 1,
		},
		// There can be storms, you get warning "if you go in storm you might die" and then your ship breaks and you wake up ashore on deserted island or in a hospital. 
		// Then the player has to figure out how to survive on the island and build a raft and shit.
	},
}
