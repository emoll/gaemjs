// world building - common consistencies of planetary worlds
// worldly
const humanWorld = {

	world: {
		//A world is a container of consistent occurences.
		// What defines a world is its state and habits

		//metaphysics, physical laws and current state.

		conventionally: {
			has: {
				//space or spatial dimensions
			}
		}

		// world = all that exists and can be encountered in the boundary of the world
		//
		// world is abstract, as in you can't obsereve the whole world/universe in one instance.
		//
		// what you experience is a path of perspectile experience through a world.
		//
		//
		//
		// narrative/thematic focus (for lack of better word) - the focus in the world that present within a scope
		//	the whole game
		//	one play session

	},

	// Earth nature

	planetaryConsistency: {
		ground: 1,
		sky: 1,
		// dayNightCycle

	},
	earthlyConsistency: {
		// planetaryConsistency
		// things you basically always want in an earthly world....
		// sun
		// moon //one moon.
		// mountains
		// weather
		// temperature
		// humidity
		// wind
		//	in the world there is a sun and a moon.
	},

	NaturalLandscape: {
		is: {
			//a landscape	
		}
		//		Composed of nature's elements
		//			Lakes
		//			Rivers
		//			Rocks
		//			Mountains
		//			cliffs! // cliffs that can go down into water which you can jump from.
		//			Dirt
		//			Plant
		//			Tree
		//			Bushes
		//			Savannah's
		//			Desserts
	},

	// earth-like world/planet

	// Sky & air things
	sun: {
		is: {
			round: 1, //understatement... it is most often essentially completely circular.
			in: {
				sky: 1
			}
			// sunlight affects temperature...
			// The sun 
			//	the sun can be seen during day.
			// The sun can either be behind the camera, above the camera or behind the landscape.
		}
	},
	lightBeam: { // AKA directional light.
		// Is a ray the visible part of a beam? 
		// A beam is a directional light
		// You can see a beam if it a half translucent medium:
		//	fog, smoke, clouds, dust, foliage, meshes, anything that has light reflective surfaces but a lot of translucent area between.
		// Is only visible if part of the light is scattered by objects: tiny particles like dust, water droplets (mist, fog, rain), hail, snow, or smoke, or larger objects such as birds
		// A light beam or beam of light 
		// Is a directional projection of light energy radiating from a light source.
		// Sunlight
		// Forms a light beam (a sunbeam) when filtered through media such as clouds, foliage, or windows.
		// To artificially produce a light beam, a lamp and a parabolic reflector is used in many lighting devices such as spotlights, car headlights, PAR Cans, and LED housings.
		// Light from certain types of laser has the smallest possible beam divergence.
	},
	godray: {
		is: {
			lightBeam: 1,
		},
		// Called sunbeam when from the sun
		// When the sun is partially obstructed, I want the engine to produce god rays with a shader. like in Spiritfarer.

	},
	sky: {
		// Seen in the background
		// The place where clouds and stars are seen
		// Far away or static in parallax?
		// Untouchable
	},
	ground: {},
	mountains: 1,

	weather: {
		// Based on temperature
		// Constantly changing slowly over time

		// Historically and symbolically something we humans have no control over.

		// TODO: add some general notion of things that are out of control of the player are good ways to control/guide the player.
		//	Something like good excuses or good circumstances or whatever.
		// Weather is out of control of the player, so it is a good way to pace the player on their journey.
		//	Because different weathers are symbolic of different things and changes actual mechanics
		//	Coldness, wetness, windness, 
	},
	rain: {
		// Cloud that are to dense to hold water, making the water drip down from the sky.
		// Rainy is an aesthetic/style often associated with sadness, (like crying)
		//	And associated with drawing back and staying inside.
		//	If you have shelter rain can be cozy.
		//	It creates puddles which are reflective.
		//	And it rejuvenates the streams of water and gives plants new life to grow.
	},
	rainbow: {
		// A spectrum of colors formed in a bow
		// Difracted light...

		// TODO:
		// Play around with rainbow stars light.
		// Rainbow coloured electricity as well.
	},
	temperature: {
		// Way to generate temperature
		// Gaussian Noise between lowestPossibleTemperature and highestPossibleTemperature
		// Scale the Gaussian with a reasonable phase in weather change

		// Temperature is an aspect that belongs only to itself, differing over space
		// Temperature is in a way just the space-field and in a way just the entities. Like it belongs to both the world and the entities and what not.
	},
	humidity: {},
	wind: {
		// Wind creates sound when interacting with things
		// Leaf sound, grass, trees, pipes(resonating flute sounds lol), sand,
		// Intensity direction
		// Can probably be based on a noise map or something

		// The wind makes a sound that is basically white noise with a ... lowpass filter??? that fades in and out.
		// And maybe has a whistling sorta tone to it?

		// Generally the wind is stronger the further up you are
		// Wind is stopped or halted by stable thick objects, like walls and rocks etc.

		// Cloth waving in the wind

		// Wind can be strong enough to push light things.

		// Extreme wind can be strong enough to push big things.
		// Storms create extreme wind strengths

		// Winds effects/moves light things
		// (Smoke is light, sand is light)


		// Light things are generally bendy/springy, springy things sway back. bendyness has a characteristic of producing a innertia force  that increases in strength as the bend is dislocated...
		// Light things that are stuck sway (sway in the wind)
	},





	cloud: {
		// Gas form
		// Humid
		// Raining happens from clouds
		// Fluffy
		// Draw with Perlin Noise
		// Similar to smoke
		// Could possibly be draw as a bunch clumped together circle shapes.
		// It is like mist but far up in the air and very big.
	},
	smoke: {
		is: {
			// Tiny dust particles caught in air flow... like sand but much smaller and thus lighter.
			// Like cloud but dry
			// More light and swirly

		},

		// Ascends from ash in a fire. or just as rest product of fire.... or ember...
		// Less heavy than dust, but is essentially fine dust
		// Has a thickness which dictates transparency
		// Usually white or light gray

		// tends to be white, gray or black.
		// Can be essentially any color...
	},
	fog: {},
	foggy: {},
	mist: {},
	condensation: {
		// Air can not hold as much water in coldness.
		// (So if you wanna collect water from the air
		//	Just get a really cool object in a hot environment
		//		The water will form on its surface and drip of it)
		// This process is called condensation.
	},
	moon: {
		shapesByMoonCycle: {
			//	circle
			//	half of a circle
			// månskära (can be drawn by subtracting a circle from a circle)
		},
		// generally white, bright
		// has some "rocky" texture.
	},

	seasons: {
		spring: 1,
		summer: 1,
		fall: 1,
		winter: 1,
	},
	dayCycle: {
		// A day cycle is always 24 hours on Earth
		// During day the sun is above the horizon.
		// During night the sun is below the horizon.
		// The day is generally 12 hours
		// The night is generally 12 hours
	},
	nighttime: {
		//		Sky is black with stars
		// It is darker
		// It is colder
		// The moon shines
		// The moon is more visible.
	},

	weather: {
		// Weather gradually shifts between archetypal weathers. (sunny, cloudy, etc)
	},
	uniqueWeathers: {
		sunny: 1,
		cloudy: 1,
		misty: 1,
		foggy: 1,
		rainy: 1,
		windy: 1,
		storm: 1,
		snowy: 1,
		thunder: 1,
	},

	climate: 1, // Natural averaging temperature etc.
	biome: 1, // Not so important, can be emergent.

	// Forests and nature
	forest: {
		// a concentration of plants.
		// I guess forestness can be a scale in landscape.
		// forests have a composition of plants/species
	},
	nature: {
		// ???
		// the phenomena of the physical world collectively, including plants, animals, the landscape, and other features and products of the earth, as opposed to humans or human creations.
	},

	water: {
		is: {
			liquid: 1,
			transluscent: 1,
			reflective: 1,
		}
	},
	whitewater: {
		// The white stuff on waves that is sorta the foaming of the wave.
		// Has a unique look to it!
	},
	bubble: {
		is: {
			transluscent: 1,
			reflective: 1,
			// Spherical?? simplex noise stuff!
		}
		// Gas contained in liquid 
		// Gas encapsulated in a film of liquid
		// Often so light as to float around.
		// Bubbles floating up because they are lighter than the surrounding material
	},

	// Water should soak cloth, making them more heavy, dangly and drippy!




	bodyOfWater: 1,
	waterfall: 1,
	pond: 1,
	// A body of water creates an image reflection
	// Actually all water reflects image but it is only seen on dark water.




	dirtyVsClean: {
		//cleanliness synonyms:
		//	purity
		//	pureness
		//	clarity
		//	simplicity
		//	non-dirty

		//	dirty = covered or marked with an unclean substance.
		//	clean = free from dirt, marks, or stains.
	},



	// Landscape things:
	beach: {
		// Random shit can wash ashore
	},
	ocean: {

	},
	lake: {

	},
	river: {

	},

	// holes
	cave: {
		// a very big hole with an interior the average characters fit to walk inside.
	},
	undergroundPassage: {

	},
	tunnel: {
		//an artificial underground passage, especially one built through a hill or under a building, road, or river.
	}



	// substance/material
	sand: {
		//			Sand glitters
		// Like a form of dust. (bit thicker than dust)
	},
	rock: {
		// I suppose rock is both a material and a form of object mostly consisting of that material
		// 	Generate rocks with simplex noise
		// 2d simple noise can be used to generate distinct artifacts by using a discrete value to eliminate all values below the tops
		// Each top can then be isolated into its own texture or sprite.
		// The problem is making an algorithm that can discern the size of one of these and put it on its own appropriately sized texture.
		// Another problem is creating hit boxes for these artifacts.

		// TODO:
		// Define:
		// rock types
		// rock compositions.
	},

	crystal: {
		is: {
			translucent: 1,
			solid: 1,
			material: 1, //not sure if needed
		}
		// crystals can diffract light like under water
		// in fact any translucent medium can diffract light!
	},
	glass: {
		//made from melted sand
		is: {
			crystal: 1,
		}
	},
}
