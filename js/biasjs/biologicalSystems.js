// LIFEFORMS AND BIOLOGY
// Biology of beings and plants etc
// TODO make a good way to consider depth of simulation.... in 99% of cases genes and offspring might be irrelevant.


// LIFEFORMS AND BIOLOGY
// 55. Biology of beings 
const biology = {
	biologicalSystem: {
		has: {
			genes: 1,
			genetics: 1,
		},
		does: {
			offspring: {
				// TODO: defines.
			},
			homeostasis: {
				is: {
					process: 1,
				}
				// creatures generally require homeostasis. To maintain internal organs by eating, breathing, moving, sleeping. regulating temperatures etc.
				// the underlying processes of living. (the things that makes the living go.)
			}
		},
		// tends to be found alive, can be found dead.
	},


	// biological needs
	homeostasis: {
		is: {
			need: 1,
			physiological: 1,
		}
	},

	// biological systems behave in order to maintain or fulfil their needs.


	humanPhysicalCondition: {
		tiredness: {
			// increasing the need to rest and relax
		},
		exhaustion: {

		},
	},


	//All life
	// multicellular +
	// Reptile +
	// mamal +
	// etc


	// 2 sexed
	sex: {
		maturity: 1, // (non - developed - over developed)
		maleMaturation: 1, // (female - male)
		femaleMaturation: 1,
	},
	//There's like sex maturation and like body maturation (developmental stages or whatever.)


	dead: { // A biological system that no longer does not homeostasis
		was: {
			does: {
				homeostasis: 1,
			}
		},
		is: {
			not: {
				does: {
					homeostasis: 1,
				}
			}
		}
	},

	// creature
	animal: {
		// animal - a living organism that feeds on organic matter, typically having specialized sense organs and nervous system and able to respond rapidly to stimuli.
		is: {
			creature: 1,
		},
		can: {
			eat: 1,
			move: 1,
			pee: 1,
			poop: 1,
		}
	},


	plant: {
		is: {
			biologicalSystem: 1,
		}
	},

	//Plant related things
	photosynthesis: {
		// the process by which plants capture the energy of sunlight and use carbon dioxide from the air to make their own food.
		is: {
			typeOf: {
				homeostasis: 1,
			}
		},

		reaction: { //or effect or "does"
			consumes: {
				soilNutrition: 1,
				moisture: 1,
			},
			produces: {
				//oxygen and shit?
			}
		}
		// photosynthesis:
		//	condition {
		//		is(plant.touch.soil) &&
		//			is(plant.touch.sunlight) &&
		//			is(plant.contains.moisture)
		//	},
		// }
	},
	// Plant dream
	//		Plant growth is controlled by growth factor
	//		Plants should be able to accumulate certain elements or nutrition.
	//		Plants only grow under certain conditions:
	//			heat
	//			wind
	//			light
	//			air moisture
	//			soil moisture.
	//		Create a plant life cycle
	//		Plants! #Should #Code 
	//			Stem
	//				single width
	//				dynamic width
	//			stem on stem
	plant: {
		//plants wilt if they do not photosynthesize in a long time//without water and nutrition and sunshine

		//		the essentials of a plant
		//			it is essentially rules for a tree structure put on rules for a line drawing.
		//
		//			how it branches
		//			how it stems
		//			what its colors are
		//
		//			Where is it in its life cycle
		//			How quickly it grows
		//			What is its condition
		//				what turmoil has it been through
		//
		//
		//
		//		if it has leafs and how its leafs are
		//		if it has flowers and which
		//		if it has fruits and which
		//
		//		"its like DNA" - Erik
		//
		//		I want plants to adhere to sets of rules that generally look good together.
		types: {
			count: "5 or more",
			unique: {
				shape: 1,
				GrowthCriteria: 1
			}
		},
		is: {
			being: 1,
			organic: 1,
			multicellular: {},
			usually: {
				immobile: {},
				green: 1,
				foundIn: {
					nature: {},
					"various habitats": {
						"fresh water": {},
						seawater: {},
						"tropical regions": {},
						desert: {},
						etc: {},
					},
				},
			},
			// drawn in order of tree,
			// from root node outwards.
		},
		can: {
			// grow through multiple means
			scaling: {},
			drawing: {},
		},
		has: {
			limbs: {
				atLeast: 1
			},
			//		cuticle, meaning they have a waxy layer on their surface that protects them and keeps them from drying out.
			//		They have eukaryotic cells with rigid cell walls.
			//		They reproduce with spores or with sex cells.
			// encoded: {},
			// maxSize: {},
			// growth process / function
			// plant growth involves deciding direction,
			// path,
			// where new segments should start growing etc.
			// an encoded branching pattern / ruleset
			// For example: branches much when small,
			// but less when long.
			// gets a maximum of 5 branches from a x type segment.
			// physicality
			// light plants bend when player steps over them(actually something something physical objects are pushed by movement of physical objects lol)
		},
		consistsOf: {
			//plant segments that are connected usually in a node tree structure
		},
		can: {
			//generate nourishment through photosynthesis.
			// grow with nourishment
			// die from extreme conditions in moisture, heat
		},
		does: {
			// performs photosynthesis in the right circumstances
			// Plants die under extreme conditions in
			// Moisture
			// Heat
		},
		// the algorithm should bias splitting at segment connection points
	},
	segment: {
		has: {
			sprite: {
				// has a width and a height
				// has a contextual position(global or parent relative)
				// has a internal origin point
				// segment has defined spatial points on which other segments grows out of with their origin.
			},
			// points on which they are attached to other segments
		},
	},
	plantSegments: {
		can: {
			belong: { //belong to one or multiple identifiable types: {
				//any plant part lol
			},
		},
		has: {
			tipPoint: {},
		},
		// The branch is drawn between the root and tip of the plant segment...
		// When a plant segment grows
		// The tip gets a new direction,
		// The tip grows away from it 's previous position.
		// A new tip is drawn over or under / behind the sprite
	},
	// Rendering and drawing methods
	//	there are various rendering methods in general
	//	rounding positions
	//	using one sprite
	//	for everything
	//	this.sprite.context.globalCompositeOperation = 'destination-over' //New shapes are drawn behind the existing canvas content.
	plantInformation: { //realistic plant model?
		// plant information https://www.youtube.com/watch?v=gJrOATCtV-k
		//
		// plant cells has a cell wall made out of carbohydrate cellulose, which stores starch which the plant can use as food.
		//
		// the cell wall gives frame work and mechanical strength to plant cells.
		//
		// most plant cells contains a green colored pigment - chlorophyll. 
		//
		// chlorophyll is present in the chloroplast which enables it to be photosynthetic.
		//
		//
		// some few plants are heterotrophic,
		// some live in symbiotic relationship with bacteria
		// some are insectovorious (eats and digests nutrients from insects)
		// some are parasitic
		//	they live on other plants and consume other plants nutrients.
		//
		//
		// some plants have sexual mode of reproduction
		// some plants have asexual mode of reproduction (cloning)
		//
		//
		//
		// plant life-cycle has two alternating phases
		//	haploid - gametophyte: produces gemets
		//	diploid - sporophyte: produces spores
		//
		// kingdom plantae is mainly divided into  five phyla
		//	bryophyta - green plants. mostly restricted to damp environments. no true roots, anchored rhizoids
		//		examples: mosses, liverwort
		//	lycopodophyta - extremely small spirally arranged leaves
		//		Example: club moss
		//		
		//	philisinophyta - leaves are tightly coiled and uncoiled in their early growth and development.
		//		on the lower surface of their leafs are clusters(dots) called sori.
		//		sori contains the sporangia required for reproduction.
		//		Example: ferns
		//	coniferophyta - are cone-bearing (kottar) plants with both male and female cones often on the same plant.
		//		leaves(leafs) are usually waxy and needle shaped.
		//		example: pine tree, cycus, pineaple? 
		//	angiospermophyta - dominant group of land plants, seed bearing plants, seeds are enclosed in a fruit.
		//		fruits are fromed from the ovary. Only phyla with flowers.
		//		flowers exist for complex mechanisms for pollen transfer and seed dispersal.
		//		there are two groups of angiospermophyta:
		//			monocots have only one seed leaf inside the seed coat(skin of the seed)
		//				monocots often have: leafs that are long and narrow with veins in straight lines (parallel venation).
		//				monocot stems have vascular bundles scattered all around. scylem and phloem are arranged together in rings.
		//				Monocots always have flower leafs in multiples of three
		//				monocots roots are fibrous
		//			dicots have two seed leaves inside the seed coat. 
		//				dicots comes in many different shapes and sizes. their veins go from the central midrib, outwards to the edge of the leaf.
		//					crossing and foing to create a net pattern over the leaf which is called reticulate venation.
		//				dicots stems separates xylem and phloem  with distinct rings. xylem at the stem core, and phloem on the external.
		//				dicots always have flower leafs in multiples of 4 or 5.
		//				dicots roots are tap roots (grows vertically straight down, one big stem in the middle)
	},

	// parts of plants
	stem: {
		is: {
			segment: 1,
			not: {
				branch: 1,
			}
		}
	},
	root: {
		// the part that
		// holds the plant
		//		stuck in the ground
		// absorbs nutrition from the soil.
	},
	fruit: {
		// Fruit typically refer to a ripe, human edible, fruit that has been harvested.
		// TODO: fruits grow on plants. Typically trees
		// I like the idea of pulling fruits from a tree, like if the player has to pull on it until the twig snaps and then the tree ricochets back.
		//// Pick fruits from trees
		is: {
			from: {
				plant: 1
			},
			organic: 1,
		},
		has: {
			flesh: {
				typically: {
					is: {
						edible: 1, // aka healthy to eat....
					}
				}
			},
			seeds: 1,
		}
	},
	seed: {
		has: {
			maturity: {}
		}
	},
	leaf: {
		// Plant leafs dies by drying out.
		// When drying out the leafs turn stale and brown.
		// When too cold leafs slowly turn yellow then orange then red dry out.
		// Stale leafs crumble when compressed.
		// Dead leafs fall plants.
		is: {
			flat: 1,
			thin: 1,
		}
	},

	flower: {},
	petal: {
		is: {
			leaf: 1,
			on: { // Attached to, part of
				flower: 1
			}
		}
	},

	// Types of plants
	tree: {
		// Something can be cut down with the right tools, maybe apply this rule to limb instead and to tools/physics impacts.
		// Something is branching. but this is true to more plants too. I guess the specifics how branching applies uniquely to trees.
		is: {
			plant: 1,
			generally: {
				tall: 1,
				big: 1,
			}
		},
		has: {
			wood: 1,
			leafs: 1,
			branches: 1,

		},
		can: {
			has: {
				fruit: 1
			}
		},
		"scale gradually with age": {
			area: "small - > big",
			material: "plant - > wood"
		}
	},
	bush: {

	},

	grass: {
		is: {
			plant: 1,
		},
		// grass can mean multiple things... there is a collection of 
		// species of grasslands, vs species of lawns...

		// often long strands

		// grass bending ideas
		// Grass bending
		//	circle around player, if grass point collide circle, if grass.x < player.x then go left else go right.
		//	if player grass distance < "pretty darn close" then bend = 100%
		//	if player grass distance between pretty darn close and outerCircle
		//	100% bend is most likely .5*Math.pi
		//	Distances.prettyDarnClose = 10
		//	if bent more than 90% then only check for unbending (Do not flip)
		//	try moveTowards smoothing if it looks to rough
	},

	//cultivated crops???

	breathing: 1,
	digestion: 1,


	//when does gender apply? It is not all biological systems. but you can basically assue genderedness....


	creature: {
		is: {
			being: 1
		},

		// monsters might be the exception to requiring homeostasis
		// ^the point is that creatures should freeze when to cold, eat when to hungry, etc.
		has: {
			//properties
			//			tallness:1, //height, actually size is more important, because then a skinny big person is tall because people are generally big vertically
			size: 1,
			conventionalityOfProportions: 1,
			fatness: 1,
			rippedness: 1, //muscularity
			age: 1,
			genderness: 1, //properties dictated by gender/ hormones
			geneticsOrSpecies: 1,
			vitalityOrHealth: 1,
		},
	},
	humanoid: {
		// Something that generally follows the physical characteristics of a human body
		is: {
			creature: 1,
			//			type: "race",
			//			alive: .9 //percent chance
		},
		has: {
			body: {
				count: 1
			},
			head: {
				count: 1
			},
			leg: {
				count: 2
			},
			arm: {
				count: 2
			},
			mood: 1,
			//has arm foreach shoulder
		},
		//does[reaction] when[condition],
		posture: "vertical" //generally big vertically
	},
	humanoidProportions: {

	},

	age: { // Biological age...

	},


	gender: {
		//	add "genderness" or something
		//		females gets less hairy on body
		//		men get lower toned voice.
		//		men gets facial hair..
		//
		//		males and females gets different tendencies..
		//			like risk taking for men and what not.
	},
	biologicalGender: { // born gender. The gender you are born with and will develop along unless hormonal interventions.
		// Either male or female
		// determines sexual organ. (which is probably not relevant in 99% of all games)
		// and wether following a male hormonal development or female.
	},
	maleHormonalDevelopment: {
		//Things that can increase with male hormones

		// Physically:
		//	bigger bodies
		//	more muscles (without exercising)
		//	facial hair
		//	adams apple
		//	more square eye sockets.
		//	body hair

		// slightly shorter lifespan ???

		// Eventually baldness????

		// darker tone of voice

		// Behaviourally:
		// less risk aversion.
		// More aggrettion, go get attitude.
		// more competetive?

	},
	femaleHormonalDevelopment: {
		// Physically:
		// wider hips
		// boob size
		// thicker thighs
		// lower center of gravity??
	},


	healthCondition: {
		nutritionalCondition: 1,
		infectiousCondition: {
			ActivityOfImmuneSystem: 1,
			viruses: 1,
			bacteria: 1,
			poisonsOrChemicals: 1,
			allergicalCompounds: 1,
		},
		sickness: 1,
		allergies: 1,
		temporaryWounds: 1,
		scarsAndPermanentWounds: 1,
	},



	genes: {
		// TODO: defines
		does: {
			// genes control the properties of the growing plant
			mutation: 1,
		},
	},
	genetics: { // the study of genes?
		//Function to produce structures that grow over time based on a dataset of genetic compound and environmental factors

		// genetics
		// The genetics are easily understood and editable in code view.

		mutation: {
			// There should be functionality to mutate genes randomly, with an intensity parameter.
			// There should also be functionality to mutate specific parts, aka freeze/lock mutation in certain fields.
		}
		// Naturally produce off spring
		// The genetic data saves and transferred easily in order spread between worlds and simulations.
		// Plants are easily saved in order to make it easy to quit and resume a game.
		// Plants are easily transferred over the internet in order to enable multiplayer and sharing.
	},

	//genetical determinants
	// eye color
	// hair color
	// skin color
	// height
	// body composition (bone, muscle, fat)


	// features comes evenly from each parent.
	//	some features are directly copied, binary.
	//	some features can be right in-between.
	//^ something like: some features are naturally more experimental.


	// Physical attributes and capacity

	// ALL thee are colorTheory?
	colorPerception: {
		// add to color (and human perception of color)
		// more detail/texture = more noise.
		//	High vibrancy makes all colors scream for attention and makes an image more noisy. (aka if colors are intense, they are all highlighted and there is more work to discern hierarchy of importance. )
		//	Meanwhile if we lower the saturation of the colors all the colors get along better.
		//	This in turn means that if we add a few colors with higher saturation they still fit in with the rest of the colors, but stand out more. 
		//	So it is not an absence of color, but color harmony.
		// AKA use saturation for highlighting.

		// fog is a good way to naturally lower the saturation and texture detail of objects.
		// create silhouettes with strong foreground background difference in color.

		// unique colors (with gap in color space) create visual distinction through contrasts.
		// use color to categorize. backgroundVsForeground, interactableVsNon-interactable.
	},
	strength: 1,
	stimulability: 1, //sensitivity.



	humanoidAnimation: {
		// I want a procedurally generated skeleton with
		// walk cycle,
		// run cycle,
		// idling and poses,
		// swim cycles,
		// attack moves,
	},
	//	seedling character editor focus
	//	focused effort to make a character editor
	//	that lets you specify human characteristics
	//	hair
	//	body dimensions
	//	what body types do I really care about?
	//		spaghetti person
	//		muscular boy/man
	//		woman proportions
	bodyDimensions: {
		waistTohip: 1, //uh?
		hipWidth: 1,
		ShoulderWidth: 1,
		shoulderHeight: 1, //shoulder range??
		necklength: 1,
		boobers: 1,
		//arm and leg proportions:1,
		feet: {
			width: 1,
			length: 1,
			toes: 1,
		}
		// whateverPartsss:1,
	},
	hair: {
		// if on creature grows over time
	},

	// physical things has temperature.

	// body parts
	limb: {
		// defined by being ofbranching attached to body
		// there are essentially two types of limbs, one with b
		// Branching
		//	I need to think about how based on my previous notes about scaling and other things
		//	make concrete documentation of how growth works and should work
	},
	head: {
		is: {
			limb: 1
		},
		has: {
			face: 1,
		},
		//usually contains 2 eyes, 2 ears, 1 nose, hair on top, 
		draw: function () {
			DrawCircle(defaultLayer, x, headY, headR, headColor, 1) // replace with draw geometry and style etc.
		},
	},
	face: {
		eye: 2,
		nose: 1,
		mouth: 1,
		sections: {
			// forehead - above eyes
			// chin - below mouth
			// cheek - left and right of mouth
		}
	},
	eye: {
		is: {
			//open or closed
		},
		has: {
			EyeBall: 1,
			conventionally: {
				eyeLid: 1,
			}
		},
		does: {
			see: {
				when: {
					is: {
						open: 1,
					}
				}
			}
		}
		// emulated vision
		// if you close your eyes you won't see the world around you, but you still see your character in black and white
		// This is to emulate the ability to sense your body.
	},
	eyeBall: {
		has: {
			Pupil: 1,
			Iris: 1,
			EyeWhite: 1,
			Vein: {
				count: "0 or more"
			},
		},
	},
	pupil: {
		does: {
			Dilates: {
				when: {
					// in darkness
					// Or in stimulation
					// or Seeing something valuable
					// or Consuming a stimulant drug
				}
			}
		}
	},
	nose: {
		// is or has been on face
	},
	mouth: {},
	neck: {
		// attacked to head and torso
	},
	leg: {
		is: {
			limb: 1
			// legs are limbs that are generally used to for standing on.
			// "each of the limbs on which a person or animal walks and stands."
		},
		has: { // to 
			point: "hip",
			point: "end"
		},
		draw: function () {}
	},
	arm: {
		is: {
			limb: 1
		},
		can: {
			slap: 1, // emergent phenomena of swinging arm with a hand
			punch: 1,
			tackle: 1, // tackle is the forceful movement of body with elbow pointing in direction to impact something...
			swing: 1,
			// grab:1, <- hand
			// hold:1, <- hand

		}
	},
	hand: {
		is: {
			limb: 1
		},
		has: {
			fingers: 5,
		},
		can: {
			open: 1,
			clench: 1, // close:1,
			hold: 1, // hold items
		}
	},
	finger: {
		conventionally: {
			has: {
				segment: 3, // has 3 segments
				nail: {
					on: {
						end: 1,
					}
				} // has a nail on the end of the finger....
			}
		},
		can: {
			trigger: 1, // pull triggers like guns etc.
			// push buttons?
		}
	},
	toe: {
		// very short finger... lol
	},
	nail: {
		// same as horn but on finger
	},
	horn: {
		// hard material that grows out of organic beings.
	},
	body: {
		has: {
			temperature: 1,
			skin: 1,
		},
		draw: function () {
			// Draw body
			//        ctx.moveTo(x - .5 * hw, y)

			//                ctx.beginPath()
			//                ctx.lineTo(x - .5 * hw, y)
			//                ctx.lineTo(x - .5 * sw, y - bh)
			//                ctx.lineTo(x + .5 * sw, y - bh)
			//                ctx.lineTo(x + .5 * hw, y)
			//                ctx.lineTo(x - .5 * hw, y)
			//                ctx.strokeStyle = "black"
			//                ctx.lineWidth = 3
			//                ctx.fillStyle = color
			//                ctx.stroke()
			//                ctx.fill()

			//        DrawRect(x - .5 * width, y, width, -height, color)
			//        StrokeRect(x - width / 2, y, width, -height, "black", 2)
			//        context.strokeStyle = color;
			//        DrawCircle(defaultLayer, o.x + x, o.y + y - width / 2, 15, color, 1)

			//Draw black body
			ctx.beginPath()
			ctx.lineTo(4 + x - .5 * hw, y + 5)
			ctx.lineTo(x - .5 * sw, y - bh)
			ctx.lineTo(x + .5 * sw, y - bh)
			ctx.lineTo(4 + x + .5 * hw, y + 5)
			ctx.lineTo(4 + x - .5 * hw, y + 5)

			ctx.lineTo(x, y - bh)
			ctx.lineTo(x + .5 * hw, y + 10)
			ctx.lineTo(6 + x - .5 * hw, y + 10)
			ctx.lineTo(x, y - bh)

			//                ctx.strokeStyle = "black"
			ctx.lineWidth = 1
			ctx.fillStyle = "#444444"
			//                ctx.stroke()
			ctx.fill()
		}
	},
	skin: {},
	skinConditions: {
		vitigo: 1, // is cool
		wounds: 1, //?
		scars: 1, // are cool
		freckles: 1, // are cool

		sunburn: 1, // sun exposure
		drySkin: 1, // moisture and oilyness?
	},

	hair: {
		//hairdo or hairstyle
		// hair length
		// hair style
		Draw: function () {
			//        headR += 2 //because we do not want edges of head to see through hair
			//        ctx.beginPath()
			//        ctx.lineTo(x - headR, headY + headR)
			//        ctx.lineTo(x, headY - .5 * headR)
			//        ctx.lineTo(x + headR, headY + headR)
			//        ctx.lineTo(x + headR, headY - headR)
			//        ctx.lineTo(x - headR, headY - headR)
			//
			//        ctx.lineTo(x - headR, headY)
			//        ctx.fillStyle = "#992233"
			//
			//        ctx.fill()  
		}
	},

	flesh: {},
	blood: {
		is: {
			red: 1,
			liquid: 1
			//"blends" with water... not sure what the term is, when water... dissipates??? into another liquid.
		},
		contains: {
			nutrition: 1, //this very vague... and maybe true to all bodies?
		},
		//associated with damage and death.
	},
	bone: {},
	skeletonConcept: { //?
		// A skeleton making algorithm just like a tree
		// Each node just like snake but a branching rib bone like a centipede
		//	Each bone is slightly unique and its size depends where on the structure it is in.
	},

	pose: {
		//everything with movable/directable limbs can pose.
	},
	humanoidPoses: {},
}
