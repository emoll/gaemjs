let metaPhysics = {
	// time related concepts
	temporality: {
		// time scales: worldtime synchronous
		// speed up
		// time controlled? (play, speed up, etc)
		// continuous vs discrete
		// realtime vs turn/action-based
		// player controlled
	},
	timeModels: { // that can be used in games lol??? //temporalities?
		realtime: 1,
		worldtime: 1,
		discreteTime: 1, //? lke board games turn based?
	},
	time: { //time, order, sequence
		// I need like a "time model".
		// how do I update and step through time?
		//	time constraints
		//	I am thinking about space and positional constraints
		//	but I have not thought about time constraints.
		// a physics simulated game physics should update as fast and often
		//	"physics must update every 10ms" //??????
		//	
		//	A game should be animated. //things should move on the screen
		//	A game should be highly responsive //inputs should be taken into account by game.

		//	timing, lasts for around 5 minutes
		// absolute order
		//	first
		//	second //replace with xTo
		//	middle?
		//	last
		//	xTo^ //<- a pattern to get a thing relative to absolute order. like 5ToLast
		// Relative time order
		//		before, precedes
		//		after
		//		during
		//
		//	time section
		//		beginning
		//		middle
		//		peak
		//		end
		//
		//	game phases
		//		tutorial
		//		early
		//		mid
		//		endgame
		//
		//	time constrained units
		//		chapter
		//		room/level
		//
		//	
		//	speed language
		//		suddenly
		//		slowly
		//	or pacing

		//real World time
		//	today
		//	CurrentSession
		//	StartOfSession
		//	EndOfSession
		//	session.start
		//	session.end
	},


	// Timing, speed and pacing
	duration: {
		// the temporal content between a beginning event and an ending event 
	},
	speed: {
		// change per unit of time.
	},

	acceleration: {

	}
	velocity: {

	},

	fast: {
		// relatively higher speed
	},
	slow: {
		// relatively lower speed
	},
	pace: {},


	//Anything that develops over time has these Identifiable temporal stages.
	startingCondition: {
		// the condition at the moment something has been instantiated/manifested
	},
	progression: 1, //development?

	timeSkip: {
		// change to a simulated time instantly
		// can be represented a a enumerated value of difference in time.
		// example: 5 minutes. resulting in instantly loading the world in the same location 5 minutes from present time.

	},


}
