// learning.js
var learning = {
	// and the limits of communication.
	// Human developmental psychology and teaching


	// A theme is the focus of the learning session - binding all concepts to single point

	// Skillful techniques considered jobs by humans:
	// Teaching, training, etc



	teaching: { //todo merge with learning? (they are two sides of the same coin) //teaching = optimize experience for learning.

		// teaching is aided learning

		// You can only teach what has been accurately modeled.
		// You should only teach what the player finds valuable learning.

		// Teaching is the assembly of virtuous person. (virtuous agency) //the transformation of human agents towards being virtuous.
		// Empowering people and working together we can find what is more virtuous.

		// You can only teach by providing experiences. // You can only learn from experience. //humans learn exclusively through experience.
		// Experiences can be abstract or concrete.


		//How and what the perfect educator teaches
		//	make education - precision, beautiful, compelling, deep, rich, accurate
		//		have the notion of what is being served firmly in the backgrounds of the attempts to make the material.
		//		
		//		love, encouragement and truth.
		//		what would you teach them if you truly love them and you wanted to encourage them and you wanted to tell them the truth?
		//	
		//	
		//
		//good thing about YouTube is that the people that watch are the ones that are interested
		//
		//
		//
		//teach about that which you love. Because someone else might fall in love too.
	},
	Pedagogy: { // Pedagogy model (learning/teaching/tutorial)
	},
	tutorial: {
		// "a period of tuition given by a university or college tutor to an individual or very small group."
		// "a method of transferring knowledge and may be used as a part of a learning process. More interactive and specific than a book or a lecture, a tutorial seeks to teach by example and supply the information to complete a certain task."
		// ... a restricted context with restricted agency where the player learns a specific rule or aspect of the game.
		// teaching the player.
		// making the player competent in "appropriately" interacting with the game.
		// enabling... guiding the player through appropriate interaction???
	},

	ability: { //literacy
		// Ability is the capacity to optimally perform predictably in a domain.
		// Ability is the capacity to maximally control the properties of a defined context or domain.

		// A defined behavior can be or become an ability of a subject.

		// expertise is an ability of an agent that is developed to a higher degree than agents in their environment
	},



	// Cooking is a type of behaviour
	// performing a type of behaviour requires some range ability



	// something something learning teaching
	// contextually relevant symbol domain. (tomatoes, cucumber, pasta, cheese)
	// symbol relation


	dificulty: { // of tasks...
		// Dificulty is what you experience when you lack ability.

		// The iniability to do a thing
		// Requiring high effort to master
		// the intensity/severity of challenge

		// the lack of mastery when performing a skill
		// The difference between an execution and it's optimal execution

		//a thing that is hard to accomplish, deal with, or understand.
	},
	dificultyCurve: {},

	measurementOfAbility: {
		QualityOfOccurence: 1,
		consistency: 1,
		nonContextDependence: 1,
		frequency: 1,
		onDemandAbility: 1,
		efficiencyOfDeployment: 1,
	},
	challenge: {
		// A challenge is a test of skill/ability.

		// It's only a challenge if the player is motivated

		// A competitive situation that determines a threshold or level of superiority/mastery in terms of ability or strength.
		// A call to prove or justify something. (put something under test)

		// that which determines a winner amongst challengers

		// A challenge determines a champion: A person who has surpassed all rivals in a sporting contest or other competition.
		// A test can determine the fittest amonst options

		// A challenge is when
		//	skills are required to succeed
		//	and it is dificult or strenous to succeed.

		// Training vs challenge
		//	training isn't necesarily challenging. but might take time.

		// skillsystem vs challenge
		// 	A challenge requires a developed skill
		// 	A challenge tests the development of a skill.
		// 	A challenge can enforce the development of skills.
		// 	skillsystem - domain of expertise.
		is: {
			or: {
				dificult: 1,
				strenous: 1,
			}
		},
		has: {
			dificulty: 1, //dificulty is the intensity/severity of challenge
		}
	},

	// ABILITIES from the universal and elemental to the context specifics

	// games are artifacts for interactive domain situational challenge traning









	learningAndTeachingTheories: { // (Education)
		//understandings of players and their needs, and the backgrounds and interests of individual players.
		//
		//
		//
		//what are the hidden dependencies the player must understand before learning this?
		//	how do you find that which the player doesn't understand
		//	how do you find the boundaries of what the player understand!
		//	
		//	
		//hidden curriculum
		//	side effects of enduction which are learned but not (publicly) intended.
		//	examples usually include transmission of: norms, values and beliefs
		//	
		//	
		//learning environment - 
		//	physical the office/room they play the game.
		//	virtual - the simulated game world
		//	different environmets affords different studies.
		//	
		//	
		//	
		//	
		//learning and navigating is synonymous. in context x these are the prominent relationships.
		//we know everything from birth everything else is just entrained biases in further recollection.
		//(to know nothing and know everything is the same, when every direction is equally valid, no constraints, then you know everything)
		//
		//it is literally to make brain contextually biased in order to prefer some outcomes over others.
		//	because some contexts are inherently more generative for some set of goals.
		//	
		//	
		//	
		//
		//learning bias: bias to first impression. We mend our minds to fit the reality of first impresion.
		//	thus new impression are just added ontop. unless you establish a dissocative break from the rest of the knowledge.
		//	
		//	we make sense of things with teh relationship between what's new and old.
		//	
		//	
		//	
		//	fantasy is like a dissociative context.
		//	
		//	
		//	
		//	
		//learningTransfer: (transfer of learning) (ability to come up with solutions to new problems)
		//You always aim to create useful learning transfer.
		//	to make the learner apply what they've learned in the broadest useful multitude of contexts.
		//	
		//You alway aim to pull the transfers of their existing knowledge base into the field or subject your are teaching.
		//	by relating it.
		//	
		//learning transfer is the ability to learn what has been established in new contexts.
		//So it is related to intelligence...
		//
		//learning transfer is literally about learning the abstraction... ability to apply between contexts.
		//"The [presented] surface structure is unimportant"
		//
		//so a good challenge in learning transfer is to introduce different problem solving contexts where what you've learned can be used. like in math when you count melons one time and money some other and what not.
		//	this is a change in surface structure, but where the problem is the same in nature.
		//
		//good problems solvers can alway removethe surface structure of a problem and get to the bottom of it.
		//	get the relevant abstract concepts to solve it.
		//	
		//	
		//	
		//introduce problems just beyond the border of their understanding.
		//
		//then you do like zelda boss battle strcture where every small thing you grinded on can be put together to defeat the boss.
		//
		//
		//ability to identify differences and similarities between contexts accurately.
		//
		//
		//the opposite is to not be lazy and provide false attributions to phenomena.
		//	aka overly relying on analogy without checking the underlying facts.
		//	It's when things seems similar in nature but in reality they have different underlying phenomena.
		//	seeing finer details with your contextual sensitivity.
		//
		//
		//gestalts are idea chunks of elemental properties. things you point to as singular concept that has a complex network of ideas and relationships, but that have been contextually simplified for usability in the current context.
		//
		//
		//
		//
		//
		//
		//we remember things that are stimulating and extraordinary... emotionally charged, dramatic, so ye...
		//	be dramatic about the biggest insights lol.
		//	
		//Affective Context Model - People remember how things made them feel, and use those emotional imprints to create memories on demand.
		//
		//(we only care and feel good about thing that we think will matter in our lives)
		//"Why is this knowledge affectively important right now?" "What can I use this info for?"
		//
		//
		//
		//
		//
		//New knowledge cannot be told to students, it believes, but rather the students' current knowledge must be challenged. In this way, students adjust their ideas to more closely resemble actual theories or concepts.
		//^(puttin people on the hero's journey lol)
		//Also good case for antagony being driver for learning and knowledge.
		//
		//
		//induce the inner critic in the player make them intrinsically motivated to learn and explore knowledge.
		//
		//
		//dialogue is way more efficient and creating knowledge than 
		//
		//Students can learn through talk, discussion, and argumentation
		//
		//teach useful knowledge resources (packages) (units of usability)
		//make things feel important? tell with enthusiasm, stories, scenarios, simulation.
		//
		//
		//
		//
		//
		//
		//
		//learning is all about mapping boundaries.
		//
		//
		//
		//
		//
		//constructivism - ability to learn relies on what you already know (scaffolding lol)
		//
		//
		//
		//
		//context = "any information that can be used to characterize the situation of an entity."
		//context-aware applications which use them to (a) adapt interfaces, (b) tailor the set of application-relevant data, (c) increase the precision of information retrieval, (d) discover services, (e) make the user interaction implicit, or (f) build smart environments.
		//Context-aware systems are concerned with the acquisition of context (e.g. using sensors to perceive a situation), the abstraction and understanding of context (e.g. matching a perceived sensory stimulus to a context), and application behaviour based on the recognized context (e.g. triggering actions based on context).
		//
		//
		//
		//
		//
		//repeated doing with slight variations creates an average memory, of everything that is relevant in the context.
		//
		//
		//
		//
		//
		//
		//
		//
		//congitive restructuring (post-modern?)
		//
		//
		//
		//socratic questioning:
		//	the teacher assumes an ignorant mindset in order to compel the student to assume the highest level of knowledge.
		//	
		//	"but what about x?"
		//	
		//	
		//double loop learning = is the question right? (am I correcting for the right thing?)
		//(Is my mental model right?)
		//
		//^ asking: has the context changed to make my solution irrelevant?
		//
		//
		//
		//
		//
		//
		//
		//meta cognition:
		//Metacognition can take many forms; it includes knowledge about when and how to use particular strategies for learning or problem-solving.[1] There are generally two components of metacognition: (1) knowledge about cognition and (2) regulation of cognition.
		//
		//
		//
		//"reflection"
		//	 "paying critical attention to the practical values and theories which inform everyday actions
		//	 A key rationale for reflective practice is that experience alone does not necessarily lead to learning; deliberate reflection on experience is essential
		//	 
		//reflection is learning by interacting with your memories of previous events through new contexts in the present.
		//	comparison of memories that lets you extrapolate new insights.
		//	
		// person who reflects throughout his or her practice is not just looking back on past actions and events, but is taking a conscious look at emotions, experiences, actions, and responses, and using that information to add to his or her existing knowledge base and reach a higher level of understanding
		// 
		// ^ produce guided reflection in games.
		// 
		// 
		// 
		// 
		//warm up gate. - warm up the necesary skills for the problem.
		//
		//
		//needless context switch and distraction as means to build resilience and critical thinking.
		//
		//
		//
		//you have to construct a problem before you can carry out any technical activity.
		//	You can build anything but we don't know how to choose what to build.
		//	
		//	
		//scaffolding is the act of correcting the child until they find mastery in the field (grokk the concept)
		//	after that correction is not needed in that area
		//	it's the focused questions and positive interactions we give to our children.
		//the hardest task they can do with scaffolding leads to the greatest learning gains.
		// 	sophisticated feedback for their level of expertise. training the child at the edge of their capacity.
		// 	
		// the goal is to develop self guided scaffolding... where the student finds the right books or tutors for what they need to know..
		// 
		// scaffolding is just development of mastery in a safe and focused feedback environment only when mastered can you build higher levels (knowledge depending on lower levels)
		// 
		// for good scaffolding you need Recognizing students' individual abilities and foundation knowledge
		// 	
		//
		//
		//zone of proximal development:
		//	 the zone of the closest, most immediate psychological development of learners that includes a wide range of their emotional, cognitive, and volitional psychological processes
		//	 
		//in the zone of proximal development, learners cannot complete tasks unaided, but can complete them with guidance.
		//
		//he teacher's job is to move the child's mind forward step-by-step (after all, teachers can't teach complex chemical equations to first-graders)
		//
		//determine which student is ready for which lesson. (and not lesson as in class but, experience)
		//
		//
		//
		//scaffolding is an activity between a learner and an expert.
		//	it's a collaborative interaction.
		//	should take place in the learner's zone of proximal development.
		//		to do this the expert needs to be aware of the learner's current level of knowledge
		//		and work to a certain extent beyond that knowledge.
		//			(connect it in unfamiliar ways..)
		//			
		//	the scaffold has to be gradually removed (where the learner shows they have a solid foundation by themselves that stands the test of differing context and time.
		//	the scaffold is remed gradually until the learner is independent.
		//	
		//There's a lot of notes on scaffolding... https://en.wikipedia.org/wiki/Instructional_scaffolding	
	},
	//A theme is the focus of the learning session


	// Techniques for presenting information in pedagogical ways for people.
	ProgressiveDisclosure: {
		// A method of managing complexity, in which only necessary or requested information is displayed. 
		// • Progressive disclosure is used to prevent information overload and provide step-by-step GUIdance. It involves separating information into layers, and only presenting layers that are necessary or relevant. 
		// • Progressive disclosure is used in user interfaces, instructional materials, and the design of physical spaces. It can also be applied to differentially support users of different skill levels; for example, displaying advanced features to advanced users only. 
		// • Use progressive disclosure to reduce information complexity. Consider hiding infrequently used controls or information, but make them available through simple requests, such as pressing a “More” button. When procedures are sensitive to error, use progressive disclosure to lead novices through the procedure step by step. 
		// See Also Control • Errors • Layering • Performance Load 

		// Theme parks progressively disclose lines so that only small segments of the line can be seen from any particular point. 
	},
	// Learning, criticality of information order?
	invertedPyramid: {
		// Information presented in order of importance—from most important to least important. 
		// • In the pyramid metaphor, the base represents the most important information, while the tip represents the least important information. To invert the pyramid is to present the important information first, and the background information last. 
		// • The inverted pyramid has been a standard in journalism for over one hundred years, and has found wide use in instructional design and technical writing. 
		// • The lead is the critical component of the inverted pyramid, as it is presented first. It should be a terse summary of the what, where, when, who, and why. 
		// • Use the inverted pyramid when presentation efficiency is important. When it is not possible to use the inverted pyramid (e.g., scientific writing), consider an abstract or executive summary at the beginning to present the key findings. 
		// See Also Progressive Disclosure • Serial Position Effects

		// Lead - information readers must have to know what happened
		// body - information that helps readers understand but is not essential
		// conclusion - information that is interesting or nice to have

		//	The report of President Lincoln’s assassination established the inverted pyramid style of writing in
	},
	pictureSuperiorityEffect: {
		// Pictures are remembered better than words. 
		// • Pictures are better recognized and recalled than words, although memory for pictures and words together is superior to either one alone. 
		// • When recall is measured immediately after the presentation of pictures or words, recall for both is equal. When recall is measured more than thirty seconds after presentation, pictures are recalled significantly better. 
		// • The recall advantage increases when exposure is casual and time-limited. For example, walking by an advertisement with a picture is more likely to be recalled than an advertisement without a picture. 
		// • Use the picture superiority effect to improve recognition and recall. Use pictures and words together when possible, ensuring that they reinforce one another for optimal effect. 
		// See Also Iconic Representation • Inattentional Blindness Mere-Exposure Effect • Recognition Over Recall

		// The authors know you are looking at the pictures in this book first, and then begrudgingly reading text when needed. 

	},









	// measures and limits of communication:
	propositionalDensity: {
		// The relationship between the elements of a design and the meanings they convey. 
		// • A proposition is a simple statement about a thing that cannot be made simpler. Propositional density is the amount of information conveyed per unit element. 
		// • There are two types of propositions: surface and deep. Surface propositions are visible elements. Deep propositions are the meanings they convey. 
		// • Propositional density can be estimated by dividing the number of deep propositions by the number of surface propositions. A propositional density greater than one makes things engaging and memorable. 
		// • Strive to achieve the highest propositional density possible, but make sure the deep propositions are complementary. Contradictory deep propositions confuse the message and nullify the benefits. 
		// See Also Framing • Inverted Pyramid • Stickiness 

		// Barack Obama’s 2008 campaign logo has a high propositional density, a key to its success. There are three visible elements (e.g., blue circle), and approximately ten meanings (e.g., O for Obama, American flag, etc.). PD = 10 / 3 = 3.33. 
	},




	// add to human performance
	// a measure of mental and physical effort required for a user to complete a task
	performanceLoad: { // human performance load
		// The mental and physical effort required to complete a task. 
		// • There are two types of performance load: cognitive and kinematic. 
		// • Cognitive load is the mental effort required to accomplish a goal (e.g., recalling a new phone number). Kinematic load is the physical effort required to accomplish a goal (e.g., dialing a phone number). 
		// • When performance load is high, time and errors increase. When performance load is low, time and errors decrease. 
		// • Minimize cognitive load by reducing information density, reducing the mental effort required to perform tasks, and automating tasks when possible. 
		// • Minimize kinematic load by reducing the number of steps in tasks, reducing the physical effort required to perform tasks, and automating tasks when possible. 
		// See Also Accessibility • Cost-Benefit • Depth of Processing 

		// Classic photo from a time-motion study conducted by Frank and Lillian Gilbreth to reduce time
		// and effort to perform tasks.
	},
	hicksLaw: {
		// The time it takes to make a decision increases with the number of options. 
		// • Proposed by British psychologist W.E. Hick. 
		// • Hick’s Law applies to simple decisions with multiple options. For example, if A happens, then push button 1; If B happens, then push button 2. 
		// • Increasing the number of choices increases decision time logarithmically. 
		// • Hick’s Law does not apply to complex decision making, or decisions requiring reading, scanning, searching, or extended deliberation. 
		// • Consider Hick’s Law in designs that involve simple decision making. Reduce the number of options to reduce response times and errors. 
		// See Also Chunking • Errors • Fitts’ Law • Interference Effects Signal-to-Noise Ratio • Wayfinding 
		// ￼
		// Hick’s law applies across a variety of contexts, including predators targeting prey, quick adjustments to camera settings, pressing different buttons in response to a light, deciding which technique to use to block a punch, and following traffic signs. The more options you have, the longer it takes to decide. 
	},

	// Ideas of an experience has to be accurately conveyd as experience to the player. This requires good communication skills and information transference
	// communication, clarity, teaching and learning
	Legibility: {
		// The visual clarity of text, generally based on size, typeface, contrast, line length, and spacing. 
		// • Use 9- to 12-point type for high-resolution media such as print. Smaller type is acceptable when limited to captions. Use larger type for low-resolution media. 
		// • Use clear typefaces that can be easily read. There is no performance difference between serif and sans serif typefaces, so select based on aesthetic preference. 
		// • Favor dark text on a light background for optimal legibility. Avoid patterned or textured backgrounds. 
		// • Make line lengths 10–12 words per line, or 35–55 characters per line. 
		// • Set leading (space between text lines, baseline to baseline) to the type size plus 1–4 points. Favor proportionally spaced typefaces to monospaced. 
		// See Also Chunking • Depth of Processing • Readability
	},
	RosettaStone: {
		// A strategy for communicating novel information using elements of common understanding. 
		// • The Rosetta Stone is an Egyptian artifact inscribed with one message in three scripts. Modern knowledge of one of the scripts enabled scholars to translate the other two unknown scripts. 
		// • Applying the Rosetta Stone principle involves embedding elements of common understanding in messages, called keys, to act as bridging elements from the known to the unknown. 
		// • Make it clear that keys are keys, to be interpreted first and used as a point of reference. 
		// • Deliver messages in stages, with each stage acting as a supporting key for subsequent stages. 
		// • When you don’t know what level of understanding the recipient will have, embed multiple keys based on widely understood or universal concepts. 
		// See Also Archetypes • Comparison • Iconic Representation Propositional Density • Uniform
		// Connectedness 

		// The plaques on the Pioneer space probe included multiple keys to help ETs decipher its message, including representations of hydrogen, the relative position of the Sun to the center of the Galaxy and fourteen pulsars, and silhouettes of the spacecraft. 
	},
	Readability: {
		// The ease with which text can be understood, based on the complexity of words and sentences. 
		// • Readability is one of the most overlooked and important aspects of design. 
		// • Readability is determined by factors such as word length, word commonality, sentence length, clauses per sentence, and syllables per sentence. 
		// • Readability can be improved by omitting needless words and punctuation, avoiding acronyms and jargon, keeping sentence length appropriate for the intended audience, and using active voice. 
		// • When targeting a specific reading level, consider readability formulas designed for this purpose. 
		// • Consider readability when creating designs that involve narrative text. Ensure that the reading level is audience appropriate. Strive to express complex concepts in simple ways, using plain language. 
		// See Also Inverted Pyramid • KISS • Stickiness • Storytelling 

		// Fry’s Readability Graph is one of many tools that can be used to ensure that readability of text is audience appropriate. 
	},
	phoneticSymbolism: {
		// The meaning conveyed by the sounds of words. 
		// • Certain letter sounds symbolize size, gender, and aggression, and reinforce these meanings in words. 
		// • Consonant sounds with a constant flow of air—such as the letters s , f , v , z —are associated with smallness, femininity, and passivity. Consonant sounds where the air is blocked—like the letters p , k , t , b , g , d , hard c —are associated with largeness, masculinity, and aggression. 
		// • Vowel sounds that widen the mouth, as with a smile—e (bee), i (sit), a (hate), e (best)—are associated with smallness, femininity, and passivity. Vowel sounds that bring the mouth into a circle—o (dome), o
		// (caught), a (can), u (food), u (put), u (luck), a (cot)—are associated with largeness, masculinity, and aggression. 
		// • Consider phonetic symbolism in naming and pricing. Ensure that the phonetic symbolism of brands and important numbers are congruent with their meanings. 
		// See Also Affordance • Priming • Propositional Density 

		// Phonetic symbolism makes these terms from Game of Thrones somewhat intelligible even though
		// the languages are fictional. 
	},
	// Memory and learning
	DepthOfProcessing: { //about human learning and memory... add to memorability...
		// Thinking hard about a thing improves the likelihood that it can be recalled. 
		// • Information that is analyzed deeply is better recalled than information that is analyzed superficially. 
		// • Memorization results from two types of rehearsal: maintenance and elaborative. Maintenance rehearsal involves repeating information back to yourself. Elaborative rehearsal involves deeper analysis, such as answering questions and creating analogies. 
		// • Elaborative rehearsal results in recall that is two to three times better than maintenance rehearsal. 
		// • The key factors determining recall are distinctiveness, relevance, and degree of elaborative rehearsal. 
		// • Consider depth of processing when recall and retention are important. Get people to think hard about information by engaging them in different types of analysis. Use case studies, examples, and similar devices to make information interesting and relevant. 
		// See Also Serial Position Effects • von Restorff Effect 

		// Information presented in a highly legible typeface (top) is easier to read, but less memorable than the same information in a less legible typeface (bottom). The reason? A typeface that is difficult to read makes readers think harder about the content, making it more memorable. 
	},
	MnemonicDevice: { //add to memory....
		// A technique for making things easier to remember through patterns of letters or associations. 
		// • Types of mnemonic devices include the following: first-letter, keyword, rhyme, and feature-name. 
		// • First-Letter: The first letter of items to be recalled are used to form the first letters in a meaningful phrase, or combined to form an acronym. 
		// • Keyword: A word that is similar to a word or phrase that is linked to a familiar bridging image. 
		// • Rhyme: One or more words in a phrase are linked to other words through rhyming schemes. 
		// • Feature-Name: A word that is related to one or more features that is linked to a familiar bridging image. 
		// • Use mnemonic devices to increase memorability of logos, slogans, and rote facts. Favor concrete words and images to leverage familiar concepts. 
		// See Also Recognition Over Recall • Serial Position Effects Stickiness • von Restorff Effect

		// Clever use of mnemonic devices can dramatically increase recall, as with this unforgettable dance studio logo.
	},
	recognitionOverRecall: {
		// Memory for recognizing things is better than memory for recalling things. 
		// • Recognition is easier than recall because recognition tasks provide cues that facilitate a memory search. 
		// • The principle is often used in interface design. For example, early computers used command line interfaces, which required recall memory for hundreds of commands. Modern computers use graphical user interfaces with commands in menus, which only require recognition of desired commands. 
		// • Decision making is also influenced by recognition. Familiar options are selected over unfamiliar options, even when the unfamiliar option may be the best choice. Recognition of an option is often a sufficient condition for making a choice. 
		// • Minimize the need for recall whenever possible. Use menus, decision aids, and similar strategies to make available options clear and visible. 
		// See Also Mere-Exposure Effect • Performance Load • Visibility  

		// On December 9, 1968, Douglas Engelbart gave “The Mother of All Demos”, which laid the foundation for user interfaces based on recognition over recall. This not only made computers easier to use, it enabled them to become mass-market products. 
	},
	mentalFillIn: { //related to memory and abstraction and learning
		// something repeatedly exposed to the player does not need as much detail as the player grokk it.
		// basically, when we have seen a thing 100 times we can either skip it entirely or use a more abstract less costly version of it.
		// it is acceptable to simplify grokked details that does not affect game-play.
	}, // We need to grokk it once and only once.

	// Memorability?
	stickiness: {
		// A formula for increasing the recognition, recall, and voluntary sharing of an idea. 
		// • Stickiness explains why certain ideas go viral and become lodged in the cultural consciousness. It is
		// characterized by the following six attributes, which form the mnemonic SUCCES : 
		// 1. Simple: Sticky ideas can be expressed simply and succinctly without sacrificing depth. 
		// 2. Unexpected: Sticky ideas contain an element of surprise, which grabs attention. 
		// 3. Concrete: Sticky ideas are specific and concrete, typically using plain language or imagery. 
		// 4. Credible: Sticky ideas are believable, often communicated by a trusted source. 
		// 5. Emotional: Sticky ideas elicit an emotional reaction. 
		// 6. Stories: Sticky ideas are expressible as stories, increasing their memorability and retelling. 
		// See Also Inverted Pyramid • Storytelling • von Restorff Effect

		//￼ (KEEP CALM AND CARRY ON) 
		// From WWII British motivational poster to modern-day meme, the enduring popularity of the message owes to its stickiness.
	},




	// principle of learning: tracking/recongition is easier than understanding.

	// Attention dictated level of abstraction - TODO: choose one consistent word plz: control, dictate, regulate, determine

	// Visual field prominense dictated level of abstraction

	//	uncertainty/inconsistency at distance.
	//	noise at a distance.
	// A technique to saving data without leaving distant things abstract.
	//	works well for like textures. might not work well for objects.
	//		aka if you look away and back and someone is replaced by a completely different looking person.
	//		so basically, noise can be used in relation to the level of memorability/recognition.
	// noise can be used to implement concretely abstract concepts that are not important...



	// communicating complexity
	hierarchy: {
		// Hierarchies are the simplest way to visualize and understand complex information. 
		// • There are three types of hierarchical structures: trees, nests, and stairs. 
		// • Tree structures locate child elements below or to the right of parent elements, forming a triangle. Trees are used to represent organizations and high-level maps. 
		// • Nest structures locate child elements within parent elements. Nest structures are used to group information and represent logical relationships. 
		// • Stair structures stack child elements vertically below parent elements, as in an outline. Stairs are used to represent complex structures that change over time. 
		// • Use tree structures to represent hierarchies of moderate complexity. Use nest structures to represent simple hierarchies and logical relationships. Use stair structures to represent complex hierarchies. 
		// See Also Alignment • Five Hat Racks • Layering • Modularity

		// This classic 1855 organizational chart of the New York and Erie Railroad uses a tree structure with elements of stairs and nests.
	},
	layering: {
		// Presenting information in stacked layers to manage complexity and foster insights. 
		// • Layering is revealing and concealing planes of information, one on top of the other, to recontextualize information and highlight relationships. 
		// • Opaque layers are useful when additional information about a particular item is desired without switching contexts (e.g., software pop-up help). 
		// • Transparent layers are useful when overlays of information combine to illustrate concepts or highlight relationships (e.g., weather maps). 
		// • Consider layering to manage complexity, elaborate information, and illustrate concepts without switching contexts. Use opaque layers when presenting elaborative information. Use transparent layers when illustrating concepts or highlighting relationships. 
		// See Also Hierarchy • Modularity • Progressive Disclosure

		// Layering is effectively used in online maps, selectively revealing and concealing useful like traffic and restaurants. 
	},


	mostAdvancedYetAcceptable: { // maya for short. hehe I am sonic
		// A strategy for determining the most commercially viable aesthetic for a design. 
		// • The most advanced yet acceptable principle (MAYA) was proposed by Raymond Loewy, the father of industrial design. 
		// • MAYA asserts that aesthetic appeal is a balancing act between uniqueness and familiarity—i.e., a product should stretch the definition of a product category, but still be recognizable as a member of that category. 
		// • The principle has been tested empirically, and it does appear that the most novel design that is also somewhat familiar has the greatest aesthetic appeal. 
		// • Consider MAYA when designing for general audiences. In contexts where aesthetic assessments are made by design or art experts, MAYA does not apply. In these cases, novelty is weighed more heavily than familiarity. 
		// See Also Form Follows Function • IKEA Effect Mere-Exposure Effect • von Restorff Effect

		// MAYA explorations by Raymond Loewy demonstrating how the ideal aesthetic form of common changes over time.
	}, // "aesthetic appeal is a balancing act between uniqueness and familiarity"



	Wayfinding: {
		// this idea originate from the book Universal Principles of Design
		// The process of using spatial and environmental information to navigate to a destination. 
		// • Wayfinding involves orientation, route decision, route monitoring, and destination recognition. 
		// • Support orientation using clearly identifiable “you are here” landmarks and signs. 
		// • Support route decisions by minimizing the number of navigational choices, and providing signs or prompts at key decision points. 
		// • Support route monitoring by connecting locations with paths that have clear beginnings, middles, and ends. Provide regular confirmations that the path is leading to the destination. 
		// • Support destination recognition by disrupting the flow of movement through a space using barriers or dead ends, and giving destinations clear identities. 
		// See Also Mental Model • Progressive Disclosure Rosetta Stone • Visibility

		//The London 2012 Olympic Park used landmarks, signage, simple routes, and destination markers to support wayfinding. 
	},





	// Storytelling and memory sequencing lol
	// Memory sequencing lollll
	story: {
		is: {
			memorable: 1,
			entertaining: 1,
			educational: 1,
		},
		//can be predefined or improvised
	},
	narrative: { //storytelling: story: {},
		// story settings for earthly consistency: most important axes:
		//	time epoch
		//	distance from equator (climate)
		//	 country/culture
	},
	mythology: {
		//mythology has a deeper truth to it than history
	},
	// Crucial elements of stories and storytelling include plot, characters and narrative point of view.
	Storytelling: {
		// story telling is just a form of efficient teaching by using memorable tricks that are easy communicate and pass on.
		// Storytelling is the perfect infotainment. Exaggerated to be interesting but containing teachings of value.

		// Evoking imagery, emotions, and understanding through the presentation of events. 
		// • Storytelling is the original method of passing knowledge from one generation to the next. 
		// • Good storytelling requires certain fundamental elements, including a setting (e.g., time and place), characters with whom the audience can identify, a plot that ties events together, events and atmospherics that evoke emotions, and a pace and flow that maintains the audience’s interest. 
		// • Good storytelling minimizes the storyteller. 
		// • Good stories tend to follow archetypal plots. 
		// • Use storytelling to engage audiences, evoke emotions, and enhance learning. When successful, an audience will experience and recall the events of the story in a personal way. It becomes a part of them. 
		// See Also Archetypes • Stickiness • Zeigarnik Effect 


		// The Civil Rights Memorial tells the story of the civil rights movement using all of the fundamental storytelling elements. 

		// Plot - plan of a series main/major events of the story
		// Plot is chains of events happening in a world. A happens so B happens so C happens ...
		// Plot exists whenjustification exists. B happens from A because [Reason]
		// He is hungry, therefore he ate popcorn, therefore he feels full



		// Storytelling often uses tropes as an efficients means to communicate quickly and connect new things to what is already established or known to the player.

		// BOTW - by putting the story in the past you separate the linear story from the story the player tells themselves through game-play.
	},
	generalStorySequence: {
		disconnect: 1, //you are disconnected from the world and from sensory stimulation. The player is transported/loaded into the game world.
	},
	storyTellingThings: { // tools for thinking about human condition and experience over time
		// playing with the familiar and unfamiliar
		// the return to the familiar.
		// look into storytelling techniques and try to encode them.
		dramatization: 1,
	},

	conflictStructure: { //antagony, neutrality, nurturality
		// there is a structure of relationsships between agents in terms of alignment of drives and how the subjects deal with others...
		// especially agent...
		// Different interaction methods appeal to different personalities.
		peaceful: 1,
		antagony: 1,
		nurture: 1, //driveToHelp:1,

		// empowering agency, making something independent.

		// controlling behavior: nudging, slavery,
		// ending/disrupting behaviour through: violence, threat of violence, distraction
	},

	intro: { //introSequence
		// Provides profesionality
		// Sets the mood
		// Associates a product with brand
		// Makes the player ready for the experience (put them in the right mood, ease them into the experience)

		// Best practices: easily skippable by pressing a button 
	},
	outro: { //outroSequence
		// Controlled exit point of the game. (opposite of entryPoint) "Välkommen åter"

		// A moment of reflection and invitation to "come back any time you want"

		// You can force outro by forcing the player to manually save the game.

		// These days outro is almost easter egg.
		// It is a sequence that is only played as the user quits the game/experience through the user interface of the game/experience.
		// Purpose:
		//	Transfer the player back into the real world
		//	if it was an intense game, cool down the player.
		//	Gradual balance to the intensity of the game/experience.
		//	Another opportunity for brand association, (made by team x and supported by these people)

		// Best practices: easily skippable by player by pressing a button 
	},




	worldBuilding: {
		// Detailing properties of a world.

		// for interesting set up
		// for precompiled, determined consistency.


		// ??????????????
		//	World building is like cuddling around the player building them inside a world view, a context, which primes them to behave writhin the narrative boundaries you set up. It is what makes sense of the actions the player is performing.
		//	A hero's journey story is just s method to circle around a new context of learning a concept where you set up a circle of mental connections that you want to collect around a single point and towards the climax you spiral through all the points of learning which either gets overwhelming or pulls you through to the other side and back
	},

	// World building and world tropes?
	history: {},







	EntryPoint: { // first impression....
		// A point of physical or attentional entry that sets the emotional tone for subsequent interactions. 
		// • Good entry points feature three elements: minimal barriers, points of prospect, and progressive lures. 
		// • Barriers should not encumber entry points. Examples include highly trafficked parking lots, noisy displays, and salespeople standing at the doors. 
		// • Entry points enable people to survey available options. Examples include entrances that provide a clear view of store layout, and web pages that provide orientation cues and visible navigation options. 
		// • Progressive lures pull people through the entry point. Examples include jumplines in newspapers and compelling displays just beyond the entry of a store. 
		// • Optimize entry points by reducing barriers, establishing clear points of prospect, and using progressive lures. 
		// See Also Desire Lines • Prospect-Refuge • Wayfinding 

		// Apple retail stores have redefined the retail experience, in part due to their exceptional entry point experience design. 
	},

	// Narrative structure and storytelling:
	threeActStructure: {
		// Three act structure of short film
		// The dramatic question: Will the [setup] find a solution to the end goal?
		// Usually it is a stereotypical role, detective - solve mystery, hero - save the day, criminal - served justice,
		// The answer to the dramatic question is often yes; no; maybe; yes, but...; or no, and what is more...
		// FIRST ACT - (usually for exposition) //establish the main subjects and their relationships and the environment they live in (the static background of their existence).
		// setup: (initial world scenario, where and what) (background... what themes/objects/feelings is it this movie orbiting around. or at least what is its ground tone...)
		//	A stable situation (A defined consistency)
		// inciting accident: Interruption from the pattern and stability of the setup 
		//	this creates the call to action for the protagonist.
		// Response: How the setup reacts to the interruption. (Maybe it will go away if we hide.)
		//	+ realization that life will never be the same.
		//	usually leading to a lowering of intensity in the story. Regrouping and making up a strategy. Temporarily safe, but now what.
		//
		// SECODN ACT - all about trying and getting more into a pickle
		// Attack: Their natural reaction was unsustainable. Relying on habit did not work. Time to learn something. By trying to remove the interruption and return to the setup state.
		//	it is about trying to wriggle out of the situation. Out of the trauma. It is about force.
		// Part of the reason protagonists seem unable to resolve their problems is because they do not yet have the skills to deal with the established confronting forces that oppose them.
		// 'all is lost': The trial and error made the protagonist use up most of their opportunities and resources. Now they are in a real pickle.
		// THIRD ACT?
		// This cannot be achieved alone and they are usually aided and abetted by mentors and co-protagonists.
		// Climax: it is what it leads to. what happens at the end. What happens at the most intense, at the core of the confrontation. The true nature of the trauma.
		// Surprise: The correct answer, or rather A solution, shows up in midst of the biggest darkness.
		// They must not only learn new skills but arrive at a higher sense of awareness of who they are and what they are capable of, in order to deal with their predicament, which in turn changes who they are.
		// This is referred to as character development or a character arc. 
		// resolution: The protagonist gets more confident, turn into a highest stable state, the outcome of the climax gets clearer. (the surprise is part of the resolution)
	},

	actionsForStoryTelling: { //describing events in time? //story? events??? quests???
		find: 1,
		found: 1,
		locate: 1,
		located: 1,
		// can be found in Sweden. Is found in Sweden.
		// can be found in snow. is sometimes found in snow.
	},

	whatMakesAGoodGameMaster: {
		// Some random notes on what a game master is
		does: {
			entertain: 1, // makes it fun, creates emotional investement
			create: {
				socailContext: {
					// creates good socialization
					//		a good time with friends
					//		Cultural fit
					//		mechanical fitness
					//		^ only get people together that fit together.
				}
			},
			improvisationalStorytelling: { // See improvise: and storytelling:
				//	a game master is responsible for telling a good story
				//		they can create a story on the fly
				//			even if the plot is defined , you generate the story
				//		How to tell a story (simulation/presentation)
				//			pacing
				//			narrative structure - anticipation
				//		How to make up a good story (consistency, story structure?)
				//		How to adapt the story as you go along (improvisation, adaptability
				//			(to player input, only restrict to avoid paradox.)
				//			give the illusion of freedom by doing the same thing/result in a different way.)
			},
			judgeAndApplyRules: {
				//	A game master must judge and apply rules
				//		deciding on constraints and looseness
				//		Knowing when it is fair to judge or react.
				//		(a program is way better than this than a human so do not worry.)
				//		Generating rules or systems or consistency for the player to expect as a solid.
				//		something to interact with.
			},
			playerEmpathy: {
				//	A game master Listen to your players.
				//		predict what the player needs
				//		What the player wants to do
				//		What the player wants to explore
				//		And give them that in a good way.
				//		And he knows what is not worth spending resources on.
				//			(like explaining botanics for someone that only likes cars)
			},
			balanceExperience: { // or style
				//	A game master you decide the intersection of the game
				//		is it narrative based or is it randomness based?
				//		Is it player opinion based?
				//		etc.
				//	A game master must control/restrict the players
				//		balancing between various player's needs.
				//	Aesthetic, mood, tone, theme
			},
		}
	},


	pacing: { // rhythm, tempo

		// aka presenting to the player what they need or should experience at the right moment.


		// Good pacing: Save the most stunning objects and effects for when it really matters! (unless the game should be highly stimulating all the time)
		// save the most interesting objects and effects and highlights for when it really matters.
		//	for the good occasions, for things that are special and rare.

		// goal: keep your players engaged by not burning them out, boring them or stressing them out too much.
		// give them enough rest between intensities.
		// give them enough variety without alienating them.
		// Do not reveal to much. To make everything too mysterious and incomprehensible.

		// think of ho to make good dramatic structure or whatever its called

		// a human preference
		//	boring/ too slow
		//	overwhelming/too fast
		//	^pacing needs to be considered for every process!
		// the pacing of a (still) image is about implied movement and about distance between objects in a composition
		// it can look like one object comes out of another and this leads the eyes.....


		// opening. opening sequence. start of a movie. prologue.
		// a great opening sequence is a most important part of a movie. (convincing the player to watch/play to the end.)
		// establish elements f your movie
		// having no opening is bad
		// it says what the movie is about.
		// it sets up the setting of the movie.
		// establishes the identity of the main cast. (often the protagonist and the antagonist)
		// it is about establishing what the next part will be about.
		// making the opening just an information dump is slightly better, as it is functionally descriptive, but without drama it creates no interest.
		// this is where the foreshadowing and red herrings happen.


		// choosing and skipping is part of selfpacing.

		// 3 cores of pacing 
		//	Intensity
		//		The challenge and effort required, along with the impact. 
		//		Easy puzzles and combat encounters are low intensity, while difficult puzzles and boss fight are high intensity.
		//		Keep in mind that the narrative can also have intensity.
		//	Variety
		//		The type of activity or things that happen. Varies from game to game, but in an action-adventure it can be going from platforming to stealth to combat.
		//		aesthetic variety - styles, themes, etc
		//		functional variety - jobs/tasks, mechanics, challenges
		//	Time
		//		The minimum and estimated average time it takes to finish something (like your level).

		// these happen on all time scales: (like a fractal)
		// micro, from one player activity to the next
		// medium, from one area to the next. or one chapter to the next.
		// macro: the whole game from beginning to end. What to remember of the overall structure.
	},

	interruption: { //or distraction
		//when something is so different as to lose coherence
	},

	// story techniques:
	delayedExplanation: {
		// The storyteller
		// The explanation does not need to be told and can be constructed afterwards.
		// when done improperly can make audience question the validity and consistency of the story.
	},
	eternalMystery: {
		//is an inifinitelyDelayedExplanation.
		// LOST
	},









	// Storytelling and perception?
	// Movement // storytelling... //storytelling is really sequenced stimuli
	commonFate: { //perception of animation and movement
		// Things that move in similar directions are perceived to be related. 
		// • One of the Gestalt principles of perception. (TODO: look up gestalt principles of perception)
		// • Things that move together in common directions are perceived as a single unit or chunk, and are interpreted to be more related than things that move at different times or in different directions. 
		// • Perceived relatedness is strongest when motion occurs at the same time, velocity, and direction. As these factors vary, perceived relatedness decreases. 
		// • Common fate influences whether things are perceived as figures or ground: moving elements are perceived as figures, and stationary elements as ground. 
		// • Consider common fate when displaying information with moving or flickering elements. Related elements should move at the same time, velocity, and direction, or flicker at the same time, frequency, and intensity. 
		// See Also Chunking • Figure-Ground • Good Continuation Similarity • Uniform Connectedness 

		// In video games, making the names and controller icons move with the players groups them, simplifying playability by making it clear who is being controlled and how to control them. 
	},

	// Symbolism
	archetypes: { //todo: find and merge where ever these is in the document
		// Universal patterns of form, social role, and story that have innate appeal. 
		// • People are instinctively drawn to certain patterns. Such patterns are often referred to as archetypes. 
		// • Interest in archetypal patterns provided our early human ancestors with an adaptive advantage. Modern
		// look in the book of symbols...
		// humans have inherited this interest. 
		// • Archetypal forms include faces, horns and canine teeth, snakes, spiders, and sexual forms. 
		// • Archetypal social roles include the hero, rebel, mentor, trickster, and villain. 
		// • Archetypal stories include the quest, tragedy, voyage and return, and conquering the monster. 
		// • Consider appropriate archetypes in your designs to capture and hold attention. 
		// See Also Affordance • Black Effects • Contour Bias Mimicry • Supernormal Stimulus • Threat Detection
	},




	// symbolism?? abstraction.
	iconicRepresentation: { //icon:
		// A simplified/abstract representant of something that is more complex. Purpose represent recognition/identification.
		// Different from model which purpose is to represent more simply represent function or properties rather than just recognition/identification.

		// Most commonly graphical in nature.

		// Icon's are commonly used in interactive user interfaces, for users to manage and interact with concepts indirectly and abstractly.

		// The use of pictorial images to improve recognition and recall.
		// • There are four kinds of icons: resemblance, exemplar, symbolic, and arbitrary. 
		// • Resemblance icons look like the things they portray. 
		// • Exemplar icons are examples of things. 
		// • Symbolic icons make use of familiar symbols. 
		// • Arbitrary icons are used to establish industry standards or international communication. 
		// • Consider iconic representation to aid recognition and recall, overcome language barriers, and enhance the aesthetics of communication. Generally, icons should be labeled and share a common visual motif for optimal performance. 
		// See Also Chunking • Performance Load Picture Superiority Effect • Rosetta Stone 
		//
		// Iconic representation reduces performance load, conserves display area, and is more understandable across cultures. 

		// A game trope that to universally fit a sprite you get an abstract visual representation of the object that does not reflect its properties.
		// this is a form of information hiding. Progressive disclosure...
		// Most games does not show all properties visually now that I think of it.... it often shares sprites etc and does not perfectly reflect the state the object is in.
		// This trope is often used to fit a visual representation of an object in a abstract space/inventory.
		// It is easier to deal (manipulate, move) with the symbols of things rather than the things themselves....
		// See also iconicRepresentation
	},
	GUI: { //graphical user interface. (Usually means abstract user interface, that uses icons and buttons and symbols that represents things in games)

		//neetly organized symbols representing aspects not aparent from the environment.
		//often represented abstractly: (or for representing the abstract aspects of the game)
		//	10%
		//	500 gold

		//Used for abstract actions.
		//	pressing buttons
		//	selecting on a map
		//	typing into a textfield
		//	slide a slider.

		//GUI is often "interfaced" by pointing and clicking device. A (mouse) cursor or touch input.


		//A HUD is a specific type of GUI that is seen in the view of the game. Usually locked to the screen.
		//	it's like a filter between the screen and the game world.
		// The HUD concpet comes from see through displays in aircraft windows or goggles.

	},
	progressBar: {
		//a GUI in form of a (loading) bar that shows progress
	},
	loadingScreen: {
		// Loading screen is a common part of the entry point playing a video game.
		// Loading screen is a moment for teh player to get ready for the experience.

		// occupation icon (hour glass) or X indicating non-interaction. Or simply removing all pointers from screen (all signals of interactibility)
		// spinner
		// loading bar

		// A type of transition occupation
		// communicates occupation. 
	},




	// Beauty art and virtue

	// The reasons beauty is important.
	aestheticUsabilityEffect: { // human perception.
		is: {
			bias: 1, //humanBias?
		},
		// Aesthetic things are perceived to be
		// * easier to use than ugly things.	
		// * more effective at fostering positive attitudes than ugly things.
		// * more likely to be tried, accepted, displayed, and repeatedly used than ugly things.
		// Aspire to create aesthetically pleasing designs. It is more than ornamentation—it is an investment in user acceptance, forgiveness, and satisfaction.
	}, // aka, why beauty, because utility
}
