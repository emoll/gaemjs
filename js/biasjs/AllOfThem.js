const all = [
	bias,
	biology,
	gameAsMedium,
	games,
	hardware,
	humanWorld,
	implements,
	physics,
	settingWorldAesthetic,
	styleAndArtisticTechnique,
	theHuman,
	abilities,
	systemic,
	learning,
	meta,
]

const allLength = all.length;
for (var i = 1; i < allLength; i++) { //start on 1 to skip bias
	Object.assign(bias, all[i]) // lazy giving all properties to bias. overrides duplicates.
}
