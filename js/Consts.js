//consts.js
/* this document contains all static values like enums, colorschemes, pre-defined draw shapes. etc. */

//Palettes
var palettes = {
	tiltedAquamarine: ["#7c7287", "#9dc0bc", "#b2edc5", "#baffdf", "#c1fff2"],
	saturatedAquamarine: ["#91f9e5", "#76f7bf", "#5fdd9d", "#499167", "#3f4531"],
	tealGrayScale: ["#cad2c5", "#84a98c", "#52796f", "#354f52", "#2f3e46"],
	aquamarine1: ["#140614", "#081d22", "#13483f", "#26734f", "#419e57"],
	teals: ["#00b9ae", "#037171", "#03312e", "#02c3bd", "#009f93"],
	ocean2: ["#5cd5cf", "#5ea6a3", "#38b6ab", "#089ca8", "#2d8b94"],
	differentWaters: ["#75dddd", "#508991", "#172a3a", "#004346", "#09bc8a"],
	gameBoyPalette: ["#9BBC0F", "#8BAC0F", "#306230", "#0f380f"],
	blackToGreen: ["#152614", "#1e441e", "#2a7221", "#119822", "#31cb00"],
	freshBlueScales: ["#131515", "#2b2c28", "#339989", "#7de2d1", "#fffafb"],
	aquamarine2: ["#09151d", "#0d2a31", "#124047", "#3ea694", "#f7efe1"],
	forestSpirit: ["#e7eada", "#648452", "#7a9f5b", "#345248", "#687f87"],
	WhiteWoodHouseRocksNearTheForest: ["#fcf3d2", "#e0ff5c", "#9cddb7", "#97cb66", "#668542"],
	SweetLemonLand: ["#d4e09b", "#f6f4d2", "#cbdfbd", "#f19c79", "#a44a3f"],
	boatsandnatureaddwhite: ["#d3ff99", "#899cd7", "#1a6391", "#20b1a4", "#4f9b6a"],

	forest: ["#67751a", "#202a19", "#e9de2d", "#92764b", "#f3f7fa"],
	ForestTunnel: ["#96adc8", "#d7ffab", "#fcff6c", "#d89d6a", "#6d454c"],



	trasluscentBottles: ["#4e1613", "#eab50b", "#82575a", "#567801", "#dcfa00"],

	magicalForest: ["#3c2d44", "#5d4869", "#a35b52", "#f3a258", "#8283a5"],
	grayTechWorksWithAquamarine1: ["#151015", "#2a2a2e", "#474b4b", "#401813", "#7b3519"],

	autumn: ["#2a1c1c", "#7b0405", "#cc9f00", "#e56f06", "#d12d03"],
	veryAutumn: ["#18233e", "#b71022", "#cd911a", "#657614", "#5c5151"],
	warmcoffebath: ["#87595a", "#482d32", "#984a40", "#e87849", "#f6d281"],
	weirdfishes: ["#c6654b", "#afa794", "#4c63b7", "#c7459c", "#7e457f"],
	whisps: ["#a8bcba", "#e5a899", "#fae2c8", "#c54525", "#2c4b4e"],
	//mono / "linear" gradients:






	//wood sand

	GoldenStarsInSpace: ["#9d7351", "#fefbd7", "#f6f66b", "#f9bd4c", "#110d05"],
	landscapeAddSky: ["#152303", "#fef0e3", "#8ea74c", "#fdef17", "#b18958"],
	niceBrowns: ["#d15122", "#7e3e23", "#161822", "#af917a", "#431824"],
	PsychadelicGoldGrayDream: ["#98a1a8", "#ae9b80", "#3e290b", "#e89c02", "#edc01b"],
	SandHeaven: ["#eec799", "#7f552c", "#2b312a", "#3d4e30", "#c5a88c"],
	sandLake: ["#ffef57", "#ffcc3b", "#604c2f", "#a7966b", "#82ca9c"],
	ShadowSand: ["#574d68", "#a38560", "#c6a15b", "#f2e86d", "#d3dfb8"],
	stormfield: ["#fde0a7", "#40353c", "#dec3ae", "#e67112", "#390000"],
	woodandmarsmallow: ["#cc9052", "#60351d", "#f1c36e", "#4f6160", "#eefefb"],

	mildlandscapes: ["#f9feff", "#85aa72", "#3c5c59", "#a1ebfd", "#d0a7a2"],
	aesthetic: ["#fedee9", "#cabdd9", "#325783", "#376058", "#131a28"],
	blues: ["#ffffff", "#78bcef", "#5474d1", "#3e2c9a", "#211a4a"],
	ocean: ["#1e2e76", "#64e7ff", "#3377c7", "#259fee", "#28b4f6"],
	cloudyWhisp: ["#020305", "#4f6074", "#a5c8de", "#b9bec1", "#f9f8f4"],
	bluedragonsnake: ["#90ddfa", "#3595fc", "#a4c9bc", "#f6eb99", "#4d3735"],


	// Sega Genesis
	pacmanPark: ["#bcf8ec", "#a6c36f", "#00a5cf", "#596157", "#ffbf46"],
	Sega: ["#ffbf00", "#e83f6f", "#2274a5", "#32936f", "#ffffff"],
	smoothCurb: ["#f7fff7", "#343434", "#2f3061", "#ffe66d", "#6ca6c1"],
	snowboardMetro: ["#b7ad99", "#ff4365", "#030301", "#00d9c0", "#fffff3"],
	television: ["#100f1a", "#ffffff", "#edd912", "#f90a95", "#4084d9"],

	//misc
	alsoAutumn: ["#3e5641", "#a24936", "#d36135", "#282b28", "#83bca9"],
	hotdogs: ["#f8c8d8", "#94505f", "#71895d", "#4b7abc", "#b18e1c"],
	mars: ["#1b0f19", "#9c455e", "#e49169", "#e4b05a", "#def0fc"],
	dragonLand: ["#c03221", "#f7f7ff", "#f2d0a4", "#545e75", "#3f826d"],

	//"Stephen Universe"
	alpine: ["#e0e7f4", "#91c4f4", "#3a3230", "#79605d", "#ae5e74"],
	flowerbed: ["#644a93", "#ca636f", "#92739f", "#bba1bf", "#fbd6dc"],
	iceLake: ["#bc8da7", "#bdb4bf", "#ff8cb2", "#d9d9d9", "#eff2ef"],

	landscape: ["#f992d9", "#372a19", "#637ddc", "#585279", "#fee8f0"],
	aesthetic2: ["#42486f", "#895a89", "#fbcbc6", "#054a71", "#49bbbd"],
	neonNightTown: ["#3a334f", "#ba85cb", "#ec54df", "#7994cb", "#1d5d68"],
	pinkSeaSky: ["#e06c9f", "#f283b6", "#edbfb7", "#b5bfa1", "#6e9887"],
	puppycat1: ["#ffffff", "#ffdecd", "#f9a8a9", "#b569c7", "#88e9ad"],
	purpleDreamLake: ["#5d1c44", "#fec0df", "#7981f1", "#deddfd", "#ffe8f4"],
	stevenBeach: ["#ffe9e2", "#cf789f", "#acb8b4", "#cec8ec", "#fce97b"],
	SubtleStar: ["#d9bbf9", "#aa9fb1", "#edbfb7", "#eeeeee", "#2b2d42"],
	sweetPurples: ["#643a71", "#8b5fbf", "#d183c9", "#e3879e", "#fec0ce"],
	whaletattoo: ["#dccdd0", "#29a1f6", "#485fc1", "#263069", "#ce759b"],



	blueSteel: ["#52d1dc", "#475b5a", "#8d8e8e", "#a3a9aa", "#bbbbbf"],
	sacramente: ["#333333", "#48e5c2", "#fcfaf9", "#f3d3bd", "#5e5e5e"],
	navyBoats: ["#d3d6df", "#676969", "#7db8bc", "#325461", "#c5a248"],

	dirt: ["#93b7be", "#f1fffa", "#d5c7bc", "#785964", "#454545"],
	clay: ["#c0a175", "#1f1e1c", "#fed8c1", "#949b92", "#d3d5bf"],


	sunburn: ["#c90755", "#101756", "#fc9790", "#49bb92", "#fed699"],
	BeachUmbrella: ["#ff8360", "#e8e288", "#7dce82", "#3cdbd3", "#00fff5"],


	earthFlower: ["#f6cadf", "#d0bfb2", "#dad9d7", "#a8c7f5", "#bfb872"],

	pinkBlack: ["#121824", "#2D2636", "#C52A78", "#232442", "#3F414D", "#272E36"],
	blackSkyBluePinkGlow: [
		"#FEF4DD", "#6B83A7", "#C8DCE3", "#BFCCD2", // clothes and human
		"#06246D", "#020645", "#418CCC", "#5582A0", // clothes and human outline?
		"#F9F6F1", "#B4DEEA", "#F0DDEA", "#57AEF5", // fish colors, last one is outline
		"#040406", "#020E48", "#145DA9", "#59ABD5", "#BEF3E6", "#98D1A9", // background gradient
		"#FFFFFF", "#F7D6DF", "#FDF4F0", "#C7A0CF", "#84A6CF", "#D5BADD", "#D94E4F", "#E00F19", "#987D9A", "#CE4274" // ground
	],
	almostRainbow: ["#6399c2", "#685ea6", "#82b387", "#dfe77d", "#7addf3"],
	sweetBeach: ["#86ced4", "#ecbb9b", "#dfdfdd", "#108ab8", "#70b8af"],

	myLittlePony: ["#dbb0ff", "#a5dbf4", "#fb98d9", "#f2ead5", "#7e3d8e"],
	cuteNature: ["#ddd2a4", "#b8bac8", "#f6cacb", "#9f5a33", "#5f2a14"],
	frozenMellowLandscape: ["#d0cdd9", "#fee0d8", "#716f6b", "#9b99aa", "#e1bbaa"],

	//mono
	crystalGems: ["#ffffff", "#f68999", "#8b6497", "#22296d", "#0d0d0d"],
	neonAlpines: ["#ffffff", "#effffa", "#e5ecf4", "#c3bef7", "#8a4fff"],
	neonPinksMaybe: ["#6622cc", "#a755c2", "#b07c9e", "#b59194", "#d2a1b8"],

	pinkBlueBerryForest: ["#f0d3f7", "#b98ea7", "#a57982", "#302f4d", "#120d31"],
	purpleBurglarAlarm: ["#a393bf", "#9882ac", "#73648a", "#453750", "#0c0910"],
	sweetDoomedSunnySky: ["#270722", "#ecce8e", "#dbcf96", "#c2c6a7", "#9ac2c5"],
	sweetPlums: ["#1b065e", "#ff47da", "#ff87ab", "#fcc8c2", "#f5eccd"],
	sweetSkeletons: ["#cebebe", "#ece2d0", "#d5b9b2", "#a26769", "#6d2e46"],
	starsparkle: ["#fef9f5", "#ddc2df", "#fae8d6", "#d8ecdc", "#f9d5b6"],
	linearHades: ["#104547", "#4b5358", "#727072", "#af929d", "#d2d6ef"],
	offPinkBlues: ["#81717a", "#9d8ca1", "#9993b2", "#a7abdd", "#b4d4ee"],
	MothMutedColors: ["#FFE4E1", "#212030", "#3F3B4A", "#4B506E", "#486E6C", "#69B19D", "#F29492", "#552B4C", "#764468", "#939AD7", "#ACC8D1", "#DCB586", "#E4676B", "#995964", "#673E46", "#7F6357", "#A23F44", "#D28167", "#F4BE98", "#A27061", "#98737A", "#F1CDD7"]
}

var palettesArray = []
for (var prop in palettes) {
	palettesArray.push(prop)
}

const renderingStates = {
	INSIDE: 1,
	OUTSIDE: 2,
	FADING: 3
}

const curves = {
	ROCK: 1,
	SAND: 2,
	GROUND: 3,
	DIRT: 4,
	WATER: 5
}

//console.log("renderingstates", renderingStates)

var SetupF = {
	Generic(x, y, speed, speedIcr, angle, dir) {
		this.x = x
		this.y = y
		this.speed = speed
		this.speedIcr = speedIcr
		this.angle = angle
		this.dir = dir
		this.Go = Go //react.Go //
		this.Turn = Turn //react.Turn //
	},
	//Merge all of these, then make prefabs that default values instead
	Example() {
		SetupF.Generic.call(this, 250, 480, 4, -.04, Noise(0.01), dirs.up)
	}, // Rename
	ConstantSpeed() {
		SetupF.Generic.call(this, 250, 460, 4, 0, 0, dirs.up)
	}, // Add parameters for speed, etc
	ConstantSpeedWithBend() {
		SetupF.Generic.call(this, 250, 480, 4, 0, Noise(0.01), dirs.up)
	}, // Add parameters for bending, etc
	SlowingButStraight() {
		SetupF.Generic.call(this, 250, 480, 4, -.04, 0, dirs.up)
	}, // Add aprameters for slowing
	StraightAccelerate() {
		SetupF.Generic.call(this, 250, 480, 0, +.1, 0, dirs.up)
	},
	Avocado() {
		SetupF.Generic.call(this, 50, 50, 1, 0, 0, dirs.down)
	},
	WitnessGrass() {
		SetupF.Generic.call(this, 250, 480, GetRandomRange(1, 3), -.005, 0, dirs.up + Noise(.6))
	},
	StringyGrass() {
		SetupF.Generic.call(this, 250, 480, GetRandomRange(2, 3), 0, 0, dirs.up + Noise(.6))
	},
	Tall() {
		SetupF.Generic.call(this, 100, 160, 4, 0, 0, dirs.up)
	}
}
var ThickF = {
	GetOscilatingFunction(a, b, times) {
		return function (i, length) {
			return Math.sin((i / length * times) * Math.PI) * a + b //Trying to make some even oscilation. Can't remember all maths dough.
		}
	},
	BySpeed(i) {
		return Math.abs(this.speed * 5)
	},
	BySpeedLog(i) {
		return Math.abs(this.speed * this.speed * 3)
	},
	Oscilating6Times(i, length) {
		return Math.sin((i / length * 6) * Math.PI) * 15 + 25 //Trying to make some even oscilation. Can't remember all maths dough.
	},
	CatEye(i, length) {
		return Math.sin((i / length * 1) * Math.PI) * 40 //Trying to make some even oscilation. Can't remember all maths dough.
	},
	NarrowCatEye(i, length) {
		return Math.sin((i / length) * Math.PI) * 10 //Trying to make some even oscilation. Can't remember all maths dough.
	}, // Merge with catEye
	Petal(i, length) {
		return Math.sin((i / length) * Math.PI) * 100 //Trying to make some even oscilation. Can't remember all maths dough.
	},
	Avocado(i) {
		return 10 - 5 * (i / 25)
	}, //Rename / make more generic and useful
	WitnessGrass(i) {
		return 10 - 8 * (i / 100) //replace i/100 with progress or whatever
	}, //Rename / make more generic and useful
	Outline(thicF, outlineThickness) {
		return function (i, length) {
			return thicF(i, length) + outlineThickness
		}
	},
	linear(startSize, endSize) {
		return function (i, length) {
			return lerp(startSize, endSize, i / length)
		}
	},
	quadratic(startSize, endSize) {
		return function (i, length) {
			var thing = (i / length)
			thing *= thing //* thing * thing
			return lerp(startSize, endSize, thing)
		}
	},
	quadratic2(startSize, endSize) {
		return function (i, length) {
			var thing = (i / length) - 1
			thing *= thing
			thing = 1 - thing
			return lerp(startSize, endSize, thing)
		}
	},
	expo(startSize, endSize, exponent) {
		return function (i, length) {
			var thing = (i / length)
			for (var i = 0; i < exponent; i++) {
				thing *= thing
			}
			return lerp(startSize, endSize, thing)
		}
	},
	expo2(startSize, endSize, exponent) {
		return function (i, length) {
			var thing = (i / length) - 1
			for (var i = 0; i < exponent; i++) {
				thing *= thing
			}
			thing = 1 - thing
			return lerp(startSize, endSize, thing)
		}
	}
}
var ColorF = {
	Example(i) {
		return GetRandomColorInGradient("#FFff00", "#00FFFF")
	},
	WitnessGrassColor() {
		var redMin = 250
		var redMax = 255
		var greenMin = 240
		var greenMax = 255
		var blueMin = 50
		var blueMax = 100

		var grayMin = 180
		var grayMax = 240

		var gray = Math.round(GetRandomRange(grayMin, grayMax))


		var red = Math.round(GetRandomRange(redMin, redMax))
		var green = Math.round(GetRandomRange(greenMin, greenMax))
		var blue = Math.round(GetRandomRange(blueMin, blueMax))

		var color = rgbToHex(gray, gray, gray) //rgbToHex(red, green, blue) + "99" //"#FFDD33"
		return color
	},
	StringyGrass() {
		var grayMin = 40
		var grayMax = 240

		var gray = Math.round(GetRandomRange(grayMin, grayMax))

		var color = rgbToHex(gray, gray, gray) //rgbToHex(red, green, blue) + "99" //"#FFDD33"
		return color
	},
	LerpColor(i, total, color1, color2) {
		var progress = i / total
		return lerpColor(color1, color2, progress)
	},
	Petal1(i, total) {
		var color1 = "#AAAAAA"
		var color2 = "#FF0033"
		return ColorF.LerpColor(i, total, color1, color2)
	},
	Petal2(i, total) {
		var color2 = "#450D26"
		var color1 = "#750C2B"
		return ColorF.LerpColor(i, total, color1, color2)
	},
	RandomGradient(i, total) {
		this.color1 = DefaultParameter(this.color1, GetRandomColor()) //Risk: might be a conflict if object already has property color1/color2.
		this.color2 = DefaultParameter(this.color2, GetRandomColor())
		return ColorF.LerpColor(i, total, this.color1, this.color2)
	},
	GetColorGradientFunction(color1, color2) {
		return function (i, total) {
			return ColorF.LerpColor(i, total, color1, color2)
		}
	},
	GetRandomFromPalette(palette) {
		return GetRandomColorInPalette(palette)
	},
	GetRandomColorsFromPalette(palette) {
		return function () {
			return GetRandomColorInPalette(palette)
		}
	},
	GetColorsFromOneRandomlySelectedPalette() {
		let palette = palettes[GetRandomEntryInArray(palettesArray)]
		return function () {
			return ColorF.GetRandomFromPalette(palette)
		}
	},
	GetMetaAsFuck() {
		return function () {
			return ColorF.GetColorsFromOneRandomlySelectedPalette()
		}
	}

}
var ExtraF = {
	WitnessGrass() {
		throw Error()
		if (Math.random() < .2) {
			this.angle -= .002
		}
		if (Math.random() < .2) {
			this.angle += .002
		}
	},
	StringyGrass() {
		if (Math.random() < .5) {
			this.angle -= .005
		}
		if (Math.random() < .5) {
			this.angle += .005
		}

	},
	AlwaysTurning() {
		this.angle += GetRandomRange(-.005, .005)
	},
	TopThing(texture) {
		return function () {
			if (this.i == this.steps - 1) {
				texture = InfiniteFuncValue.call(this, texture)
				Draw.Sprite(this.layer1, texture, this.dir - dirs.up, this.x, this.y, texture.pivotPoint.x, texture.pivotPoint.y, texture.width * .5)
			}
		}
	},
	BranchInterval(texturre, dir, offset) {
		return function () {
			if (this.i % offset == 0) {
				var texture = InfiniteFuncValue.call(this, texturre)
				Draw.Sprite(this.layer1, texture, InfiniteFuncValue.call(this, dir), this.x, this.y, texture.pivotPoint.x, texture.pivotPoint.y, texture.width * .5)
			}
		}
	},
	GenericBranch(probability, dir, texturre) {
		if (IsUndefined(probability)) console.warn("probability is undefined.")
		if (IsUndefined(dir)) console.warn("dir is undefined.")
		if (IsUndefined(texturre)) console.warn("texturre is undefined.")
		return function () {
			if (probability > Math.random()) {
				var texture = InfiniteFuncValue.call(this, texturre)
				if (IsUndefined(texture.pivotPoint)) {
					console.warn("texture.pivotPoint in this context is undefined.")
					console.warn("Here is texture:", texture)
				}

				Draw.Sprite(this.layer1, texture, InfiniteFuncValue.call(this, dir), this.x, this.y, texture.pivotPoint.x, texture.pivotPoint.y, texture.width * .5)
			}
		}
	},
	branchSticks() {
		if (Math.random() < .02) {
			var dir = Math.random() * 2 * Math.PI
			var texturre = GetCraftedLayer(40, SetupF.Example, 10, ColorF.RandomGradient, 1000, ExtraF.branchSticks)
			Draw.Sprite(this.layer1, texturre, dir, this.x, this.y, texturre.pivotPoint.x, texturre.pivotPoint.y, texturre.width * .5)

		}
	},
	Leafing() {
		if (Math.random() < .1) {
			var dir = Math.random() < .5 ? (Math.PI / 4) : (-Math.PI / 4)
			var texturre = GetCraftedLayer(20, SetupF.ConstantSpeed, ThickF.NarrowCatEye, "#00FF0066", 500, ExtraF.AlwaysTurning)
			Draw.Sprite(this.layer1, texturre, dir, this.x, this.y, texturre.pivotPoint.x, texturre.pivotPoint.y, texturre.width * .5)

		}
	},
	FeatherBranch() {
		if (Math.random() < 1) {
			var dir = (Math.PI / 4)
			var texturre = GetCraftedLayer(Math.round(GetRandomRange(20, 40)), SetupF.ConstantSpeed, 1, "#ffffff66", 500, ExtraF.AlwaysTurning)
			Draw.Sprite(this.layer1, texturre, dir, this.x, this.y, texturre.pivotPoint.x, texturre.pivotPoint.y, texturre.width * .5)
			Draw.Sprite(this.layer1, texturre, dir - Math.PI * .5, this.x, this.y, texturre.pivotPoint.x, texturre.pivotPoint.y, texturre.width * .5)


		}
	},
	TallBranch() {
		if (Math.random() < 1) {
			var dir = Math.random() < .5 ? 3 * (Math.PI / 4) : 3 * (-Math.PI / 4)
			var texturre = GetCraftedLayer(Math.round(GetRandomRange(5, 40)), SetupF.Tall, 3, ColorF.GetRandomFromPalette(palettes[palettesArray[60]]), 200)
			Draw.Sprite(this.layer1, texturre, dir, this.x, this.y, texturre.pivotPoint.x, texturre.pivotPoint.y, texturre.width * .5)

		}
	},
}

/* //TODO only load this when you need it, like with a function or sometheng.

*/




var randomShapes = [
    [
        [1, 1, 0],
        [0, 1, 1]
    ],
    [
        [1, 1, 0],
        [1, 1, 1]
    ],
    [
        [1, 1, 1],
        [1, 0, 1]
    ],
    [
        [1, 1],
        [0, 1]
    ]
]
var wtf = 1
//console.log("no consts fuck") //????



const keyCodes = {
	0: 'That key has no keycode',
	3: 'break',
	8: 'backspace',
	9: 'tab',
	12: 'clear',
	13: 'enter',
	16: 'shift',
	17: 'ctrl',
	18: 'alt',
	19: 'pause/break',
	20: 'caps lock',
	21: 'hangul',
	25: 'hanja',
	27: 'escape',
	28: 'conversion',
	29: 'non-conversion',
	32: 'space',
	33: 'page up',
	34: 'page down',
	35: 'end',
	36: 'home',
	37: 'left arrow',
	38: 'up arrow',
	39: 'right arrow',
	40: 'down arrow',
	41: 'select',
	42: 'print',
	43: 'execute',
	44: 'Print Screen',
	45: 'insert',
	46: 'delete',
	47: 'help',
	48: '0',
	49: '1',
	50: '2',
	51: '3',
	52: '4',
	53: '5',
	54: '6',
	55: '7',
	56: '8',
	57: '9',
	58: ':',
	59: 'semicolon (firefox), equals',
	60: '<',
	61: 'equals (firefox)',
	63: 'ß',
	64: '@ (firefox)',
	65: 'a',
	66: 'b',
	67: 'c',
	68: 'd',
	69: 'e',
	70: 'f',
	71: 'g',
	72: 'h',
	73: 'i',
	74: 'j',
	75: 'k',
	76: 'l',
	77: 'm',
	78: 'n',
	79: 'o',
	80: 'p',
	81: 'q',
	82: 'r',
	83: 's',
	84: 't',
	85: 'u',
	86: 'v',
	87: 'w',
	88: 'x',
	89: 'y',
	90: 'z',
	91: 'Windows Key / Left ⌘ / Chromebook Search key',
	92: 'right window key',
	93: 'Windows Menu / Right ⌘',
	95: 'sleep',
	96: 'numpad 0',
	97: 'numpad 1',
	98: 'numpad 2',
	99: 'numpad 3',
	100: 'numpad 4',
	101: 'numpad 5',
	102: 'numpad 6',
	103: 'numpad 7',
	104: 'numpad 8',
	105: 'numpad 9',
	106: 'multiply',
	107: 'add',
	108: 'numpad period (firefox)',
	109: 'subtract',
	110: 'decimal point',
	111: 'divide',
	112: 'f1',
	113: 'f2',
	114: 'f3',
	115: 'f4',
	116: 'f5',
	117: 'f6',
	118: 'f7',
	119: 'f8',
	120: 'f9',
	121: 'f10',
	122: 'f11',
	123: 'f12',
	124: 'f13',
	125: 'f14',
	126: 'f15',
	127: 'f16',
	128: 'f17',
	129: 'f18',
	130: 'f19',
	131: 'f20',
	132: 'f21',
	133: 'f22',
	134: 'f23',
	135: 'f24',
	144: 'num lock',
	145: 'scroll lock',
	160: '^',
	161: '!',
	162: '؛ (arabic semicolon)',
	163: '#',
	164: '$',
	165: 'ù',
	166: 'page backward',
	167: 'page forward',
	168: 'refresh',
	169: 'closing paren (AZERTY)',
	170: '*',
	171: '~ + * key',
	172: 'home key',
	173: 'minus (firefox), mute/unmute',
	174: 'decrease volume level',
	175: 'increase volume level',
	176: 'next',
	177: 'previous',
	178: 'stop',
	179: 'play/pause',
	180: 'e-mail',
	181: 'mute/unmute (firefox)',
	182: 'decrease volume level (firefox)',
	183: 'increase volume level (firefox)',
	186: 'semi-colon / ñ',
	187: 'equal sign',
	188: 'comma',
	189: 'dash',
	190: 'period',
	191: 'forward slash / ç',
	192: 'grave accent / ñ / æ / ö',
	193: '?, / or °',
	194: 'numpad period (chrome)',
	219: 'open bracket',
	220: 'back slash',
	221: 'close bracket / å',
	222: 'single quote / ø / ä',
	223: '`',
	224: 'left or right ⌘ key (firefox)',
	225: 'altgr',
	226: '< /git >, left back slash',
	230: 'GNOME Compose Key',
	231: 'ç',
	233: 'XF86Forward',
	234: 'XF86Back',
	235: 'non-conversion',
	240: 'alphanumeric',
	242: 'hiragana/katakana',
	243: 'half-width/full-width',
	244: 'kanji',
	251: 'unlock trackpad (Chrome/Edge)',
	255: 'toggle touchpad',
};

const keyValues = getSwappedKeyValue(keyCodes)

const keyLocations = {
	0: 'General keys',
	1: 'Left-side modifier keys',
	2: 'Right-side modifier keys',
	3: 'Numpad',
};


var bodyDimensions = {
	body1: {
		height: 50,
		shoulderWidth: 10,
		hipWidth: 27,
		legLength: 37,
	},
	body2: {
		height: 50,
		shoulderWidth: 20,
		hipWidth: 27,
	}
}
var LimbDimensions = {
	head1: {
		headYOffset: "-height - 5", //TODO: I need a way to define relative positions. the alternatives seems to be strign or function. if function ()=>{-get(context, value) -5}
		headR: 7.5,
		headColor: "#444444"
	},
	head2: {
		headYOffset: "-height - 5",
		headR: 10,
		headColor: "pink"
	},
	leg: {
		legLength: 37,
	}
}

var limbs = {
	leg1: {
		stepLength: "body.width", // why tho?
		footReach: length // aka leg length //Circle: new Circle(this.x, this.y, this.legLength, "red", 1)
	}
}
