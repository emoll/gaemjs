//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* GenerateNature.js */

/* This file only serves the purpose of running the game Generate Nature
All content in this file should only be what makes this game project and calls to external files that makes this game run.
Anything than be used generically for other projects should be externalized from this file.
*/


/* Clean up todo
    Find a good place for the temporarySprite that is used for creating plants.
        does it need to be in global scope? No, but it needs to be shared by all plant nodes, and possibly other things as well.
        Think of it as a singleton that the browser can use in order to not put more things on the heap? I think?
        Find out the actual purpose of temporary sprite.
    Come up with a good method for performing ab performance tests and keep both algorithms parrallelly.
    sync with git and make sure the git server exists
    Set clearer boundaries between Engine, Draw, Utils and Game Objects.
    Separate Old game objects to it's own file
    Decide wether or not to fully incorporate behaviour.js or to keep things in their own objects.
    Find a way to dump the content of a function inside a for loop. basically find a way to compose a function with the content of other function.
   make subscriber pattern that works with google compiler
*/


//var player // used by house proximity and by grass bending....
//var house
//var windows = new e.botw.RoundWindow(200, -200, 50)

/*
var botw = function () {
    //    //water reflection stuff
    //    var reflectionLayer = CreateNewLayer(canvas.width, canvas.height)
    //    var reflectionPattern
    //     //water reflection stuff
    //    curveLayers = WorldSets["beach"]
    curveLayers = WorldSets["snow"]

    //    house = new e.botw.Compartment(512, 600, 536, 300)
    //    house.AddWallPiece("exterior", new e.botw.Door(0, 0, 80, 150))
    //    house.AddWallPiece("exterior", windows)
    player = new e.botw.Player(200, 200)
    Curves.Ground = function () {
        return Curves.flat() + 70
    }
    StartEngine()
}
*/

function DrawHouser() {
	this.x = 0
	this.y = 0
	this.x2 = 10
	this.y2 = 10
	this.minWidth = 50
	this.minHeight = 50

	this.Update = function () {
		Math.seedrandom(seedForRandomness)
		if (mouse.left) {
			if (mouse.left != this.lastMouse) {
				this.x = mouse.x
				this.y = mouse.y
			}
			this.x2 = mouse.x
			this.y2 = mouse.y
		}
		this.lastMouse = mouse.left
	}
	this.Draw = function () {
		var minX = this.x < this.x2 ? this.x : this.x2
		var minY = this.y < this.y2 ? this.y : this.y2
		var maxX = this.x > this.x2 ? this.x : this.x2
		var maxY = this.y > this.y2 ? this.y : this.y2

		var width = maxX - minX < this.minWidth ? this.minWidth : maxX - minX
		var height = maxY - minY < this.minHeight ? this.minHeight : maxY - minY

		var texture = CreateNewLayer(width, height, 0, 0)
		Textures.BrickTexture(texture)
		Draw.Sprite(defaultLayer, texture, 0, minX, minY, 0, 0)
	}
	entities.push(this)
}
var d = new DrawHouser()

function Play(game) {
	game()
}

//Play(botw)
//make("botw", "is", "run")
//console.log("maked")
