//Advance Wars basic features:
console.log("Advance Wars Initiate?")

var testUnitMap = "ooooooooooioooioooooooooo"
var testGroundMap = "ssssssssssffffftttttttttt"

var s = {
	advancewars:{
		is:{
			videoGame:1,
		},
		// competitive
		// strategy
		// turn-based
	},
	teams:{ // teams and factions
		// identifiable and discernable.
		//		goals: archetypal yet complementary colors
	},
}

// The hardcode stuff
var teams = {
	0: {
		style: {
			fillStyle: "LightSlateGray", //"silver",
		}
	},
	1: {
		style: {
			fillStyle: "OrangeRed" //"crimson" //"brown", //"maroon" //"red"
		}
	},
	2: {
		style: {
			fillStyle: "RebeccaPurple" //"Purple" //
		}
	},
	3: {
		style: {
			fillStyle: "saddlebrown" //"chocolate"

		}
	},
	4: {
		style: {
			fillStyle: "seagreen" //"DarkOliveGreen" //"cadetBlue" //"crimson" //"brown", //"maroon"
		}
	},
}

var unitTypes = {
	infantry: {
		//p1 - 5 minutes
		sprite: "triangle",
		cost: 100,
		steps: 5,
		damage: 50, // (determined procedurally with weapon type and health)

		//position(this is part of map though not enity)

		movementType: 0, //1 unique movementType per entity
		weaponType: 0, //[] //1 or 2 unique weaponType per entity
		armorType: 0, //weaknesses and strengths and such

		vision: 5,

		range: 1, //(default 1)
		loadRounds: 0, //how many turns standing still before it can perform attack
		flags: { //or special properties
			canCarry: 1, //array of unit types it can carry
			canSupply: 1,
			canGoUnderWater: 1,
		},
		spriteSet: {},
		sounds: {},
	},
	worm: 1,
	fly: 1,
	spider: 1,
	blob: 1,
}

var weaponTypes = { // what is it strong and not strong against?
	sword: {
		damage: .75,
		maxAmmo: 0 //zero means infinite
	},
	gun: 1,
	StrongGun: 1,
	cannon: 1,
	dropBomb: 1
}

var movementTypes = { //cost per underground material (one per each groundType)
	legs: 1,
	wheels: 1,
	rollers: 1,
	wings: 1,
	propeller: 1,
}

var armorTypes = { //maybe these should be per unit instead. They define general defense multiplier and specific multipliers against other movement or armor groups.
	infantry: 1,
	car: 1,
	tank: 1,
	bigTank: 1,
	//???
	blob: 1,
	skin: 1,
	fur: 1,
	shell: 1,
	//???
	cloth: 1,
	steel: 1,
}

var bodyTypes = {
	tiny: 1,
	slanky: 1,
	normal: 1,
	thick: 1,
	massive: 1
}

var factorySet = {
	none: 0,
	allInclusive: ["infantry", "worm", "fly", "spider", "blob"]
}

var groundTypes = { //terrain
	sand: {
		color: "Moccasin", //"sandyBrown", //"Wheat", //"LightGoldenRodYellow",//"LemonChiffon", //"Khaki","Bisque", "yellow",    PaleGoldenRod  AntiqueWhite  LemonChiffon 
		defense: 1
	},
	forest: {
		color: "MediumSeaGreen", //"LightGreen", //"SpringGreen", //"forestGreen", //"DarkSeaGreen", //"DarkOliveGreen",   "OliveDrab", "Orange", "Olive", "YellowGreen", "GreenYellow",
		defense: 2
	},
	//buildings
	town: {
		color: "LightSlateGray", //"silver", //TODO: use team color!
		defense: 3,
		takeOverable: true,
		builds: factorySet.none
	},
	water: {
		color: "LightSeaGreen", // LightSkyBlue MediumSpringGreen PaleTurquoise LightBlue LightSeaGreen  MediumAquaMarine, "blue",
		defense: 1
	}
}

var weatherModifiers = {
	wind: 1,
	rain: 1,
	snow: 1,
}

var MovementCostCalculator = function () { //	Movement/obstruction calculator.
	//	obstruction(you can't land on other's vehicles, you can only pass through your own or your team's units)
}
var groundCostForUnitType = function (unitType, ground) {
	return movementTypes[unitType.movementType][ground].cost
}
var groundCostForUnit = function (unit, ground) {
	return groundCostForUnitType(unit.type, ground)
}
var stepCost = function (unit, pos) {
	return groundCostForUnit(unit, getPosData(pos).ground)
}

var TheAdvanceWarsDamageFormula = function () {
	//    B = Base damage(found in the damage chart here)
	//    ACO = Attacking CO attack value(example: 130
	//        for Kanbei)
	//    R = Random number 0 - 9(Luck)
	//    AHP = HP of attacker(the displayed number from 1 through 10)
	//    DCO = Defending CO defense value(example: 80
	//        for Grimm)
	//    DTR = Defending Terrain Stars(e.g.plain = 1, wood = 2)
	//    DHP = HP of defender(the displayed number from 1 through 10)

	//    Damage % = Actual damage, expressed as a percentage
	var damage = (B * ACO / 100 + R) * (AHP / 10) * (200 - (DCO + DTR * DHP) / 100)
	return damage
}
var attackDamageInteractionCalculator = function () { //Attacking/damage system
	//First attack bonus
}
var FireExchange = function (attacker, attacked) {
	//Fire exchange can be a complicated simulation. Let's keep it simple.
	attacked.health -= attacker.damage * attacker.health
	attacker.health -= attacked.damage * attacker.health
	//	p1 = attacker
	//	p2 = attacked
	//    
	//    //First interaction - p1 shoots 
	//    p2.health = p1.health * p1.attackPower * p2.responseDelay
	//    
	//    //Second interaction - cross fire
	//    for()
	//    p1.helth = p2.
}

var symbolToGround = {
	s: "sand",
	f: "forest",
	t: "town",
}
var symbolToUnitType = {
	o: false,
	i: "infantry",
	w: "worm",
	f: "fly",
	s: "spider",
	b: "blob",
}

var getPosData = function (pos) {
	var ground
	var unit
	for (var i = 0; i < unitList.length; i++) {
		if (unitList[i].posX === pos.x && unitList[i].posY === pos.y) {
			unit = unitList[i]
			break;
		}
	}
	return {
		ground: ground,
		unit: unit
	}
}

//????
var COs = {

}
var CO = {}

//End of The hard code stuff


//LISTS TO BE POPULATED
var currentPlayer = { //TEMP
	team: 1
}

var map = {
	width: 5,
	height: 5,
	unitLayer: [], //- 20 minutes
	//(2d grid with positions filling pointers to existing units)
	groundLayerData: testGroundMap,
}

var unitList = [] // the list holding all unit objects
playerUnits = {
	active: [],
	done: []
}
//END OF LISTS TO BE POPULATED


//UNIT TESTINT LOL
var mapDataUnitTest = function (data) {
	if (data.length !== map.width * map.height) {
		console.log("map data is size:", data.length, "should be size:", map.width * map.height)
		return false
	}


	console.log("map data test successful")
	return true
}
//my tests
mapDataUnitTest(testUnitMap)
mapDataUnitTest(testGroundMap)
//todo tests
//END OF UNIT TESTING LOL


//GENERATE STUFF
//populate unitList
for (var i = 0; i < testUnitMap.length; i++) {
	//    if (testUnitMap[i] === 'o') continue
	var unitType = symbolToUnitType[testUnitMap[i]]
	if (!unitType) {
		console.log("no such unit type")
		continue
	}
	var x = i % map.width
	var y = Math.floor(i / map.width)

	var unit = {
		uniqueID: unitList.length,
		team: 1,
		type: unitType,
		health: 100,
		fuel: 10,
		movesLeft: 5,
		posX: x,
		posY: y,
	}
	unitList.push(unit)
}



//PATHING STUFF
//live tempdata when moving a character.
var unitTempPosition = {
	x: 0,
	y: 0
}
var pathPointsOnGrid = [] // contains {x,y} coordinates
var unitPath = []
var unitPathCost
var selectedUnit
var prepath = function (dir) {
	if (dir === oppositeDir(unitPath[unitPath.length - 1])) {
		unitPath.pop()
	} else {
		//Enough 
		//		var globalPos
		//		selectedUnit.movesLeft >= stepCost(selectedUnit, globalPos)
		unitPath.push(dir)
		return true
	}
	return false
}
//END OF PATHING STUFF








var online = {
	//ways to do online
	// have the same seed random gen on server and client
	// in order to show the movements instantly
	// or only have random on the server for no cheating with randomness predictions.
	pushMove() { //every move is iretractable and is pushed to the server.
		//        server.push
		//        server.wait for a response.
		// if no response do some connection error messages.
		//        saving to online
		//server should only respond to the user in control.
	}
}


var gridPosToScreenArea = function (x, y, spacing2) {
	if (isNaN(x)) console.log("warning, x is nan")
	if (isNaN(y)) console.log("warning, y is nan")
	console.log(x, y)
	return new Geometry.rectangle(spacing + x * (tileSize), spacing + y * tileSize, tileSize - spacing * 2, tileSize - spacing * 2)
}
var gridPosToScreenPos = function (x, y) {
	return {
		x: (spacing + x * (tileSize)) + .5 * (tileSize - spacing * 2),
		y: spacing + y * tileSize + .5 * (tileSize - spacing * 2)
	}
}

var drawAllUnits = function () {
	for (var i = 0; i < unitList.length; i++) {
		DrawUnit(unitList[i])
	}
}
var DrawUnit = function (unit) {
	var drawingArea = gridPosToScreenArea(unit.posX, unit.posY)
	//TODO: draw healthbar
	console.log(unit)
	console.log("drawing area", drawingArea)
	Draw[unitTypes[unit.type].sprite](null, drawingArea, teams[unit.team].style)
	//TODO: draw 

}
var drawPath = function () {
	var pathScreenPositions = []
	for (var i = 0; i < pathPointsOnGrid.length; i++) {
		pathScreenPositions.push(gridPosToScreenPos(pathPointsOnGrid[i]))
	}
	var pathStyle = {
		fillStyle: "#00000000",
		lineWidth: 10,
		strokeStyle: "#ffffff66",
	}
	Draw.Points(null, pathScreenPositions, pathStyle)
}









var selection = {
	x: 0,
	y: 0
} //rename to gameGridSelection or 



var selectionStyle = {
	fillStyle: "#00ff0000",
	strokeStyle: "#FFFFFF",
	lineWidth: 4,
}
var selectionState //...


//Player actions lol
var nextUnit = function () {
	//check if you are allowed to switch (see how advance wars does it, if it cancels any current path or menu selection)
	//go to next unit in the unit index
	//move the marker to next unit?
	//Select new unit????
}
var selectNextUnit = function () {

}

var select = function (dir) {
	if (selectionState === "menu") {
		//code... //this is a bad way to do I don't like these if statements you know.
		return
	}
	//updateposition
	if (dir === "up") selection.y -= 1 % map.width
	if (dir === "right") selection.x++
	if (dir === "left") selection.x--
	if (dir === "down") selection.y++

	g.Draw() //lazy redraw
	drawAllUnits()

	//update graphics
	var rect = gridToSmallScreenArea(selection.x, selection.y)
	Draw.Rectangle(null, rect, selectionStyle)
	if (selectionState === "pathing") {
		if (prepath(dir)) { //this if statement feels bootleg
			pathPointsOnGrid.push(selection)
		}
	}
}
var confirm = function () {
	//attempting to select:
	var posData = getPosData(selection)

	if (posData.unit && posData.unit.team === currentPlayer.team) {
		selectUnit(posData.unit)
		startPathing()
	}
}

//Indirect actions lol
var selectUnit = function (unit) {
	console.log(unit, "selected")
}
var deSelectUnit = function (unit) {
	//Deselect...
	//	remove any selection graphics
	//	clear any arrows paths or menus on current selection
}
var startPathing = function () {
	selectionState = "pathing"

}

var PerformMove = function () {
	//simulate the attacks/interactions
	//    move player unit from active to done
}
var endTurn = function () {

}

var newTurn = function () {
	//	reset used units on new turn - 2 minutes (loop through and change status)
	//    income per turn
}



var gridToSmallScreenArea = function (x, y) {
	var spacing = 4
	if (isNaN(x)) console.log("warning, x is nan")
	if (isNaN(y)) console.log("warning, y is nan")
	console.log(x, y)
	return new Geometry.rectangle(spacing + x * (tileSize), spacing + y * tileSize, tileSize - spacing * 2, tileSize - spacing * 2)
}



//PLAYER INPUT AND EVENT LISTENING
this.ControlOn = function (event) {
	if (event.repeat) return
	//event.code is generated by the browser use console.log(event) to figure out the right string
	if (event.code === "KeyQ") nextUnit()
	if (event.code === "KeyZ") confirm()
	if (event.code === "KeyX") goBack()
	if (event.code === "Space") confirm()
	if (event.code === "ArrowUp") select("up")
	if (event.code === "ArrowDown") select("down")
	if (event.code === "ArrowRight") select("right")
	if (event.code === "ArrowLeft") select("left")
	//                if (event.which == 37) this.Control("WalkLeft") // Left Arrow
	//                if (event.which == 39) this.Control("WalkRight") // Right Arrow
}
this.ControlOff = function (event) {
	//    if (event.code === "ArrowRight") this.walkRight = 0
	//    if (event.code === "ArrowLeft") this.walkLeft = 0
	//    if (event.code === "KeyX") this.plant = false
}

addEventListener("keydown", getFunctionCalledThroughTarget(this, this.ControlOn, Event))
addEventListener("keyup", getFunctionCalledThroughTarget(this, this.ControlOff, Event))

//random drag and drop test
new Draggable.Draggable(document.querySelectorAll('ul'), {
	draggable: 'li'
})
//END OF PLAYER INPUT AND EVENT LISTENING

//DRAW THE GAME
var g = new Grid(map.width, map.height)
g.Draw()
drawAllUnits()

//theoretical properties of units
var theoreticalPropertiesForUnits = {
	// movement/evassion type.
	// target size
	// weapon type and weapon size (cannon, 20x bigger cannon, etc)
	// defence type (scales, metal, skin, uh whatevers.)
	// visibility (cammoflague?????)
	// vision length
}

//ideas
//building bridges on demand...
//
//advance wars but with construction.... hmm.
//	that would be kinda interesting video game.
//
//
//
//be brilliant in logistics.
//
//
//
//complex information manufacturing


///

//the advance wars turn based game video game editor sort of thing
//	that works server side etc....
//
//
//
//advance wars todo:
//	google authentication
//	the server side
//	php????
//	
//
//
//
//cool ideas
//	you plant like wheat fields and forests and shit
//	maybe a futuristic version of advancewars?
//	I really want the abstract advance wars where you see all the stuff directly
//
//
//
//
//should I make advancewars in like html instead of canvas?
//	it probably make it easier to scroll the window on mobile etc.
//	just must be sure that zoom in doesn't fuck with the GUI
//
//
//
//
//
//what am I dreading about advance wars?
//	spending many months and not getting a working product.
//	
//
//
//replay what happened this turn.
//	history view where you can go back every single step
//		a view where you see the lines of actions so it's easy to understand 
//			without the animation movement
//
//
//look for source code of advance wars 2
//
//otherwise grab the data of listed stuff about advance wars
//
//
//
//hq districts idea
//		
//	when you lose a hq you lose the power of anything created by that district!
//
//	so instead of losing the battle your district becomes theirs.