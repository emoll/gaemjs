//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* Draw.js */

/* File content description:
	
	Functions that defines shapes and prints visual patterns on surfaces
*/
/* Todo:
	There are too many things that exists to make circles. Reduce it to one thing.
	Create specific areas for concern. One for shapes one for drawing one for sprites or whatever
	Make it more clear how layers work and what they are
	Implement camera/viewport rendering
*/

/* words
context, layer, sprite, texture, image, plane
Layer is a generic term for printable surface
texture is a layer with pixeldata
sprite is a texture used for rendering an object
Layer is the more generic term and works best in most cases
Draw, render, print,
*/


var Textures = { // Patterns and gradients <3
	VerticalPlankTexture: function (texture, plankWidth = 25, padding = 10, spacing = 16, lastPlankStyle = "crop", plankColor = "#cc7755", backgroundColor = "#aa6655") {
		DrawRect(texture, 0, 0, texture.width, texture.height, backgroundColor)
		var i = padding
		var l = texture.width - padding - plankWidth

		for (i; i < l; i += plankWidth + spacing) {
			DrawRect(texture, i, padding, plankWidth, texture.height - 2 * padding, plankColor, 1)
		}
		if (lastPlankStyle === "crop") {
			console.log(i - l)
			DrawRect(texture, i, padding, l + plankWidth - i, texture.height - 2 * padding, plankColor, 1)
		}
	},
	//    HorizontalPlankTexture,
	BrickTexture: function (texture, brickWidth = 25, brickHeight = 14, padding = 3, spacing = 3, lastBrickStyle = "crop", brickColor = "#6699ff", backgroundColor = "#999999") {
		//nice brick colors: "#ff4422" "#6699ff" "#339999" "#ff0000"
		Draw.Rect(texture, 0, 0, texture.width, texture.height, backgroundColor)
		var x = padding
		var y = padding
		var l = texture.height - padding //this draws one brick too mane too the right because bricks looked good going out the edges..
		var ll = texture.width - padding //this draws one brick too mane too the right because bricks looked good going out the edges..
		var i = 0

		for (y; y < l; y += brickHeight + spacing + Math.random()) {
			x = i % 2 ? padding : -.5 * brickWidth
			for (x; x < ll; x += brickWidth + spacing + Math.random()) {
				//                DrawRect(texture, x, y, brickWidth, brickHeight, brickColor, 1)
				Draw.Rect(texture, x + Math.random(), y + Math.random() - .5, brickWidth + Math.random(), brickHeight + Math.random(), changeHue(brickColor, Math.random() * 15), 1)
			}
			i++
		}
	},
}

var styles = {
	default: {
		// Compositing
		globalAlpha: 1, // Alpha value that is applied to shapes and images before they are composited onto the canvas. Default 1.0 (opaque).
		globalCompositeOperation: 0, // With globalAlpha applied this sets how shapes and images are drawn onto the existing bitmap.

		// Fill and stroke styles
		// Fill styling is used for colors and styles inside shapes and stroke styling is used for the lines around shapes.
		fillStyle: 0, //Color or style to use inside shapes. Default #000 (black).
		strokeStyle: "#0000", //Color or style to use for the lines around shapes. Default #000 (black).

		// Line styles
		// The following methods and properties control how lines are drawn.
		lineWidth: 0, //Width of lines. Default 1.0.
		lineCap: 0, //Type of endings on the end of lines. Possible values: butt (default), round, square.
		lineJoin: 0, //Defines the type of corners where two lines meet. Possible values: round, bevel, miter (default).
		miterLimit: 0, //Miter limit ratio. Default 10.
		//setLineDash()

		// Text styles
		// The following properties control how text is laid out.
		font: 0, // Font setting. Default value 10px sans-serif.
		textAlign: 0, // Text alignment setting. Possible values: start (default), end, left, right, center.
		textBaseline: 0, // Baseline alignment setting. Possible values: top, hanging, middle, alphabetic (default), ideographic, bottom.
		direction: 0, // Directionality. Possible values: ltr, rtl, inherit (default).
	},
	thick: {
		//is about outlines and maybe minimum width/height 
	},
	thin: {},

}

function GenericFinalDrawCallThing(ctx) {
	//    if(ctx.fillStyle) {}// I don't think I even need to check because js does the checks deeper anyways
	ctx.closePath();
	ctx.fill()
	ctx.stroke()
}
var Draw = { //Draw in this context means Put on a texture
	//TODO: define standard interface for all draw.
	// I think this is good one: target, substance, style
	//area is the place within the thing is allowed to be drawn. it's like the size of the geomtry
	//I should essentially have a function that can take a set of parameters and figure out what geometry it is.
	//like if it has x, y, side, it is probably square, it could be circle., if it has width and height, it is a rectangle.
	Geometry: function (target, area, geometry, style) {
		Draw[geometry.target](target, geometry, style)
	},
	Circle: function (target, circle, style = 0) {
		//extract context
		var ctx = GetContext(target) //this is specifically canvas context

		//set style parameters
		if (isNaN(style))
			Object.assign(ctx, style) //trafsers properties from styles[style] to ctx
		else
			Object.assign(ctx, styles[style])
		//        ctx.beginPath() // I'm not sure if this line is needed in order to make circle and not fuck other stuff up.
		ctx.arc(circle.x, circle.y, circle.r, 0, 2 * Math.PI)
		GenericFinalDrawCallThing(ctx)
	},
	rectangleRequirements: [
		"target",
		"rectangle",
		"style"
	],
	rectangle: function (target, rect, style) {
		if (!target) target = defaultLayer
		var ctx = GetContext(target)

		ctx.beginPath() //because else this shit overrides the style of previous draw calls lol.

		//set style parameters //todo maybe move this to GenericFinalDrawCallThing to avoid copy paste.
		if (styles[style]) style = styles[style] //transfers properties from styles[style] to ctx (I think these are predesigned styles)
		Object.assign(ctx, style)
		//maybe uh reset style with a ctx save thing to not ruin everything with an obscure property set onto the context forever.

		ctx.rect(rect.x, rect.y, rect.width, rect.height)
		GenericFinalDrawCallThing(ctx)
	},
	Rect: function (...args) {
		Draw.Rectangle(...args)
	},
	triangle: function (target, area, style) { //Using rect area because it's easier and it's cool to define something with another shape lol

		//Defining the most easy to define standard triangle in a square. (its not Equilateral(liksidig) triangle because math)
		var points = []
		points.push({
			x: area.x + .5 * area.width,
			y: area.y
		})
		points.push({
			x: area.x,
			y: area.y + area.height
		})
		points.push({
			x: area.x + area.width,
			y: area.y + area.height
		})


		Draw.Points(target, points, style)

	},
	Points: function (target, points, style) {
		if (!target) target = defaultLayer
		var ctx = GetContext(target)

		ctx.beginPath() //because else this shit overrides the style of previous draw calls lol.

		ctx.moveTo(points[0].x, points[0].y)
		for (var i = 1; i < points.length; i++) { //starts at 1 because of the moveTo
			ctx.lineTo(points[i].x, points[i].y)
		}

		//set style parameters //todo maybe move this to GenericFinalDrawCallThing to avoid copy paste.
		if (styles[style]) style = styles[style] //transfers properties from styles[style] to ctx (I think these are predesigned styles)
		Object.assign(ctx, style)


		GenericFinalDrawCallThing(ctx)
	},

	Sprite: function (targetLayer, image, angleInRad, positionX, positionY, axisX, axisY) {
		var targetContext = GetContext(targetLayer)
		var _roundImages = false
		if (_roundImages) {
			positionX = Math.round(positionX)
			positionY = Math.round(positionY)
			axisX = Math.round(axisX)
			axisY = Math.round(axisY)
		}
		targetContext.save() //added
		if (positionX != 0 && positionY != 0)
			targetContext.translate(positionX, positionY)
		if (angleInRad != 0)
			targetContext.rotate(angleInRad)
		targetContext.drawImage(image, -axisX, -axisY)
		targetContext.restore() //added
	},
	Text: function (x, y, text, color, size) {
		var targetContext = GetContext(targetLayer)
		targetContext.font = size + "pt Calibri"
		targetContext.fillStyle = color
		targetContext.fillText(text, x, y)
	},
	Smiley: function (targetLayer) {
		var targetContext = GetContext(targetLayer)
		targetContext.globalAlpha = .5

		targetContext.fillStyle = GetRandomColor()
		targetContext.beginPath()
		targetContext.arc(75, 75, 30, 0, Math.PI * 2, true) // Outer circle
		targetContext.fill()
		targetContext.closePath()

		targetContext.globalAlpha = 1
		targetContext.beginPath()
		targetContext.moveTo(110, 75)
		targetContext.arc(75, 75, 25, 0, Math.PI, false) // Mouth
		targetContext.closePath()
		targetContext.fillStyle = "#000"
		targetContext.fill()

		targetContext.beginPath()
		targetContext.moveTo(65, 65)
		targetContext.arc(60, 65, 5, 0, Math.PI * 2, true) // Left eye
		targetContext.moveTo(95, 65)
		targetContext.arc(90, 65, 5, 0, Math.PI * 2, true) // Right eye
		targetContext.closePath()
		targetContext.fill()
	},
}

//FILTERS AND STUFF
//Search tags: transparency, erase, translusence, opacity
function SubtractTexture(targetLayer, texture, offsetX, offsetY) { //todo reread convention between target and texture
	offsetX = DefaultParameter(offsetX, 0)
	offsetY = DefaultParameter(offsetY, 0)
	targetLayer.context.globalCompositeOperation = 'destination-out'
	Draw.Sprite(targetLayer, texture, 0, offsetX, offsetY, texture.width * .5, texture.height * .5, texture.width * .5)
	targetLayer.context.globalCompositeOperation = 'source-over' // default
	return targetLayer
}

function DrawFilteredVersionOnSelf(image, filter, value) { // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/filter
	var newImage = CreateNewLayer(image.width, image.height, image.pivotPoint)
	image.context.filter = filter + "(" + value + ")"
	image.context.drawImage(image, 0, 0)
	image = newImage
}

function GetFilteredImage(image, filter, value) { // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/filter

	//Examples of Filters!
	//textures = GetFilteredImage(textures, "contrast",		"500%")
	//textures = GetFilteredImage(textures, "saturate",		"500%")
	//textures = GetFilteredImage(textures, "brightness",	"100%")
	//textures = GetFilteredImage(textures, "blur",			"10px")
	//textures = GetFilteredImage(textures, "hue-rotate",	"50deg")
	//textures = GetFilteredImage(textures, "invert",		"100%")
	//textures = GetFilteredImage(textures, "brightness",	"10%")
	//textures = GetFilteredImage(textures, "opacity",		"50%")

	var filterString = filter + "(" + value + ")"
	var newImage = CreateNewLayer(image.width, image.height, image.pivotPoint)

	console.log(filterString)
	newImage.context.filter = filterString
	newImage.context.drawImage(image, 0, 0)
	return newImage
}

function GetGlowFilteredImage(image, value) { // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/filter
	var blurImage = CreateNewLayer(image.width, image.height, image.pivotPoint)
	var finalImage = CreateNewLayer(image.width, image.height, image.pivotPoint)

	blurImage.context.filter = "blur(" + value + "px)"
	blurImage.context.drawImage(image, 0, 0)

	finalImage.context.drawImage(blurImage, 0, 0)
	finalImage.context.drawImage(image, 0, 0)
	return finalImage
}

function GetMergedImage(image1, image2) {
	var newImage = CreateNewLayer(image1.width, image1.height, image1.pivotPoint)
	newImage.context.drawImage(image1, 0, 0)
	newImage.context.drawImage(image2, 0, 0)
	return newImage
}

function GetScaledLayer(texture, scale) { //Use this as default when setting height and width on layers, because chrome clears the texture if you change width or height of canvas
	width = texture.width * scale
	height = texture.height * scale
	pivotPoint = texture.pivotPoint * scale

	var newImage = CreateNewLayer(width, height, pivotPoint)
	newImage.context.scale(scale, scale)
	newImage.context.drawImage(texture, 0, 0)
	return newImage
}

// END OF FILTERS AND STUFF

//check for negative radius ... lol
//if (radius < 0) {
//    radius = Math.abs(radius)
//    console.warn("circleGeometry.radius is negative, inverting it with abs for continuation.")
//}

var positions = {
	center: {
		x: 1920 / 2,
		y: 1080 / 2
	}
}

function makeRocks() {
	// flood fill shapes
	var a = [[], []]
	var offsetX
	var offsetY

	var lowerThreshhold = .85
	var upperTreshhold = 1

	var steps = 0
	var stepsFilled0 = 0
	var stepsFilled1 = 0
	var stepsOther = 0
	var noiseF = Curves.RockMaker


	var boundaries = {
		minX: 0,
		minY: 0,
		maxX: 0,
		maxY: 0,
	}


	function floodFill2DNoise() {

		var tries = 0
		var noise = 0
		//Find a pixel
		while (noise < lowerThreshhold || noise > upperTreshhold) {
			tries++
			offsetX = Math.random() * 100000000000
			offsetY = Math.random() * 100000000000
			noise = noiseF(offsetX, offsetY)
		}

		console.log("attemps to find artifact:", tries)
		console.log(offsetX, offsetY)


		//flood_fill
		flood_fill(0, 0)
		console.log("fill steps", steps)
		console.log("filled 0", stepsFilled0)
		console.log("filled 1", stepsFilled1)
		console.log("neither", stepsOther)
		console.log(a)
		console.log("wa", Object.keys(a))

		console.log(boundaries)

		return a
	}

	function flood_fill(pos_x, pos_y) {
		steps++
		if (typeof a[pos_x] === 'undefined') a[pos_x] = []
		if (a[pos_x][pos_y]) {
			stepsOther++
			return
		}

		var noise = noiseF((pos_x + offsetX), (pos_y + offsetY))

		if (noise < lowerThreshhold || noise > upperTreshhold) {
			stepsFilled0++

			a[pos_x][pos_y] = -1
			//        console.log("no")
			return
		}

		stepsFilled1++

		if (pos_x < boundaries.minX) boundaries.minX = pos_x
		if (pos_y < boundaries.minY) boundaries.minY = pos_y
		if (pos_x > boundaries.maxX) boundaries.maxX = pos_x
		if (pos_y > boundaries.maxY) boundaries.maxY = pos_y

		a[pos_x][pos_y] = noise //normalize(noise, lowerThreshhold, upperTreshhold)

		flood_fill(pos_x + 1, pos_y) // then i can either go south
		flood_fill(pos_x - 1, pos_y) // or north
		flood_fill(pos_x, pos_y + 1) // or east
		flood_fill(pos_x, pos_y - 1) // or west
	}



	function DrawPixelData(pixelArray, boundaries) {
		var width = boundaries.maxX - boundaries.minX + 1
		var height = boundaries.maxY - boundaries.minY + 1
		console.log("height is", height, boundaries.maxY, boundaries.minY)
		var imagedata = context.createImageData(width, height)




		var keys = Object.keys(pixelArray)
		var l = keys.length
		for (var i = 0; i < l; i++) {
			var keykeys = Object.keys(pixelArray[keys[i]])
			var ll = keykeys.length
			for (var ii = 0; ii < ll; ii++) {

				if (pixelArray[keys[i]][keykeys[ii]] === -1) continue;
				//            console.log(pixelArray[keys[i]][keykeys[ii]])

				var x = parseInt(keys[i]) - boundaries.minX
				var y = parseInt(keykeys[ii]) - boundaries.minY
				//            console.log(keykeys[ii], boundaries.minY)

				// Get the pixel index
				var pixelindex = (y * width + x) * 4

				//            // Generate a xor pattern with some random noise
				//            var red = ((x) % 256) ^ ((y) % 256)
				//            var green = ((2 * x) % 256) ^ ((2 * y) % 256)
				//            var blue = 50 + Math.floor(Math.random() * 100)
				//
				//            // Rotate the colors
				//            blue = (blue) % 256;
				//
				//            // Set the pixel data
				//            imagedata.data[pixelindex] = red; // Red
				//            imagedata.data[pixelindex + 1] = green; // Green
				//            imagedata.data[pixelindex + 2] = blue; // Blue
				//            imagedata.data[pixelindex + 3] = 255 * pixelArray[keys[i]][keykeys[ii]]; // Alpha

				imagedata.data[pixelindex] = 0 // Red
				imagedata.data[pixelindex + 1] = 0 // Green
				imagedata.data[pixelindex + 2] = 0 // Blue
				imagedata.data[pixelindex + 3] = 255 * pixelArray[keys[i]][keykeys[ii]] // Alpha
			}
		}

		context.putImageData(imagedata, 200, 200)
	}

	floodFill2DNoise()
	DrawPixelData(a, boundaries)
}

function CreateNewLayer(width, height, pivotX = .5 * width, pivotY = .5 * height) {
	var layer = document.createElement("canvas")
	layer.context = layer.getContext("2d")
	//    context.imageSmoothingEnabled= false //performance improvement test
	layer.width = width
	layer.height = height
	layer.pivotPoint = {
		x: pivotX,
		y: pivotY
	} //default value pivotPoint
	layer.Draw = function (offset, rotation = 0) {
		Draw.Sprite.call(this, defaultLayer, this, rotation, offset.x, offset.y, this.pivotPoint.x, this.pivotPoint.y)
	}
	return layer
}

//WARNING EVERYTHING THAT USES DRAW FUNCTIONS MUST NOW REFER TO LAYER AND NOT CONTEXT.




function GetCraftedLayer(steps, BehaviourSetupFunction, ThicknessFunction, ColorFunction, diameter, ChanceFunction, coolStep = steps) {
	if (!(this instanceof GetCraftedLayer)) return new GetCraftedLayer(steps, BehaviourSetupFunction, ThicknessFunction, ColorFunction, diameter, ChanceFunction, coolStep)
	var myCraftedLayer = GetCraftedLayerS(steps, BehaviourSetupFunction, ThicknessFunction, ColorFunction, diameter, ChanceFunction)

	var lastAnswer
	var answer
	for (var i = 0; i < coolStep - 1; i++) { //coolstep-1 because I use the last .next outside the loop
		answer = myCraftedLayer.next()
		if (IsUndefined(answer.value)) {
			answer = lastAnswer //this is a hack because I did something wrong somewhere :SSSSS
			console.log("what the fuck is this answer", answer)
			break
		}
		lastAnswer = answer
		// console.log(i,lastAnswer)
	}

	answer = myCraftedLayer.next()
	if (!answer.done && _autoUpdateIterators) {
		//console.log("adding to the iterators!")
		iterators.push(myCraftedLayer)
	}
	if (IsUndefined(answer.value)) {
		answer = lastAnswer //this is a hack because I did something wrong somewhere :SSSSS
		console.log("what the fuck is this answer", answer)
	}



	if (IsUndefined(answer.value)) {
		console.warn("In function GetCraftedLayer. The crafted layer is undefined.")
		console.warn("These were the inputs that caused it:")
		console.warn("	steps:", steps)
		console.warn("	BehaviourSetupFunction:", BehaviourSetupFunction)
		console.warn("	ThicknessFunction:", ThicknessFunction)
		console.warn("	ColorFunction:", ColorFunction)
		console.warn("	diameter:", diameter)
		console.warn("	ChanceFunction:", ChanceFunction)
		console.warn("	coolStep:", coolStep)
	}

	// the following is hacks to let the return be a drawable graphic while also providing a method to update the iterator of the graphic externally
	var result = answer.value
	result.Update = function () {
		return myCraftedLayer.next()
	}
	return result
}

// Get Craft by formula
function* GetCraftedLayerS(steps, BehaviourSetupFunction, ThicknessFunction, ColorFunction, diameter, ChanceFunction) {
	//if (!(this instanceof GetCraftedLayer)) return new GetCraftedLayer(steps, BehaviourSetupFunction, ThicknessFunction, ColorFunction, diameter, ChanceFunction)
	// ^ I don't really know what I'm doing.. I'm testing coroutines so I try to move the above thing to getcraftedlayer instead
	// I have a constant battle in my mind if I should use thickness function or behaviours like Go() etc.
	CallWarningOnUndefined(arguments)
	this.InfiniteFuncValue = InfiniteFuncValue
	this.steps = this.InfiniteFuncValue.call(this, steps)
	var layer1 = this.layer1 = CreateNewLayer(diameter, diameter)
	this.setup = BehaviourSetupFunction
	this.InfiniteFuncValue(this.setup)
	this.chanceFunction = ChanceFunction
	this.ThicknessFunction = ThicknessFunction
	this.ColorFunction = ColorFunction
	layer1.pivotPoint = {
		x: this.x,
		y: this.y
	}


	//hack:
	Object.assign(layer1.context, styles.default)


	this.GetThickness = function (i) {
		return this.InfiniteFuncValue(this.ThicknessFunction, i, this.steps)
	}


	for (var i = 0; i < this.steps; i++) {
		this.i = i
		//let progress = i / steps
		this.Go()
		this.Turn()

		var circleGeometry = {
			x: this.x,
			y: this.y,
			r: this.GetThickness(i)
		}

		var circleStyle = {
			fillStyle: this.InfiniteFuncValue(this.ColorFunction, i, this.steps)
		}
		//Draw.Circle(layer1, this.x, this.y, this.GetThickness(i), this.InfiniteFuncValue(this.ColorFunction, i, this.steps), 1) // TODO: Replace all this.infinite function with Infinitefunction.call(this, ...)
		Draw.Circle(layer1, circleGeometry, circleStyle) // TODO: Replace all this.infinite function with Infinitefunction.call(this, ...)

		if (!this.chanceFunction) {
			if (IsUndefined(this.layer1)) console.warn("In GetCraftedLayerS. the value to be returned is undefined")
			yield this.layer1
			continue
		}
		if (Array.isArray(this.chanceFunction)) {

			for (var ii = 0; ii < this.chanceFunction.length; ii++) {
				this.InfiniteFuncValue.call(this, this.chanceFunction[ii])
			}
		} else this.InfiniteFuncValue.call(this, this.chanceFunction) //TODO better check
		if (IsUndefined(this.layer1)) console.warn("In GetCraftedLayerS. the value to be returned is undefined")
		yield this.layer1
	}
	if (IsUndefined(this.layer1)) console.warn("In GetCraftedLayerS. the value to be returned is undefined")
	return this.layer1
} // Rename

//Get crafter by formula
function LayerRecipeCrafter(steps, BehaviourSetupFunction, ThicknessFunction, ColorFunction, diameter, ChanceFunction) {
	return function () {
		return GetCraftedLayer(steps, BehaviourSetupFunction, ThicknessFunction, ColorFunction, diameter, ChanceFunction)
	}
}

//Get Craft by constraints
function Craft(restrictions) {
	/* // fix these default parameters
	r.steps = 100
	r.diameter = 500
	r.thickness = 10
	r.color = "#ff00ff"
	r.Setup = SetupF.ConstantSpeed
	r.Extra = ExtraF.GenericBranch(.20, function() {return this.dir + (Math.random() < .5 ? .6 : dirs.left -.6) },
	*/
	return GetCraftedLayer(restrictions.steps, restrictions.setup, restrictions.thickness, restrictions.color, restrictions.diameter, restrictions.extra)
}

//Get crafter by constraints
//TODO


function DrawTextureOnce(x, y, texture) {
	Draw.Sprite(defaultLayer, texture, 0, x, y, texture.width * .5, texture.height * .5, texture.width * .5) // Fix the specific 50 dimension stuff
}

function DrawTextureInSquare(texture, times, rect) {
	for (var i = 0; i < times; i++) {
		DrawTextureOnce(GetRandomRange(rect.x, rect.x + rect.width), GetRandomRange(rect.y, rect.y + rect.height), texture())
	}
} //Vague, plz specify what it does (especially 'times')

function DrawTextureOnceInCenter(texture) {
	DrawTextureOnce(canvas.centerPosition.x, canvas.centerPosition.y, texture)
}

function ThingDrawer(x, y, textureFunction) {
	entities.push(this)
	this.textureFunction = textureFunction
	this.x = x
	this.y = y
	this.Update = function () {}
	this.Draw = function () {
		DrawTextureOnce(this.x, this.y, InfiniteFuncValue(this.textureFunction))
	}
}


function Pattern(times, padding, radius, texture, offset) {
	//The original Pattern(texture) used times = 5, padding = 300, radius = 50 or 250
	for (var ii = 0; ii < times; ii++) {
		for (var i = 0; i < times; i++) {
			Draw.Sprite(defaultLayer, InfiniteFuncValue(texture), 0, offset + i * padding, offset + ii * padding, radius, radius, radius)
			//The following is a sig-saggy version
			//Draw.Sprite(defaultLayer, InfiniteFuncValue(texture), 0, i * padding + (ii % 2) * padding * .5, 0.5 * ii * padding, radius, radius, radius)
		}
	}
}

function GetStarLayer(texture, wings, rotationOffset = 0) {
	var textureRoot = InfiniteFuncValue(texture)
	var radius = textureRoot.height
	var layer1 = CreateNewLayer(radius * 2, radius * 2)
	for (var i = 0; i < wings; i++) {
		textureRoot = InfiniteFuncValue(texture)
		Draw.Sprite(layer1, textureRoot, (i / wings) * 2 * Math.PI + rotationOffset, radius, radius, textureRoot.pivotPoint.x, textureRoot.pivotPoint.y, textureRoot.height * .5)
	}
	return layer1
}



function GetFanLayer(texture, wings, angle) {
	var exampleTexture = InfiniteFuncValue.call(this, texture)
	var radius = exampleTexture.height
	var layer1 = CreateNewLayer(radius * 2, radius * 2)
	for (var i = 0; i < wings; i++) {
		let myTexture = InfiniteFuncValue.call(this, texture)
		//										Offset
		Draw.Sprite(layer1, myTexture, i * angle - (wings * angle) * .5, radius, radius, myTexture.width * .5, myTexture.height - 20, myTexture.height * .5)
	}
	return layer1
}

function GetRandomlyDistributedFanLayer(texture, wings, angle, span) {
	var exampleTexture = InfiniteFuncValue.call(this, texture)
	var radius = exampleTexture.height
	var layer1 = CreateNewLayer(radius * 2, radius * 2)
	for (var i = 0; i < wings; i++) {
		let myTexture = InfiniteFuncValue.call(this, texture)
		var randomAngle = angle + Noise(.5 * span)
		Draw.Sprite(layer1, myTexture, randomAngle, radius, radius, myTexture.width * .5, myTexture.height - 20, myTexture.height * .5)
	}
	return layer1
}


// var allStuff = [
// 	prefabs.avocado,
// 	prefabs.balloon,
// 	prefabs.catEye,
// 	prefabs.googleMapPin,
// 	prefabs.grass,
// 	prefabs.grasss,
// 	prefabs.longGrass,
// 	prefabs.petal1,
// 	prefabs.SquiggleGrass,
// 	prefabs.straightBalloon,
// 	prefabs.stringyGrass,
// 	prefabs.tinySpookLeaf,
// 	prefabs.wideCatEye,
// 	prefabs.WitnessGrass,
// 	prefabs.wormGrass
// ]

var prefabs = {
	catEye() {
		return GetCraftedLayer(100, SetupF.ConstantSpeed, ThickF.CatEye, "#00FFFF", 500)
	},
	tinySpookLeaf() {
		return GetCraftedLayer(100, SetupF.Example, ThickF.CatEye, "#00FFFF", 500)
	},
	longGrass() {
		return GetCraftedLayer(100, SetupF.ConstantSpeed, ThickF.NarrowCatEye, ColorF.Example, 500)
	},
	grasss() {
		return GetCraftedLayer(100, SetupF.Example, ThickF.NarrowCatEye, ColorF.RandomGradient, 500)
	},
	wormGrass() {
		return GetCraftedLayer(100, SetupF.Example, ThickF.NarrowCatEye, ColorF.Example, 500)
	},
	wideCatEye() {
		return GetCraftedLayer(100, SetupF.ConstantSpeed, ThickF.Petal, ColorF.Example, 500)
	},
	balloon() {
		return GetCraftedLayer(100, SetupF.Example, ThickF.Petal, ColorF.RandomGradient, 500)
	},
	straightBalloon() {
		return GetCraftedLayer(100, SetupF.SlowingButStraight, ThickF.Petal, ColorF.Example, 500)
	},
	petal1() {
		return GetCraftedLayer(100, SetupF.SlowingButStraight, ThickF.Petal, ColorF.Petal1, 500)
	},
	googleMapPin() {
		return GetCraftedLayer(70, SetupF.StraightAccelerate, ThickF.BySpeedLog, "#00FFFF", 500)
	},
	avocado() {
		return GetCraftedLayer(25, SetupF.Avocado, ThickF.Avocado, ColorF.Example, 100)
	},
	WitnessGrass() {
		return GetCraftedLayer(100, SetupF.WitnessGrass, ThickF.WitnessGrass, ColorF.WitnessGrassColor, 500, ExtraF.WitnessGrass)
	},
	stringyGrass() {
		return GetCraftedLayer(110, SetupF.StringyGrass, 2, ColorF.StringyGrass, 500, ExtraF.StringyGrass)
	},
	SquiggleGrass() {
		return GetCraftedLayer(100, SetupF.ConstantSpeedWithBend, ThickF.Oscilating6Times, ColorF.RandomGradient, 500)
	},
	grass() {
		return GetCraftedLayer(100, SetupF.Example, ThickF.BySpeed, ColorF.RandomGradient, 500)
	},
	tinyLeafs() {
		return GetFanLayer(LayerRecipeCrafter(20, SetupF.SlowingButStraight, ThickF.NarrowCatEye, "#FFFFFF", 500, ExtraF.AlwaysTurning), 5, .4)
	}
} // Prefabs	

function GetRandomLayer() {
	return GetRandomEntryInArray(allStuff)
}
