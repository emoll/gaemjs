//Copyright © 20017-2018 Emil Elthammar
//This file is part of the "Gaem Engine”
//For conditions of distribution and use, see copyright notice in emleng.h

/* Engine.js */

/* File content description:
	"low level" stuff
	It provides the interface between the player and the game world
	It defines the Fundamentals of the game world:
		space
		time
		Collision
		physical input
*/

//console.log(window.Worker) //????

// Settings
var maxFramesPerSecondLimit = 120
var isUnitTest = false
var isWraparound = false
var isScreenClearing = true
var seedForRandomness = Math.random() // "Emil"
var backgroundFillType = "transparent" //"gradient" //"no" //
//var backgroundPalette = palettes[GetRandomEntryInArray(palettesArray)] //commented out because google compiler
//var numBackgroundColors = Math.round(Math.random() * backgroundPalette.length) //commented out because google compiler.....
var screenClearingColor = "olive" //"black" //"MistyRose" //"Turquoise" //"MidnightBlue" //"skyBlue" //"pink" //"RoyalBlue" //GameBoyPalette[3] //"#F61041" //"#363534" //"black" //"#0D0F0A" //"#151515" //"white" //"silver" // "seagreen" //

// Screen related
var canvas = document.getElementById("myCanvas")
canvas.width = document.body.clientWidth
canvas.height = window.innerHeight
canvas.centerPosition = {
	x: .5 * canvas.width,
	y: .5 * canvas.height
}
var context = canvas.getContext("2d")
//context.imageSmoothingEnabled = false

//Experiment
var bgCanvas = document.getElementById("myBGCanvas")
bgCanvas.width = document.body.clientWidth
bgCanvas.height = window.innerHeight
var bgContext = bgCanvas.getContext("2d")
//LoadBackgroundGradient() // commented out because google compiler complained
bgContext.fillStyle = backgroundGradient
bgContext.fillRect(0, 0, canvas.width, canvas.height)

//drawing layerz
var defaultLayer = {} //hack solution
defaultLayer.context = context

//var debugLayer = CreateNewLayer(canvas.width, canvas.height)
var curveLayers = [] //used by stuff
var renderLayers = [
//    debugLayer
]

var backgroundGradient
if (backgroundFillType == "gradient") {
	LoadBackgroundGradient()
}


ClearScreen() //Needs to be below the background gradients etc

//Needed variables 
var localStorage = localStorage // Line added to fix closure compiler... :U
var Tone = Tone // Fix for closure compiler

var pointer = {
	x: 100,
	y: 100
} // I guess this is used somewhere?


function Camera(x, y) {
	this.x = x
	this.y = y
	this.zoom = 1
	this.getOffset = function () {
		return {
			x: -this.x,
			y: -this.y
		}
	}
}
var camera = new Camera(0, 0)

var subscriberList = []
var controllers = []

function CheckCollision(entityIndexA, entityIndexB) {
	CheckCircleCollision(entities[entityIndexA], entities[entityIndexB])
}

var grid
var tileSize = 32 //for tile games 


var _autoUpdateIterators = true

// Event Handling
function Subscribe(funct, channel) { //this function must be called like this Subscribe.call(this, funct, channel) to retain the subscriber as the caller.
	subscriberList[channel] = DefaultParameter(subscriberList[channel], [])
	funct.parent = this //this stuff is to make it call from the right function to gain access to the right "this" context
	console.log("I'm subscribing:", this)
	subscriberList[channel].push(funct)
	console.log(subscriberList)
}

function Broadcast(channel, value) {
	if (subscriberList[channel] === undefined) return
	subscriberList[channel].forEach(function (entry) {
		var event = {
			channel: channel,
			value: value
		}
		//        console.log(channel, value)
		//        console.log("trying to use parent:", entry.parent)
		entry.call(entry.parent, event)
	})
}

function AddMyEventListeners() {

	// Mouse
	//    canvas.addEventListener("mousemove", function (event) { //canvas doesn't listen, probably because I have other layers ontop
	addEventListener("mousemove", function (event) {
		mouse.x = getMousePos(canvas, event).x
		mouse.y = getMousePos(canvas, event).y

		var message = "Mouse position: " + mouse.x + "," + mouse.y
	}, false)

	addEventListener("mousedown", function (event) {
		SetMouseButton(event.button, true)
		//        Broadcast("mousedown", event.button)

	}, false)

	addEventListener("mouseup", function (event) {
		SetMouseButton(event.button, false)
		//        Broadcast("mouseup", event.button)


	}, false)

	// Keyboard
	addEventListener("keydown", function (event) {
		if (event.repeat) return //everyone hates repeating keys
		//        Broadcast("keydown", event.which) //outcomented because google compiler
		//hard code because google compiler didn't like my broadcast system
		if (event.which == 27) {
			gameLoop = 0 // stops the game
			draw = 0
		}
		// end of hard code
	}, false)

	document.addEventListener("keyup", function (event) {
		//        Broadcast("keyup", event.which) //outcommented because google compiler
	}, false)
}


// Input Handling
var mouse = {
	x: 0,
	y: 0,
	left: false,
	middle: false,
	right: false
}

function getMousePos(canvas, event) {
	var rect = canvas.getBoundingClientRect()
	return {
		x: event.clientX - rect.left,
		y: event.clientY - rect.top
	}
}

function SetMouseButton(num, bool) {
	if (num == 0) mouse.left = bool
	if (num == 1) mouse.middle = bool
	if (num == 2) mouse.right = bool
}




function Controller(name) { //TODO: Fix so one button can broadcast multiple functions
	var self = this
	self.name = name
	self.table = []
	self.Bind = function (inputTypeStr, keyCode, message) {
		if (self.table[inputTypeStr] === undefined)
			self.table[inputTypeStr] = []
		self.table[inputTypeStr][keyCode] = message
	}
	self.Signal = function (event) {
		if (self.table[event.channel][event.value] === undefined) return
		Broadcast("controller-" + self.name, self.table[event.channel][event.value])
	}
	Subscribe(self.Signal, "keydown")
	Subscribe(self.Signal, "keyup")
}

function AddController() {
	var controller = new Controller("" + Math.random()) //previously controllers.length but google compiler doesn't like
	controllers.push(controller)
	return controller
}


// META 
var player //This variable should be replaced by players
function Player(name, state, score) {
	var self = this
	self.name = name
	self.state = state
	self.score = score
	self.AddScore = function (event) {
		self.score += event.value
	}
}

function Highscore() {
	var names = []
	var scores = []

	var AddScore = function (name, score) {
		score.push(score)
		names.push(name)
	}
}


const RenderMode = {
	AUTO: 1,
	MANUAL: 2,
	FULLAUTO: 3 //Update function is based on renderer
}

var renderMode = RenderMode.FULLAUTO

// Game Loop
var gameLoop

var GUI = []
var entities = []
var iterators = [] // undone drawing thingies

var lastUpdate = 0
var IsEngineStarted = false

function StartEngine() {
	if (IsEngineStarted) return
	else IsEngineStarted = true

	AddMyEventListeners()

	//set interval is apparently satan, so I'm replacing it with timeout
	//    gameLoop = setInterval(function () {
	//        var now = Date.now()
	//        var dt = now - lastUpdate
	//        lastUpdate = now
	//        engine.Update(dt)
	//    }, 1000 / maxFramesPerSecondLimit)
	gameLoop = function (dt) {
		//        canvas.width = document.body.clientWidth
		//        canvas.height = window.innerHeight
		if (!dt) {
			var now = Date.now()
			dt = now - lastUpdate
			lastUpdate = now
		}
		engine.Update(dt)
		const msPerFrame = Math.round(1000 / maxFramesPerSecondLimit)
		var remainder
		if (dt > msPerFrame) {
			remainder = dt - msPerFrame
		} else {
			remainder = 0
		}
		if (renderMode == RenderMode.MANUAL) engine.Draw()
		if (renderMode != RenderMode.FULLAUTO) setTimeout(gameLoop, msPerFrame - remainder)
	}
	//    gameLoop()
	window.requestAnimationFrame(engine.Draw) //Draw is unbundled from updates! JS keeps track of drawing.

	if (isUnitTest) log("engine started!")
}

function UpdateIterators() {
	iterators.forEach(function (iterator, i) {
		var result = iterator.next()
		if (result.done) iterators.pop(i)
	})
}

var engine = {
	Update(dt) {
		camera.x = mouse.x
		camera.y = mouse.y
		var updateTimes = 1
		if (_autoUpdateIterators) UpdateIterators()
		for (var t = 0; t < updateTimes; t++) {
			var l = entities.length
			for (var i = 0; i < l; i++) {
				//this sucks! This has nothing to do with entities. there should be a collider list that only goes through colliders
				// Therefore I comment it out now:
				//			if (i != entities.length - 1) {
				//				for (var u = i + 1; u < entities.length; u++) {
				//					CheckCollision(i, u) 
				//				}
				//			}

				entities[i].Update(dt)
				//TODO: Separate this to only wrap around engine
				if (isWraparound) {
					entities[i].x = WrapAround(entities[i].x, canvas.width)
					entities[i].y = WrapAround(entities[i].y, canvas.height)
				}

				if (entities[i].dead) {
					delete entities[i]
					entities.splice(i, 1)
				}
			}
		}
	},
	Draw(dt) {
		dt = dt - lastUpdate
		lastUpdate += dt

		if (renderMode == RenderMode.FULLAUTO) {
			gameLoop(dt)
		}
		var cameraOffset = camera.getOffset()
		if (isScreenClearing) ClearScreen()
		//context.save() //alpha fix
		//DrawBoard() //for grid games
		//    DrawDirt(cameraOffset)
		var i = 0
		var l = curveLayers.length
		for (i; i < l; i++) {
			curveLayers[i].Draw(cameraOffset)
		}
		DrawEntities(cameraOffset)
		i = 0
		l = renderLayers.length
		for (i; i < l; i++) {
			DrawTextureOnce(0, 0, renderLayers[i])
		}
		//context.restore() //alpha fix
		//    context.drawImage(reflectionLayer, 0, 500, canvas.width, canvas.height)
		if (renderMode == RenderMode.AUTO || renderMode == RenderMode.FULLAUTO) window.requestAnimationFrame(engine.Draw)
	}
}
//var requestFrame = window.requestAnimationFrame

//var myWorker = new Worker("js/worker.js")
//myWorker.onmessage = function (e) {
//    console.log("helo", e.data)
//}
//// myWorker.postMessage({
////                    a: points,
////                    e: o,
////                    f: resolution
////                })
//myWorker.postMessage({
//    points: [],
//    startX: 0,
//    endX: 1920,
//    f: 1,
//    offset: {
//        x: 10,
//        y: 10
//    },
//    steps: 50
//})

function CreateChangePointAfterFunctionFunction(startX, endX, steps, f) {
	var xs = new Int16Array(new ArrayBuffer(2 * steps))
	for (var i = 0; i < steps; i++) {
		xs[i] = startX + (i / (steps - 1)) * endX
	}
	return function (xPoints, yPoints, offset, z) {
		offset.x = Math.round(offset.x)
		offset.y = Math.round(offset.y)
		for (var i = 0; i < steps; i++) {
			xPoints[i] = xs[i]
			var y = z * f((1 / z) * (xs[i] - offset.x)) + offset.y
			yPoints[i] = y > 1080 ? 1080 : y
		}
	}

}

function changePointsAfterFunction(xPoints, yPoints, startX, endX, f, offset, steps) { //specifically used to make a line from left side of screen to right side, which is  why x and offset are separate things
	offset.x = Math.round(offset.x)
	offset.y = Math.round(offset.y)
	for (var i = 0; i < steps; i++) {
		var x = startX + (i / (steps - 1)) * endX
		xPoints[i] = x
		yPoints[i] = f(x - offset.x) + offset.y
	}
}

function GetCurveLayer(noiseFunction, color, resolution = 48, lineWidth, lineColor = "white",
	xPoints = new Int16Array(new ArrayBuffer(2 * resolution)),
	yPoints = new Int16Array(new ArrayBuffer(2 * resolution)),
	ctx = context,
	lastOffset = {
		x: 0,
		y: 0
	},
	lo = lastOffset,
	myChangePointsAfterFunction,
	mySmoothPathThroughPoints = SmoothPathThroughPoints,
	//    myWorker = new Worker("js/worker.js"),
	_webWorkers = false,
	prevCanvasWidth = canvas.width
) {
	myChangePointsAfterFunction = CreateChangePointAfterFunctionFunction(0, canvas.width, resolution, noiseFunction)
	//    myWorker.onmessage = function (e) {
	//        points = e.data
	//        //        Draw2()
	//        ctx.beginPath()
	//        mySmoothPathThroughPoints.call(this, points, ctx)
	//        ctx.lineTo(canvas.width, canvas.height)
	//        ctx.lineTo(0, canvas.height)
	//        //        ctx.closePath()
	//        if (color === "reflection") {
	//            ctx.fillStyle = reflectionPattern
	//            ctx.globalAlpha = 1
	//
	//            reflectionLayer.context.clearRect(0, 0, canvas.width, canvas.height)
	//        } else {
	//            ctx.fillStyle = "red" //color
	//        }
	//        if (lineWidth) {
	//            ctx.lineWidth = lineWidth
	//            ctx.strokeStyle = lineColor
	//            ctx.stroke()
	//        }
	//
	//        ctx.fill()
	//    }
	return {
		//        var window
		Draw(o) { //o = offset
			if (prevCanvasWidth != canvas.width) {
				myChangePointsAfterFunction = CreateChangePointAfterFunctionFunction(0, canvas.width, resolution, noiseFunction)
				prevCanvasWidth = canvas.width
			}
			if (_webWorkers) {
				var data = {
					points: points,
					startX: 0,
					endX: canvas.width,
					f: 1,
					offset: o,
					steps: resolution
				}
				myWorker.postMessage(data, [points.buffer])
				console.log("ye")
				return
			}
			if (o.x == lo.x && o.y == lo.y) {} else {
				myChangePointsAfterFunction.call(this, xPoints, yPoints, o, camera.zoom)
				lo.x = o.x
				lo.y = o.y
			}
			//the following is same as Draw2
			ctx.beginPath()
			mySmoothPathThroughPoints.call(this, xPoints, yPoints, ctx)
			ctx.lineTo(canvas.width, canvas.height)
			ctx.lineTo(0, canvas.height)
			ctx.closePath()
			if (color === "reflection") {
				ctx.fillStyle = reflectionPattern
				ctx.globalAlpha = 1

				reflectionLayer.context.clearRect(0, 0, canvas.width, canvas.height)

			} else {
				ctx.fillStyle = color
			}
			if (lineWidth) {
				ctx.lineWidth = lineWidth
				ctx.strokeStyle = lineColor
				ctx.stroke()
			}
			ctx.fill()
		},

		Draw2() {
			ctx.beginPath()
			mySmoothPathThroughPoints.call(this, points)
			ctx.lineTo(canvas.width, canvas.height)
			ctx.lineTo(0, canvas.height)
			//        ctx.closePath()
			if (color === "reflection") {
				ctx.fillStyle = reflectionPattern
				ctx.globalAlpha = 1
				reflectionLayer.context.clearRect(0, 0, canvas.width, canvas.height)
			} else {
				ctx.fillStyle = color
			}
			if (lineWidth) {
				ctx.lineWidth = lineWidth
				ctx.strokeStyle = lineColor
				ctx.stroke()
			}
			ctx.fill()

		},
	}
}

function Layer() { //something something collection of layers so you draw one layer and that layer then draw all sub layers
	this.subLayers = []
	this.Draw = function () {
		this.subLayers.Draw()
	}
}


//todo, drawing layers, so you can put different 


function SmoothPathThroughPoints(xPoints, yPoints, context) {
	// move to the first point
	context.moveTo(xPoints[0], yPoints[0])
	var l = xPoints.length - 2
	for (var i = 1; i < l; i++) {
		var xc = Math.round(.5 * (xPoints[i] + xPoints[i + 1]))
		var yc = Math.round(.5 * (yPoints[i] + yPoints[i + 1]))
		context.quadraticCurveTo(xPoints[i], yPoints[i], xc, yc)
	}
	// curve through the last two points
	context.quadraticCurveTo(xPoints[i], yPoints[i], xPoints[i + 1], yPoints[i + 1])
}


var _layerSorting = true

function sortFunc(a, b) {
	if (a.sortTop) return 100
	if (a.sortBottom) return -100
	if (b.sortTop) return -100
	if (b.sortBottom) return 100
	return a.y - b.y
}

function DrawEntities(offset) {
	if (_layerSorting) {
		entities.sort(sortFunc)
	}
	for (var i = 0; i < entities.length; i++) {
		entities[i].Draw(offset)
		if (isWraparound) DrawWrapAround(entities[i])
	}
}



function DrawWrapAround(entity) {
	entity.Draw(entity.x + canvas.width)
	entity.Draw(entity.x - canvas.width)
	entity.Draw(entity.x, entity.y + canvas.height)
	entity.Draw(entity.x, entity.y - canvas.height)
	entity.Draw(entity.x + canvas.width, entity.y + canvas.height)
	entity.Draw(entity.x + canvas.width, entity.y - canvas.height)
	entity.Draw(entity.x + canvas.width, entity.y - canvas.height)
	entity.Draw(entity.x - canvas.width, entity.y + canvas.height)
}

function DrawGUI() {
	for (var i = 0; i < GUI.length; i++) {
		GUI[i].Draw()
	}
}


//function LoadBackgroundGradient() { //commented out because google compiler
//    backgroundGradient = context.createLinearGradient(0, 0, 0, 1080)
//    // backgroundGradient.addColorStop(0, "#F61041")
//    // backgroundGradient.addColorStop(1, "#710525")
//    for (var i = 0; i < numBackgroundColors; i++) {
//        backgroundGradient.addColorStop(i / numBackgroundColors, backgroundPalette[i])
//    }
//    backgroundGradient.addColorStop(0, backgroundPalette[0])
//    backgroundGradient.addColorStop(1, backgroundPalette[2])
//    //		backgroundGradient.addColorStop(0, palettes.blackSkyBluePinkGlow[12])
//    //		backgroundGradient.addColorStop(.2, palettes.blackSkyBluePinkGlow[13])
//    //		backgroundGradient.addColorStop(.5, palettes.blackSkyBluePinkGlow[14])
//    //		backgroundGradient.addColorStop(.7, palettes.blackSkyBluePinkGlow[15])
//    //		backgroundGradient.addColorStop(.9, palettes.blackSkyBluePinkGlow[16])
//    //		backgroundGradient.addColorStop(1, palettes.blackSkyBluePinkGlow[21])
//}

function ClearScreen() {
	if (isUnitTest) log("CLEARING SCREEN")
	context.fillStyle = screenClearingColor
	if (backgroundFillType == "transparent") {
		context.clearRect(0, 0, canvas.width, canvas.height)
		return
	} else if (backgroundFillType == "gradient") {
		context.fillStyle = backgroundGradient
	}
	FillScreen(1)
}

// Gradient clearning!
// var grd = context.createLinearGradient(1000, 0, 0, 1000)
// grd.addColorStop(0, "#F61041")
// grd.addColorStop(1, "#710525")
// context.fillStyle = grd
// end of clearing

function FillScreen(alpha) {
	context.globalAlpha = alpha //Bit broken due to rounding error
	//An idea might be to use the subtractive fill I use elsewhere.
	context.fillRect(0, 0, canvas.width, canvas.height)
}


//Randomness
Math.seedrandom(seedForRandomness)







/*
objects can subscribe to phenomena, they then become a observer of the phenomena.
phenomena can be gradual or instant,
phenomena can be in different states.

you can request observation of current state
or
you can get notified as an observer whenever the state changes.

A controlSet is a map of input states. //most cases from human input. mapping player intention to input data

A controller observes inputs of a control set.



*/
//var controlSet = {
//    Jump: [keyboard_x, keyboard_z, ]
//    Hit: [GUI_]
//}

//
//function Phenomena() {
//    this.observers = []; // observers
//}
//
//Phenomena.prototype = {
//
//    subscribe: function (fn) {
//        this.observers.push(fn)
//    },
//
//    unsubscribe: function (fn) {
//        this.observers = this.observers.filter(
//            function (item) {
//                if (item !== fn) {
//                    return item;
//                }
//            }
//        )
//    },
//
//    fire: function (o, thisObj) {
//        var scope = thisObj || window;
//        this.observers.forEach(function (observer) {
//            observer.call(scope, o)
//        })
//    }
//}
//
//// log helper
//
//var log = (function () {
//    var log = "";
//
//    return {
//        add: function (msg) {
//            log += msg + "\n";
//        },
//        show: function () {
//            alert(log)
//            log = "";
//        }
//    }
//})()
//
//function run() {
//
//    var clickHandler = function (item) {
//        log.add("fired: " + item)
//    }
//
//    var click = new Click()
//
//    click.subscribe(clickHandler)
//    click.fire('event #1')
//    click.unsubscribe(clickHandler)
//    click.fire('event #2')
//    click.subscribe(clickHandler)
//    click.fire('event #3')
//
//    log.show()
//}
