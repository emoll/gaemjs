function Process() {
	prerequisite; // or dependencies?
	ExamplesOfPreRequisities; // list of example to help suggest what is possible in different contexts
	SubProcesses;
	Label; //External description of what it does.
	Cost;
	Outcomes;
	Duration; // might be dynamic dependent on environment and input.
	Rate; //of side effects
	SideEffects; //Like outcomes but during process
	MaintenanceCost; //Negative sideeffects that needs to be regulated to maintain process
	Progress; // Indication of how done

	//properties
	stoppable;
	triggerable;

}


function Outcome() {
	Cause; // Can be unknown
	Consequences; // Other processes or outcomes. Can be unknown
	Probability; // Can be unknown
}
